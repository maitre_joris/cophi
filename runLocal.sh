cd multimode-version;
ln -s ../fonts fonts
ln -s ../thirdparty thirdparty
mkdir build; cd build;
# A build will produce both client-server app and standalone app
cmake .. -DTHIRDPARTY_DIR=../..
make -j9
cd ..;
./build/paraCoordLocal 1000 600 ../data-samples/carsCleaned.csv --with-column-labels --with-row-labels -k 6 -c kmeans -n 6,7,8 -o 1,5
