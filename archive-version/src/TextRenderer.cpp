#include <utility>

#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#include <vector.h>
#include <wchar.h>
#include <queue>

#include "Color.h"
#include "Rectangle.h"
//#include <tulip/QuadTree.h>
#include "Iterator.h"

#include <TextRenderer.h>

#include <edtaa3func.h>
#include <utf8Iterator.h>

#include <iostream>


// ------------------------------------------------------ make_distance_map ---
unsigned char *
make_distance_map( unsigned char *img,
                   unsigned int width, unsigned int height )
{
    short * xdist = (short *)  malloc( width * height * sizeof(short) );
    short * ydist = (short *)  malloc( width * height * sizeof(short) );
    double * gx   = (double *) calloc( width * height, sizeof(double) );
    double * gy      = (double *) calloc( width * height, sizeof(double) );
    double * data    = (double *) calloc( width * height, sizeof(double) );
    double * outside = (double *) calloc( width * height, sizeof(double) );
    double * inside  = (double *) calloc( width * height, sizeof(double) );
    size_t i;

    // Convert img into double (data)
    double img_min = 255, img_max = -255;
    for( i=0; i<width*height; ++i)
    {
        double v = img[i];
        data[i] = v;
        if (v > img_max) img_max = v;
        if (v < img_min) img_min = v;
    }
    // Rescale image levels between 0 and 1
    for( i=0; i<width*height; ++i)
    {
        data[i] = (img[i]-img_min)/img_max;
    }

    // Compute outside = edtaa3(bitmap); % Transform background (0's)
    computegradient( data, width, height, gx, gy);
    edtaa3(data, gx, gy, height, width, xdist, ydist, outside);
    for( i=0; i<width*height; ++i)
        if( outside[i] < 0 )
            outside[i] = 0.0;

    // Compute inside = edtaa3(1-bitmap); % Transform foreground (1's)
    memset(gx, 0, sizeof(double)*width*height );
    memset(gy, 0, sizeof(double)*width*height );
    for( i=0; i<width*height; ++i)
        data[i] = 1 - data[i];
    computegradient( data, width, height, gx, gy);
    edtaa3(data, gx, gy, height, width, xdist, ydist, inside);
    for( i=0; i<width*height; ++i)
        if( inside[i] < 0 )
            inside[i] = 0.0;

    // distmap = outside - inside; % Bipolar distance field
    unsigned char *out = (unsigned char *) malloc( width * height * sizeof(unsigned char) );
    for( i=0; i<width*height; ++i)
    {
        outside[i] -= inside[i];
        outside[i] = 128+outside[i]*16;
        if( outside[i] < 0 ) outside[i] = 0;
        if( outside[i] > 255 ) outside[i] = 255;
        out[i] = 255 - (unsigned char) outside[i];
        //out[i] = (unsigned char) outside[i];
    }

    free( xdist );
    free( ydist );
    free( gx );
    free( gy );
    free( data );
    free( outside );
    free( inside );
    return out;
}

//*****************************FontTextureManager**********************
FontTextureManager::FontTextureManager() :
    currentIndex(0) {

}

FontTextureManager::~FontTextureManager() {
    for(std::vector<texture_atlas_t*>::iterator it=atlasVector.begin() ;
        it != atlasVector.end() ;
        ++it) {
        texture_atlas_delete(*it);
    }

    for(std::vector<texture_font_t*>::iterator it=texturesVector.begin() ;
        it != texturesVector.end() ;
        ++it) {
        texture_font_delete(*it);
    }
}

int FontTextureManager::loadFont(std::string fontName) {
    texture_atlas_t* atlas =      texture_atlas_new( 1024, 1024, 1 );
    texture_font_t* fontTexture = texture_font_new( atlas, fontName.c_str(), 64 );
    texture_atlas_upload( atlas );
    atlasVector.push_back(atlas);
    texturesVector.push_back(fontTexture);
    int fontIndex = currentIndex++;
    fontMap[fontName] = fontIndex;
    return fontIndex;
}

int FontTextureManager::getId(std::string fontName) {

    std::map<std::string, int>::iterator font = fontMap.find(fontName);
    if (font == fontMap.end()) {
        return loadFont(fontName);
    }

    return font->second;
}

std::string FontTextureManager::getName(int fontId) {
    std::map<std::string,int>::iterator it = fontMap.begin();
    for (;it!=fontMap.end();++it) {
        if (it->second == fontId)
            return it->first;
    }
    return "";
}

texture_atlas_t* FontTextureManager::getAtlas(int fontId) {
    return atlasVector[fontId];
}

texture_atlas_t* FontTextureManager::getAtlas(std::string fontName) {
    return getAtlas(getId(fontName));
}

texture_font_t* FontTextureManager::getTexture(int fontId) {
    return texturesVector[fontId];
}

texture_font_t* FontTextureManager::getTexture(std::string fontName) {
    return getTexture(getId(fontName));
}

void FontTextureManager::convertTextureToDistanceMap(int fontId) {
    if (!distanceMapComputed){
        texture_atlas_t* atlas = getAtlas(fontId);
        unsigned char* map = make_distance_map(atlas->data, atlas->width, atlas->height);
        memcpy(atlas->data, map, atlas->width*atlas->height*sizeof(unsigned char));
        free(map);
        texture_atlas_upload(atlas);
    }
    distanceMapComputed=true;
}

static std::vector<wchar_t> getCharcodes(std::string str) {
    Iterator<wchar_t>* utf8iter = new Utf8Iterator(str);
    std::vector<wchar_t> charcodes;
    while(utf8iter->hasNext()) {
        charcodes.push_back(utf8iter->next());
    }
    delete utf8iter;
    return charcodes;
}

FontTextureManager* FontTextureManager::singleton = 0;
//*****************************Label************************************
Label::Label(std::string text, std::string font, float angle, Vec2f pos) : nodePos(pos), scaling(1), rotation(angle) {
    this->font = FontTextureManager::getInstance()->getId(font);
    glGenBuffers(1, &vboID);
    setText(text);
}

Rectf Label::getBoundingBox() {
    texture_font_t* texFont = FontTextureManager::getInstance()->getTexture(font);
    float ascender = texFont->ascender;
    float descender = texFont->descender;
    return Rectf(nodePos[0] - length/(2*texFont->height), nodePos[1] - ascender/texFont->height,
            nodePos[0] + length/(2*texFont->height), nodePos[1] - descender/texFont->height );
}

Rectf Label::getAxisAlignedViewBox(float textScale, GlMat4f matProjMod) {
    texture_font_t* texFont = FontTextureManager::getInstance()->getTexture(font);
    float x1 = -length/(2*texFont->height) * textScale;
    float x2 = length/(2*texFont->height) * textScale;
    float y1 = 0.5 * textScale;
    float y2 = -0.5 * textScale;

    //computing rotated boundingBox
    //includes rotation and translation to node position
    Vec2f points[4];
    points[0] = Vec2f(cos(rotation)*x1 - sin(rotation)*y1,
                      sin(rotation)*x1 + cos(rotation)*y1) + nodePos;
    points[1] = Vec2f(cos(rotation)*x2 - sin(rotation)*y1,
                      sin(rotation)*x2 + cos(rotation)*y1) + nodePos;
    points[2] = Vec2f(cos(rotation)*x2 - sin(rotation)*y2,
                      sin(rotation)*x2 + cos(rotation)*y2) + nodePos;
    points[3] = Vec2f(cos(rotation)*x1 - sin(rotation)*y2,
                      sin(rotation)*x1 + cos(rotation)*y2) + nodePos;

    //Find min and max X and Y to get Axis aligned box
    float xmin=points[0].getX(), xmax=xmin, ymin=points[0].getY(), ymax=ymin;
    for(int i =0; i<4 ; ++i) {
        Vec2f v=points[i];
        if(v.getX() > xmax) xmax = v.getX();
        else if(v.getX() < xmin) xmin = v.getX();

        if(v.getY() > ymax) ymax = v.getY();
        else if(v.getY() < ymin) ymin = v.getY();
    }

    //apply projection to the box
    Vec4f bottomLeft(xmin, ymin, 0.0,1.0);
    bottomLeft = matProjMod * bottomLeft;
    Vec4f topRight(xmax, ymax, 0.0,1.0);
    topRight = matProjMod * topRight;

    return Rectf(bottomLeft[0], bottomLeft[1], topRight[0], topRight[1]);
}

void Label::computeViewBox(const float maxTextSize, const float minTextSize, const Vec2f screenRes, const GlMat4f matProjMod, const float occlusionRatio) {
    //compute scaling for size
    float scaleY = matProjMod[1][1];

    viewBox = getBoundingBox();
    float textSize = (viewBox[1]-viewBox[0])[1]*screenRes[1]*scaleY/2.0f;
    float maxScale = maxTextSize/textSize;
    float minScale = minTextSize/textSize;
    float scale= std::min(maxScale*scaleY, std::max(minScale*scaleY, scaleY));
    scaling[1] = scale/scaleY;
    scaling[0] = scaling[1];

    Vec2f center = nodePos;
    viewBox[0] -= center;
    viewBox[1] -= center;
    viewBox[0] *= scaling*occlusionRatio;
    viewBox[1] *= scaling*occlusionRatio;
    viewBox[0] += center;
    viewBox[1] += center;

    GlMat4f m = matProjMod;
    //m.transpose();
    Vec4f tmp = Vec4f(viewBox[0], 0.0, 1.0);
    tmp = m*tmp;
    viewBox[0][0] = tmp[0];
    viewBox[0][1] = tmp[1];
    tmp = Vec4f(viewBox[1], 0.0, 1.0);
    tmp = m*tmp;
    viewBox[1][0] = tmp[0];
    viewBox[1][1] = tmp[1];
}

void Label::setText(std::string text) {
    data.clear();
    texture_font_t* textureFont = FontTextureManager::getInstance()->getTexture(font);

    std::vector<wchar_t> charcodes = getCharcodes(text);

    wchar_t previous=0;
    vec2 pen = {{ 0 , 0 }};
    for( std::vector<wchar_t>::const_iterator it=charcodes.cbegin()
         ; it != charcodes.cend() ; ++it) {
        texture_glyph_t *glyph = texture_font_get_glyph( textureFont, *it );
        if( glyph != NULL ){
            int kerning = 0;
            if( it != charcodes.cbegin()){
                kerning = texture_glyph_get_kerning( glyph, previous );
            }
            pen.x += kerning;
            float x0 = pen.x + glyph->offset_x;
            float y0 = pen.y + glyph->offset_y;
            float x1 = x0 + glyph->width;
            float y1 = y0 - glyph->height;
            float s0 = glyph->s0;
            float t0 = glyph->t0;
            float s1 = glyph->s1;
            float t1 = glyph->t1;
            /*    2------1  5
                 *    |     /  /|
                 *    |   /  /  |
                 *    | /  /    |
                 *    0  3------4
                 */
            data.push_back(Vec4f(x0,y0, s0,t0));
            data.push_back(Vec4f(x1,y1, s1,t1));
            data.push_back(Vec4f(x0,y1, s0,t1));
            data.push_back(Vec4f(x0,y0, s0,t0));
            data.push_back(Vec4f(x1,y0, s1,t0));
            data.push_back(Vec4f(x1,y1, s1,t1));
            pen.x += glyph->advance_x;
        }
        previous = *it;
    }

    length = pen.x;
    //float height = textureFont->ascender - textureFont->descender;
    float height = textureFont->height;

    for(unsigned int i=0 ; i<data.size() ; ++i) {
        data[i][0] = (data[i][0] - length/2) / height ;
        data[i][1] /= height;
    }

    glBindBuffer( GL_ARRAY_BUFFER, vboID );
    glBufferData( GL_ARRAY_BUFFER, sizeof( Vec4f ) * data.size(), &data[0], GL_STATIC_DRAW );
}



//*********************************TextProgram********************************
TextProgram::TextProgram() : ShaderProgram("../shaders/text_shaders2.glsl" ) {
    posCoord   = glGetAttribLocation(  program, "posXYtexST" );
    matProjMod = glGetUniformLocation( program, "MatProjMod" );
    texture    = glGetUniformLocation( program, "texture"    );
    pos        = glGetUniformLocation( program, "pos"        );
    res        = glGetUniformLocation( program, "res"        );
    scaling    = glGetUniformLocation(program, "scaling");
    activateOutline = glGetUniformLocation(program, "activate_outline");
    outlineColor = glGetUniformLocation(program, "outline_color");
    glowColor = glGetUniformLocation(program, "glow_color");
    textColor = glGetUniformLocation(program, "glyph_color");
    rotation = glGetUniformLocation(program, "rotAngle");
}

//****************************TextRenderer*************************
TextRenderer::TextRenderer(MatrixManager *manager) :
    matrixMangr(manager)
{
    textShaders = new TextProgram();
    textColor = Color::Black;
    outlineColor = Color::Black;
    glowColor = Color::White;

    return;
}

Label* TextRenderer::createLabel(Vec3f position, float angle,std::string font, std::string text) {
    return new Label(text, font, angle, position);
}

void TextRenderer::draw(std::vector<Label *> &labels, float minTextSize, float maxTextSize) {
    if (labels.empty()){
        return;
    }
    glEnable( GL_BLEND );
    glDisable( GL_CULL_FACE);
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    glDepthFunc(GL_LESS);

    std::vector<Label*>::const_iterator itOL = labels.begin();

    glUseProgram( textShaders->program );
    glEnableVertexAttribArray(textShaders->posCoord);

    GlMat4f matProjMod = matrixMangr->getMatProjMod();

    glUniformMatrix4fv(textShaders->matProjMod, 1, GL_FALSE, &matProjMod[0][0]);
    glUniform1i( textShaders->texture, 0 );
    glActiveTexture( GL_TEXTURE0 );

    texture_atlas_t* atlas = FontTextureManager::getInstance()->getAtlas((*itOL)->font);
    glBindTexture( GL_TEXTURE_2D, atlas->id );
    int curfont = (*itOL)->font;

    glUniform1i(textShaders->activateOutline, false);
    glUniform3f(textShaders->outlineColor, outlineColor.getR()/255.f, outlineColor.getG()/255.f, outlineColor.getB()/255.f);
    glUniform3f(textShaders->glowColor, glowColor[0], glowColor[1], glowColor[2]);

    glUniform3f(textShaders->textColor, textColor.getR()/255.f, textColor.getG()/255.f, textColor.getB()/255.f);
    glUniform4f(textShaders->res, matrixMangr->getWindowWidth(), matrixMangr->getWindowHeight(), minTextSize, maxTextSize);

    float i =0;
    for( ; itOL != labels.end(); ++itOL ){
        Label* l = (*itOL);
        {
            glBindBuffer(GL_ARRAY_BUFFER, l->vboID);
            glVertexAttribPointer(textShaders->posCoord, 4, GL_FLOAT, GL_FALSE, 0, NULL);
        }
        if (curfont != (*itOL)->font) {
            texture_atlas_t* atlas = FontTextureManager::getInstance()->getAtlas((*itOL)->font);
            curfont = (*itOL)->font;
            glBindTexture( GL_TEXTURE_2D, atlas->id );
        }

        glUniform1f(textShaders->rotation, l->rotation);
        glUniform2fv(textShaders->scaling, 1, &(l->scaling[0]));
        Vec2f center = l->nodePos;
        glUniform2fv( textShaders->pos, 1, &(center[0]) );
        glDrawArrays( GL_TRIANGLES, 0, l->data.size() );
        glBindBuffer( GL_ARRAY_BUFFER, 0 );
        i++;
    }
    glBindTexture( GL_TEXTURE_2D, 0 );

    glDisableVertexAttribArray( textShaders->posCoord );
    glDisable( GL_BLEND );
    glDisable(GL_DEPTH_TEST);
}

float TextRenderer::getLabelSize(Label *label) const {
    Rectf viewBox = label->getBoundingBox();
    float resY = matrixMangr->getWindowHeight();

    /////////////
    float scaleY = matrixMangr->getMatProjMod()[1][1];
    /////////////

    float textSize = (viewBox[1]-viewBox[0])[1]*resY*scaleY/2.0f;
    return textSize*1.75;
}

void TextRenderer::setLabelSize(Label *label, float newSize) {
    Rectf viewBox = label->getBoundingBox();
    float resY = matrixMangr->getWindowHeight();
    float scaleY = matrixMangr->getMatProjMod()[1][1];
    float textSize = (viewBox[1]-viewBox[0])[1]*resY*scaleY/2.0f;
    float scale = newSize/textSize*1.75;
    label->scaling.set(scale, scale);
}

void TextRenderer::computeLabelViewBox(Label *label)
{
    label->viewBox = label->getBoundingBox();

    Vec2f center = label->nodePos;
    label->viewBox[0] -= center;
    label->viewBox[1] -= center;
    label->viewBox[0] *= label->scaling;
    label->viewBox[1] *= label->scaling;
    label->viewBox[0] += center;
    label->viewBox[1] += center;

    GlMat4f m = matrixMangr->getMatProjMod();

    //m.transpose();
    Vec4f tmp = Vec4f(label->viewBox[0], 0.0, 1.0);
    tmp = m*tmp;
    label->viewBox[0][0] = tmp[0];
    label->viewBox[0][1] = tmp[1];
    tmp = Vec4f(label->viewBox[1], 0.0, 1.0);
    tmp = m*tmp;
    label->viewBox[1][0] = tmp[0];
    label->viewBox[1][1] = tmp[1];
}

//}
