/*
  Copyright 2015 Timothée Jourde <itim.lcf@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <Renderer.h>

#include <GL/glut.h>

#include "Vector.h"

#include <tuple>
#include <iostream>



Renderer::Renderer()
    : Matrix()
    //    , LabelRenderer   (&Matrix, "../fonts/Vera.ttf", 0, 10)
    , AxeLabelRenderer(&Matrix, "../fonts/Vera.ttf", 10, 15)
    //, ColorGradient({Color(0,255,0), Color(60,60,60), Color(255,0,0)}, true)
    //, ColorGradient({Color(142,186,152), Color(67,162,202)}, true)
    , ColorGradient({Color(0,57,115), Color(200,200,160), Color(145,40,55)}, true)
//>>>>>>> .r9338
    , IntervalAsSize(false), MappingAnimationTime(1.), StartAnimationTime(-1.),
      DeletingDimension(false), RemoveDimensionStep(-1.), DeletedDimension(-1)
{
    Matrix.initViewport(Vec4i(0,0,10,10));

    glGetIntegerv(GL_MAX_VERTEX_UNIFORM_VECTORS,   &MaxVertexUniform);
    glGetIntegerv(GL_MAX_FRAGMENT_UNIFORM_VECTORS, &MaxFragmentUniform);

    /////////////////////////
    MaxVertexUniform /= 2;
    MaxFragmentUniform /= 2;
    /////////////////////////

    MaxVertexUniform   -= RESERVED_UNIFORM; // on laisse de la marge pour les paramètres
    MaxFragmentUniform -= RESERVED_UNIFORM;

    MaxNodes = MaxVertexUniform / SIZEOF_NODE;
    MaxEdges = MaxVertexUniform / (SIZEOF_EDGE + 2*SIZEOF_NODE);

    glGenBuffers(1, &NodesBuffer);
    glGenBuffers(1, &MarksBuffer);
    glGenBuffers(1, &FrequenciesBuffer);
    glGenBuffers(1, &ArrowsBuffer);
    glGenBuffers(1, &EdgesBuffer);
    glGenBuffers(1, &EdgesElements);

    VertexUniformBuffer   = new Vec4f[MaxVertexUniform];
    FragmentUniformBuffer = new Vec4f[MaxFragmentUniform];

    SetNodesBuffer();
    SetEdgesBuffer();
    SetFrequenciesBuffer();

    return;
}
Renderer::~Renderer()
{
    delete [] VertexUniformBuffer;
    delete [] FragmentUniformBuffer;
    // TODO: désallouer buffers OpenGL ?...
}

void Renderer::OnRootGraph(Data & d, bool center)
{
    DataSource = &d;

    Invert.resize(DataSource->GetDimensions().size());

    SetMarksBuffer();
    UpdateArrowsBuffer();

    UpdateLabels();

    if(center)
        Fit();

    //    glutPostRedisplay();

    //    LabelRenderer.convertTextureToDistanceMap();
    //AxeLabelRenderer.convertTextureToDistanceMap();
}
void Renderer::OnSubGraph (Data & observable, int dimension, int clusterId, node clusterNode, const Data::subGraph & subgraph)
{
    FocusedEdge     = edge();
    FocusedSubGraph = clusterNode;
}

void Renderer::OnSubGraph (Data & observable, edge e, const Data::subGraph & subgraph)
{
    FocusedEdge     = e;
    FocusedSubGraph = node();
}

// void Renderer::OnDelete(Data & observable) {
//    DataSource = nullptr;
// }

void Renderer::SetNodesBuffer()
{
    /* VBO */
    Vec3f * buffer = new Vec3f[MaxNodes * 6];

    for (unsigned i=0; i < MaxNodes; i ++)
    {
        buffer[6*i + 0] = Vec3f(0.f, 0.f, i);
        buffer[6*i + 1] = Vec3f(1.f, 0.f, i);
        buffer[6*i + 2] = Vec3f(0.f, 1.f, i);
        buffer[6*i + 3] = Vec3f(1.f, 1.f, i);
        buffer[6*i + 4] = Vec3f(0.f, 1.f, i);
        buffer[6*i + 5] = Vec3f(1.f, 0.f, i);
    }

    glBindBuffer(GL_ARRAY_BUFFER, NodesBuffer);
    glBufferData(GL_ARRAY_BUFFER, MaxNodes * 6 * sizeof(Vec3f), buffer, GL_STATIC_DRAW);
    delete [] buffer;
}

void Renderer::SetMarksBuffer()
{
    const unsigned size = DataSource->GetDimensions().size();

    /* VBO */
    Vec3f * buffer = new Vec3f[size * 2];

    for (unsigned i=0; i < size; i++)
    {
        const bool isGap = DataSource->IsGap(i);
        // une ligne verticale par dimension
        buffer[2*i]     = Vec3f(i, -1, isGap);
        buffer[(2*i)+1] = Vec3f(i,  1, isGap);
    }

    glBindBuffer(GL_ARRAY_BUFFER, MarksBuffer);
    glBufferData(GL_ARRAY_BUFFER, size * 2 * sizeof(Vec3f), buffer, GL_STATIC_DRAW);
    delete [] buffer;
}

typedef Vector<float,  5, double> Vec5f;
Vec5f vec5(float a, float b, float c, float d, float e) {
    Vec5f v;
    v[0]=a; v[1]=b; v[2]=c; v[3]=d; v[4]=e;
    return v;
}

void Renderer::UpdateArrowsBuffer()
{
    const unsigned nbDimensions = DataSource->GetDimensions().size();

    /* VBO */
    Vec5f * buffer = new Vec5f[nbDimensions*3];

    for (unsigned i=0; i < nbDimensions; i++)
    {
        const bool isGap = DataSource->IsGap(i);

        const float y = (1-Invert[i])*2 - 1; // [-1 ; 1]

        buffer[(3*i)]   = vec5(i, y, isGap,  -1, 0 );
        buffer[(3*i)+1] = vec5(i, y, isGap,   0, y );

        //buffer[(4*i)+2] = vec5(i, y, cs[i].isGap,   0, y );
        buffer[(3*i)+2] = vec5(i, y, isGap,   1, 0 );
    }

    glBindBuffer(GL_ARRAY_BUFFER, ArrowsBuffer);
    glBufferData(GL_ARRAY_BUFFER, nbDimensions * 3 * sizeof(Vec5f), buffer, GL_STATIC_DRAW);
}

float Renderer::getT(unsigned i, unsigned curveDefinition, unsigned endsDefinition)
{
    const float d = 0.1;
   if (i%2 == 1) i-=1;
    if (i <= 1)
        return 0;
    else if (i >= (curveDefinition + 2*endsDefinition)-2)
        return 1;
    else if (i < endsDefinition)
        return d * (i /(float) endsDefinition);
    else if (i > endsDefinition + curveDefinition)
        return 1 - d + d * ((i - endsDefinition - curveDefinition) /(float) endsDefinition);
    else{

        return d + ((i - endsDefinition) /(float) curveDefinition) * (1 - 2*d);
    }
}

void Renderer::SetEdgesBuffer(unsigned curveDefinition, unsigned endsDefinition)
{
    if ((curveDefinition + 2*endsDefinition) % 2 == 1)
        curveDefinition += 1; // le nombre de sommets sera pair (plus simple)

    EdgeDefinition = curveDefinition + 2*endsDefinition;

    /* VBO */
    Vec3f * vertexBuffer = new Vec3f[EdgeDefinition];

    for (unsigned i=0; i <  EdgeDefinition; i++)
        vertexBuffer[i] = Vec3f(getT(i%EdgeDefinition, curveDefinition, endsDefinition), ( (float)((i%2)+1)*2 ) - 3, i/EdgeDefinition);

    glBindBuffer(GL_ARRAY_BUFFER, EdgesBuffer);
    glBufferData(GL_ARRAY_BUFFER, EdgeDefinition * sizeof(Vec3f), vertexBuffer, GL_STATIC_DRAW);
    delete [] vertexBuffer;


}

void Renderer::SetFrequenciesBuffer(unsigned definition)
{
    definition = (definition-2)*2 + 2;
    FrequencyDefinition = definition;

    /* VBO */
    Vec2f * vertexBuffer = new Vec2f[MaxNodes * definition];

    for (unsigned n=0; n<MaxNodes; n++)
    {
        vertexBuffer[n*definition] = Vec2f(0, n);

        for (unsigned i=1; i<definition-1; i+=2) {
            const Vec2f vertex = Vec2f( i /(float) (definition-1), n);
            vertexBuffer[n*definition + i]     = vertex;
            vertexBuffer[n*definition + i + 1] = vertex;
        }

        vertexBuffer[n*definition + (definition-1)] = Vec2f(1, n);
    }

    glBindBuffer(GL_ARRAY_BUFFER, FrequenciesBuffer);
    glBufferData(GL_ARRAY_BUFFER, MaxNodes * definition * sizeof(Vec2f), vertexBuffer, GL_STATIC_DRAW);
    delete [] vertexBuffer;
}

void Renderer::Draw(bool colorPicking)
{
    //colorPicking = true;
    if (DataSource == nullptr) {
        glClearColor(0.5, 0.5, 0.5, 1);
        glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
        return;
    }


    glClearColor(BackgroundColor.x(), BackgroundColor.y(), BackgroundColor.z(), BackgroundColor.w());
    glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    //glEnable(GL_DEPTH);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL); // en attendant

    glEnable(GL_BLEND);
    //    glDisable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnableVertexAttribArray(0);
    if (colorPicking){
#ifndef EMSCRIPTEN
      glDisable(GL_MULTISAMPLE);
#endif
      ;
    }
    else
    {
#ifndef EMSCRIPTEN
      glEnable(GL_MULTISAMPLE);
#endif
        if(DisplayMarksAndArrows){//!AnimatingDeletion && (!AnimatingInsertion && !InsertingDimension)){
            DrawMarks();
            DrawArrows();
        }
    }

    const bool behind = ((FocusedSubGraph.isValid() or FocusedEdge.isValid()) and not colorPicking);
    if (DataMovingSource!=nullptr){
        DrawNodes(DataMovingSource, nullptr, behind, colorPicking, 10.f);
    }

    DrawNodes(DataSource, nullptr, behind, colorPicking);
    if (!hideEdges)DrawEdges(DataSource->GetRootGraph().edges(), nullptr, behind, colorPicking);

    if (not colorPicking)
    {
        if (FocusedSubGraph.isValid())
        {
            const Data::subGraph & sub = DataSource->GetSubGraph(FocusedSubGraph);
            DrawNodes( sub.nodes, &(sub.nodesWeight), false, false );
            if (!hideEdges)DrawEdges( sub.edges, &(sub.edgesWeight), false, false );
        }
        else if (FocusedEdge.isValid())
        {
            const Data::subGraph & sub = DataSource->GetSubGraph(FocusedEdge);
            DrawNodes( sub.nodes, &(sub.nodesWeight), false, false );
            if (!hideEdges)
                DrawEdges( sub.edges, &(sub.edgesWeight), false, false );
        }

        if (not EnableHistogram)
            DrawFrequencies(DataSource->GetRootGraph().nodes());

        if (DisplayLabels)
        {
#ifndef EMSCRIPTEN
            glDisable(GL_MULTISAMPLE);
#endif
//            glDisable(GL_DEPTH);
            glDisable(GL_DEPTH_TEST);

            //            LabelRenderer.draw();
            AxeLabelRenderer.draw();
        }
    }

    glDisable(GL_BLEND);
    glDisableVertexAttribArray(0);
}


void Renderer::DrawNodes(const std::vector<node> & nodes, const std::vector<unsigned long long> * weightOverride, bool behind, bool colorPicking, float forceVisibility)
{
    GlMat4f mat   = Matrix.getMatProjMod();

    glUseProgram(NodeShader.program);
    glBindBuffer(GL_ARRAY_BUFFER, NodesBuffer);

    /* Attributes mapping */
    const int loc = glGetAttribLocation(NodeShader.program, "Vertex");
    glEnableVertexAttribArray( loc );
    glVertexAttribPointer(loc,3,GL_FLOAT,GL_FALSE, 0,NULL);

    /* Uniforms */
    glUniformMatrix4fv(NodeShader.MatProjMod, 1, GL_FALSE, &mat[0][0]);
    glUniform2f(NodeShader.Res, Matrix.getWindowWidth(), Matrix.getWindowHeight());

    glUniform1f(NodeShader.Height, Height);
    glUniform1f(NodeShader.Width, Width);
    glUniform1f(NodeShader.NodeWidth, NodeWidth);
    glUniform1f(NodeShader.Gap, Gap);
    glUniform1i(NodeShader.EnableHistogram, EnableHistogram);

    glUniform1i(NodeShader.ColorPicking, colorPicking);
    glUniform1i(NodeShader.Behind, behind);
    glUniform1f(NodeShader.ForceVisibilty, forceVisibility);
    /* End Uniforms */

    nodeView * nodesBuffer   = (nodeView*) VertexUniformBuffer;
    distribView    * distribBuffer = (distribView*)    FragmentUniformBuffer;

    unsigned nodesBufferSize   = 0;
    unsigned distribBufferSize = 0;

    unsigned renderedNodes = 0;

    const Vec4f screen = screenRect(mat.inverse());

    for (unsigned i=0; i < nodes.size(); i++)
    {
        nodeView nv = NodeView( nodes[i],DataSource, colorPicking );

        const Vec4f r = NodeRect(nv);
        const Vec4f nodeRect = Vec4f ( r.x(), r.y()
                                       ,r.x()+r.z(), r.y()+r.w());

        if ( rectIntersects(screen, nodeRect) )
        {
            const std::vector<float> & distrib = DataSource->GetNodeProp(nodes[i]).distrib;

            if(nodesBufferSize+1 > MaxNodes)// or distribBufferSize+distrib.size() > MaxFragmentUniform)
            {
                glUniform4fv(NodeShader.Data,        nodesBufferSize*SIZEOF_NODE, (float*)VertexUniformBuffer);
                glUniform4fv(NodeShader.DData, distribBufferSize,           (float*)FragmentUniformBuffer);
                glDrawArrays(GL_TRIANGLES, 0, 6 * nodesBufferSize);

                nodesBufferSize   = 0;
                distribBufferSize = 0;
            }

            if (weightOverride != nullptr)
                nv.subWeight = (*weightOverride)[i] /(float) DataSource->GetNbIndividuals();

            nv.distribFrom = distribBufferSize;
            nv.distribTo   = distribBufferSize + distrib.size();

            nodesBuffer[nodesBufferSize] = nv;

            for(unsigned i=0; i< 10 || i< distrib.size(); i++){
                if(i < distrib.size())
                    distribBuffer[nodesBufferSize].distrib[i] = distrib[i];
                else distribBuffer[nodesBufferSize].distrib[i] = 0;
            }

            distribBuffer[nodesBufferSize].nbElement = float(distrib.size());
            distribBuffer[nodesBufferSize].isCool = distribBuffer[nodesBufferSize].isCool2 =
                    distribBuffer[nodesBufferSize].isCool3 = distribBuffer[nodesBufferSize].isCool4 =
                    distribBuffer[nodesBufferSize].isCool5 = 1.;


            nodesBufferSize++;
            distribBufferSize += distrib.size();

            renderedNodes++;
        }
    }

    if(nodesBufferSize > 0)
    {
        glUniform4fv(NodeShader.Data,        nodesBufferSize*SIZEOF_NODE, (float*)VertexUniformBuffer);
        glUniform4fv(NodeShader.DData, distribBufferSize,           (float*)FragmentUniformBuffer);
        glDrawArrays(GL_TRIANGLES, 0, 6 * nodesBufferSize);
    }
}

void Renderer::DrawNodes(Data *data, const std::vector<unsigned long long> * weightOverride, bool behind, bool colorPicking, float forceVisibility)
{
    std::vector<node> nodes = data->GetRootGraph ().nodes ();
    GlMat4f mat   = Matrix.getMatProjMod();

    glUseProgram(NodeShader.program);
    glBindBuffer(GL_ARRAY_BUFFER, NodesBuffer);

    /* Attributes mapping */
    const int loc = glGetAttribLocation(NodeShader.program, "Vertex");
    glEnableVertexAttribArray( loc );
    glVertexAttribPointer(loc,3,GL_FLOAT,GL_FALSE, 0,NULL);

    /* Uniforms */
    glUniformMatrix4fv(NodeShader.MatProjMod, 1, GL_FALSE, &mat[0][0]);
    glUniform2f(NodeShader.Res, Matrix.getWindowWidth(), Matrix.getWindowHeight());

    glUniform1f(NodeShader.Height, Height);
    glUniform1f(NodeShader.Width, Width);
    glUniform1f(NodeShader.NodeWidth, NodeWidth);
    glUniform1f(NodeShader.Gap, Gap);
    glUniform1i(NodeShader.EnableHistogram, EnableHistogram);

    glUniform1i(NodeShader.ColorPicking, colorPicking);
    glUniform1i(NodeShader.Behind, behind);
    glUniform1f(NodeShader.ForceVisibilty, forceVisibility);
    /* End Uniforms */

    nodeView * nodesBuffer   = (nodeView*) VertexUniformBuffer;
    distribView    * distribBuffer = (distribView*)    FragmentUniformBuffer;

    unsigned nodesBufferSize   = 0;
    unsigned distribBufferSize = 0;

    unsigned renderedNodes = 0;

    const Vec4f screen = screenRect(mat.inverse());

    for (unsigned i=0; i < nodes.size(); i++)
    {
        nodeView nv = NodeView( nodes[i], data, colorPicking );
        const Vec4f r = NodeRect(nv);
        const Vec4f nodeRect = Vec4f ( r.x(), r.y()
                                       ,r.x()+r.z(), r.y()+r.w());

        if ( rectIntersects(screen, nodeRect) )
        {
            const std::vector<float> & distrib = data->GetNodeProp(nodes[i]).distrib;

            if(nodesBufferSize+1 > MaxNodes)
            {
                glUniform4fv(NodeShader.Data,        nodesBufferSize*SIZEOF_NODE, (float*)VertexUniformBuffer);
                glUniform4fv(NodeShader.DData, nodesBufferSize*SIZEOF_NODE,           (float*)FragmentUniformBuffer);
                glDrawArrays(GL_TRIANGLES, 0, 6 * nodesBufferSize);

                nodesBufferSize   = 0;
                distribBufferSize = 0;
            }

            if (weightOverride != nullptr)
                nv.subWeight = (*weightOverride)[i] /(float) data->GetNbIndividuals();

            nv.distribFrom = distribBufferSize;
            nv.distribTo   = distribBufferSize + distrib.size();

            nodesBuffer[nodesBufferSize] = nv;

            for(unsigned i=0; i< 10 || i< distrib.size(); i++)
            {
                if(i < distrib.size())
                    distribBuffer[nodesBufferSize].distrib[i] = distrib[i];
                else distribBuffer[nodesBufferSize].distrib[i] = 0;
             }

            distribBuffer[nodesBufferSize].nbElement = float(distrib.size());
            distribBuffer[nodesBufferSize].isCool = distribBuffer[nodesBufferSize].isCool2 =
                    distribBuffer[nodesBufferSize].isCool3 = distribBuffer[nodesBufferSize].isCool4 =
                    distribBuffer[nodesBufferSize].isCool5 = 1.;

            nodesBufferSize++;
//            distribBufferSize += distrib.size();

            renderedNodes++;
        }
    }

    if(nodesBufferSize > 0)
    {
        glUniform4fv(NodeShader.Data,        nodesBufferSize*SIZEOF_NODE, (float*)VertexUniformBuffer);
        glUniform4fv(NodeShader.DData, nodesBufferSize*SIZEOF_NODE,           (float*)FragmentUniformBuffer);
        glDrawArrays(GL_TRIANGLES, 0, 6 * nodesBufferSize);
    }
}

void Renderer::DrawEdges(const std::vector<edge> & edges, const std::vector<unsigned long long> * weightOverride, bool behind, bool colorPicking)
{
    //return;

    GlMat4f mat = Matrix.getMatProjMod();

    glUseProgram(EdgeShader.program);

    glBindBuffer(GL_ARRAY_BUFFER,         EdgesBuffer);
//    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EdgesElements);

     /* Attributes mapping */
    const int loc = glGetAttribLocation(EdgeShader.program, "Vertex");
    glEnableVertexAttribArray( loc );
    glVertexAttribPointer(loc,3,GL_FLOAT,GL_FALSE,0,NULL);

    /* Uniforms */
    glUniformMatrix4fv(EdgeShader.MatProjMod, 1, GL_FALSE, &mat[0][0]);
    glUniform2f(EdgeShader.Res, Matrix.getWindowWidth(), Matrix.getWindowHeight());
    glUniform1f(EdgeShader.Height, Height);
    glUniform1f(EdgeShader.Width, Width);
    glUniform1f(EdgeShader.NodeWidth, NodeWidth);
    glUniform1f(EdgeShader.Gap, Gap);
    glUniform1f(EdgeShader.Curvature, Curvature);
    glUniform1f(EdgeShader.EdgeRed, EdgeRed);
    glUniform1i(EdgeShader.ColorPicking, colorPicking);
    glUniform1i(EdgeShader.Behind, behind);

    if (colorPicking)
        glUniform1f(EdgeShader.Tubular, 0.);
    else
        glUniform1f(EdgeShader.Tubular, 0.6);
    /* End Uniforms */

    const Vec4f screen = screenRect(mat.inverse());
    std::vector<edgeNodeNode> visibleEdges(edges.size());
    for (unsigned i = 0; i < edges.size(); i++)
    {
        edgeNodeNode enn = EdgeNodeNode(edges[i]);

        const Vec4f r1 = NodeRect(enn.end1);
        const Vec4f r2 = NodeRect(enn.end2);

        const float botLeftY  = std::min( r1.y(), r2.y() );
        const float topRightY = std::max( r1.y()+r1.w(), r2.y()+r2.w() );

        const Vec4f edgeRect = Vec4f ( r1.x()+r1.z(), botLeftY,
                                       r2.x()       , topRightY );

        if (weightOverride != nullptr){
            enn.edge.subWeight = (*weightOverride)[i] /(float) DataSource->GetNbIndividuals();
        }
        if ( rectIntersects(screen, edgeRect) )
            // TODO: ne pas calculer le rect de l'arrete !
            // utiliser celui engloband les 2 noeuds aux extrémités c'est largement suffisant
        {
            visibleEdges.push_back(enn);
        }
    }
    visibleEdges.shrink_to_fit();
    std::sort(visibleEdges.begin(),visibleEdges.end(),[](edgeNodeNode e1,edgeNodeNode e2){return e1.edge.z<e2.edge.z;});

    edgeNodeNode * buffer = (edgeNodeNode*) VertexUniformBuffer; // for each edgeView we pass 2 nodeView
    unsigned bufferSize = 0;
    unsigned renderedEdges = 0;

    for (edgeNodeNode & enn:visibleEdges){
        glUniform4fv(EdgeShader.Data, (SIZEOF_EDGE + 2*SIZEOF_NODE), (float*) &enn); // envoyer qu'une arête
        glDrawArrays(GL_TRIANGLE_STRIP,0, EdgeDefinition); // envoyé nombre de points = EdgeDefinition
        bufferSize = 0;

    }
}


void Renderer::DrawMarks()
{
    GlMat4f mat = Matrix.getMatProjMod();
    glUseProgram(MarkShader.program);
    glBindBuffer(GL_ARRAY_BUFFER, MarksBuffer);

    /* Attributes mapping */
    const int loc = glGetAttribLocation(MarkShader.program, "Vertex");
    glEnableVertexAttribArray( loc );
    glVertexAttribPointer(loc,3,GL_FLOAT,GL_FALSE,0,NULL);

    /* Uniforms */
    glUniformMatrix4fv(MarkShader.MatProjMod, 1, GL_FALSE, &mat[0][0]);
    glUniform2f(MarkShader.Res, Matrix.getWindowWidth(), Matrix.getWindowHeight());

    glUniform1f(MarkShader.Height, Height);
    glUniform1f(MarkShader.Width, Width);
    glUniform1f(MarkShader.NodeWidth, NodeWidth);
    glUniform1f(MarkShader.Gap      , Gap);

    //glLineWidth(1);
    glDrawArrays(GL_LINES, 0, DataSource->GetDimensions().size() * 2);
}

void Renderer::DrawArrows()
{
    GlMat4f mat = Matrix.getMatProjMod();
    glUseProgram(ArrowShader.program);
    glBindBuffer(GL_ARRAY_BUFFER, ArrowsBuffer);

    /* Attributes mapping */
    const int loc1 = glGetAttribLocation(ArrowShader.program, "Arrow");
    glEnableVertexAttribArray( loc1 );
    glVertexAttribPointer(loc1,3,GL_FLOAT,GL_FALSE, sizeof(GL_FLOAT)*5, (void*)0);

    const int loc2 = glGetAttribLocation(ArrowShader.program, "Vertex");
    glEnableVertexAttribArray( loc2 );
    glVertexAttribPointer(loc2,2,GL_FLOAT,GL_FALSE, sizeof(float)*5, (void*) (sizeof(float)*3));

    /* Uniforms */
    glUniformMatrix4fv(ArrowShader.MatProjMod, 1, GL_FALSE, &mat[0][0]);
    glUniform2f(ArrowShader.Res, Matrix.getWindowWidth(), Matrix.getWindowHeight());

    glUniform1f(ArrowShader.Height, Height);
    glUniform1f(ArrowShader.Width, Width);
    glUniform1f(ArrowShader.NodeWidth, NodeWidth);
    glUniform1f(ArrowShader.Gap      , Gap);

    //glLineWidth(1);
    glDrawArrays(GL_TRIANGLES, 0, DataSource->GetDimensions().size() * 3);
}

void Renderer::DrawFrequencies(const std::vector<node> & nodes)
{
    GlMat4f mat   = Matrix.getMatProjMod();

    glUseProgram(FrequencyShader.program);
    glBindBuffer(GL_ARRAY_BUFFER, FrequenciesBuffer);

    /* Attributes mapping */
    const int loc = glGetAttribLocation(FrequencyShader.program, "Vertex");
    glEnableVertexAttribArray( loc );
    glVertexAttribPointer(loc, 2, GL_FLOAT, GL_FALSE, 0, NULL);

    /* Uniforms */
    glUniformMatrix4fv(FrequencyShader.MatProjMod, 1, GL_FALSE, &mat[0][0]);

    glUniform1f(FrequencyShader.Height, Height);
    glUniform1f(FrequencyShader.Width, Width);
    glUniform1f(FrequencyShader.NodeWidth, NodeWidth);
    glUniform1f(FrequencyShader.Gap, Gap);
    /* End Uniforms */

    unsigned renderedNodes = 0;
    nodeView * buffer = (nodeView*) VertexUniformBuffer;
    unsigned bufferSize = 0;

    const Vec4f screen = screenRect(mat.inverse());

    for (unsigned i=0; i < nodes.size(); i++)
    {
        const nodeView nv = NodeView( nodes[i], DataSource );

        const Vec4f r = NodeRect(nv);
        const Vec4f nodeRect = Vec4f ( r.x(), r.y()
                                       ,r.x()+r.z(), r.y()+r.w());

        if ( rectIntersects(screen, nodeRect) )
        {
            buffer[bufferSize] = nv;
            //std::cerr << "distortion=" << nv.distortion << std::endl;

            bufferSize++;
            renderedNodes++;

            if(bufferSize == MaxNodes)
            {
                glUniform4fv(FrequencyShader.Data, bufferSize * SIZEOF_NODE, (float*)buffer);
                glDrawArrays(GL_LINES, 0, bufferSize * FrequencyDefinition);
                bufferSize = 0;
            }
        }
    }

    if(bufferSize > 0)
    {
        glUniform4fv(FrequencyShader.Data, bufferSize * SIZEOF_NODE, (float*)buffer);
        glDrawArrays(GL_LINES, 0, bufferSize * FrequencyDefinition);
        bufferSize = 0;
    }
}

bool Renderer::Tick(float time)
{
    bool ret = false;
    if (DataSource == nullptr) return false;

    if(MappingAnimationTime<1){
        MappingAnimationTime = easeInOut( (time - StartAnimationTime) / (InvertDuration) );
    }
    else if (InvertAxis >= 0)
    {
        const float nd = easeInOut( (time - InvertSince) / (InvertDuration) );
        const float d  = (InvertInvert) ? 1-nd : nd;

        Invert[InvertAxis] = d;

        UpdateArrowsBuffer();
        UpdateLabels ();
        ShowLabels ();

        if (nd == 1)
        {
            UpdateLabels();
            InvertAxis = -1;
        }

        ret = true;
    }
    else if(DeletedDimension != -1 && DataTmpSource->GetDimensions().size() != 0){
        if(DeletingDimension){
            RemoveDimensionStep = 1.- easeInOut( (time - StartAnimationTime) / (DeleteDuration) );
            if(RemoveDimensionStep <= 0.){
                StartAnimationTime = time;
                DataSource->DeleteDimension(DataTmpSource, DeletedDimension);
                for(unsigned int i = DeletedDimension; i+1 < Invert.size(); ++i){
                    Invert[i] = Invert[i+1];
                }
                assert(Invert.size() != 0);
                Invert.resize(Invert.size()-1);
                DeletingDimension = false;
                UpdateArrowsBuffer();
            }
        }
        else {
            RemoveDimensionStep = easeInOut( (time - StartAnimationTime) / (DeleteDuration) );
            if(DeletionDisplacementAnimation == 0.f){

                for(unsigned int i = 0; i < DataSource->GetDimensionProp().size();++i){
                    DataSource->GetDimensionProp (i).pos = i;
                    for(auto n : DataSource->GetDimensions (i)){
                        DataSource->GetNodeProp (n.second).dimension =i;
                    }
                }
                DeletedDimension = -1;
                AnimatingDeletion = false;
                RemoveDimensionStep =-1.;
                // delete DataTmpSource; TODO trouver pourquoi segfault
                DataTmpSource = nullptr;
                UpdateArrowsBuffer();
                ret = true;
            }
            else {
                if(RemoveDimensionStep >= 1.){

                    for(unsigned int i = 0; i < DataSource->GetDimensionProp().size();++i){
                        DataSource->GetDimensionProp (i).pos = i;
                        for(auto n : DataSource->GetDimensions (i)){
                            DataSource->GetNodeProp (n.second).dimension =i;
                        }
                    }

                    DeletedDimension = -1;
                    AnimatingDeletion = false;
                    RemoveDimensionStep =-1.;
                    // delete DataTmpSource; TODO trouver pourquoi segfault
                    DataTmpSource = nullptr;
                    UpdateArrowsBuffer();
                    ret = true;

                }
                else {
                    for (unsigned int i=DeletedDimension;i<DataSource->GetDimensions ().size ();++i){
                        assert(DataSource->GetDimensions ()[i].size()>0);
                        float initPos = DataSource->GetNodeProp (DataSource->GetDimensions ()[i][0]).dimension -
                                OldRemoveDimensionStep*DeletionDisplacementAnimation;
                        float pos = initPos + RemoveDimensionStep*DeletionDisplacementAnimation;
                        for(auto n : DataSource->GetDimensions (i)){
                            DataSource->GetNodeProp (n.second).dimension =pos;
                        }
                        DataSource->GetDimensionProp (i).pos = pos;
                    }
                    OldRemoveDimensionStep = RemoveDimensionStep;

                }
            }
        }
    }
    else if(IsAnimatingInsertion()  && DataTmpSource->GetDimensions().size() != 0){

        // suppression progressive des arêtes
        if(InsertionAnimationStage  == 2){

            InsertDimensionStep = 1.- easeInOut( (time - StartAnimationTime) / (InsertDuration) );
            // changement de couleur des aretes
            if(InsertDimensionStep <= 0.){
                StartAnimationTime = time;
                InsertionAnimationStage = 3;
                OldInsertDimensionStep = 0.;
                DataSource->InsertDimension (DataTmpSource, insertedDBDim, PrevDim, NextDim );
                Invert.resize(Invert.size()+1);
                for(unsigned int i = Invert.size()-1; i > NextDim; --i){
                    Invert[i] = Invert[i-1];
                }
                Invert[NextDim]=0.;
            }
        }
        // decale vers la droite
        else if(InsertionAnimationStage  == 1){
           // HideLabels ();
            InsertDimensionStep = easeInOut( (time - StartAnimationTime) / (DeleteDuration) );
            // pas de deplacement on insere
            if(InsertionDisplacementAnimation == 0.f){
                OldInsertDimensionStep = 0.;
                InsertionAnimationStage = 2;
                StartAnimationTime = time;
                InsertDimensionStep =1.;
//                DataSource->InsertDimension (DataTmpSource, insertedDBDim, PrevDim, NextDim );
//                Invert.resize(Invert.size()+1);
//                for(unsigned int i = Invert.size()-1; i > NextDim; --i){
//                    Invert[i] = Invert[i-1];
//                }
//                Invert[NextDim]=0.;
            }
            else {

                // decalage termine, on insere
                if(InsertDimensionStep >= 1.){

                    OldInsertDimensionStep = 0.;
                    InsertionAnimationStage = 2;
                    StartAnimationTime = time;
                    InsertDimensionStep =1.;
                    UpdateArrowsBuffer();
                }
                else {
                    for (unsigned int i=NextDim;i < DataSource->GetDimensions ().size ();++i){
                        assert(DataSource->GetDimensions ()[i].size()>0);
                        float initPos = DataSource->GetNodeProp (DataSource->GetDimensions ()[i][0]).dimension -
                                OldInsertDimensionStep*InsertionDisplacementAnimation;
                        float pos = initPos + InsertDimensionStep*InsertionDisplacementAnimation;
                        for(auto n : DataSource->GetDimensions (i)){
                            DataSource->GetNodeProp (n.second).dimension =pos;
                        }
                        DataSource->GetDimensionProp (i).pos = pos;
                    }
                    OldInsertDimensionStep = InsertDimensionStep;
                }
            }
        }
        // insertion progressive des sommets et arêtes
        else if(InsertionAnimationStage  == 3){
            InsertDimensionStep = easeInOut( (time - StartAnimationTime) / (DeleteDuration) );

            if(InsertDimensionStep >=1){
                for(unsigned int i = 0; i < DataSource->GetDimensionProp().size();++i){
                    DataSource->GetDimensionProp (i).pos = i;
                    for(auto n : DataSource->GetDimensions (i)){
                        DataSource->GetNodeProp (n.second).dimension =i;
                    }
                }
                InsertionAnimationStage = 0;
                insertedDBDim = -1;
                PrevDim = -1;
                NextDim = -1;
                AnimatingInsertion = false;
                InsertingDimension = false;
                InsertDimensionStep =-1.;
                // delete DataTmpSource; TODO trouver pourquoi segfault
                DataTmpSource = nullptr;
                ret = true;
                UpdateArrowsBuffer();
//                ShowLabels();
//                UpdateLabels ();
            }
        }
    }
    else
        ret = false;
    return ret;
}


Vec3f Renderer::idColor(const unsigned id)
{
    const unsigned b = id%256;
    const unsigned g = (id / 256) % 256;
    const unsigned r = (id / (256*256)) % 256;

    return Vec3f(r/255., g/255., b/255.);
}

// identique à la fonction nodeRect des shaders
Vec4f Renderer::NodeRect(const nodeView & n) const // bottom left corner (x, y, width, height)
{
    Vec4f r = Vec4f( n.x * (NodeWidth + Width),
                               n.weightBefore * (Height - Gap*Height) + n.gapBefore * (Gap*Height),
                               NodeWidth,
                               n.weight * (Height - Gap*Height)
                               );
    const float h = (n.isGap == 1) ? Height : Height - (Gap*Height);

    const float invY = h - r.y() - r.w();
    const float dy   = invY - r.y();

    r.setY( r.y() + n.invert*dy );

    if (n.isGap == 0)
        r.setY( r.y() +  (Height*Gap) / 2. );

    return r;
}



bool Renderer::getDisplayLabels() const
{
    return DisplayLabels;
}



Renderer::nodeView Renderer::NodeView(node n, Data * dataSource, bool colorPicking)
{
    const Data::nodeProp & np = dataSource->GetNodeProp(n);
    nodeView nv;

    // For animation between interval and distribution mode
    Data::dimensionProp &dimP = dataSource->GetDimensionProp(np.dimension);
     if (DataMovingSource!= nullptr && DataMovingSource == dataSource)
         dimP = dataSource->GetDimensionProp(0);
    float intervalSize = (dimP.max-dimP.min);
    float distributionWeight = np.weight /(float) dataSource->GetNbIndividuals();
    float intervalWeight = MIN_NODE_SIZE;
    float intervalMin = 0.;
    if(intervalSize > 0.){
        intervalWeight = std::max((np.max - np.min )/intervalSize, MIN_NODE_SIZE);
        intervalMin = (np.min - dimP.min) / intervalSize;
    }
    if(IntervalAsSize){
        nv.weight = intervalWeight  + (1. - MappingAnimationTime) * (distributionWeight - intervalWeight);
        nv.weightBefore = intervalMin + (1. - MappingAnimationTime) * (np.weightBefore - intervalMin);
        nv.gapBefore    = (1. - MappingAnimationTime) * np.gapBefore;
        nv.distortion   = (1. - MappingAnimationTime) * np.distortion;
    }
    else{
        nv.weight = distributionWeight + (1. - MappingAnimationTime) * (intervalWeight - distributionWeight);
        nv.weightBefore = np.weightBefore + (1. - MappingAnimationTime) * (intervalMin - np.weightBefore);
        nv.gapBefore    = MappingAnimationTime * np.gapBefore;
        nv.distortion   = np.distortion * MappingAnimationTime;
    }


    nv.subWeight    = -1;
    if (DataMovingSource!= nullptr && DataMovingSource == dataSource)
        nv.x = dimP.pos;
    else
        nv.x            = np.dimension;
    nv.isGap        = dataSource->IsGap( np.dimension );
    nv.invert       = Invert[np.dimension];
    nv.gapRatio     = dimP.gapRatio;
    nv.color     = to_gl( ColorGradient.getColorAtPos(np.weightBefore) );

    if (colorPicking)
        nv.color     = Vec4f( idColor(n), 1. );
    else {
        if(AnimatingDeletion && np.dimension == DeletedDimension)
            nv.color[3]=RemoveDimensionStep;
        if(InsertingDimension && np.db_dimension == insertedDBDim )
            nv.color[3]=InsertDimensionStep;
    }
    return nv;
}

Renderer::edgeView Renderer::EdgeView(edge e)
{
    const Data::edgeProp & ep = DataSource->GetEdgeProp(e);

    edgeView ev;
    ev.subWeight = -1;
    ev.z         = ep.z; // !!
    ev.idColor   = idColor(DataSource->GetRootGraph().numberOfNodes() + e );

    // For animation beetween interval and distribution mode
    float distributionWeight = ep.weight /(float) DataSource->GetNbIndividuals();
    float intervalWeight = MIN_EDGE_WIDTH;
    if(IntervalAsSize){
        ev.weight = intervalWeight  + (1. - MappingAnimationTime) * (distributionWeight - intervalWeight);
        ev.sourcePos = ep.sourcePos * (1.- MappingAnimationTime) + MID_EDGE_POS * MappingAnimationTime;
        ev.destPos   = ep.destPos * (1.- MappingAnimationTime) + MID_EDGE_POS * MappingAnimationTime;;
    }
    else {
        ev.weight =  distributionWeight  + (1. - MappingAnimationTime) * (intervalWeight- distributionWeight);
        ev.sourcePos = MID_EDGE_POS * (1.- MappingAnimationTime) + ep.sourcePos* MappingAnimationTime;
        ev.destPos   = MID_EDGE_POS * (1.- MappingAnimationTime) + ep.destPos * MappingAnimationTime;;
    }
    return ev;
}

Renderer::edgeNodeNode Renderer::EdgeNodeNode(edge e)
{
    const std::pair<node,node> ends = DataSource->GetRootGraph().ends(e);

    edgeView ev = EdgeView(e);
    nodeView srcV = NodeView(ends.first, DataSource);
    nodeView tgtV = NodeView(ends.second, DataSource);

    const Data::nodeProp & np = DataSource->GetNodeProp(ends.first);
    if(InsertingDimension && (InsertionAnimationStage == 2) && np.dimension == PrevDim )
        srcV.color[3]=InsertDimensionStep;
    else if(InsertingDimension && (InsertionAnimationStage == 2)&& np.dimension == NextDim)
        tgtV.color[3]=InsertDimensionStep;
    if (AnimatingDeletion && np.dimension == DeletedDimension-1)
        srcV.color[3]=RemoveDimensionStep;

    return {ev, srcV, tgtV};
    //    return {
    //        EdgeView(e),
    //                NodeView(ends.first),
    //                NodeView(ends.second)
    //    };
}


void Renderer::InvertAt(float x, float y, float time)
{
    if (DataSource == nullptr) return;

    const Vec3f p = Matrix.screenToModel( Matrix.windowToScreen(Vec2f(x,y)) );
    x = p.x();
    y = p.y();

    if ( InvertAxis >= 0
         or x > GetTotalWidth()+GetTotalNodeWidth()+Width/2 or x < -Width/2
         or y < -Height/2 or y > Height+Height/2 )
        return;

    InvertAxis  = (x + Width/2) / (Width + NodeWidth);
    InvertAxis  = std::min(InvertAxis, (int) DataSource->GetDimensions().size()-1);
    InvertSince = time;
    /* on détermine dans quel sens inverser */
    if (Invert[ InvertAxis ] == 0)
        InvertInvert = false;
    else
        InvertInvert = true;
}

void Renderer::updateMovingDimPosition(int x, int y){

    const Vec3f p = Matrix.screenToModel( Matrix.windowToScreen(Vec2f(x,y)) );
    float sDim = p.x();
    y = p.y();

    sDim  = (sDim ) / (Width + NodeWidth);
    DataMovingSource->GetDimensionProp (0).pos = sDim;
}

void Renderer::UpdateLabels(){
    if (DataSource == nullptr) return;

    if (not DisplayLabels)
        return;

    AxeLabelRenderer.clear();

    const auto & dimensions = DataSource->GetDimensionProp ();

    const double pi = std::acos(-1);
    const float radian = pi/4;

    for (unsigned x = 0; x < dimensions.size(); x++)
    {
        AxeLabelRenderer.addLabel( (isblank(dimensions[x].label)) ? "?" : dimensions[x].label,
                                   dimensions[x].pos * (Width + NodeWidth) + NodeWidth/2,
                                   -0.7, 20);
        Label* lbl = AxeLabelRenderer.getLabel(x);
        float cos = (NodeWidth+Width/2)/std::cos(radian);
        float s = cos/2.;
        Vec2f sc = Vec2f(s,s);
        lbl->scaling =sc; // ne fait pas du tout ce que l'on veut....
        Rectf BdBx = lbl->getBoundingBox();
        float h = BdBx.height();
        float w = BdBx.width();
        lbl->nodePos[0] -= std::cos(radian)*(w/4.)-h/4.;
        lbl->nodePos[1] =  -(1.+std::sin(radian)*(w/4.));
        lbl->rotation = radian;
    }
    LabelsOcclusion ();
}

int readPixelId(int x, int y) {
    unsigned char pixel[4];
    glReadPixels(x, y, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, pixel);

    return pixel[0]*256*256 + pixel[1]*256 + pixel[2];
}

void Renderer::setDataMovingSource(Data * dt){
    DataMovingSource = dt;
}

int Renderer::getDimension(float x, float y){
    if (DataSource == nullptr) return -1;

    const Vec3f p = Matrix.screenToModel( Matrix.windowToScreen(Vec2f(x,y)) );
    x = p.x();
    y = p.y();
    int sDim = -1;
    if ( sDim>= 0
         or x > GetTotalWidth()+GetTotalNodeWidth()+Width/2 or x < -Width/2
         or y < -Height/2 or y > Height+Height/2 )
        return -1;

    sDim  = (x + Width/2) / (Width + NodeWidth);
    sDim  = std::min(sDim, (int) DataSource->GetDimensions().size()-1);
    return sDim;
}

tuple<int,int> Renderer::getSurroundingDimension(float x, float y){
    if (DataSource == nullptr) return make_tuple(-1,-1);

    const Vec3f p = Matrix.screenToModel( Matrix.windowToScreen(Vec2f(x,y)) );
    x = p.x();
    y = p.y();
    float sDim = -1;
    int prev=-1, next = -1;
    if ( y < -Height/2 or y > Height+Height/2 )
        return make_tuple(-1,-1);

    sDim  = (x) / (Width + NodeWidth);
    if (sDim<0) prev= -1;
    else
        prev = floor(std::min((int)sDim, (int) DataSource->GetDimensions().size()-1));
    next = prev+1;
    return make_tuple(prev,next);
}

int Renderer::getId(int x, int y)
{
    if (DataSource == nullptr) return false;

    y = Matrix.getWindowHeight() - y;

    Draw(true);
    glFlush();
    glFinish();
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    const unsigned nbNodes = DataSource->GetRootGraph().numberOfNodes();

    int id = PickId(x,y);

    if (id < 0 || id > nbNodes)
    {
        return -1;
    }
    else return id;
}

int Renderer::PickId(int x, int y)
{
    int maxId =
            DataSource->GetRootGraph().numberOfNodes() +
            DataSource->GetRootGraph().numberOfEdges() - 1;

    int dist = 0;
    while (true)
    {
        for (int px = std::max(0, x-dist); px < std::min(x+dist+1, Matrix.getWindowWidth()); px++)
        {
            int id = readPixelId(px,y-dist);
            if (id <= maxId)
                return id;

            id = readPixelId(px,y+dist);
            if (id <= maxId)
                return id;
        }

        for (int py = std::max(0, y-(dist-1)); py < std::min(y+(dist-1)+1, Matrix.getWindowHeight()); py++)
        {
            int id = readPixelId(x-dist,py);
            if (id <= maxId)
                return id;

            id = readPixelId(x+dist,py);
            if (id <= maxId)
                return id;
        }

        dist++;
        if (dist > 10)
            return -1;
    }
}

bool Renderer::Select(int x, int y, bool nodesOnly, bool pullUpEdge)
{
    if (DataSource == nullptr) return false;

    const Vec3f p = Matrix.screenToModel( Matrix.windowToScreen(Vec2f(x,y)) );
    if( (p.x() < -NodeWidth || p.x() > GetTotalWidth () + GetTotalNodeWidth () + NodeWidth) ||
            (p.y() < -0.7 || p.y() > Height * 1.1))
            return false;

    y = Matrix.getWindowHeight() - y;

    Draw(true);
    glFlush();
    glFinish();
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    const unsigned nbNodes = DataSource->GetRootGraph().numberOfNodes();
    //const unsigned nbEdges = DataSource->GetRootGraph().numberOfEdges();

    bool result = false;
    int id = PickId(x,y);
    node n(id);
    if (id >= 0 && DataSource->GetRootGraph().isElement (n) and not pullUpEdge) // node
    {
       DataSource->RequestSubGraph( node(id) );

        result = true;
    }
    else if (!DataSource->GetRootGraph().isElement (n) and not nodesOnly)  // edge
    {
        const edge e(id - nbNodes);

        if(!DataSource->GetRootGraph().isElement (e)) return false;

        if (pullUpEdge)
        {
            DataSource->SetEdgeZ(e, FrontZ);
            FrontZ += 0.0001;
        }
        else
        {
            DataSource->RequestSubGraph( e);
        }

        result = true;
    }
    Draw(false);
    return result;
}
void Renderer::EndSelect()
{
    FocusedSubGraph = node();
    FocusedEdge     = edge();
}

void Renderer::HideLabels() {
    DisplayLabels = false;
}

void Renderer::ShowLabels()
{
    if (not DisplayLabels)
    {
        DisplayLabels = true;
        UpdateLabels();
    }
}

void Renderer::HideMarksAndArrows(){
    DisplayMarksAndArrows = false;
}
void Renderer::ShowMarksAndArrows(){
    DisplayMarksAndArrows = !DisplayMarksAndArrows;
}

void Renderer::LabelsOcclusion() {
    //    LabelRenderer.occlusion();
    AxeLabelRenderer.occlusion();
}

void Renderer::Zoom(float x, float y, const float scale) {
    Matrix.zoom(scale, Vec2f(x,y));
    LabelsOcclusion();
}
void Renderer::Translate(float dx, float dy)
{
    const float d = WindowToModelFactor();
    Matrix.moveModel( Vec3f(d*dx,d*dy,0) );
}

void Renderer::Center() {
    if (DataSource == nullptr) return;

    Matrix.centerBox( Vec4f(0,0, DataSource->GetDimensions().size() * (NodeWidth + Width) - Width, Height) );
    LabelsOcclusion();
}

void Renderer::Fit() {
    if (DataSource == nullptr) return;

    Center();
    const float d = WindowToModelFactor();
    SetHeight(0.8 * d * Matrix.getWindowHeight());
    SetTotalWidth(d * Matrix.getWindowWidth() - GetTotalNodeWidth());
    Center();
}

void Renderer::ChangeViewport(const Vec4i x)
{
    Matrix.changeViewport(x);

    LabelsOcclusion();
}

const MatrixManager & Renderer::GetMatrixManager() {
    return Matrix;
}

void Renderer::SetHeight   (const float x) {
    Height = std::max(0.f, x);
    UpdateLabels();
}
void Renderer::SetWidth    (const float x) {
    Width = std::max(0.f, x);
    UpdateLabels();
}
void Renderer::SetNodeWidth(const float x) {
    NodeWidth = std::max(0.f, x);
    UpdateLabels();
}

void Renderer::SetEdgeRed(const float x) {
    EdgeRed = between(0.01f, x, 1.f);
    //    UpdateLabels();
}
void Renderer::SetTotalWidth    (const float x) {
    if (DataSource == nullptr) return;

    SetWidth( x / (DataSource->GetDimensions().size()-1) );
}
void Renderer::SetTotalNodeWidth(const float x) {
    if (DataSource == nullptr) return;

    SetNodeWidth( x / DataSource->GetDimensions().size() );
}
void Renderer::SetEdgeReduction(const float x) {
    if (DataSource == nullptr) return;

    SetEdgeRed( x );
}
void Renderer::SetGap      (const float x) {
    Gap = between(0.f, x, 1.f);
    //UpdateLabels();
}
void Renderer::SetCurvature(const float x) {
    Curvature = between(0.01f, x, 1.f);
}


void Renderer::InsertDimension(int srcX, int srcY, int tgtX, int tgtY){
    if (DataSource == nullptr) return;
    const Vec3f srcP = Matrix.screenToModel( Matrix.windowToScreen(Vec2f(srcX,srcY)) );
    const Vec3f tgtP = Matrix.screenToModel( Matrix.windowToScreen(Vec2f(tgtX,tgtY)) );

    float model_srcX = srcP.x();
    float model_srcY = srcP.y();
    float model_tgtX = tgtP.x();
    float model_tgtY = tgtP.y();

    if ( model_srcY < -Height/2 or model_srcY > Height+Height/2)
        //or model_tgtY < -Height/2 or model_tgtY > Height+Height/2)
        return;

    int srcAxis= (model_srcX + Width/2) / (Width + NodeWidth);
    srcAxis = std::min(srcAxis, (int) DataSource->GetDimensions().size()-1);

    bool before = false;
    int tgtAxis= model_tgtX / (Width);
    if(model_tgtX <= 0)// >= (int) DataSource->GetDimensions().size()-1 )
        before = true;
    tgtAxis = std::min(tgtAxis, (int) DataSource->GetDimensions().size()-1);

    //DataSource->InsertDimensions(srcAxis, tgtAxis, before);

}


float Renderer::WindowToModelFactor() const
{
    const GlMat4f mProjection = Matrix.getProjectionMatrix();
    const GlMat4f mModelView  = Matrix.getModelViewMatrix();
    const GlMat4f invMat      = mProjection * mModelView;
    const Vec4f v1 = invMat * Vec4f(1, 0., 0., 1.);
    const Vec4f v2 = invMat * Vec4f(0, 0,  0,  1);
    return (2./Matrix.getWindowWidth()) / Vec3f(v1).dist(Vec3f(v2));
}


Data * Renderer::GetDataTmpSource(){
    return DataTmpSource;
}

void Renderer::SetDeletionDataSource(Data * TmpSource, int Dim){
    if(DataSource == nullptr) return;
    EndSelect ();
    DataTmpSource = TmpSource;
    DeletedDimension = Dim;
}

void Renderer::SetDeletionDataSource(Data * TmpSource, int x, int y){
    int Sid = getId(x,y);
    if(Sid < 0 || Sid >= DataSource->GetRootGraph().numberOfNodes()) return;
    int Dim = DataSource->GetNodeProp()[Sid].dimension;
    SetDeletionDataSource (TmpSource, Dim);
}

void Renderer::SetInsertionDataSource(Data * TmpSource, int Dim_DB, int Prev_Client, int Next_Client){
    if(DataSource == nullptr) return;
    EndSelect ();
    DataTmpSource = TmpSource;

    insertedDBDim = Dim_DB;
    PrevDim = Prev_Client;
    NextDim = Next_Client;
}

bool Renderer::IsAnimatingDeletion(){
    return AnimatingDeletion;

}



void Renderer::AnimateDeletion(float Time){
    AnimatingDeletion = true;
    StartAnimationTime = Time;
    RemoveDimensionStep = 1.;
    OldRemoveDimensionStep = 0.;
    DeletingDimension = true;

    vector< map<int, node> > dimensions = DataSource->GetDimensions ();
//    assert(dimensions.size() < DeletedDimension);
    if(DeletedDimension == dimensions.size()-1)
        DeletionDisplacementAnimation = 0.f;
    else {
        DeletionDisplacementAnimation = -1.f;
//        map<int, node> DimToRemove = dimensions[DeletedDimension];
//        map<int, node> DimToMove = dimensions[DeletedDimension+1];
//        assert(DimToMove.size () != 0 && DimToMove.size()!=0);
//        float posF = DataSource->GetNodeProp (DimToRemove[0]).dimension;
//        float posI = DataSource->GetNodeProp (DimToMove[0]).dimension;
//        DeletionDisplacementAnimation = posF - posI;
    }
}

void  Renderer::AnimateInsertion(float Time){
    InsertionAnimationStage = 1;
    StartAnimationTime = Time;
    InsertDimensionStep = 1.;
    OldInsertDimensionStep = 0.;
    InsertingDimension = true;

    vector< map<int, node> > dimensions = DataSource->GetDimensions ();
    assert(dimensions.size() < DeletedDimension);
    if(NextDim == dimensions.size())
        InsertionDisplacementAnimation = 0.f;
    else
        InsertionDisplacementAnimation = 1.f;// Width;
}



Renderer::NodeProgram::NodeProgram(): ShaderProgram("../shaders/node_shader.glsl")
{
    MatProjMod   = glGetUniformLocation(program,"MatProjMod");
    Res          = glGetUniformLocation(program,"Res");
    Data         = glGetUniformLocation(program,"Data");
    DData  = glGetUniformLocation(program,"DData");

    Height    = glGetUniformLocation(program,"Height");
    Width     = glGetUniformLocation(program,"Width");
    NodeWidth = glGetUniformLocation(program,"NodeWidth");
    Gap       = glGetUniformLocation(program,"Gap");
    EnableHistogram = glGetUniformLocation(program,"EnableHistogram");

    ColorPicking = glGetUniformLocation(program,"ColorPicking");
    Behind       = glGetUniformLocation(program,"Behind");
}

Renderer::EdgeProgram::EdgeProgram(): ShaderProgram("../shaders/edge_shader.glsl")
{
    MatProjMod   = glGetUniformLocation(program,"MatProjMod");
    Res          = glGetUniformLocation(program,"Res");
    Data         = glGetUniformLocation(program,"Data");

    Height    = glGetUniformLocation(program,"Height");
    Width     = glGetUniformLocation(program,"Width");
    NodeWidth = glGetUniformLocation(program,"NodeWidth");
    Gap       = glGetUniformLocation(program,"Gap");

    Curvature = glGetUniformLocation(program,"Curvature");
    EdgeRed = glGetUniformLocation(program,"EdgeRed");

    ColorPicking = glGetUniformLocation(program,"ColorPicking");
    Behind       = glGetUniformLocation(program,"Behind");
    Tubular      = glGetUniformLocation(program,"Tubular");
}


Renderer::MarkProgram::MarkProgram(): ShaderProgram("../shaders/mark_shader.glsl")
{
    MatProjMod = glGetUniformLocation(program,"MatProjMod");
    Res        = glGetUniformLocation(program,"Res");

    Height    = glGetUniformLocation(program,"Height");
    Width     = glGetUniformLocation(program,"Width");
    NodeWidth = glGetUniformLocation(program,"NodeWidth");
    Gap       = glGetUniformLocation(program,"Gap");
}


Renderer::ArrowProgram::ArrowProgram(): ShaderProgram("../shaders/arrow_shader.glsl")
{
    MatProjMod = glGetUniformLocation(program,"MatProjMod");
    Res        = glGetUniformLocation(program,"Res");

    Height    = glGetUniformLocation(program,"Height");
    Width     = glGetUniformLocation(program,"Width");
    NodeWidth = glGetUniformLocation(program,"NodeWidth");
    Gap       = glGetUniformLocation(program,"Gap");
}

Renderer::FrequencyProgram::FrequencyProgram(): ShaderProgram("../shaders/frequency_shader.glsl")
{
    MatProjMod   = glGetUniformLocation(program,"MatProjMod");
    //Res          = glGetUniformLocation(program,"Res");
    Data         = glGetUniformLocation(program,"Data");

    Height    = glGetUniformLocation(program,"Height");
    Width     = glGetUniformLocation(program,"Width");
    NodeWidth = glGetUniformLocation(program,"NodeWidth");
    Gap       = glGetUniformLocation(program,"Gap");
}
