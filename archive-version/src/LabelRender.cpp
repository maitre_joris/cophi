#include <vector.h>
#include <fstream>
#include <sstream>
#include <queue>
#include <utility>

//#include <tulip/QuadTree.h>

#include <edtaa3func.h>
#include <utf8Iterator.h>
#include <LabelRender.h>

using namespace std;

////*****************************Intersectors**********************

//void ListIntersector::addBox(const Rectf box) {
//    boxes.push_back(box);
//}

//bool ListIntersector::intersects(const Rectf box) {
//    for (unsigned int i=0 ; i<boxes.size() ; ++i) {
//        if (box.intersect(boxes[i])) return true;
//    }
//    return false;
//}

//void ListIntersector::clear() {
//    boxes.clear();
//}

//QuadTreeIntersector::QuadTreeIntersector() {
//    qtree = new QuadTreeNode<int>(Rectf(-1,-1,1,1));
//}

//QuadTreeIntersector::~QuadTreeIntersector() {
//    delete qtree;
//}

//void QuadTreeIntersector::addBox(const Rectf box) {
//    qtree->insert(box, boxes.size());
//    boxes.push_back(box);
//}

//bool QuadTreeIntersector::intersects(const Rectf box) {
//    std::vector<int> res;
//    qtree->getElements(box, res);
//    for (unsigned int i=0 ; i<res.size() ; ++i)
//        if (box.intersect(boxes[res[i]])) return true;
//    return false;
//}

//void QuadTreeIntersector::clear() {
//    delete qtree;
//    qtree = new QuadTreeNode<int>(Rectf(-1,-1,1,1));
//    boxes.clear();
//}

////*****************************OcclusionAlgorithms**********************

//OcclusionAlgorithm::OcclusionAlgorithm(Intersector *inter) {
//    intersector = inter;
//}

//OcclusionAlgorithm::~OcclusionAlgorithm() {
//    delete intersector;
//}

//ClippingOcclusionAlgorithm::ClippingOcclusionAlgorithm(Intersector *inter)
//    : OcclusionAlgorithm(inter) {

//}

//void ClippingOcclusionAlgorithm::doOcclusion(std::vector<Label *> &labelList, std::vector<Label *> &occlusionList, const float ratio) {
//    //reserve the size of labelList, maybe all labels will be drawn. Resized later if needed
//    occlusionList.clear();
//    occlusionList.reserve( labelList.size() );
//    //labelList iterators
//    std::vector<Label*>::iterator it, itE;
//    intersector->clear();

//    Rectf screen(-1,-1,1,1);

//    it = labelList.begin();

//    for( itE = labelList.end(); it != itE; ++it ){
//        Rectf bbox = (*it)->viewBox;

//        if (bbox.intersect(screen)) {
//            if( !intersector->intersects(bbox) ){
//                occlusionList.push_back( *it );
//                intersector->addBox(bbox);
//            }
//        }
//    }
//    occlusionList.shrink_to_fit();
//}

//static void move(std::vector<Label*> &v, size_t from, size_t to) {
//    if (from <= to || from >= v.size()) return;

//    Label* l = v[from];
//    for(;from > to;from--)
//        v[from] = v[from-1];
//    v[to] = l;
//}

//ReorderingOcclusionAlgorithm::ReorderingOcclusionAlgorithm(Intersector *inter)
//    : OcclusionAlgorithm(inter) {

//}

//void ReorderingOcclusionAlgorithm::doOcclusion(std::vector<Label *> &labelList, std::vector<Label *> &occlusionList, const float ratio) {
//    Rectf screen(-1, -1, 1, 1);
//    occlusionList.clear();
//    intersector->clear();

//    unsigned int i=0, j=0;
//    while(i<labelList.size()) {
//        Label* l = labelList[i];
//        Rectf vbi = l->viewBox;
//        if (vbi.intersect(screen)) {
//            if (!intersector->intersects(vbi)) {
//                occlusionList.push_back(l);
//                intersector->addBox(vbi);
//                ++i;
//            }
//            else {
//                j=i+1;
//                while (j<labelList.size()) {
//                    Rectf vbj = l->viewBox;
//                    if (vbj.intersect(screen)) {
//                        if (!intersector->intersects(vbj)) {
//                            move(labelList, j, i);
//                            occlusionList.push_back(labelList[i]);
//                            intersector->addBox(vbj);
//                            break;
//                        }
//                        else ++j;
//                    }
//                    else break;
//                }
//                i = j+1;
//            }
//        }
//        else ++i;
//    }
//}
//NoOccAlgo::NoOccAlgo(Intersector *inter) : OcclusionAlgorithm(inter){

//}
//void NoOccAlgo::doOcclusion(std::vector<Label *> &labelList, std::vector<Label *> &occlusionList, const float ratio) {

//    occlusionList.clear();
//    intersector->clear();

//    std::queue<std::pair<unsigned int, unsigned int>> drawnReorder;

//    for (size_t i=0, j=0 ; i<labelList.size() ; ++i) {
//        //drawnReorder.push(std::make_pair(j,i));
//        //labelList[i] = labelList[drawnReorder.front().second];
//        occlusionList.push_back(labelList[i]);
//    }
//    }

//HeadReorderingOccAlgo::HeadReorderingOccAlgo(Intersector *inter)
//    : OcclusionAlgorithm(inter) {

//}
//void HeadReorderingOccAlgo::doOcclusion(std::vector<Label *> &labelList, std::vector<Label *> &occlusionList, const float ratio) {
//    Rectf screen(-1, -1, 1, 1);
//    occlusionList.clear();
//    intersector->clear();

//    std::queue<std::pair<unsigned int, unsigned int>> drawnReorder;
//    std::queue<std::pair<unsigned int, Label*>> notDrawnReorder;

//    for (size_t i=0, j=0 ; i<labelList.size() ; ++i) {
//        Rectf vb = labelList[i]->viewBox;
//        Vec2f center = vb.center();
//        vb[0] -= center;
//        vb[1] -= center;
//        vb[0] *= ratio;
//        vb[1] *= ratio;
//        vb[0] += center;
//        vb[1] += center;
//        labelList[i]->viewBox = vb;
//        if (labelList[i]->viewBox.intersect(screen) && !intersector->intersects(vb)) {
//            occlusionList.push_back(labelList[i]);
//            intersector->addBox(vb);
//            if(i!=j) {
//                drawnReorder.push(std::make_pair(j,i));
//            }
//            ++j;
//        }
//    }


//    for (size_t i=0 ; i<labelList.size() ; ++i) {
//        if (drawnReorder.size() > 0 &&
//                drawnReorder.front().first == i) {
//            if (notDrawnReorder.size() > 0 &&
//                    notDrawnReorder.front().first == i) {
//                labelList[i] = labelList[drawnReorder.front().second];

//                std::pair<int, Label*> tmp = notDrawnReorder.front();
//                notDrawnReorder.pop();
//                for(size_t k=0 ; k<notDrawnReorder.size() ; k++) {
//                    tmp.first = notDrawnReorder.front().first;
//                    notDrawnReorder.push(tmp);
//                    tmp=notDrawnReorder.front();
//                    notDrawnReorder.pop();
//                }
//                tmp.first = drawnReorder.front().second;
//                notDrawnReorder.push(tmp);

//                drawnReorder.pop();
//            }
//            else {
//                Label* tmp = labelList[i];
//                labelList[i] = labelList[drawnReorder.front().second];
//                notDrawnReorder.push(std::make_pair(drawnReorder.front().second, tmp));
//                drawnReorder.pop();
//            }
//        }
//        else if (notDrawnReorder.size() > 0 &&
//                 notDrawnReorder.front().first == i) {
//            labelList[i] = notDrawnReorder.front().second;
//            notDrawnReorder.pop();
//        }
//    }
//}

//InPlaceHeadReorderingOccAlgo::InPlaceHeadReorderingOccAlgo(Intersector *inter)
//    : OcclusionAlgorithm(inter) {

//}

//void InPlaceHeadReorderingOccAlgo::doOcclusion(std::vector<Label *> &labelList, std::vector<Label *> &occlusionList, const float ratio) {
//    Rectf screen(-1,-1,1,1);
//    occlusionList.clear();
//    intersector->clear();

//    size_t j=0;
//    for (size_t i=0 ; i<labelList.size() ; ++i) {
//        if(labelList[i]->viewBox.intersect(screen) && !intersector->intersects(labelList[i]->viewBox)) {
//            occlusionList.push_back(labelList[i]);
//            intersector->addBox(labelList[i]->viewBox);
//            if(i!=j) move(labelList, i, j);
//            j++;
//        }
//    }
//}

//*****************************LabelRender**********************
LabelRender::LabelRender(MatrixManager *manager, const std::string & fontPath, float minSize, float maxSize) :
   font(fontPath),
   maxTextSize(maxSize), minTextSize(minSize)
{
    matrixMngr = manager;
    renderer = new TextRenderer(manager);
    //occAlgo = new HeadReorderingOccAlgo(new QuadTreeIntersector()); // fuite
    //occAlgo = new NoOccAlgo(new QuadTreeIntersector());
}

LabelRender::~LabelRender() {
    delete renderer;
   // delete occAlgo;
    clear();
}

void LabelRender::clear()
{
   for (Label* l : labels)
      delete l;
   labelsMap.clear();
   labels.clear();
   labelsToDraw.clear();
   renderer = new TextRenderer(matrixMngr);
}


//void LabelRender::setText(int index, std::string text, std::string font, Vec2f position ){
//    Label* l = new Label();
//    int fontId = fontManager.getId(font);
//    l->font = fontId;
//    l->index = index;
//    glGenBuffers( 1, &l->vboID );

//    texture_font_t* textureFont = fontManager.getTexture(fontId);

//    std::vector<wchar_t> charcodes = getCharcodes(text);

//    wchar_t previous=0;
//    vec2 pen = {{ 0 , 0 }};
//    for( vector<wchar_t>::const_iterator it=charcodes.cbegin()
//         ; it != charcodes.cend() ; ++it) {
//        texture_glyph_t *glyph = texture_font_get_glyph( textureFont, *it );
//        if( glyph != NULL ){
//            int kerning = 0;
//            if( it != charcodes.cbegin()){
//                kerning = texture_glyph_get_kerning( glyph, previous );
//            }
//            pen.x += kerning;
//            float x0 = pen.x + glyph->offset_x;
//            float y0 = pen.y + glyph->offset_y;
//            float x1 = x0 + glyph->width;
//            float y1 = y0 - glyph->height;
//            float s0 = glyph->s0;
//            float t0 = glyph->t0;
//            float s1 = glyph->s1;
//            float t1 = glyph->t1;
//            /*    2------1  5
//       *    |     /  /|
//       *    |   /  /  |
//       *    | /  /    |
//       *    0  3------4
//       */
//            l->data.push_back(Vec4f(x0,y0, s0,t0));
//            l->data.push_back(Vec4f(x1,y1, s1,t1));
//            l->data.push_back(Vec4f(x0,y1, s0,t1));
//            l->data.push_back(Vec4f(x0,y0, s0,t0));
//            l->data.push_back(Vec4f(x1,y0, s1,t0));
//            l->data.push_back(Vec4f(x1,y1, s1,t1));
//            pen.x += glyph->advance_x;
//        }
//        previous = *it;
//    }
//    Vec2f minV(l->data[0][0],l->data[0][1]);
//    Vec2f maxV(l->data[0][0],l->data[0][1]);

//    for (size_t i=1; i< l->data.size(); ++i) {
//        minV[0] = std::min(l->data[i][0], minV[0]);
//        minV[1] = std::min(l->data[i][1], minV[1]);
//        maxV[0] = std::max(l->data[i][0], maxV[0]);
//        maxV[1] = std::max(l->data[i][1], maxV[1]);
//    }

//    Vec2f center = (minV + maxV)/2.f;
//    Vec2f labelSize  = (maxV - minV);
//    float ratio = labelSize[0] / labelSize[1];
//    for (size_t i=0; i < l->data.size(); ++i) {
//        l->data[i][0] = (l->data[i][0] - center[0]) * (1.0f / labelSize[1]);
//        l->data[i][1] = (l->data[i][1] - center[1]) * (1.0f / labelSize[1]);
//    }
//    //Rectf newBox(box[0][0]-5, box[0][1]-5, box[1][0]+5, box[1][1]+5);
//    float length = 1.0f * ratio/2.0f;
//    float height = 1.0f / 2.0f;
//    Rectf bbox(position[0]-length, position[1]-height, position[0]+length, position[1]+height);
//    l->boundingBox = bbox;
//    l->viewBox = bbox;
//    // COMMENT : "labelList[index] = l" leads to a seg fault
//    labelList.push_back(l);
//    glBindBuffer( GL_ARRAY_BUFFER, l->vboID );
//    glBufferData( GL_ARRAY_BUFFER, sizeof( Vec4f ) * l->data.size(), &l->data[0], GL_STATIC_DRAW );
//}

void LabelRender::occlusion( float ratio ){
    //return;

    for(int i=0 ; i<labels.size() ; ++i) {
        float size = renderer->getLabelSize(labels[i]);
        if (size > maxTextSize)
            renderer->setLabelSize(labels[i], maxTextSize);
        else if (size < minTextSize)
            renderer->setLabelSize(labels[i], minTextSize);
        else
            renderer->setLabelSize(labels[i], size);
        renderer->computeLabelViewBox(labels[i]);
    }
    //occAlgo->doOcclusion(labels, labelsToDraw, ratio);
    labelsToDraw = labels;
}

void LabelRender::draw() {
    renderer->draw(labelsToDraw, minTextSize, maxTextSize);
   // renderer->draw(labels, minTextSize, maxTextSize);
}

//void LabelRender::loadLabels(Graph* g) {
//    StringProperty* str = g->getProperty<StringProperty>("viewLabel");
//    LayoutProperty* layout = g->getProperty<LayoutProperty>("viewLayout");
//    node n;
//    forEach(n, g->getNodes()) {
//        string text = str->getNodeValue(n);
//        if (!text.empty()) {
//            Vec3f pos = layout->getNodeValue(n);
//            Label* label  = renderer->createLabel(pos, 0,font, text);
//            labels.push_back(label);
//            labelsMap.insert(std::make_pair(n.id, label));
//        }
//    }
//    FontTextureManager* fontManager = FontTextureManager::getInstance();
//    if (!fontManager->distanceMapComputed)
//        fontManager->convertTextureToDistanceMap(fontManager->getId(font));
//}

void LabelRender::addLabel(const std::string & str, float x, float y, float z)
{
   labels.push_back( renderer->createLabel(Vec3f(x,y,z), 0, font, str) );
}


void LabelRender::setMinTextSize(const float min) {
    minTextSize = min;
}

void LabelRender::setMaxTextSize(const float max) {
    maxTextSize = max;
}

Label* LabelRender::getLabel(node n) {
    std::map<int, Label*>::iterator ret;
    ret = labelsMap.find(n.id);
    if (ret == labelsMap.end())
        return NULL;
    else
        return (*ret).second;

}

Label* LabelRender::getLabel(unsigned id) {
   return labels[id];
}


MatrixManager* LabelRender::getMatrixManager() const {
    return matrixMngr;
}
