/*
  Copyright 2015 Timothée Jourde <itim.lcf@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <utils.h>

#include <string>
#include <cmath>
#include "Vector.h"
#include "ColorScale.h"
#include <glmatrix.h>

using namespace std;

std::string fileExtension(const std::string & path)
{
    const size_t unixDir = path.rfind("/");
    const size_t winDir  = path.rfind("\\");

    size_t dir;
    if (unixDir != std::string::npos and winDir != std::string::npos)
        dir = std::max(unixDir, winDir);
    else if (unixDir != std::string::npos)
        dir = unixDir;
    else if (winDir != std::string::npos)
        dir = winDir;
    else
        return "";

    const std::string basename = path.substr(dir, std::string::npos);
    const size_t dot = basename.rfind('.');

    if (dot == 0 or dot == std::string::npos or dot == std::string::npos-1)
        return "";
    else
        return basename.substr(dot+1, std::string::npos);
}


bool rectContains(const Vec4f r, const Vec2f p) {
    return (   p.x() >= r.x() and p.y() >= r.y()
           and p.x() <= r.z() and p.y() <= r.w()
           );
}

bool rectIntersects_(const Vec4f a, const Vec4f b)
{
    const Vec2f bSize = Vec2f(b.z() - b.x(), b.w() - b.y());

    return (  rectContains(a, Vec2f(b.x(),b.y()))
           or rectContains(a, Vec2f(b.z(),b.w()))
           or rectContains(a, Vec2f(b.x() + bSize.x(), b.y()))
           or rectContains(a, Vec2f(b.x(), b.y() + bSize.y()))

           or (   a.x() >= b.x() and a.x() <= b.z()
              and a.z() >= b.x() and a.z() <= b.z()
              and a.y() <= b.y() and a.w() >= b.w()
              )
           );
}
bool rectIntersects(const Vec4f a, const Vec4f b) {
    return (rectIntersects_(a,b) or rectIntersects_(b,a));
}

Vec4f screenRect(const GlMat4f & invProj)
{
   const  Vec4f botLeft  = invProj * Vec4f(-1,-1, 0,1);
   const  Vec4f topRight = invProj * Vec4f(1,1, 0,1);
    return Vec4f(botLeft.x(), botLeft.y(), topRight.x(), topRight.y());
}

Vec4f to_gl(Color c) {
   return Vec4f((float)c.getRGL(),(float)c.getGGL(),(float)c.getBGL(),(float)c.getAGL());
}


// void debug(istream& s, const string& token)
// {
//    //cerr << "< " << token << " >" << endl;

//    /*if (token == "-------")
//    {
//       string line;
//       getline(s, line);
//       cerr << "\n\n==> debug:\n" << line << endl;

//       exit(42);
//       } */
// }

tokenType readToken(istream & s, string & res)
{
   res.clear();

   int c;

   c = s.get();
   if      (c == ',')   return EMPTY_TOKEN;
   else if (c == '\n')  return LINE_END;
   else if (c == EOF)   return FILE_END;

   while (true) {
      res.push_back(c);

      c = s.get();
      if      (c == ',')   {
         //debug(s,res);
         return TOKEN;
      }
      else if (c == '\n')  {
         //debug(s,res);
         return LAST_TOKEN_LINE;
      }
      else if (c == EOF)   {
         //debug(s,res);
         return LAST_TOKEN_FILE;
      }
   }
}
string readTokenOfType(istream & s, const int flags)
{
   string res;
   if ( readToken(s,res) & flags ){
      return res;
   }
   else
      throw ParseError("token invalide");
}


float easeInOut(float t) // t doit être dans [0;1] // le retour est dans [0;1]
{
   const float LINE = 9; // doit être dans ]0;+inf[ , quand LINE tend vers 0, la fonction tend vers f(x)=x.
   if      (t <= 0)      return 0;
   else if (t >= 1)      return 1;
   else                  return ( atan((t-0.5) * LINE)
                  / (-2 * atan(-0.5*LINE))
                  + 0.5 );

}

bool isblank(const std::string & str) {
   for (auto c : str)
      if (not std::isblank(c)) return false;

   return true;
}
