/*
  Copyright 2015 Timothée Jourde <itim.lcf@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <Data.h>
#include <utils.h>


#include "ForEach.h"
#include <tuple>
#include <iostream>
#include <sstream>
#include <string>
#include <cfloat>

using namespace std;


template <typename T>
inline T & atR(vector<T> & v, const unsigned i) {
    if (v.size() < i+1)
        v.resize(i+1);
    return v[i];
}


Data::Data()
{
    RootGraph.alloc(NodeProp);
    RootGraph.alloc(EdgeProp);
    RootGraph.alloc(NSubGraphs);
    RootGraph.alloc(ESubGraphs);
}

Data::Data(Data * dataSrc,int dim){
    RootGraph.alloc(NodeProp);
    RootGraph.alloc(EdgeProp);
    RootGraph.alloc(NSubGraphs);
    RootGraph.alloc(ESubGraphs);

    Dimensions.resize(1);
    map<int, node> clust2nodes;
    node newNode, n;
    for (auto it: dataSrc->GetDimensions(dim)){
        n =it.second;
        newNode = RootGraph.addNode();

        NodeProp[newNode].dimension = 0;
        NodeProp[newNode].clusterId = dataSrc->NodeProp[n].clusterId;
        NodeProp[newNode].db_dimension = dataSrc->NodeProp[n].db_dimension;
        NodeProp[newNode].min = dataSrc->NodeProp[n].min;
        NodeProp[newNode].max = dataSrc->NodeProp[n].max;
        NodeProp[newNode].weight = dataSrc->NodeProp[n].weight;
        NodeProp[newNode].weightBefore = dataSrc->NodeProp[n].weightBefore;
        NodeProp[newNode].gapBefore = dataSrc->NodeProp[n].gapBefore;
        NodeProp[newNode].distortion = dataSrc->NodeProp[n].distortion;
        NodeProp[newNode].distrib.resize(dataSrc->NodeProp[n].distrib.size());
        for(unsigned int i = 0; i < dataSrc->NodeProp[n].distrib.size(); ++i){
            NodeProp[newNode].distrib[i] = dataSrc->NodeProp[n].distrib[i];// * dataSrc->DimensionProp[dim].norm;
        }
        clust2nodes[NodeProp[newNode].clusterId] = newNode;
    }
    Dimensions[0] = clust2nodes;

    DB2ClientDimNumber[dataSrc->Client2DBDimNumber[dim]] = 0;
    Client2DBDimNumber[0] = dataSrc->Client2DBDimNumber[dim];

    DimensionProp.resize(1);
    DimensionProp[0].label = dataSrc->DimensionProp[dim].label;
    DimensionProp[0].pos = dataSrc->DimensionProp[dim].pos;
    DimensionProp[0].min = dataSrc->DimensionProp[dim].min;
    DimensionProp[0].max = dataSrc->DimensionProp[dim].max;
    DimensionProp[0].gapRatio = dataSrc->DimensionProp[dim].gapRatio;
    DimensionProp[0].norm = dataSrc->DimensionProp[dim].norm;

    Gap.resize(1);
    Gap[0] = dataSrc->Gap[dim];

    Count();
}

Data::~Data()
{
    Clear();
    //RootGraph.free(SubGraphs);
 }


/////////////////////////
// PARSING REMOTE DATA //
/////////////////////////

void Data::ParseResponse(istream & s, parseNodeLineFn parseNodeLineF, edgeAddFn edgeAddF)
{
    unsigned line = 1;

    try
    {
        while ((this->*parseNodeLineF)(s)) line++;
        line++;
        while (ParseEdgeLine(s, edgeAddF)) line++;
    }
    catch(const ParseError & e) {
        throw ParseError(e.message, line);
    }
    catch(const invalid_argument & e) {
        throw ParseError("nombre invalide", line);
    }
}

bool Data::ParseSubNodeLine(istream & s)
{
    string token;
    switch (readToken(s,token)) {
    case TOKEN:      break;
    case LINE_END:   return false;
    default: throw  ParseError("début de ligne invalide");
    }
    const int db_dim = stoi(token);
    bool endLine = false;
    while (not endLine)
    {
        const int clusterId = stoi(readTokenOfType(s, TOKEN));

        switch (readToken(s, token)) {
        case TOKEN:           break;
        case LAST_TOKEN_LINE: endLine = true; break;
        default:              throw ParseError("dernier token invalide");
        }

        const unsigned long long nbBars = stoull(token);
        unsigned long long weight  = 0;
        for(unsigned int i = 0; i < nbBars; ++i){
            switch (readToken(s, token)) {
            case TOKEN:           break;
            case LAST_TOKEN_LINE: endLine = true; break;
            default:              throw ParseError("dernier token invalide");
            }
            weight += stoull(token);
        }

        //        unsigned long long weight = stoull( token );
        if ( not AddSubNode(db_dim,clusterId,weight) )
            throw ParseError("noeud inexistant dans le graphe raçine " + to_string(db_dim) + "/"+ to_string(clusterId));
    }

    return true;
}

bool Data::ParseRootNodeLine(istream & s)
{
    string token;
    switch (readToken(s,token)) {
    case TOKEN:      break;
    case LINE_END:   return false;
    default: throw  ParseError("début de ligne invalide");
    }
    const int db_dim = stoi(token);
    int Client_Dim;
    if(DB2ClientDimNumber.find(db_dim)!=DB2ClientDimNumber.end())
        Client_Dim = DB2ClientDimNumber[db_dim];
    else{
        Client_Dim = Dimensions.size();
        DB2ClientDimNumber[db_dim] = Client_Dim;
        Client2DBDimNumber[Client_Dim] = db_dim;
    }

    string lbl = readTokenOfType(s, TOKEN);
    //    atR(Labels, Client_Dim) = lbl;
    //Labels[.push_back[client_Dim] = lbl;
    atR(DimensionProp, Client_Dim).label = lbl;
    atR(DimensionProp, Client_Dim).pos = Client_Dim;


    bool endLine = false;
    while (not endLine)
    {
        const int clusterId = stoi( readTokenOfType(s, TOKEN) );

        const double min = stod( readTokenOfType(s, TOKEN) );
        const double max = stod( readTokenOfType(s, TOKEN) );

        const int nDistrib = stoi( readTokenOfType(s, TOKEN) );

        vector<float> distrib;
        distrib.resize(nDistrib);

        int i=0;
        for(; i < nDistrib-1; i++){
            distrib[i] = stof( readTokenOfType(s, TOKEN) );
        }
        switch (readToken(s, token)) {
        case TOKEN:           break;
        case LAST_TOKEN_LINE: endLine = true; break;
        default:              throw ParseError("dernier token invalide");
        }
        distrib[i] = stoull( token );
        AddRootNode(db_dim, clusterId, min, max, move(distrib));
    }

    return true;
}


bool Data::ParseEdgeLine(istream & s, edgeAddFn add)
{
    string token;
    switch (readToken(s,token)) {
    case TOKEN:      break;
    case LINE_END:   return false;
    case FILE_END:   return false;
    default:         throw  ParseError("début de ligne invalide");
    }
    const int srcDim = stoi(token);
    switch (readToken(s,token)) {
    case TOKEN:      break;
    case LINE_END:   return false;
    case FILE_END:   return false;
    default:         throw  ParseError("début de ligne invalide");
    }
    const int tgtDim = stoi(token);

    bool endLine = false;
    while (not endLine)
    {
        const int srcCluster  = stoi( readTokenOfType(s, TOKEN) );
        const int destCluster = stoi( readTokenOfType(s, TOKEN) );

        switch (readToken(s, token)) {
        case TOKEN:           break;
        case LAST_TOKEN_LINE: endLine = true; break;
        case LAST_TOKEN_FILE: endLine = true; break;
        default:              throw ParseError("dernier token invalide");
        }
        const unsigned long long weight = stoull( token );

        if ( not (this->*add)(srcDim, tgtDim, srcCluster, destCluster, weight) )
            throw ParseError("l'arrete référence un noeud inexistant " + to_string(srcDim) + "/" + to_string(srcCluster) + " /// " + to_string(tgtDim) + "/" + to_string(destCluster));
    }

    return true;
}


node Data::AddRootNode(int db_dimension, int clusterId, float min, float max, std::vector<float> distrib)
{
    node n = RootGraph.addNode();

    int dim;
    if(DB2ClientDimNumber.find(db_dimension)!=DB2ClientDimNumber.end())
        dim = DB2ClientDimNumber[db_dimension];
    else{
        dim = Dimensions.size();
        DB2ClientDimNumber[db_dimension] = dim;
        Client2DBDimNumber[dim] = db_dimension;
    }

    atR(Dimensions,dim)[clusterId] = n;
    NodeProp[n].clusterId = clusterId;
    NodeProp[n].dimension = (float)dim;
    NodeProp[n].db_dimension = db_dimension;
    NodeProp[n].distrib   = move(distrib);
    NodeProp[n].min = min;
    NodeProp[n].max = max;
    NodeProp[n].weight = 0;
    for(auto w : NodeProp[n].distrib){
        NodeProp[n].weight += w;
    }
    return n;
}

bool Data::AddRootEdge(int db_srcDimension, int db_tgtDimension, int srcCluster, int destCluster, unsigned long long weight)
{


    node nodeSrc, nodeDest;
    try
    {
        nodeSrc  = Dimensions.at(DB2ClientDimNumber.at(db_srcDimension)).at(srcCluster);
        nodeDest = Dimensions.at(DB2ClientDimNumber.at(db_tgtDimension)).at(destCluster);
    }
    catch(const out_of_range & e) {
        return false;
    }

    edge e = RootGraph.addEdge(nodeSrc, nodeDest);
    EdgeProp[e].weight = weight;
    return true;
}

bool Data::AddSubNode(int dimension, int clusterId, unsigned long long weight)
{
    node n;
    try {
        n = Dimensions.at(DB2ClientDimNumber.at(dimension)).at(clusterId);
    }
    catch(const out_of_range & e) {
        return false;
    }

    CurrentSubGraph->nodes.push_back( n );
    CurrentSubGraph->nodesWeight.push_back( weight );
    return true;
}
//bool Data::AddSubEdge(int srcDimension, int srcCluster, int destCluster, unsigned long long weight)
//{
//    node nodeSrc, nodeDest;
//    try
//    {
//        nodeSrc  = Dimensions.at(srcDimension)  .at(srcCluster);
//        nodeDest = Dimensions.at(srcDimension+1).at(destCluster);
//    }
//    catch(const out_of_range & e) {
//        return false;
//    }

//    const edge e = RootGraph.existEdge(nodeSrc,nodeDest);
//    if (e.isValid())
//    {
//        CurrentSubGraph->edges.push_back( e );
//        CurrentSubGraph->edgesWeight.push_back( weight );
//        return true;
//    }
//    else // normalement impossible
//        return false;
//}

bool Data::AddSubEdge(int db_srcDimension,int db_tgtDimension, int srcCluster, int destCluster,  long long unsigned weight)
{

    node nodeSrc, nodeDest;
    try
    {
        nodeSrc  = Dimensions.at(DB2ClientDimNumber.at(db_srcDimension))  .at(srcCluster);
        nodeDest = Dimensions.at(DB2ClientDimNumber.at(db_tgtDimension)).at(destCluster);
    }
    catch(const out_of_range & e) {
        return false;
    }
    const edge e = RootGraph.existEdge(nodeSrc,nodeDest);
    if (e.isValid())
    {
        CurrentSubGraph->edges.push_back( e );
        CurrentSubGraph->edgesWeight.push_back( weight );
        return true;
    }
    else // normalement impossible
        return false;
}


void Data::Init()
{
    Clear();
    Count();

    Gap.resize( Dimensions.size() );
    DimensionProp.resize(Dimensions.size());
    PositionNodes();
    PositionEdges();

    ZOrderEdges();
    //NormalizeDistribPerNode();
    NormalizeDistribPerDimension();
}

void Data::ClearSubGraphs(){
    node n;
    forEach( n , RootGraph.getNodes ()){
        subGraph * sub = &(NSubGraphs[n]);
        if(sub->nodes.size() != 0){
            sub->nodes.clear();
            sub->nodesWeight.clear();
            sub->edges.clear();
            sub->edgesWeight.clear();

        };
        //delete sub;
    }

    for(auto e : RootGraph.getEdges()){
        subGraph * sub = &(ESubGraphs[e]);
        if(sub->edges.size() != 0){
            sub->nodes.clear();
            sub->nodesWeight.clear();
            sub->edges.clear();
            sub->edgesWeight.clear();

        };
        //delete sub;
    }
}

void Data::Clear()
{
    return;
    if (Observer_ != nullptr)
    {
        //Observer_->OnDelete(*this);
        delete Observer_;
        Observer_ = nullptr;
    }

    Server = nullptr;
    RemoteDataSetPath = "";
    RootGraph.clear();
    Dimensions.clear();
    Gap.clear();
    //Labels.clear();
    NbIndividuals = 0;

    RootGraph.delAllNodes();
    RootGraph.delAllEdges();
    RootGraph.free(EdgeProp);
    RootGraph.free(NodeProp);
    RootGraph.free(NSubGraphs);
    RootGraph.free(ESubGraphs);

    //    delete SubGraphs;
    //    delete NodeProp;
    //    delete EdgeProp;
}

void Data::Count()
{
    NbIndividuals = 0;
    for (const auto it : Dimensions[0])
        NbIndividuals += NodeProp[it.second].weight;
}


/////////////////
// POSITIONING //
/////////////////

void Data::PositionNodes()
{
 // progress("Positioning nodes...");
    //unsigned progressI = 0;

    for (unsigned dim=0; dim<Dimensions.size(); dim++) // pour chaque axe
    {
        vector<node> nodes;
        for (const auto it : Dimensions[dim])
            nodes.push_back(it.second);

        sort(nodes.begin(), nodes.end(), compNode(NodeProp));

        /* [PASSE 1] somme des écarts, somme des intervales, initialisation distortion */
        float intervalSum = 0;
        float gapSum = 0;
        float prev   = NodeProp[nodes[0]].max;
        float maxDimValue = NodeProp[nodes[0]].max;
        float minDimValue = NodeProp[nodes[0]].min;
        for (unsigned i=1; i < nodes.size(); i++)
        {
            nodeProp & np = NodeProp[nodes[i]];
            if(np.max > maxDimValue) maxDimValue = np.max;
            if(np.min < minDimValue) minDimValue = np.min;

            intervalSum += np.max - np.min;
            np.distortion = 0; // par défaut

            gapSum += abs(np.min - prev);
            prev = np.max;
        }
        Gap[dim] = (gapSum != 0);

        DimensionProp[dim].max      = maxDimValue;
        DimensionProp[dim].min      = minDimValue;
        DimensionProp[dim].gapRatio = 0.;
        //        if (dim==9) DimensionProp[dim].gapRatio = 0.9;
        //        if (dim==6) DimensionProp[dim].gapRatio = 1.25;

        /* [PASSE 2] minDistortion */
        float minDistortion = FLT_MAX;

        if (intervalSum == 0)
            minDistortion = 0;
        else
            for (unsigned i=1; i < nodes.size(); i++)
            {
                nodeProp & np = NodeProp[nodes[i]];
                np.distortion = ((np.max - np.min) /(float) intervalSum) - (np.weight /(float) NbIndividuals);

                if (np.distortion < minDistortion)
                    minDistortion = np.distortion;
            }


        /* [PASSE 3] gapBefore, weightBefore, et distortion */
        nodeProp & np0 = NodeProp[ nodes[0] ];
        np0.gapBefore    = 0;
        np0.weightBefore = 0;
        float weightBefore = np0.weight /(float) NbIndividuals;
        float gapBefore    = 0;
        prev   = NodeProp[nodes[0]].max;
        for (unsigned i=1; i < nodes.size(); i++)
        {
            nodeProp &  np = NodeProp[nodes[i]];

            np.distortion = (np.distortion - minDistortion) / 2.0;

            gapBefore += (gapSum == 0) ? 0 : abs(np.min - prev) / gapSum;


            np.gapBefore = gapBefore;
            np.weightBefore = weightBefore;

            weightBefore += np.weight /(float) NbIndividuals;
            prev = np.max;

//            progressI++;
//            progress.update( progressI /(float) RootGraph.numberOfNodes() );
        }
    }

//    progress.update(1);
}

void Data::PositionEdges()
{
//    Progress progress("Positioning edges...");
//    unsigned i = 0;

    for (auto & nodes : Dimensions)
        for (const auto it : nodes) // pour chaque noeud
        {
            const node n = it.second;
            const nodeProp & np = NodeProp[n];

            vector< pair<edge,float> > inEdges, outEdges;
            for(const edge e : RootGraph.star(n))
            {
                const pair<node,node> ends = RootGraph.ends(e);
                if (n == ends.first)
                    outEdges.push_back({ e, NodeProp[ends.second].weightBefore });
                else
                    inEdges.push_back({  e, NodeProp[ends.first ].weightBefore });
            }

            sort(inEdges.begin() , inEdges.end() , compEdge());
            sort(outEdges.begin(), outEdges.end(), compEdge());


            float pos = 0;
            for(const auto it : inEdges)
            {
                edgeProp &  ep = EdgeProp[it.first];
                const float hh = ep.weight /(float) np.weight / 2;
                pos += hh;
                ep.destPos = pos;
                pos += hh;
            }

            pos = 0;
            for(const auto it : outEdges)
            {
                edgeProp &  ep = EdgeProp[it.first];
                const float hh = ep.weight /(float) np.weight / 2;
                pos += hh;
                ep.sourcePos = pos;
                pos += hh;
            }

//            i++;
//            progress.update( i /(float) RootGraph.numberOfNodes() );
        }

//    progress.update(1);
}

void Data::ZOrderEdges()
{
    std::vector<edge> edges = RootGraph.edges();
    sort(edges.begin(),edges.end(), [this](edge a, edge b){
        return this->EdgeProp[a].weight > this->EdgeProp[b].weight;
    });

    float z = 0;
    for(const edge e : edges)
    {
        EdgeProp[e].z = z;
        z += 0.0001;
    }
}

void Data::NormalizeDistribPerNode()
{
    for (auto & nodes : Dimensions)
        for (const auto it : nodes) // pour chaque noeud
        {
            nodeProp & np = NodeProp[it.second];

            float max = FLT_MIN;
            for (const float d : np.distrib)
                if (d > max)
                    max = d;

            for (float & d : np.distrib)
                d = d/max;
        }
}

void Data::NormalizeDistribPerDimension()
{
    for (auto & nodes : Dimensions)
    {
        float max = FLT_MIN;
        for (const auto it : nodes) // pour chaque noeud
            for (const float d : NodeProp[it.second].distrib)
                if (d > max){
                    max = d;
                    DimensionProp[NodeProp[it.second].dimension].norm = max;
                }
        for (const auto it : nodes) // pour chaque noeud
            for (float & d : NodeProp[it.second].distrib)
                d = d/max;
    }
}


void Data::SetRemoteDataSet(const std::string & hostname, const short port, const std::string & baseurl, const std::string & path, const std::string dimensions)
{
    if (Server != nullptr)
        delete Server;
    Server = new RestServer(hostname,port,baseurl);
    RemoteDataSetHostName = hostname;
    RemoteDataSetPort = port;
    RemoteDataSetBaseURL = baseurl;
    RemoteDataSetPath = path;

    Server->sendRequest(RemoteDataSetPath+dimensions,
                        [this](const std::string response)
    {
        // cerr << "\n\n\n\n[RESPONSE]\n" << response << "\n[END RESPONSE]\n\n\n\n";
        stringstream ss(response);

        try
        {
            this->ParseResponse( ss, &Data::ParseRootNodeLine, &Data::AddRootEdge );
            this->Init();

            // on notifie la vue
            if (Observer_ != nullptr)
                Observer_->OnRootGraph(*this);
        }
        catch(const ParseError & e) {
            cerr << "[PARSE ERROR] \"" << e.message << "\" on a root graph response, line " << e.line << endl;
        }
    });
}

void Data::setGapRatio(int d, const float x){
    if (x>=0. && x<=0.9){
        DimensionProp[d].gapRatio = x;
        // on notifie la vue
        if (Observer_ != nullptr)
            Observer_->OnRootGraph(*this);
    }
}

bool Data::RequestSubGraph(int dimension, int clusterId)
{
    int db_dimension = Client2DBDimNumber.at(dimension);
    const node n = Dimensions.at(dimension).at(clusterId);
    subGraph * sub = &( NSubGraphs[n] );

    if (not sub->nodes.empty())
    {
        // on notifie la vue
        if (Observer_ != nullptr)
            Observer_->OnSubGraph(*this, dimension, clusterId, n, *sub);

        return false;
    }
    string path = RemoteDataSetPath + "?axe=" + to_string(db_dimension) + "&pos=" + to_string(clusterId) + "&dimensions=";
    for(unsigned int i = 0; i < Dimensions.size();++i){
        if(Client2DBDimNumber.find(i) == Client2DBDimNumber.end()) continue;
        int db_dim = Client2DBDimNumber.at(i);
        path += to_string(db_dim);
        //       if( i + 1 < Dimensions.size())
        path += ",";
    }
    path = path.substr (0, path.size()-1);
    Server->sendRequest(path,
                        [sub,dimension,clusterId,n,this](const std::string response)
    {
        //        cerr << "\n\n\n\n[RESPONSE]\n" << response << "\n[END RESPONSE]\n\n\n\n";
        //exit(1);

        stringstream ss(response);
        this->CurrentSubGraph = sub;

        try
        {

            this->ParseResponse( ss, &Data::ParseSubNodeLine, &Data::AddSubEdge );

            // on notifie la vue
            if (Observer_ != nullptr)
                Observer_->OnSubGraph(*this, dimension, clusterId, n, *sub);
        }
        catch (const ParseError & e) {
            cerr << "[PARSE ERROR] \"" << e.message << "\" on a sub graph response ; nodeKey (" << dimension << "," << clusterId << "), line " << e.line << endl;
        }

        this->CurrentSubGraph = nullptr;
    });

    return true;
}



bool Data::RequestSubGraph(edge e)
{
    subGraph * sub = &( ESubGraphs[e] );

    if (not sub->nodes.empty())
    {
        // on notifie la vue
        if (Observer_ != nullptr)
            Observer_->OnSubGraph(*this, e, *sub);
        return false;
    }


    node src = RootGraph.source(e);
    node tgt = RootGraph.target(e);
    const nodeProp & srcp = NodeProp[src];
    const nodeProp & tgtp = NodeProp[tgt];

    int src_db_dimension = Client2DBDimNumber.at(srcp.dimension);
    int tgt_db_dimension = Client2DBDimNumber.at(tgtp.dimension);
    string path = RemoteDataSetPath + "?axeSrc=" + to_string(src_db_dimension) + "&posSrc=" + to_string(srcp.clusterId) +
            "&axeDst=" + to_string(tgt_db_dimension) + "&posDst=" + to_string(tgtp.clusterId) + "&dimensions=";
    for(unsigned int i = 0; i < Dimensions.size();++i){
        if(Client2DBDimNumber.find(i) == Client2DBDimNumber.end()) continue;
        int db_dim = Client2DBDimNumber.at(i);
        path += to_string(db_dim);
        path += ",";
    }
    path = path.substr (0, path.size()-1);
    Server->sendRequest(path,
                        [sub,e,this](const std::string response)
    {
        stringstream ss(response);
        this->CurrentSubGraph = sub;

        try
        {
//            cerr << "\n\n\n\n[RESPONSE]\n" << response << "\n[END RESPONSE]\n\n\n\n";
            this->ParseResponse( ss, &Data::ParseSubNodeLine, &Data::AddSubEdge );

            // on notifie la vue
            if (Observer_ != nullptr)
                Observer_->OnSubGraph(*this, e, *sub);
        }
        catch (const ParseError & pe) {
            cerr << "[PARSE ERROR] \"" << pe.message << "\" on a sub graph response ; edge " << e.id << ", line " << pe.line << endl;
        }

        this->CurrentSubGraph = nullptr;
    });

    return true;
}


bool Data::PerformRequests()
{
    if (Server != nullptr){
        return Server->performRequests();

    }
    else
        return false;
}

std::string Data::ToString() const
{
    return "";
}

Data * Data::GetDeletionSource(int DimClient){
    Data * DataTmpSource = new Data();
    string dimsList = "?dimensions=";
    unsigned int i = DimClient;
    if(DimClient > 0) i = DimClient-1;
    for(; i <= (DimClient+1<Dimensions.size()?DimClient+1:DimClient); ++i){
        if(i != DimClient){
            dimsList += to_string(Client2DBDimNumber[i]) + ",";
        }
    }
    assert(dimsList.size() != 0);
    dimsList = dimsList.substr(0, dimsList.size()-1);
    DataTmpSource->SetRemoteDataSet(RemoteDataSetHostName,RemoteDataSetPort,RemoteDataSetBaseURL,RemoteDataSetPath, dimsList);
    return DataTmpSource;
}


Data * Data::GetInsertionSource(int DimDB, int Prev_Client, int Next_Client){

    assert(DB2ClientDimNumber.find(DimDB) == DB2ClientDimNumber.end());
    Data * DataTmpSource = new Data();

    string dimsList = "?dimensions=";
    if(Prev_Client != -1)
        dimsList+= to_string(Client2DBDimNumber[Prev_Client]) + ",";
    dimsList+= to_string(DimDB);
    if(Next_Client < Dimensions.size())
        dimsList += ","+to_string(Client2DBDimNumber[Next_Client]) ;

    DataTmpSource->SetRemoteDataSet(RemoteDataSetHostName,RemoteDataSetPort,RemoteDataSetBaseURL,RemoteDataSetPath, dimsList);

    return DataTmpSource;

}


void Data::InsertDimension(Data * DataTmpSource, int DB_Dim, int Client_Prev, int Client_Next){
    VectorGraph TmpRoot = DataTmpSource->RootGraph;
    vector< std::map<int,node> > TmpDimensions = DataTmpSource->Dimensions;
    assert(TmpDimensions.size()<=3 && TmpDimensions.size()>=1);


    // MAJ Client2DBDimNumber et de DB2ClientDimNumber
    int lastClientDim = Dimensions.size();
    for(int i = lastClientDim; i > Client_Next && Client_Next<Dimensions.size(); --i){
        Client2DBDimNumber[i] = Client2DBDimNumber[i-1];
        DB2ClientDimNumber[Client2DBDimNumber[i]] = i;
    }
    Client2DBDimNumber[Client_Next]= DB_Dim;
    DB2ClientDimNumber[DB_Dim] = Client_Next;


    // SUPPRESSION ANCIENNES ARETES
    map<int, node> PrevDim;
    if(Client_Prev != -1){
        PrevDim = TmpDimensions[0];
        map<int, node> NextDim;
        if(Client_Next < Dimensions.size()) NextDim = TmpDimensions[2];
        for(auto prev  : PrevDim){
            node n1 = Dimensions[Client_Prev][prev.first];
            for(auto next : NextDim){
                node n2 = Dimensions[Client_Next][next.first];//next.second;
                edge ETmp = RootGraph.existEdge(n1,n2, false);
                if(ETmp.isValid()){
                    RootGraph.delEdge(ETmp);
                }
            }
        }
    }

    // MAJ Dimensions
    Dimensions.resize(Dimensions.size()+1);
    for(int i = lastClientDim; i > Client_Next && Client_Next<Dimensions.size(); --i){
        Dimensions[i] = Dimensions[i-1];

        for(auto it : Dimensions[i]){
            node n = it.second;
            NodeProp[n].dimension = i;
            NodeProp[n].db_dimension = Client2DBDimNumber[i];
        }

    }
    Dimensions[Client_Next].clear();

    // INSERTION NOUVEAUX SOMMETS
    map<int, node> insertDim;
    if(Client_Prev == -1)
        insertDim = TmpDimensions[0];
    else insertDim = TmpDimensions[1];
    for(auto it : insertDim){
        int clustId = it.first;
        node n = it.second;
        nodeProp &np = DataTmpSource->NodeProp[n];
        vector <float> newDistrib;
        newDistrib.clear ();
        float norm_value = DataTmpSource->DimensionProp[np.dimension].norm;
        for (float & d : np.distrib)
            d = d*norm_value;
        n = AddRootNode (DB_Dim, clustId, np.min, np.max, np.distrib);
        for (float & d : NodeProp[n].distrib)
            d = d/norm_value;
    }


    // INSERTION NOUVELLES ARETES
    edge e;
    forEach(e, TmpRoot.getEdges()){
        node src = TmpRoot.source(e);
        node tgt = TmpRoot.target(e);
        nodeProp & srcNp =DataTmpSource->NodeProp[src];
        nodeProp & tgtNp =DataTmpSource->NodeProp[tgt];
        AddRootEdge (srcNp.db_dimension, tgtNp.db_dimension, srcNp.clusterId, tgtNp.clusterId, DataTmpSource->EdgeProp[e].weight);

    }


    // MAJ DIMENSIONPROP
    DimensionProp.resize(DimensionProp.size()+1);
    for(int i = DimensionProp.size()-1; i > Client_Next && Client_Next < DimensionProp.size(); --i){
        int pos = DimensionProp[i].pos;
        DimensionProp[i] = DimensionProp[i-1];
        if(i == DimensionProp.size()-1){
            DimensionProp[i].pos = DimensionProp[i-1].pos + 1;
        } else DimensionProp[i].pos = pos;

    }
    int pos = DimensionProp[Client_Next].pos;
    if(Client_Prev == -1){
        DimensionProp[Client_Next] = DataTmpSource->GetDimensionProp ()[0] ;
    }
    else if(Client_Next < DimensionProp.size()){
        DimensionProp[Client_Next] = DataTmpSource->GetDimensionProp ()[1] ;
    }
    if(Client_Next != DimensionProp.size()-1)
        DimensionProp[Client_Next].pos = pos;
    else
        DimensionProp[Client_Next].pos = DimensionProp[Client_Next-1].pos + 1;

    PositionNodes ();
    PositionEdges ();

    ClearSubGraphs ();
    Count();


    if (Observer_ != nullptr)
        Observer_->OnRootGraph(*this, false);

}

void  Data::DeleteDimension(Data *DataTmpSource, int AnimatedDimension){
    VectorGraph TmpRoot = DataTmpSource->RootGraph;
    vector< std::map<int,node> > TmpDimensions = DataTmpSource->Dimensions;
    assert(TmpDimensions.size()<=2 && TmpDimensions.size()>0);

    if(TmpDimensions.size() == 2){
        map<int, node> TmpDim1 = TmpDimensions[0];
        map<int, node> TmpDim2 = TmpDimensions[1];

        for(auto tmp1 : TmpDim1){
            int id1 = tmp1.first;
            node n1 = tmp1.second;

            for(auto tmp2 : TmpDim2){
                int id2 = tmp2.first;
                node n2 = tmp2.second;
                edge ETmp = TmpRoot.existEdge(n1,n2, false);
                if(ETmp.isValid()){
                    AddRootEdge (Client2DBDimNumber[AnimatedDimension-1],
                            Client2DBDimNumber[AnimatedDimension+1],
                            id1, id2, DataTmpSource->GetEdgeProp(ETmp).weight);
                }
            }
        }
    }
    // suppression de l'axe dans les données
    //suppression des sommets et arêtes
    for (auto t: Dimensions[AnimatedDimension]){//delDim){
        node n = t.second;
        RootGraph.delEdges (n);
        RootGraph.delNode(n);
    }
    // maj des map DB2Client
    for (unsigned int i = AnimatedDimension;i+1<Client2DBDimNumber.size ();++i){
        Client2DBDimNumber[i] = Client2DBDimNumber[i+1];
    }
    Client2DBDimNumber.erase (Client2DBDimNumber.size()-1);
    DB2ClientDimNumber.clear();
    for(auto it : Client2DBDimNumber){
        int idClient = it.first;
        int idDB = it.second;
        DB2ClientDimNumber[idDB] = idClient;
    }

    DimensionProp.erase(DimensionProp.begin()+AnimatedDimension);
    Dimensions.erase(Dimensions.begin()+AnimatedDimension);

    PositionNodes ();
    PositionEdges ();
    Count();

    ClearSubGraphs ();
    if (Observer_ != nullptr)
        Observer_->OnRootGraph(*this, false);
}


/*
void Data::InsertDimensions(int srcDim, int tgtDim, bool before){
    string path = RemoteDataSetPath + "?dimensions=";
    if(before ){
        for(unsigned int i = 0; i < Dimensions.size();++i){
            int db_dim = Client2DBDimNumber[i];
            if(i == srcDim)
                continue;//db_dim = Client2DBDimNumber.at(tgtDim);
            else if( i == tgtDim){
                int tmp_dim = Client2DBDimNumber.at(srcDim);
                path += to_string(tmp_dim)+",";
            }
            path += to_string(db_dim);
            if( i + 2 < Dimensions.size() or (i+1 != srcDim and i+2 == Dimensions.size()))
                path += ",";
        }
    }
    else {
        for(unsigned int i = 0; i < Dimensions.size();++i){
            int db_dim = Client2DBDimNumber.at(i);
            if(i == srcDim)
                continue;//db_dim = Client2DBDimNumber.at(tgtDim);
            else if( i == tgtDim){
                path += to_string(db_dim)+",";
                db_dim = Client2DBDimNumber.at(srcDim);
            }
            path += to_string(db_dim);
            if( i + 2 <= Dimensions.size())// or (i+1 != srcDim and i+2 == Dimensions.size()))
                path += ",";
        }
    }
    RootGraph.free(NodeProp);
    RootGraph.free(EdgeProp);
    RootGraph.free(SubGraphs);
    RootGraph.clear();
    Dimensions.clear();
    Gap.clear();
    Labels.clear();
    NbIndividuals = 0;

    DimensionProp.clear();
    DB2ClientDimNumber.clear();
    Client2DBDimNumber.clear();

    RootGraph.alloc(NodeProp);
    RootGraph.alloc(EdgeProp);
    RootGraph.alloc(SubGraphs);
    Server->sendRequest(path,
                        [this](const std::string response)
    {

        stringstream ss(response);

        try
        {
            this->ParseResponse( ss, &Data::ParseRootNodeLine, &Data::AddRootEdge );
            this->Init();

            // on notifie la vue
            if (Observer_ != nullptr)
                Observer_->OnRootGraph(*this);
        }
        catch(const ParseError & e) {
            cerr << "[PARSE ERROR] \"" << e.message << "\" on a root graph response, line " << e.line << endl;
        }
    });
    //XXX TODO : mettre a jour le sous-graphe s'il existe
}
*/
