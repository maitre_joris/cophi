#-------------------------------------------------
#
# Project created by QtCreator 2012-01-27T17:38:13
#
#-------------------------------------------------
TEMPLATE = app
CONFIG += console
CONFIG -= qt
CONFIG -= gui

TARGET = gview

SOURCES += \
    ColorScale.cpp \
    vectorgraph.cpp \
    glmatrix.cpp \
    matrixManager.cpp \
#    Animation.cpp \
    httprequest.cpp \
    RestServer.cpp \
    utf8Iterator.cpp \
    shaderProgram.cpp \
    ../thirdparty/freetype-gl/texture-atlas.c \
    ../thirdparty/freetype-gl/texture-font.c \
    ../thirdparty/freetype-gl/vector.c \
    ../thirdparty/freetype-gl/edtaa3func.c \
    ../include/cxx/GlMatrix.cxx \
    TextRenderer.cpp \
    ParaCoord.cpp \
    utils.cpp \
    Data.cpp \
    Renderer.cpp \
    LabelRender.cpp \
    ../include/cxx/Vector.cxx \
    ../include/cxx/Array.cxx \
    Color.cpp
#    binds.cpp




HEADERS += \
    ../include/Edge.h \
    ../include/Node.h \
    ../include/vectorgraph.h \
    ../include/vectorgraphproperty.h \
    ../include/glmatrix.h \
    ../include/shaderProgram.h \
    ../thirdparty/freetype-gl/vector.h \
    ../thirdparty/freetype-gl/vec234.h \
    ../thirdparty/freetype-gl/texture-font.h \
    ../thirdparty/freetype-gl/texture-atlas.h \
    ../thirdparty/freetype-gl/edtaa3func.h \
    ../include/matrixManager.h \
    ../include/TextRenderer.h \
    ../include/utils.h \
    ../include/Data.h \
    ../include/Renderer.h \
    ../include/utils.h \
    ../include/LabelRender.h \
    ../include/httprequest.h \
    ../include/RestServer.h \
    ../include/ColorScale.h \
    ../include/Iterator.h \
    ../include/StlIterator.h \
    ../include/memorypool.h \
    ../include/ForEach.h \
    ../include/Vector.h \
    ../include/Array.h \
    ../include/Rectangle.h \
    ../include/Color.h

#LIBS += -lGLU -lGLEW -lglut -lgomp -lGL
LIBS += -lGLU -lGLEW -lglut -lGL
#LIBS += -L/usr/local/lib -ltulip-core-4.8  -lcurl -lcurlpp  -lyajl-tulip-4.8 -lgzstream-tulip-4.8 -lfreetype
LIBS += -L/usr/local/lib -lcurl -lcurlpp -lfreetype
INCLUDEPATH +=/usr/local/include/ /usr/include/

#LIBS += -L/home/joris/Applications/tulip_dir/debug-4.8/install/lib -ltulip-core-4.8  -lcurl -lcurlpp  -lyajl-tulip-4.8 -lgzstream-tulip-4.8 -lfreetype
#INCLUDEPATH +=/home/joris/Applications/tulip_dir/debug-4.8/install/include/

INCLUDEPATH += ./ \
 /usr/include/freetype2 \
 ../include/ \
 ../thirdparty/freetype-gl/ \
 ../thirdparty/

QMAKE_CXXFLAGS+=  -fopenmp -std=c++1y -march=native -mtune=native
QMAKE_CFLAGS+= #-Wno-sign-compare -Wno-unused-parameter -Wno-unused-but-set-variable
QMAKE_CXXFLAGS_DEBUG+= -g -O0 -DWEBGL
QMAKE_CXXFLAGS_RELEASE+= -O1 -DNDEBUG -DWEBGL -fpermissive -Wno-reorder -Wno-sign-compare # -Wno-return-type -Wno-unused-parameter

OTHER_FILES += \
    ../fonts/Vera.ttf \
    ../shaders/ \
    ../shaders/node_shader.glsl \
    #../shaders/text_shaders.glsl \
    ../shaders/text_shaders2.glsl \
    ../shaders/mark_shader.glsl \
    ../shaders/arrow_shader.glsl \
    ../shaders/edge_shader2.glsl \
    ../shaders/edge_shader.glsl \
    ../shaders/frequency_shader.glsl
