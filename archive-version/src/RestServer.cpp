#include <iostream>
#include <sstream>
#include "RestServer.h"

RestServer::RestServer(const std::string &hostname, const short port, const std::string &baseURL)
    : hostname(hostname), port(port), baseURL(baseURL)
{

}

void RestServer::sendRequest(const std::string &requestPath, std::function<void (std::string)> callback)
{
    std::stringstream sstream;
    //sstream << "--socks5-hostname localhost:9999 " << "http://" << hostname << ":" << port << baseURL << requestPath;
    sstream << "http://" << hostname << ":" << port << baseURL << requestPath;
    HttpRequest* req = HttpRequest::makeRequest(sstream.str(), callback);
    requests.push_back(req);
}

bool RestServer::performRequests()
{
    bool remains = false;
    for (unsigned i=0 ; i<requests.size() ; ++i)
        remains |= requests[i]->perform();
    std::vector<HttpRequest*>::iterator i=requests.begin();
    while(i != requests.end()) {
        if ((*i)->isFinished()) {
            delete *i;
            requests.erase(i);
        }
        else ++i;
    }
    return remains;
}

void RestServer::clearWaitingRequests()
{

}
RestServer::~RestServer(){
    std::vector<HttpRequest*>::iterator i=requests.begin();
    while(i != requests.end()) {
            delete *i;
            requests.erase(i);

    }
}
