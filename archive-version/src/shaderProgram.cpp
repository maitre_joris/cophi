#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>

#include <shaderProgram.h>

using namespace std;

ShaderProgram::ShaderProgram(std::string filename):_name(filename) {
    program = glCreateProgram();
    std::ifstream in(filename.c_str());
    std::stringstream vertex;
    std::string line;
    while (!in.eof() && std::getline(in, line)) {
        if (line.compare("/*_fragment_*/") == 0) break;
        vertex << line << std::endl;
    }

    std::stringstream fragment;
    while (!in.eof() && std::getline(in, line)) {
        if (line.compare("/*_end_*/") == 0) break;
        fragment << line << std::endl;
    }
    glAttachShader(program, createShader(vertex.str().c_str()  , GL_VERTEX_SHADER));
    glAttachShader(program, createShader(fragment.str().c_str(), GL_FRAGMENT_SHADER));
    glLinkProgram(program);
    char msg[5120];
    glGetProgramInfoLog(program, sizeof msg, NULL, msg);
    if (strlen(msg) > 0) {
        std::cerr << "********************ProgInfoLog************************" << std::endl;
        std::cerr << "Shader : " << _name << std::endl;
        std::cerr << "Program info: " <<  std::endl << msg << std::endl;
        std::cerr << "********************************************" << std::endl;
//        std::cerr << GL_INFO_LOG_LENGTH << endl;
//        std::cerr << glGetError() << endl;
    }
}

GLuint ShaderProgram::createShader(const char source[], int type) {
    char msg[5120];
    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, (const GLchar**)(&source), NULL);
    glCompileShader(shader);
    glGetShaderInfoLog(shader, sizeof msg, NULL, msg);
    if (strlen(msg) > 0) {
        std::cerr << "******************ShaderInfoLog**************************" << std::endl;
        if (type ==  GL_VERTEX_SHADER)
            std::cerr<< " Vertex ";
        else
            std::cerr << " Fragment ";
        std::cerr << "shader : " << _name << std::endl;
        std::cerr << "Program info: " <<  std::endl << msg << std::endl;
        std::cerr << "********************************************" << std::endl;
    }
    return shader;
}


