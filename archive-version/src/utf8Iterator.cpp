#include "utf8Iterator.h"

const unsigned char firstBit = 128;
const unsigned char secondBit = 64;
const unsigned char thirdBit = 32;
const unsigned char fourthBit = 16;

/**
 * @brief Computes the number of bytes based on the first utf-8 encoded byte
 * @param currentChar The first byte of the multi-byte character
 * @return The number of bytes contained in the multi-byte character
 */
static std::string::difference_type getNbBytes(char currentChar) {
    std::string::difference_type offset = 1;
    if ((currentChar & firstBit) && (currentChar & secondBit)) {
        if (currentChar & thirdBit) {
            if (currentChar & fourthBit)
                offset = 4;
            else
                offset = 3;
        }
        else
            offset = 2;
    }
    return offset;
}

/**
 * @brief Returns the approriate mask to get significant bits from the first byte, based on total number of bytes
 * @param bytes The total number of bytes in the multi-byte character
 * @return The mask for the first byte
 */
static unsigned char getMask(int bytes) {
    switch (bytes) {
    case 2:
        return 0x1f;
    case 3:
        return 0x0f;
    case 4:
        return 0x07;
    default:
        return 0xff;
    }
}


Utf8Iterator::Utf8Iterator(const std::string s):
    utf8String(s) {
    iter = utf8String.begin();
}

bool Utf8Iterator::hasNext() {
    return (iter < utf8String.end());
}

wchar_t Utf8Iterator::next() {
    int nbBytes = getNbBytes(*iter);

    if (nbBytes == 1) {
        return *(iter++);
    }

    wchar_t charcode = *iter & getMask(nbBytes);
    for (int i=0 ; i<nbBytes-1 ; ++i) {
        charcode = charcode << 6;
        charcode += *(++iter) & 0x3f;
    }

    //moving to next charcode
    ++iter;

    return charcode;
}


