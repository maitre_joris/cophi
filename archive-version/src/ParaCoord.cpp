/*
  Copyright 2015 Timothée Jourde <itim.lcf@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#define GL_GLEXT_PROTOTYPES
#define EGL_EGLEXT_PROTOTYPES

//#ifndef WEBGL
//#include <omp.h>
//#endif

extern "C" {
//#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glut.h>
}

#include <utils.h>
#include <Data.h>
#include <Renderer.h>
#include <sstream>
//#include <tulip/TlpTools.h>
//#include <tulip/PropertyTypes.h>
#include "Color.h"
#include <iostream>


using namespace std;


enum state { DEFAULT,
             NONE,
             INVERT,
             A_POSITION,
             A_SIZE,
             A_NODE_WIDTH,
             A_EDGE_WIDTH,
             A_GAP,
             PICKER,
             DELDIM,
             INSDIM,
             A_CURVATURE,
             SWITCH_DIM
           };


void capture();
void mode(state x);

void onReshape(int w, int h);
void onDraw(void);
void onIdle(void);
void onTimer(int call);
void onKeyDown(const unsigned char key, const int , const int );
void onKeyUp(const unsigned char key, const int , const int );
void onMouseButton(int button, int state, int x, int y);
void onMouseMove(int x, int y);
void onActiveMouseMove(int x, int y);
void onPassiveMouseMove(int x, int y);


bool outOfScreen(int x, int y);
float screenToModel();
void loadFile(const string & filename);


////////////////
// ATTRIBUTES //
////////////////

const float ALTER_SENSITIVITY = 0.8;
const int   MIN_MOVE = 20;


//// VERSION NON VIRTUALISEE
//const string hostname = "147.210.129.73";
//const string baseurl = "/ParaCoordService/ParaCoordClusterSwapEdgeSubGraphsServlet/";
//const string baseurl = "/ParaCoordService/ParaCoordClusterSwapServlet/";
//int port = 8043;
//string filename = "ParaCoordKMeans_uscomma.csv_CompletWithDist3";
//string filename = "ParaCoordKMeans_relation-network-directed-clean.csv_CompletWithDist2";
//string filename = "ParaCoordKMeans_cars2.csv_CompletMoreClustersTest2";
//string filename = "ParaCoordKMeans_relation-network-directed-clean.csv_CompletSorted";
//string filename = "ParaCoordKMeans_uscomma.csv_CompletSorted";
//string filename = "ParaCoordKMeans_cars2.csv_CompletMoreClustersTest4";//"paracoordkmeans_higgs_with_labels_hybrid";

//http://speeddata:8080/ParaCoordService/ParaCoordClusterHybridServlet/paracoordkmeans_vac_hybrid
/*
const string hostname = "speeddata.labri.fr";
int port = 8080;
const string baseurl ="/ParaCoordService/ParaCoordClusterHybridServlet/";
////string filename = "paracoordkmeans_higgs_with_labels_hybrid";
//string filename = "paracoordkmeans_cars2_hybrid3";
string filename = "paracoordkmeans_vac_hybrid";
*/

/// VIRTUALISEE


const string hostname = "147.210.13.82";
int port = 8080;
const string baseurl ="/ParaCoordService/ParaCoordClusterHybridServlet/";
string filename = "paracoordkmeans_higgs_with_labels_hybrid";

//string filename = "paracoordkmeans_cars2_hybrid";




Renderer* Render;
Data *DataSource;
Data * movingData = nullptr;
Data * DataInsSource = nullptr;
state     State = DEFAULT;

int Width, Height;

float FromWidth, FromHeight, FromGap, FromEdge, FromNodeWidth, FromCurvature;

int X, bx, Y, by;
int Sid, backUpDim;
int newDim = -1;
int MovingDimensionAnimationStage = 0;
bool animatingDeletion = false, animatingInsertion = false;
unsigned int w = 800, h=800;
int FromX, FromY;
int ActiveMouseButton = -1;
//RestServer *servlet;
vector<double> infos;

vector<int> dims;
int myDim, tempDim, prevDim, nextDim, DBDim;
bool mLeft, finishedAnimation =false ;

///////////
// MODES //
///////////

void capture()
{
    FromX = X;
    FromY = Y;

    FromHeight = Render->GetHeight();
    FromWidth  = Render->GetTotalWidth();
    FromGap    = Render->GetGap();
    FromEdge    = Render->GetEdgeRed();
    FromNodeWidth = Render->GetTotalNodeWidth();
    FromCurvature = Render->GetCurvature();
}
void mode(state s)
{
    if(State == s and State == SWITCH_DIM)
        return;

    State = s;
    capture();
}

void changeDataset(string path = filename ){//= "ParaCoordKMeans_cars2.csv_CompletMoreClustersTest"

    delete Render;
    DataSource->Clear();
    delete DataSource;
    string dimsList = "?dimensions=";
    for (unsigned int i =0; i<dims.size ();++i){
        if (dims[i]!=-1){
            dimsList = string(dimsList) + to_string(dims[i]);
            dimsList = string(dimsList) + string(",");
        }
    }
    dimsList = dimsList.substr(0, dimsList.size()-1);
    DataSource = new Data();
    Render = new Renderer();
    DataSource->SetObserver(*Render);
    DataSource->SetRemoteDataSet(hostname, port, baseurl, string(path),string(dimsList));
    Render->ChangeViewport( Vec4i(0,0,Width,Height) );
    //    Render->setLabelsGenerated (false);
    Render->UpdateLabels();
    filename=path;
    //    glutPostRedisplay();
}
//bool first = true;

void deleteDimension(){
    if(Render->GetDataTmpSource() != nullptr) return;
    cout << "enter dimension to remove id: " << endl;
    int rep;
    cin >>rep;
    Data * DataTmpSource = DataSource->GetDeletionSource(rep);
    Render->SetDeletionDataSource (DataTmpSource,rep );
    return;
}

void deleteDimension(int ddim){
    if(Render->GetDataTmpSource() != nullptr) return;
    Data * DataTmpSource = DataSource->GetDeletionSource(ddim);
    Render->SetDeletionDataSource (DataTmpSource,ddim);
    return;
}
void insertDimension(){
    if(Render->GetDataTmpSource() != nullptr) return;
    cout << "enter dimension (db id) to insert id: " << endl;
    int rep;
    cin >>rep;

    cout << "where to insert id (client) " << endl;
    int prevD, nextD;
    cin >> prevD >> nextD;
    if (nextD==prevD+1){
        Data * DataTmpSource = DataSource->GetInsertionSource(rep, prevD, nextD);
        Render->SetInsertionDataSource (DataTmpSource,rep,prevD, nextD );
        animatingInsertion = true;
    }else cerr << "erreur dans les dimensions cibles ! " << endl;
    return;
}


void insertDim(int rep, int prevD = -1, int nextD = 0){
Data * DataTmpSource = DataSource->GetInsertionSource(rep, prevD, nextD);
Render->SetInsertionDataSource (DataTmpSource,rep,prevD, nextD );
animatingInsertion = true;
return;
}
void insertDimension(int ddim, int pdim = -1, int ndim = 0){
    //  if(Render->GetDataTmpSource() != nullptr) return;
    DataInsSource = DataSource->GetInsertionSource(ddim, pdim, ndim);
    if (MovingDimensionAnimationStage != 1)
        Render->SetInsertionDataSource (DataInsSource,ddim,pdim, ndim );

    return;
}
//////////////
// BINDINGS //
//////////////

extern "C" {
void c_loadDistantData(char hostname[], int port, char baseurl[], char path[]) {
    DataSource->SetRemoteDataSet(string(hostname), port, string(baseurl), string(path));

    stringstream ss;
    ss << path;
    ss >> filename;
    delete Render;
    Render = new Renderer();
    delete DataSource;
    DataSource = new Data();
    DataSource->SetObserver(*Render);
    DataSource->SetRemoteDataSet(hostname, port, baseurl, filename);
    Render->ChangeViewport( Vec4i(0,0,Width,Height) );
}

void c_loadDistantDataPath(char path[]) {
    stringstream ss;
    ss << path;
    ss >> filename;
    delete Render;
    DataSource->Clear();
    delete DataSource;
    DataSource = new Data();
    Render = new Renderer();
    DataSource->SetObserver(*Render);
    DataSource->SetRemoteDataSet(hostname, port, baseurl, filename);
    Render->UpdateLabels();
    Render->ChangeViewport( Vec4i(0,0,Width,Height) );
}

void c_center() {
    Render->Center();
}

void c_fit() {
    Render->Fit();
}

void c_defaultMode() {
    mode(DEFAULT);
}
void c_invertMode() {
    mode(INVERT);
}

void c_noneMode() {
    mode(NONE);
}

void c_switchDimMode() {
    mode(SWITCH_DIM);
}

void c_delMode(){
   mode(DELDIM);
}

void c_insMode(){
   mode(INSDIM);
   cout<<"je passe en mode insertion" << endl;
}

void c_updateNewDim( float x){
   newDim = floor(x+0.5);
}

float* c_infoMode() {
    mode(PICKER);
    infos.shrink_to_fit();
    float arr[infos.size()];
    copy(infos.begin(),infos.end(),arr);
    return arr;
}

int c_get_nbDims(){
    return DataSource->GetDimensions ().size ();//GetLabels ().size ();
}

char* c_getDims(int x) {
    return  const_cast<char*>(DataSource->GetDimensionProp (x).label.c_str());//GetLabels ()[x].c_str());
}
void c_histogramView() {
    Render->HistogramView();
    //    glutPostRedisplay();
}
void c_distortionView() {
    Render->DistortionView();
    //    glutPostRedisplay();
}

void c_setGap(float x) {
    Render->SetGap(x);
    //    glutPostRedisplay();
}
void c_setNodeWidth(float x) {
    Render->SetNodeWidth(x/2);
    //    glutPostRedisplay();
}
void c_setEdgeWidth(float x) {
    Render->SetEdgeRed(x/2);
    //    glutPostRedisplay();
}
void c_setWidth(float x) {
    Render->SetWidth(x*3);
    //    glutPostRedisplay();
}
void c_setCurvature(float x) {
    Render->SetCurvature(x);
    //    glutPostRedisplay();
}

void c_changeMapping() {
    Render->changeMapping(glutGet(GLUT_ELAPSED_TIME) /(float) 1000);
    //    glutPostRedisplay();
}
void c_displayLabels() {
    if (Render->getDisplayLabels())
        Render->HideLabels();
    else
        Render->ShowLabels();
    //    glutPostRedisplay();
}
void c_setHeight(float x) {
    Render->SetHeight(x*4);
    //    glutPostRedisplay();
}

int c_dataLoaded(){
    return DataSource->GetNbIndividuals();
}

void c_setFilename(char *x){
    filename = x;
    stringstream ss;
    ss << x;
    ss >> filename;
}
void c_changeDimension(int d){

    if (DataSource->GetDB2ClientDimNumber ().find(d)!=DataSource->GetDB2ClientDimNumber ().end()){
        deleteDimension (DataSource->GetDB2ClientDimNumber (d));
    }else
        insertDimension (d);
}

void c_setInvertionDuration(float d){
    Render->SetInvertionDuration (d);
}
void c_setInsertionDuration(float d){
    Render->SetInsertionDuration (d);
}
void c_setDeletionDuration(float d){
    Render->SetDeletionDuration (d);
}

}

//////////////////////
// EVENTS CALLBACKS //
//////////////////////
void displayLabels(){
    if (Render->getDisplayLabels())
        Render->HideLabels();
    else
        Render->ShowLabels();
}


void onReshape(int w, int h)
{
    Width  = w;
    Height = h;
    Render->ChangeViewport( Vec4i(0,0,w,h) );
}


void onDraw(void)
{
    Render->Draw();
    glutSwapBuffers();
}


void onIdle(void){
    if(Render->GetDataTmpSource() != nullptr && Render->GetDataTmpSource()->GetDimensions().size() == 0)
        Render->GetDataTmpSource()->PerformRequests ();
    if(DataInsSource!= nullptr && DataInsSource->GetDimensions().size() == 0)
        DataInsSource->PerformRequests();


    //finishedAnimation = Render->Tick(glutGet(GLUT_ELAPSED_TIME) / (float) 1000);
    if(animatingDeletion){
        if(Render->GetDataTmpSource() != nullptr && Render->GetDataTmpSource()->GetDimensions().size() != 0 && !Render->IsAnimatingDeletion()){
            Render->HideLabels();
            Render->HideMarksAndArrows();
            Render->AnimateDeletion (glutGet(GLUT_ELAPSED_TIME) /1000.f);
        }else {
            animatingDeletion = !(Render->Tick(glutGet(GLUT_ELAPSED_TIME) / (float) 1000));
            if(!animatingDeletion)   {
                Render->ShowLabels();
                Render->UpdateLabels();
                Render->ShowMarksAndArrows();
            }
        }

    }
    else if(animatingInsertion){
        if(!Render->IsAnimatingInsertion() && Render->GetDataTmpSource() != nullptr && Render->GetDataTmpSource()->GetDimensions().size() != 0){
            Render->HideLabels();
            Render->HideMarksAndArrows();
            Render->AnimateInsertion(glutGet(GLUT_ELAPSED_TIME) /1000.f);
        }else {
            animatingInsertion = !(Render->Tick(glutGet(GLUT_ELAPSED_TIME) / (float) 1000));
            if(!animatingInsertion)   {
                Render->ShowLabels();
                Render->UpdateLabels();
                Render->ShowMarksAndArrows();
            }
        }

    }
    //we are switching dimensions
    else if(MovingDimensionAnimationStage != 0){
        if(DataInsSource!= nullptr && DataInsSource->GetDimensions().size() == 0){
            DataInsSource->PerformRequests();
        }

        if (MovingDimensionAnimationStage==1){
            if(Render->GetDataTmpSource() != nullptr && Render->GetDataTmpSource()->GetDimensions().size() != 0 && !Render->IsAnimatingDeletion() && !Render->IsAnimatingInsertion()){
                Render->HideLabels();
                Render->HideMarksAndArrows();
                Render->AnimateDeletion (glutGet(GLUT_ELAPSED_TIME) /1000.f);
            }
            else if(Render->GetDataTmpSource() != nullptr && Render->GetDataTmpSource()->GetDimensions().size() != 0 && Render->IsAnimatingDeletion())
            {
                finishedAnimation = Render->Tick(glutGet(GLUT_ELAPSED_TIME) / (float) 1000);
                if(finishedAnimation) {
                    if (DataInsSource!= nullptr && DataInsSource->GetDimensions ().size ()!=0){
                        if (myDim<prevDim){
                            Render->SetInsertionDataSource (DataInsSource,DBDim,prevDim-1, prevDim);
                        }else{
                            Render->SetInsertionDataSource (DataInsSource,DBDim,prevDim, nextDim);
                        }
                    }
                    MovingDimensionAnimationStage = 2;
                    finishedAnimation = false;
                }
            }
        }
        else if (MovingDimensionAnimationStage == 2 && Render->GetDataTmpSource() != nullptr && Render->GetDataTmpSource()->GetDimensions().size() != 0){
            if(!Render->IsAnimatingInsertion()){
                Render->AnimateInsertion(glutGet(GLUT_ELAPSED_TIME) / (float) 1000);
            }
            else {
                finishedAnimation = Render->Tick(glutGet(GLUT_ELAPSED_TIME) / (float) 1000);

                if(finishedAnimation){
                    MovingDimensionAnimationStage = 0;
                    DBDim = tempDim = -1;
                    DataInsSource = nullptr;
                    finishedAnimation = false;
                    Render->ShowLabels ();
                    Render->UpdateLabels();
                    Render->ShowMarksAndArrows();
                }
            }
        }
    }
    else Render->Tick(glutGet(GLUT_ELAPSED_TIME) / (float) 1000);


    glutPostRedisplay();
    DataSource->PerformRequests();
}
void setColorGradient(){
    cout << "enter new values at format r g b r g b r g b (the 3 last value can be -1 -1 -1 for a colorgradient with two values)" << endl;
    int r,g,b, r1,g1,b1,r2,g2,b2 = -1;
    cin >> r >> g >> b >> r1 >> g1 >> b1 >> r2 >> g2 >> b2 ;
    vector<Color> colors;
    colors.push_back (Color(r,g,b));
    colors.push_back (Color(r1,g1,b1));
    if (r2 !=-1)
        colors.push_back (Color(r2,g2,b2));
    Render->setColorGradient(colors);
}


void onKeyDown(const unsigned char key, const int , const int )
{
    switch(int(key))
    {
    case 's': mode(A_SIZE);              break;
    case 'n': mode(A_NODE_WIDTH);        break;
    case 'r': mode(A_EDGE_WIDTH);        break;
    case 'g': mode(A_GAP);               break;
    case 'v': mode(A_CURVATURE);         break;
    case 'i': mode(INVERT);              break;
    case ' ': mode(DEFAULT);             break;
    case 'd': mode(SWITCH_DIM);          break;
    case 'p': mode(PICKER);              break;
    case 't': mode(DELDIM);              break;
    case 'w': mode(INSDIM);              break;
    case 'T': deleteDimension();         break;
    case 'a': insertDimension(backUpDim);break;
    case 'A': insertDimension();         break;
    case ':': setColorGradient();        break;
    case 'l': displayLabels();           break;
    case 'c': Render->Center();          break;
    case 'f': Render->Fit();             break;
    case 'h': Render->ToogleHistogram(); break;
    case 'm': Render->changeMapping(glutGet(GLUT_ELAPSED_TIME) /(float) 1000);/*glutPostRedisplay();*/break;
    case 'q':
        exit(EXIT_SUCCESS);
        break;
    default:;
    }
}

void onKeyUp(const unsigned char key, const int , const int )
{
    if(State == SWITCH_DIM || State == PICKER || State == INSDIM) return;
    if (State == DELDIM) return;
    mode(DEFAULT);
}

void onMouseButton(int button, int state, int x, int y)
{
    if(Render->GetDataTmpSource() != nullptr) return;
    if (button == GLUT_LEFT_BUTTON)
    {
        if (state == GLUT_DOWN)
        {
            capture();
            ActiveMouseButton = GLUT_LEFT_BUTTON;
            if (State == INVERT){
                Render->InvertAt(x,y, glutGet(GLUT_ELAPSED_TIME) /(float) 1000);
            }if (State == SWITCH_DIM){
                if(Render->GetDataTmpSource() != nullptr) return;
                Sid = Render->getDimension (x,y);
                myDim = Sid;
                if(Sid == -1) return;
                movingData = new Data(DataSource, myDim);
                Render->setDataMovingSource (movingData);
                Render->EndSelect ();
            }
        }
        else if (state == GLUT_UP)
        {
            ActiveMouseButton = -1;
            if (State == DEFAULT)
            {
                if (not Render->Select(x,y))
                    Render->EndSelect();
            }
            else if (State == A_POSITION)
                mode(DEFAULT);

            else if (State == DELDIM){
                int dim = Render->getDimension (x,y);
                deleteDimension (dim);
                backUpDim = DataSource->GetDB2ClientDimNumber ()[dim];
                animatingDeletion = true;
                //                mode(DEFAULT);
            }
            else if(State == SWITCH_DIM){
                if(myDim == -1) return;
                Render->setDataMovingSource (nullptr);
                movingData = nullptr;
                tuple<int,int>PNDims = Render->getSurroundingDimension (x,y);
                prevDim = get<0>(PNDims);
                nextDim = get<1>(PNDims);
                cout << myDim<<  " " << nextDim << " "<< prevDim << endl;
                if(myDim==prevDim || myDim == nextDim) {
                    myDim = -1;
                    return;
                }
                MovingDimensionAnimationStage = 1;
                DBDim = DataSource->GetClient2DBDimNumber ()[myDim];
                deleteDimension (myDim);
                insertDimension(DBDim,prevDim,nextDim);
                //                mode(DEFAULT);
            }
            else if (State == INSDIM){
                if (newDim!=-1){
                    tuple<int,int>PNDims = Render->getSurroundingDimension (x,y);
                    prevDim = get<0>(PNDims);
                    nextDim = get<1>(PNDims);

                    insertDimension(newDim,prevDim,nextDim);
                    newDim = -1;
                }
            }
            else if(State == PICKER){
                Sid = Render->getId(x,y);
                if (Sid>=0 && Sid<DataSource->GetRootGraph().numberOfNodes()){

                    infos.clear();

                    infos.push_back(Sid);
                    infos.push_back(DataSource->GetNodeProp()[Sid].min);
                    infos.push_back(DataSource->GetNodeProp()[Sid].max);
                    infos.push_back(DataSource->GetNodeProp()[Sid].weight);
                }else{
                    Sid=-1;
                    cout<<"Error not a Node: "<<Sid<<endl;
                    infos.clear();
                    infos.push_back(Sid);
                }
            }
            Render->ShowLabels();
            Render->LabelsOcclusion();
        }
    }
    else if (button == GLUT_RIGHT_BUTTON)
    {
        if (state == GLUT_DOWN)
        {
            ActiveMouseButton = GLUT_RIGHT_BUTTON;
        }
        else if (state == GLUT_UP)
        {
            ActiveMouseButton = -1;
            Render->Select(x,y,false,true);
        }
    }
    else if (button == 3)
        Render->Zoom(x, y, 1.2);
    else if (button == 4)
        Render->Zoom(x, y, 0.8);
    Render->LabelsOcclusion();
    //    glutPostRedisplay();
}

void onMouseMove(int x, int y)
{
    X = x;
    Y = y;
}

void onActiveMouseMove(int x, int y)
{
    if(Render->GetDataTmpSource() != nullptr) return;
    bx = X, by = Y;
    onMouseMove(x,y);
    const float d = Render->WindowToModelFactor();
    const float dx = x-FromX;
    const float dy = FromY-y;

    const float mdx = d*dx;
    const float mdy = d*dy;
    switch (State)
    {
    case DEFAULT:
        break;

    case A_POSITION:
        Render->Translate(dx, dy);
        FromX = x;
        FromY = y;
        break;

    case A_SIZE:
        Render->HideLabels();
        Render->SetTotalWidth ( FromWidth  + mdx );
        Render->SetHeight( FromHeight + mdy );
        cout << Render->GetTotalNodeWidth () / Render->GetHeight () << endl;
        break;

    case A_NODE_WIDTH:
        Render->HideLabels();
        Render->SetTotalNodeWidth ( FromNodeWidth + mdx );
        break;
    case A_EDGE_WIDTH:
        Render->HideLabels();
        Render->SetEdgeReduction( FromEdge + (dy /(float) Height) * ALTER_SENSITIVITY );
        break;
    case SWITCH_DIM:
        if(myDim!= -1)
            Render->updateMovingDimPosition(x, y);
        //        glutPostRedisplay();
        break;
    case A_GAP:
        Render->HideLabels();
        Render->SetGap ( FromGap + (dy /(float) Height) * ALTER_SENSITIVITY );
        break;

    case A_CURVATURE:
        Render->SetCurvature ( FromCurvature + (dy /(float) Height) * ALTER_SENSITIVITY );
        break;

    default:;
    }

    if ( (abs(dx) > MIN_MOVE or abs(dy) > MIN_MOVE) and State == DEFAULT and ActiveMouseButton == GLUT_LEFT_BUTTON )
        mode(A_POSITION);
    //glutPostRedisplay();
}
void onPassiveMouseMove(int x, int y)
{
    onMouseMove(x,y);
            if (newDim!=-1){
                cout << "je passe ici"<<endl;
                tuple<int,int>PNDims = Render->getSurroundingDimension (x,y);
                prevDim = get<0>(PNDims);
                nextDim = get<1>(PNDims);
                insertDim(newDim,prevDim,nextDim);
                newDim = -1;

        }
}


//////////////////////
// UTILS & loadFile //
//////////////////////

bool outOfScreen(int x, int y)
{
    return (x<0 || x>Width || y<0 || y>Height);
}

int main(int argc, char *argv[]) {
    //#ifndef EMSCRIPTEN
    //    initTulipLib("/usr/local/lib"); // ??
    //#else
    //#ifdef EMSCRIPTEN
    //    initTypeSerializers();
    //#endif

    //#ifndef EMSCRIPTEN
    //    omp_set_num_threads(omp_get_num_procs());
    //#endif
    if (argc>2){
        w=atoi(argv[1]);
        h=atoi(argv[2]);
    }
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH | GLUT_STENCIL | GLUT_MULTISAMPLE);
    glutInitWindowSize(w, h);
    if ((glutCreateWindow("Tulip Glut Viewer")) == GL_FALSE) {
        cerr << "Unable to create a OpenGl Glut window" << endl;
        exit(EXIT_FAILURE);
    }

    /* Set up glut callback functions */
    glutIdleFunc         ( onIdle      );
    //glutTimerFunc  (0, glut_timer_callback, 0);
    glutReshapeFunc      ( onReshape   );
    glutDisplayFunc      ( onDraw      );
    glutKeyboardFunc     ( onKeyDown   );
    glutKeyboardUpFunc   ( onKeyUp     );
    glutMouseFunc        ( onMouseButton     );
    glutMotionFunc       ( onActiveMouseMove  );
    glutPassiveMotionFunc( onPassiveMouseMove );

    /* Attributes initialization */
    Width  = glutGet(GLUT_WINDOW_WIDTH);
    Height = glutGet(GLUT_WINDOW_HEIGHT);
    DataSource = new Data();
    Render = new Renderer();
    Render->ChangeViewport( Vec4i(0,0,Width,Height) );
    DataSource->SetObserver(*Render);
    string dim = "?dimensions=0,1";
    DataSource->SetRemoteDataSet(hostname,port,baseurl,filename);

    glutMainLoop();

    DataSource->Clear();
    delete DataSource;
    delete Render;
    return EXIT_SUCCESS;
}
