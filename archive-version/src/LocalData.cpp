/*
  Copyright 2015 Romain Bourqui  <romain.bourqui@labri.fr>
  Copyright 2015 Timothée Jourde <itim.lcf@gmail.com>
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// THIS CLASS IS NOT USED ANYMORE.
#include <LocalData.h>
#include <utils.h>

#include <tulip/Graph.h>
#include <tulip/ForEach.h>
#include <tulip/SimpleTest.h>
#include <tulip/ColorProperty.h>
#include <tulip/ColorScale.h>
#include <tulip/TulipPluginHeaders.h>

//#include <time.h>
#include <math.h>
#include <climits>
#include <fstream>
#include <sstream>

using namespace std;

void OldData::Clear()
{
   Characteristics.clear();
   Individuals.clear();
   
   if (Graph != nullptr)
   {
      delete Graph;
      Graph = nullptr;
   }

   NbIndividuals = 0;
}

void OldData::FreeWorkingMemory()
{
   Individuals.clear();

   for (characteristic & c : Characteristics)
      for (auto & v : c.values)
      {
         v.second.individuals.clear();
         v.second.individuals.shrink_to_fit();
      }
}

void OldData::SetGraph(tlp::Graph * g)
{ 
   if (Graph != nullptr)
      delete Graph;
   
   Graph = g;

   LayoutProp = g->getProperty<tlp::LayoutProperty>("viewLayout");
   SizeProp   = g->getProperty<tlp::SizeProperty>("viewSize");
   LabelProp  = g->getProperty<tlp::StringProperty>("viewLabel");
   WeightProp = g->getProperty<tlp::IntegerProperty>("weight");
   ValueProp      = g->getProperty<tlp::IntegerProperty>("value");
   SubGraphIdProp = g->getProperty<tlp::IntegerProperty>("subGraph");
   ColorProp      = g->getProperty<tlp::ColorProperty>("viewColor");

   GapProp          = g->getProperty<tlp::DoubleProperty>("gapBefore");
   WeightBeforeProp = g->getProperty<tlp::DoubleProperty>("weightBefore");
   NormWeightProp   = g->getProperty<tlp::DoubleProperty>("normWeight");

   SourcePosProp = g->getProperty<tlp::DoubleProperty>("sourcePos");
   DestPosProp   = g->getProperty<tlp::DoubleProperty>("destPos");
}

void OldData::InitProp()
{
   WeightProp->setAllEdgeValue(0);
   WeightProp->setAllNodeValue(0);
   SubGraphIdProp->setAllNodeValue(-1);
}


void OldData::ProcessCSV(const std::string & filename, const std::string & delimiters)
{
   Clear();
   SetGraph( tlp::newGraph() );
   InitProp();

   std::ifstream in(filename, std::ifstream::ate | std::ifstream::binary);

    if (not in.is_open())
        throw "unable to open file " + filename;

    const std::ifstream::pos_type size = in.tellg();
    in.seekg(0);

   std::string line;
   std::getline(in, line);

   vector<string> tokens;
   tokenize(line, tokens, delimiters);

   Characteristics.resize( tokens.size() );
   const int k = 50000000 / (tokens.size() * sizeof(int));
   Individuals.set_chunk_size( tokens.size() * std::max(k,1) ); // on alloue par bloc de ~50Mo
                                                                // et on s'assure que la taille des blocs
                                                                // soit un multiple du nb de dimensions
                                                                // pour ne pas perdre de mémoire
   
   for(unsigned i = 0; i < tokens.size(); i++)
      Characteristics[i].name = tokens[i];

   NbIndividuals = 0;

   Progress progress("Importing nodes and edges...");
   std::ifstream::pos_type readBytes = 0;

   while (! in.eof())
   {
      tokens.clear();

      std::getline(in, line);
      readBytes += line.size() + 1;
      
      if (line.size() == 0)
         continue;

      tokenize(line, tokens, delimiters);
      if(tokens.size() != Characteristics.size())
         continue;
      
      int * individual = Individuals.add( Characteristics.size() );
      for(unsigned i = 0; i < tokens.size(); i++)
         individual[i] = atoi( tokens[i].c_str() );
      
      ProcessIndividual( individual );

      progress.update(readBytes /(float) size);
      NbIndividuals++;
   }
   progress.update(1);
   
   
   PositionNodes();
   PositionEdges();
   ApplyColorGradient();
   
   ComputeSubGraphs();
   FreeWorkingMemory();
}



void OldData::ProcessIndividual(int individual[])
{
   tlp::node prev;
   for(unsigned i = 0; i < Characteristics.size(); i++)
   {
      auto & values = Characteristics[i].values;
      const int v = individual[i];

      if(values.find(v) == values.end())
         values[v].node = Graph->addNode();

      value & val = values[v];

      val.individuals.push_back(individual);
      ValueProp->setNodeValue(val.node, v);

      if(prev.isValid())
      {
         tlp::edge e = Graph->existEdge(prev, val.node);
         if(!e.isValid())
            e = Graph->addEdge(prev, val.node);

         WeightProp->setEdgeValue(e, WeightProp->getEdgeValue(e)+1);
      }
      prev = val.node;
   }
}



void OldData::PositionNodes()
{
   // on calcule le poid de chaque sommet
   Progress progress("Computing the weight of each node...");
   unsigned nodeI = 0;
   tlp::node n;
   forEach(n, Graph->getNodes())
   {
      float val = 0;
      tlp::edge e;
      forEach(e, Graph->getInEdges(n))
         val += WeightProp->getEdgeValue(e);

      WeightProp->setNodeValue(n, val);

      nodeI++;
      progress.update(nodeI/(float)Graph->numberOfNodes());
   }
   progress.update(1);

   // on positionne les sommets
   int xshift = 0;
   bool first = true;

   unsigned progressIMax = 0;
   for(characteristic & c : Characteristics)
      progressIMax += (c.values.size() * 2) - 1;
   unsigned progressI = 0;

   progress.init("Positionning nodes...");

   for(characteristic & c : Characteristics)
   {    
      auto & values = c.values;

      // on calcule la somme des écarts
      float gapSum = 0;
      int prev = values.begin()->first;
      if (values.begin() != values.end())
         for(auto it = std::next(values.begin()); it != values.end(); ++it)
         {
            gapSum += it->first - prev - 1;
            prev = it->first;

            progress.update(progressI/(float)progressIMax);
            progressI++;
         }

      c.isGap = (gapSum != 0);

      const float nodeHeightSum = Height - (Height*VerticalSpacing);

      prev = values.begin()->first;
      float yshift       = 0;
      float gapBefore    = 0;
      float weightBefore = 0;
      for(auto itn : values)
      {
         int val = itn.first;
         tlp::node n = itn.second.node;
         if(first)
         {
            // on calcules le poid de chaque sommet sur la première colonnes
            float va = 0;
            tlp::edge e;
            forEach(e, Graph->getOutEdges(n))
               va += WeightProp->getEdgeValue(e);

            WeightProp->setNodeValue(n, va);
         }

         const float normGap = (gapSum == 0 or val == prev) ? 0 : ((val-prev-1)/gapSum);         
         gapBefore += normGap;
         
         const float gap =  (VerticalSpacing*Height) * normGap;
         prev = val;
         
         const float normalizedWeight = WeightProp->getNodeValue(n) / (float) NbIndividuals;
         const float nodeHeight = normalizedWeight * nodeHeightSum;
         
         const tlp::Coord c(xshift * (HorizontalGap+NodeWidth), gap + yshift + nodeHeight/2, 0);
         const tlp::Size si(NodeWidth, nodeHeight, 1);
         
         SizeProp->setNodeValue(n, si);
         LayoutProp->setNodeValue(n, c);
         LabelProp->setNodeValue(n, std::to_string(val));
         
         NormWeightProp->setNodeValue(n, normalizedWeight);
         WeightBeforeProp->setNodeValue(n, weightBefore);
         GapProp->setNodeValue(n, gapBefore);
         
         //cout << "node: pos:" << c << " size:" << si << endl;
         
         yshift += nodeHeight + gap;
         weightBefore += normalizedWeight;
         
         progressI++;
         progress.update(progressI/(float)progressIMax);
      }

      /*
        node u = data.aggregatedGraph->addNode();
        l->setNodeValue(u, Coord(xshift * (horizontalGap+nodeWidth), -0.3, 0));
        s->setNodeValue(u, Size(nodeWidth, 0.1, 0));
        la->setNodeValue(u, p.name);
      */

      xshift++;
      first = false;
   }

   progress.update(1);
}




void OldData::PositionEdges()
{
    Progress progress("Positionning edges...");
    unsigned nodeI = 1;

    map<tlp::edge, array<tlp::Coord,4>> bends;

    tlp::node n;
    forEach(n, Graph->getNodes())
    {
        if(Graph->deg(n) == 0)
            continue;

        SortEdges(n, true,  bends);
        SortEdges(n, false, bends);

        nodeI++;
        progress.update(nodeI/(float)Graph->numberOfNodes());
    }
    progress.update(1);

    tlp::edge e;
    forEach(e, Graph->getEdges())
    {
       NormWeightProp->setEdgeValue(e, WeightProp->getEdgeValue(e) /(float) NbIndividuals);
       LayoutProp->setEdgeValue(e, { bends[e][0], bends[e][1], bends[e][2], bends[e][3] });
    }


}


void OldData::SortEdges(tlp::node node, bool outEdges, map<tlp::edge, array<tlp::Coord,4>> & bends)
{
   const unsigned bend1 = (outEdges) ?  0 :  3;
   const unsigned bend2 = (outEdges) ?  1 :  2;
   const float    sign  = (outEdges) ? +1 : -1;
   tlp::DoubleProperty * posProp = (outEdges) ? SourcePosProp : DestPosProp;

   const tlp::Coord nodePos    = LayoutProp->getNodeValue(node);
   const tlp::Coord nodeSize   = SizeProp->getNodeValue(node);
   const float      nodeWeight = WeightProp->getNodeValue(node);

   auto * edgesIt = (outEdges) ? Graph->getOutEdges(node) : Graph->getInEdges(node);

   tlp::edge e;
   vector<pair<tlp::edge,int>> edges;
   
   forEach(e, edgesIt)
      edges.push_back( pair<tlp::edge,int>(e, ValueProp->getNodeValue(Graph->opposite(e,node))) );

   sort(edges.begin(), edges.end(), comp_edge());

   const float startY = nodePos.y() - nodeSize.y()/2;
   float sum = 0;
   float normWeightSum = 0;
   for(unsigned int i = 0; i < edges.size(); ++i)
   {
      tlp::edge e = edges[i].first;
      const float normalizedWeight = WeightProp->getEdgeValue(e) / (float) nodeWeight;
      const float height = normalizedWeight * nodeSize.y();
      
      normWeightSum += normalizedWeight / 2;
      posProp->setEdgeValue(e, normWeightSum);
      normWeightSum += normalizedWeight / 2;
      
      bends[e][bend1] = tlp::Coord(nodePos.x() + sign * (nodeSize.x()/2) ,   startY + height/2. + sum, 0);
      bends[e][bend2] = tlp::Coord(nodePos.x() + sign * (HorizontalGap/2),   startY + height/2. + sum, 0);

      tlp::Size s = SizeProp->getEdgeValue(e);
      if (outEdges)
         s.setX(height);
      else
         s.setY(height);

      SizeProp->setEdgeValue(e, s);

      sum += height;
   }
}

void OldData::ComputeSubGraphs()
{
   unsigned long long progressI   = 0;
   const unsigned long long progressIMax = NbIndividuals * Characteristics.size();
   Progress progress("Computing sub graphs...");


   for (const characteristic & c : Characteristics)
      for (auto & val : c.values)
      {
         //cerr << "sub" << endl;
         
         const tlp::node node = val.second.node;
         const auto & individuals = val.second.individuals;
         
         tlp::Graph * sub = Graph->addSubGraph();
         tlp::IntegerProperty * subWeightProp     = sub->getLocalProperty<tlp::IntegerProperty>("weight");
         tlp::DoubleProperty  * subNormWeightProp = sub->getLocalProperty<tlp::DoubleProperty>("normWeight");
         
         subWeightProp->setAllNodeValue(0);
         subWeightProp->setAllEdgeValue(0);
         
         SubGraphIdProp->setNodeValue(node, sub->getId());
         
         for(const int * individual : individuals) 
         {
            int v = individual[0];
            tlp::node n = Characteristics[0].values[v].node;
            
            if (not sub->isElement(n))
               sub->addNode(n);
            
            subWeightProp->setNodeValue(n, 1 + subWeightProp->getNodeValue(n));
            
            for (unsigned j=1; j < Characteristics.size(); j++)
            {
               tlp::node prevN = n;
               
               v = individual[j];
               n = Characteristics[j].values[v].node;
               
               if (not sub->isElement(n))
                  sub->addNode(n);
               
               subWeightProp->setNodeValue(n, 1 + subWeightProp->getNodeValue(n));
               
               tlp::edge e = sub->existEdge(prevN, n, false);
               if (not e.isValid())
               {
                  e = Graph->existEdge(prevN, n, false);
                  if (not e.isValid())
                      cerr << "invalid edge !" << endl;
                  
                  sub->addEdge(e);
                  subWeightProp->setEdgeValue(e, 0);
               }
               subWeightProp->setEdgeValue(e, 1 + subWeightProp->getEdgeValue(e));
            }
            
            progressI++;
            progress.update(progressI /(float) progressIMax);
         }
         
         tlp::node n;
         forEach(n, sub->getNodes())
            subNormWeightProp->setNodeValue(n, subWeightProp->getNodeValue(n) /(float) NbIndividuals);
         
         for(tlp::edge e : sub->getEdges())
         {
            subNormWeightProp->setEdgeValue(e, subWeightProp->getEdgeValue(e) /(float) NbIndividuals);
         }
      }
   progress.update(1);
}

void OldData::ApplyColorGradient()
{
   tlp::ColorScale cs(true);
   
   for (characteristic & c : Characteristics)
      for (auto & v : c.values)
         ColorProp->setNodeValue(v.second.node, cs.getColorAtPos( WeightBeforeProp->getNodeValue(v.second.node) ));
}



void OldData::ImportRemoteRootGraph(Data & data) // dirty hack
{
   Clear();
   HackNodeMap.clear();
   HackEdgeMap.clear();
   
   SetGraph( tlp::newGraph() );
   InitProp();
   

   Characteristics.resize( data.Dimensions.size() );
   NbIndividuals = data.NbIndividuals;

   for(unsigned i=0; i<Characteristics.size(); i++)
   {
      characteristic & c = Characteristics[i];
      c.name  = data.Labels[i];
      c.isGap = data.IsGap(i);
      
      for(const auto it : data.Dimensions[i])
      {
         tlp::node rn = it.second;
         tlp::node ln = Graph->addNode();
         c.values[it.first].node  = ln;
         HackNodeMap[rn]          = ln;
         
         WeightBeforeProp->setNodeValue(ln, data.NodeProp[rn].weightBefore);
         GapProp         ->setNodeValue(ln, data.NodeProp[rn].gapBefore   );
         NormWeightProp  ->setNodeValue(ln, data.NodeProp[rn].weight /(float) NbIndividuals );
         /*
         std::cerr << "node: " << "weight=" << data.NodeProp[it.second].weight /(float) NbIndividuals
                   << " weightBefore=" << data.NodeProp[it.second].weightBefore
                   << " gapBefore=" << data.NodeProp[it.second].gapBefore
                   << endl;
         */
      }
   }

   for(const tlp::edge re : data.RootGraph.edges())
   {
      const pair<tlp::node,tlp::node> ends = data.RootGraph.ends(re);
      tlp::edge le = Graph->addEdge( HackNodeMap[ends.first], HackNodeMap[ends.second] );
      
      NormWeightProp->setEdgeValue(le, data.EdgeProp[re].weight /(float) NbIndividuals );
      SourcePosProp ->setEdgeValue(le, data.EdgeProp[re].sourcePos );
      DestPosProp   ->setEdgeValue(le, data.EdgeProp[re].destPos );

      HackEdgeMap[re] = le;
   }

   ApplyColorGradient();
}

void OldData::ImportRemoteSubGraph(int dimension, int clusterId, Data::subGraph & other) // sub dirty hack
{
   tlp::node node = Characteristics.at(dimension).values.at(clusterId).node;

   tlp::Graph* sub = Graph->addSubGraph();
   tlp::DoubleProperty * subNormWeightProp = sub->getLocalProperty<tlp::DoubleProperty>("normWeight");
   
   if ( SubGraphIdProp->getNodeValue(node) != -1 )
      return; // déjà importé

   SubGraphIdProp->setNodeValue(node, sub->getId());

   for(unsigned i=0; i<other.nodes.size(); i++)
   {
      tlp::node n = HackNodeMap[other.nodes[i]];
      sub->addNode(n);
      subNormWeightProp->setNodeValue(n, other.nodesWeight[i] /(float) NbIndividuals);
   }

   for(unsigned i=0; i<other.edges.size(); i++)
   {
      tlp::edge e = HackEdgeMap[other.edges[i]];
      sub->addEdge(e);
      subNormWeightProp->setEdgeValue(e, other.edgesWeight[i] /(float) NbIndividuals);
   }
}
