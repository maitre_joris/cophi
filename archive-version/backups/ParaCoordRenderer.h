#ifndef PARACOORDRENDERER_H
#define PARACOORDRENDERER_H
/* GL_OES_standard_derivatives */
#ifndef GL_OES_standard_derivatives
#define GL_OES_standard_derivatives 1
#endif

#include <iostream>
#include <cmath>
#include <vector>
#ifndef WEBGL
//#include <parallel/algorithm>
#endif
#include <map>
#include <unordered_map>
#include <algorithm>
#include <string.h>
#include <tulip/vectorgraph.h>
#include <tulip/Vector.h>
#include <tulip/Graph.h>
#include <tulip/TlpTools.h>
#include <tulip/DoubleProperty.h>
#include <tulip/IntegerProperty.h>
#include <tulip/ColorProperty.h>
#include <tulip/SizeProperty.h>
#include <tulip/StringProperty.h>
#include <tulip/BooleanProperty.h>
#include <tulip/LayoutProperty.h>

#include <tulip/ForEach.h>
#ifndef WEBGL
#include <omp.h>
#endif
#include <sstream>
#include <fstream>

#include <glmatrix.h>
#include <shaderProgram.h>
#include <TextRenderer.h>
#include <matrixManager.h>
#include <Data.h>

//using namespace std;
namespace tlp {

  class NodeProgram : public ShaderProgram {
  public :
    NodeProgram();
    GLuint matProjMod;
    GLuint res;
    GLuint data;

    GLuint colorPicking; // il faudrait plutôt 2 shaders (la condition est inutilement côuteuse)
                         // mais ça implique de dupliquer le code ou d'utiliser un préprocesseur
    GLuint behind;
  };

  class EdgeProgram : public ShaderProgram {
  public :
    EdgeProgram();
    GLuint matProjMod;
    GLuint res;
    GLuint data;
    GLuint colorPicking;
    GLuint behind;
    GLuint tubular;
  };

  class MarkProgram : public ShaderProgram {
  public :
    MarkProgram();
    GLuint matProjMod;
    GLuint res;
  };

  /**
   * @brief The NodeStruct structure
   */
  struct NodeStruct {   // structure that we want to transmit to our shaders - ONLY FLOATS NO INTEGERS

      Vec2f  position;  // [0].xy
      Vec2f  size;      // [0].zw

      float weight;     // [1].x
      float shape;      // [1].y
      Vec2f  _padding;  // [1].zw

      Vec4f  color;     // [2]
      Vec4f  bcolor;    // [3]

      Vec4f  idColor;   // [4]
  };
  const unsigned SIZEOFNODE = 5; // 4 vec4 (on aligne)


  struct EdgeStruct {

      // Bézier Curve parameters
      Vec2f  origin;      // [0].xy
      Vec2f  control1;    // [0].zw
      Vec2f  control2;    // [1].xy
      Vec2f  destination; // [1].zw

      Vec4f originColor;      // [2]
      Vec4f destinationColor; // [3]

      float width;           // [4].x
      Vec3f _padding;        // [4].yzw

      Vec4f  idColor;        // [5]
  };
  const unsigned SIZEOFEDGE = 6;



  class ParaCoordRenderer{

  public:
    ParaCoordRenderer(MatrixManager *mManager);
    ~ParaCoordRenderer();
    void loadGraph(Data2 &&);
//    void loadGraph(StructGenerator *sg, MatrixManager *mManager);

    Vec4f computeBBox();
    void centerGraph();
    void updateResolution();
    MatrixManager* getMatrixManager();
    void init();
    void draw(bool);
    void zoom(const Vec2f pos, const float scale);

    void stopHighlight();
    bool highlightAt(unsigned,unsigned);

  private:
    void clear();

    std::string intToChar(int a);
    void setLabels(node &n, int x, int y, int weight);
    
    void fillStructures();
    void fillStructureNode();
    void fillStructureEdge();

    void drawNodes(GLuint buffer, NodeProgram *shader, unsigned int nbVertex, const std::vector<NodeStruct> &structures, bool,bool);
    void drawMarks();
    void drawEdges(const std::vector<EdgeStruct> & structures, bool behind, bool colorPicking);


    void setQuads(std::vector<Vec3f> &quads, unsigned int size, GLuint buf);
    void setMarks();
    void setEdgesBuffer(unsigned curveDefinition, unsigned endsDefinition);



    void extendBoundingBox(const Coord & point);
    void resetBoundingBox(const Coord & point);

    Data2  data;
    Vec2f res;

    int vertmem;

    TextRenderer *textRender; // à remplacer par // LabelRender *labelRender;
    std::string addFont = "../fonts/Vera.ttf";
    std::vector<Label *> labelvector;
    std::ostringstream oss;
    std::string s;

    NodeProgram *nodeShader;
    MarkProgram *markShader;
    EdgeProgram *edgeShader;


    MatrixManager* mManager;

    float cWidth;
    unsigned int nbNodes;
    unsigned int nbEdges;

    float xMin;
    float xMax;
    float yMin;
    float yMax;

    //unsigned int nbAxe;

    //DRAWN STRUCTURES
    std::vector<Vec3f> nodeQuads;

    std::vector<NodeStruct>   nodes;
    std::vector<NodeStruct>   highlightedNodes;

    std::vector<EdgeStruct>    edges;
    std::vector<EdgeStruct>    highlightedEdges;


    std::vector<edge> edgeMap;
    std::vector<node> nodeMap;


    ColorProperty  * c;
    DoubleProperty * w;
    StringProperty * l;
    LayoutProperty * pos;
    SizeProperty   * sp;

    // BUFFERS
    GLuint nodesbuffer;
    GLuint marksbuffer;
    GLuint edgesbuffer;
    GLuint edgeselements;
    unsigned edgeDefinition;
  };
}
#endif // PARACOORDRENDERER_H
