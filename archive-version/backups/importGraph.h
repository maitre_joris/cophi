#ifndef IMPORTGRAPH_H
#define IMPORTGRAPH_H

#include <string>
#include <tulip/Graph.h>
#include <Data.h>

bool importGraph(const std::string & filename, Data2 &);


class Data2
{
public:

    class Property
    {
    public:
        std::string name;
        std::map<int,tlp::node> values;

      Property(const std::string & name = "koukou") : name(name)
        {}
        Property(Property && p) {
            name   = std::move( p.name   );
            values = std::move( p.values );
        }
    };

    tlp::Graph * graph;
    tlp::Graph * aggregatedGraph;
    std::vector<Property> properties;
    long individuals = 0;

    void setAggregatedGraph(tlp::Graph * g) {
        delete aggregatedGraph;
        aggregatedGraph = g;
    }

    Data2() {
        aggregatedGraph = tlp::newGraph();
    }
    Data2(tlp::Graph * g)
        : aggregatedGraph(g)
    {}
    Data2(const Data2 & d) = delete;
    Data2(Data2 && d) {
        (*this) = std::move(d);
    }

    Data2 & operator=(const Data2 & d) = delete;
    Data2 & operator=(Data2 && d) {
       std::swap(aggregatedGraph, d.aggregatedGraph);
        properties  = std::move( d.properties );
        individuals = d.individuals;

        return *this;
    }

    ~Data2() { delete aggregatedGraph; }
};

#endif // IMPORTGRAPH_H
