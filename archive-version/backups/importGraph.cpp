#include <time.h>
#include <math.h>
#include <climits>
#include <fstream>
#include <sstream>
#include <tulip/Graph.h>
#include <tulip/ForEach.h>
#include <tulip/SimpleTest.h>
#include <tulip/ColorProperty.h>
#include <tulip/ColorScale.h>
#include <tulip/TulipPluginHeaders.h>
#include <Data.h>
#include <utils.h>

using namespace std;
using namespace tlp;

struct Edge {
    edge e;
    int val;
    Edge(edge e, int val): e(e), val(val){}
};

struct CompEdge {
    bool operator()(const Edge&e, const Edge&e1){
        return e.val < e1.val;
    }
};

/*struct HashEdge{
  size_t operator()(const edge & e) const{
    return e.id;
  }
  };*/
void parseCSV(const string & filename, Data2 & data)
{
    //graph->getProperty<IntegerProperty>("viewShape")->setAllNodeValue(1);
    //graph->getProperty<DoubleProperty>("viewBordeSize")->setAllNodeValue(1);

   //IntegerProperty * weight = data.aggregatedGraph->getProperty<IntegerProperty>("to");
   DoubleProperty * weight = data.aggregatedGraph->getProperty<DoubleProperty>("weight");
   DoubleProperty * prop   = data.aggregatedGraph->getProperty<DoubleProperty>("value"); // sûrement un doublons de weight, à vérifier

    weight->setAllEdgeValue(0);
    weight->setAllNodeValue(0);

    prop->setAllNodeValue(0.);

    std::ifstream in(filename, std::ifstream::ate | std::ifstream::binary);
    std::ifstream::pos_type size = in.tellg();
    in.seekg(0);

    std::string line;
    std::getline(in, line);

    data.individuals = 0;

    vector<string> labels;
    tokenize(line, labels, ",");

    data.properties.reserve(labels.size() - 1);
    for(unsigned i = 1; i<labels.size(); i++)
        data.properties.emplace_back(labels[i]);

    Progress progress("Importing nodes and edges...");
    std::ifstream::pos_type readBytes = 0;

    while (! in.eof())
    {
        std::getline(in, line);
        readBytes += line.size() + 1;

        //if (strlen(line) == 0)
          //  break;

        vector<string> tokens;
        tokenize(line, tokens, ",");
        if(tokens.size() != labels.size()) continue;

        node prev = node();
        for(unsigned int i = 1; i < tokens.size(); ++i)
        {
            auto & values = data.properties[i-1].values;

            string l = tokens[i];
            float li = atof(l.c_str());

            if(values.find(li) == values.end())
            {
               values[li] = data.aggregatedGraph->addNode();
            }

            node n = values[li];

            prop->setNodeValue(n, li);

            if(prev.isValid())
            {
                edge e = data.aggregatedGraph->existEdge(prev, n);
                if(!e.isValid()){
                   e = data.aggregatedGraph->addEdge(prev,n);
                }
                weight->setEdgeValue(e, weight->getEdgeValue(e)+1);
            }
            prev = n;
        }

        progress.update(readBytes /(float) size);

        data.individuals++;
    }
    progress.update(1);
}


void positionNodes(Data2 & data, float horizontalGap = 1.3, float nodeWidth = 0.05, float verticalSpacing = 0.2)
{
   const float height = 1;

    LayoutProperty * l  = data.aggregatedGraph->getProperty<LayoutProperty>("viewLayout"); // position
    SizeProperty   * s  = data.aggregatedGraph->getProperty<SizeProperty>("viewSize");     // size
    StringProperty * la = data.aggregatedGraph->getProperty<StringProperty>("viewLabel");

    DoubleProperty * weight = data.aggregatedGraph->getProperty<DoubleProperty>("weight");

    // on calcule le poid de chaque sommet
    Progress progress("Computing the weight of each node...");
    unsigned nodeI = 1;
    node n;
    forEach(n, data.aggregatedGraph->getNodes())
    {
        float val = 0;
        edge e;
        forEach(e, data.aggregatedGraph->getInEdges(n))
            val += weight->getEdgeValue(e);

        weight->setNodeValue(n, val);

        progress.update(nodeI/(float)data.aggregatedGraph->numberOfNodes());
        nodeI++;
    }
    progress.update(1);

    // on positionne les sommets
    int xshift = 0;
    bool first = true;

    unsigned progressIMax = 0;
    for(Data2::Property & p : data.properties)
        progressIMax += (p.values.size() * 2) - 1;
    unsigned progressI = 1;

    progress.init("Positionning nodes...");

    for(Data2::Property & p : data.properties)
    {    
        auto & values = p.values;

        // on calcule la somme des écarts
        float gapSum = 0;
        int prev = values.begin()->first;

        if (values.begin() != values.end())
            for(auto it = std::next(values.begin()); it != values.end(); ++it)
            {
                gapSum += it->first - prev - 1;
                prev = it->first;

                progress.update(progressI/(float)progressIMax);
                progressI++;
            }

        const float nodeHeightSum = height - (height*verticalSpacing);

        prev = values.begin()->first;
        float yshift = 0;
        for(auto itn : values)
        {
            int val = itn.first;
            node n = itn.second;
            if(first)
            {
                // on calcules le poid de chaque sommet sur la première colonnes
                float va = 0;
                edge e;
                forEach(e, data.aggregatedGraph->getOutEdges(n))
                    va += weight->getEdgeValue(e);

                weight->setNodeValue(n, va);
            }

            const float gap = (gapSum == 0 or val == prev) ? 0 : (verticalSpacing*height) * ((val-prev-1)/gapSum);
            prev = val;

            const float normalizedWeight = weight->getNodeValue(n) / (float) data.individuals;
            const float nodeHeight = normalizedWeight * nodeHeightSum;

            const Coord c(xshift * (horizontalGap+nodeWidth), gap + yshift + nodeHeight/2, 0);
            const Size si(nodeWidth, nodeHeight, 1);


            s->setNodeValue(n, si);
            l->setNodeValue(n, c);
            la->setNodeValue(n, to_string(val));

            //cout << "node: pos:" << c << " size:" << si << endl;

            yshift += nodeHeight + gap;

            progress.update(progressI/(float)progressIMax);
            progressI++;
        }

        /*
          node u = data.aggregatedGraph->addNode();
          l->setNodeValue(u, Coord(xshift * (horizontalGap+nodeWidth), -0.3, 0));
          s->setNodeValue(u, Size(nodeWidth, 0.1, 0));
          la->setNodeValue(u, p.name);
        */

        xshift++;
        first = false;
    }

    progress.update(1);
}


void makeBends(Data2 & data, const float horizontalGap
               , node node, const Coord nodePos, const Size nodeSize, const float nodeWeight, DoubleProperty * value
               , Iterator<edge> * edgesIt, const unsigned numberOfEdges, const bool outEdges, const DoubleProperty * weight, DoubleProperty * normWeightProp, SizeProperty * size, map<edge, vector<Coord>> & bends)
{
    const unsigned bend1 = (outEdges) ?  0 :  3;
    const unsigned bend2 = (outEdges) ?  1 :  2;
    const float    sign  = (outEdges) ? +1 : -1;

    edge e;
    vector<Edge> edges;
    edges.reserve(numberOfEdges);
    forEach(e, edgesIt)
       edges.emplace_back(e, value->getNodeValue(data.aggregatedGraph->opposite(e,node)));

    sort(edges.begin(), edges.end(), CompEdge());

    const float startY = nodePos.y() - nodeSize.y()/2;
    float sum = 0;

    for(unsigned int i = 0; i < edges.size(); ++i)
    {
        edge e = edges[i].e;
        const float normalizedWeight = weight->getEdgeValue(e) / (float) nodeWeight;
        const float height = normalizedWeight * nodeSize.y();

        bends[e][bend1] = Coord(nodePos.x() + sign * (nodeSize.x()/2) ,   startY + height/2. + sum, 0);
        bends[e][bend2] = Coord(nodePos.x() + sign * (horizontalGap/2),   startY + height/2. + sum, 0);

        Size s = size->getEdgeValue(e);
        if (outEdges)
            s.setX(height);
        else
            s.setX(height);

        size->setEdgeValue(e, s);

        sum += height;
    }
}

void positionEdges(Data2 & data, float horizontalGap = 1.3, float verticalSpacing = 0.2)//, float verticalSpacing = 0, float aspectRatio = 16/9.)
{
    srand(time(nullptr));

    DoubleProperty * weight = data.aggregatedGraph->getProperty<DoubleProperty>("weight");
    DoubleProperty * normWeightProp = data.aggregatedGraph->getProperty<DoubleProperty>("normalizedWeight");

    LayoutProperty * l  = data.aggregatedGraph->getProperty<LayoutProperty>("viewLayout"); // position
    SizeProperty   * s  = data.aggregatedGraph->getProperty<SizeProperty>("viewSize");     // size
    DoubleProperty * prop   = data.aggregatedGraph->getProperty<DoubleProperty>("value");

    Progress progress("Positionning edges...");
    unsigned nodeI = 1;

    map<edge, vector<Coord>> bends;

    edge e;
    forEach(e, data.aggregatedGraph->getEdges())
        bends[e].resize(4);

    node n;
    forEach(n, data.aggregatedGraph->getNodes())
    {
        Size  size = s->getNodeValue(n);
        Coord pos  = l->getNodeValue(n);
        const float w = weight->getNodeValue(n);
        const float nbEdges = data.aggregatedGraph->numberOfEdges();

        if(data.aggregatedGraph->deg(n) == 0)
            continue;

        makeBends(data, horizontalGap
                  ,n , pos, size, w, prop
                  ,data.aggregatedGraph->getOutEdges(n), nbEdges, true , weight, normWeightProp, s, bends);

        makeBends(data, horizontalGap
                  ,n , pos, size, w, prop
                  ,data.aggregatedGraph->getInEdges(n) , nbEdges, false, weight, normWeightProp, s, bends);

        progress.update(nodeI/(float)data.aggregatedGraph->numberOfNodes());
    }
    progress.update(1);

    forEach(e, data.aggregatedGraph->getEdges())
        l->setEdgeValue(e, bends[e]);

}


void computeSubGraphs(Data2 & data)
{
   IntegerProperty * subGraphIdProp = data.aggregatedGraph->getProperty<IntegerProperty>("subGraph");

   node n;
   forEach(n, data.aggregatedGraph->getNodes())
   {
      Graph * sub = data.aggregatedGraph->addSubGraph();
      subGraphIdProp->setNodeValue(n, sub->getId());

      sub->addNode(n);

      edge e;
      forEach(e, data.aggregatedGraph->getInOutEdges(n))
         sub->addEdge(e);
   }
}

void applyColorGradient(Data2 & data)
{
   LayoutProperty * layoutProp  = data.aggregatedGraph->getProperty<LayoutProperty>("viewLayout");
   ColorProperty * colorProp   = data.aggregatedGraph->getProperty<ColorProperty>("viewColor");

   ColorScale cs(true);

   for (Data2::Property & p : data.properties)
      for (auto & v : p.values)
         colorProp->setNodeValue(v.second, cs.getColorAtPos( layoutProp->getNodeValue(v.second).getY() ));
}


//=============================================================
void importGraph(const string & filename, Data2 & data)
{
   parseCSV(filename, data);
   positionNodes(data);
   positionEdges(data);
   computeSubGraphs(data);
   applyColorGradient(data);
   
   tlp::saveGraph(data.aggregatedGraph, "/tmp/DEBUG.tlp");
}

