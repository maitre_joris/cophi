
extern "C" {
#define GL_GLEXT_PROTOTYPES
#define EGL_EGLEXT_PROTOTYPES
//#include <GL/glew.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
}
#include "ParaCoordRenderer.h"
#include <tulip/Delaunay.h>
#include <tulip/ColorScale.h>
//#include <unordered_map>
#include <time.h>

using namespace std;

#ifdef _OPENMP
#include <omp.h>
#define MAX_THREADS (unsigned int) omp_get_max_threads()
#else
#define MAX_THREADS 1
#endif

#include <utils.h>

namespace tlp {
   void printNodeStruct (NodeStruct & s)
   {
      cout << "Node {\n"
           << "  position     = " << s.position[0]  << "," << s.position[1] << "\n"
           << "  size         = " << s.size.x()     << "," << s.size.y()    << "\n"
           << "  weight       = " << s.weight       << "\n"
           << "  shape        = " << s.shape        << "\n"
           << "  color        = " << s.color[0]     << " " << s.color[1]  << " " << s.color[2]  << " " << s.color[3]  << "\n"
           << "  border color = " << s.bcolor[0]    << " " << s.bcolor[1] << " " << s.bcolor[2] << " " << s.bcolor[3] << "\n"
           << "}\n" << endl;
   }

   NodeProgram::NodeProgram(): ShaderProgram("../shaders/node_shader.glsl")
   {
      matProjMod    = glGetUniformLocation(program,"matProjMod");
      data = glGetUniformLocation(program,"data");
      res = glGetUniformLocation(program,"res");
      colorPicking = glGetUniformLocation(program,"colorPicking");
      behind = glGetUniformLocation(program,"behind");
   }

   EdgeProgram::EdgeProgram(): ShaderProgram("../shaders/edge_shader.glsl")
   {
      matProjMod = glGetUniformLocation(program,"matProjMod");
      data = glGetUniformLocation(program,"data");
      res  = glGetUniformLocation(program,"res");
      colorPicking = glGetUniformLocation(program,"colorPicking");
      behind = glGetUniformLocation(program,"behind");
      tubular = glGetUniformLocation(program,"tubular");
   }


   MarkProgram::MarkProgram(): ShaderProgram("../shaders/mark_shader.glsl")
   {
      matProjMod = glGetUniformLocation(program,"matProjMod");
      res = glGetUniformLocation(program,"res");
   }



   void ParaCoordRenderer::clear() {
      nodeQuads.clear();
      labelvector.clear();
   }

   /**
    * @brief ParaCoordRenderer::loadGraph
    * @param g
    * chargement et traitements sur le graph
    */
   void ParaCoordRenderer::loadGraph(Data2 && d)
   {
      clear();

      this->data = std::move(d);

      init();
   }

   /**
    * @brief ParaCoordRenderer::ParaCoordRenderer
    * @param g
    * @param mManager
    * Initialisation du renderer, des shaders/shaderProgram
    */
   ParaCoordRenderer::ParaCoordRenderer(MatrixManager *mManager) {
      cerr<<"PCR constructor"<<endl;

      this->mManager=mManager;
      this->nodeShader  = new NodeProgram();
      this->markShader  = new MarkProgram();
      this->edgeShader  = new EdgeProgram();
      this->textRender  = new TextRenderer(this->mManager);

      // nbAxe = 0;


      glGenBuffers(1, &nodesbuffer);
      glGenBuffers(1, &marksbuffer);
      glGenBuffers(1, &edgesbuffer);
      glGenBuffers(1, &edgeselements);




      init();
   }
   ParaCoordRenderer::~ParaCoordRenderer(){
   }

   MatrixManager* ParaCoordRenderer::getMatrixManager() {
      return mManager;
   }

   void ParaCoordRenderer::setLabels(node &n,int x, int y, int weight){
      oss<< l->getNodeValue(n);
      s=oss.str();
      labelvector.push_back(textRender->createLabel(Vec3f(x-1,y+weight/2,0.),0.,addFont,s));
      oss.str("");
      oss.str().clear();
   }

   Vec4f idColor(const unsigned id)
   {
      const unsigned b = id%256;
      const unsigned g = (id / 256) % 256;
      const unsigned r = (id / (256*256)) % 256;

      return Vec4f(r/255., g/255., b/255., 1.);
   }

   /**
    * @brief ParaCoordRenderer::fillStructureNode
    * @param vg
    * @param nProp
    */
   void ParaCoordRenderer::fillStructureNode()
   {
      ColorProperty   * colorProp = data.aggregatedGraph->getProperty<ColorProperty> ("viewColor");
      IntegerProperty * idProp    = data.aggregatedGraph->getProperty<IntegerProperty> ("id");

      cout<<"fillStructureNode();"<<endl;

      srand(time(nullptr));

      // unsigned int position=1., axe = 1.;

      resetBoundingBox(pos->getNodeValue(data.aggregatedGraph->getOneNode()));

      nodeMap.resize(data.aggregatedGraph->numberOfNodes());
      nodes.resize(data.aggregatedGraph->numberOfNodes());

      Progress progress("Generating nodes uniform buffer...");

      unsigned i = 0;
      node ni;
      forEach(ni, data.aggregatedGraph->getNodes())
      {
         NodeStruct currNode;

         const Color color = colorProp->getNodeValue(ni);
         
         const Coord point = pos->getNodeValue(ni);
         const Size  size  = sp->getNodeValue(ni);

         //cout << "node: " << point.x() << "," << point.y() << endl;
         //getc(stdin);

         currNode._padding = Vec2f(0);

         currNode.position = Vec2f(point.x(), point.y());
         currNode.size     = Vec2f(size.x(), size.y());

         //currNode.weight = (float)w->getNodeValue(ni);
         currNode.shape  = 0;

         currNode.color.set((float)color.getRGL(),(float)color.getGGL(),(float)color.getBGL(),(float)color.getAGL());
         currNode.bcolor.set((float)color.getRGL(),(float)color.getGGL(),(float)color.getBGL(),(float)color.getAGL());

         currNode.idColor = idColor(i);

         nodes[i]   = currNode;

         nodeMap[i] = ni;
         idProp->setNodeValue(ni, i);


//            setLabels(ni,axe,position,currNode.weight);
         //position += currNode.weight;

         extendBoundingBox(point - size);
         extendBoundingBox(point + size);

         progress.update(i/(float)(data.aggregatedGraph->numberOfNodes()-1));

         i++;
      }
      progress.update(1);
      // ++nbAxe;
      //if (position > yMax) yMax = position;

   }


   void ParaCoordRenderer::fillStructureEdge()
   {
      LayoutProperty * layoutProp  = data.aggregatedGraph->getProperty<LayoutProperty>("viewLayout");
      ColorProperty  * colorProp   = data.aggregatedGraph->getProperty<ColorProperty> ("viewColor");
      SizeProperty   * sizeProp    = data.aggregatedGraph->getProperty<SizeProperty>("viewSize");
      //DoubleProperty * weightProp = data.graph->getProperty<DoubleProperty>("weight");
      IntegerProperty * idProp    = data.aggregatedGraph->getProperty<IntegerProperty> ("id");

      edges.resize(data.aggregatedGraph->numberOfEdges());
      edgeMap.resize(data.aggregatedGraph->numberOfEdges());

      Progress progress("Generating edges uniform buffer...");

      unsigned i = 0;
      edge e;
      forEach(e, data.aggregatedGraph->getEdges())
      {
         EdgeStruct & es = edges[i];

         edgeMap[i] = e;
         idProp->setEdgeValue(e, i);

         const vector<Coord> & bends = layoutProp->getEdgeValue(e);
         es.origin      = Vec2f(bends[0].x(), bends[0].y());
         es.control1    = Vec2f(bends[1].x(), bends[1].y());
         es.control2    = Vec2f(bends[2].x(), bends[2].y());
         es.destination = Vec2f(bends[3].x(), bends[3].y());

         es.width = sizeProp->getEdgeValue(e).x();

         const std::pair<node,node> & ends = data.aggregatedGraph->ends(e);
         Color a = colorProp->getNodeValue(ends.first);
         Color b = colorProp->getNodeValue(ends.second);
         es.originColor      = Vec4f(a.r()/255.0, a.g()/255.0, a.b()/255.0, a.a()/255.0);
         es.destinationColor = Vec4f(b.r()/255.0, b.g()/255.0, b.b()/255.0, b.a()/255.0);

         es.idColor = idColor(nodeMap.size() + i);

         progress.update(i/(float)(data.aggregatedGraph->numberOfEdges()-1));

         i++;
      }
      progress.update(1);

   }


   void ParaCoordRenderer::extendBoundingBox(const Coord &point)
   {
      if (point.x() < xMin)
         xMin = point.x();
      else if (point.x() > xMax)
         xMax = point.x();

      if (point.y() < yMin)
         yMin = point.y();
      else if (point.y() > yMax)
         yMax = point.y();
   }

   void ParaCoordRenderer::resetBoundingBox(const Coord &point)
   {
      xMin = xMax = point.x();
      yMin = yMax = point.y();
   }

   /**
    * @brief ParaCoordRenderer::fillStructures
    * creation du vectorgraph utilisé pour faire le rendu
    */
   void ParaCoordRenderer::fillStructures(){

      cout<<"fillStructures(); "<<endl;

      fillStructureNode();
      fillStructureEdge();
   }

   /**
    * @brief ParaCoordRenderer::setQuads
    * @param quads
    * @param size
    * @param buf
    * methode generique pour instancier des quads (ensemble de deux triangles tilisé pour le dessin)
    */
   void ParaCoordRenderer::setQuads(vector<Vec3f> &quads, unsigned int size, GLuint buf)
   {
      /*************** VBO FOR NODES ****************/
      quads.resize(6 * size);
      for (unsigned int i=0; i<size; ++i)
      {
         const unsigned shift = 6 * i;

         quads[0 + shift] = Vec3f(0.f, 0.f, i);
         quads[1 + shift] = Vec3f(1.f, 0.f, i);
         quads[2 + shift] = Vec3f(0.f, 1.f, i);
         quads[3 + shift] = Vec3f(1.f, 1.f, i);
         quads[4 + shift] = Vec3f(0.f, 1.f, i);
         quads[5 + shift] = Vec3f(1.f, 0.f, i);
      }

      glBindBuffer(GL_ARRAY_BUFFER, buf);
      glBufferData(GL_ARRAY_BUFFER, quads.size() * sizeof(Vec3f), &quads[0], GL_STATIC_DRAW);
   }

   void ParaCoordRenderer::setMarks()
   {
      glBindBuffer(GL_ARRAY_BUFFER, marksbuffer);
      glBufferData(GL_ARRAY_BUFFER, data.properties.size() * 2 * sizeof(Vec2f), nullptr, GL_STATIC_DRAW );//GL_STATIC_DRAW);

      glBindBuffer(GL_ARRAY_BUFFER, marksbuffer);
      Vec2f * buffer = (Vec2f*) glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
      if (buffer == nullptr) {
         cerr << "glMapBuffer() returning nullptr " << data.properties.size() << endl;
         exit(1);
      }

      /*************** VBO FOR MARKS ****************/
      for (unsigned int i=0; i < data.properties.size(); i++)
      {
         // une ligne verticale de repère par propriété
         // la hauteur est normalisé à 1
         buffer[2*i]    = Vec2f(i*1.35, 0); //  1.35 à l'arrache: temporaire 
         // besoin de repenser/refactoriser la gestion des propriétés d'affichages
         // data.graph devrais surement être complétement normalisé
         // les transformations se fesant soit dans fillStructureNode()
         // soit dans le vertex shader (plus côuteux, mais on peut modifier les propriétés à la volée)
         // on verra après avoir fait les arrêtes
         buffer[(2*i)+1] = Vec2f(i*1.35, 1);
      }

      glUnmapBuffer(GL_ARRAY_BUFFER);
   }

   float getT(unsigned i, unsigned curveDefinition, unsigned endsDefinition)
   {
      const float d = 0.1;

      if (i <= 1)
         return 0;
      else if (i >= (curveDefinition + 2*endsDefinition)-2)
         return 1;
      else if (i < endsDefinition)
         return d * (i /(float) endsDefinition);
      else if (i > endsDefinition + curveDefinition)
         return 1 - d + d * ((i - endsDefinition - curveDefinition) /(float) endsDefinition);
      else
         return d + ((i - endsDefinition) /(float) curveDefinition) * (1 - 2*d);
   }
   void ParaCoordRenderer::setEdgesBuffer(unsigned curveDefinition = 50, unsigned endsDefinition = 45)
   {
      if ((curveDefinition + 2*endsDefinition) % 2 == 1)
         curveDefinition += 1; // le nombre de sommets sera pair (plus simple)

      const unsigned maxEdges = (vertmem - 10) / SIZEOFEDGE;
      edgeDefinition = curveDefinition + 2*endsDefinition;

      /*************** VBO FOR EDGES ****************/
      glBindBuffer(GL_ARRAY_BUFFER, edgesbuffer);
      glBufferData(GL_ARRAY_BUFFER, maxEdges * edgeDefinition * sizeof(Vec3f), nullptr, GL_STATIC_DRAW );//GL_STATIC_DRAW);

      Vec3f * vertexBuffer = (Vec3f*) glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
      if (vertexBuffer == nullptr) {
         cerr << "glMapBuffer() 1 returning nullptr in setEdgeBuffer()" << endl;
         exit(1);
      }

      for (unsigned i=0; i < maxEdges * edgeDefinition; i++)
         vertexBuffer[i] = Vec3f(getT(i%edgeDefinition, curveDefinition, endsDefinition), ( (float)((i%2)+1)*2 ) - 3, i/edgeDefinition);

      glUnmapBuffer(GL_ARRAY_BUFFER);


      /*************** INDEXES FOR EDGES ****************/
      const unsigned triangles = (maxEdges * (edgeDefinition-2));

      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, edgeselements);
      glBufferData(GL_ELEMENT_ARRAY_BUFFER, triangles * 3 * sizeof(unsigned), nullptr, GL_STATIC_DRAW);//GL_STATIC_DRAW);

      unsigned * elementsBuffer = (unsigned*) glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);
      if (elementsBuffer == nullptr) {
         cerr << "glMapBuffer() 2 returning nullptr in setEdgeBuffer()" << endl;
         exit(1);
      }

      unsigned i = 0;
      for (unsigned e=0; e < maxEdges*edgeDefinition ; e += edgeDefinition)
         for (unsigned v=0; v < edgeDefinition-2; v++)
         {
            elementsBuffer[i]   = e+v;
            elementsBuffer[i+1] = e+v+1;
            elementsBuffer[i+2] = e+v+2;

            i += 3;
         }

      glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
   }

   void ParaCoordRenderer::init() {
      cerr<<"init"<<endl;

      if (data.individuals == 0)
         return;

      this->nbEdges = this->data.aggregatedGraph->numberOfEdges();
      this->nbNodes = this->data.aggregatedGraph->numberOfNodes();

      this->c   = data.aggregatedGraph->getProperty<ColorProperty>("viewColor");

      this->w   = data.aggregatedGraph->getProperty<DoubleProperty>("weight");
      this->l   = data.aggregatedGraph->getProperty<StringProperty>("viewLabel");
      this->pos = data.aggregatedGraph->getProperty<LayoutProperty>("viewLayout");
      this->sp  = data.aggregatedGraph->getProperty<SizeProperty>("viewSize");

      /*unsigned i = 0;
      for (const Data2::Property & p : data.properties)
      {
         Label * l = textRender->createLabel(Vec3f(i*1.35,-0.2,0), 0, addFont, p.name);
         labelvector.push_back(l);

         for (const auto & v : p.values)
         {
            const float y = pos->getNodeValue(v.second).y();
            Label * l = textRender->createLabel(Vec3f(i*1.35, y, 0), 0, addFont, std::to_string(v.first));
            labelvector.push_back(l);
         }

         i++;
         }*/

      fillStructures();

      glGetIntegerv(GL_MAX_VERTEX_UNIFORM_VECTORS, &vertmem);
      cout << "Vertmem:" << vertmem << endl;

      setQuads(nodeQuads,nbNodes,nodesbuffer);
      setMarks();
      setEdgesBuffer();

      cerr<<"nodesBuffer generated"<<endl;
   }

   void ParaCoordRenderer::centerGraph() {
      mManager->centerBox(computeBBox());
   }

   Vec4f ParaCoordRenderer::computeBBox() {
      return Vec4f(xMin, yMin, xMax-xMin, yMax-yMin);
   }

   void ParaCoordRenderer::zoom(const Vec2f pos, const float scale) {
      mManager->zoom(scale,pos);
   }

   void ParaCoordRenderer::updateResolution(){
      res[0] = mManager->getWindowWidth();
      res[1] = mManager->getWindowHeight();
   }


   void ParaCoordRenderer::stopHighlight()
   {
      highlightedNodes.clear();
      highlightedEdges.clear();
   }

   bool ParaCoordRenderer::highlightAt(unsigned x, unsigned y)
   {
      draw(true);
      glFlush();
      glFinish(); 
      glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
      
      unsigned char pixel[4];
      glReadPixels(x, y, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, pixel);

      unsigned id = pixel[0]*256*256 + pixel[1]*256 + pixel[2];
      
      IntegerProperty * weightProp     = data.aggregatedGraph->getProperty<IntegerProperty>("weight");
      IntegerProperty * subGraphIdProp = data.aggregatedGraph->getProperty<IntegerProperty>("subGraph");
      IntegerProperty * idProp         = data.aggregatedGraph->getProperty<IntegerProperty>("id");
      
      if (id >= nodes.size() + edges.size())
         return false;
      
      else if (id < nodeMap.size()) // node 
      {
         highlightedNodes.clear();
         highlightedEdges.clear();
         //std::cout << "catched node " << id << " !" << std::endl;
         
         Graph * sub =  data.aggregatedGraph->getSubGraph( subGraphIdProp->getNodeValue(nodeMap[id]) );
         IntegerProperty * subWeightProp  = sub->getLocalProperty<IntegerProperty>("weight");

         edge e;
         forEach(e, sub->getEdges())
         {
            EdgeStruct es = edges[idProp->getEdgeValue(e)];
            es.width *= (subWeightProp->getEdgeValue(e) /(float) weightProp->getEdgeValue(e));
            highlightedEdges.push_back( es );
         }

         node n;
         forEach(n, sub->getNodes())
         {
            NodeStruct ns = nodes[idProp->getNodeValue(n)];
            ns.size.setY( ns.size.y() * (subWeightProp->getNodeValue(n) /(float) weightProp->getNodeValue(n)) );
            highlightedNodes.push_back( ns );
         }

         return true;
      }
      else  // edge
      {
         highlightedNodes.clear();
         highlightedEdges.clear();
         id -= nodeMap.size();

         //std::cout << "catched edge " << id << " !" << std::endl;

         highlightedEdges.push_back( edges[id] );

         const std::pair<node,node> & ends = data.aggregatedGraph->ends(edgeMap[id]);
         highlightedNodes.push_back( nodes[idProp->getNodeValue(ends.first) ] );
         highlightedNodes.push_back( nodes[idProp->getNodeValue(ends.second)] );
         
         return true;
      }
   }


   void ParaCoordRenderer::draw(bool colorPicking = false) {
      /***** drawing *********/
      glClearColor(1., 1., 1., 1.);
      glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

      //glEnable(GL_DEPTH);
      //glEnable(GL_DEPTH_TEST);
      glDepthFunc(GL_NEVER); // pas besoin du z-buffer non ?
      // du coup le dessin se fait suivant l'ordre des appels
      // c'est plus simple pour l'instant

      glEnable(GL_BLEND);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
      glEnableVertexAttribArray(0);

      if (colorPicking)
         glDisable(GL_MULTISAMPLE);
      else
         glEnable(GL_MULTISAMPLE);

      updateResolution();

      const bool behind = ((not highlightedNodes.empty() or not highlightedEdges.empty()) and not colorPicking);

      if (not colorPicking)
         drawMarks();
      
      drawEdges(edges, behind, colorPicking);

      const int nbVertexNodes = (vertmem - 10) / SIZEOFNODE;//(max nbUniform - nbUniform ds shader )/ nb vec4 ds nodeStruct
      drawNodes(nodesbuffer,nodeShader,nbVertexNodes,nodes, behind, colorPicking);
      
      if (not colorPicking)
      {
         if (not highlightedNodes.empty())
            drawNodes(nodesbuffer, nodeShader, nbVertexNodes, highlightedNodes, false, colorPicking);

         if (not highlightedEdges.empty())
            drawEdges(highlightedEdges, false, colorPicking);

         glDisable(GL_MULTISAMPLE);
         textRender->draw(labelvector,10.,10.);
      }

      glDisableVertexAttribArray(0);
   }





   void ParaCoordRenderer::drawNodes(GLuint buffer, NodeProgram *shader, unsigned int nbVertex,const vector<NodeStruct> & structures, bool behind, bool colorPicking)
   {
      GlMat4f mPM   = mManager->getMatProjMod();

      glUseProgram(shader->program);
      glUniformMatrix4fv(shader->matProjMod   , 1, GL_FALSE, &mPM[0][0]);
      glBindBuffer(GL_ARRAY_BUFFER, buffer);
      glBindAttribLocation(shader->program, 0, "Vertex");
      glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE, 0,NULL);
      glUniform2fv(shader->res, 1, &res[0]);
      glUniform1i(shader->colorPicking, colorPicking);
      glUniform1i(shader->behind, behind);

      unsigned int buf = 0;
      unsigned renderedNodes = 0;
      NodeStruct bufferNodes[nbVertex];

      const Vec4f screen = screenRect(mPM.inverse());

      for (unsigned i=0; i < structures.size(); i++)
      {
         const NodeStruct & ns = structures[i];

         const Vec4f nodeRect = Vec4f ( ns.position.x() - ns.size.x()/2., ns.position.y() - ns.size.y()/2.
                                        , ns.position.x() + ns.size.x()/2., ns.position.y() + ns.size.y()/2.);

         if ( rectIntersects(screen, nodeRect) )
         {
            /*NodeStruct dbg;
            float sw = screen.z() - screen.x();
            float sy = screen.w() - screen.y();

            dbg.position = Vec2f(screen.x() + sw/2, screen.y() + sy/2);
            dbg.size = Vec2f(sw, sy);
            dbg.color = Vec4f(0.5,1,0,1);*/

            bufferNodes[buf] = ns;
            buf++;
            renderedNodes++;

            if(buf == nbVertex)
            {
               glUniform4fv(shader->data, SIZEOFNODE * nbVertex, (float*)&(bufferNodes[0]));
               glDrawArrays(GL_TRIANGLES, 0, 6 * buf);
               buf = 0;
            }
         }
      }

      if(buf > 0)
      {
         glUniform4fv(shader->data, SIZEOFNODE * nbVertex, (float*)&(bufferNodes[0]));
         glDrawArrays(GL_TRIANGLES, 0, 6 * buf);
      }

      //cout << "Rendered Nodes: " << renderedNodes << endl;
   }

   void ParaCoordRenderer::drawMarks()
   {
      GlMat4f mPM = mManager->getMatProjMod();
      glUseProgram(markShader->program);
      glUniformMatrix4fv(markShader->matProjMod, 1, GL_FALSE, &mPM[0][0]);
      glBindBuffer(GL_ARRAY_BUFFER, marksbuffer);
      glBindAttribLocation(markShader->program, 0, "Vertex");
      glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE,0,NULL);
      glUniform2fv(markShader->res, 1, &res[0]);

      glLineWidth(1);
      glDrawArrays(GL_LINES, 0, data.properties.size() * 2);
   }

   void ParaCoordRenderer::drawEdges(const std::vector<EdgeStruct> & structures, bool behind, bool colorPicking)
   {
      const unsigned maxEdges = (vertmem - 10) / SIZEOFEDGE;

      GlMat4f mPM = mManager->getMatProjMod();
      glUseProgram(edgeShader->program);
      glUniformMatrix4fv(edgeShader->matProjMod, 1, GL_FALSE, &mPM[0][0]);

      glBindBuffer(GL_ARRAY_BUFFER,         edgesbuffer);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, edgeselements);

      glBindAttribLocation(edgeShader->program, 0, "Vertex");
      glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,NULL);
      glUniform2fv(edgeShader->res, 1, &res[0]);
      glUniform1i(edgeShader->colorPicking, colorPicking);
      glUniform1i(edgeShader->behind, behind);

      if (colorPicking or behind)
         glUniform1f(edgeShader->tubular, 0.);
      else
         glUniform1f(edgeShader->tubular, 0.6);

      EdgeStruct buffer[maxEdges];
      unsigned bufferSize = 0;
      unsigned renderedEdges = 0;

      const Vec4f screen = screenRect(mPM.inverse());

      for (unsigned i = 0; i < structures.size(); i++)
      {
         const EdgeStruct & es = structures[i];

         const float edgeHeight = abs(es.origin.y() - es.destination.y());
         const Vec4f edgeRect = (es.origin.y() <= es.destination.y()) ?
            Vec4f(es.origin.x(), es.origin.y() - es.width/2, es.destination.x(), es.destination.y() + es.width/2)
            : Vec4f(es.origin.x(), es.origin.y() - edgeHeight - es.width/2, es.destination.x(), es.destination.y() + edgeHeight + es.width/2);

         if ( rectIntersects(screen, edgeRect) )
         {
            buffer[bufferSize] = es;
            bufferSize++;
            renderedEdges++;

            if (bufferSize == maxEdges)
            {
               const unsigned triangles = (bufferSize * (edgeDefinition-2));
               glUniform4fv(edgeShader->data, maxEdges * SIZEOFEDGE, (float*) buffer);
               glDrawElements(GL_TRIANGLES, triangles * 3, GL_UNSIGNED_INT, (void*)0);

               bufferSize = 0;
            }
         }
      }

      if (bufferSize > 0)
      {
         const unsigned triangles = (bufferSize * (edgeDefinition-2));
         glUniform4fv(edgeShader->data, maxEdges * SIZEOFEDGE, (float*) buffer);
         glDrawElements(GL_TRIANGLES, triangles * 3, GL_UNSIGNED_INT, (void*)0);

         bufferSize = 0;
      }

      //cout << "Rendered Edges: " << renderedEdges << endl;
   }
}
