
var histogramMode = true;
function changeMode(){
  if(histogramMode){
    Module.distortionView();
    histogramMode = false;
  }
  else{
    Module.histogramView();
    histogramMode = true;
  }
}

var showLabels = true;
function changeLabels(){
    Module.displayLabels();
  var lbl = document.getElementById('lbl');
  if(showLabels){
    showLabels = false;
    lbl.src="img/addLabels-alpha2.png";

  }
  else{
    showLabels = true;
    lbl.src="img/removeLabels-alpha2.png";
  }
}
var mapping = true;
function changeMapping(){
  var map = document.getElementById('mapping');
  if (mapping){
    mapping = false;
    map.src="./img/mapping-alpha2.png";
    // map1.style.transform = 'rotate(0deg)';
  }else{
    mapping = true;
    map.src="./img/mapping-alpha.png";
    // map1.style.transform = 'rotate(180deg)';
  }
  Module.changeMapping();
}

var infMode = false;
var noneMode = false;
var invertMode = false;
var defaultMode = true;
var oldMode = "default";
var infoBulle =document.getElementById("ib");
function getInfo(){
  HideContent("infoBulle");
  test = Module.infoMode();
  if (Module.getValue(test+(4*0),'float')>0){
  infoBulle.innerHTML ="";
  infoBulle.innerHTML +="<b>id:</b>"+Module.getValue(test+(4*0),'float')+"</br>";
  infoBulle.innerHTML +="<b>min:</b>"+Module.getValue(test+(4*1),'float')+"</br>";
  infoBulle.innerHTML +="<b>max:</b>"+Module.getValue(test+(4*2),'float')+"</br>";
  infoBulle.innerHTML +="<b>weight:</b>"+Module.getValue(test+(4*3),'float')+"</br>";
  ShowContent("infoBulle");
  }
}
var allAttributes = {names:[],dbID:[]};
var hiddenAttributes = [];
var newAttributes = [];
var getfirstAttributes = true;

function arr_diff (a1, a2) {
    var toRemove = [], diff = [];
    for (var i = 0; i < a1.length; i++) {
        toRemove[a1[i]] = false;
    }
    for (var i = 0; i < a2.length; i++) {
      toRemove[a2[i]] = true;
    }

    for (var i = 0; i < a1.length; i++) {
        if (!toRemove[a1[i]]) {
           diff.push(a1[i]);
        }    
    }
    return diff;
};
function updateAttributes(){
  var nbd = Module.get_nbDims();
  newAttributes=[];
  for (i=0; i<nbd;++i){
    var text = Module.get_dims(i);
    newAttributes.push(text);
  }
  hiddenAttributes = arr_diff(allAttributes.names,newAttributes);
}
//var Vis2DataDim = [];
var mAttrib = document.getElementById("mattrib");
var context = document.getElementById("menu")
function loadFirstAttributes(){
  if (getfirstAttributes){
    var nbd = Module.get_nbDims();
    for (i=0; i<nbd;++i){
    var text = Module.get_dims(i);
      allAttributes.names.push(text);
      allAttributes.dbID.push(i);
    }
  getfirstAttributes = false;
}
}
function updateContextmenu(){
  context.innerHTML = "";
  for (i=0; i<hiddenAttributes.length;++i){
    text = hiddenAttributes[i];

    for (j=0; j<allAttributes.names.length;++j){
      if (allAttributes.names[j]===text){
        context.innerHTML+= "<menuitem label="+text+"  onClick='adddim("+j+")'></menuitem>";
      }
    }
  }
}

function adddim(v){
  console.log('adding dimension: '+v);
  Module.updateInsDim(v);      
}

function loadAttributes(){
  var nbd = Module.get_nbDims();
  mAttrib.innerHTML = "";
  mAttrib.innerHTML += "<span>";
  // context.innerHTML = "";
  for (i=0; i<nbd;++i){
    var text = Module.get_dims(i);
    // console.log(text);
    // console.log("=======");
    // console.log(i);
    // text = text.replace(" ", "_");
    // context.innerHTML+= "<menuitem type='checkbox' name="+text+" label="+text+"  onClick='display(this)' value="+i+" checked></menuitem>";
	  mAttrib.innerHTML += "<label><input type='checkbox' name="+text+" onclick='handleClick(this)' value="+i+" checked> "+text+" </br></label>";
  }
  mAttrib.innerHTML += "</span>";
}
function display(cb){
  console.log(cb.name);
  console.log(cb.label);
  console.log(cb.value);
}
function handleClick(cb) {
  console.log("Clicked, new value of "+ cb.value +" code "+ cb.name +"= " + cb.checked);
  //if (!cb.checked){
//	hiddenAttributes.push(cb.value);
	//Vis2DataDim.splice(Vis2DataDim.indexOf(cb.value),1);
	console.log("indexOf : "+parseInt(cb.value));
	Module.changeDimension(parseInt(cb.value));
 // }else{
  	
  //}
}

function switchClickMode(newMode){
  loadFirstAttributes();
  if (newMode === oldMode) newMode = "default";
  canvas.style.cursor="default";
  oldMode = newMode;
  document.getElementById("invert").src="img/rev-back-forth-alpha2.png";
  document.getElementById("switch").src="img/switchDims.png";
  document.getElementById("delDim").src="img/delDims.png";
  document.getElementById("insDim").src="img/insDims.png";
  document.getElementById("info").src="img/info-alpha.png";
  document.getElementById("infoBulle").style.display="none";
  document.getElementById('menu').innerHTML = "";
  document.getElementById("canvas").onclick = function() {}
  HideContent("infoBulle");
  switch (newMode){
    case "invert":
      Module.invertMode();
      canvas.style.cursor="row-resize";
      document.getElementById("invert").src="img/rev-back-forth-alpha3.png";
      break;
    case "switch":
      Module.switchDimMode();
      canvas.style.cursor = "ew-resize";
      document.getElementById("switch").src="img/movingDims.png";
      break;
    case "delDim":
      Module.switchDelMode();
      canvas.style.cursor = "pointer";
      document.getElementById("delDim").src="img/del.png";
      break;
    case "info":
      // document.getElementById("infoBulle").innerHTML = "Sid: "+Module.infoMode();
      canvas.style.cursor="help";
      Module.infoMode();
      document.getElementById("canvas").onclick = function() { getInfo(); }
      document.getElementById("info").src="img/info-alpha2.png";
      break;

      case "attrib":
      canvas.style.cursor = "pointer";
        document.getElementById("insDim").src="img/add.png";
        Module.addDim();
        loadFirstAttributes();
        updateAttributes();
        break;
    case "none":
      Module.noneMode();
      break;
    case "default":
      Module.defaultMode();
      // document.getElementById("invert").src="img/rev-back-forth-alpha2.png";
      // document.getElementById("info").src="img/info-alpha.png";
      break;
  }
  updateContextmenu();
}
// Copyright 2006,2007 Bontrager Connection, LLC
// http://www.willmaster.com/
// Version: July 28, 2007
var cX = 0; var cY = 0; var rX = 0; var rY = 0;
var canvas = document.getElementsByTagName('canvas')[0];
function UpdateCursorPosition(e){ cX = e.pageX; cY = e.pageY;}
function UpdateCursorPositionDocAll(e){ cX = event.clientX; cY = event.clientY;}

if(document.all) { document.onmousemove = UpdateCursorPositionDocAll; }
else { document.onmousemove = UpdateCursorPosition; }

function AssignPosition(d) {
  if(self.pageYOffset) {
    rX = self.pageXOffset;
    rY = self.pageYOffset;
  }
  else if(document.documentElement && document.documentElement.scrollTop) {
    rX = document.documentElement.scrollLeft;
    rY = document.documentElement.scrollTop;
  }
  else if(document.body) {
    rX = document.body.scrollLeft;
    rX = cX;
    rY = document.body.scrollTop;
    rY = cY;
  }
  if(document.all) {
    cX += rX;
    cY += rY;
  }
  d.style.left = (cX+1) + "px";
  d.style.top = (cY+1) + "px";
}

function HideContent(d) {
  if(d.length < 1) { return; }
  document.getElementById(d).style.display = "none";
}

function ShowContent(d) {
  if(d.length < 1) { return; }
  var dd = document.getElementById(d);
  AssignPosition(dd);
  dd.style.display = "block";
}

var menu2 = document.getElementById("menu2");
var menu1 = document.getElementById("menu1");
var menu0 = document.getElementById("menu0");

var oldmenu = "none";
function switchMenu(menu){
	closeMenu(oldmenu);
	if (menu === oldmenu)
		menu = "none";

	switch (menu){
		case "attributes":
			loadAttributes();

			var h = mAttrib.scrollHeight;
			menu0.style.height = '500px';
			menu0.style.backgroundColor = ' #548CD4';
			menu0.style.color = 'rgba(255,255,255,1)';
			break;
		case "loader":
		    	menu1.style.height = '200px';
			menu1.style.color = 'rgba(255,255,255,1)';
			menu1.style.backgroundColor = ' #548CD4';
			break;
		case "graphics":
			menu2.style.height = '565px';
			menu2.style.backgroundColor = ' #548CD4';
			menu2.style.color = 'rgba(255,255,255,1)';
			break;
		case "none":
			break;
        break;
	}
	oldmenu = menu;
}
function closeMenu(m){
	switch (m){
	case "graphics":
		menu2.style.height = '0px';
		menu2.style.backgroundColor = 'RGBA(0,0,0,0)';
		menu2.style.color = 'rgba(255,255,255,0)';
		break;
	case "loader":
		menu1.style.height = '0px';
		menu1.style.backgroundColor = 'RGBA(0,0,0,0)';
		menu1.style.color =  'rgba(255,255,255,0)';
		break;
	case  "attributes":
		menu0.style.height = '0px';
		menu0.style.backgroundColor = 'RGBA(0,0,0,0)';
		menu0.style.color =  'rgba(255,255,255,0)';
		break;
	}
}

/*function menuAction(){
  if (!menuIsOpen && loadIsOpen) loadAction();
  if (menuIsOpen){
    menu2.style.height = '0px';
    menu2.style.backgroundColor = 'RGBA(0,0,0,0)';
    menu1.style.color = 'rgba(255,255,255,0)';
    menuIsOpen=false;

  }
  else{
    menu2.style.height = '400px';
    menu2.style.backgroundColor = ' #548CD4';
    menu2.style.color = 'rgba(255,255,255,1)';
    menuIsOpen = true;
  }
}

function loadAction(){
  if (menuIsOpen && !loadIsOpen) menuAction();
  if (loadIsOpen){
    menu1.style.height = '0px';
    menu1.style.backgroundColor = 'RGBA(0,0,0,0)';
    menu1.style.color =  'rgba(255,255,255,0)';
    loadIsOpen=false;
  }
  else{
    menu1.style.height = '200px';
    menu1.style.color = 'rgba(255,255,255,1)';
    menu1.style.backgroundColor = ' #548CD4';
    loadIsOpen = true;
  }
}
*/
function setGap(val){
	document.getElementById("gap").innerHTML=parseFloat(val).toFixed(2)+"%";
  Module.setGap(val);
}
function setSpace(val){
	document.getElementById("space").innerHTML=parseFloat(val).toFixed(1);
  Module.setWidth(val);
}
function setNodeWidth(val){
	document.getElementById("nodeWidth").innerHTML=parseFloat(val).toFixed(2);
  Module.setNodeWidth(val);
}
function setHeight(val){
	document.getElementById("height").innerHTML=parseFloat(val).toFixed(2);
  Module.setHeight(val);
}
function setCurvature(val){
	document.getElementById("curvature").innerHTML=parseFloat(val).toFixed(2);
  Module.setCurvature(val);
}
function setEdgeReduction(val){
	document.getElementById("EWR").innerHTML=parseFloat(val).toFixed(0)+"%";
  Module.setEdgeReduction(2.-val*2/100.);
}

function setInvDimSpeed(val){
  document.getElementById("invDimSpeed").innerHTML=parseFloat(val).toFixed(1)+" sec";
  Module.setInvDimSpeed(val);
}
function setInsDimSpeed(val){
  document.getElementById("insDimSpeed").innerHTML=parseFloat(val).toFixed(1)+" sec";
  Module.setInsDimSpeed(val);
}
function setDelDimSpeed(val){
  document.getElementById("delDimSpeed").innerHTML=parseFloat(val).toFixed(1)+" sec";
  Module.setDelDimSpeed(val);
}