"use strict";

/* UI events handlers : faudra mettre tout ça dans Module au lieu de aparac... */
function loadData(name){
  console.log("loading: "+name);
  Module.loadDistantDataPath(name);
//  document.body.style.cursor = 'wait';
//  while (Module.dataLoaded()==0){
//	   document.body.style.cursor = 'wait';
//  }
//  document.body.style.cursor = 'default';
//  console.log("nbElements in dataset: "+Module.dataLoaded());
 // Module.fit();
//  window.
//  loadAction();
//  loadAttributes();
}

var aparac = ui('aparac');

var canvas = document.getElementsByTagName('canvas')[0];
var win = window;
var menu1 = document.getElementById('menu1');
var menu2 = document.getElementById('menu2');

var statusElement   = document.getElementById('status');
var progressElement = document.getElementById('progress');
var spinnerElement  = document.getElementById('spinner');


var Module = {
  arguments:[document.body.clientWidth, document.body.clientHeight],
  preRun: [
    function() {
      //statusElement.innerHTML = "PreRun !!";
      console.log("[preRun] C bindings + GLUT events re-bindings");

      /* Bindings */

      Module.center          = Module.cwrap('c_center'         , '', []);
      Module.fit             = Module.cwrap('c_fit'            , '', []);
      Module.defaultMode     = Module.cwrap('c_defaultMode'    , '', []);
      Module.invertMode      = Module.cwrap('c_invertMode'     , '', []);
      Module.noneMode        = Module.cwrap('c_noneMode'       , '', []);
      Module.switchDimMode   = Module.cwrap('c_switchDimMode'  , '', []);
      Module.switchDelMode   = Module.cwrap('c_delMode'        , '', []);
      Module.addDim         = Module.cwrap('c_insMode'         ,'' , []);
      Module.histogramView   = Module.cwrap('c_histogramView'  , '', []);
      Module.distortionView  = Module.cwrap('c_distortionView' , '', []);
      Module.changeMapping   = Module.cwrap('c_changeMapping'  , '', []);
      Module.displayLabels   = Module.cwrap('c_displayLabels'  , '', []);

      Module.infoMode        = Module.cwrap('c_infoMode'               , 'number', []);
      Module.get_nbDims      = Module.cwrap('c_get_nbDims'             , 'number', []);
      Module.get_dims        = Module.cwrap('c_getDims'                , 'string', ['number']);
      Module.dataLoaded	     = Module.cwrap('c_dataLoaded'             , 'number', []);
      Module.setGap          = Module.cwrap('c_setGap'                 , ''      , ['number']);
      Module.setEdgeReduction= Module.cwrap('c_setEdgeWidth'           , ''      , ['number']);
      Module.setCurvature    = Module.cwrap('c_setCurvature'           , ''      , ['number']);
      Module.setNodeWidth    = Module.cwrap('c_setNodeWidth'           , ''      , ['number']);
      Module.setWidth        = Module.cwrap('c_setWidth'               , ''      , ['number']);
      Module.setHeight       = Module.cwrap('c_setHeight'              , ''      , ['number']);
      Module.setFilename     = Module.cwrap('c_setFilename'            , ''      , ['string']);
      Module.loadDistantData = Module.cwrap('c_loadDistantData'        , ''      , ['string', 'number', 'string', 'string']);
      Module.loadDistantDataPath = Module.cwrap('c_loadDistantDataPath',''       , ['string']);
      Module.setDelDimSpeed = Module.cwrap('c_setDeletionDuration'     ,''       , ['number']);
      Module.setInsDimSpeed = Module.cwrap('c_setInsertionDuration'    ,''       , ['number']);
      Module.setInvDimSpeed = Module.cwrap('c_setInvertionDuration'    ,''       , ['number']);
      Module.changeDimension= Module.cwrap('c_changeDimension'         ,''       , ['number']);
      Module.updateInsDim   = Module.cwrap('c_updateNewDim'            ,''       , ['number']);
    }
  ],
  postRun: [],
  print: (function() {
    var element = document.getElementById('output');
    if (element) element.value = ''; // clear browser cache
    return function(text) {
      if (arguments.length > 1) text = Array.prototype.slice.call(arguments).join(' ');
      // These replacements are necessary if you render to raw HTML
      //text = text.replace(/&/g, "&amp;");
      //text = text.replace(/</g, "&lt;");
      //text = text.replace(/>/g, "&gt;");
      //text = text.replace('\n', '<br>', 'g');
      console.log(text);
      if (element) {
        element.value += text + "\n";
        element.scrollTop = element.scrollHeight; // focus on bottom
      }
    };
  })(),
  printErr: function(text) {
    if (arguments.length > 1) text = Array.prototype.slice.call(arguments).join(' ');
    if (0) { // XXX disabled for safety typeof dump == 'function') {
      dump(text + '\n'); // fast, straight to the real console
    } else {
      console.error(text);
    }
  },
  canvas: (function() {
    var canvas = document.getElementById('canvas');

    // As a default initial behavior, pop up an alert when webgl context is lost. To make your
    // application robust, you may want to override this behavior before shipping!
    // See http://www.khronos.org/registry/webgl/specs/latest/1.0/#5.15.2
    canvas.addEventListener("webglcontextlost", function(e) { alert('WebGL context lost. You will need to reload the page.'); e.preventDefault(); }, false);

    return canvas;
  })(),
  setStatus: function(text) {
    if (!Module.setStatus.last) Module.setStatus.last = { time: Date.now(), text: '' };
    if (text === Module.setStatus.text) return;
    var m = text.match(/([^(]+)\((\d+(\.\d+)?)\/(\d+)\)/);
    var now = Date.now();
    if (m && now - Date.now() < 30) return; // if this is a progress update, skip it if too soon
    if (m) {
      text = m[1];
      // progressElement.value = parseInt(m[2])*100;
      // progressElement.max = parseInt(m[4])*100;
      // progressElement.hidden = false;
      // spinnerElement.hidden = false;
    } else {
      // progressElement.value = null;
      // progressElement.max = null;
      // progressElement.hidden = true;
      // if (!text) spinnerElement.style.display = 'none';
    }
    statusElement.innerHTML = text;
  },
  totalDependencies: 0,
  monitorRunDependencies: function(left) {
    this.totalDependencies = Math.max(this.totalDependencies, left);
    Module.setStatus(left ? 'Preparing... (' + (this.totalDependencies-left) + '/' + this.totalDependencies + ')' : 'All downloads complete.');
  }
};

Module.setStatus('Downloading...');
window.onerror = function(event) {
  // TODO: do not warn on ok events like simulating an infinite loop or exitStatus
  //Module.setStatus('Exception thrown, see JavaScript console');
  // spinnerElement.style.display = 'none';
  //Module.setStatus = function(text) {
    //if (text) Module.printErr('[post-exception status] ' + text);
  //};
};

function ui(rootId)
{
  var self = {};

  function get(c) {
    return document.getElementById(rootId).getElementsByClassName(c).item(0);
  };

  var dbg = get('debug');
  function debug(msg) {
    dbg.innerHTML = msg;
  };

  var controlFileName = get('controlFileName');
  var statusFileName  = get('statusFileName');
  function setDataName(name) {
    controlFileName.innerHTML = name;
    statusFileName.innerHTML = name;
  };
  function stopProp(ev){
    console.log("stop prop"+ev);
    ev.stopPropagation();
  }
  self.onReady = function() {
    /* GLUT events re-binding */
    window.removeEventListener("mousemove", GLUT.onMousemove, true);
    window.removeEventListener("mousedown", GLUT.onMouseButtonDown, true);
    window.removeEventListener("mouseup", GLUT.onMouseButtonUp, true);
    window.removeEventListener("mousewheel", GLUT.onMouseWheel, true);     // IE9, Chrome, Safari, Opera
    window.removeEventListener("DOMMouseScroll", GLUT.onMouseWheel, true); // Firefox
    canvas.addEventListener("mousemove", GLUT.onMousemove, true);
    canvas.addEventListener("mousedown", GLUT.onMouseButtonDown, true);
    canvas.addEventListener("mouseup", GLUT.onMouseButtonUp, true); // IE9, Chrome, Safari, Opera
    canvas.addEventListener("mousewheel", GLUT.onMouseWheel, true); // Firefox
    canvas.addEventListener("DOMMouseScroll", GLUT.onMouseWheel, true);
    self.onResize();
    // Module.fit();
  }
  var canvasContainer = get('canvasContainer');
  self.onResize = function() {
    Module.setCanvasSize(canvasContainer.clientWidth, canvasContainer.clientHeight);
  };

  return self;
};
window.onresize = aparac.onResize;
