#! /bin/bash

gcc -O2 -c texture-*.c vector.c
ar -q libfreetype-gl.a *.o
mv libfreetype-gl.a /usr/local/lib/
rm libfreetype-gl.a