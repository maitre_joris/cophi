#!/bin/sh
BOLD="\033[1m"
GREEN_B="\033[32m"
NORM="\033[0m"
echo "$BOLD$GREEN_B""Configuring freetype with emconfigure...$NORM"
/home/bourqui/Softs/emsdk_portable/emscripten/incoming/emconfigure ./configure
echo "$BOLD$GREEN_B""Make... (emmake)$NORM"
/home/bourqui/Softs/emsdk_portable/emscripten/incoming/emmake make
echo "$BOLD$GREEN_B""Binding FT.o...$NORM"
/home/bourqui/Softs/emsdk_portable/emscripten/incoming/em++ -O2 objs/*.o -o libfreetype.o
echo "$BOLD$GREEN_B""Cleaning...$NORM"
make clean
