/*_vertex_*/
//#version 450
#ifndef GL_ES
//#version 450
#endif
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
#endif

uniform   mat4 MatProjMod;
uniform   vec2 scaling;
uniform   vec2 pos;
uniform   vec2 res;
attribute vec4 posXYtexST;
varying   vec2 texcoord;
void main()
{
    // COMMENT : Those lines of code don't work, the text is displayed but has no min/max size.
    //           You can comment it and replace
    //           p.x *= s.x and p.y *= s.y BY
    //           p.x *= scaling.x and p.y *= scaling.y
    vec2 scale = vec2( MatProjMod[0][0], MatProjMod[1][1] );
    vec4 p      = vec4( posXYtexST.xy , 0.0, 1.0 );
    p.x *= scaling.x;
    p.y *= scaling.y;
    p.xy += pos;
    gl_Position = MatProjMod * p;
    texcoord    = posXYtexST.zw;
}
/*_fragment_*/
//#version 450
#ifndef GL_ES
//#version 450
#endif
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
#endif

varying vec2 texcoord;
uniform sampler2D texture;
uniform vec3 glyph_color;
const float glyph_center   = 0.50;
uniform bool activate_outline;
uniform vec3 outline_color;
const float outline_center = 0.55;
uniform vec3 glow_color;
const float glow_center    = 1.25;
void main()
{
  vec4 c = texture2D(texture, texcoord);
  vec3 rgb;

  float dist  = c.a;
  float width = fwidth(dist);
  float alpha = smoothstep(glyph_center-width, glyph_center+width, dist);

  if(activate_outline) {
  //Outline
  float mu = smoothstep(outline_center-width, outline_center+width, dist);
  if (dist<outline_center+width && dist>outline_center-width) {
    rgb = outline_color;
  } else {
    rgb = glyph_color;
  }
  gl_FragColor = vec4(rgb, max(alpha,mu));
    } else {
      gl_FragColor = vec4(glyph_color, alpha);
  }

  // Glow+outline
//  vec3 rgb = mix(glow_color, glyph_color, alpha);
//  float mu = smoothstep(glyph_center, glow_center, sqrt(dist));
//  c = vec4(rgb, max(alpha,mu));
//  float beta = smoothstep(outline_center-width, outline_center+width, dist);
//  rgb = mix(outline_color, c.rgb, beta);
//  gl_FragColor = vec4(rgb, max(c.a,beta));
}

