/*_vertex_*/
//#version 450
#version 410
#ifndef GL_ES
#endif
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
#endif

#define pi 3.141592653589793238462643383279

const int RESERVED_UNIFORM = 16;

struct nodeView
{
   float weight;       // [0].x
   float gapBefore;    // [0].y
   float weightBefore; // [0].z
   float x;            // [0].w

   float isGap;        // [1].x
   float distortion;   // [1].y
   float distribFrom;  // [1].z
   float distribTo;    // [1].w

   vec4  color;        // [2]

   float subWeight;    // [3].x
   float invert;       // [3].y
   vec2  _padding;     // [3].zw
};
const int SIZEOF_NODE = 4; // sizeof(nodeView) / sizeof(vec4)
const int MAX_NODES = (gl_MaxVertexUniformVectors - RESERVED_UNIFORM) / SIZEOF_NODE;


//Attributes
#ifndef GL_ES
in vec2 Vertex;
#endif
#ifdef GL_ES
attribute vec2 Vertex;
#endif


// Uniform
uniform vec4 Data[SIZEOF_NODE * MAX_NODES];//nbVec4 ds NodeStruct * nb max Vertex
uniform mat4 MatProjMod;
uniform float Height;
uniform float Width;
uniform float NodeWidth;
uniform float Gap;

vec4 nodeRect(nodeView n) // bottom left corner (x, y, width, height)
{
   vec4 r = vec4( n.x * (NodeWidth + Width),
                  n.weightBefore * (Height - Gap*Height) + n.gapBefore * (Gap*Height),
                  NodeWidth,
                  n.weight * (Height - Gap*Height)
      );

   float h = (n.isGap == 1.) ? Height : Height - (Gap*Height);

   float invY = h - r.y - r.w;
   float dy   = invY - r.y;

   r.y = r.y + n.invert*dy;

   if (n.isGap == 0.)
      r.y += (Height*Gap) / 2.;

   return r;
}

float inv(float x, float line)  // line: ]0 ; +inf[
                                //       ]angle droit ; ligne droite[
{
   float l = 1. + (1. / ( -(1./line) -1. ));
   return line * (-(1./(l*x - 1.)) -1.);
}

float endFreq   (float x) { return 1. - inv(x, 0.1); }
float startFreq (float x) { return endFreq(1.-x);     }
float freqSmooth(float x) { return endFreq(x) * startFreq(x); }

float freq(float x, float f) {
   return ( sin(2.*x*2.*pi)*f + 1. ) / 2.;
}


void main(void)
{
   int id = int(floor(Vertex.y+0.5));
   int s = id * SIZEOF_NODE;

   nodeView n = nodeView (
      Data[s+ 0].x,
      Data[s+ 0].y,
      Data[s+ 0].z,
      Data[s+ 0].w,

      Data[s+ 1].x,
      Data[s+ 1].y,
      Data[s+ 1].z,
      Data[s+ 1].w,

      Data[s+ 2],

      Data[s+ 3].x,
      Data[s+ 3].y,
      Data[s+ 3].zw
      );

   vec4 r = nodeRect(n);
   vec4 position = vec4( r.x + freq(Vertex.x, n.distortion) * r.z,
                         r.y + Vertex.x * r.w,
                         1., 1.);

   gl_Position = MatProjMod * position;
}

/*_fragment_*/
////#version 410
#ifndef GL_ES
#endif
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
#endif

void main(void)
{
   gl_FragColor = vec4(0.,0.,0.,1.);
}
/*_end_*/
