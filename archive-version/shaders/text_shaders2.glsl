/*_vertex_*/
//#version 410
#ifndef GL_ES
#endif
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
#endif

uniform   mat4 MatProjMod;
//uniform   mat4 MatModView;
uniform   vec2 scaling;
uniform   vec2 pos;
uniform   vec4 res;
uniform  float rotAngle;
#ifndef GL_ES
in vec4 posXYtexST;
out vec2 texcoord;
#endif
#ifdef GL_ES
attribute vec4 posXYtexST;
varying   vec2 texcoord;
#endif

void main()
{
    //project 1pt to get its size
    float pointSize = length(MatProjMod*vec4(0.0,1.0,0.0,0.0));
    float minTxtSize = res.z;
    float maxTextSize = res.w;
    float textPtSize = clamp(pointSize*res.y, minTxtSize, maxTextSize);
    float scale = 0.5;

    vec4 p = vec4( posXYtexST.xy , 1.0, 1.0 );
    p.x *= scale;
    p.y *= scale;
    vec2 tmp = p.xy;
    p.x = cos(rotAngle)*tmp.x - sin(rotAngle)*tmp.y;
    p.y = sin(rotAngle)*tmp.x + cos(rotAngle)*tmp.y;
    p.xy += pos;
    gl_Position = MatProjMod * p;
    texcoord    = posXYtexST.zw;
}
/*_fragment_*/
//#version 410
#ifndef GL_ES
#endif
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
#endif


#ifndef GL_ES
in vec2 texcoord;
#endif
#ifdef GL_ES
varying vec2 texcoord;
#endif

uniform sampler2D texture;
uniform vec3 glyph_color;
uniform bool activate_outline;
uniform vec3 outline_color;
uniform vec3 glow_color;

const float glyph_center   = 0.50;
const float outline_center = 0.55;
const float glow_center    = 1.25;

void main()
{
  vec4 c = texture2D(texture, texcoord);
  vec3 rgb;

  float dist  = c.a;
  float width = fwidth(dist);
  float alpha = smoothstep(glyph_center-width, glyph_center+width, dist);

  if(activate_outline)
  {
     /* Glow+outline */
     vec3 rgb = mix(glow_color, glyph_color, alpha);
     float mu = smoothstep(glyph_center, glow_center, sqrt(dist));
     c = vec4(rgb, max(alpha,mu));
     float beta = smoothstep(outline_center-width, outline_center+width, dist);
     rgb = mix(outline_color, c.rgb, beta);
     gl_FragColor = vec4(rgb, max(c.a,beta));
  }
  else
     gl_FragColor = vec4(glyph_color, alpha);
}

