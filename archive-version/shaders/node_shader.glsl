/*_vertex_*/
//#version 410
//#version 410
#ifndef GL_ES
#endif
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
#endif


struct nodeView
{
    float weight;       // [0].x
    float gapBefore;    // [0].y
    float weightBefore; // [0].z
    float x;            // [0].w

    float isGap;        // [1].x
    float distortion;   // [1].y
    float distribFrom;  // [1].z
    float distribTo;    // [1].w

    vec4  color;        // [2]

    float subWeight;    // [3].x
    float invert;       // [3].y
    float gapRatio;     // [3].z
    float  _padding;     // [3].w
};


const int SIZEOF_NODE = 4; // sizeof(nodeView) / sizeof(vec4)
const int RESERVED_UNIFORM = 16;
const int MAX_VERTEX_UNIFORM = gl_MaxVertexUniformVectors/2 - RESERVED_UNIFORM;

const vec4  BEHIND_COLOR = vec4(0.95);
const float BRIGHTEN = -0.25;

/* Uniforms */
uniform vec4 Data[MAX_VERTEX_UNIFORM];

uniform mat4 MatProjMod;
uniform vec2 Res;

uniform float Height;
uniform float Width;
uniform float NodeWidth;
uniform float Gap;

uniform int ColorPicking;
uniform int Behind;
uniform float ForceVisibility;
/* END Uniforms */

#ifndef GL_ES
/* Attributes */
in vec3 Vertex;

/* Varyings */
out vec4  Color;
out vec2  Pos;
out vec2  Pixels;
out float SubWeight;
out float Index;
out float behind;
out float Invert;
#endif
#ifdef GL_ES
/* Attributes */
attribute vec3 Vertex;

/* Varyings */
varying vec4  Color;
varying vec2  Pos;
varying vec2  Pixels;
varying float SubWeight;
varying float Index;
varying float behind;
varying float Invert;
#endif



float pixelHeight(vec2 resolution, mat4 projection)
{
    vec4  a = projection * vec4(0.,0., 0.,1.);
    vec4  b = projection * vec4(0.,1., 0.,1.);
    float d = abs(a.y - b.y);

    float  normHeight = (2./resolution.y);

    return normHeight / d;
}

vec4 nodeRect(nodeView n) // bottom left corner (x, y, width, height)
{
    vec4 r = vec4( (n.x+n.gapRatio)* (NodeWidth + Width),
                   n.weightBefore * (Height - Gap*Height) + n.gapBefore * (Gap*Height),
                   NodeWidth,
                   n.weight * (Height - Gap*Height)
                   );

    float h = (n.isGap == 1.) ? Height : Height - (Gap*Height);

    float invY = h - r.y - r.w;
    float dy   = invY - r.y;

    r.y = r.y + n.invert*dy;

    if (n.isGap == 0.)
        r.y += (Height*Gap) / 2.;

    return r;
}


void main(void)
{
    int id = int(floor(Vertex.z));
    int s = id * SIZEOF_NODE;
    Index = float(s);
    nodeView n = nodeView (
                Data[s+ 0].x,
            Data[s+ 0].y,
            Data[s+ 0].z,
            Data[s+ 0].w,

            Data[s+ 1].x,
            Data[s+ 1].y,
            Data[s+ 1].z,
            Data[s+ 1].w,

            Data[s+ 2],

            Data[s+ 3].x,
            Data[s+ 3].y,
            Data[s+ 3].z,
            Data[s+ 3].w
            );

    float pix        = pixelHeight(Res, MatProjMod);
    float minHeight  = pix*2.;

    vec4 r = nodeRect(n);
    vec4 position = vec4( r.x + Vertex.x * r.z,
                          r.y + Vertex.y * max(minHeight, r.w),
                          n.weightBefore, 1.);
    vec4 color =  float(Behind)*BEHIND_COLOR + float(1-Behind)*n.color;

    position.z += 10.*ForceVisibility;//10.*float(Behind);

    Color       = color;
    Pos         = vec2(Vertex.x, Vertex.y);
    Pixels      = vec2(r.z/pix, r.w/pix);

    SubWeight   = n.subWeight / n.weight;
    behind = float(Behind);
    Invert = n.invert;
    gl_Position = MatProjMod * position;
}





/*_fragment_*/
//#version 410
#ifndef GL_ES
#endif
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
#endif

const int NB_DISTR = 10;
const int RESERVED_UNIFORM = 16;
const int MAX_FRAGMENT_UNIFORM = gl_MaxFragmentUniformVectors - RESERVED_UNIFORM;
const float EPS = 1E-2;
const vec4  BEHIND_COLOR = vec4(0.95);
/* Uniforms */
uniform vec4 DData[MAX_FRAGMENT_UNIFORM];
uniform int ColorPicking;
uniform int EnableHistogram;

#ifndef GL_ES
/* Varyings */
in vec4  Color;
in vec2  Pos;
in vec2  Pixels;
in float SubWeight;
in float Index;
in float behind;
in float Invert;
#endif
#ifdef GL_ES
/* Varyings */
varying vec4  Color;
varying vec2  Pos;
varying vec2  Pixels;
varying float SubWeight;
varying float Index;
varying float behind;
varying float Invert;
#endif

struct distribView
{

    float distrib[NB_DISTR];
    float nbElement;
    float isCool;

    float isCool2;
    float isCool3;
    float isCool4;
    float isCool5;
};

float easeInOut(float x, float line)
{
    return ( atan((x-0.5) * line)
             / (-2. * atan(-0.5*line))
             + 0.5 );
}


void main(void)
{
    float alpha = Color.a;

    if (ColorPicking != 0){
        //        Color.a = 1.;
        gl_FragColor = vec4(Color.rgb,1.);
    }
    else
    {
        vec4 color;
        if (EnableHistogram == 0)
        {
            if (SubWeight >= 0.)
                color = (Pos.y > SubWeight) ? 0.6*Color : mix( vec4(1.,1.,1.,1.), Color, 0.5 );
            else
                color = Color;
        }
        else
        {
           int idx = int(floor(Index+0.5));
            distribView distr;
            for (int i=0;i<=MAX_FRAGMENT_UNIFORM;++i){
                if (i==idx){
                    distr.distrib[0] = DData[i].x;
                    distr.distrib[1] = DData[i].y;
                    distr.distrib[2] = DData[i].z;
                    distr.distrib[3] = DData[i].w;

                    distr.distrib[4] = DData[i+1].x;
                    distr.distrib[5] = DData[i+1].y;
                    distr.distrib[6] = DData[i+1].z;
                    distr.distrib[7] = DData[i+1].w;

                    distr.distrib[8] =  DData[i+2].x;
                    distr.distrib[9] =  DData[i+2].y;
                    distr.nbElement = DData[i+2].z;
                    break;
                }
            }
            float y = distr.nbElement*Pos.y;
            if(Invert > 0.9){
                y = distr.nbElement*(1.-Pos.y);
            }
            int i = int(floor( y ));

            i = int(min(float(i), distr.nbElement-1.));

            float distrib1;
            for (int x = 0; x < 10; x++) {
                if (x == i) {
                    distrib1 = distr.distrib[x];
                    break;
                }
            }
            float shift = 1.;
            if((y - floor(y)) < 0.5)
                shift = -1.;
            float dy = (y - floor(y))*2. - 1.;
            int   j  = i + int(sign(dy));
            int i2 = int( min(distr.nbElement-1., max(float(j), 0.)) );

            if(i2 != i){
                float distrib2;
                for (int x = 0;  x< 10; x++) {
                    if (x == i2) {
                        distrib2 = distr.distrib[x];
                        break;
                    }
                }
                distrib1 = mix(distrib1, distrib2, easeInOut( abs(dy)/2., 6. ));
            }

            if( Invert > 0.1 && Invert < 0.9){
                color = Color;
                color.a = 1.;
            }
            else {

                vec4 dcolor = mix( vec4(1.,1.,1.,1.), Color, distrib1*0.6 );
                if(Invert > 0. && Invert <  0.1){
                    color = (abs((Pos.x*2.)-1.) > distrib1) ? Color : mix( Color, dcolor, (0.1-Invert)/(0.1-EPS) );
                }
                else if (Invert > 0.9 && Invert < 1.)
                    color = (abs((Pos.x*2.)-1.) > distrib1) ? Color : mix( Color, dcolor, 1.-(1.-Invert) / (1.- 0.9)) ;
                else {
                    dcolor.a = 1.;
                    color = (abs((Pos.x*2.)-1.) > distrib1) ? Color :  dcolor;
                }
                color = float(behind)*BEHIND_COLOR + float(1.-behind)*color;
                color.a = 1.;
            }

        }

        vec2 dPix = (vec2(1.,1.)-abs(Pos*2. - vec2(1.,1.)))*Pixels*0.5;
        color = mix(color, vec4(1.,1.,1.,1.), smoothstep(2.,  0., dPix.x));
        color = mix(color, vec4(1.,1.,1.,1.), smoothstep(1.,  0., dPix.y));

        if (SubWeight >= 0.)
            color = (Pos.y > SubWeight) ? mix( vec4(1.,1.,1.,1.), color, 0.5 )/*0.6*color*/ : color;//mix( vec4(1.,1.,1.,1.), color, 0.5 );


        color.a = alpha;
        gl_FragColor = color;
    }
}
/*_end_*/
