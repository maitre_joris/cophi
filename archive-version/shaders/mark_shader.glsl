/*_vertex_*/
//#version 410
#ifndef GL_ES
#endif
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
#endif


// Uniform
uniform vec2 Res;  //screen res. used to compute the size of 1 px 4xy
uniform mat4 MatProjMod; // modelview * projection matrix
uniform float Height;
uniform float Width;
uniform float NodeWidth;
uniform float Gap;

const float PADDING = 20.;


//Attributes
#ifndef GL_ES
in vec3 Vertex;
#endif
#ifdef GL_ES
attribute vec3 Vertex;
#endif


float pixelHeight(vec2 resolution, mat4 projection)
{
    vec4  a = projection * vec4(0.,0., 0.,1.);
    vec4  b = projection * vec4(0.,1., 0.,1.);
    float d = abs(a.y - b.y);

    float  normHeight = (2./resolution.y);

    return normHeight / d;
}


//========
void main(void)
{
   float pix = pixelHeight(Res,MatProjMod);
   float padding = 0.3;//pix * PADDING;

   // Vertex.z : isGap
   float y;
//   if (Vertex.z == 1.)
//   {
//      float h = (Height + padding*2.)/2.;
//      y = Vertex.y * h + Height/2.;
//   }
//   else
//   {
//      float h = (Height + padding*2. - Gap*Height)/2.;
//      y = Vertex.y * h + Height/2.;
//   }

   float h = (Height + padding*2.)/2.;
   y = Vertex.y * h + Height/2.;


   //Color = vec4(0.7,0.7,0.7,1);
   gl_Position = MatProjMod * vec4(Vertex.x * (Width+NodeWidth) + NodeWidth/2.,
                                   y,
                                   -50., 1.);
}

/*_fragment_*/
//#version 410
#ifndef GL_ES
#endif
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
#endif

void main(void) {
   gl_FragColor = vec4(0.,0.,0.,1.);//vec4(0.6,0.6,0.6,1.);
    // TODO: on peut faire des pointillés avec le fragment shader, ça serai mieux
}
/*_end_*/
