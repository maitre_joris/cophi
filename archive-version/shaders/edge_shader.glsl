/*_vertex_*/
//#version 410
#ifndef GL_ES
#endif
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
#endif


struct edgeView
{
   float sourcePos; // [0].x
   float destPos;   // [0].y

   float weight;    // [0].z
   float z;         // [0].w

   vec3  idColor;   // [1].xyz
   float subWeight; // [1].w
};

struct nodeView
{
   float weight;       // [0].x
   float gapBefore;    // [0].y
   float weightBefore; // [0].z
   float x;            // [0].w

   float isGap;        // [1].x
   float distortion;   // [1].y
   float distribFrom;  // [1].z
   float distribTo;    // [1].w

   vec4  color;        // [2]

   float subWeight;    // [3].x
   float invert;       // [3].y
   float gapRatio;     // [3].z
   float _padding;     // [3].w
};
const int SIZEOF_EDGE = 2;// sizeof(edgeView) / sizeof(vec4);
const int RESERVED_UNIFORM = 16;
const int SIZEOF_NODE = 4; // sizeof(nodeView) / sizeof(vec4)
const int MAX_EDGES = (gl_MaxVertexUniformVectors - RESERVED_UNIFORM) / (SIZEOF_EDGE + 2*SIZEOF_NODE);

const vec4 BEHIND_COLOR = vec4(0.95);
const float BRIGHTEN = -0.25;

// Uniform
uniform vec4 Data[MAX_EDGES * (SIZEOF_EDGE + 2*SIZEOF_NODE)];

uniform mat4 MatProjMod;
uniform vec2 Res;

uniform float Height;
uniform float Width;
uniform float NodeWidth;
uniform float Gap;

uniform float Curvature;
uniform float EdgeRed;
uniform float Tubular;

uniform int ColorPicking;
uniform int Behind;

#ifndef GL_ES
//Attributes (in)
in vec3 Vertex;
// varying (out)
out vec4  Color;
out float Angle;
out float SubWeight;
out float hq;
#endif
#ifdef GL_ES
//Attributes
attribute vec3 Vertex;
// varying
varying vec4  Color;
varying float Angle;
varying float SubWeight;
varying float hq;
#endif


float inv(float x, float line)  // line: ]0 ; +inf[
                                //       ]angle droit ; ligne droite[
{
   float l = 1. + (1. / ( -(1./line) -1. ));
   return line * (-(1./(l*x - 1.)) -1.);
}

float animateEdgePos(float t) // NOTE: pas très propre
                              // nodeView.invert est déjà animé dans Render::Tick ...
{
   if (t >= 0.6)
      return (t-0.6) / 0.4;
   else
      return 0.;
}

float pixelHeight(vec2 resolution, mat4 projection)
{
    vec4  a = projection * vec4(0.,0., 0.,1.);
    vec4  b = projection * vec4(0.,1., 0.,1.);
    float d = abs(a.y - b.y);

    float  normHeight = (2./Res.y);

    return normHeight / d;
}

vec4 nodeRect(nodeView n) // bottom left corner (x, y, width, height)
{
   vec4 r = vec4( n.x * (NodeWidth + Width),
                  n.weightBefore * (Height - Gap*Height) + n.gapBefore * (Gap*Height),
                  NodeWidth,
                  n.weight * (Height - Gap*Height)
      );

   float h = (n.isGap == 1.) ? Height : Height - (Gap*Height);

   float invY = h - r.y - r.w;
   float dy   = invY - r.y;

   r.y = r.y + n.invert*dy;

   if (n.isGap == 0.)
      r.y += (Height*Gap) / 2.;

   return r;
}

const float d = 0.3;

//========
void main(void)
{
   int id = int(floor(Vertex.z));
   int s  = id * (SIZEOF_EDGE + 2*SIZEOF_NODE);

    edgeView e = edgeView (
       Data[s+ 0].x,
       Data[s+ 0].y,
       Data[s+ 0].z,
       Data[s+ 0].w,
       Data[s+ 1].xyz,
       Data[s+ 1].w
    );
    nodeView n1 = nodeView (
       Data[s+ 2].x,
       Data[s+ 2].y,
       Data[s+ 2].z,
       Data[s+ 2].w,

       Data[s+ 3].x,
       Data[s+ 3].y,
       Data[s+ 3].z,
       Data[s+ 3].w,

       Data[s+ 4],

       Data[s+ 5].x,
       Data[s+ 5].y,
       Data[s+ 5].z,
       Data[s+ 5].w
       );
    nodeView n2 = nodeView (
       Data[s+ 6].x,
       Data[s+ 6].y,
       Data[s+ 6].z,
       Data[s+ 6].w,

       Data[s+ 7].x,
       Data[s+ 7].y,
       Data[s+ 7].z,
       Data[s+ 7].w,

       Data[s+ 8],

       Data[s+ 9].x,
       Data[s+ 9].y,
       Data[s+ 9].z,
       Data[s+ 9].w
       );

   float dSourcePos = 1. - e.sourcePos - e.sourcePos;
   float dDestPos   = 1. - e.destPos   - e.destPos;

   e.sourcePos = e.sourcePos + dSourcePos * animateEdgePos(n2.invert);
   e.destPos   = e.destPos   + dDestPos   * animateEdgePos(n1.invert);

   /* Bézier Curve parameters */
   float width = e.weight * (Height - Gap*Height);
   vec4 r1 = nodeRect(n1);
   vec4 r2 = nodeRect(n2);

   vec2 origin      = vec2( r1.x + NodeWidth, r1.y + e.sourcePos * r1.w );
   vec2 destination = vec2( r2.x            , r2.y + e.destPos   * r2.w );

   vec2 control1    = vec2( origin.x      + Curvature*Width, origin.y      );
   vec2 control2    = vec2( destination.x - Curvature*Width, destination.y );


   /* Computing Bézier Curve position */
   float t = Vertex.x;

   vec2 c = 3. * (control1 - origin);
   vec2 b = 3. * (control2 - control1) - c;
   vec2 a = destination - origin - c - b;

   vec2 position = a*t*t*t + b*t*t + c*t + origin;


   /* Computing Bézier Curve tangent */
   vec2  derivative  = a*3.*t*t + b*2.*t + c;
   vec2  tangent     = vec2(-derivative.y, derivative.x);
   vec2  normTangent = tangent / sqrt(tangent.x*tangent.x + tangent.y*tangent.y);


   /* Width Factor */
   float wf;
   if (t <= d)
      wf = 1. - (t/d);
   else if (t >= 1.-d)
      wf = (t-(1.-d)) / d;
   else
      wf = 0.;

   float reducedWidth = width * EdgeRed;
   if (reducedWidth > width) reducedWidth = 0.99*width;
   hq=reducedWidth;

   float widthFactor = 1. + ( inv(wf, 0.1) * ( (width/reducedWidth)-1. ) );

   float minWidth = pixelHeight(Res, MatProjMod) * 3.;
   float localWidth = max(widthFactor * reducedWidth, minWidth);

   position += Vertex.y * normTangent * (localWidth/2.);

   position.x = min( max(r1.x+NodeWidth, position.x), r2.x );

   /* Color interpolation */
   vec4 localColor = ((1.-t) * n1.color + t * n2.color);
   vec4 hPosition = vec4(position, e.z, 1.);

   float angle;
   localColor = float(Behind)*BEHIND_COLOR + float(1-Behind)*localColor;
   hPosition.z -= 100.*float(Behind);
   angle = float(1-Behind)*Vertex.y;

   localColor.a = (n1.color.a < n2.color.a) ? n1.color.a : n2.color.a;//1.;

   SubWeight = e.subWeight / e.weight;
   Angle = angle;
   Color = (ColorPicking != 0) ? vec4(e.idColor, 1.) : localColor;
   gl_Position = MatProjMod * hPosition;
}

/*_fragment_*/

//#version 410
#ifndef GL_ES
in vec4  Color;
in  float Angle;
in  float SubWeight;
in float hq;
#endif
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
varying  vec4  Color;
varying  float Angle;
varying  float SubWeight;
varying float hq;
#endif

uniform  float Tubular;
const vec4 BEHIND_COLOR = vec4(0.7,0.7,0.7,.8);


void main(void)
{
    float alpha = Color.a;
    float newAngle = abs( sin(abs(acos(Angle))));
    vec4 color = Color * abs( sin(abs(acos(Angle))) * Tubular + (1.-Tubular) );
    color.a = alpha;

    if (SubWeight >= 0. && (Angle+1.)/2. > SubWeight){
        color = BEHIND_COLOR;
    }
    else {
        float t = (Angle + 1.)/2.;
        t=abs(Angle);
        float d = t*hq;
        float w = fwidth(d);
        float alph = 1.-smoothstep(hq-w,hq,d);
        color.a *= alph;
    }/*
    if (SubWeight >= 0. && (Angle+1.)/2. > SubWeight)
       color = Color * 0.2;

    float t = (Angle + 1.)/2.;
    t=abs(Angle);
    float d = t*hq;
    float w = fwidth(d);
    float alph = 1.-smoothstep(hq-w,hq,d);
    color.a *= alph;*/
    gl_FragColor = color;
   // return;
}
/*_end_*/
