/*_vertex_*/
//#version 450
//#version 410
#ifndef GL_ES
#endif
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
#endif

//Attributes
attribute vec3 Arrow;
attribute vec2 Vertex;


// Uniform
uniform vec2 Res;  //screen res. used to compute the size of 1 px 4xy
uniform mat4 MatProjMod; // modelview * projection matrix
uniform float Height;
uniform float Width;
uniform float NodeWidth;
uniform float Gap;


const float MARGIN = 20.;
const float HEIGHT = 16.;
const float WIDTH  = 7.;

// varying
//varying vec4 Color; //   fill color of the node

float pixelHeight(vec2 resolution, mat4 projection)
{
    vec4  a = projection * vec4(0.,0., 0.,1.);
    vec4  b = projection * vec4(0.,1., 0.,1.);
    float d = abs(a.y - b.y);

    float  normHeight = (2./resolution.y);

    return normHeight / d;
}


//========
void main(void)
{
   float pix = pixelHeight(Res,MatProjMod);
   float margin = 0.3;//pix * MARGIN;
   float width  = min(pix * WIDTH, (Width+NodeWidth)/2.);
   float height = width / (WIDTH/HEIGHT);


   // Vertex.z : isGap
   float baseY;
//   if (Arrow.z == 1.)
//   {
//      float h = (Height + margin*2.)/2.;
//      baseY = Arrow.y * h + Height/2.;
//   }
//   else
//   {
//      float h = (Height + margin*2. - Gap*Height)/2.;
//      baseY = Arrow.y * h + Height/2.;
//   }
   float h = (Height + margin*2.)/2.;
   baseY = Arrow.y * h + Height/2.;

   //Color = vec4(0.7,0.7,0.7,1);
   gl_Position = MatProjMod * vec4(Arrow.x * (Width+NodeWidth) + NodeWidth/2. + Vertex.x * width,
                                   baseY + Vertex.y * height,
                                   -50., 1.);
}

/*_fragment_*/
//#version 450
//#version 410
#ifndef GL_ES
#endif
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
#endif

//varying  vec4 Color;

void main(void) {
  // gl_FragColor = vec4(0.7,0.7,0.7,1.);
     gl_FragColor = vec4(0.,0.,0.,1.);
    // TODO: on peut faire des pointillés avec le fragment shader, ça serai mieux
}
/*_end_*/
