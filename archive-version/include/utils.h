#ifndef UTILS_H
#define UTILS_H

//#pragma once

#include "Vector.h"
//#include <tulip/Color.h>
#include "ColorScale.h"

#include <glmatrix.h>
#include <vector>
#include <list>

std::string fileExtension(const std::string & path);

bool rectContains(const Vec4f r, const Vec2f p);
bool rectIntersects(const Vec4f a, const Vec4f b);
Vec4f screenRect(const GlMat4f & invProj);

void tokenize(const std::string& str,
              std::vector<std::string>& tokens,
              const std::string& delimiters = " ");


class ParseError {
public:
   int line;
   int col;
   std::string message;
   
   ParseError(const std::string & msg, int l=-1, int c=-1)
      : line(l), col(c), message(msg)
   {};
};
enum tokenType {
   TOKEN            = 1
   ,EMPTY_TOKEN     = (1 << 1)
   ,LAST_TOKEN_LINE = (1 << 2)
   ,LAST_TOKEN_FILE = (1 << 3)
   ,LINE_END        = (1 << 4)
   ,FILE_END        = (1 << 5)
};
tokenType readToken(std::istream &, std::string & result);
std::string readTokenOfType(std::istream &, int);



Vec4f to_gl(Color);

float easeInOut(float t);

template<typename T>
T between(T a, T x, T b) {
   return std::max(a, std::min(x, b));
}

bool isblank(const std::string & str);

template <typename T>
class vector_list
{
  private:
   unsigned chunk_size = (1000000 / 2) / sizeof(T); // par défaut on alloue par bloc de 0.5 Mo
   
  public:
   std::list< std::vector<T> > container;

   vector_list() {}
   vector_list(unsigned chunk) {
      set_chunk_size(chunk);
   }

   void init()
   {
      if (container.empty())
      {
         container.emplace_back();
         container.back().reserve(chunk_size);
      }
   }

   void clear(bool zero = true)
   {
      if (zero)
         container.resize(0);
      else
      {
         container.resize(1);
         container.back().clear();
         container.back().reserve(chunk_size);
         container.back().shrink_to_fit();
      }
   }
   
   T * add(unsigned n)
   {
      init();
      if ( container.back().size() + n > container.back().capacity() )
      {
         //std::cerr << "[ALLOC] " << std::flush;
         container.emplace_back();
         container.back().reserve(chunk_size);
      }

      container.back().resize( container.back().size() + n );
      return &( container.back().back() ) - (n-1);
   }

   void push_back(const T & value)
   {
      (* add(1)) = value;
   }

   void set_chunk_size(unsigned chunk) {
      if (chunk > 0)
         chunk_size = chunk;
   }
};


class Progress
{
private:
    bool     First = true;
    unsigned State;
    unsigned Max;

    std::string Label;

    static const unsigned DEFAULT_SIZE = 60;
public:
    Progress(const std::string & label, unsigned size = DEFAULT_SIZE) {
        init(label,size);
    }

    void init(const std::string & label, unsigned size = DEFAULT_SIZE) {
        if (not First)
            update(1);

        First = true;
        State = 0;
        Max   = size;
        Label = label;
    }

    template <typename T>
    void printn(const T & str, unsigned n)
    {
        if (n == 0)
            return;

        for (unsigned i=0; i<n; i++)
            std::cout << str;

        std::cout << std::flush;
    }

    void update(float t)
    {
        if (First)
        {
            First = false;
            std::cout << "\n" << Label << "\n|";
            printn('.', Max);
            std::cout << "|\n|" << std::flush;
        }

        t = std::min(t, 1.f);

        unsigned newState = t*Max;
        printn('>', newState - State);

        if (newState == Max and newState != State)
            std::cout << "|\n" << std::flush;

        State = newState;
    }
};


#endif
