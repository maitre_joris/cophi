#ifndef SHADERPROGRAM_H
#define SHADERPROGRAM_H

#include <string>

#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>


/**
 * @brief The NodeProgram class
 */
class ShaderProgram {
public:
    GLuint program;
protected:
    ShaderProgram(std::string filename);
private:
    std::string _name;
    GLuint createShader(const char source[], int type);
};



#endif // SHADERPROGRAM_H
