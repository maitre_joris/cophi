#ifndef _LABELRENDER_H_
#define _LABELRENDER_H_


#include <string>
#include "Rectangle.h"
//#include <tulip/QuadTree.h>
#include "Iterator.h"
//#include <tulip/LayoutProperty.h>
//#include <tulip/StringProperty.h>
#include <vector>
#include <map>

#include <texture-atlas.h>
#include <texture-font.h>

#include <glmatrix.h>

#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>

#include <shaderProgram.h>
#include <matrixManager.h>
#include <TextRenderer.h>



//TODO : Interface (?)
//TODO : Call the destructor in Graph2DRenderer.h, just a "delete labelRender" in order to free the memory used by atlas
/**
 * @class LabelRender
 * @brief This class provides several method to manage labels
 */
class LabelRender {
public:
    /**
    * @brief Constructor
    * Constructs an instance of LabelRender with empty lists
    */
    LabelRender(MatrixManager* manager, const std::string & font, float minSize, float maxSize);
    ~LabelRender();
    /**
    * @fn void occlusion( float ratio )
    * @brief Applies occlusion to labels
    * @param ratio With a ratio of 0 all labels are displayed
    * 1 is the default behavior, no overlap between labels
    * A bigger ratio means less label are displayed
    */
    void occlusion( float ratio = 1 );
    /**
    * @fn void draw()
    * @brief Draw the labels after that the occlusion has been applied
    */
    void draw();
    //void loadLabels(Graph *g);
    Label* getLabel(node n);
    MatrixManager* getMatrixManager() const;
    void setMinTextSize(const float min);
    void setMaxTextSize(const float max);

    void addLabel(const std::string & str, float x, float y, float z);
    Label* getLabel(unsigned id);
    void clear();
    void convertTextureToDistanceMap()
    {
        FontTextureManager* fontManager = FontTextureManager::getInstance();
        fontManager->convertTextureToDistanceMap(fontManager->getId(font));
    }


private:
    float minTextSize;
    float maxTextSize;
    std::string font;
    TextRenderer* renderer;
    MatrixManager* matrixMngr;
    std::vector<Label*> labels;
    std::vector<Label*> labelsToDraw;
    std::map<int, Label*> labelsMap;
};

#endif
