#ifndef ANIMATION_H
#define ANIMATION_H

#include <cmath>
#include <functional>

typedef std::function<float(const float)> easingFunction;

namespace tlp {

inline float easeLinear(const float t){ return t; }
inline float easeInCubic(const float t) { return std::pow(t, 3); }
extern easingFunction easeOutCubic;
extern easingFunction easeInOutCubic;
inline float easeOutElastic(const float t) {
    const float p = 0.3;
    return std::pow(2, -10*t) * std::sin((t-p/4) * (2*M_PI)/p) + 1;
}
extern easingFunction easeInElastic;
float easeOutBounce(const float t);


class Animation {
public:
    Animation(int duration);
    Animation(int duration, easingFunction f);
    virtual ~Animation() ;
    void start();
    void stop();
    virtual void advance() = 0;
    bool isFinished() const;
    bool isRunning() const;
    void extendDuration(int time);
    void resetDuration(int newDuration) { animationDuration = newDuration;}
protected:
    int animationDuration;
    int startTime;
    easingFunction ease;
private:
    bool running;
};

}

#endif // ANIMATION_H
