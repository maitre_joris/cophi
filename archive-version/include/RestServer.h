#ifndef RESTSERVER_H
#define RESTSERVER_H

#include <string>
#include <functional>
#include <vector>

#include "httprequest.h"

class RestServer {
public:
    RestServer(const std::string& hostname, const short port, const std::string& baseURL);
	//baseUrl pour paraCoord = "/ParaCoordService/ParaCoordServlet/"
    ~RestServer();
    void sendRequest(const std::string& requestPath, std::function<void(std::string)> callback);
    bool performRequests();
    void clearWaitingRequests();
protected:
    std::string hostname;
    short port;
    std::string baseURL;
    std::vector<HttpRequest*> requests;
};

#endif // RESTSERVER_H
