#ifndef DATA_H
#define DATA_H

//#pragma once

#include <RestServer.h>

#include "Node.h"
#include "Edge.h"
#include "vectorgraph.h"

#include <vector>
#include <map>
#include <iostream>
using namespace std;
const float MID_EDGE_POS = .5f;
class Data
{
    //friend class OldData; // dirty hack

public:

    struct subGraph {
        std::vector<node> nodes;
        std::vector<unsigned long long> nodesWeight;

        std::vector<edge> edges;
        std::vector<unsigned long long> edgesWeight;
    };

    struct dimensionProp{
        float pos;
        float min, max;
        float gapRatio;
        float norm;
        string label;
    };

    struct nodeProp {
        unsigned long long weight;
        int clusterId;
        float dimension;
        int db_dimension;
        float min, max;
        std::vector<float> distrib;

        float gapBefore;
        float weightBefore;
        float distortion;
    };
    struct edgeProp {
        unsigned long long weight;
        float sourcePos;
        float destPos;
        float z;
    };

    class Observer {
    public:
        virtual void OnRootGraph(Data & observable, bool center = true) = 0;
        virtual void OnSubGraph (Data & observable, int dimension, int clusterId, node clusterNode, const Data::subGraph & subgraph) = 0;
        virtual void OnSubGraph (Data & observable, edge e, const Data::subGraph & subgraph) = 0;
        //virtual void OnDelete(Data & observable) = 0;
        virtual ~Observer() {};
    };


private:
    RestServer* Server = nullptr;
    std::string RemoteDataSetHostName;
    int RemoteDataSetPort;
    std::string RemoteDataSetBaseURL;
    std::string RemoteDataSetPath;
    Observer* Observer_ = nullptr;

    std::vector<bool> Gap;


    VectorGraph RootGraph;
    unsigned long long NbIndividuals = 0;

    NodeProperty<subGraph> NSubGraphs;
    EdgeProperty<subGraph> ESubGraphs;

    NodeProperty<nodeProp> NodeProp;
    EdgeProperty<edgeProp> EdgeProp;


    //   std::vector<std::string> Labels;
    std::vector< std::map<int,node> > Dimensions;
    std::vector<dimensionProp> DimensionProp;

    std::map<int, int> DB2ClientDimNumber;
    std::map<int, int> Client2DBDimNumber;


    // int dimPos = 0;
    void Init();
    void Count();

    //typedef bool(RemoteData::*nodeAddFn)(int dimension,    int value, unsigned long long weight);
    typedef bool(Data::*edgeAddFn)(int srcDimension, int tgtDimension, int srcCluster, int destCluster, unsigned long long weight);
    typedef bool(Data::*parseNodeLineFn)(std::istream &);

    void ParseResponse(std::istream &, parseNodeLineFn, edgeAddFn);
    bool ParseRootNodeLine(std::istream &);
    bool ParseSubNodeLine(std::istream &);
    bool ParseEdgeLine(std::istream &, edgeAddFn);

    node AddRootNode(int db_dimension   , int clusterId, float min, float max, std::vector<float> distrib);
    //  bool AddRootEdge   (int srcDimension, int srcCluster, int destCluster, unsigned long long weight);
    bool AddRootEdge (int db_srcDimension, int db_tgtDimension, int srcCluster, int destCluster,  long long unsigned weight);
    subGraph* CurrentSubGraph = nullptr;
    bool AddSubNode(int dimension   , int clusterId, unsigned long long weight);
    //bool AddSubEdge(int srcDimension, int srcCluster, int destCluster  , unsigned long long weight);
    bool AddSubEdge(int db_srcDimension,int db_tgtDimension, int srcCluster, int destCluster, unsigned long long weight);
    struct compEdge {
        bool operator()(const std::pair<edge,float> & a,
                        const std::pair<edge,float> & b){
            return a.second < b.second;
        }
    };

    struct compNode {
        bool operator()(const node a, const node b){
            return prop[a].min < prop[b].min;
        }

        compNode(const NodeProperty<nodeProp> & np) : prop(np) {}
        const NodeProperty<nodeProp> & prop;
    };

    void PositionNodes();
    void PositionEdges();

    void ZOrderEdges();

    void NormalizeDistribPerNode();
    void NormalizeDistribPerDimension();

    void ClearSubGraphs();
public:
    Data();
    Data (Data * dataSrc,int dim);
    ~Data();
    void Clear();

    std::map<int, int>&  gClient2DBDimNumber(){return Client2DBDimNumber;}

    Data * GetDeletionSource(int DimClient);
    Data * GetInsertionSource(int DimDB, int Prev_Client, int Next_Client);
    void DeleteDimension(Data *DataTmpSource, int AnimatedDimension);
    void InsertDimension(Data * DataTmpSource, int DB_Dim, int Client_Prev, int Client_Next);
    //void InsertDimensions(int srcDim, int tgtDim, bool before = true);
    void SetRemoteDataSet(const std::string & hostname, const short port, const std::string & baseurl, const std::string & path, const std::string dimensions="");
    bool RequestSubGraph(int dimension, int clusterId);
    bool RequestSubGraph(node clusterNode) {
        const nodeProp & np = NodeProp[clusterNode];
        return RequestSubGraph(np.dimension,np.clusterId);
    }
    bool RequestSubGraph(edge e);

    bool PerformRequests();
    void setGapRatio(int d, const float x);
    std::string ToString() const;

    // GETTERS
    const VectorGraph & GetRootGraph() const {
        return RootGraph;
    }
    std::vector<std::map<int,node>> & GetDimensions() {
                                         return Dimensions;
}

                                         const std::map<int,node> & GetDimensions(int Dim) const {
        return Dimensions[Dim];
    }
    /*const std::vector<std::string> & GetLabels() const {
      return Labels;
   }*/
    const subGraph & GetSubGraph(int dimension, int clusterId) const {
        return NSubGraphs[ Dimensions.at(dimension).at(clusterId) ];
    }

    const subGraph & GetSubGraph(node n) const {
        return NSubGraphs[n];
    }

    const subGraph & GetSubGraph(edge e) const {
        return ESubGraphs[e];
    }

    const map<int, int> & GetDB2ClientDimNumber() const {
        return DB2ClientDimNumber;
    }
    map<int, int> & GetDB2ClientDimNumber() {
        return DB2ClientDimNumber;
    }
    int & GetDB2ClientDimNumber(int x)  {
        return DB2ClientDimNumber.at(x);
    }

    const int & GetDB2ClientDimNumber(int x) const {
        return DB2ClientDimNumber.at(x);
    }

    const map<int, int> & GetClient2DBDimNumber() const {
        return Client2DBDimNumber;
    }

    map<int, int> & GetClient2DBDimNumber() {
        return Client2DBDimNumber;
    }
    int & GetClient2DBDimNumber(int x)  {
        return Client2DBDimNumber.at(x);
    }
    const int & GetClient2DBDimNumber(int x) const {
        return Client2DBDimNumber.at(x);
    }
    nodeProp & GetNodeProp(node n) {

        return NodeProp[n];
    }
    NodeProperty<nodeProp> & GetNodeProp() {
        return NodeProp;
    }
    const edgeProp & GetEdgeProp(edge e) const {
        return EdgeProp[e];
    }
    unsigned long long GetNbIndividuals() const {
        return NbIndividuals;
    }
    bool IsGap(int dimension) const {
        return Gap[dimension];
    }
    dimensionProp& GetDimensionProp(unsigned int i) {
        return DimensionProp[i];
    }

    const dimensionProp& GetDimensionProp(unsigned int i) const{
        return DimensionProp[i];
    }
    vector<dimensionProp>& GetDimensionProp() {
        return DimensionProp;
    }
    // SETTERS
    void SetEdgeZ(edge e, float z) {
        EdgeProp[e].z = z;
    }
    void SetObserver(Observer & o) {
        Observer_ = &o;

        if (RootGraph.numberOfNodes() > 0)
            Observer_->OnRootGraph(*this);
    }
};
#endif
