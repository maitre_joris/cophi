/*
  Copyright 2015 Romain Bourqui  <romain.bourqui@labri.fr>
  Copyright 2015 Timothée Jourde <itim.lcf@gmail.com>
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// THIS CLASS IS NOT USED ANYMORE.
#pragma once

#include <utils.h>
#include <Data.h>

#include <tulip/Graph.h>
#include <tulip/ForEach.h>
#include <tulip/SimpleTest.h>
#include <tulip/ColorProperty.h>
#include <tulip/ColorScale.h>
#include <tulip/TulipPluginHeaders.h>

//#include <time.h>
#include <math.h>
#include <climits>
#include <fstream>
#include <sstream>
#include <vector>
#include <array>

class OldData
{
  private:
   
   struct comp_edge {
      bool operator()(const std::pair<tlp::edge,int> & a, const std::pair<tlp::edge,int> & b){
         return a.second < b.second;
      }
   };

   struct value
   {
      tlp::node node;
      std::vector<int*> individuals;
   };

   struct characteristic
   {
      std::string name;
      bool isGap;
      std::map<int,value> values;
   };

   tlp::Graph * Graph = nullptr;
   std::vector<characteristic> Characteristics;
   unsigned long long NbIndividuals = 0;
   vector_list<int> Individuals;

   tlp::LayoutProperty * LayoutProp;
   tlp::SizeProperty   * SizeProp;
   tlp::StringProperty * LabelProp;
   tlp::IntegerProperty * WeightProp;
   //tlp::DoubleProperty * NormWeightProp;
   tlp::IntegerProperty * ValueProp;
   tlp::IntegerProperty * SubGraphIdProp;
   tlp::ColorProperty   * ColorProp;

   tlp::DoubleProperty * GapProp;
   tlp::DoubleProperty * WeightBeforeProp;
   tlp::DoubleProperty * NormWeightProp;

   tlp::DoubleProperty * SourcePosProp;
   tlp::DoubleProperty * DestPosProp;
   

   float VerticalSpacing = 0.2;
   float Height          = 1;
   float HorizontalGap   = 1.3;
   float NodeWidth       = 0.05;
   
   void SetGraph(tlp::Graph *);
   void InitProp();

   void ProcessIndividual(int[]);

   void PositionNodes();
   void PositionEdges();
   void SortEdges(tlp::node, bool outEdges, std::map<tlp::edge, std::array<tlp::Coord,4>> & bends);

   void ComputeSubGraphs();
   void ApplyColorGradient();

   void FreeWorkingMemory();

  public:

   std::map<tlp::node,tlp::node> HackNodeMap;
   std::map<tlp::edge,tlp::edge> HackEdgeMap;

   void Clear();
   void ProcessCSV(const std::string & filename, const std::string & delimiters = ",");

   void ImportRemoteRootGraph(Data & data); // dirty hack
   void ImportRemoteSubGraph(int dimension, int clusterId, Data::subGraph & sub); // sub dirty hack

   // getters
   tlp::Graph & GetGraph() {
      return *Graph;
   };
   const std::vector<characteristic> & GetCharacteristics() {
      return Characteristics;
   };
   unsigned long long GetNbIndividuals() {
      return NbIndividuals;
   };

   
};

