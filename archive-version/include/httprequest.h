#ifndef HTTPREQUEST_H
#define HTTPREQUEST_H

#include <string>
#include <functional>

class HttpRequest
{
public:
    HttpRequest(std::string url, std::function<void(std::string)> callback);
    virtual ~HttpRequest() {}
    virtual bool perform() = 0;
    virtual bool isFinished() = 0;

    static HttpRequest* makeRequest(std::string url, std::function<void(std::string)> callback);
protected:
    std::string url;
    std::function<void(std::string)> onComplete;
};

#endif // HTTPREQUEST_H
