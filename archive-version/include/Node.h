#ifndef Tulip_NODE_H
#define Tulip_NODE_H
#include <climits>
#include <functional>
struct node {

    unsigned int id;

    node():id(UINT_MAX) {}


    explicit node(unsigned int j):id(j) {}

    operator unsigned int() const {
        return id;
    }

    bool operator!=(const node n) const {
        return id!=n.id;
    }

    bool operator==(const node n) const {
        return id==n.id;
    }

    bool isValid() const {
        return id!=UINT_MAX;
    }
};


namespace std {
template<> struct hash<node> {
    size_t operator()(const node n) const {return n.id;}
};

template<> struct equal_to<node> {
    size_t operator()(const node n,const node n2) const {
        return n.id==n2.id;
    }
};

template<> struct less<node> {
    size_t operator()(const node n,const node n2) const {
        return n.id<n2.id;
    }
};
}
#endif
