/*
  Copyright 2015 Timothée Jourde <itim.lcf@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//#pragma once

/* GL_OES_standard_derivatives */
#ifndef GL_OES_standard_derivatives
#define GL_OES_standard_derivatives 1
#endif

#ifndef WEBGL
#include <omp.h>
//#include <parallel/algorithm>
#endif

#include <glmatrix.h>
#include <shaderProgram.h>
//#include <TextRenderer.h>
#include <matrixManager.h>
#include <LabelRender.h>
#include <Data.h>
#include <utils.h>

#include "vectorgraph.h"
#include "Vector.h"
#include "ColorScale.h"

#include <iostream>
#include <cmath>
#include <cfloat>
#include <vector>
#include <map>

const float MIN_EDGE_WIDTH = 0.001;
const float MIN_NODE_SIZE  =  0.005;
// ATTENTION: 'extern "C" {}' ??
class Renderer : public Data::Observer
{
  private:

   ////////////////////////////////////////////////////////////////////////
   // STRUCTURES to be transmitted verbatim to the node and edge shaders //
   ////////////////////////////////////////////////////////////////////////

   struct nodeView
   {
      float weight;
      float gapBefore;
      float weightBefore;
      float x;     // [integer] characteristic/dimension index (thus abscissa position)

      float isGap; // [boolean] wether there is a gap on the axis
      float distortion;
      float distribFrom;
      float distribTo;

      Vec4f color;   // NOTE: a color gradient could easily be passed as a uniform and thus be dynamic

      float     subWeight;
      float     invert;  // [0;1] axe inversion (continous for the animation)
      float     gapRatio;
      float     padding;
   };

   struct distribView
   {

      float distrib[10];
      float nbElement;

      // juste pour que ça fasse la meme taille qu'un nodeview
      float isCool;
      float isCool2;
      float isCool3;
      float isCool4;
      float isCool5;
   };

   static const unsigned SIZEOF_NODE = sizeof(nodeView) / sizeof(Vec4f);

   // the edge shader will also receive 2 nodeView for each edgeView
   struct edgeView
   {
      float sourcePos; // [0;1] vertical position on the source node
      float destPos;   //                         on the destination node

      float weight;
      float z;

      Vec3f idColor; // for color picking
      float      subWeight;
   };
   static const unsigned SIZEOF_EDGE = sizeof(edgeView) / sizeof(Vec4f);

   // pour concatener 1 edgeView et 2 nodeView en mémoire au moment du dessin
   struct edgeNodeNode
   {
      edgeView edge;
      nodeView end1;
      nodeView end2;
   };

   // les arrêtes sont stockés sous cette forme
   struct edgeRefs
   {
      edgeView edge;
      unsigned end1;
      unsigned end2;
   };

   // référence un noeud ou une arrête en redéfinissant son poid
   struct ref
   {
       unsigned id;
       float    weight;
   };




   //////////////////////
   // SHADERS HANDLING //
   //////////////////////

   class NodeProgram : public ShaderProgram {
     public :
      NodeProgram();
      GLuint MatProjMod;
      GLuint Res;
      GLuint Data;
      GLuint DData;

      GLuint Height;
      GLuint Width; // distance between two axes
      GLuint NodeWidth;
      GLuint Gap;   // [0;1] part of Height dedicated to represent a gap between non-contigous values
      GLuint EnableHistogram;

      GLuint ColorPicking; // [boolean] color picking rendering
      GLuint Behind;       // [boolean] the node is in the background (not selected)
      GLuint ForceVisibilty;
   };

   class EdgeProgram : public ShaderProgram {
     public :
      EdgeProgram();
      GLuint MatProjMod;
      GLuint Res;
      GLuint Data;

      GLuint Height;
      GLuint Width;
      GLuint NodeWidth;
      GLuint Gap;

      GLuint Curvature; // [0;1] curvature of the edges
      GLuint EdgeRed; // reduction factor for middle part of edges

      GLuint ColorPicking;
      GLuint Behind;
      GLuint Tubular; // [0;1] tubular effect intensity
   };

   class MarkProgram : public ShaderProgram {
     public :
      MarkProgram();
      GLuint MatProjMod;
      GLuint Res;

      GLuint Height;
      GLuint Width;
      GLuint NodeWidth;
      GLuint Gap;
   };

   class ArrowProgram : public ShaderProgram {
     public :
      ArrowProgram();
      GLuint MatProjMod;
      GLuint Res;

      GLuint Height;
      GLuint Width;
      GLuint NodeWidth;
      GLuint Gap;
   };


   class FrequencyProgram : public ShaderProgram {
     public :
      FrequencyProgram();
      GLuint MatProjMod;
      //GLuint Res;
      GLuint Data;

      GLuint Height;
      GLuint Width;
      GLuint NodeWidth;
      GLuint Gap;
   };



   ////////////////
   // ATTRIBUTES //
   ////////////////

   Data * DataSource = nullptr;
   LabelRender AxeLabelRenderer;

   float Height    = 1.5;
   float Width     = 2;   // distance between 2 axes
   float Gap       = 0.2; // 0.3; // [0;1] part of Height dedicated to represent a gap between non-contigous values
   float NodeWidth = 0.3;
   float Curvature = 0.3; // [0;1] curvature of the edges
   float EdgeRed = 0.3;
   bool  EnableHistogram = true;
   Vec4f BackgroundColor = Vec4f(1., 1., 1., 1.);
   ColorScale ColorGradient;

   float FrontZ = 10;

   /* animations */
   int   InvertAxis  = -1;
   float InvertSince = FLT_MAX;
   float InvertDuration = 1.;
   bool  InvertInvert;
   std::vector<float> Invert;

   Data * DataTmpSource = nullptr;
   Data * DataMovingSource = nullptr;

   // Suppresion
   float DeleteDuration = 1.;
   bool DeletingDimension = false;
   bool AnimatingDeletion = false;
   float RemoveDimensionStep;
   int DeletedDimension;
   float DeletionDisplacementAnimation;
   float OldRemoveDimensionStep;

   // insertion
   float InsertDuration = 1.;
   int insertedDBDim = -1;
   int PrevDim = -1;
   int NextDim = -1;
   bool AnimatingInsertion = false;
   bool InsertingDimension = false;
   int InsertionAnimationStage = 0;
   int DeletionAnimationStage = 0;
   float InsertDimensionStep;
   float OldInsertDimensionStep;
   float InsertionDisplacementAnimation = 0.f;

   /* node size mapping */
   bool IntervalAsSize;
   float MappingAnimationTime;
   float StartAnimationTime;

   NodeProgram  NodeShader;
   EdgeProgram  EdgeShader;
   MarkProgram  MarkShader;
   ArrowProgram ArrowShader;
   FrequencyProgram FrequencyShader;

   MatrixManager Matrix;

//   LabelRender LabelRenderer;
   bool DisplayMarksAndArrows=true;
   bool DisplayLabels = true;
   bool hideEdges = false;
   //static const FONT = "../fonts/Vera.ttf";

   // structures
   std::vector<nodeView> Nodes;
   std::vector<ref>      SelectedNodes;
   std::vector<edgeRefs> Edges;
   std::vector<ref>      SelectedEdges;

   node FocusedSubGraph;
   edge FocusedEdge;

   // map view structures to Graph elements
   std::vector<node> NodeMap;
   std::vector<edge> EdgeMap;

   // buffers
   GLuint NodesBuffer;
   GLuint MarksBuffer;
   GLuint ArrowsBuffer;

   GLuint EdgesBuffer;
   GLuint EdgesElements;
   unsigned EdgeDefinition;

   GLuint FrequenciesBuffer;
   unsigned FrequencyDefinition;

   Vec4f * VertexUniformBuffer    = nullptr;
   Vec4f * FragmentUniformBuffer  = nullptr;
   //float * VertexUniformBuffer    = nullptr;
   //float * FragmentUniformBuffer  = nullptr;

   int MaxVertexUniform;
   int MaxFragmentUniform;
//   bool labelsGenerated = false;

   unsigned MaxEdges;
   unsigned MaxNodes;
   static const int RESERVED_UNIFORM = 16;

   /////////////
   // METHODS //
   /////////////
   nodeView NodeView(node, Data *dataSource ,bool colorPicking = false);
   edgeView EdgeView(edge);
   edgeNodeNode EdgeNodeNode(edge);
   Vec4f NodeRect(const nodeView & n) const;
   int PickId(int x, int y);
   static Vec3f idColor(const unsigned id);
   static float getT(unsigned i, unsigned curveDefinition, unsigned endsDefinition);

   void SetNodesBuffer();
   void SetEdgesBuffer(unsigned curveDefinition = 50, unsigned endsDefinition = 30);
   void SetMarksBuffer();
   void SetFrequenciesBuffer(unsigned definition = 200);
   void UpdateArrowsBuffer();
   void DrawNodes(Data * data, const std::vector<unsigned long long> * weightOverride, bool behind = false, bool colorPicking = false, float forceVisibility = 0.f);
   void DrawNodes(const std::vector<node> & nodes, const std::vector<unsigned long long> * weightOverride = nullptr, bool behind = false, bool colorPicking = false, float forceVisibility = 0.f);
   void DrawEdges(const std::vector<edge> & edges, const std::vector<unsigned long long> * weightOverride = nullptr, bool behind = false, bool colorPicking = false);
   void DrawMarks();
   void DrawArrows();
   void DrawFrequencies(const std::vector<node> & nodes);

  public:
   Renderer();
   ~Renderer();
   void UpdateLabels();
   int getId(int x, int y);
   tuple<int,int> getSurroundingDimension(float x, float y);
   int getDimension(float x, float y);
   LabelRender getAxeLabel(){
       return AxeLabelRenderer;
   };
   std::vector<nodeView> getNodes(){return Nodes;}
   void OnRootGraph(Data & d, bool center = true) override;
   void OnSubGraph (Data & observable, int dimension, int clusterId, node clusterNode, const Data::subGraph & subgraph) override;
   void OnSubGraph (Data & observable, edge e, const Data::subGraph & subgraph) override;

   //void OnDelete(Data & observable) override;
   void setColorGradient(vector<Color> colors){
       ColorGradient = ColorScale (colors, true);
   };

   void setGapRatio(int d);
   bool Tick(float time);
   void Draw(bool colorPicking = false);
   void setDataMovingSource(Data * dt);
   void updateMovingDimPosition(int x, int y);
   void HideLabels();
   void ShowLabels();

   void HideMarksAndArrows();
   void ShowMarksAndArrows();
   void LabelsOcclusion();

   void Zoom(float x, float y, const float scale);
   void Translate(float dx, float dy);
   void Center();
   void Fit();
   void ChangeViewport(const Vec4i);
   float WindowToModelFactor() const;


   Data * GetDataTmpSource();

   // Deletion of dimension
   bool IsAnimatingDeletion();
   void SetDeletionDataSource(Data *TmpSource, int Dim);
   void SetDeletionDataSource(Data * TmpSource, int x, int y);
   void AnimateDeletion(float Time);
   void SetDeletionDuration(float dur){
       DeleteDuration = dur;
   }
   int getDeletedDimension()const {return DeletedDimension;}
   int getinsertedDBDim()const {return insertedDBDim;}
   // insertion of dimension
   void SetInsertionDataSource(Data * TmpSource, int Dim_DB, int Prev_Client, int Next_Client);
   bool IsAnimatingInsertion(){ return InsertingDimension;}//return AnimatingInsertion;}
   void SetInsertionDuration(float dur){
       InsertDuration = dur;
   }
   void AnimateInsertion(float Time);

   const MatrixManager & GetMatrixManager();

   float GetHeight        () const { return Height;    }
   float GetWidth         () const { return Width;     }
   float GetNodeWidth     () const { return NodeWidth; }
   float GetTotalWidth    () const {
      if (DataSource == nullptr) return 0;

      return Width     * (DataSource->GetDimensions().size()-1);
   }
   float GetTotalNodeWidth() const {
      if (DataSource == nullptr) return 0;

      return NodeWidth * DataSource->GetDimensions().size();
   }
   float GetGap           () const { return Gap;       }
   float GetCurvature     () const { return Curvature; }
   float GetEdgeRed     () const { return EdgeRed; }

   void SetHeight        (const float);
   void SetWidth         (const float);
   void SetTotalWidth    (const float);
   void SetNodeWidth     (const float);
   void SetEdgeRed       (const float);
   void SetTotalNodeWidth(const float);
   void SetEdgeReduction (const float);
   void SetGap           (const float);
   void SetCurvature     (const float);
   void ToogleHistogram  () {
      EnableHistogram = not EnableHistogram;
   }
   void HistogramView  () {
      EnableHistogram = true;
   }
   void DistortionView  () {
      EnableHistogram = false;
   }

   void changeMapping(float time) {
        MappingAnimationTime = 0;
        StartAnimationTime = time;
       IntervalAsSize = !IntervalAsSize;
   }

   void InvertAt(float x, float y, float time);
   void SetInvertionDuration(float dur){
       InvertDuration = dur;
   }



   bool Select(int x, int y, bool nodesOnly = false, bool pullUpEdge = false);
   void EndSelect();


   void InsertDimension(int srcX, int srcY, int tgtX, int tgtY);
   bool getDisplayLabels() const;
   bool gethideEdges() const{return hideEdges;}
   void sethideEdges(bool b) {hideEdges = b;}
//   bool getLabelsGenerated() const;
//   void setLabelsGenerated(bool value);
};
