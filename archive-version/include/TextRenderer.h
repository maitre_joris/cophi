#ifndef TEXTRENDERER_H
#define TEXTRENDERER_H

#include <map>

#include <texture-atlas.h>
#include <texture-font.h>

#include "Vector.h"
#include "Rectangle.h"
#include "ColorScale.h"
#include "shaderProgram.h"
#include <matrixManager.h>


/**
  * @class TextProgram
  * @brief Derived class for handling the text shaders
  */
class TextProgram : public ShaderProgram {
public:
    TextProgram();
    GLuint posCoord;
    GLuint matProjMod;
    GLuint texture;
    GLuint pos;
    GLuint res;
    GLuint scaling;
    GLuint activateOutline;
    GLuint outlineColor;
    GLuint glowColor;
    GLuint textColor;
    GLuint rotation;
};

class FontTextureManager {
public:
    static FontTextureManager* getInstance() {
        if (!singleton)
            singleton = new FontTextureManager();
        return singleton;
    }

    ~FontTextureManager();

    texture_atlas_t* getAtlas(std::string fontName);
    texture_atlas_t* getAtlas(int fontId);
    texture_font_t* getTexture(std::string fontName);
    texture_font_t* getTexture(int fontId);

    int getId(std::string fontName);
    bool distanceMapComputed = false;
    std::string getName(int fontId);

    void convertTextureToDistanceMap(int fontId);

private:
    FontTextureManager();
    int currentIndex;
    std::map<std::string, int> fontMap;
    std::vector<texture_atlas_t*> atlasVector;
    std::vector<texture_font_t*> texturesVector;

    int loadFont(std::string fontName);
    static FontTextureManager* singleton;
};


class Label{
public:
    Label(std::string text, std::string font, float angle, Vec2f pos);
    Rectf viewBox;
    std::vector<Vec4f> data;
    GLuint vboID;
    Vec2f nodePos;
    Vec2f scaling;
    float rotation;
    float length;
    int font;

    void computeViewBox(const float maxTextSize, const float minTextSize, const Vec2f screenRes, const GlMat4f matProjMod, const float occlusionRatio);
    Rectf getBoundingBox();
    Rectf getAxisAlignedViewBox(float textSize, GlMat4f matProjMod);
    void setText(std::string text);
};



class TextRenderer {
public:
    TextRenderer(MatrixManager* manager);
    void draw(std::vector<Label*> &labels, float minTextSize, float maxTextSize);
    Label* createLabel(Vec3f position, float angle, std::string font, std::string text);
    float getLabelSize(Label* label) const;
    void setLabelSize(Label* label, float newSize);
    void computeLabelViewBox(Label* label);
private:
    MatrixManager* matrixMangr;
    TextProgram *textShaders;
    Color outlineColor;
    Color glowColor;
    Color textColor;
};



#endif // TEXTRENDERER_H
