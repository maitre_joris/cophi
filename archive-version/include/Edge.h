#ifndef Tulip_EDGE_H
#define Tulip_EDGE_H
#include <climits>
#include <functional>


struct edge {

    unsigned int id;

    edge():id(UINT_MAX) {}

    explicit edge(unsigned int j):id(j) {}

    operator unsigned int() const {
        return id;
    }

    bool operator==(const edge e) const {
        return id==e.id;
    }

    bool operator!=(const edge e) const {
        return id!=e.id;
    }

    bool isValid() const {
        return id!=UINT_MAX;
    }
};



//these three functions allow to use edge as a key in a hash-based data structure (e.g. hashmap).
namespace std {
template<>
struct hash<edge>{
    size_t operator()(const edge e) const {return e.id;}
};

template<>
struct equal_to<edge> {
    size_t operator()(const edge e,const edge e2) const {
        return e.id==e2.id;
    }
};

template<>
struct less<edge> {
    size_t operator()(const edge e,const edge e2) const {
        return e.id<e2.id;
    }
};
}
#endif

