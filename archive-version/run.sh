#!/usr/bin/env bash
set -e

mkdir -p Builds/release/
cd Builds/release/

qmake ../../src/paraCoord.pro
make -j4

cd ../../src/

export LD_LIBRARY_PATH=/usr/lib/x86_64-linux-gnu:/home/joris/Applications/tulip_dir/release_4.7/install/lib
#ulimit -s unlimited

../Builds/release/gview $*
