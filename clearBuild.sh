WEB_APP_DIR="web"
CORE_APP_DIR="multimode-version"
BUILD_DIRS=("buildClient" "buildStandalone" "buildBench" "jsbuild")
JS_OUTPUT_DIR=$WEB_APP_DIR"/src/bin"

# Product of cpp build
for buildDir in $BUILD_DIRS
 do 
  rm -vr $CORE_APP_DIR"/$buildDir"/* &> /dev/null
 done
rm -vr $CORE_APP_DIR/bin/* &> /dev/null

# Products of js build
rm -v $JS_OUTPUT_DIR/* &> /dev/null

# Product of gulp ui build
rm -v $WEB_APP_DIR/public/index.html $WEB_APP_DIR/public/*js* $WEB_APP_DIR/public/styles/styles.css $WEB_APP_DIR/public/scripts/* &> /dev/null
rm -vr $WEB_APP_DIR/node_modules &> /dev/null


