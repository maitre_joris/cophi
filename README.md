# Visualization client for abstract parallell coordinates

The visualization client aims at rendering abstract parallel coordinates and offers several interactive modes.

Two versions exist:

1. the client application relies on a remote service for hosting and computing over prepared data
2. the local application functions on its own on provided files
While the client version targets visual exploration for large documents (with distributed and a priori computing), the local one aims at providing an equivalent exploration experience for smaller data sets in a simple setting (without any architecture requirement).

Both applications are written in C++ and OpenGL. Two compilation modes are available:

1. Native version: using command line arguments and with a minimalist interface mostly actionable through key shortcuts
2. Portable web version: ASM.js-compiled native version integrated in a richer web UI (with buttons, menu and such)

## Installation

### Dependencies
- CMake >= 3.0
- emscripten
- GLSL >= 4.30
- GLFW3
- freetype
- nodejs

### Native version
In an empty `./build` directory, execute `cmake ..` and `make` (-jX where X is your number of CPU + 1)
Four different build types are controlled by two variables: `BASE_MODE` (`Standalone` or `Client`) `HIERARCHICAL` (`ON`or `OFF`). Use them with `-DVAR_NAME=VALUE`. After installation a corresponding executable named `paraCoord` will be placed into the `./bin` folder.

### Web UI
Make sure the commands `emcmake` and `emmake` are availbale in a terminal (if necessary `source path_to_emsdk/emsdk_env.sh`) and that the path to the working directory does not contain space or non-ASCII characters.

In an empty `./build` directory, execute `emcmake cmake .. -DJS_INSTALL_DIR=./web/src/bin/` and `emmake make install`
Then install all dependencies listed in `package.json` in `./web` using `npm install`.

Using Gulp, two seperate web interfaces can be build alternativaly with: `gulp --mode local` and `gulp --mode remote`. The whole application is located under `./web/public`

## Usage

### Notes
'shaders' and 'fonts' folders are used at run-time by the native application and should be located in the execution folder.

### Native version
Local version arguments: `width height filepath [options]̀`

The two first arguments describe the size of the window to be opened. The provided file path should contain a CSV-type file. Supported cell delimiter are space, tabulation, comma and semi-column. 

Options:

- -k: followed by the number of clusters to generate on each dimension
- -i|--with-row-labels: indicates that the first column should be ignored
- -h|--with-column-labels: indicates that the first row should be used to label axes
- -c|--clustering [kmeans|binning|adaptive-binning]
- -n|--nominal|-o|--ordinal: followed by a list of comma-separated column numbers to handle as categories

For the client version, arguments are:  `width height datasetName configFilePath`
A config file format example is provided as `server.conf`. 

### Web applications
A development version can be launched using  `gulp live --mode local|remote`.

