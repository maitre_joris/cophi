#ifndef CLUSTERED_HIERARCHICAL_DATA_H
#define CLUSTERED_HIERARCHICAL_DATA_H

#include "data/HierarchicalData.h"
#include "clustering/HClustering.h"

class ClusteredHierarchicalData : public HierarchicalData {
private:
    struct ThreadArguments {
        HClustering *clustering;
        const std::vector<float> *dimValues;
        unsigned long nbIndividuals;
        Extrema extrema;
        std::vector<HNode> clusters;

        ThreadArguments() {}

        ThreadArguments(HClustering *c,
                        const std::vector<float> *v,
                        unsigned long nbI) : clustering(c),
                                             dimValues(v),
                                             nbIndividuals(nbI) {}
    };

    static void *createDimension(void *args);

    void fetchThreadResult(unsigned int i,
                           const ThreadArguments &args);

public:

    ClusteredHierarchicalData(bool enabledMultiThreading,
                              HClustering *clustering,
                              const std::set<unsigned int> &noClustering,
                              const std::vector<std::vector<float>> &dimValues);
};

#endif //CLUSTERED_HIERARCHICAL_DATA_H
