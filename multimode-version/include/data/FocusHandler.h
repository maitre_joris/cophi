#ifndef FOCUS_HANDLER_H
#define FOCUS_HANDLER_H

class FocusHandler {
    FocusHandler();

    virtual ~FocusHandler();

    virtual void popLevel() = 0;

    virtual unsigned long getNbIndividuals() const = 0;

    virtual unsigned long getDepth() const = 0;

};

#endif //FOCUS_HANDLER_H
