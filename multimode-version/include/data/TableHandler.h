#ifndef TABLE_HANDLER_H
#define TABLE_HANDLER_H

#include<data/Properties.h>
#include<string>
#include<functional>
#include<vector>
#include<unordered_set>
#include<unordered_map>
#include "data/Table.h"
#include "clustering/IClustering.h"
#include "util/Utils.h"

class TableHandler {
public:
    typedef std::set<unsigned int> DimensionSelection;

    struct Configuration {
        std::string data = "";
        bool isFilePath = false;
        bool withColumnLabels = false;
        bool withRowLabels = false;
        int linesToRead = READ_ALL_LINES;
        DimensionSelection nominal;
        DimensionSelection ordinal;
        IClustering::Type clustering = IClustering::Type::NONE;
        unsigned int nbClusters = 0;
        float minValue = std::numeric_limits<float>::infinity();
        float maxValue = std::numeric_limits<float>::infinity();

        void parseCommaList(std::string s, std::set<unsigned int> &r) {
            std::vector<std::string> tokens;
            tokenize(s, tokens, ',');
            for (std::string t : tokens) {
                try {
                    r.insert((unsigned) std::stoi(t));
                } catch (std::exception) {
                    std::cerr << "Failed to parse one item in dimension list" << std::endl;
                }
            }
        };

    };

    TableHandler(const TableHandler &rhs);

    TableHandler(const Configuration &config);

    TableHandler(const std::string filePath, int lineToRead);

    TableHandler(const std::vector<std::vector<float>> &values);

    ~TableHandler();

    unsigned int getNbIndividuals() const;

    unsigned int getNbDimensions() const;

    const std::vector<std::vector<float>> &getTable() const;

    const std::vector<std::string> getLabels() const;

    void exportCSV(std::ostream *os,
                   const Individuals &individuals,
                   const std::vector<unsigned int> &dimensionOrdering) const;

    void clear();

    float getValue(unsigned int i, unsigned int dbDim) const;

    std::string getFilename() const;

    const std::string getClusterLabel(unsigned int dim, int value) const;

    unsigned int countInner(unsigned int dbDim, float min, float max) const;

    /**
     *
     * @param dbDim
     * @param min
     * @param max
     * @return
     */
    Individuals getInner(unsigned int dbDim, float min, float max) const;

    /**
     * Complexity log(n)+a where a is the number of distinct values not contained
     * in [minFocus, maxFocus]
     * @param minFocus
     * @param maxFocus
     * @param dbDim
     * @return
     */

    LowHighPair<std::pair<unsigned int, float>> getOuter(float minFocus, float maxFocus, unsigned int dbDim) const;

    Extrema getCommonExtrema() const;

    bool isQualitative(unsigned int dim) const;

private:
    Configuration config;
    char delimiter = ' ';
    unsigned int nbIndividuals = 0;
    unsigned int nbDimensions = 0;
    std::vector<std::string> columnLabels;
    std::vector<std::string> rowLabels;
    Table *table = nullptr;
    std::string rowLabelColumnName;
    std::vector<std::unordered_map<int, std::string>> nominalLabels;
    bool foundInvalidValues = false;
    static const float INVALID_VALUE_PLACEHOLDER;
    static const int READ_ALL_LINES;

    void writeRow(unsigned int row,
                  const std::vector<unsigned int> &dimensionOrdering,
                  std::stringstream &ss) const;

    float processToken(std::string &token, unsigned int dim, std::vector<std::map<std::string, int>> &labelToValue);

    char detectDelimiter(std::string &line) const;

    void readData(std::istream *in, int lineToRead);

    void readData(int lineToRead = READ_ALL_LINES);

    int initRead(std::istream *in, int linesToRead);

    std::vector<std::map<std::string, int>> parseFile(std::istream *in,
                                                      int linesToRead,
                                                      int nbColumns);


};


#endif //TABLE_HANDLER_H

