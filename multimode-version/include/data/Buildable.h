#ifndef BUILDABLE_H
#define BUILDABLE_H

#include <data/Properties.h>
#include <Node.h>
#include <vector>

class Buildable {
public:
    virtual ~Buildable() {}

    virtual bool addNode(unsigned int dbDim,
                         cluster clusterId,
                         float min,
                         float max,
                         Distribution distribution) = 0;

    virtual bool addEdge(unsigned int srcDbDimension,
                         unsigned int tgtDbDimension,
                         cluster srcClusterId,
                         cluster tgtCluster,
                         unsigned int weight) = 0;

    virtual void updateDimension(unsigned int dbDim,
                                 const std::string &label) = 0;

};


#endif //BUILDABLE_H
