#ifndef FILE_HIERARCHICAL_DATA_H
#define FILE_HIERARCHICAL_DATA_H

#include "data/HierarchicalData.h"
#include "data/TableHandler.h"

class FileHierarchicalData : public HierarchicalData {
private:
    const char SEPARATOR = ',';

    void readLine(const std::string &line, TableHandler *valueProvider);

    HNode *insertNode(unsigned int dbDim, const ClusterIdentifier &ci);

public:
    FileHierarchicalData(std::istream &in, TableHandler *valueProvider, unsigned int k);
};

#endif //FILE_HIERARCHICAL_DATA_H
