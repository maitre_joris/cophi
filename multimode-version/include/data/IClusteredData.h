#ifndef ICLUSTERED_DATA_H
#define ICLUSTERED_DATA_H

#include <data/LocalData.h>
#include <clustering/Hierarchy.h>
#include <iostream>
#include <functional>

class IClusteredData {
public:
    friend cluster LocalData::getIndividualCluster(unsigned int i,
                                                   unsigned int dbDim) const;

    friend Individuals LocalData::getClusterIndividuals(const NodeProp &np) const;


    virtual ~IClusteredData() {}

    virtual Individuals getIndividuals(unsigned int dbDim,
                                       const ClusterIdentifier &clusterId) const =0;

    virtual void forEachCluster(unsigned int dbDim,
                                const ClusterIdentifier &parentId,
                                std::function<void(const ClusterProp &)>) const =0;

    virtual ClusterProp getCluster(unsigned int dbDim,
                                   const ClusterIdentifier &clusterId) const =0;

    virtual unsigned int getBiggestBucket(unsigned int dbDim, const ClusterIdentifier &parent) const =0;

    virtual const std::vector<Extrema> &getDimensionExtrema() const =0;

    virtual std::vector<const HNode *> initDimensionNodes() const =0;

protected:
    virtual cluster getValueCluster(unsigned int dbDim,
                                    float value,
                                    const ClusterIdentifier &parent) const =0;

    virtual cluster getCluster(unsigned int dbDim,
                               unsigned int i,
                               const ClusterIdentifier &parent) const =0;

};

#endif //ICLUSTERED_DATA_H
