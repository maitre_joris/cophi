#ifndef CLUSTERED_DATA_H
#define CLUSTERED_DATA_H

#include "data/IClusteredData.h"
#include <iostream>
#include <unordered_set>
#include <clustering/IClustering.h>
#include <data/TableHandler.h>
#include <unordered_map>
#include <fstream>
#include <sstream>

class ClusteredData : public IClusteredData {
private:
    struct ThreadArguments {
        IClustering *clustering;
        const std::vector<float> *dimValues;
        unsigned int nbIndividuals;
        std::unordered_map<unsigned int, cluster> individualToClusters;
        std::unordered_map<cluster, Individuals> clusterToIndividuals;
        std::unordered_map<cluster, ClusterProp> clusterProperties;
        Extrema extrema;
        unsigned int maxBucketWeight = 0;

        ThreadArguments() {}

        ThreadArguments(IClustering *c,
                        const std::vector<float> *v,
                        unsigned int nbI) : clustering(c),
                                            dimValues(v),
                                            nbIndividuals(nbI) {}
    };

    std::vector<std::unordered_map<cluster, Individuals>> clusterToIndividuals;
    std::vector<std::unordered_map<cluster, ClusterProp>> clusterProperties;
    std::vector<std::unordered_map<unsigned int, cluster >> individualToCluster;
    std::vector<Extrema> dimensionExtrema;
    std::vector<unsigned int> dimensionMaxBucketWeight;

    static void *createDimension(void *args);

    void fetchThreadResult(unsigned int i, const ThreadArguments &args);

    void resizeVectors(unsigned int nbDimensions);

public:

    ClusteredData() {}

    ClusteredData(bool enabledMultiThreading,
                  IClustering *clustering,
                  const std::set<unsigned int> &noClustering,
                  const std::vector<std::vector<float>> &dimValues);

    Individuals getIndividuals(unsigned int dbDim,
                               const ClusterIdentifier &clusterId) const override;

    void forEachCluster(unsigned int dbDim,
                        const ClusterIdentifier &parentId,
                        std::function<void(const ClusterProp &)>) const override;

    ClusterProp getCluster(unsigned int dbDim,
                           const ClusterIdentifier &clusterId) const override;

    cluster getCluster(unsigned int dbDim,
                       unsigned int i,
                       const ClusterIdentifier &parent) const override;

    cluster getValueCluster(unsigned int dbDim,
                            float value,
                            const ClusterIdentifier &parent) const override;

    unsigned int getBiggestBucket(unsigned int dbDim, const ClusterIdentifier &parent) const override;

    const std::vector<Extrema> &getDimensionExtrema() const override;

    std::vector<const HNode *> initDimensionNodes() const override;

};

#endif //CLUSTERED_DATA_H
