#ifndef REMOTE_DATA_H
#define REMOTE_DATA_H

#include <server/RequestHandler.h>
#include <data/DataHandler.h>
#include <data/Properties.h>
#include "tlp/Node.h"
#include "tlp/Edge.h"
#include "tlp/vectorgraph.h"
#include <vector>
#include <map>
#include <iostream>

template<typename T>
inline T &atR(std::vector<T> &v, const unsigned i) {
    if (v.size() < i + 1)
        v.resize(i + 1);
    return v[i];
}

/**
 * Class dedicated to represent data loaded from a server response.
 * It holds methods for stream building. Some methods of base class are
 * overloaded to update data structures as new data is read.
 */
class RemoteData : public DataHandler, public Buildable {
private:
    struct RollBackState : public DataHandler::State {
        VectorGraph graph;
        std::vector<Dimension> axesNodes;
        NodeProperty<NodeProp> nodeProperties;
        NodeProperty<Distribution> nodeDistributions;
        EdgeProperty<EdgeProp> edgeProperties;
        DimensionInterval toDbDimensions;
        std::unordered_map<unsigned int, unsigned int> toAxisId;

        RollBackState() {}

        RollBackState(const State &current) {
            highlight = current.highlight;
            highlightOrigin = current.highlightOrigin;
            dimensionParents = current.dimensionParents;
            lowerContextStacks = current.lowerContextStacks;
            upperContextStacks = current.upperContextStacks;
        }

        void rollBack(State &current) {
            current.highlight = highlight;
            current.highlightOrigin = highlightOrigin;
            current.dimensionParents = dimensionParents;
            current.lowerContextStacks = lowerContextStacks;
            current.upperContextStacks = upperContextStacks;

        }
    };

    RequestHandler *provider;
    std::unordered_map<unsigned int, unsigned int> toAxisId;
    HighlightBase toBeRestored;
    NodeProperty<Highlight> nSubGraphs;
    EdgeProperty<Highlight> eSubGraphs;
    std::vector<Extrema> rootDimensionExtrema;
    std::vector<Extrema> axisExtrema;
    std::vector<unsigned int> dimensionBiggestBucket;
    //RollBackState previous;
    State previous;
    int focusAxis = -1;

    bool addNode(unsigned int dbDim,
                 cluster clusterId,
                 float min,
                 float max,
                 Distribution distribution) override;

    bool addEdge(unsigned int srcDbDimension,
                 unsigned int tgtDbDimension,
                 cluster srcClusterId,
                 cluster tgtClusterId,
                 unsigned int weight) override;

    void updateDimension(unsigned int dbDim,
                         const std::string &label) override;

    void createDeletionElements(const AxisInterval &axes) override;

    void createInsertionElements(const DimensionInterval &, int) override;

    void computeExtrema(std::vector<Extrema> &holder);

    void computeDimensionMaxBucketWeight();

    void requestHighlightFromOrigin(const DimensionInterval axes);

    bool isReadySubGraph(const edge &e) const;

    bool isReadySubGraph(const node &n) const;

    void clearSubGraphs();

    void clearSavedHighlight();

    bool isDisplayed(const HighlightBase &highlightOrigin);

    unsigned int countIndividuals();

    void saveState() override;

    void rollBackState();

    void saveHighlight() override;

    void restoreHighlight() override;

public:
    RemoteData(DataObserver *obs, RequestHandler *builder);

    ~RemoteData();

    void triggerHighlight(const node &n) override;

    void triggerHighlight(const edge &e) override;

    void displayHighlighting();

    bool prepareDeletion(const AxisInterval axes);

    void prepareInsertion(const AxisInterval selectedAxes,
                          const DimensionInterval selectedDims,
                          int insertAfter);

    void restitchHighlight(unsigned int start, unsigned int end) override;

    void clearHighlight() override;

    const Extrema getAxisExtrema(unsigned int axisId) const override;

    void triggerHighlight(const std::vector<ClusterInterval> &selectedClusters) override;

    void triggerHighlight(const std::vector<SelectedElement> &elts) override;

    unsigned int getBiggestBucket(unsigned int dbDim) const override;

    void prepareOpening(unsigned int axisId,
                        cluster id) override;

    void prepareClosing(unsigned int axisId,
                        unsigned int nbClosings) override;

    void doCreateOpeningElements(unsigned int axisId,
                                 cluster id,
                                 const ClusterIdentifier &chain) override;

    void doCreateClosingElements(unsigned int axisId,
                                 unsigned int nbClosings) override;

    void displayFocus();


};

#endif // REMOTE_DATA_H
