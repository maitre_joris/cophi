#ifndef DATA_PROVIDER_H
#define DATA_PROVIDER_H

#include <data/Buildable.h>

class DataProvider {
public:
    virtual ~DataProvider() { };
    virtual bool parseHighlightResponse(Buildable *) = 0;

    virtual bool parseIntervalResponse(Buildable *) = 0;

    virtual bool parseDataSetResponse(Buildable *) = 0;

};

#endif //DATA_PROVIDER_H
