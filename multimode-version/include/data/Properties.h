#ifndef PROPERTIES_H
#define PROPERTIES_H

#include <cmath>
#include<map>
#include<unordered_set>
#include<vectorgraph.h>
#include<string>
#include<sstream>
#include<set>

typedef int cluster;
typedef std::map<cluster, node> Dimension;
typedef std::vector<unsigned int> Distribution;
typedef std::vector<unsigned int> Individuals;
typedef std::pair<unsigned int, cluster> ClusterPosition;
typedef std::tuple<unsigned int, cluster, cluster> ClusterInterval;

template<class T>
class LowHighPair {
private:
    std::pair<T, T> p;
public:
    LowHighPair() {}

    LowHighPair(T low, T high) {
        p = std::make_pair(low, high);
    }

    T &getLow() { return p.first; }

    T &getHigh() { return p.second; }

    const T &getLow() const { return p.first; }

    const T &getHigh() const { return p.second; }

    bool between(const T &l, const T &h) const {
        return (l <= p.first && l <= p.second && h >= p.first && h >= p.second);
    }

    bool ordered() const {
        return p.first <= p.second;
    }

    bool operator<(const LowHighPair &o) const {
        return p < o.p;
    }

    float sum() const {
        return p.first + p.second;
    }

};

// Minimal data to represent a cluster
class ClusterProp {
public:
    static const cluster INVALID_ID;
    static const cluster LOWER_META_NODE_ID;
    static const cluster HIGHER_META_NODE_ID;
    static const unsigned int DISTRIBUTION_BUCKET_NB;

    cluster clusterId = INVALID_ID;
    unsigned int weight = 0;
    float min, max;
    Distribution distribution;

    virtual bool operator<(const ClusterProp &o) const;

    ClusterProp() {}

    ClusterProp(cluster id);

    ClusterProp(cluster id, float value, unsigned long weight);

    ClusterProp(const ClusterProp &);

    ClusterProp(const std::set<ClusterProp> toBeMerged, cluster id);

    std::string toString() const;

    bool focusable() const;

    cluster getId() const;

    virtual unsigned long getBiggestBucket() const;

    bool isMeta() const;

    float getMin() const;

    float getMax() const;

    bool inBound(float value) const;

    unsigned long getWeight() const;

    void initDistribution(const std::set<ClusterProp> &toBeMerged);

    bool isHigherContext() const;

    bool isLowerContext() const;

    void merge(const ClusterProp &o);

};

// Positioned cluster data
class NodeProp : public ClusterProp {
public:
    unsigned int dimension = 0;
    unsigned int dbDimension = 0;
    float gapBefore = 0.f;
    float weightBefore = 0.f; // Remove?
    float distortion = .0f;

    NodeProp();

    NodeProp(const ClusterProp &, unsigned int axis, unsigned int dbDim);
};

struct EdgeProp {
    unsigned int weight;
    float sourcePos;
    float destPos;
    float z;
};

struct Extrema {
    float min = std::numeric_limits<float>::infinity();
    float max = -std::numeric_limits<float>::infinity();

    bool isValid() const {
        return min != std::numeric_limits<float>::infinity() &&
               max != -std::numeric_limits<float>::infinity() &&
               min <= max;
    }

    float size() const {
        return std::abs(max - min);
    }

    void update(float v) {
        min = std::min(v, min);
        max = std::max(v, max);
    }
};


struct CompEdge {
    bool operator()(const std::pair<edge, float> &a,
                    const std::pair<edge, float> &b) const;
};

struct CompNode {
    bool operator()(const node &a, const node &b) const;

    CompNode(const NodeProperty<NodeProp> &np);

    const NodeProperty<NodeProp> &prop;
};

#endif //PROPERTIES_H

