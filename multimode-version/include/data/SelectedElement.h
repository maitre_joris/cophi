#ifndef SELECTED_ELEMENT_H
#define SELECTED_ELEMENT_H

#include "tlp/Edge.h"
#include "tlp/Node.h"

struct SelectedElement {
    node n = node();
    edge e = edge();
    int subNo = -1;

    bool isValid() const {
        return n.isValid() || e.isValid();
    }

    void clear() {
        n = node();
        e = edge();
        subNo = -1; // When a node contains sub-nodes
    }

    SelectedElement() {
        clear();
        assert(!isValid());
    };

    SelectedElement(node n, unsigned int subNo) : n(n), subNo(subNo) {}

    SelectedElement(node n) : SelectedElement(n, -1) {
        assert(!e.isValid());
    }

    SelectedElement(edge e) : e(e), subNo(-1) {
        assert(!n.isValid());
    }

    bool hasSubSelection() const {
        return subNo > -1;
    }

    unsigned int getSubNo() const {
        assert(hasSubSelection());
        return subNo;
    }

    bool operator<(const SelectedElement &o) const {
        if (this->e.isValid() && !o.e.isValid()) return false;
        if (!this->e.isValid() && o.e.isValid()) return true;
        if (this->e.isValid() && o.e.isValid()) return this->e < o.e;
        if (this->n.isValid() && o.n.isValid()) return this->n < o.n;
        else return true;
    }

    bool operator==(const SelectedElement o) const {
        bool consistent = (isValid() && o.isValid()) ||
                          !(isValid() || o.isValid());
        return consistent && o.e == e && o.n == n && o.subNo == subNo;
    }

    bool operator!=(const SelectedElement o) const {
        bool consistent = (isValid() && o.isValid()) ||
                          !(isValid() || o.isValid());
        return !consistent || o.e != e || o.n != n;
    }

};


#endif //SELECTED_ELEMENT_H
