#ifndef SUBGRAPH_H
#define SUBGRAPH_H

#include <data/ViewData.h>
#include <data/Properties.h>
#include <util/AxisInterval.h>
#include <data/Buildable.h>
#include <data/SelectedElement.h>
#include <vector>
#include <unordered_map>


class DataHandler;

class Highlight : public ViewData, public Buildable {
private:
    SelectedElement origin;
    Individuals individuals;
    std::unordered_map<node, std::pair<unsigned int, Distribution>> nodes;
    std::unordered_map<edge, unsigned int> edges;
    DataHandler *base = nullptr;

public:
    Highlight(DataHandler *base, SelectedElement e);

    Highlight(DataHandler *base, SelectedElement e, Individuals &i);

    Highlight();

    virtual ~Highlight();

    bool isAtRoot(unsigned int axisId) const override;

    void forEachNode(std::function<void(const node &)> f) const override;

    void forEachEdge(std::function<void(const edge &)> f) const override;

    void forEachNode(unsigned int axisId,
                     std::function<void(const node &)> f) const override;

    void forEachEdge(unsigned int axisId,
                     std::function<void(const edge &)> f) const override;

    unsigned int getNbNodes() const override;

    unsigned int getNbEdges() const override;

    float getNodeSubWeight(const node &n) const override;

    float getEdgeSubWeight(const edge &e) const override;

    const Extrema getAxisExtrema(unsigned int i) const override;

    void clear();

    bool isEmpty() const;

    void addNode(const node n, unsigned int weight, Distribution d);

    void addEdge(const edge, unsigned int weight);

    bool addNode(unsigned int dbDim,
                 cluster clusterId,
                 float min,
                 float max,
                 Distribution distribution) override;

    bool hasNode(const node &n) const override;

    bool hasEdge(const edge &e) const override;

    bool hasEdgeEnds(const edge &e);

    bool addEdge(unsigned int srcDbDimension,
                 unsigned int tgtDbDimension,
                 cluster srcClusterId,
                 cluster tgtClusterId,
                 unsigned int weight) override;

    const Ends getEnds(const edge &e) const override;

    const std::vector<edge> getStar(const node &n) const override;

    unsigned int getNbIndividuals() const override;

    void updateDimension(unsigned int dbDim,
                         const std::string &label) override;

    const std::vector<float> getNormalizedDistribution(const node &n) const override;

    const Individuals &getIndividuals() const;

    void deleteElements(AxisInterval axes);

    void deleteNodes(unsigned int atAxis);

    void deleteEdges(unsigned int justAfter);

    SelectedElement getOrigin() const;

    std::string toString() const;

};

#endif //SUBGRAPH_H
