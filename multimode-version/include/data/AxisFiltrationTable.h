#ifndef AXIS_FILTRATION_TABLE_H
#define AXIS_FILTRATION_TABLE_H

#include <utility>
#include <map>
#include "Properties.h"

class AxisFiltrationTable {
private:
    std::vector<std::map<LowHighPair<float>, cluster>> table;
public:
    typedef std::pair<cluster, float> Hook;

    void init(unsigned int nbAxes);

    void insertCluster(cluster id, unsigned int axisId,
                       LowHighPair<float> verticalScope);

    Hook getUpperMost(unsigned int axisId, float upLimit) const;

    Hook getLowest(unsigned int axisId, float downLimit) const;

    std::set<cluster> getInBetweenClusters(unsigned int axisId,
                                           cluster up,
                                           cluster down) const;

    void toString() const;
};


#endif //AXIS_FILTRATION_TABLE_H
