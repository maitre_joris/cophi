#ifndef DATA_HANDLER_H
#define DATA_HANDLER_H

#include <data/ViewData.h>
#include <data/Highlight.h>
#include <renderer/animation/Animation.h>
#include <server/HighlightBase.h>
#include <util/AxisInterval.h>
#include <util/DimensionInterval.h>
#include <data/ClusterIdentifier.h>
#include <clustering/Hierarchy.h>
#include <util/Utils.h>
#include <data/Properties.h>
#include <unordered_set>
#include <stack>

static const float Z_UNIT = 1E-4f;
const float MID_EDGE_POS = .5f;

typedef std::deque<ClusterProp> ContextStack;

enum class HighlightingOp {
    replace = 0, add = 1, substract = 2, intersect = 3
};

class DataHandler : public ViewData, public Animation::AnimationObserver {
public:
    class DataObserver {
    public:
        virtual void onDataLoaded(DataHandler *observable,
                                  bool center = true) = 0;

        virtual void onHighlight(const Highlight *sub) = 0;

        virtual ~DataObserver() {};
    };

    DataHandler(DataObserver *obs, bool contextHighlight);

    virtual ~DataHandler();

    virtual void clearHighlight();

    virtual void prepareHighlight(HighlightingOp op);

    void applyHighlight(SelectedElement elt);

    void applyHighlight(std::vector<SelectedElement> &elts);

    void deleteDimension(AxisInterval axes) override;

    void insertAxes(DimensionInterval dbDims,
                    int justAfterAxisId) override;

    void applyOpening(const node n);

    void applyClosing(const unsigned int axisId, unsigned int nbLevels);

    virtual void prepareOpening(unsigned int axisId, cluster id);

    virtual void prepareClosing(const unsigned int axisId, unsigned int nbLevels);

    void forEachNode(std::function<void(const node &)> f) const override;

    void forEachNode(unsigned int axisId,
                     std::function<void(const node &)> f) const override;

    void forEachEdge(std::function<void(const edge &)> f) const override;

    void forEachEdge(unsigned int axisId,
                     std::function<void(const edge &)> f) const override;


    std::unordered_set<edge> getEdges(unsigned int leftAxisId) const;


    std::string getAxisLabel(unsigned int axisId) const;

    std::string getDimensionLabel(unsigned int dbDim) const;


    unsigned int getNbSourceDimensions() const;

    unsigned int getNbIndividuals() const override;

    void setObserver(DataObserver &o);

    unsigned int getLastAxisId() const;

    float getFrontZ() const;

    edge getEdgeById(unsigned int clientId) const;

    bool isElementById(int clientId) const;

    unsigned int getMaxNodeId() const;

    const Dimension &getDimension(unsigned int axisId) const;

    unsigned int getNbAxes() const override;

    const Ends getEnds(const edge &e) const override;

    float getNodeSubWeight(const node &n) const override;

    float getEdgeSubWeight(const edge &e) const override;

    DimensionInterval toDimensions(AxisInterval axes) const;

    unsigned int toDimension(unsigned int axis) const;


    const edge getEdge(unsigned int srcDbDimension,
                       unsigned int tgtDbDimension,
                       cluster srcClusterId,
                       cluster tgtClusterId) const;

    const edge getEdge(std::pair<ClusterPosition, ClusterPosition>) const;

    std::pair<std::set<edge>, std::set<node>> getElements(const AxisInterval i,
                                                          bool withSideEdges = false) const;

    Extrema getGlobalExtrema() const;

    node getNodeById(unsigned int clientId) const;

    unsigned int getEdgeClientId(const edge &e) const;

    void pullEdgeBack(const edge &e);

    void positionEdges(unsigned int dim);

    void positionNodes(unsigned int axisId);

    virtual void createDeletionElements(const AxisInterval &axes) = 0;

    virtual void createInsertionElements(const DimensionInterval &dbDims,
                                         int justAfterAxisId) = 0;

    const node getNodeFromDbDim(unsigned int dbDim, cluster clusterId) const;

    const node getNodeFromAxis(unsigned int axisId, cluster id) const;

    unsigned int getNodeDbDim(const node &n) const;

    const NodeProp &getNodeProperty(const node &n) const;

    const EdgeProp &getEdgeProperty(const edge &e) const;

    const std::vector<edge> getStar(const node &n) const override;

    const DimensionInterval &getAxesAsDimensions() const;

    const std::vector<float> getNormalizedDistribution(const node &n) const override;

    static const std::vector<float> createNormalizedDistribution(const Distribution &d, unsigned int normalizer);

    unsigned int getNbNodes() const override;

    unsigned int getNbEdges() const override;

    virtual unsigned int getBiggestBucket(unsigned int dbDim) const =0;

    virtual void triggerHighlight(const std::vector<ClusterInterval> &clusterIntervals) = 0;

    const Highlight *getCurrentHighlight() const;

    bool isAtRoot(unsigned int axisId) const override;

    cluster getAncestor(unsigned int axisId, unsigned int generations) const;

    const ContextStack &getContextNodeHierarchy(unsigned int axisId, cluster id) const;

    float getContextWeightRatio(unsigned int axisId) const;

    LowHighPair<float> getContextIntervalRatio(unsigned int axisId) const;

    Extrema getFocusExtrema(unsigned int axisId) const;

    Extrema getFocusInnerExtrema(unsigned int axisId) const;

    bool hasEdge(const edge &e) const override;

    bool hasNode(const node &n) const override;

    unsigned int getLevel(unsigned int dbDim) const;

    bool isContext(const SelectedElement &e) const;

    void checkCoherence();

    virtual void invertSelection();

protected:
    struct State {
        Highlight *highlight = nullptr;
        HighlightBase highlightOrigin;
        std::vector<ClusterIdentifier> dimensionParents;
        std::vector<ContextStack> lowerContextStacks;
        std::vector<ContextStack> upperContextStacks;

        void clear() {
            dimensionParents.clear();
            lowerContextStacks.clear();
            upperContextStacks.clear();
            highlightOrigin.clear();
            highlight = nullptr;
        }
    };

    DataObserver *observer_ = nullptr;
    VectorGraph graph;
    unsigned int nbSourceDimensions;
    unsigned int nbSourceIndividuals;
    DimensionInterval toDbDimensions;
    std::vector<Dimension> axesNodes;
    NodeProperty<NodeProp> nodeProperties;
    EdgeProperty<EdgeProp> edgeProperties;
    std::vector<std::string> dimensionLabels;
    State current;
    std::vector<const HNode *> dimensionParentNode;
    const bool contextCanBeHighlighted;

    DataHandler(const DataHandler &rhs);

    virtual void saveState();

    virtual void restitchHighlight(unsigned int start, unsigned int end) = 0;

    void updateDisplayValues(unsigned int fromAxis);

    const node addNode();

    const edge addEdge(node &leftNode, node &rightNode);

    void restitchAfterDeletion(const AxisInterval &axes,
                               std::function<void(unsigned int,
                                                  unsigned int)> createEdges);

    void deleteEdges(unsigned int leftAxisId);

    virtual void triggerHighlight(const node &n) = 0;

    virtual void triggerHighlight(const edge &e) = 0;

    virtual void triggerHighlight(const std::vector<SelectedElement> &elts) = 0;

    void init(unsigned int nbDimensions, unsigned int nbIndividuals);

    void finish();

    unsigned int toAxis(unsigned int dbDim) const;

    void deleteAxisElements(unsigned int axisId);

    virtual void doCreateOpeningElements(unsigned int axisId, cluster focus, const ClusterIdentifier &) =0;

    virtual void doCreateClosingElements(unsigned int axisId, unsigned int nbClosings) =0;

    virtual void saveHighlight() =0;

    virtual void restoreHighlight() =0;


private:
    typedef std::unordered_map<cluster, int> Links;
    unsigned int maxNodeId = 0, minEdgeId = 0, maxEdgeId = 0;
    float backZ = -1.f;
    float totalInter = 0.f, totalIntra = 0.f;

    void sortEdges();

    bool isConnected();

    void mergeCluster(const std::pair<cluster, node> &uncle,
                      unsigned int axis,
                      std::set<ClusterProp> &unclesProps,
                      std::pair<Links, Links> &unclesLinks) const;

    void deleteNodes(unsigned int leftAxisId);

    void createNodeAggregate(cluster id,
                             unsigned int axis,
                             const std::set<ClusterProp> &nodes,
                             const std::pair<Links, Links> &edges);


};

#endif //DATA_HANDLER_H
