#ifndef VIEWDATA_H
#define VIEWDATA_H

#include<vector>
#include<functional>
#include<tlp/Node.h>
#include<tlp/Edge.h>
#include<data/Properties.h>

/*
 * Wrapper class used for building Views objects (EdgeView and NodeView)
 */

struct Ends {
    node source;
    node dest;
};

class ViewData {
public:
    virtual ~ViewData() {}

    virtual void forEachNode(std::function<void(const node &)> f) const = 0;

    virtual void forEachNode(unsigned int axisId,
                             std::function<void(const node &)> f) const = 0;

    virtual void forEachEdge(std::function<void(const edge &)> f) const = 0;

    virtual void forEachEdge(unsigned int axisId,
                             std::function<void(const edge &)> f) const = 0;

    virtual unsigned int getNbNodes() const = 0;

    virtual unsigned int getNbEdges() const = 0;

    virtual float getNodeSubWeight(const node &n) const = 0;

    virtual float getEdgeSubWeight(const edge &e) const = 0;

    virtual const Ends getEnds(const edge &e) const = 0;

    virtual const std::vector<edge> getStar(const node &n) const = 0;

    virtual unsigned int getNbIndividuals() const = 0;

    virtual const std::vector<float> getNormalizedDistribution(const node &n) const = 0;

    virtual const Extrema getAxisExtrema(unsigned int axisId) const = 0;

    virtual bool isAtRoot(unsigned int axisId) const =0;

    virtual bool hasNode(const node &n) const =0;

    virtual bool hasEdge(const edge &e) const =0;

};


#endif //VIEWDATA_H
