#ifndef HIERARCHICAL_DATA_H
#define HIERARCHICAL_DATA_H

#include "clustering/Hierarchy.h"
#include "data/IClusteredData.h"

class HierarchicalData : public IClusteredData {
protected:
    typedef std::pair<std::set<ClusterProp>, std::set<ClusterProp>> Siblings;

    std::vector<HNode> dimensionClustering;
    std::vector<Extrema> dimensionExtrema;

    void resizeVectors(unsigned int nbDimensions);

    const HNode &findNode(unsigned int dbDim,
                          const ClusterIdentifier &clusterId) const;

    Siblings getSiblings(unsigned int dbDim,
                         const ClusterIdentifier &id) const;

public:

    HierarchicalData();

    virtual ~HierarchicalData() {}

    Individuals getIndividuals(unsigned int dbDim,
                               const ClusterIdentifier &clusterId) const override;

    void forEachCluster(unsigned int dbDim,
                        const ClusterIdentifier &parentId,
                        std::function<void(const ClusterProp &)>) const override;

    ClusterProp getCluster(unsigned int dbDim,
                           const ClusterIdentifier &clusterId) const override;

    cluster getCluster(unsigned int dbDim,
                       unsigned int i,
                       const ClusterIdentifier &parent) const override;

    unsigned int getBiggestBucket(unsigned int dbDim, const ClusterIdentifier &parent) const override;

    const std::vector<Extrema> &getDimensionExtrema() const override;

    cluster getValueCluster(unsigned int dbDim,
                            float value,
                            const ClusterIdentifier &parent) const override;

    std::string dump() const;

    std::vector<const HNode *> initDimensionNodes() const;

};

#endif //HIERARCHICAL_DATA_H
