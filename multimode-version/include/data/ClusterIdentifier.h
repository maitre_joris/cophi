#ifndef CLUSTER_IDENTIFIER_H
#define CLUSTER_IDENTIFIER_H

#include "data/Properties.h"

/*
 * At a data level, clusters are a sequence of focus operation. At a higher
 * level, they are just flat sequential numbers ('cluster')
 */
class ClusterIdentifier {
private:
    std::vector<cluster> topDownChain;
    bool aggregate = false;
public:
    ClusterIdentifier();

    ClusterIdentifier(cluster id);

    ClusterIdentifier(const ClusterIdentifier &o, cluster id);

    cluster getLeafId() const;

    void chain(cluster focused);

    bool isRoot() const;

    std::string toString() const;

    std::vector<cluster>::const_iterator begin() const;

    std::vector<cluster>::const_iterator end() const;

    size_t size() const;

    static ClusterIdentifier getParent(const ClusterIdentifier &c);

    ClusterIdentifier parent();

    ClusterIdentifier drilled(cluster id);

    unsigned int getNbLevels() const;


};

#endif //CLUSTER_IDENTIFIER_H
