#ifndef LOCAL_DATA_H
#define LOCAL_DATA_H

#include <clustering/IClustering.h>
#include <data/DataHandler.h>
#include <data/TableHandler.h>
#include "tlp/vectorgraph.h"
#include "util/Utils.h"
#include <math.h>
#include <climits>
#include <fstream>
#include <sstream>
#include <vector>
#include <unordered_map>
#include <array>

class IClusteredData;

struct pairhash {
public:
    template<typename T, typename U>
    std::size_t operator()(const std::pair<T, U> &x) const {
        return std::hash<T>()(x.first) ^ std::hash<U>()(x.second);
    }
};

class LocalData : public DataHandler {
    friend IClusteredData;

    friend void threadRoutine(const LocalData *data,
                              unsigned int from, unsigned int to,
                              std::unordered_map<edge, unsigned int> &edgeWeights,
                              const Individuals &individuals,
                              int i, int j);

private:
    struct EdgeKey {
        unsigned int leftAxisId = 0;
        cluster leftId;
        cluster rightId;

        bool operator==(const EdgeKey &other) const {
            return (leftId == other.leftId
                    && leftAxisId == other.leftAxisId
                    && rightId == other.rightId);
        }
    };

    struct edgekeyhash {
        std::size_t operator()(const EdgeKey &k) const {
            using std::size_t;
            using std::hash;

            // Compute individual hash values for members and combine them using XOR
            // and bit shifting:
            return ((hash<unsigned int>()(k.leftAxisId)
                     ^ (hash<int>()(k.leftId) << 1)) >> 1)
                   ^ (hash<int>()(k.rightId) << 1);
        }
    };

    const TableHandler *provider;
    const IClusteredData *clustering;
    Highlight *preview = nullptr;
    HighlightingOp currentOp = HighlightingOp::replace;
    bool enabledMultiThreading = true;
    Individuals toBeRestored;

    void createNodes(unsigned int dbDim,
                     unsigned int axisId);

    void createEdges(unsigned int leftAxisId,
                     unsigned int rightAxisId);

    /**
     * Set-theoretic difference or relative complement of B in A (denoted A - B
     * or A \ B), the set of all individuals that are members of A but
     * not members of B is inserted into result
     */
    Individuals individualsRelativeComp(const Individuals &iA,
                                        const Individuals &iB) const;

    void buildHighlightPreview(HighlightingOp op,
                               Individuals &newIndividuals);

    void restitchHighlight(unsigned int start, unsigned int end) override;

    void createDeletionElements(const AxisInterval &axes) override;

    void createInsertionElements(const DimensionInterval &dbDims,
                                 int tAfterAxisId) override;

    cluster getIndividualCluster(unsigned int i,
                                 unsigned int dbDim) const;

    Individuals getClusterIndividuals(const NodeProp &np) const;

    void buildSubGraph(const Individuals &individuals,
                       unsigned int from,
                       unsigned int to,
                       std::unordered_map<node, unsigned int> &nodeWeights,
                       std::unordered_map<node, Distribution> &nodeDistributions,
                       std::unordered_map<edge, unsigned int> &edgeWeight);

    void buildSubGraphMT(const Individuals &individuals,
                         unsigned int from,
                         unsigned int to,
                         std::vector<std::unordered_map<EdgeKey, unsigned int, edgekeyhash>> &edgeWeights,
                         std::vector<std::unordered_map<cluster, unsigned int>> &nodeWeights,
                         std::vector<std::unordered_map<cluster, Distribution>> &nodeDistributions);

    void fillBucket(unsigned int i,
                    unsigned int dbDim,
                    Distribution &d,
                    const ClusterProp *cp);

    void buildData();

    void buildHighlight(Highlight *h);

    unsigned int getBiggestBucket(unsigned int dbDim) const override;

    void createFocusElements(unsigned int axisId, const ClusterIdentifier &id);

    void createContextElements(unsigned int dbDim, unsigned int axisId, const ClusterIdentifier &parent);

    void doCreateOpeningElements(unsigned int axisId, cluster focus, const ClusterIdentifier &parent) override;

    void doCreateClosingElements(unsigned int axisId, unsigned int nbClosings) override;

    void saveHighlight() override;

    void restoreHighlight() override;


public:
    LocalData(const LocalData &rhs);

    ~LocalData();

    LocalData(DataObserver *obs,
              const TableHandler *csv,
              IClustering *,
              const std::set<unsigned int> &categoricalDimensions,
              bool multiThreading);

    LocalData(DataObserver *obs,
              const TableHandler *csv,
              const IClusteredData *dbData,
              bool multiThreading);

    void clearHighlightPreview();

    bool isEnabledMultiThreading() const;

    void setEnabledMultiThreading(bool b);

    void saveHighlightPreview();

    const Individuals &getHighlightedIndividuals() const;

    void setCurrentHighlightOp(HighlightingOp op);

    HighlightingOp getCurrentHighlightOp();

    void triggerHighlight(const edge &e) override;

    void triggerHighlight(const node &e) override;

    void triggerHighlight(const std::vector<ClusterInterval> &clusterInterval) override;

    void triggerHighlight(const std::vector<SelectedElement> &elts) override;

    void exportData(std::ostream *os,
                    bool highlightedOnly,
                    bool includeDeletedDimensions) const;

    const Extrema getAxisExtrema(unsigned int axisId) const override;

    std::string getLabel(unsigned int dbDim, int id) const;

    void reorderAxes(DimensionInterval toDbDims);


    void benchTriggerHighlight(const edge &e);

    void benchTriggerHighlight(const node &n);

    void benchFocus(const node &n);

    void benchFocusOut(unsigned int axisId);

    void clear();

    void prepareHighlight(HighlightingOp op) override;

    void invertSelection() override;
};


#endif // LOCAL_DATA_H
