#ifndef TABLE_H
#define TABLE_H

#include<vector>
#include<map>
#include<unordered_set>
#include<functional>
#include "Properties.h"

class Table {
private:
    bool knownNbIndividuals = false;
    //std::vector<std::map<float, Individuals>> valueToRow;
    std::vector<std::vector<std::pair<float, Individuals>>> valueToRow;

public:
    std::vector<std::vector<float>> table;

    Table(const std::vector<std::vector<float>> &table);

    Table(unsigned int d, int n);

    void computeReverseIndex();

    void insertValue(unsigned int d, unsigned int i, float value);

    Individuals getInBetween(unsigned int d,
                             float min,
                             float max) const;

    unsigned int countOver(unsigned int d, float v) const;

    unsigned int countUnder(unsigned int d, float v) const;

    unsigned int countBetween(unsigned int d, float min, float max) const;

    float getMinUpper(unsigned int d, float v) const;

    float getMaxLower(unsigned int d, float v) const;


};


#endif //TABLE_H
