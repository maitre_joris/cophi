#ifndef TLP_GLMATRIX_GL
#define TLP_GLMATRIX_GL

#include <cassert>
#include <iostream>
#include <vector>
#include "Vector.h"
#include "Array.h"

#define MATRIX GlMatrix<Obj,SIZE>

template<typename Obj,unsigned int SIZE>
class GlMatrix: public Array< Vector<Obj,SIZE> , SIZE > {
public:
  GlMatrix() {}
  GlMatrix(const Array< Vector<Obj,SIZE> , SIZE > &a) :
    Array< Vector<Obj,SIZE> , SIZE >(a) {
  }

  // Builds a correlation matrix from a covariance matrix !
  GlMatrix(const std::vector<std::vector<Obj> > &covarianceMatrix);

  /**
   * Fill the matrix with the value of obj
   */
  inline MATRIX& fill(Obj obj);
  /**
   * Compute the determinant of the matrix,
   */
  Obj determinant() const;
  /**
   * Transpose the matrix and return "&(*this)".
   */
  MATRIX& transpose();
  /**
   * Inverse the matrix and return "&(*this)"
   */
  MATRIX& inverse();
  /**
   * add another matrix to the current one and return "&(*this)"
   */
  inline MATRIX & operator+=(const MATRIX &mat);
  /**
   * substract another matrix from the current and return "&(*this)"
   */
  inline MATRIX & operator-=(const MATRIX &mat);
  /**
   * Check equality of two Matrices
   */
  inline bool operator==(const MATRIX &) const;
  /**
   * Check non equality of two Matrices
   */
  inline bool operator!=(const MATRIX &) const;
  /**
   * Multiply the matrix by another matrix and return "&(*this)"
   */
  inline MATRIX & operator*=(const MATRIX &mat);
  /**
   * Multiply all elements of the matrix by obj, return "&(*this)"
   */
  inline MATRIX & operator*=(const Obj &obj);
  /**
   *  Divide the matrix by another one return "&(*this)"
   */
  inline MATRIX & operator/=(const MATRIX &mat);
  /**
   * Divide all elements of the matrix by obj, return "&(*this)"
   */
  inline MATRIX & operator/=(const Obj &obj);
  /**
   * Returns the cofactor Matrix of this
   */
  MATRIX cofactor() const;
  /**
   * Returns a new matrix equal to the division of the matrix by
   * another matrix"
   */
  MATRIX operator/(const MATRIX &mat2) const;

  /**
   * Returns a new matrix equal to the division of the matrix by
   * obj"
   */
  MATRIX operator/(const Obj &obj) const;

  Obj operator()(unsigned int col, unsigned int row) const;
  Obj& operator()(unsigned int col, unsigned int row);

  /**
   * Returns a new vector equal to the most influent eigenvector of the
   * matrix
   */
  inline Vector<Obj,SIZE> powerIteration(const unsigned int nIterations) const;

#ifndef DOXYGEN_NOTFOR_DEVEL
  /**
   * Simplifies a 3x3 matrix in 2x2 matrix to be used with computeEigenVector
   */
  inline bool simplify(GlMatrix<Obj, 2> &simplifiedMatrix) const;

  /**
   * Returns the EigenVector of the matrix corresponding to the EigenValue passed, with a base x
   *           /!\ This can only be used with a 2x2 matrix !!! /!\
   */
  inline bool computeEigenVector(const float x, Vector<Obj, 3> &eigenVector) const;
#endif // DOXYGEN_NOTFOR_DEVEL

};

typedef GlMatrix<float,  3> GlMat3f;
typedef GlMatrix<double, 3> GlMat3d;
typedef GlMatrix<float,  4> GlMat4f;
typedef GlMatrix<double, 4> GlMat4d;

/**
 * Returns a new matrix equal to the sum of 2 matrices
 */
template<typename Obj, unsigned int SIZE>
inline MATRIX operator+(const MATRIX &mat1, const MATRIX &mat2);
/**
 * Returns a new matrix equal to the difference of 2 matrices
 */
template<typename Obj, unsigned int SIZE>
inline MATRIX operator-(const MATRIX &mat1, const MATRIX &mat2);
/**
 * Returns a new matrix equal to the multiplication of the matrix by
 * obj
 */
template<typename Obj, unsigned int SIZE>
inline MATRIX operator*(const MATRIX &mat, const Obj &obj);
/**
 * Returns a new matrix equal to the multiplication of the matrix by
 * another matrix
 */
template<typename Obj, unsigned int SIZE>
inline MATRIX operator*(const MATRIX &mat1, const MATRIX &mat2);
/**
 * Returns a new vector equal to the multiplication of the vector by
 * a matrix,(the vector is automatically transposed to do the multiplication)
 */
template<typename Obj, unsigned int SIZE>
inline Vector<Obj,SIZE> operator*(const Vector<Obj,SIZE> &vec, const GlMatrix<Obj, SIZE> &);
/**
 * Returns a new vector equal to the multiplication of the matrix by
 * a vector
 */
template<typename Obj, unsigned int SIZE>
inline Vector<Obj,SIZE> operator*( const GlMatrix<Obj, SIZE> &, const Vector<Obj,SIZE> &vec);





#include "GlMatrix.cxx"

/**
 * init identity 4x4 matrix.
 */
void identity(GlMat4f &m);
/**
 * init rotation 4x4 matrix.
 */
void rotate(GlMat4f &m, const float angle,  const Vec3f &coords);

/**
 * init translate 4x4 matrix.
 */
void scale(GlMat4f &m, const Vec3f &coords);

void glScale(GlMat4f &m, const Vec3f &coords);

/**
 * init translate 4x4 matrix.
 */
void translate(GlMat4f &m, const Vec3f &coords);

void glTranslate(GlMat4f &m, const Vec3f &coords);

void perspective(GlMat4f &m, float fovy, float aspect, float zNear, float zFar);

void ortho(GlMat4f &m, float left, float right, float bottom, float top, float zNear, float zFar);

Vec3f project(const Vec3f &obj, const GlMat4f &model, const GlMat4f &proj, const Vec4i view);

Vec3f unProject(const Vec3f &obj, const GlMat4f &model, const GlMat4f &proj, const Vec4i view);



#endif
