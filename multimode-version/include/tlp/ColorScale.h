#ifndef COLOR_SCALE_H_
#define COLOR_SCALE_H_

#include <vector>
#include <map>
#include "Color.h"

class ColorScale  {

public:

    ColorScale(const bool gradient = true);
    ColorScale(const std::vector<Color> &colors, const bool gradient = true);


    ColorScale(const ColorScale& scale);
    ColorScale& operator=(const ColorScale& scale);
    virtual ~ColorScale();

    unsigned int getStopsCount() {
        return colorMap.size();
    }

    virtual void setColorScale(const std::vector<Color> colors, const bool gradient = true);

    virtual void setColorAtPos(const float pos, const Color &color);

    virtual Color getColorAtPos(const float pos) const;

    bool colorScaleInitialized() const {
        return colorScaleSet;
    }

    std::map<float, Color> getColorMap() const {
        return colorMap;
    }

    void setColorMap(const std::map<float, Color>& colorMap);

    bool isGradient() const {
        return gradient;
    }

    void setColorMapTransparency(unsigned char transparency);

protected:

    std::map<float, Color> colorMap;
    bool gradient;
    bool colorScaleSet;

};

#endif /* COLOR_SCALE_H_ */

