#ifndef RECTANGLE_NA_H
#define RECTANGLE_NA_H

/**
 * Computes AB.AC
 */
inline float inlineDotProduct(const Vec2f &a, const Vec2f &b, const Vec2f &p) {
  return ((p[0] - a[0]) * (b[0] - a[0])) + ((p[1] - a[1]) * (b[1] - a[1]));
}

/**
 * @ingroup Structures
 * \brief class for rectangle
 * From Wulip
 *
 * Enables to both create and manipulate a 2D Axis Not Aligned Rectangle
 *
 * Author : <a Arnaud </a>
 */
struct RectangleNA : public Array<Vec2f, 4> {
    RectangleNA() {
      (*this)[0].fill(1);
      (*this)[1].fill(1);
      (*this)[2].fill(-1);
      (*this)[3].fill(-1);
    }

    /**
    * Create a new rectangle.
    */
    RectangleNA(const Vec2f &A, const Vec2f &B, const Vec2f &C,
                const Vec2f &D) {
      (*this)[0] = A;
      (*this)[1] = B;
      (*this)[2] = C;
      (*this)[3] = D;
      organizePoints();
    }

    RectangleNA(const Rectf &r) {
      (*this)[0] = r[0];
      (*this)[2] = r[1];
      (*this)[1][0] = r[0][0];
      (*this)[1][1] = r[1][1];
      (*this)[3][0] = r[1][0];
      (*this)[3][1] = r[0][1];
    }

    /**
   * @return an Aligned Rectangle bounding box for the Not Aligned Rectangle.
   */
    Rectf searchBoundingBox() const {
      Vec2f min = (*this)[0];
      Vec2f max = (*this)[0];
      for (int i = 1; i < 4; i++) {
        min[0] = std::min(min[0], (*this)[i][0]);
        min[1] = std::min(min[1], (*this)[i][1]);
        max[0] = std::max(max[0], (*this)[i][0]);
        max[1] = std::max(max[1], (*this)[i][1]);
      }
      return Rectf(min, max);
    }

    /**
   * @return true if there is an intersection between bounding boxes of two rectangles.
   */
    bool boundingBoxIntersect(const RectangleNA &r) const {
      Rectf bb1 = searchBoundingBox();
      Rectf bb2 = r.searchBoundingBox();

      return bb1.intersect(bb2);
    }

    /**
   * Make a correct order of the points
   */
    void organizePoints() {
      if ((*this)[0] - (*this)[1] != (*this)[3] - (*this)[2]) {
        Vec2f tmp;
        tmp = (*this)[2];
        if ((*this)[0] - (*this)[1] == (*this)[2] - (*this)[3]) {
          (*this)[2] = (*this)[3];
          (*this)[3] = tmp;
        } else {

          (*this)[2] = (*this)[1];
          (*this)[1] = tmp;
        }
      }
    }

    /**
    * @return true if the point is on or "above" the line made by the 2 first points
    * false if "under" the line
    */
    bool searchSide(const Vec2f &A, const Vec2f &B, const Vec2f &p) const {
      float distance1 =
              (-A[0] + A[1] - B[1] + p[0]) * (-A[0] + A[1] - B[1] + p[0]) +
              (-A[0] - A[1] + B[0] + p[1]) * (-A[0] - A[1] + B[0] + p[1]);
      float distance2 =
              (-A[0] - A[1] + B[1] + p[0]) * (-A[0] - A[1] + B[1] + p[0]) +
              (A[0] - A[1] - B[0] + p[1]) * (A[0] - A[1] - B[0] + p[1]);
      return distance1 <= distance2;
    }

    /**
   * @return true if an edge of the rectangle is a line separator with another rectangle
   */
    bool searchSeparatorWithDistance(const RectangleNA &r) const {
      Vec2f A = Vec2f(0.0f, 0.0f);
      Vec2f B = Vec2f(0.0f, 0.0f);
      Vec2f C = Vec2f(0.0f, 0.0f);
      for (int i = 0; i < 4; i++) {
        A = (*this)[i];
        (i == 0) ? B = (*this)[3] : B = (*this)[i - 1];
        (i == 3) ? C = (*this)[0] : C = (*this)[i + 1];
        bool thisSide = searchSide(A, B, C);
        for (int j = 0; j < 4; j++) {
          if (searchSide(A, B, r[j]) == thisSide)
            break;
          if (r[j] == r[3])
            return true;
        }
      }
      return false;
    }

    bool searchSeparator(const RectangleNA &r) const {
      return searchSeparatorWithDotProduct(r);
    }

    /**
     * This version of search separator uses dot product with a normal vector
     * to find on which side of the rectangle stand each point of the other rectangle
     * There is no need to actually compute the normal vector, since each side is the
     * inward pointing normal vector of the previous side
     * This way, it is only necessary to do a dot product with each side and check if it is < 0
     */
    bool searchSeparatorWithDotProduct(const RectangleNA &r) const {
      for (int i = 0; i < 4; ++i) {
        bool isASeparator = true;
        for (int j = 0; j < 4; ++j) {
          float currentDotP = inlineDotProduct((*this)[i], (*this)[(i + 1) % 4],
                                               r[j]);
          if (currentDotP > 0) {
            isASeparator = false;
            break;
          }
        }
        if (isASeparator) return true;
      }
      return false;
    }

    /**
   * @return true if there is an intersection with the rectangle r.
   */
    bool intersect(const RectangleNA &r) const {
      //if (boundingBoxIntersect(r))
      return !(searchSeparator(r) || r.searchSeparator((*this)));
      //return false;
    }

    bool intersect(const Rectf &r) const {
      return searchBoundingBox().intersect(r);
    }

    void translate(const Vec2f &tr) {
      for (int i = 0; i < 4; ++i) {
        (*this)[i] += tr;
      }
    }

    void scale(const float s) {
      for (int i = 0; i < 4; ++i) {
        (*this)[i] *= s;
      }
    }

    void rotate(const float angle) {
      float c = cos(angle), s = sin(angle);
      for (int i = 0; i < 4; ++i) {
        Vec2f tmp = (*this)[i];
        (*this)[i][0] = tmp[0] * c - tmp[1] * s;
        (*this)[i][1] = tmp[0] * s + tmp[1] * c;
      }
    }

    /**
  * Return true if point is inside the rectangle
  */
    bool isInside(const Vec2f &p) const {
      Rectf bb = (*this).searchBoundingBox();
      if (bb.isInside(p)) {
        Vec2f A = Vec2f(0.0f, 0.0f);
        Vec2f B = Vec2f(0.0f, 0.0f);
        Vec2f C = Vec2f(0.0f, 0.0f);
        for (int i = 0; i < 4; i++) {
          A = (*this)[i];
          (i == 0) ? B = (*this)[3] : B = (*this)[i - 1];
          (i == 3) ? C = (*this)[0] : C = (*this)[i + 1];
          if (searchSide(A, B, C) != searchSide(A, B, p))
            break;
          if (A == (*this)[3])
            return true;
        }
      }
      return false;

    }

};

#endif //RECTANGLE_NA_H
