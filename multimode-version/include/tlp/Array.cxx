//=================================================================
template<typename Obj, unsigned int SIZE>
Obj Array<Obj, SIZE>::operator[](const unsigned int i) const {
    assert(i < SIZE);
    return array[i];
}

//=================================================================
template<typename Obj, unsigned int SIZE>
Obj &Array<Obj, SIZE>::operator[](const unsigned int i) {
    assert(i < SIZE);
    return array[i];
}

//=================================================================
template<typename Obj, unsigned int SIZE>
std::ostream &operator<<(std::ostream &os, const Array <Obj, SIZE> &a) {
    os << "(";

    for (unsigned int i = 0; i < SIZE; ++i) {
        if (i > 0)
            os << ",";

        os << a[i];
    }

    os << ")";
    return os;
}
//#include <sstream>
//=================================================================
//template <typename Obj,unsigned int SIZE>
//QDebug operator<<(QDebug dbg,const Array<Obj,SIZE>& s) {
//  std::stringstream ss;
//  ss << s;
//  dbg.nospace() << ss.str().c_str();
//  return dbg.space();
//}

template<typename Obj, unsigned int SIZE>
std::istream &operator>>(std::istream &is, Array <Obj, SIZE> &outA) {
    char c;
    int pos = is.tellg();
    is.clear();

    // skip spaces
    while (bool(is >> c) && isspace(c)) {}

    if (c != '(') {
        is.seekg(pos);
        is.setstate(std::ios::failbit);
        return is;
    }

    for (unsigned int i = 0; i < SIZE; ++i) {
        bool ok;

        if (i > 0) {
            // skip spaces
            while ((ok = bool(is >> c)) && isspace(c)) {}

            if (!ok || c != ',') {
                is.seekg(pos);
                is.setstate(std::ios::failbit);
                return is;
            }
        }

        // skip spaces
        while ((ok = bool(is >> c)) && isspace(c)) {}

        is.unget();
        bool done = bool(is >> outA.array[i]);

        if (!done) {
            is.seekg(pos);
            is.setstate(std::ios::failbit);
            return is;
        }
    }

    // skip spaces
    while (bool(is >> c) && isspace(c)) {}

    if (c != ')') {
        is.seekg(pos);
        is.setstate(std::ios::failbit);
        return is;
    }

    return is;
}
//=================================================================
