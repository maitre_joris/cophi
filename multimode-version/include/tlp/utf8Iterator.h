#ifndef UTF8ITERATOR_H
#define UTF8ITERATOR_H

#include <string>

#include <Iterator.h>
#include <memorypool.h>

namespace tlp {

    class Utf8Iterator : public Iterator<wchar_t> {
    public:
        Utf8Iterator(const std::string s);

        bool hasNext();

        wchar_t next();

    private:
        const std::string utf8String;
        std::string::const_iterator iter;
    };

}

#endif // UTF8ITERATOR_H
