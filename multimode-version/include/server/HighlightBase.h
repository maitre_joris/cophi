#ifndef HIGHLIGHT_BASE_H
#define HIGHLIGHT_BASE_H

#include "data/Properties.h"

/* Class representing possible argument type to attach to highlight ("selection") request.
 * Highlighting can be induced by a node (or cluster), an edge (intersection of two clusters),
 * or multiples nodes
 * - A node is defined by a dimension identifier and a cluster identifier (ClusterPosition).
 * - An edge is defined its node ends (two ClusterPositions)
 * - An interval is defined by one or zero sequence of nodes per dimension, where a sequence of nodes
 * is a set of adjacent ClusterPosition (whose cluster identifiers are sequential).
 */

class HighlightBase {
private:
    enum BaseType {
        CLUSTER,
        EDGE,
        INTERVAL
    };
    ClusterPosition firstOrigin;
    ClusterPosition secondOrigin;
    std::vector<ClusterInterval> intervals;
    BaseType type;
    bool isEmpty = true;
public:
    HighlightBase();

    bool isValid() const;

    void clear();

    HighlightBase(unsigned int dimension, cluster id);

    HighlightBase(unsigned int firstDimension, cluster firstId,
                unsigned int secondDimension, cluster secondId);

    HighlightBase(const std::vector<ClusterInterval>& intervals);

    bool isClusterBase() const;

    bool isIntervalBase() const;

    bool isEdgeBase() const;

    ClusterPosition getClusterPosition() const;

    std::pair<ClusterPosition, ClusterPosition> getEdgePosition() const;

    std::vector<ClusterInterval> getIntervals() const;


};


#endif //HIGHLIGHT_BASE_H

