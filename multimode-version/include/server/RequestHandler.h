#ifndef REQUEST_BUILDER_H
#define REQUEST_BUILDER_H

#include<server/Server.h>
#include<server/HighlightBase.h>
#include<data/Buildable.h>
#include<data/DataProvider.h>
#include<util/DimensionInterval.h>
#include<util/AxisInterval.h>
#include<data/Properties.h>
#include<stack>
#include<unordered_map>


/**
 * Response parsing error are thrown for both reasons:
 * - syntactic reason (i.e. unexpected character type)
 * - semantic reason (edge refers to non-existent node)
 */

class ParseError {
public:
    static const std::string INVALID_LAST_TOKEN;
    static const std::string INVALID_TOKEN;
    static const std::string INVALID_VALUE;
    static const std::string INVALID_NODE;
    static const std::string INVALID_EDGE;
    static const std::string INVALID_LINE_START;
    static const std::string INCOHERENT_CONTENT;
    int line;
    int col;
    std::string message;

    ParseError(const std::string &msg, int l = -1, int c = -1)
            : line(l), col(c), message(msg) {};
};

/**
 * Response token types. A token is a sequence of one or more characters
 * containing exactly one delimiting character (comma, line break or EOF)
 * in its last position. Subsets of tokens (@see tokenType enum) are
 * the empty tokens and the non-empty tokens. Empty tokens are either:
 * EMPTY_VALUE, LINE_END or FILE_END. Non-empty tokens are REGULAR_VALUE,
 * LINE_LAST_VALUE and FILE_LAST_VALUE.
 */
enum tokenType {
    REGULAR_VALUE = (1 << 0),
    EMPTY_VALUE = (1 << 1),
    LINE_LAST_VALUE = (1 << 2),
    FILE_LAST_VALUE = (1 << 3),
    LINE_END = (1 << 4),
    FILE_END = (1 << 5)
};


/**
 * Server wrapper: send request and parse responses for a one dataSet (i.e. one
 * end point)
 */
class RequestHandler : public DataProvider {
private:
    Server *server = nullptr;
    std::string dataSet;
    // Same request type could be on same response variable
    std::string fullDataSetResponse = "";
    std::string intervalResponse = "";
    std::string highlightResponse = "";
    std::unordered_map<std::string, std::string> cache; // Interval response request cache
    std::deque<std::string> awaitingRequests; //TODO: flush responses when another request has been made in between
    const unsigned long CACHE_LIMIT = 200;

    /**
     * Build Buildable *b from parsing provided server response.
     * @param response string body of the received server response
     * @param b Buildable object (AxisData or SubGraph) to be receiving
     * modification from the response content
     * @param isHighlightResponse boolean used to skip some modifications
     * when building a SubGraph (@see parseNodeLine and parseEdgeLine)
     * @return false if the parsing failed, true otherwise
     */
    bool parseResponse(std::string &response,
                       Buildable *b,
                       bool withMinMax = true,
                       bool withHeader = false);

    /**
     * @brief Identify the type of a string token from provided input stream.
     * @param s input stream
     * @param result is filled with the value of non-empty tokens
     * @return tokenType
     * @return EMPTY_TOKEN
     */
    static tokenType readToken(std::istream &s, std::string &result);

    /**
     * @brief Read next token on provided input stream and compare its type to
     * the one provided.
     * @param type, required tokenType
     * @param s, input stream
     * @return valid-type token content
     * @throw ParseError if read type and provided type don't match
     */
    static std::string readTokenOfType(std::istream &s, tokenType type);

    /**
     * @brief Node part of the response reading process.
     * Server response are structured in two parts, a first block defining
     * clusters (a.k.a nodes) and their properties and a second part related
     * to edges. Lines from the node part have the following syntax:
     * {dimension},{label},{clusters} where 'clusters' is a comma-separated list
     * of cluster properties of format {id},{min},{max},{1||10},{distribution}.
     * A 'distribution' is one or a list of ten values (in accordance with the
     * preceding element).
     * In a highlight case, 'label', 'min' and 'max' are not provided.
     * @param s, input stream
     * @param b, element to be holding the modifications
     * @param isHighlightResponse, indicates how the response is parsed
     */
    bool parseNodeLine(std::istream &s, Buildable *b,
                       bool withMinMax,
                       bool withHeader);

    /**
     * @brief Edge part of the response reading process.
     * Server response are structured in two parts, a first block defining nodes
     * and a second part defining edges.
     * Lines from the edge part have the following syntax:
     * {dimension},{edges} where 'edges' is a comma-separated list of edge
     * properties of format {left},{right}. 'left' and 'right' are pairs of
     * dimension and cluster id.
     * * @param s, input stream
     * @param b, element to be holding the modifications
     */
    bool parseEdgeLine(std::istream &, Buildable *);


public:
    enum Type {
        INTERVAL,
        DATASET,
        HIGHLIGHTING
    };

    /**
     * Instantiate a handler using a server based on the provided
     * Configuration file.
     * @param c Configuration POCO
     */
    RequestHandler(const Server::Configuration &c);

    RequestHandler(const std::string &serverAddress);


    /**
     * Instantiate a handler using a MockServer (based on a local file)
     */
    RequestHandler();

    /**
     * Call dataSet endpoint with query parameters 'dimensions'
     * Example: basePath/dataSet?dimensions=...
     * Used for insertion and deletion (only when needed i.e. if the deleted
     * axis interval is not a prefix nor a postfix of the full axis sequence)
     * @params dims an ordered list of requested dimensions
     **/
    void requestInterval(const DimensionInterval dims);

    /**
     * Request missing edges between neighbours axes when deleting on
     * Only to be used in mode where the server is state-aware (typically
     * Hierarchical mode)
     * @param dimension identifier
     */
    void requestDeletion(const DimensionInterval dims);

    /**
     * Request missing data when inserting a dimension
     * Only to be used in mode where the server is state-aware (typically
     * Hierarchical mode)
     * @param dimension identifier
     * @param pos denotes the insertion position, with 0 being at head
     */
    void requestInsertion(const DimensionInterval dims, unsigned int pos);

    /**
     * Call dataSet endpoint for node induced highlighting
     * Example: basePath/dataSet?dimensions=...?&axe=...&pos=...
     * @params dims an ordered list of requested dimensions
     * @note Providing a dimension list is mandatory
     */
    void requestHighlight(const DimensionInterval dims,
                          const HighlightBase base);

    void requestFocusIn(unsigned int dbDim, cluster id);

    void requestFocusOut(unsigned int dbDim, unsigned int k);

    bool parseDataSetResponse(Buildable *) override;

    bool parseIntervalResponse(Buildable *) override;

    bool parseHighlightResponse(Buildable *) override;

    void performRequests();

    void requestDataSet(const std::string &dataSet,
                        std::function<void()> onResponse);

    bool receivedResponse(Type type) const;

};


#endif //REQUEST_BUILDER_H
