#ifndef QUERY_PARAMETERS_H
#define QUERY_PARAMETERS_H

#include <string>
#include <util/DimensionInterval.h>
#include <data/Properties.h>

class QueryParameters {
private:
    static const std::string DIMENSIONS_KEY;
    static const std::string FILTER_KEY;
    static const std::string NODE_DIMENSION_KEY;
    static const std::string NODE_ID_KEY;
    static const std::string EDGE_DIMENSION_SRC_KEY;
    static const std::string EDGE_DIMENSION_DST_KEY;
    static const std::string EDGE_ID_SRC_KEY;
    static const std::string EDGE_ID_DST_KEY;
    static const std::string CHAIN_KEY;
    static const std::string HIGHLIGHT_KEY;
    static const std::string START;
    static const std::string DELIMITER;
    /* Hierarchical mode keys */
    static const std::string FOCUS_OFF_KEY;
    static const std::string FOCUS_ON_KEY;
    static const std::string INSERT_KEY;
    static const std::string REMOVE_KEY;

    static std::string makeParam(const std::string &key,
                                 const std::string &value);

public:
    static std::string makeRemove(const DimensionInterval dims);

    static std::string makeInsert(const DimensionInterval dims, unsigned int pos);

    static std::string makeInterval(const DimensionInterval dims);

    static std::string makeHighlightNode(const DimensionInterval dims,
                                         unsigned int dbDim,
                                         cluster clusterId);

    static std::string makeFocusIn(unsigned int dbDim,
                                   cluster clusterId);

    static std::string makeFocusOut(unsigned int dbDim,
                                    unsigned int k);

    static std::string make(const DimensionInterval dims,
                            const std::vector<ClusterInterval> &intervals);


    static std::string makeHighlightEdge(const DimensionInterval dims,
                                         unsigned int srcDbDim,
                                         cluster srcClusterId,
                                         unsigned int dstDbDim,
                                         cluster dstClusterId);
};

#endif //QUERY_PARAMETERS_H
