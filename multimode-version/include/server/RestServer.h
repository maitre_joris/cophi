#ifndef REST_SERVER_H
#define REST_SERVER_H

#include <server/Server.h>
#include "server/HttpRequest.h"
#include <string>
#include <functional>
#include <vector>


class RestServer : public Server {
public:
    RestServer(const Configuration config);

    RestServer(const std::string &serverAddress);

    ~RestServer();

    void sendRequest(const std::string &requestPath,
                     std::function<void(std::string)> callback) override;

    bool performRequests() override;

    void clearWaitingRequests() override;

    static Configuration parseConfigFile(const std::string &filePath);

protected:
    std::string hostname;
    short port;
    std::string baseURL;
    std::vector<HttpRequest *> requests;
};

#endif // REST_SERVER_H
