#ifndef SERVER_H
#define SERVER_H

#include <string>
#include <functional>

class Server {
public:
    struct Configuration {
        std::string hostname = "";
        short int port = 0;
        std::string base = "";
    };

    Server() {}

    virtual ~Server() {}

    virtual void sendRequest(const std::string &requestPath,
                             std::function<void(std::string)> callback) = 0;

    virtual bool performRequests() = 0;

    virtual void clearWaitingRequests() = 0;
};

#endif //SERVER_H
