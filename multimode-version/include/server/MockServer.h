#ifndef MOCK_SERVER_H
#define MOCK_SERVER_H

#include <server/HttpRequest.h>
#include <server/Server.h>
#include <iostream>
#include <iterator>
#include <string>
#include <regex>

class MockServer : public Server {
public:
    MockServer();

    ~MockServer();

    void sendRequest(const std::string &requestPath,
                     std::function<void(std::string)> callback) override;

    bool performRequests() override;

    void clearWaitingRequests() override;

private:
    class QueryValidator {
        const std::string DIMENSIONS = "\\?dimensions=(([0-9]+),?)+";
        const std::string INSERTION = "\\?a=(([0-9]+),?)+;([0-9]+)";
        const std::string OPENING = "\\?r=(([0-9]+),?)+";
        const std::string CLOSING = "\\?o=(([0-9]+),([0-9]+))";
        const std::string DELETION = "\\?f=(([0-9]+),([0-9]+);?)?";
    };


    std::regex dimensionList;
    std::function<void(std::string)> callback;
    std::string responseFolderPath = "test/responses/";
    std::vector<unsigned int> dimensions;
};


#endif //MOCK_SERVER_H
