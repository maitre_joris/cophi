#ifndef PARACOORD_H
#define PARACOORD_H

#include"Visualization.h"
//
//void reshapeCallback(GLFWwindow *, int w, int h);
//
//void keyCallback(GLFWwindow *window, int key, int, int action, int);
//
//void mouseMoveCallback(GLFWwindow *window, double x, double y);
//
//void mouseButtonCallback(GLFWwindow *window, int button, int action, int mods);
//
//void mouseScrollCallback(GLFWwindow *window, double xOffSet, double yOffSet);
//
//bool outOfScreen(float x, float y);
//
//void errorCallback(int error, const char *description);
//
//void onKeyDown(int key);
//
///**
// * Receiving all command line except two first numbers, already processed as
// * integers to initialize the context
// */
//Visualization *loadFromArgs(unsigned int argc, char **argv,
//                            unsigned int width,
//                            unsigned int height);
//
//Visualization::Modifier getModifier(int key, int mod);
//
//void onKeyDown(int key);
//
//void onReshape(unsigned int width, unsigned int height);
//
//#ifdef LOCAL
//
//void loadData(const std::string &data,
//              bool file = true,
//              const std::string &ordinalDims = "",
//              const std::string &nominalDims = "",
//              float minValue = std::numeric_limits<float>::infinity(),
//              float maxValue = std::numeric_limits<float>::infinity(),
//              bool withColumnLabels = true,
//              bool withRowLabels = true,
//              unsigned int nbClusters = 2,
//              IClustering::Type clustering = IClustering::KMEANS);
//
//#else
//void loadData(const std::string &file,
//              const std::string& hostname,
//              short int port,
//              const std::string& baseName);
//#endif
//
//void center();
//
//void fit();
//
//void changeMode(int s);
//
//void setColorScheme(ColorScheme::Type scheme);
//
//unsigned long getNbAxes();
//
//unsigned long getNbSourceDims();
//
//unsigned long getNbIndividuals();
//
//std::string getDimensionLabel(unsigned int d);
//
//void switchInnerNodeView();
//
//void setGap(float x);
//
//void setDimensionToInsert(unsigned int d);
//
//void toggleLabels();
//
//void changeMapping();
//
//void exportData(const std::string &result,
//                bool highlightOnly,
//                bool includeDeletedDimensions);
//
//std::vector<float> getPick();
//
//DimensionList getDeletedDims();
//
//void changeGradientScope();


#endif //PARACOORD_H
