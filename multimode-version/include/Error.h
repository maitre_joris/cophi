#ifndef ERROR_H
#define ERROR_H

#include <exception>
#include <string>

class DataException : virtual public std::exception {
public:
    const char *what() const noexcept {
        return "DataException";
    }
};

class ImportError : virtual public std::exception {
public:
    enum Type {
        MEMORY = 0,
        CLUSTERING = 1,
        DELIMITER = 2,
        NO_FILE = 3,
        NONSQUARE_TABLE = 4,
        INVALID_VALUE = 5,
        UNEXPECTED = 6,
        SERVER = 7 // Merge error types?
    };

    ImportError(Type t, unsigned int l) : type(t) { formatMessage(t, l); }

    ImportError(Type t) : type(t) { formatMessage(t, 0); }

    ~ImportError() throw() {}

    Type getType() const { return type; };

    const char *what() const noexcept {
        return message.c_str();
    }


private:
    Type type;
    std::string message;

    void formatMessage(Type type, unsigned int line) {
        switch (type) {
            case MEMORY:
                message = "Out of memory at " + std::to_string(line);
                break;
            case CLUSTERING:
                message = "Invalid clustering argument";
                break;
            case DELIMITER:
                message = "Unrecognized of unconsistent delimiter at " + std::to_string(line);
                break;
            case NO_FILE:
                message = "Could not find file";
                break;
            case NONSQUARE_TABLE:
                message = "Uneven cell number per line at " + std::to_string(line);
                break;
            case INVALID_VALUE:
                message = "Found an invalid value at " + std::to_string(line);
                break;
            case SERVER:
                message = "Server connection failed or data set unavailable";
                break;
            case UNEXPECTED:
                message = "Unexpected error";
                break;
        }
    }
};


class InvalidStateError : virtual public DataException {
public:
    const char *what() const noexcept {
        return "InvalidStateError";
    }

};

class VoidPick : virtual public DataException {
public:
    const char *what() const noexcept {
        return "VoidPick";
    }

};

class ValueNotFound : virtual public DataException {
public:
    const char *what() const noexcept {
        return "ValueNotFound";
    }
};


class CanceledOperation : virtual public std::exception {
public:
    enum Type {
        EMPTY_SELECTION,
        NULL_AXIS_SWAP,
        ONE_AXIS_SHOULD_REMAIN,
        INVALID_AXIS_ARGUMENTS
    };
private:
    Type type;
public:
    CanceledOperation(Type t) : type(t) {};

    const char *what() const noexcept {
        switch (type) {
            case EMPTY_SELECTION:
                return "Box selection is empty";
            case NULL_AXIS_SWAP:
                return "No swap to perform";
            case ONE_AXIS_SHOULD_REMAIN:
                return "Cannot delete all axes";
            case INVALID_AXIS_ARGUMENTS:
                return "Provide axis interval or axis position is outside boundaries";
        }
        return nullptr;
    }
};

class QueryError : public DataException {
public:
    enum Type {
        NODE_UNKNOWN,
        EDGE_UNKNOWN,
        INVALID_QUERY
    };
private:
    Type type;
public:
    QueryError(Type t) : type(t) {};

    const char *what() const noexcept {
        switch (type) {
            case NODE_UNKNOWN:
                return "Unknown queried node";
            case EDGE_UNKNOWN:
                return "Unknown queried edge";
            case INVALID_QUERY:
                return "Badly formatted query parameters";
        }
        return nullptr;
    }

};

class BuildError : public DataException {
public:
    enum Type {
        DOUBLE_NODE,
        DOUBLE_EDGE,
        UNKNOWN_EDGE //Ends unknown
    };
private:
    Type type;
public:
    BuildError(Type t) : type(t) {};

    const char *what() const noexcept {
        switch (type) {
            case DOUBLE_EDGE:
                return "Doubled edges";
            case DOUBLE_NODE:
                return "Doubled nodes";
            case UNKNOWN_EDGE:
                return "Unknown edge ends";
        }
        return nullptr;
    }
};


#endif //ERROR_H
