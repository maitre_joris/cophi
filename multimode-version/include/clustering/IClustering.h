#ifndef ICLUSTERING_H
#define ICLUSTERING_H

#include <data/Properties.h>
#include <vector>
#include <set>
#include <map>

class IClustering {
public:
    enum Type {
        KMEANS,
        NONE,
        BINNING,
        ADAPTIVE_BINNING,
        CANOPY
    };

    IClustering(unsigned int k) : nbClusters(k) {}

    virtual ~IClustering() {}

    /**
     * Fill results vector with cluster identifiers for each data of
     * input vector
     **/
    virtual unsigned int run(const std::vector<float> &input,
                             std::vector<cluster> &result) = 0;

    unsigned int getNbClusters() const { return nbClusters; };

    virtual Type getType() const = 0;

    void renumberClusters(const std::set<cluster> &assigned,
                          std::vector<cluster> &result);

    void reorderClusters(const std::map<float, cluster> &assigned,
                         std::vector<cluster> &result);

    virtual std::string toString() const = 0;

protected:
    unsigned int nbClusters = 0;
};

#endif // ICLUSTERING_H
