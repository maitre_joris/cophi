#ifndef BINNING_H
#define BINNING_H

#include "IClustering.h"

class Binning : public IClustering {
public:
    Binning(unsigned int k);

    unsigned int run(const std::vector<float> &input,
                     std::vector<cluster> &result) override;

    Type getType() const override;

    std::string toString() const override;
};


#endif //BINNING_H
