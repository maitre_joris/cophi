#ifndef H_CLUSTERING_H
#define H_CLUSTERING_H

#include "clustering/IClustering.h"
#include "clustering/Hierarchy.h"

class HClustering : public IClustering {
public:
    HClustering(unsigned int k) : IClustering(k) {}

    virtual ~HClustering() {}

    virtual std::vector<HNode> bottomUp(const std::vector<float> &input) = 0;

    //virtual std::vector<HNode> topDown(const std::vector<float> &input) = 0;
protected:
    void setNbClusters(unsigned int newK) { nbClusters = newK; };

};

#endif //H_CLUSTERING_H
