#ifndef HIERARCHY_H
#define HIERARCHY_H

#include "data/Properties.h"
#include <set>
#include <limits>
#include <functional>

/* A class for grouping of elements meant to be constructed
 * in a hierarchical fashion, with each children cluster
 * being included in his parent */
class HNode : public ClusterProp {
private:
    std::vector<HNode> children;
    unsigned int childrenBiggestBucket = 0;
    HNode *parent = nullptr;

    void renumberChildren();

    void childrenBinarySearch(std::function<int(const HNode *)> compFunc,
                              std::function<void(unsigned int, bool)> resultFunc) const;

public:
    static const cluster ROOT_NODE_ID;

    HNode();

    HNode(cluster id);

    HNode(float value, const Individuals &occurrences, cluster id);

    HNode(const std::vector<HNode> &childrenNodes, cluster id);

    HNode(const HNode &n, unsigned int id);

    float distance(const HNode &o) const;

    bool isValid(unsigned int k, int l) const;

    unsigned int getChildPos(cluster id);

    const HNode *getChild(cluster id) const;

    HNode *insertChild(cluster id, unsigned int pos);

    cluster getMatchingChildrenId(std::function<bool(const HNode &)>) const;

    void applyChildren(std::function<void(const HNode &)>) const;

    cluster findInChildren(float value) const;

    unsigned int getMaxDepth() const;

    unsigned int getChildrenBiggestBucket() const;

    unsigned int getNbDescendants() const;

    HNode *getParent() const;

    std::vector<HNode> &getChildren();

    void reLink();

};

#endif //HIERARCHY_H

