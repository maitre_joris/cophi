#ifndef ADAPTIVE_BINNING_H
#define ADAPTIVE_BINNING_H

#include "IClustering.h"


class AdaptiveBinning : public IClustering {
public:

    AdaptiveBinning(unsigned int k);

    unsigned int run(const std::vector<float> &input,
                     std::vector<cluster> &result) override;

    Type getType() const override;

    std::string toString() const override;
};

#endif //ADAPTIVE_BINNING_H
