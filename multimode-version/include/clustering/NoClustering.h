#ifndef NO_CLUSTERING_H
#define NO_CLUSTERING_H

#include <clustering/HClustering.h>

class NoClustering : public HClustering {

public:
    NoClustering();

    ~NoClustering();

    unsigned int run(const std::vector<float> &input,
                     std::vector<cluster> &result) override;

    std::vector<HNode> bottomUp(const std::vector<float> &input) override;

    IClustering::Type getType() const override;

    std::string toString() const override;
};


#endif //NO_CLUSTERING_H
