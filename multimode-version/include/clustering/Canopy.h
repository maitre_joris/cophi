#ifndef CANOPY_H
#define CANOPY_H

#include "clustering/IClustering.h"
#include "clustering/HClustering.h"
#include "clustering/Hierarchy.h"

class Canopy : public HClustering {
public:
    Canopy(unsigned int k);

    unsigned int run(const std::vector<float> &input,
                     std::vector<cluster> &result) override;

    std::vector<HNode> bottomUp(const std::vector<float> &input) override;

    Type getType() const override;

    std::string toString() const override;
};

#endif //CANOPY_H
