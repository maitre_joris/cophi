#ifndef KMEANS_H
#define KMEANS_H

#include<cmath>
#include<unordered_map>
#include "IClustering.h"

class KMeans : public IClustering {
public:

    KMeans(unsigned int k, float e, unsigned seed,
           unsigned maxIter);

    KMeans();

    KMeans(unsigned int k, unsigned maxIter);

    ~KMeans();

    unsigned int run(const std::vector<float> &input,
                     std::vector<cluster> &result) override;

    float getEpsilon() const { return epsilon; }

    unsigned getMaxIteration() const { return maxIteration; }

    unsigned getSeed() const { return seed; }

    Type getType() const override;

    std::string toString() const override;

private:
    class Point {
    private:
        float value;
    protected:
        virtual void setValue(float v) {
          this->value = v;
        }

    public:
        Point() = delete;

        Point(float v) : value(v) {
        }

        Point(const Point &d) : value(d.value) {
        }

        float dist(const Point &d) const {
          return fabs(d.value - this->value);
        }

        float getValue() const {
          return value;
        }
    };

    class Centroid : public Point {
    private:
        int id;
        float minValue;
        float maxValue;
        unsigned int count = 0;

        void initMinMax(float v);

    public:
        void setValue(float v) override;

        void assign(const Point *p);

        Centroid(Point v, unsigned int group);

        unsigned int getId() const;

        void setFinalId(unsigned int id);

        void print() const;

        float getMin() const {
          return minValue;
        }

        float getMax() const {
          return maxValue;
        }

        unsigned int getCount() const {
            return count;
        }
    };

    class Value : public Point {
    private:
        Centroid *centroid;
    public:
        Value(float v) : Point(v) { }

        void setCentroid(Centroid *c) {
            if (c == nullptr) throw "Invalid cluster";
          this->centroid = c;
          c->assign(this);
        }

        Centroid *getCentroid() const {
          return centroid;
        }

        unsigned int getGroup() const {
          return centroid->getId();
        }

    };

    float epsilon;
    unsigned maxIteration;
    unsigned seed;

    void init(const std::vector<Value> &data,
              std::vector<Centroid *> &centroids) const;

    Centroid *nearest(const Value &pt,
                      std::vector<Centroid *> &centroids) const;

    void assignmentStep(std::vector<Value> &data,
                        std::vector<Centroid *> &centroids) const;

    float updateStep(std::vector<Value> &data,
                     std::vector<Centroid *> &centroids) const;

    bool hasConverged(float centroidDeviation) const;

    float doStep(std::vector<Value> &data,
                 std::vector<Centroid *> &centroids) const;

    static bool compCentroid(const Centroid *lhs, const Centroid *rhs) {
      if (std::abs(lhs->getMin() - rhs->getMin()) <
          std::numeric_limits<float>::epsilon())
        return lhs->getMax() < rhs->getMax();
      else return lhs->getMin() < rhs->getMin();
    }
};

#endif //KMEANS_H
