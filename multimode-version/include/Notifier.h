#ifndef NOTIFIER_H
#define NOTIFIER_H

#include <Error.h>

class Notifier {
public:
    enum Events {
        HIGHLIGHT_ON = 1,
        HIGHLIGHT_OFF = 2,
        AXIS_CHANGE = 3,
        HELP = 4,
    };
    // Other mode values are defined by State enum
    // and range in [3; 10]
    static unsigned int MAX_MODE_TO_BE_NOTIFIED;
    static unsigned int MIN_MODE_TO_BE_NOTIFIED;
public:
    static void onHighlight(bool isOn);

    static void onEvent(unsigned int event);

    static void onAxisChange();

    static void onError(ImportError::Type code);

    static void error(std::string s);

    static void info(std::string s);

    static void onVariationFilterChange(float center, float range);
};


#endif //NOTIFIER_H
