#ifndef VISUALIZATION_H
#define VISUALIZATION_H

#include "renderer/Renderer.h"
#include "util/DimensionSet.h"
#include "renderer/ColorScheme.h"
#include "data/DataHandler.h"
#include "server/RequestHandler.h"
#include "data/LocalData.h"
#include "Notifier.h"

const float ALTER_SENSITIVITY = 0.8;
const int MIN_MOVE = 20;

struct Pick {
    std::vector<float> numerical;
    std::string categorical;

    bool empty() {
        return numerical.empty() && categorical.empty();
    }
};

class TestVisualization;

class Visualization {
    friend TestVisualization;
public:
    /* Fixed values are those passed to JS */
    enum State {
        DEFAULT = 10,         // Highlight on click, modifier mode
        INVERT_AXIS = 20,     // Axis orientation
        MOVE_AXIS = 30,       // Axis delete+insert
        DELETE_AXIS = 40,     // Axis deletion
        INSERT_DIMENSION = 50,// Axis insertion
        SCALE = 60,          // Overall dimension, modifier mode, no pan
        FILTER = 70,         // Highlight w/ sliders
        PICKER = 80,         // Info on click
        EDGE_LOOK = 81,      // Change edge look, modifier mode, no pan
        FOCUS = 90,          // Focus+context exploration
        VARIATION = 100,      // Highlight from edge angle range (variation)
        TOP_VARIATION = 110   // Highlight from edge angle range (variation)
    };

    enum class Modifier {
        SHIFT,
        CONTROL,
        ALT,
        NONE
    };

    enum class MoveStage {
        IDLE = 0,
        DELETING,
        INSERTING
    };

    enum class Animating {
        NONE,
        DELETION,
        INSERTION,
        FOCUS
    };

    enum class SwitchableParameter {
        INNER_NODE_VIEW,
        GRADIENT,
        HEIGHT_MAPPING,
        GRADIENT_SCOPE,
        CONTEXT_COMPRESSION_MODE,
        INTERVAL_HIGHLIGHT_MODE,
        DISPLAY_AXES,
        DISPLAY_LABELS
    };

    enum class ContinuousParameter {
        CONTEXT_COMPRESSION,
        EDGE_REDUCTION,
        EDGE_CURVATURE,
        EDGE_DEFINITION,
        FREQUENCY_DEFINITION,
    };

    enum class EnhancementMode { //TODO
        FOCUSABLE_ONLY,
    };

    struct EdgeFilter {
    private:
        //unsigned int axis = 0;
        float center = 0.f;
        float range = 0.05f;
        const float MAX_VAR = 1.f;
        const float MIN_VAR = -1.f;
    public:

        float getRange() const {
            return range;
        }

        float getCenter() const {
            return center;
        }

        void setRange(float value) {
            range = std::max(0.f, std::min(value, 2.f));
            Notifier::onVariationFilterChange(center, range);
        }

        void setCenter(float value) {
            center = std::max(-1.f, std::min(value, 1.f));
            Notifier::onVariationFilterChange(center, range);
        }

        float getMin() const {
            return std::max(MIN_VAR, center - range / 2.f);
        }

        float getMax() const {
            return std::min(MAX_VAR, center + range / 2.f);
        }
    };

#define EPS 0.001

    struct CursorPos {
        float x = 0;
        float y = 0;
        unsigned int tick = 0;

        void update(float x, float y) {
//            if (std::abs(x - this->x) < EPS && std::abs(y - this->y) < EPS) {
//                tick++;
//            } else {
            tick = 0;
            this->x = x;
            this->y = y;
            // }
        }
    };

    DataHandler *dataSource = nullptr;
    DisplayParameters displayParameters;
    EdgeFilter selectedEdges;
    Renderer *renderer = nullptr;

private:
    State state = DEFAULT;
    Display *display = nullptr;
    RequestHandler *requester = nullptr;
    int width, height;
    DimensionInterval selectedDims;
    DimensionSet deletedDims;
    AxisInterval selectedAxes;
    int insertAfter = -1;
    float fromWidth, fromHeight, fromGap, fromEdge, fromNodeWidth, fromCurvature, fromContextCompression;
    float fromX, fromY;
    CursorPos last;
    Animating animating = Animating::NONE;
    MoveStage animatingMove = MoveStage::IDLE;
    bool selectionBox = false;
    bool movingThumbnail = false;
    bool movingSlider = false;
    Pick pick;
    Vec2f futureInsertionPos;
    Modifier currentModifier = Modifier::NONE;
    bool doPan = false;
    int focusAxis = -1;
    // TODO Unify selection input
    std::vector<SelectedElement> selectedElements;
    std::vector<SelectedElement> tmpSelectedElements;

    bool isAxisSelectionMode(State s) const;

    bool isPanMode(State s) const;

    void animateDeletion(float time);

    void animateInsertion(float time);

    void animateFocus(float time);

    void animateMoveInsertion(float time);

    void animateMoveDeletion(float time);

    void initiateBoxSelection(float x, float y);

    void terminateBoxSelection();

    void initiateAxisThumbnail(float x, float y);

    void terminateAxisThumbnail(float x, float y);

    void cancelSelection();

    void cancelMove();

    void handleSelectionBox(float x, float y);

    HighlightingOp modifierToOp(Modifier mod);

    void passiveAxisSelection(float x, float y);

    void previewHighlight(float x, float y);

    bool highlight(std::vector<SelectedElement> &elts);

    bool focus(float x, float y);

    bool isHighlighting();

    void onSelectionBoxClick(float time);

    void scale(float dx, float dy);

    bool configureMultithreading() const;

    void triggerDeletion();

    void triggerInsertion();

    void triggerFocus();

    void readAxisOperationResponse();

    void readHighlightResponse();

    void triggerMoveInsertion();

    void triggerMoveDeletion();

    void init();

    void printError() const;

    Pick createNodePick(const NodeProp &np, float weightRatio);

    Pick createEdgePick(float weight, float weightRatio);

    Pick createRegularPick(const SelectedElement &e);

    void highlightFromEdgeVariation(float x, float y);

    void enhanceFromEdgeVariation(float x, float y);

public:
    bool activateEnhancement = false;

    Visualization(unsigned int width, unsigned int height);

    ~Visualization();

    void capture();

    void switchParameter(SwitchableParameter parameter, float time);

    void changeMode(State s);

    void loadLocalData(const TableHandler::Configuration &config);

    void loadDataSet(const std::string &dataSet);

    void loadRemoteData(const Server::Configuration &config,
                        const std::string &dataSet);

    void loadRemoteData(const std::string &dataSet,
                        const std::string &serverAddress);

    void loadHierarchy(const std::string &dataFile,
                       const std::string &hierarchyFile);

    std::string getDimensionLabel(unsigned int dId) const;

    unsigned long getNbAxes() const;

    void setGap(float x);

    void center();

    void fit();

    bool isOutOfScreen(float x, float y);

    void onActiveMouseMove(float x, float y);

    void hideHighlightPreview();

    void insertDimension(float x, float y);

    Pick pickElement(float x, float y);

    void onMouseWheel(float x, float y, float dy);

    void onReshape(unsigned int width, unsigned int height);

    void onDraw();

    void onIdle(float time);

    void onMouseMove(float x, float y);

    void insertDimension(unsigned int);

    void deleteAxis(unsigned int);

    bool isReady() const;

    void onLeftButtonRelease(float x, float y, float time);

    void onLeftButtonPress(float x, float y);

    void onPassiveMouse(float x, float y);

    bool isAnimating() const;

    void enhanceElement(float x, float y, bool nodeOnly = true);

    /**
     * Local mode only option: write on output file the CSV formatted
     * content (individuals) of all or a portion of visualization axes values.
     * @param highlightedOnly specifies that output values should be filtered
     * to match current highlighting
     * @param includeDeletedDimensions specifies that even hidden (deleted) axes
     * values should be included
     * @param result, either a filePath or a string meant to hold the result
     */
    void exportData(std::string &result,
                    bool inFile,
                    bool highlightedOnly,
                    bool includeDeletedDimensions);

    Pick getPick();

    DimensionSet getDeletedDims();

    void setFutureInsertionPosition(float x, float y);

    void setColorScheme(ColorScheme::Type scheme);

    void setCurrentModifier(Modifier mod);

    void clear();

    Visualization::Modifier getCurrentModifier() const;

    unsigned int getNbSourceDimensions() const;

    unsigned long getNbIndividuals() const;

    Visualization(std::vector<std::vector<float>> &values, unsigned int k);

};


#endif //VISUALIZATION_H
