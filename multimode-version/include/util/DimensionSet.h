#ifndef DIMENSION_SET_H
#define DIMENSION_SET_H


#include <set>
#include "DimensionInterval.h"
typedef std::vector<unsigned int> DimensionList;

class DimensionSet {
    std::set<unsigned int> dimensions;

public:
    void add(DimensionInterval &c);

    void remove(DimensionInterval &c);

    void clear();

    bool isEmpty() const;

    DimensionList getAsVector() const;

};

#endif //DIMENSIONSET_H
