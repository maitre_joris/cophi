#ifndef UTILS_H
#define UTILS_H

#include <vector>
#include <regex>
#include <thread>
#include <list>
#include <iostream>
#include <chrono>
#include <tlp/Color.h>
#include <tlp/glmatrix.h>
#include <tlp/Vector.h>
#include <data/Properties.h>

typedef Vector<float, 5, double> Vec5f;

Vec5f vec5(float a, float b, float c, float d, float e);

std::string fileExtension(const std::string &path);

bool rectContains(const Vec4f r, const Vec2f p);

bool rectIntersects(const Vec4f a, const Vec4f b);

Vec4f screenRect(const GlMat4f &invProj);

void tokenize(const std::string &str,
              std::vector<std::string> &tokens,
              const char delimiter = ' ');

typedef std::chrono::high_resolution_clock Clock;
typedef std::chrono::milliseconds milliseconds;
typedef std::chrono::microseconds microseconds;

void printSpentTime(Clock::time_point t0, std::string task);

long timeDifference(Clock::time_point t0);

std::string join(const std::vector<unsigned> &v, const std::string &t);

Vec4f to_gl(Color);

template<typename T>
void emplace(std::vector<T> &v, int prev, size_t size) {
    assert(size > 0);
    assert(prev >= -1);
    v.resize(v.size() + size);
    for (int i = v.size() - 1; i > (int) (prev + size); --i) {
        v[i] = v[i - size];
    }
}

template<typename T>
void erase(std::vector<T> &v, unsigned int begin, size_t size) {
    assert(v.size() >= begin + size);
    v.erase(v.begin() + begin, v.begin() + begin + size);
}

template<typename T>
T between(T a, T x, T b) {
    return std::max(a, std::min(x, b));
}

template<typename T>
bool isBetween(T inf, T value, T sup) {
    return value >= inf && value <= sup;
}

bool isBlank(const std::string &str);

void fillDistribution(float value,
                      Distribution &distribution,
                      float min,
                      float max);

template<class I, class R>
R multiThreadingWrapper(unsigned int nbElements,
                        std::function<const I(int)> getElement,
                        std::function<void(const I &, int, R &)> handleChunckItem,
                        std::function<void(const R &, R &)> mergeResult,
                        R &result0) {
    const unsigned nbThreads = std::thread::hardware_concurrency();
    std::vector<std::thread *> threads = std::vector<std::thread *>(nbThreads, nullptr);
    std::vector<R> threadResult(threads.size());
    unsigned int chunks = nbElements / nbThreads;

    for (unsigned int t = 0; t < threads.size(); t++) {
        unsigned int a = t * chunks;
        unsigned int b = t == threads.size() - 1 ? nbElements : a + chunks;

        if (a <= b) {
            threads.at(t) = new std::thread([a, b, t,
                                                    &threadResult,
                                                    &getElement,
                                                    &handleChunckItem]() {
                for (unsigned int i = a; i < b; i++) {
                    handleChunckItem(getElement(i), i, threadResult.at(t));
                }
            });
        }
    }

    for (unsigned int t = 0; t < threads.size(); t++) {
        if (threads.at(t) != nullptr) {
            threads.at(t)->join();
            mergeResult(threadResult.at(t), result0);
            delete threads.at(t);
        }
    }
    return result0;
}

namespace nu {

    const int MILLION = (int) std::pow(10.f, 6);
    const int BILLION = (int) std::pow(10.f, 9);
    const int KILO = (int) std::pow(10.f, 3);

    std::string integerToString(int i);

    int readIntegerString(const std::string &s);
}


#endif
