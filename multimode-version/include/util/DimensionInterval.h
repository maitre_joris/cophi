#ifndef DIMENSION_INTERVAL_H
#define DIMENSION_INTERVAL_H

#include<vector>
#include<iostream>

class DimensionInterval {
private:
    std::vector<unsigned int> orderedDimensions;
public:
    class NotFound {

    };

    DimensionInterval();

    DimensionInterval(unsigned int dim);

    DimensionInterval(unsigned int left, unsigned int right);

    DimensionInterval &chain(unsigned int d);

    DimensionInterval &chain(const DimensionInterval &d);

    void clear();

    bool empty() const;

    unsigned int at(unsigned int i) const;

    typedef std::vector<unsigned int>::const_iterator const_iterator;

    const_iterator begin() const;

    const_iterator end() const;

    size_t size() const;

    std::string toCSV() const;

    void remove(unsigned int pos, unsigned int size);

    void insert(int posBefore, const DimensionInterval &dims);

    const std::vector<unsigned int> asVector() const;

    unsigned int find(unsigned int dbDim) const;
};


#endif //DIMENSION_INTERVAL_H
