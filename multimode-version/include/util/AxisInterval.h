#ifndef AXIS_INTERVAL_H
#define AXIS_INTERVAL_H

#include <iostream>

const static int NONE_ID = -1;

/**
 * Inclusive axis interval used for batch operation on axes
 */
class AxisInterval {
private:
    unsigned int left = 0;
    unsigned int right = 0;
    bool empty = false;

    void expand();
public:
    class InvalidAxisInterval {

    };

    class const_iterator {
        friend class AxisInterval;

    public:
        //typedef unsigned int value_type;
        //typedef int difference_type;

        unsigned int operator*() const { return i_; }

        const const_iterator &operator++() {
          ++i_;
          return *this;
        }

        const_iterator operator++(int) {
          const_iterator copy(*this);
          ++i_;
          return copy;
        }

        bool operator==(const const_iterator &other) const {
          return i_ == other.i_;
        }

        bool operator!=(const const_iterator &other) const {
          return i_ != other.i_;
        }

    protected:
        const_iterator(int start) : i_(start) { }

    private:
        unsigned int i_;
    };

    AxisInterval();

    AxisInterval(const AxisInterval &axes, bool expand = false);

    AxisInterval(unsigned l, unsigned r);

    AxisInterval(int l, int r);

    bool isEmpty() const;

    bool contains(unsigned int i) const;

    bool isDirectLeftNeighbour(unsigned int i) const;

    size_t size() const;

    void clear();

    /**
     * Returns true if i is on the left of the axis range
     * @param i axis id or -1 (denoting head position)
     */
    bool isBefore(int i) const;

    unsigned int getLeft() const;

    unsigned int getRight() const;

    unsigned int getSingleAxis() const;

    unsigned int operator[](int index) const;

    const_iterator begin() const;

    const_iterator end() const;

    std::string toString();

    bool isSideOn(unsigned int lastAxisId) const;
};

#endif //AXIS_INTERVAL_H
