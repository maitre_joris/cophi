#ifndef MATRIXMANAGER_H
#define MATRIXMANAGER_H

#include <stack>
#include "glmatrix.h"
#include "util/Utils.h"

class MatrixManager {
public:
    MatrixManager();

    // Matrices get/set
    GlMat4f getModelViewMatrix() const { return mModelView; }

    GlMat4f getProjectionMatrix() const { return mProjection; }

    GlMat4f getMatProjMod();

    void setModelViewMatrix(GlMat4f m);

    void setProjectionMatrix(GlMat4f m);

    //Matrices operations
    void moveModel(Vec3f translationVector);

    void moveScreen(Vec3f translationVector);

    void zoom(float scale, Vec2f screenPos);

    void initViewport(Vec4i viewport);

    void changeViewport(Vec4i viewport);

    void centerBox(Vec4f modelBox);

    void reinitMatrices();

    //coordinates conversions
    Vec2f modelToScreen(const Vec3f model);

    Vec3f screenToModel(const Vec2f screen);

    Vec2f windowToScreen(const Vec2f window) const;

    Vec2f windowToModel(const Vec2f window);

    int getWindowHeight() const { return winH; }

    int getWindowWidth() const { return winW; }

    bool hasChanged() { return changed; }

    void reinitChangedState() { changed = false; }

    //push/pop
    void pushModelViewMatrix();

    void pushProjectionMatrix();

    void popModelViewMatrix();

    void popProjectionMatrix();

    float windowToModelFactor() const;

private:
    GlMat4f mModelView;
    GlMat4f mProjection;
    GlMat4f mProjMod;
    std::stack<GlMat4f> modelStack;
    std::stack<GlMat4f> projStack;
    bool shouldRecompute;
    bool changed;
    int winW, winH;

    void computeMatProjMod();
};


#endif // MATRIXMANAGER_H
