#ifndef DISPLAY_PARAMETERS_H
#define DISPLAY_PARAMETERS_H

#include <tlp/ColorScale.h>
#include <renderer/ColorScheme.h>

typedef struct sizes {
    float height = 1.5f;
    float axisSpacing = 4.f;
    float maxNodeWidth = 1.f;
    float minNodeWidth = .4f;
    float weightVerticalSpace = 0.7f;     // [0;1] portion of height dedicated to nodes IN WEIGHT MODE
    float contextCompression = 0.3f;      // Either a portion of node height or a compression rate
} Sizes;

enum ContextCompression {
    CONSTANT_SCREEN_SPACE,
    CONSTANT_COMPRESSION
};

enum IntervalHighlight {
    RELATIVE_TO_TOTAL,
    RELATIVE_TO_ELEMENT
};

enum HeightMapping {
    WEIGHT,
    INTERVAL_SIZE
};

class DisplayParameters {
public:
    class ParameterObserver {
    public:
        virtual void onViewsNeedUpdate() = 0;

        virtual void onColorChange() = 0;

        virtual void onScaleChange() = 0;

        virtual void onAxisChange() = 0;

        virtual ~ParameterObserver() {};
    };

    const float THUMBNAIL_SCALE_FACTOR = 0.5f;
    const float LABEL_MARGIN = .50f;


private:
    ParameterObserver *observer = nullptr;
    Sizes dimensions;
    float edgeCurvature = 0.1f; // [0;1]
    float edgeReduction = 0.3f; // [0;1]
    ContextCompression contextMode = CONSTANT_SCREEN_SPACE;
    HeightMapping mappingMode = INTERVAL_SIZE;
    IntervalHighlight intervalHighlight = RELATIVE_TO_TOTAL;
    bool globalGradient = false;
    bool showAxes = true;
    bool showLabels = true;
    bool showEdges = true;
    bool showDistortion = false;
    unsigned int frequencyDefinition;
    unsigned int edgeDefinition;
    unsigned int colorScaleIndex = 3;
    ColorScale colorScale;
    float greyScale = .2f;
    int nbHorizontalLines = 0;

public:
    DisplayParameters();

    void setGreyScale(float v);

    float getGreyScale() const;

    void attachObserver(ParameterObserver *observer);

    void setContextCompression(float compression);

    float getContextCompression() const;

    void setContextMode(ContextCompression m);

    void setHeightMappingMode(HeightMapping m);

    HeightMapping getHeightMappingMode() const;

    void setIntervalHighlightMode(IntervalHighlight m);

    IntervalHighlight getIntervalHighlightMode() const;

    void setHeight(float height);

    void setGapRatio(float gap);

    int getGridLines();

    void setSpacing(float width);

    void setNodeWidth(float nodeWidth);

    void setFrequencyDefinition(unsigned def);

    unsigned getFrequencyDefinition() const;

    void setEdgeDefinition(unsigned def);

    float getHeight() const;

    float getGapRatio() const;

    float getAxisSpacing() const;

    float getMaxNodeWidth() const;

    float getMinNodeWidth() const;

    float getAxisWidth() const;

    const Sizes &getDimensions() const;

    bool displayAxes() const;

    bool displayEdges() const;

    bool displayLabels() const;

    void setColorScale(unsigned int index);

    bool displayDistortion() const;

    ContextCompression getContextMode() const;

    float getPadding() const;

    float getEdgeReduction() const;

    float getEdgeCurvature() const;

    void setEdgeReduction(float);

    void setEdgeCurvature(float);

    unsigned getEdgeDefinition() const;

    void setDistortion(bool display);

    void setDisplayLabels(bool display);

    void setDisplayAxes(bool display);

    const ColorScale &getColorScale() const;

    bool globalGradientMode() const;

    void setGlobalGradient(bool b);

    void nextGradient();
};


#endif //DISPLAY_PARAMETERS_H


