#ifndef TRIANGLE_VIEW_H
#define TRIANGLE_VIEW_H

#include "renderer/item/VisualItem.h"
#include <Vector.h>

class TriangleView : public VisualItem {
private:
    static const Vec2f VERTICES[]; // for one triangle
    float position; // in [0; 1]
public:
    static const float HEIGHT;

    TriangleView();

    TriangleView(float p);

    Vec4f getPoint(unsigned int axisId, bool upper, unsigned int id);

    float getPosition() const;

    virtual void setPosition(float p);


};

#endif //TRIANGLE_VIEW_H
