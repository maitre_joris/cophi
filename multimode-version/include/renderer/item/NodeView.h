#ifndef NODE_VIEW_H
#define NODE_VIEW_H

#include "renderer/item/VisualItem.h"
#include "renderer/DisplayParameters.h"
#include "tlp/Vector.h"
#include <vector>

class NodeView : public VisualItem {
public:
    const static float CONTEXT_REDUCTION_WIDTH;
    const static float NO_SUBWEIGHT;
    const static Vec4f CONTEXT_COLOR;
    const static float MIN_SIZE;

    float weight = 0.f;
    float gapBefore;
    float weightBefore;
    float x;

    float distortion = 0.f;
    float subWeight = NO_SUBWEIGHT;
    float invert = 0.f;
    float interval;

    float intervalBefore;
    float biggestBucket;
    float widthReduction = 0.f;
    float enhancement = false;

    Vec4f color = Vec4f(0.f);

    void setAlpha(float alpha);

    void setInversionRate(float invertRate);

    float getInversionRate() const;

    void setEnhancement(bool v);

    bool isValid() const;

    void setAsContext();

    void setBiggestBucket(const std::vector<float> &d);

    void print() const;

    Vec4f getDimensions(const DisplayParameters *dp, float w2i) const;

    Vec4f bounds(const DisplayParameters *dp, float w2i) const;


};

class DistribView : public VisualItem {
public:
    float distrib[10];
    float nbElement = 0.f;
    float padding1;

    Vec4f padding2;

    DistribView() {
    }

    DistribView(const std::vector<float> &d) {
        for (unsigned j = 0; j < d.size(); j++)
            distrib[j] = d[j];
        nbElement = float(d.size());
    }
};

#endif //NODE_VIEW_H
