#ifndef EDGE_VIEW_H
#define EDGE_VIEW_H


class EdgeView : public VisualItem {
public:
    float sourcePos = 0.f; // [0;1] vertical position on the source node
    float destPos = 0.f;   //                         on the destination node
    float z = 0.f;
    float subWeight = -1.f;

    float sourceWeight = 0.f;
    float destWeight = 0.f;
    float weight = 0.f;   // Canonical weight
    float isHighlighted = false;

    float padding2;
    Vec3f idColor; // for color picking


    NodeView end1;
    NodeView end2;

    static bool moreOpaque(const EdgeView &e1, const EdgeView &e2) {
        float alpha1 = std::min(e1.end1.color[3], e1.end2.color[3]);
        float alpha2 = std::min(e2.end1.color[3], e2.end2.color[3]);
        return alpha1 > alpha2;
    }

    void setAlpha(float alpha) {
        end1.setAlpha(alpha);
        end2.setAlpha(alpha);
    }

    void setEnhancement(bool v) {
        end1.setEnhancement(v);
        end2.setEnhancement(v);
    }

    void setWeights(float main, float src, float dst) {
        weight = main;
        sourceWeight = src;
        destWeight = dst;
    }

    Vec4f bounds(const DisplayParameters *dp, float w2i) {
        const Vec4f r1 = end1.getDimensions(dp, w2i);
        const Vec4f r2 = end2.getDimensions(dp, w2i);
        const float botLeftY = std::min(r1.y(), r2.y());
        const float topRightY = std::max(r1.y() + r1.w(), r2.y() + r2.w());
        return Vec4f(r1.x() + r1.z(), botLeftY, r2.x(), topRightY);
    }
};

#endif //EDGE_VIEW_H
