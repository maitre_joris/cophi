#ifndef SELECTION_BOX_H
#define SELECTION_BOX_H

#include <cmath>
#include <util/AxisInterval.h>

class FramingBox {
private:
    // Model coordinates
    float firstX;
    float secondX;
    bool isActive = false;
public:

    void setFirstPoint(float x);

    void setSecondPoint(float x);

    float getLeftX() const;

    float getRightX() const;

    void hide();

    void show();

    bool isShown();

    bool contains(float x) const;

    void scale(float factor);

    void adapt(const AxisInterval &axes,
               float padding,
               float width,
               float nodeWidth);
};


#endif //SELECTION_BOX_H
