#ifndef SOLID_RECTANGLE_RENDERER_H
#define SOLID_RECTANGLE_RENDERER_H

#include <tlp/Vector.h>
#include <tlp/glmatrix.h>
#include "renderer/ShaderProgram.h"

class SelectionBoxRenderer {
public:
    struct SolidRectangle {
        Vec2f upperLeft;
        Vec2f lowerRight;
        float z;

        Vec2f getLowerLeft() const {
            return Vec2f(upperLeft.x(), lowerRight.y());
        }

        float getWidth() const {
            return std::abs(upperLeft.x() - lowerRight.x());
        }

        float getHeight() const {
            return std::abs(upperLeft.y() - lowerRight.y());
        }
    };

private:
    const unsigned int POINT_SIZE = 7;
    const unsigned int NB_POINTS = 4;

    class SelectionBoxProgram : public ShaderProgram {
    public :
        SelectionBoxProgram();

        GLint z;
        GLint matProjMod;
        GLint width;
        GLint height;
        GLint res;
        GLint lowerLeftAnchor;
    };

    SelectionBoxProgram boxProgram;
    GLuint overlayBuffer;
    GLuint lineBuffer;
    GLuint buffer;

    void startup();

    void shutdown();

public:
    SelectionBoxRenderer();

    virtual ~SelectionBoxRenderer();

    void setBuffer();

    // const mat here does not work
    void draw(GlMat4f mat, const Vec2f res, SolidRectangle r);
};


#endif //SOLID_RECTANGLE_RENDERER_H
