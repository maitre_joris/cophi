#ifndef VIEW_STORAGE_H
#define VIEW_STORAGE_H

#include <data/ViewData.h>
#include <data/DataHandler.h>
#include <renderer/animation/Animation.h>
#include "renderer/item/NodeView.h"
#include "renderer/item/EdgeView.h"
#include "Vector.h"
#include <deque>
#include <unordered_map>

class NodeViewStorage {
private:
    const ViewData *data;
    std::unordered_map<node, std::pair<NodeView, std::deque<NodeView>>> contextNodeViews;
    std::unordered_map<node, std::pair<NodeView, DistribView>> nodeViews;
public:
    std::vector<float> largestFocusWeights;

    enum class Type {
        FOCUS,
        CONTEXT_DETAIL,
        CONTEXT_SHELL
    };

    class ViewNotFound {

    };

    NodeViewStorage();

    NodeViewStorage(const ViewData *base);

    void initNodeViews(unsigned int size);

    virtual void clear();

    NodeView &getNodeView(const node &n);

    const NodeView &getNodeView(const node &n) const;

    DistribView &getDistribView(const node &n);

    const DistribView &getDistribView(const node &n) const;

    bool hasNode(const node &n) const;

    const ViewData *getViewData();

    const std::unordered_map<node, std::pair<NodeView, DistribView>> &getNodeViews() const;

    void insertNodeView(const node n,
                        const NodeView &nv,
                        const DistribView &dv);

    void insertContextNodeView(const node n, const NodeView &shell, std::deque<NodeView> &details);

    std::deque<NodeView> &getDetailViews(const node &n);

    const std::deque<NodeView> &getDetailViews(const node &n) const;

    virtual bool empty() const;

    void setNodeAlpha(const node &n, float alpha);

    void setNodesAlpha(float alpha);

    void setNodesAlphaIf(float alpha, std::function<bool(const NodeView &, Type type)> f);

    const std::unordered_map<node, std::pair<NodeView, std::deque<NodeView>>> &getContextViews() const;

    NodeView &getDetailView(const node &n, unsigned int subNo);

    void repositionNodes(const Dimension &orderedNodes, bool weightOnly);

    void rescaleWeightInterval(const Dimension &orderedNodes, bool weightOnly);

    bool allValid() const;

    unsigned long getNbNodes() const;
};

class ViewStorage : public NodeViewStorage {
private:
    std::unordered_map<edge, EdgeView> edgeViews;
    std::vector<edge> sortedEdgeKeys;
    float maxEdgeWeight;

public:

    ViewStorage();

    ViewStorage(const ViewData *base);

    void initEdgeViews(unsigned int size);

    void clear() override;

    void sortEdges();

    bool empty() const override;

    unsigned long getNbEdges() const;

    EdgeView &getEdgeView(const edge &e);

    const EdgeView &getEdgeView(const edge &e) const;

    bool hasEdge(const edge &e) const;

    void insertEdgeView(const edge e, EdgeView &ev);

    const std::unordered_map<edge, EdgeView> &getEdgeViews() const;

    void setEdgeAlphaIf(float alpha, std::function<bool(const EdgeView &)> f);

    float getMaxEdgeWeight() const;

    const std::vector<edge> &getSortedEdges() const;
};


#endif
//VIEW_STORAGE_H
