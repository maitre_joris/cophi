#ifndef SHADER_PROGRAM_H
#define SHADER_PROGRAM_H

#include <string>

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>

/**
 * @brief ShaderProgram class
 */
class ShaderProgram {
private:
    std::string _name;

    /**
     * @brief Compile a shader
     * @param source shader source
     * @param type shader type (must be either GL_VERTEX_SHADER,
     * GL_TESS_CONTROL_SHADER, GL_TESS_EVALUATION_SHADER,
     * GL_GEOMETRY_SHADER, GL_FRAGMENT_SHADER, or GL_COMPUTE_SHADER)
     * @return shader identifier
     */
    GLuint createShader(const char source[], GLenum type);

    /**
     * Bind the program object to the OpenGL Context
     */
protected:
    GLuint program;
public:
    void use();

    GLuint getAttributeLocation(const char *attribute);

    /**
    * @brief Compile a program from the given shaders
    */
    ShaderProgram(std::string filename);

    virtual ~ShaderProgram();
};


#endif // SHADER_PROGRAM_H
