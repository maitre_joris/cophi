#ifndef COLOR_SCHEME_H
#define COLOR_SCHEME_H

#include<vector>
#include<Color.h>

class ColorScheme {
public:
    enum Type {
        GREEN_BLACK_RED = 0,
        BABY_ROOM = 1,
        PALAMPA = 2,
        VIRIDIS = 3,
        LEGACY = 4
    };
    static std::vector<std::vector<Color>> SCHEMES;
};
#endif //COLOR_SCHEME_H
