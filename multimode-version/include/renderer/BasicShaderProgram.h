#ifndef BASIC_SHADER_PROGRAM_H
#define BASIC_SHADER_PROGRAM_H

#include<renderer/ShaderProgram.h>
#include<renderer/DisplayParameters.h>

class BasicShaderProgram : public ShaderProgram {
protected:
    GLint matProjMod;
    GLint height;
    GLint width;
    GLint nodeWidth;
public:
    BasicShaderProgram(std::string shaderPath);

    void setUniforms(GLfloat *mat, const Sizes& dimensions);
};

#endif //BASIC_SHADER_PROGRAM_H
