#ifndef GL_DISPLAY_H
#define GL_DISPLAY_H

#include "Display.h"
#include"Vector.h"
#include "renderer/selection/SelectionBoxRenderer.h"
#include <renderer/ViewStorage.h>
#include "renderer/item/NodeView.h"
#include "renderer/item/EdgeView.h"
#include "tlp/glmatrix.h"
#include "renderer/axis/LabelRender.h"
#include "renderer/axis/AxisHandler.h"
#include "renderer/BasicShaderProgram.h"


/* GL_OES_standard_derivatives */
#ifndef GL_OES_standard_derivatives
#define GL_OES_standard_derivatives 1
#endif

class GlDisplay : public Display {
private:
    const int MAX_PICKING_DISTANCE = 5;

    class TriangleProgram : public BasicShaderProgram {
    public:
        TriangleProgram();

        GLint res;
    };

    class NodeProgram : public ShaderProgram {
    public :
        NodeProgram();

        GLint matProjMod;
        GLint res;
        GLint data;
        GLint distributionData;
        GLint axisHeight;
        GLint axisSpacing;
        GLint nodeWidth;
        GLint gap;   // [0;1] part of height dedicated to represent a gap between non-contiguous values
        GLint colorPicking; // [boolean] color picking rendering
        GLint behind;       // [boolean] the node is in the background (not selected)
        GLint nodeZ;
        GLint anchor;
        GLint weightToInterval; // 0 when nodes' height encode their weight, 1 for their interval
        GLint greyScale;
        GLint distributionAlpha;
    };

    class EdgeProgram : public ShaderProgram {
    public :
        EdgeProgram();

        GLint matProjMod;
        GLint res;
        GLint data;
        GLint height;
        GLint axisSpacing;
        GLint nodeWidth;
        GLint gap;
        GLint edgeCurvature; // [0;1] curvature of the edges
        GLint edgeReduction; // reduction factor for middle part of edges
        GLint colorPicking;
        GLint behind;
        GLint anchor;
        GLint weightToInterval;
        GLint greyScale;
    };

    class FrequencyProgram : public ShaderProgram {
    public :
        FrequencyProgram();

        GLint matProjMod;
        GLint data;
        GLint height;
        GLint axisSpacing;
        GLint nodeWidth;
        GLint gap;
        GLint z;
        GLint anchor;
        GLint weightToInterval;
    };

    class ArrowShader : public BasicShaderProgram {
    private:
        GLint res;
        GLint alpha;
    public:
        ArrowShader(const std::string &shaderPath);

        void setUniforms(const Sizes &dimensions,
                         MatrixManager &matrixManager,
                         bool fade);
    };

    MatrixManager matrixManager;

    GLuint nodesBuffer;
    GLuint edgesBuffer;
    GLuint boxBuffer;
    GLuint triangleBuffer;
    GLuint frequenciesBuffer;
    GLuint lineBuffer;
    GLuint arrowBuffer;

    Vec4f *vertexUniformBuffer = nullptr;
    Vec4f *fragmentUniformBuffer = nullptr;
    int maxVertexUniform;
    int maxFragmentUniform;
    unsigned int maxEdges;
    unsigned int maxNodes;
    static const int RESERVED_UNIFORM = 21;
    NodeProgram nodeShader;
    EdgeProgram edgeShader;
    FrequencyProgram frequencyShader;
    TriangleProgram triangleShader;
    BasicShaderProgram lineShader;
    ArrowShader arrowShader;
    SelectionBoxRenderer rectangleShader;
    LabelRender labelRenderer;
    //GridRenderer gridRenderer;

    static const GLsizei DISTRIBUTION_VIEW_SIZE = (GLsizei) (sizeof(DistribView) /
                                                             sizeof(Vec4f));
    static const GLsizei NODE_VIEW_SIZE = (GLsizei) (sizeof(NodeView) /
                                                     sizeof(Vec4f));
    static const GLsizei EDGE_VIEW_SIZE = (GLsizei) (sizeof(EdgeView) /
                                                     sizeof(Vec4f));

    void setNodesBuffer();

    void setEdgesBuffer(unsigned curveDefinition = 40,
                        unsigned endsDefinition = 20);

    void setMarksBuffer();

    void setFrequenciesBuffer(unsigned definition = 200);

    unsigned int sendNode(const NodeView &nv,
                          const DistribView &dv,
                          NodeView *buffer,
                          DistribView *distribBuffer,
                          unsigned int bufferIndex);

    void glSendNodes(unsigned int nodesBufferSize) const;

    void updateLineBuffer(unsigned int nbAxes);

    void drawLines(const Sizes &dimensions, GlMat4f mat, GLsizei nbAxes);

    void updateArrowBuffer(unsigned long nbAxes,
                           const Inversion &inversions);

    void drawArrows(GLsizei nbAxes,
                    bool fade);

public:
    GlDisplay(DisplayParameters *params);

    ~GlDisplay();

    void drawInit(const Vec4f &backgroundColor, bool colorPicking) override;

    void drawCleanUp() override;

    void drawSelectionBox(Vec2f pointA,
                          Vec2f pointB,
                          float z) override;

    void init() override;

    void drawAxes(bool fade) override;

    void drawNodes(NodeViewStorage &views,
                   float w2i,
                   float distributionAlpha,
                   float zOffset,
                   bool behind,
                   bool colorPicking,
                   bool asThumbnail,
                   Vec2f thumbnailAnchor) override;

    void drawEdges(const ViewStorage &views,
                   float w2i,
                   float zOffset,
                   bool behind,
                   bool colorPicking,
                   bool asThumbnail,
                   Vec2f thumbnailAnchor) override;

    void drawFrequencies(NodeViewStorage &views,
                         float w2i,
                         float zOffset,
                         bool asThumbnail,
                         Vec2f thumbnailAnchor = Vec2f(0.f)) override;

    void drawSliders() override;

    void drawText() override;

    void updateTriangles(std::vector<LowHighPair<TriangleView>> &triangles) override;

    Vec2f windowToModel(Vec2f coordinates) override;

    void clearText() override;

    void addText(const std::string &text, Vec2f pos) override;

    void updateAxes(const Inversion &inversions) override;

    int pickingRoutine(int x, int y, std::function<bool(int)> isValid) override;

    void zoom(float x, float y, const float scale) override;

    void translate(float dx, float dy) override;

    void center(unsigned int nbAxes) override;

    void fit(unsigned int nbAxes) override;

    void changeViewport(const Vec4i x) override;

    float windowToModelFactor() override;
};


#endif //GL_DISPLAY_H
