#ifndef MOCK_DISPLAY_H
#define MOCK_DISPLAY_H

#include "renderer/Display.h"

class MockDisplay : public Display {


public:
    MockDisplay(DisplayParameters *params) : Display(params) {}

    ~MockDisplay() {}

    void init() override {}

    void drawInit(const Vec4f &, bool) override {}

    void drawCleanUp() {}

    void drawSelectionBox(Vec2f,
                          Vec2f,
                          float) override {}

    void drawFrequencies(NodeViewStorage &,
                         float,
                         float,
                         bool,
                         Vec2f) override {}

    void drawSliders() override {}

    void drawText() override {}


    void drawAxes(bool) override {}

    void drawNodes(NodeViewStorage &,
                   float,
                   float,
                   float,
                   bool,
                   bool,
                   bool,
                   Vec2f) override {}

    void drawEdges(const ViewStorage &,
                   float,
                   float,
                   bool,
                   bool,
                   bool,
                   Vec2f) override {}

    void updateTriangles(std::vector<LowHighPair<TriangleView>> &) override {}

    Vec2f windowToModel(Vec2f) { return Vec2f(0.); }

    void clearText() override {}

    void addText(const std::string &, Vec2f) override {}

    int pickingRoutine(int, int, std::function<bool(int)>) { return NONE_ID; }

    void zoom(float, float, const float) {}

    void translate(float, float) {}

    void center(unsigned int) {}

    void fit(unsigned int) {}

    virtual void changeViewport(const Vec4i) {}

    float windowToModelFactor() { return 0.f; }
};

#endif //MOCK_DISPLAY_H

