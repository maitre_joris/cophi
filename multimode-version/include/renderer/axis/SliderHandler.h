#ifndef SLIDER_HANDLER_H
#define SLIDER_HANDLER_H

#include "renderer/axis/SliderHandler.h"
#include "renderer/ShaderProgram.h"
#include "renderer/axis/Slider.h"
#include "renderer/DisplayParameters.h"
#include "renderer/BasicShaderProgram.h"
#include "renderer/Display.h"
#include <vector>


class InvalidSliderPosition {

};


class SliderHandler {
public:
    static const std::pair<float, float> INIT_POS;

    SliderHandler(Display *display);

    ~SliderHandler();

    void updateBuffer();

    bool pickSlider(unsigned int axisId, float modelY, float modelHeight);

    bool isSliderPicked() const;

    void init(unsigned int nbAxes);

    void setInitialPosition(unsigned int axisId, cluster upper, cluster lower);

    const MovingSlider getMovingSlider() const;

    void release();

    bool isEditedSliderPair(unsigned int axisId) const;

    void hookMovingSlider(cluster id, float position);

    void setSliderPositions(unsigned int axisId, float l, float u);

    std::pair<cluster, cluster> getSliderAnchors(unsigned int axisId) const;

    bool isActive() const;

    void clear();


private:
    static const std::pair<float, float> MOVE_BOUNDARIES;
    Display *const display;
    std::vector<LowHighPair<Slider>> sliders;
    MovingSlider slider;
    bool active = false;
};


#endif //SLIDER_HANDLER_H
