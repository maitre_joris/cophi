#ifndef SLIDER_H
#define SLIDER_H

#include <util/Utils.h>
#include "renderer/item/TriangleView.h"

class Slider : public TriangleView {
private:
    cluster anchor;
    bool unchanged = true;
public:
    Slider();

    Slider(cluster anchor, float position);

    bool wasMoved() const;

    void setAnchor(cluster a);

    cluster getAnchor() const;

    void setPosition(float p) override;

};

class MovingSlider {
private:
    bool active = false;
    bool upper;
    unsigned int axisId;
    LowHighPair<Slider> *sliderPair = nullptr;

public:
    MovingSlider();

    MovingSlider(bool upper,
                 unsigned int axisId,
                 LowHighPair<Slider> *pair);

    bool isActive() const;

    void activate();

    void deactivate();

    bool isUpper() const;

    unsigned int getAxisId() const;

    float getPosition() const;

    cluster getAnchor() const;
};

#endif //SLIDER_H
