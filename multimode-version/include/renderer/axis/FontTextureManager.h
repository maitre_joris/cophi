#ifndef FONT_TEXTURE_MANAGER_H
#define FONT_TEXTURE_MANAGER_H

#include <vector>
#include <string>
#include <map>

#include <texture-font.h>
#include <texture-atlas.h>

class FontTextureManager {
public:
    static FontTextureManager *getInstance() {
        if (!singleton)
            singleton = new FontTextureManager();
        return singleton;
    }

    ~FontTextureManager();

    texture_atlas_t *getAtlas(std::string fontName);

    texture_atlas_t *getAtlas(int fontId);

    texture_font_t *getTexture(std::string fontName);

    texture_font_t *getTexture(int fontId);

    int getId(std::string fontName);

    bool distanceMapComputed = false;

    std::string getName(int fontId);

    void convertTextureToDistanceMap(int fontId);

private:
    FontTextureManager();

    int currentIndex;
    std::map<std::string, int> fontMap;
    std::vector<texture_atlas_t *> atlasVector;
    std::vector<texture_font_t *> texturesVector;

    int loadFont(std::string fontName);

    static FontTextureManager *singleton;
};

#endif //FONT_TEXTURE_MANAGER_H
