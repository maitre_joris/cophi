#ifndef LABEL
#define LABEL

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <string>
#include <vector>
#include <tlp/Vector.h>
#include <tlp/Rectangle.h>
#include <tlp/glmatrix.h>
#include <tlp/RectangleNA.h>


class Label {
private:
    std::vector<Vec4f> data;
    GLuint vboID;
    Vec2f modelPos;
    float size;
    float rotation;
    float length;
    unsigned int nbGlyphs;
    unsigned short font;

    /*RectangleNA modelBox;

    RectangleNA getBaseBox();*/

    Rectf getBoundingBox();

    void computeModelBox();

    float computeCorrectiveScale(const Vec2f &screenRes,
                                 const GlMat4f &matProjMod);


public:
    Label(std::string text, std::string font, float angle, Vec2f pos);

    void setText(const std::string &text);

    void setRotation(const float angle);

    float getRotation() const;

    int getFont() const;

    GLuint getVBO() const;

    int getVBOSize() const;

    Vec2f getPosition() const;

    void setHorizontalPosition(float x);

    RectangleNA getViewBox(float windowWidth,
                           float windowHeight,
                           const GlMat4f &matProjMod);

    float getLength() const;

    float getSize() const;

    Vec2f getAnchorOffset() const;
};

#endif // LABEL
