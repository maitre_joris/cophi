#ifndef AXIS_HANDLER_H
#define AXIS_HANDLER_H

#include <renderer/Display.h>
#include <renderer/animation/Inversion.h>
#include <renderer/animation/BasicStagedAnimation.h>


class AxisHandler {
private:
    Animation::AnimationObserver *observer = nullptr;
    Inversion inversions; //Merge flat here
    Display *const display;
    DimensionInterval toInsert;
    AxisInterval toDelete;
    AxisInterval toInvert;
    bool animatingSwap = false;
    BasicStagedAnimation deletions;
    BasicStagedAnimation insertions;
    float shiftAmount;
    int prevAxis = NONE_ID;
    bool noFade = false;
    bool noShifting = false;
    typedef std::function<void(unsigned int left, unsigned int right, float alpha, bool edgesOnly)> SetOpacityFunc;
    typedef std::function<void(unsigned int startingDim, float delta)> ShiftFunc;
    typedef std::function<void()> RebuildFunc;
    typedef std::function<void(AxisInterval axes, std::vector<float> rate)> InvertFunc;
    SetOpacityFunc setOpacity;
    RebuildFunc rebuildViews;
    ShiftFunc shiftViews;
    InvertFunc updateInversionRate;

    bool tickInsertion(float time);

    bool tickDeletion(float time);

    bool tickInversion(float time);

    void bootstrapAnimation();

    void wrapUpAnimation();

public:

    AxisHandler(Display *display);

    void setCallBacks(SetOpacityFunc setOpacity,
                      RebuildFunc rebuildViews,
                      ShiftFunc shiftViews,
                      InvertFunc updateInversionRate);

    void init(Animation::AnimationObserver *o);

    bool isAnimating(int mask) const;

    void prepareInsertion(DimensionInterval toInsert,
                          int justAfter,
                          unsigned int lastAxis);

    void prepareDeletion(AxisInterval axes,
                         unsigned int lastAxis);

    void launchDeletion(float time);

    void launchInsertion(float time);

    void launchInversion(AxisInterval toInvert, float time);

    bool noActivityPending() const;

    void clearOperation();

    int getJustAfterAxis() const;

    DimensionInterval getToInsert() const;

    AxisInterval getToDelete() const;

    bool tick(float time);

    float getInversionRate(unsigned int axisId) const;

    void setSwap(bool s);

};


#endif //AXIS_HANDLER_H

