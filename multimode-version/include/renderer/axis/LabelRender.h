#ifndef _LABELRENDER_H_
#define _LABELRENDER_H_

#define GL_GLEXT_PROTOTYPES

#include "texture-atlas.h"
#include "texture-font.h"
#include "tlp/glmatrix.h"
#include "tlp/Rectangle.h"
#include "tlp/Iterator.h"
#include "renderer/axis/Label.h"
#include "renderer/ShaderProgram.h"
#include "renderer/MatrixManager.h"
#include "renderer/DisplayParameters.h"
#include <GL/gl.h>
#include <string>
#include <vector>
#include <map>

/**
  * @class TextProgram
  * @brief Derived class for handling the text shaders
  */
class TextProgram : public ShaderProgram {
public:
    TextProgram();

    GLuint posCoord;
    GLuint matProjMod;
    GLuint texture;
    GLuint pos;
    GLuint res;
    GLuint size;
    GLuint anchorOffset;
    GLuint activateOutline;
    GLuint outlineColor;
    GLuint glowColor;
    GLuint textColor;
    GLuint rotation;
};


/**
 * @class LabelRender
 * @brief Provides several method to manage labels
 */
class LabelRender {
public:
    /**
    * @brief Constructor
    * Constructs an instance of LabelRender with empty lists
    */
    LabelRender(MatrixManager *manager);

    ~LabelRender();

    /**
    * @fn void draw()
    * @brief Draw all labels
    */
    void draw(float labelSize);

    MatrixManager *getMatrixManager() const;

    void addLabel(const std::string &str, float x, float y);

    void clear();

    void onScaleChange(const DisplayParameters &params);

private:
    static const float MIN_FONT_SIZE;
    static const float MAX_FONT_SIZE;
    static const std::string FONT;
    const unsigned int AXIS_LABEL_MAX_LENGTH = 10;
    const std::string LABEL_ELLIPSIS = "...";
    const float AXIS_LABEL_ROT_ANGLE = static_cast<float>(M_PI) / 4;
    MatrixManager *matrixManager;
    TextProgram *textShaders = nullptr;
    Color outlineColor;
    Color glowColor;
    Color textColor;
    float minTextSize;
    float maxTextSize;
    std::vector<Label *> labels;
    std::vector<Label *> labelsToDraw;
};

#endif
