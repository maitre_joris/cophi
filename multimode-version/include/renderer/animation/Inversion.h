#ifndef INVERSION_H
#define INVERSION_H

#include <renderer/animation/Animation.h>
#include <util/AxisInterval.h>
#include <cfloat>
#include <vector>

class Inversion : public Animation {
private:
    //TODO possible to merge axesToInvert and toBeInverted?
    std::vector<bool> toBeInverted;
    std::vector<float> isInverted;
    std::vector<float> savedStates;

    void endAnimation() override;

public:
    Inversion();

    bool doStep(AxisInterval axes, float animationTime);

    void clear();

    void eraseAxes(AxisInterval axes, bool saveDeletedStates);

    void insertAxes(unsigned int fromAxisId, unsigned int size,
                    bool restoreSave);

    float getState(unsigned int axisId) const;

    std::vector<float> getStates() const;

    void animate(AxisInterval axes, float time);

    void init(unsigned int nbAxes);

    bool isAnimating() const override;

    unsigned int getNbAxes() const;
};


#endif //INVERSION_H
