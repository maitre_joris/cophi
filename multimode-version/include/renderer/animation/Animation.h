#ifndef ANIMATION_H
#define ANIMATION_H

#include<exception>
#include<float.h>
#include "util/DimensionInterval.h"
#include "util/AxisInterval.h"

/**
 * Interface for animation objects, defines common animation duration
 */
class Animation {
private:
    bool animating = false;
    float startingTime = FLT_MAX;
    float duration = .7f;
    float currentStep = 0.f;

    static float easeInOut(float t);

protected:

    virtual void endAnimation();

    virtual void startAnimation(float time, float initStep);

    void initNextStep(float step);

    void resetTime(float time);

    bool doStepEaseIn(float time);

    bool doStepEaseOut(float time);

public:
    class AnimationObserver {
    public:
        virtual void insertAxes(DimensionInterval dbDims,
                                int justAfterAxisId) = 0;

        virtual void deleteDimension(AxisInterval axes) = 0;

        virtual unsigned int getNbAxes() const = 0;

        virtual ~AnimationObserver() {};
    };


    enum Type {
        INVERSION = 0b10000,
        DELETION = 0b01000,
        INSERTION = 0b00100,
        SWAP = 0b00010,
        FOCUS = 0b00001,
        ALL = 0b11111
    };

    Animation();

    virtual ~Animation();

    virtual bool isAnimating() const;

    void setDuration(float dur);

    static float transition(float start, float end, float step);

    virtual float getCurrentStep() const;
};


class InvalidAnimationStage : virtual public std::exception {

};

#endif //ANIMATION_H
