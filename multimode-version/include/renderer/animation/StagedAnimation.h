#ifndef STAGED_ANIMATION_H
#define STAGED_ANIMATION_H

#include "renderer/animation/Animation.h"
#include <vector>


class StagedAnimation : public Animation {
public:
    enum Stage {
        shifting, fadeOut, fadeIn, division, merging, none
    };

private:
    unsigned int currentStage;
    std::vector<std::pair<Stage, bool>> stageSequence;

    float getStepInit(unsigned int stageIndex);


protected:

    StagedAnimation(std::vector<std::pair<Stage, bool>> sequence);

    void moveOnToNextStage(float time);

    bool isEndingStage() const;

    void setStageSequence(std::vector<std::pair<Stage, bool>> sequence);

public:
    Stage getStage() const;

    bool is(Stage stage) const;

    virtual void animate(float time);

    void skipStage(float time);

    void finish();

    virtual bool doStep(float time);


};


#endif //STAGED_ANIMATION_H