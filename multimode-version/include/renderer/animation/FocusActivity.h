#ifndef FOCUS_H
#define FOCUS_H

#include <renderer/animation/StagedAnimation.h>
#include <vector>

class FocusActivity : public StagedAnimation {
private:
    static std::vector<std::pair<Stage, bool>> FOCUS_SEQUENCE;
public:
    FocusActivity();

    void animate(float time) override;

    bool doStep(float time) override;
};

#endif //FOCUS_H
