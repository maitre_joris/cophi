#include "Animation.h"
#include "renderer/DisplayParameters.h"
#ifndef MAPPING_CHANGE_H
#define MAPPING_CHANGE_H

class MappingChange : public Animation {
private:
    bool easeIn = false;

public:
    MappingChange();

    void animate(float time, HeightMapping target);

    bool doStep(float time);

    float getCurrentStep() const override;

    };

#endif //MAPPING_CHANGE_H

