#ifndef FOCUS_ANIMATOR_H
#define FOCUS_ANIMATOR_H

#include <map>
#include "data/Properties.h"
#include "Error.h"
#include "renderer/ViewStorage.h"
#include "data/DataHandler.h"
#include "renderer/DisplayParameters.h"
#include "FocusActivity.h"

typedef std::pair<NodeView, DistribView> FullFocusNodeView;
typedef std::pair<NodeView, std::deque<NodeView>> FullContextNodeView;
typedef std::set<cluster> ClusterSet;
typedef std::map<cluster, FullFocusNodeView> ViewIndex;

class FocusAnimator {
private:
    enum class Type {
        EXPAND,
        COMPRESS
    };

    enum class ContextPosition {
        HIGHER,
        LOWER
    };

    enum class ContractedTo {
        FOCUS,
        CONTEXT
    };

    enum class AtTime {
        INITIAL,
        FINAL
    };

    struct State {
        ContextStack initUpperStack;
        ContextStack initLowerStack;
        unsigned int closings = 0;

        State() {}

        State(ContextStack u, ContextStack l, unsigned int n) : initUpperStack(u), initLowerStack(l), closings(n) {
            assert(closings <= u.size());
        }

        LowHighPair<int> getIdAtLevel(unsigned int l) const {
            // Level 0 corresponds to empty stacks, level 1 to first item, etc
            if (l == 0) return LowHighPair<int>(-1, -1);
            else
                return LowHighPair<int>(getIdAtPos(initLowerStack, l - 1),
                                        getIdAtPos(initUpperStack, l - 1));
        }

        unsigned int getNbLevels() const { return initLowerStack.size(); }

        unsigned int getNbClosings() const { return closings; };

        unsigned int getLevelAfterClosings() const { return (int) getNbLevels() - (int) closings; }


    private:
        int getIdAtPos(const ContextStack &stack, unsigned int l) const {
            assert(stack.size() > l);
            if (stack.at(l).weight == 0) return -1;
            else {
                int id = -1;
                for (unsigned int k = 0; k <= l; k++)
                    if (stack.at(k).weight > 0) id++;
                assert(id >= 0);
                return id;
            }

        }
    };

    struct LightNodeViewBundle {
        std::map<cluster, NodeView> focus;
        std::map<unsigned int, NodeView> upperDetails;
        std::map<unsigned int, NodeView> lowerDetails;

        void forEachInOrder(std::function<void(NodeView &)> f) {
            // From lowest to highest
            for (auto &nv : lowerDetails)
                f(nv.second);
            for (auto &nv : focus)
                f(nv.second);
            for (auto &nv : upperDetails)
                f(nv.second);
        }

        unsigned int size() const {
            return focus.size() + upperDetails.size() + lowerDetails.size();
        }
    };

    struct FullNodeViewBundle {
        ViewIndex focus;
        FullContextNodeView upperContext;
        FullContextNodeView lowerContext;
    };

    const DisplayParameters &parameters;
    const DataHandler &dataHandler;
    Type type;
    cluster contractedFocusId;
    int focusAxisId;
    State hierarchyChange;
    Dimension initialMapping;
    FullNodeViewBundle initialViews;
    FullNodeViewBundle finalViews;
    ClusterSet focusChildren;
    LightNodeViewBundle contractedFocus;
    std::map<cluster, NodeView> upperContractedContext; // uncle nodes
    std::map<cluster, NodeView> lowerContractedContext; // uncle nodes

    const NodeView &getContextShell(ContextPosition position) const;

    const NodeView &getFocusShell() const;

    void fadeVanishingNodes(float alpha, NodeViewStorage &views);

    void setAppearingAlpha(float alpha, NodeViewStorage &views);

    const Dimension getExpandedFocusNodes() const;

    const Dimension getExpandedContextNodes(ContextPosition position) const;

    const Dimension &getCompressedStateNodes() const;

    const Dimension &getExpandedStateNodes() const;

    void computeContractedViews(const FullNodeViewBundle &withContractedFocus,
                                const FullNodeViewBundle &withToBeContracted);

    void normalize(LightNodeViewBundle &views, bool withSpacing);

    void fit(LightNodeViewBundle &views, const NodeView &shell, bool withSpacing);

    void fill(const Dimension &sourceNodes,
              const FullNodeViewBundle &sourceViews,
              LightNodeViewBundle &result);

    void contract(const Dimension &sourceNodes,
                  const ViewIndex &sourceViews,
                  LightNodeViewBundle &result,
                  const NodeView &shell,
                  bool withSpacing);

    void reshapeContext(NodeViewStorage &views,
                        std::pair<cluster, node> n,
                        ContextPosition position,
                        float step);

    void contractToContext(NodeViewStorage &views,
                           ContextPosition position,
                           float step);

    void saveViews(const NodeViewStorage &views,
                   FullNodeViewBundle &target);

    void setInitialState(const NodeViewStorage &views);

    const std::deque<NodeView> getMorphingDetailView(AtTime time, ContextPosition position) const;

    const NodeView &getContractionView(cluster id,
                                       ContractedTo to,
                                       AtTime time,
                                       ContextPosition position = ContextPosition::HIGHER) const;

public:
    FocusAnimator(const DataHandler *, const DisplayParameters *);

    void initFocusExpand(cluster focusId, unsigned int axisId, const NodeViewStorage &views);

    void initFocusCompress(cluster lastOpenedId, unsigned int axisId, unsigned int nbClosings,
                           const NodeViewStorage &views);

    void clear();

    void setFinalState(NodeViewStorage &finalViews);

    void doDivisionStep(float step, NodeViewStorage &mainViews, NodeViewStorage &overlayViews);

    void doExpandStep(float step, NodeViewStorage &mainViews, NodeViewStorage &overlayViews);


};


#endif //FOCUS_ANIMATOR_H
