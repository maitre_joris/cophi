#ifndef BASIC_STAGED_ANIMATION_H
#define BASIC_STAGED_ANIMATION_H

#include "renderer/animation/StagedAnimation.h"

/*
 * Conductor class for fade-in/shifting/fade-out animations
 */


class BasicStagedAnimation : public StagedAnimation {
private:
    static std::vector<std::pair<Stage, bool>> STAGE_SEQUENCE;

protected:
    std::pair<float, float> stepDelta; // Used when shifting axes
    float shiftAmount = 0.f;

    bool doFadeOutStep(float time);

    bool doShiftingStep(float time);

    bool doFadeInStep(float time);

public:
    BasicStagedAnimation();

    using StagedAnimation::animate;
    void animate(float, float);

    float getStepDelta();

    float getShiftAmount() const;

    bool doStep(float time) override;

};


#endif //BASIC_STAGED_ANIMATION_H
