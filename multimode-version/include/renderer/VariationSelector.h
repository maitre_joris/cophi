

#ifndef PARALLEL_COORDINATES_VARIATION_SELECTOR_H
#define PARALLEL_COORDINATES_VARIATION_SELECTOR_H

#include <cassert>

class VariationSelector {
    std::vector<std::map<float, edge>> axisVariations;

public:
    VariationSelector() {

    }

    void clear() {
        axisVariations.clear();
    }

    void init(DataHandler *data) {
        for (unsigned int d = 0; d < data->getNbAxes() - 1; d++) {
            std::map<float, edge> variations; // Variation always expressed from left to right
            for (const edge &e : data->getEdges(d)) {
                const Ends &ends = data->getEnds(e);
                const NodeProp &sp = data->getNodeProperty(ends.source);
                const NodeProp &dp = data->getNodeProperty(ends.dest);

                float meanDest, meanSrc;
                auto destExtrema = data->getAxisExtrema(dp.dimension);
                if (destExtrema.size() == 0) meanDest = 0.5f;
                else meanDest = (dp.min + (dp.max - dp.min) / 2.f) / destExtrema.size();

                auto srcExtrema = data->getAxisExtrema(sp.dimension);
                if (srcExtrema.size() == 0) meanSrc = 0.5f;
                else meanSrc = (sp.min + (sp.max - sp.min) / 2.f) / srcExtrema.size();

                float variation = (meanDest - meanSrc) / 2.f;
                //assert(variation >= -1.001f && variation <= 1.001f); //TODO
                variations.insert({variation, e});
            }

            // Center-normalize by axis in [-1, 1]
//            if (maxVar - minVar > std::numeric_limits<float>::epsilon()) {
//                float a = 2.f / (maxVar - minVar);
//                float b = 1.f - maxVar * a;
//                for (const auto &p: rawVariations) {
//                    float key = (p.second * a + b);
//                    variations.insert({key, p.first});
//
//                }
//            }

            axisVariations.push_back(variations);
        }
    }

    float getVariation(const edge &e) const {
        for (unsigned int i = 0; i < axisVariations.size(); i++) {
            const auto &variations = axisVariations.at(i);
            for (const auto &p : variations) {
                if (p.second == e) return p.first;
            }
        }
        return 0;
    }


    std::vector<edge> filter(unsigned int leftDim, float minVar, float maxVar) {
        std::vector<edge> result;
        if (leftDim >= axisVariations.size()) return result;
        auto &variations = axisVariations.at(leftDim);
        auto start = variations.lower_bound(minVar);
        auto end = variations.upper_bound(maxVar);

        std::transform(start, end, std::back_inserter(result),
                       [](const std::pair<float, edge> &item) { return item.second; });
        return result;
    }

};

#endif //PARALLEL_COORDINATES_VARIATION_SELECTOR_H
