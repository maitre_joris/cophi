#ifndef VISUAL_ITEM_RENDERER_H
#define VISUAL_ITEM_RENDERER_H

class VisualItemRenderer {
protected:
    const float MIN_EDGE_WIDTH = 0.05f;
    const int MAX_PICKING_DISTANCE = 5;
    const float THUMBNAIL_SCALE_FACTOR = 0.5f;

    const float MAX_NODE_Z_OFFSET = 2.f; // over node z
    const float FREQUENCY_Z_OFFSET = MAX_NODE_Z_OFFSET + .5f; // over node z
    const float SELECTION_BOX_UNDERLAY_Z_OFFSET = MAX_NODE_Z_OFFSET + 1.f; // over front edge z
    const float SELECTION_BOX_Z_OFFSET = SELECTION_BOX_UNDERLAY_Z_OFFSET + .5f; // over front edge z
    const float THUMBNAIL_UNDERLAY_Z_OFFSET = SELECTION_BOX_Z_OFFSET + 1.f; // over front edge z
    const float THUMBNAIL_Z_OFFSET = SELECTION_BOX_Z_OFFSET + 2.f; // over front edge z
    const float LABEL_MARGIN = -.1f;
};

#endif //VISUAL_ITEM_RENDERER_H
