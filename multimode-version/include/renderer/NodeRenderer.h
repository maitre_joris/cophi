#ifndef NODE_RENDERER_H
#define NODE_RENDERER_H

#include "renderer/VisualItemRenderer.h"
#include "renderer/ShaderProgram.h"
#include "renderer/item/NodeView.h"
#include "renderer/MatrixManager.h"
#include "renderer/DisplayParameters.h"

class NodeRenderer : public VisualItemRenderer {
private:
    const static float DEFAULT_NODE_Z;

    class NodeProgram : public ShaderProgram {
    public :
        NodeProgram();

        GLint MatProjMod;
        GLint Res;
        GLint Data;
        GLint DData;
        GLint Height;
        GLint AxisSpacing;
        GLint NodeWidth;
        GLint Gap;   // [0;1] part of height dedicated to represent a gap between non-contiguous values
        GLint EnableHistogram;
        GLint ColorPicking; // [boolean] color picking rendering
        GLint Behind;       // [boolean] the node is in the background (not selected)
        GLint NodeZ;
        GLint Anchor;
        GLint WeightToInterval; // 0 when nodes' height encode their weight, 1 for their interval
    };

    NodeProgram nodeShader;
    GLuint nodesBuffer;
    Vec4f *vertexUniformBuffer = nullptr;
    Vec4f *fragmentUniformBuffer = nullptr;
    MatrixManager *matrixManager = nullptr;
    DisplayParameters *parameters = nullptr;

    unsigned int sendNode(const NodeView &nv,
                          const DistribView &dv,
                          NodeView *buffer,
                          DistribView *distribBuffer,
                          unsigned int bufferIndex);

    void glSendNodes(unsigned int nodesBufferSize) const;

    void setNodesBuffer();

public:
    NodeRenderer(MatrixManager *mng, DisplayParameters *p);

    void draw(std::unordered_map<node, std::pair<NodeView, DistribView>> &focusNodes,
              std::unordered_map<node, std::pair<NodeView, std::deque<NodeView>>> &contextNodes,
              GlMat4f mat,
              bool behind = false,
              bool colorPicking = false,
              float nodeZ = DEFAULT_NODE_Z,
              bool asThumbnail = false);


};

#endif //NODE_RENDERER_H
