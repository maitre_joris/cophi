#ifndef SELECTION_RENDERER_H
#define SELECTION_RENDERER_H

#include "util/AxisInterval.h"

class SelectionRenderer {
public:
    virtual AxisInterval passiveSelectionBox(float windowX, float windowY) = 0;

    virtual void hideSelectionBox() = 0;

    virtual AxisInterval getSelectionBoxAxisInterval() = 0;

    virtual void startSelectionBox(float windowX, float windowY) = 0;

    virtual void updateSelectionBox(float windowX, float windowY) = 0;

    virtual AxisInterval endSelectionBox() = 0;

    virtual void triggerAxisThumbnail(AxisInterval axes) = 0;

    virtual void updateAxisThumbnailPosition(float x, float y) = 0;

    virtual bool selectionBoxContains(float windowX, float windowY) = 0;

    virtual void hideAxisThumbnail() = 0;


};


#endif //SELECTION_RENDERER_H
