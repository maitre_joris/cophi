#ifndef DISPLAY_H
#define DISPLAY_H

#include "renderer/DisplayParameters.h"
#include <renderer/ViewStorage.h>
#include <renderer/item/TriangleView.h>
#include "renderer/animation/Inversion.h"
#include "renderer/MatrixManager.h"

class Display {
protected:
    DisplayParameters *params;
    unsigned int nbAxes = 0;
    unsigned int nbTriangles = 0;

    Display(DisplayParameters *params) : params(params) {
    }

public:
    virtual ~Display() {};

    DisplayParameters *getParameters() { return params; }

    virtual void init() =0;

    virtual void drawInit(const Vec4f &backgroundColor, bool colorPicking) =0;

    virtual void drawCleanUp() =0;

    virtual void updateTriangles(std::vector<LowHighPair<TriangleView>> &triangles) {
        this->nbTriangles = triangles.size();
    }

    virtual void drawSliders() = 0;

    virtual void drawAxes(bool fade) = 0;

    virtual void drawSelectionBox(Vec2f pointA,
                                  Vec2f pointB,
                                  float z) =0;


    virtual void drawFrequencies(NodeViewStorage &views,
                                 float w2i,
                                 float zOffset,
                                 bool asThumbnail = false,
                                 Vec2f thumbnailAnchor = Vec2f(0.f)) = 0;

    virtual void drawText() = 0;

    virtual void drawNodes(NodeViewStorage &views,
                           float w2i,
                           float distributionAlpha,
                           float zOffset = 0.f,
                           bool behind = false,
                           bool colorPicking = false,
                           bool asThumbnail = false,
                           Vec2f thumbnailAnchor = Vec2f(0.f)) =0;

    virtual void drawEdges(const ViewStorage &views,
                           float w2i,
                           float zOffset = 0.f,
                           bool behind = false,
                           bool colorPicking = false,
                           bool asThumbnail = false,
                           Vec2f thumbnailAnchor = Vec2f(0.f)) =0;

    virtual Vec2f windowToModel(Vec2f coordinates) = 0;

    virtual void clearText() = 0;

    virtual void addText(const std::string &text, Vec2f pos) = 0;

    virtual void updateAxes(const Inversion &inversions) {
        this->nbAxes = inversions.getNbAxes();
    }

    virtual int pickingRoutine(int x, int y, std::function<bool(int)> isValid) = 0;

    virtual void zoom(float x, float y, const float scale) = 0;

    virtual void translate(float dx, float dy)= 0;

    virtual void center(unsigned int nbAxes)= 0;

    virtual void fit(unsigned int nbAxes)= 0;

    virtual void changeViewport(const Vec4i x) = 0;

    virtual float windowToModelFactor() = 0;
};

#endif //DISPLAY_H
