/*
  Copyright 2015 Timothée Jourde <itim.lcf@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef RENDERER_H
#define RENDERER_H

#include "renderer/DisplayParameters.h"
#include "renderer/Display.h"
#include "renderer/VariationSelector.h"
#include "renderer/axis/AxisHandler.h"
#include "renderer/animation/BasicStagedAnimation.h"
#include "renderer/animation/MappingChange.h"
#include "renderer/animation/Inversion.h"
#include "renderer/animation/FocusActivity.h"
#include "renderer/animation/FocusAnimator.h"
#include "renderer/axis/SliderHandler.h"
#include "renderer/ViewStorage.h"
#include "renderer/ShaderProgram.h"
#include "renderer/BasicShaderProgram.h"
#include "renderer/MatrixManager.h"
#include "renderer/selection/SelectionBox.h"
#include "renderer/selection/SelectionBoxRenderer.h"
#include "util/DimensionInterval.h"
#include "util/Utils.h"
#include "tlp/ColorScale.h"
#include "util/AxisInterval.h"
#include "data/ViewData.h"
#include "data/DataHandler.h"
#include "data/AxisFiltrationTable.h"
#include "tlp/glmatrix.h"
#include <iostream>
#include <cmath>
#include <cfloat>
#include <vector>
#include <map>

const float MIN_EDGE_WIDTH = 0.05f;
const float DEFAULT_NODE_Z = 0.f;
const float MAX_NODE_Z_OFFSET = 1.f;
const float NODE_GAUGE_Z_OFFSET = 2.f;
const float FOCUS_ANIMATION_OFFSET = 3.f;
const float FREQUENCY_Z_OFFSET = MAX_NODE_Z_OFFSET + .1f;
// Over frontZ, corresponding to highest edges
const float SELECTION_BOX_UNDERLAY_Z_OFFSET = MAX_NODE_Z_OFFSET + 1.f;
const float SELECTION_BOX_Z_OFFSET = MAX_NODE_Z_OFFSET + 1.5f;
const float THUMBNAIL_UNDERLAY_Z_OFFSET = SELECTION_BOX_Z_OFFSET + 1.f;
const float THUMBNAIL_Z_OFFSET = THUMBNAIL_UNDERLAY_Z_OFFSET + 1.f;

class Renderer : public DataHandler::DataObserver, public ::DisplayParameters::ParameterObserver {
    static const Vec4f BACKGROUND_COLOR;

private:
    static const GLsizei DISTRIBUTION_VIEW_SIZE = (GLsizei) (
            sizeof(DistribView) /
            sizeof(Vec4f));
    static const GLsizei NODE_VIEW_SIZE = (GLsizei) (sizeof(NodeView) /
                                                     sizeof(Vec4f));
    static const GLsizei EDGE_VIEW_SIZE = (GLsizei) (sizeof(EdgeView) /
                                                     sizeof(Vec4f));

    struct NodeCompression {
        struct WeightInterval {
            float weight = 1.f, interval = 1.f;

            std::string toString() const {
                return "(w:" + std::to_string(weight) + ", i:" + std::to_string(interval) + ")";
            }
        };

        WeightInterval focus;
        WeightInterval context;

        std::string toString() const {
            return "FOCUS " + focus.toString() + "\nCONTEXT " + context.toString();
        }
    };

    // Core element views, stored and updated when consistent changes occur
    ViewStorage coreViews;
    ViewStorage coreHighlightingViews;
    ViewStorage thumbnailViews;
    ViewStorage thumbnailHighlightingViews;
    ViewStorage animationOverlay;

    Display &display;
    DisplayParameters *params = nullptr;
    DataHandler *dataSource = nullptr;
    AxisHandler axisHandler;

    SliderHandler sliderHandler;
    VariationSelector variationSelector;
    Vec2f thumbnailAnchor;
    FramingBox thumbnailUnderlay;
    FramingBox currentSelection;
    AxisFiltrationTable clusterPositions;
    FocusActivity focusActivity;
    FocusAnimator *focusAnimator = nullptr;
    cluster focusNode = ClusterProp::INVALID_ID;
    int focusAxis = -1;
    unsigned int focusClosings = 0;
    MappingChange mappingChanges;
    std::vector<SelectedElement> enhancedElements;
    bool highlighting = false;
    Extrema globalExtrema;

    float getWeightToInterval() const;

    void updateLabels();

    /**
     * @brief Computes a node rectangle coordinates
     * @note Identical to nodeRect() used in shaders
     * @return Vec4f of (p_x, p_y, width, height) where p is the rectangle
     * bottom left corner
     */
    Vec4f getNodeDimensions(const NodeView &n) const;

    Vec4f bounds(const NodeView &nv) const;

    Vec4f bounds(const EdgeView &ev) const;

    NodeView createNodeView(const node &n,
                            const ViewData *dataSource,
                            NodeCompression compression) const;

    NodeView createBaseNodeView(const NodeProp &np,
                                const std::vector<float> &normalizedDistribution,
                                float subWeight = -1) const;

    std::deque<NodeView> createSubViews(const NodeProp &np,
                                        NodeCompression::WeightInterval compression) const;

    void buildHighlightViews(const ViewData *data, const ViewStorage &base, ViewStorage &target);

    EdgeView createEdgeView(const ViewData *d,
                            const edge &e,
                            Vec3f color,
                            ViewStorage &vs) const;

    void updateNodeViewColor(const node &n, bool colorPicking,
                             NodeView &nv,
                             const NodeProp &np) const;

    void updateEdgeViewColor(const ViewData *d,
                             const edge e,
                             EdgeView &ev,
                             float alpha) const;

    int pickId(int x, int y);

    static float getT(unsigned i, unsigned curveDefinition,
                      unsigned endsDefinition);

    void drawSelectionBox();

    void buildNodeViews(NodeViewStorage &views, const ViewData *d, bool);

    void buildEdgeViews(ViewStorage &views, const ViewData *d);

    void buildViews(ViewStorage &views, bool colorPicking = false);

    void glSendNodes(unsigned int nodesBufferSize) const;

    void setAxisAlpha(unsigned axisId,
                      ViewStorage &views,
                      float alpha);

    void setEdgeViewsAlpha(unsigned int leftAxisId,
                           ViewStorage &views,
                           float alpha);

    void setEdgeViewsAlpha(float minAxisId,
                           float maxAxisId,
                           ViewStorage &views,
                           float alpha);

    void updateInversionRate(unsigned dim,
                             float rate,
                             ViewStorage &views);

    void shiftViews(unsigned int startingDim,
                    float delta,
                    ViewStorage &views);

    int getNearestAxisFromModelX(float) const;

    int getLeftAxisFromModelX(float) const;

    int getRightAxisFromModelX(float) const;

    AxisInterval getSelectionBoxAxisInterval();

    void drawThumbnail();

    void updateAxis(unsigned int axisId);

    int pickingRoutine(int x, int y);

    bool tickMappingChange(float time);

    bool tickInversion(float time);

    bool tickDeletion(float time);

    bool tickFocus(float time);

    bool tickInsertion(float time);

    Vec4f propertyToColor(const NodeProp &np) const;

    void updateGlobalExtrema();

    void rebuildCurrentViews();

    std::pair<unsigned int, unsigned int> pickSubContextView(float x, float y, const node &n);

    static Vec3f idToColor(const unsigned id);

    void applyColorPickingColoring(NodeViewStorage &views);

    void applyGradientColoring(NodeViewStorage &views);

    void slidersToNodes();

    void computeNodeYMap();

    void applySliderFiltering();

    void initDivisionPreview(const ViewStorage &views);

    void onScaleChange() override;

    bool isOuterCoordinate(float modelX, float modelY, float margin) const;

    unsigned int sendNode(const NodeView &nv,
                          const DistribView &dv,
                          NodeView *buffer,
                          DistribView *distribBuffer,
                          unsigned int bufferIndex);

    void positionNodeView(NodeViewStorage &views, unsigned int axisId);

    NodeCompression getNodeCompression(unsigned int axisId);


public:
    Renderer(Display &display, DisplayParameters &dp);

    ~Renderer();

    void setFocusOpeningNode(unsigned int axisId, cluster id);

    void setFocusClosingAxis(unsigned int axisId, unsigned int nbClosings);

    void sandbox();

    void hideEnhancedElements();

    void enhanceElement(float x, float y, bool nodeOnly = true);

    void enhanceEdges(unsigned int axis, float min, float max);

    std::vector<edge> getEdges(unsigned int axis, float min, float max);

    void onFocus();

    void onAxisChange() override;

    void onDataLoaded(DataHandler *d, bool center = true) override;

    void onHighlight(const Highlight *sub) override;

    unsigned int getNbClosing(float x, float y, const node &n);

    float getVariation(edge e) const;

    /**
     * Main executing method in Renderer.
     * @param time
     * @return true if an animation is finished, false if one is on-going
     */
    bool tick(float time);

    void draw(bool colorPicking = false);

    void updateAxisThumbnailPosition(float windowX, float windowY);

    SelectedElement getElement(float windowX, float windowY);

    node getNode(float windowX, float windowY);

    edge getEdge(float windowX, float windowY);

    std::pair<int, int> getSurroundingAxes(float windowX, float windowY);

    void setDeletionDuration(float dur);

    void setColorGradient(unsigned int index);

    void setGapRatio(int d);

    void setNodesBuffer();

    void setEdgesBuffer(unsigned curveDefinition = 40,
                        unsigned endsDefinition = 20);

    void setMarksBuffer();

    void setFrequenciesBuffer(unsigned definition = 200);

    bool isAnimating(Animation::Type type) const;

    float getTotalSpacing() const;

    float getTotalNodeWidth() const;

    void histogramView();

    void distortionView();

    void animateMappingChange(float time);

    /**
     * @brief Triggers deletion of given axes
     * @param dim should be a valid AxisInterval
     */
    void setAxesToDelete(AxisInterval axes);

    /**
     * @brief Triggers inversion operation for all axes specifies
     * @param axes should be a valid AxisInterval
     * @param time would be the base time for animation rendering
     */

    void triggerInversion(AxisInterval axes, float time);

    void triggerInsertion(float time);

    void triggerDeletion(float time, bool swap);

    void triggerAxisThumbnail(AxisInterval);

    void triggerFocus(float time);

    /**
     * @brief Triggers insertion of provided dimensions just after
     * futureLeftAxis
     * @param dbDims should a non-empty container of non-materialized dimensions
     * @param futureLeftAxis should be a either valid axis identifier or -1
     * to denote insertion at heading position
     */
    void setDimensionsToInsert(DimensionInterval dbDims, int futureLeftAxis);


    /**
     * Triggers insertion of previously-removed dimension dimDb at
     * (x,y) position
     * @param dimDb should be a valid (database) dimension identifier
     */
    void triggerInsertion(unsigned dimDb, float x, float y);


    bool highlightSubGraph(float windowX, float windowY,
                           bool nodesOnly = false,
                           bool pullUpEdge = false);

    void hideHighlight();

    bool isHighlighting() const;

    int getNearestAxisFromWindow(float windowX, float windowY);

    void hideThumbnail();

    void startSelectionBox(float windowX, float windowY);

    void updateSelectionBox(float windowX, float windowY);

    /**
     * @brief Tighten the current free box selection to its axes and set it to
     * be drawn.
     * @return AxisInterval of all selected axes
     */
    AxisInterval endSelectionBox();

    /**
     * @brief Create a selection box from the given window coordinates if they
     * belong to an axis' tight boundaries.
     * @return AxisInterval either empty or containing an axis corresponding to
     * the provided coordinates
     */
    AxisInterval passiveSelectionBox(float windowX, float windowY);

    void hideSelectionBox();

    /**
     * @brief Determine if the provided window coordinates belongs to any axis' tight
     * boundaries.
     * An axis' tight boundaries are its bounding box enlarged on
     * each sides by the margin parameter.
     * @see Renderer::getSelectionPadding()
     */

    AxisInterval getTightBoundaryAxisFrom(float windowX, float windowY,
                                          float margin);

    /**
     * Determine if the provided window coordinates belongs to the current
     * box selection. A selection box should be active.s
     */
    bool selectionBoxContains(float windowX, float windowY);

    /**
     * @brief Deactivate the rendering of filtering sliders
     */
    void hideSliders();

    /**
     * @brief Activate the rendering of filtering sliders
     */
    void showSliders();

    /**
     * @brief Toggle the moving axes (or sliding axes) mode.
     * This information is solely used for saving axes' orientation (inverted
     * of not) and does not trigger any operation
     */

    bool sliderClick(float windowX, float windowY);

    void onSliderMove(float windowX, float windowY);

    LowHighPair<float> getUpperLowerY(const NodeView &nv) const;

    void onSliderRelease();

    void setGap(const float g);

    void setHeight(const float h);

    void setTotalSpacing(const float x);

    void setTotalNodeWidth(const float x);

    DisplayParameters &getParameters();

    bool isInnerArea(float x, float y);

    void setEdgeCurvature(float c);

    void setContextCompression(float c);

    void setEdgeReduction(float r);

    void onViewsNeedUpdate() override;

    void onColorChange() override;

};

#endif
