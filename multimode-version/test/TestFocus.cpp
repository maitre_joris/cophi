#include <clustering/Canopy.h>
#include <data/ClusteredHierarchicalData.h>
#include "TestFocus.h"

void TestFocus::setUp(void) {
    std::vector<std::vector<float>> values;
    values.resize(D);
    for (unsigned int i = 0; i < D; i++) {
        values.at(i) = generator.make(N);
    }
    TableHandler *th = new TableHandler(values);
    Canopy c = Canopy(K);
    const std::set<unsigned int> noClustering;
    ld = new LocalData(nullptr, th, &c, noClustering, true);
}

void TestFocus::tearDown(void) {
    delete ld;
}

void TestFocus::testFocusIn() {
    for (int i = 0; i < 100; i++) {
        LocalData tmp(*ld);
        tmp.checkCoherence();
        node n = selector.draw(&tmp, (D + i) % D);
        if (tmp.getNodeProperty(n).focusable()) {
            CPPUNIT_ASSERT_ASSERTION_PASS(tmp.applyOpening(n));
        } else {

        }
    }
}

void TestFocus::testFocusOut() {

}