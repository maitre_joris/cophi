#ifndef TEST_UTILS_H_H
#define TEST_UTILS_H_H

#include <vector>
#include <random>
#include <data/DataHandler.h>

class RandomNodeSelector {
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 *gen = new std::mt19937(rd()); //Standard mersenne_twister_engine seeded with rd()
public:
    ~RandomNodeSelector() {
        delete gen;
    }

    node draw(const DataHandler *dataHandler, unsigned int axisId) {
        const Dimension &nodes = dataHandler->getDimension(axisId);
        int min = 0, max = -1;
        for (const auto &p : nodes) {
            if (p.first != ClusterProp::HIGHER_META_NODE_ID &&
                p.first != ClusterProp::LOWER_META_NODE_ID) {
                min = std::min(min, (int) p.first);
                max = std::max(max, (int) p.first);
            }
        }
        assert(min >= 0 && max >= 0);
        std::uniform_int_distribution<> dis(min, max);
        cluster c = dis(*gen);
        assert(nodes.find(c) != nodes.end());
        return nodes.at(c);
    }
};

class ValueGenerator {
    std::random_device rd;
    std::mt19937 *gen = new std::mt19937(rd());
public:
    ~ValueGenerator() {
        delete gen;
    }

    std::vector<float> make(unsigned int n) {
        std::uniform_int_distribution<> dis(0, 1000);
        std::vector<float> result(n);
        for (unsigned int i = 0; i < n; i++) {
            result.at(i) = (float) dis(*gen);
        }
        return result;
    }
};

#endif //TEST_UTILS_H_H
