#include <iostream>

#ifdef _OPENMP

#include <omp.h>

#endif
#ifndef BENCH

#include <cppunit/TestRunner.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/BriefTestProgressListener.h>
#include "TestAxisOperations.h"
#include "TestVisualization.h"

#ifdef LOCAL

#include "TestClustering.h"
#include "TestFileHierarchicalData.h"
#include "TestFocus.h"

CPPUNIT_TEST_SUITE_REGISTRATION(TestClustering);
CPPUNIT_TEST_SUITE_REGISTRATION(TestFileHierarchicalData);
CPPUNIT_TEST_SUITE_REGISTRATION(TestFocus);

#endif

CPPUNIT_TEST_SUITE_REGISTRATION(TestAxisOperations);
CPPUNIT_TEST_SUITE_REGISTRATION(TestVisualization);


int main(int, char **) {
    //--- Create the event manager and test controller
    CPPUNIT_NS::TestResult controller;

    //--- Add a listener that collects test result
    CPPUNIT_NS::TestResultCollector result;
    controller.addListener(&result);

    //--- Add a listener that print dots as test bottomUp.
    CPPUNIT_NS::BriefTestProgressListener progress;
    controller.addListener(&progress);

    //--- Add the top suite to the test runner
    CPPUNIT_NS::TestRunner runner;
    runner.addTest(CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest());
    runner.run(controller);

    return result.wasSuccessful() ? 0 : 1;
}

#else

#include "Bench.h"
#include <iostream>
#include <sstream>
#include <locale>
#include <iomanip>

void runBench(const std::string &inputFile, int linesToRead) {
    Bench bench(inputFile, linesToRead);
    bench.run();
    std::string outputFile = std::string("bench") + nu::integerToString(linesToRead);
    std::ostream os(outputFile, std::ios_base::trunc);
    if (!os.is_open())
        throw std::logic_error(std::string("Unable to open file ").append(outputFile));
    bench.exportResults(&os);
    os.close();
}

std::string getTime() {
    std::time_t t = std::time(nullptr);
    std::stringstream ss;
    ss << std::put_time(std::localtime(&t), "%F_%T");
    return ss.str();
}

void runBenchFromHierarchy(const std::string &dataFile,
                           const std::string &hierarchyFile) {
    int n = nu::readIntegerString(hierarchyFile);

    Bench bench(dataFile, n, hierarchyFile);
    bench.run();
    std::stringstream ss;
    ss << getTime() << "_" << nu::integerToString(n);
    std::ostream *os = new std::ofstream(ss.str(), std::ios_base::trunc);
    if (!((std::ofstream *) os)->is_open())
        throw std::logic_error(std::string("Unable to open file ").append(ss.str()));
    bench.exportResults(os);
    ((std::ofstream *) os)->close();
}


int main(int argc, char **argv) {
    if (argc == 0) std::cerr << "Missing file path argument" << std::endl;
    std::string filePath = std::string(argv[1]);
#ifdef _OPENMP
    std::cout << "Using OpenMP w/" << omp_get_max_threads() << std::endl;
#endif
    for (int i = 2; i < argc; i++) {
        runBenchFromHierarchy(filePath, std::string(argv[i]));
    }
}

#endif