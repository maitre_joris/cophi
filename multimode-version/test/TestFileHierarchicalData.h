#ifndef TEST_FILE_HIERARCHICAL_DATA_H
#define TEST_FILE_HIERARCHICAL_DATA_H

#include "Utils.h"
#include "data/HierarchicalData.h"
#include <cppunit/TestRunner.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/extensions/TestFactoryRegistry.h>

class TestFileHierarchicalData : public CPPUNIT_NS::TestCase {
CPPUNIT_TEST_SUITE(TestFileHierarchicalData);
        CPPUNIT_TEST(testOneDimension);
        CPPUNIT_TEST(testDump);
    CPPUNIT_TEST_SUITE_END();

private:
    const unsigned int N = 50;
    const unsigned int K = 5;
    const unsigned int D = 4;
    ValueGenerator generator;
    RandomNodeSelector selector;
    TableHandler *th = nullptr;
    HierarchicalData *hData;
public:
    void setUp(void) override;

    void tearDown(void) override {
        delete hData;
    }

protected:
    void testDump();

    void testOneDimension();
};


#endif //TEST_FILE_HIERARCHICAL_DATA_H
