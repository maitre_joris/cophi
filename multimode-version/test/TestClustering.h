#ifndef TEST_CLUSTERING_H
#define TEST_CLUSTERING_H

#include <iostream>
#include <cppunit/TestRunner.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include "Utils.h"

class TestClustering : public CPPUNIT_NS::TestCase {
CPPUNIT_TEST_SUITE(TestClustering);
        CPPUNIT_TEST(testBinning);
        CPPUNIT_TEST(testKMeans);
        CPPUNIT_TEST(testCanopy);
        CPPUNIT_TEST(testHierarchicalCanopy);
        CPPUNIT_TEST(testNumbering);
    CPPUNIT_TEST_SUITE_END();
private:
    const unsigned int N = 1000;
    const unsigned int K = 20;
    ValueGenerator generator;
    std::vector<float> dimensionValues;
public:
    void setUp(void) override;

    void tearDown(void) override {}

protected:
    void testHierarchicalCanopy();

    void testNumbering();

    void testCanopy();

    void testBinning();

    void testKMeans();
};

#endif //TEST_CLUSTERING_H
