#ifndef TEST_FOCUS_H
#define TEST_FOCUS_H

#include <cppunit/TestRunner.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <data/TableHandler.h>
#include <data/HierarchicalData.h>
#include "Utils.h"

class TestFocus : public CPPUNIT_NS::TestCase {
CPPUNIT_TEST_SUITE(TestFocus);
        CPPUNIT_TEST(testFocusIn);
        CPPUNIT_TEST(testFocusOut);
    CPPUNIT_TEST_SUITE_END();

private:
    const unsigned int N = 50;
    const unsigned int K = 5;
    const unsigned int D = 4;
    ValueGenerator generator;
    RandomNodeSelector selector;
    LocalData *ld = nullptr;
public:
    void setUp(void) override;

    void tearDown(void) override;

protected:
    void testFocusIn();

    void testFocusOut();
};


#endif //TEST_FOCUS_H
