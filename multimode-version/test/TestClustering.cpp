#include <random>
#include "TestClustering.h"
#include "clustering/Canopy.h"
#include "clustering/Binning.h"
#include "clustering/KMeans.h"
#include "clustering/AdaptiveBinning.h"

void TestClustering::setUp() {
    dimensionValues = generator.make(N);
    assert(dimensionValues.size() == N);
}

void TestClustering::testHierarchicalCanopy() {
    std::vector<HNode> result = Canopy(K / 2).bottomUp(dimensionValues);
    CPPUNIT_ASSERT(result.size() <= K / 2);
    for (const HNode &n : result) {
        CPPUNIT_ASSERT(n.isValid(K / 2, -1));
    }
}

void TestClustering::testCanopy() {
    Canopy cl = Canopy(K);
    std::vector<cluster> result;
    unsigned int nbClusters = cl.run(dimensionValues, result);
    CPPUNIT_ASSERT_ASSERTION_PASS(CPPUNIT_ASSERT(nbClusters <= K));
}

void TestClustering::testNumbering() {

}

void TestClustering::testBinning() {
    Binning cl1 = Binning(K);
    std::vector<cluster> result;
    unsigned int nbClusters = cl1.run(dimensionValues, result);
    CPPUNIT_ASSERT(nbClusters <= K);
    result.clear();
    AdaptiveBinning cl2 = AdaptiveBinning(K);
    nbClusters = cl2.run(dimensionValues, result);
    CPPUNIT_ASSERT(nbClusters <= K);
}

void TestClustering::testKMeans(void) {
    KMeans cl = KMeans(K, 30);
    std::vector<cluster> result;
    unsigned int nbClusters = cl.run(this->dimensionValues, result);
    CPPUNIT_ASSERT_ASSERTION_PASS(CPPUNIT_ASSERT(nbClusters <= K));
}