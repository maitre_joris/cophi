#include "TestVisualization.h"
#include "Utils.h"

void TestVisualization::setUp() {
    std::vector<std::vector<float>> values(D);
    for (unsigned int i = 0; i < D; i++) {
        values.at(i) = generator.make(N);
    }
    viz = new Visualization(values, K);
}

void TestVisualization::tearDown() {
    delete viz;
}

void TestVisualization::testDeletion() {
    CPPUNIT_ASSERT_ASSERTION_PASS(viz->getDeletedDims().isEmpty());
    /*viz->deleteAxis(0);
    viz->onIdle(time(NULL));
    while (viz->isAnimating()) {
        time_t t = time(NULL);
        std::cout << t << std::endl;
        viz->onIdle(t);
    }*/
}

void TestVisualization::testInsertion() {

}