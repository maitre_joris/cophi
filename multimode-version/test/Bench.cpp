#include <iostream>
#include <data/TableHandler.h>
#include <clustering/Canopy.h>
#include <data/FileHierarchicalData.h>
#include <renderer/DisplayParameters.h>
#include "Bench.h"

template std::vector<node> Bench::getBiggestNodes<3>();

void Bench::valueTableInit() {
    timeResults.insert({Type::NODE_SELECTION, std::vector<long>()});
    timeResults.insert({Type::EDGE_SELECTION, std::vector<long>()});
    timeResults.insert({Type::CLOSING, std::vector<long>()});
    timeResults.insert({Type::OPENING, std::vector<long>()});
    sizes.insert({Type::NODE_SELECTION, Sizes()});
    sizes.insert({Type::EDGE_SELECTION, Sizes()});
    sizes.insert({Type::CLOSING, Sizes()});
    sizes.insert({Type::OPENING, Sizes()});
}

Bench::Bench(const std::string &dataFile, int lineToRead) {
    TableHandler *csvHandler = new TableHandler(dataFile, lineToRead);
    Canopy clustering = Canopy(K);
    std::set<unsigned int> noClustering;
    dataHandler = new LocalData(nullptr,
                                csvHandler,
                                &clustering,
                                noClustering,
                                false);
    assert(lineToRead == dataHandler->getNbIndividuals());
    valueTableInit();

}

Bench::Bench(const std::string &dataFile,
             int lineToRead,
             const std::string &hierarchyFile) {
    TableHandler *csvHandler = new TableHandler(dataFile, lineToRead);
    std::ifstream in = std::ifstream(hierarchyFile);
    if (!in.is_open()) throw "Could not find hierarchy file";
    IClusteredData *dbData = new FileHierarchicalData(in, csvHandler, 31);
    dataHandler = new LocalData(nullptr,
                                csvHandler,
                                dbData,
                                true);
    assert(lineToRead == dataHandler->getNbIndividuals());
    in.close();
    valueTableInit();
}


Bench::~Bench() {
    delete dataHandler;
}

template<class T>
void addKey(std::unordered_map<int, T> &map, int key) {
    if (map.find(key) != map.end()) return;
    map.insert({key, T()});
}

void Bench::updateValues(int nbLines, unsigned int nbEdges, long time, Type type) {
    timeResults.at(type).push_back(time);
    auto &s = sizes.at(type);
    if (s.nbLines == 0) s.nbLines = nbLines;
    if (s.nbEdges == 0) s.nbEdges = nbEdges;
}

/*bool Bench::selectOpenCloseSelect(unsigned int dim, Type openOrClose) {
    unsigned int level = dataHandler->getLevel(dim);
    // Selection
    node targetNode = selector.draw(dataHandler, dim);
    int weight = dataHandler->getNodeProperty(targetNode).weight;
    Clock::time_point t0 = Clock::now();
    dataHandler->triggerHighlight(targetNode);
    Clock::time_point t1 = Clock::now();
    long inMs = std::chrono::duration_cast<milliseconds>(t1 - t0).count();
    updateValues(level, weight, inMs, NODE_SELECTION);

    // Opening or close
    if (openOrClose == OPENING) {
        targetNode = selector.draw(dataHandler, dim);
        int shots = 1;
        while (!dataHandler->getNodeProperty(targetNode).focusable() && shots < 10) {
            targetNode = selector.draw(dataHandler, dim);
            shots++;
        }
        if (shots < 10) {
            weight = dataHandler->getNodeProperty(targetNode).weight;
            t0 = Clock::now();
            dataHandler->focusNode(targetNode);
            assert(dataHandler->getLevel(dim) == level + 1);
        } else return false;

    } else if (openOrClose == CLOSING) {
        t0 = Clock::now();
        if (dataHandler->getLevel(dim) > 0) {
            dataHandler->focusOut(dim, 1);
            assert(dataHandler->getLevel(dim) == level - 1);
        } else return false;
    }
    t1 = Clock::now();
    inMs = std::chrono::duration_cast<milliseconds>(t1 - t0).count();
    updateValues(level, weight, inMs, openOrClose);

    // Selection
    level = dataHandler->getLevel(dim);
    targetNode = selector.draw(dataHandler, dim);
    weight = dataHandler->getNodeProperty(targetNode).weight;
    t0 = Clock::now();
    dataHandler->triggerHighlight(targetNode);
    t1 = Clock::now();
    inMs = std::chrono::duration_cast<milliseconds>(t1 - t0).count();
    updateValues(level, weight, inMs, NODE_SELECTION);
    return true;
}*/

/*float Bench::subRun(unsigned int dimensionUplet) {
    unsigned int nbDimensions = dataHandler->getNbAxes();
    int total = 0;
    int failed = 0;
    // Sequential
    for (unsigned int i = 1; i < nbDimensions - dimensionUplet; ++i) {
        std::vector<int> nbOpenings(dimensionUplet, 0);

        for (unsigned int uid = 0; uid < dimensionUplet; uid++) {
            for (int j = 0; j < MAX_DEPTH; j++) {
                total++;
                if (selectOpenCloseSelect(i + uid, Type::OPENING)) nbOpenings.at(uid)++;
                else failed++;
            }
        }

        for (unsigned int uid = 0; uid < dimensionUplet; uid++) {
            for (int j = 0; j < MAX_DEPTH; j++) {
                total++;
                if (!selectOpenCloseSelect(i + uid, Type::CLOSING)) failed++;
            }
        }
    }
    // Alternate
    if (dimensionUplet > 1) {
        for (unsigned int i = 1; i < nbDimensions - dimensionUplet; ++i) {
            std::vector<int> nbOpenings(dimensionUplet, 0);
            for (int j = 0; j < MAX_DEPTH; j++) {
                for (unsigned int uid = 0; uid < dimensionUplet; uid++) {
                    total++;
                    if (selectOpenCloseSelect(i + uid, Type::OPENING)) nbOpenings.at(uid)++;
                    else failed++;
                }
            }

            for (unsigned int j = 0; j < MAX_DEPTH; j++) {
                for (unsigned int uid = 0; uid < dimensionUplet; uid++) {
                    total++;
                    if (!selectOpenCloseSelect(i + uid, Type::CLOSING)) failed++;
                }
            }
        }
    }
    return float(failed) / float(total);
}*/

void reorderAxes(LocalData *dataHandler, std::vector<node> biggest) {
    unsigned int centralDim = dataHandler->getNodeProperty(biggest.at(0)).dbDimension;
    unsigned int leftDim = dataHandler->getNodeProperty(biggest.at(1)).dbDimension;
    unsigned int rightDim = dataHandler->getNodeProperty(biggest.at(2)).dbDimension;
    unsigned int nbAxes = dataHandler->getNbAxes();
    std::cout << "Biggest nodes are on dimensions: " << centralDim << "," << leftDim << "," << rightDim
              << std::endl;
    DimensionInterval newAxesOrder;
    for (unsigned int d = 0; d < nbAxes / 2; d++) {
        if (d != centralDim && d != leftDim && d != rightDim)
            newAxesOrder.chain(d);
    }
    newAxesOrder.chain(leftDim).chain(centralDim).chain(rightDim);
    for (unsigned int d = nbAxes / 2; d < nbAxes; d++) {
        if (d != centralDim && d != leftDim && d != rightDim)
            newAxesOrder.chain(d);
    }
    dataHandler->reorderAxes(newAxesOrder);
    std::cout << "Axes ordering " << dataHandler->getAxesAsDimensions().toCSV() << std::endl;

}

edge Bench::getBiggestEdge() {
    edge result;
    unsigned int max = 0;
    dataHandler->forEachEdge([this, &result, &max](const edge &e) {
        unsigned int weight = dataHandler->getEdgeProperty(e).weight;
        if (weight > max) {
            max = std::max(weight, max);
            result = e;
        }
    });
    return result;
}

void Bench::subRunSelection(node biggestNode, edge biggestEdge) {
    long inMs;
    Clock::time_point t0, t1;
    unsigned int nbLines = dataHandler->getNodeProperty(biggestNode).weight;

    // Full operation for retrieving nbEdges
    dataHandler->triggerHighlight(biggestNode);
    unsigned int nbEdges = dataHandler->getCurrentHighlight()->getNbEdges();
    assert(nbEdges > 0);
    for (unsigned int t = 0; t < N_EXP; t++) {
        dataHandler->clearHighlight();
        t0 = Clock::now();
        dataHandler->benchTriggerHighlight(biggestNode);
        t1 = Clock::now();
        inMs = std::chrono::duration_cast<milliseconds>(t1 - t0).count();
        updateValues(nbLines, nbEdges, inMs, NODE_SELECTION);
    }

    // Full operation for retrieving nbEdges
    dataHandler->triggerHighlight(biggestEdge);
    nbLines = dataHandler->getEdgeProperty(biggestEdge).weight;
    nbEdges = dataHandler->getCurrentHighlight()->getNbEdges();
    assert(nbEdges > 0);
    for (unsigned int t = 0; t < N_EXP; t++) {
        dataHandler->clearHighlight();
        t0 = Clock::now();
        dataHandler->benchTriggerHighlight(biggestEdge);
        t1 = Clock::now();
        inMs = std::chrono::duration_cast<milliseconds>(t1 - t0).count();
        updateValues(nbLines, nbEdges, inMs, EDGE_SELECTION);
    }
    dataHandler->clearHighlight();
}

void Bench::subRunOpening(node biggestNode) {
    long inMs;
    Clock::time_point t0, t1;
    unsigned int d = dataHandler->getNodeProperty(biggestNode).dimension;
    unsigned int nbLines = dataHandler->getNodeProperty(biggestNode).weight;
    // Count edges
    unsigned int nbEdges = 0;

    for (unsigned int t = 0; t < N_EXP; t++) {
        LocalData tmp(*dataHandler);
        t0 = Clock::now();
        tmp.benchFocus(biggestNode);
        t1 = Clock::now();
        inMs = std::chrono::duration_cast<milliseconds>(t1 - t0).count();
        tmp.clear();
        if (t == 0) {
            if (d < tmp.getLastAxisId()) nbEdges += tmp.getEdges(d).size();
            if (d > 0) nbEdges += tmp.getEdges(d - 1).size();
        }
        updateValues(nbLines, nbEdges, inMs, OPENING);
    }
}

std::string dimensionToString(const Dimension &dimension) {
    std::stringstream ss;
    for (auto &p: dimension) {
        ss << p.first << ",";
    }
    return ss.str();
}

void Bench::reachState121(std::vector<node> topBiggestNodes, unsigned int middleAxisId) {
    dataHandler->focusNode(topBiggestNodes.at(0));
    dataHandler->focusNode(topBiggestNodes.at(1));
    dataHandler->focusNode(topBiggestNodes.at(2));
    node random = selector.draw(dataHandler, middleAxisId);
    dataHandler->focusNode(random);
}

void Bench::subRunClosing(std::vector<node> topBiggestNodes) {
    long inMs;
    Clock::time_point t0, t1;
    unsigned int d = dataHandler->getNodeProperty(topBiggestNodes.at(0)).dimension;
    reachState121(topBiggestNodes, d);
    unsigned int nbEdges = 0;
    for (unsigned int t = 0; t < N_EXP; t++) {
        LocalData tmp(*dataHandler);
        t0 = Clock::now();
        tmp.benchFocusOut(d);
        t1 = Clock::now();
        inMs = std::chrono::duration_cast<milliseconds>(t1 - t0).count();
        if (t == 0) {
            if (d < tmp.getLastAxisId()) nbEdges += tmp.getEdges(d).size();
            if (d > 0) nbEdges += tmp.getEdges(d - 1).size();
        }
        tmp.clear();
        updateValues(0, nbEdges, inMs, CLOSING);
    }
}

void Bench::run() {

    /*float failedRate = (subRun(1) + subRun(2) + subRun(3)) / 3.f;
    std::cout << "Failed: " << failedRate * 100.f << "%" << std::endl;*/

    // Selection on biggest node and edge
    edge biggestEdge = getBiggestEdge();
    auto biggestNodes = getBiggestNodes<3>();
    std::cout << "Starting selection bench..." << std::endl;
    subRunSelection(biggestNodes.at(0), biggestEdge);
    dataHandler->clearHighlight();
    // Opening the biggest
    reorderAxes(dataHandler, biggestNodes);
    biggestNodes = getBiggestNodes<3>();
    std::cout << "Starting opening bench..." << std::endl;
    subRunOpening(biggestNodes.at(0));
    // Closing the biggest
    std::cout << "Starting closing bench..." << std::endl;
    subRunClosing(biggestNodes);
}

template<int N>
std::vector<node> Bench::getBiggestNodes() {
    assert(dataHandler != nullptr);
    std::vector<std::pair<unsigned int, node>> selected;
    for (unsigned int d = 0; d < dataHandler->getNbAxes(); d++) {
        //if (notFrom.find(d) == notFrom.end()) {
        unsigned int max = 0;
        node n;
        const auto &nodes = dataHandler->getDimension(d);
        for (const auto &p : nodes) {
            unsigned int w = dataHandler->getNodeProperty(p.second).weight;
            if (w > max) {
                max = w;
                n = p.second;
            }
        }
        selected.push_back({max, n});
        //}
    }
    std::sort(selected.begin(), selected.end());
    std::vector<node> result(N);
    int j = selected.size();
    std::cout << "Biggest nodes: " << std::endl;
    for (int i = 0; i < N; i++) {
        if (selected.size() > i) {
            j--;
            result.at(i) = selected.at(j).second;
            std::cout << selected.at(j).first << ", ";
        }
    }
    std::cout << std::endl;
    return result;
}

void dumpSection(std::ostream *output,
                 const std::string &key,
                 char sep,
                 std::vector<long> &results,
                 const Bench::Sizes &sizes) {

    auto createStats = [](std::vector<long> &values) -> Bench::Stats {
        long min = std::numeric_limits<long>::max(), max = 0l;
        float mean = 0.f;
        for (long value : values) {
            min = std::min(min, value);
            max = std::max(max, value);
            mean += value;
        }
        mean /= values.size();
        float var = 0;
        for (long value : values) {
            float d = (float(value) - mean);
            var += d * d;
        }
        var /= values.size();
        std::vector<float> deciles(9);
        std::sort(values.begin(), values.end());
        for (int i = 0; i < deciles.size(); i++) {
            int pos = (int) rint(float(i + 1) * 1. / 10. * values.size());
            assert(pos < values.size() && pos >= 0);
            deciles.at(i) = values.at(pos);
        }
        return {min, max, mean, sqrt(var), deciles};
    };


    Bench::Stats stats = createStats(results);
    float nbLines = float(sizes.nbLines);
    float nbEdges = float(sizes.nbEdges);
    *output << key << sep << stats.toCSV(sep) << sep << nbLines << sep << nbEdges << std::endl;


}

void Bench::exportResults(std::ostream *output) {
    unsigned int nbValues = 16;
    std::string labels[nbValues] = {"type", "min", "mean",
                                    "max", "stddev", "d1",
                                    "d2", "d3", "d4",
                                    "d5", "d6", "d7",
                                    "d8", "d9", "nbLines",
                                    "nbEdges"};
    for (int i = 0; i < nbValues; i++) *output << labels[i] << (i == nbValues - 1 ? ' ' : SEPARATOR);
    *output << std::endl;

    dumpSection(output, "NSELECT", SEPARATOR, timeResults.at(NODE_SELECTION), sizes.at(NODE_SELECTION));
    dumpSection(output, "ESELECT", SEPARATOR, timeResults.at(EDGE_SELECTION), sizes.at(EDGE_SELECTION));
    dumpSection(output, "OPENING", SEPARATOR, timeResults.at(OPENING), sizes.at(OPENING));
    dumpSection(output, "CLOSING", SEPARATOR, timeResults.at(CLOSING), sizes.at(CLOSING));
}

