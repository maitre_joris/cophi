#include "TestFileHierarchicalData.h"
#include "data/ClusteredHierarchicalData.h"
#include "data/FileHierarchicalData.h"
#include "clustering/Canopy.h"

void TestFileHierarchicalData::setUp() {
    std::vector<std::vector<float>> values;
    values.resize(D);
    for (unsigned int i = 0; i < D; i++) {
        values.at(i) = generator.make(N);
    }
    th = new TableHandler(values);
    Canopy c = Canopy(K);
    const std::set<unsigned int> noClustering;
    hData = new ClusteredHierarchicalData(true, &c, noClustering, values);
}

void TestFileHierarchicalData::testOneDimension() {
    delete th;
    std::vector<std::vector<float>> values(1);
    values.at(0) = {1, 2, 3, 4, 5, 6, 7, 8};
    th = new TableHandler(values);
    std::stringstream ss;
    ss << "0,1,8,0\n" <<
       "0,1,4,0,0\n" <<
       "0,5,8,0,1\n" <<
       "0,1,2,0,0,0\n" <<
       "0,3,4,0,0,1\n" <<
       "0,5,6,0,1,2\n" <<
       "0,7,8,0,1,3\n" << std::endl;
    std::istringstream in = std::istringstream(ss.str());
    FileHierarchicalData(in, th, 2);
    delete th;
}


void TestFileHierarchicalData::testDump() {
    std::string asString = hData->dump();
    std::istringstream in = std::istringstream(asString);
    IClusteredData *cd = new FileHierarchicalData(in, th, K);
    LocalData ld = LocalData(nullptr, th, cd, true);
    for (int i = 0; i < 100; i++) {
        int axisId = D / 2;
        node n = selector.draw(&ld, axisId);
        if (ld.getNodeProperty(n).focusable()) {
            ld.applyOpening(n);
            ld.applyClosing(axisId, 1);
        }
    }

}
