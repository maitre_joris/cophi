#include <random>
#include <Error.h>
#include "TestAxisOperations.h"

#ifdef LOCAL
#include <clustering/HClustering.h>
#include <clustering/Canopy.h>
#else

#include <server/MockServer.h>
#include <data/RemoteData.h>

#endif

void TestAxisOperations::testInsertion() {
    CPPUNIT_ASSERT_ASSERTION_PASS(dataHandler->deleteDimension(AxisInterval(1, 1)));
    CPPUNIT_ASSERT_ASSERTION_PASS(CPPUNIT_ASSERT(dataHandler->getNbAxes() == D - 1));
    CPPUNIT_ASSERT_THROW(dataHandler->insertAxes(DimensionInterval(0), 0), CanceledOperation);
    CPPUNIT_ASSERT_THROW(dataHandler->insertAxes(DimensionInterval(1), -3), CanceledOperation);
    CPPUNIT_ASSERT_NO_THROW(dataHandler->insertAxes(DimensionInterval(1), 0));
    CPPUNIT_ASSERT_ASSERTION_PASS(CPPUNIT_ASSERT(dataHandler->getNbAxes() == D));
    CPPUNIT_ASSERT_ASSERTION_PASS(dataHandler->deleteDimension(AxisInterval(1, 1)));
    CPPUNIT_ASSERT_ASSERTION_PASS(dataHandler->deleteDimension(AxisInterval(1, 1)));
    CPPUNIT_ASSERT_NO_THROW(dataHandler->insertAxes(DimensionInterval(1).chain(2), 0));
}

void TestAxisOperations::testDeletion() {
    dataHandler->deleteDimension(AxisInterval(0, 1));
    CPPUNIT_ASSERT_ASSERTION_PASS(CPPUNIT_ASSERT(dataHandler->getNbAxes() == D - 2));
    unsigned int lastDim = dataHandler->getNbAxes() - 1;
    CPPUNIT_ASSERT_THROW(dataHandler->deleteDimension(AxisInterval(lastDim, lastDim + 1)), CanceledOperation);
    CPPUNIT_ASSERT_ASSERTION_PASS(dataHandler->deleteDimension(AxisInterval(lastDim, lastDim)));
    CPPUNIT_ASSERT_ASSERTION_PASS(CPPUNIT_ASSERT(dataHandler->getNbAxes() == D - 3));
}

void TestAxisOperations::testAfterFocus() {
    CPPUNIT_ASSERT_ASSERTION_PASS(dataHandler->deleteDimension(AxisInterval(3, 3)));
    node random = selector.draw(dataHandler, 2);
    if (dataHandler->getNodeProperty(random).focusable()) {
        CPPUNIT_ASSERT_ASSERTION_PASS(dataHandler->applyOpening(random));
        CPPUNIT_ASSERT_ASSERTION_PASS(dataHandler->insertAxes(DimensionInterval(3), 2));
    }
}


void TestAxisOperations::tearDown() {
    delete dataHandler;
}


void TestAxisOperations::setUp() {
#ifdef LOCAL
    std::vector<std::vector<float>> values(D);
    for (unsigned int i = 0; i < D; i++) {
        values.at(i) = generator.make(N);
    }
    TableHandler * csvHandler = new TableHandler(values);
    Canopy clustering = Canopy(K);
    std::set<unsigned int> noClustering;
    dataHandler = new LocalData(nullptr,
                                csvHandler,
                                &clustering,
                                noClustering,
                                true);
#else
    RequestHandler requester;
    dataHandler = new RemoteData(nullptr, &requester);
#endif

}