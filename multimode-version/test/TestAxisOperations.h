#ifndef TEST_AXIS_OPERATIONS_H
#define TEST_AXIS_OPERATIONS_H

#include <iostream>
#include <cppunit/TestRunner.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <data/LocalData.h>
#include "Utils.h"

class TestAxisOperations : public CPPUNIT_NS::TestCase {
CPPUNIT_TEST_SUITE(TestAxisOperations);
        CPPUNIT_TEST(testInsertion);
        CPPUNIT_TEST(testDeletion);
        CPPUNIT_TEST(testAfterFocus);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp(void) override;

    void tearDown(void) override;

protected:

    void testInsertion();

    void testAfterFocus();

    void testDeletion();

private:
    DataHandler *dataHandler;
    RandomNodeSelector selector;
    ValueGenerator generator;
    const unsigned int D = 10;
    const unsigned int N = 100;
    const int K = 20;
};

#endif //TEST_AXIS_OPERATIONS_H
