#ifndef TEST_VISUALIZATION_H
#define TEST_VISUALIZATION_H

#include <cppunit/TestRunner.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <Visualization.h>
#include "Utils.h"

class TestVisualization : public CPPUNIT_NS::TestCase {
CPPUNIT_TEST_SUITE(TestVisualization);
        CPPUNIT_TEST(testInsertion);
        CPPUNIT_TEST(testDeletion);
    CPPUNIT_TEST_SUITE_END();
public:
    void setUp(void) override;

    void tearDown(void) override;

protected:

    void testInsertion();

    void testDeletion();

private:
    Visualization *viz;
    ValueGenerator generator;
    const unsigned int D = 10;
    const unsigned int N = 100;
    const int K = 20;

};

#endif //TEST_VISUALIZATION_H
