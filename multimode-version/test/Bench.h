#ifndef BENCH_H
#define BENCH_H

#include <data/LocalData.h>
#include <map>
#include <random>
#include "Utils.h"

class Bench {
public:
    struct Sizes {
        unsigned long nbLines = 0;
        unsigned long nbEdges = 0;
    };

    struct Stats {
        long min, max;
        float mean, stddev;
        std::vector<float> deciles; // 9 values

        std::string toCSV(char separator) const {
            std::stringstream ss;
            ss.precision(5);
            ss << min << separator << mean << separator << max << separator << stddev;
            for (unsigned int i = 0; i < deciles.size(); i++)
                ss << separator << deciles[i];
            return ss.str();
        }
    };

    Bench(const std::string &dataFile, int lineToRead);

    Bench(const std::string &dataFile,
          int lineToRead,
          const std::string &hierarchyFile);


    ~Bench();

    void run();

    void exportResults(std::ostream *output);

private:
    const int MAX_DEPTH = 3;
    const int N_EXP = 1000;
    const int K = 31;
    char SEPARATOR = '\t';


    enum Type {
        NODE_SELECTION,
        EDGE_SELECTION,
        OPENING,
        CLOSING
    };

    LocalData *dataHandler;
    RandomNodeSelector selector;
    std::map<Type, std::vector<long>> timeResults;
    std::map<Type, Sizes> sizes;

    void updateValues(int nbLines, unsigned int nbEdges, long time, Type type);

    template<int N>
    std::vector<node> getBiggestNodes();

    edge getBiggestEdge();

    void valueTableInit();

    void subRunSelection(node biggestNode, edge biggestEdge);

    void subRunOpening(node biggestNode);

    void subRunClosing(std::vector<node> topBiggestNodes);

    void reachState121(std::vector<node> topBiggestNodes, unsigned int middleAxisId);


};


#endif //BENCH_H
