set(CMAKE_HOST_SYSTEM "Linux-4.13.0-32-generic")
set(CMAKE_HOST_SYSTEM_NAME "Linux")
set(CMAKE_HOST_SYSTEM_VERSION "4.13.0-32-generic")
set(CMAKE_HOST_SYSTEM_PROCESSOR "x86_64")

include("/home/joris/Applications/emsdk-portable/emscripten/1.37.9/cmake/Modules/Platform/Emscripten.cmake")

set(CMAKE_SYSTEM "Emscripten-1")
set(CMAKE_SYSTEM_NAME "Emscripten")
set(CMAKE_SYSTEM_VERSION "1")
set(CMAKE_SYSTEM_PROCESSOR "x86")

set(CMAKE_CROSSCOMPILING "TRUE")

set(CMAKE_SYSTEM_LOADED 1)
