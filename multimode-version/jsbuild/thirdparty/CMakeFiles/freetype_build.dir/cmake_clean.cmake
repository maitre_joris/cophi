file(REMOVE_RECURSE
  "CMakeFiles/freetype_build"
  "CMakeFiles/freetype_build-complete"
  "freetype-emscripten/src/freetype_build-stamp/freetype_build-install"
  "freetype-emscripten/src/freetype_build-stamp/freetype_build-mkdir"
  "freetype-emscripten/src/freetype_build-stamp/freetype_build-download"
  "freetype-emscripten/src/freetype_build-stamp/freetype_build-update"
  "freetype-emscripten/src/freetype_build-stamp/freetype_build-patch"
  "freetype-emscripten/src/freetype_build-stamp/freetype_build-configure"
  "freetype-emscripten/src/freetype_build-stamp/freetype_build-build"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/freetype_build.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
