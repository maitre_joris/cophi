/*_vertex_*/
#version 100
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
#endif

#ifdef GL_ES
attribute float axisId;
attribute float y;
attribute vec2 position;
#else
in float axisId;
in float y;
in vec2 position;
#endif

uniform vec2 res;  //screen res. used to compute the size of 1 px 4xy
uniform mat4 matProjMod; // modelview * projection matrix
uniform float height;
uniform float width;
uniform float nodeWidth;

const float HEIGHT = 10.;
const float WIDTH  = 6.;

float pixelHeight(vec2 resolution, mat4 projection)
{
    vec4  a = projection * vec4(0.,0., 0.,1.);
    vec4  b = projection * vec4(0.,1., 0.,1.);
    float d = abs(a.y - b.y);
    return (2./resolution.y) / d;
}

//========
void main(void)
{
   float pix = pixelHeight(res,matProjMod);
   float triangleWidth  = min(pix * WIDTH, (width+nodeWidth)/2.);
   float triangleHeight = triangleWidth / (WIDTH/HEIGHT);
   float anchor = (y + 1.) * height/2.;
   gl_Position = matProjMod * vec4(axisId * (width+nodeWidth) + nodeWidth/2. + position.x * triangleWidth,
                                   anchor + position.y * triangleHeight,
                                   -50.,
                                   1.);
}

/*_fragment_*/
#version 100
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
#endif
uniform float alpha;
const vec3 BLACK = vec3(0.,0.,0.);

void main(void) {
     gl_FragColor = vec4(BLACK, alpha);
}
/*_end_*/
