/*_vertex_*/
#version 100
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
#endif

struct nodeView
{
    float weight;
    float gapBefore;
    float weightBefore;
    float x;

    float distortion;
    float subWeight;
    float invert;
    float intervalSize;

    float intervalSizeBefore;
    float biggestBucket;
    float widthReduction;
    float enhancement;

    vec4  color;
};

const int SIZEOF_NODE = 4;
const int RESERVED_UNIFORM = 6;
const int MAX_NODES = (gl_MaxVertexUniformVectors/2 - RESERVED_UNIFORM) / SIZEOF_NODE;
const float BEHIND_Z = -0.8;
const float ENHANCED_Z = 50.;
const float MIN_NODE_WHITENESS = .5;
const float MAX_NODE_Z_OFFSET = 1.;
const float MAX_CONTEXT_WIDTH = .7;

uniform vec4 data[MAX_NODES * SIZEOF_NODE];

uniform mat4 matProjMod;

uniform vec2 res;
uniform float axisHeight;
uniform float axisSpacing;

uniform float nodeWidth;
uniform float gap;
uniform bool colorPicking;
uniform bool behind;

uniform float nodeZ;
uniform float weightToInterval;
uniform vec2 anchor;

uniform float greyScale;

#ifndef GL_ES
in  vec3  vertex;
out vec4  nodeColor;
out vec2  pos;
out vec2  pixelSize;
out float gaugeHeight;
out float index;
out float inverted;
out float enhanced;
#else // GL_ES
attribute vec3 vertex;
varying vec4  nodeColor;
varying vec2  pos;
varying vec2  pixelSize;
varying float gaugeHeight;
varying float index;
varying float inverted;
varying float enhanced;
#endif

float pixelHeight(vec2 resolution, mat4 projection)
{
    vec4  a = projection * vec4(0.,0., 0.,1.);
    vec4  b = projection * vec4(0.,1., 0.,1.);
    float d = abs(a.y - b.y);
    return (2./resolution.y) / d;
}

vec4 nodeRect(float x, float nodeHeight, float heightBefore, float gapBefore, float invert) {
// bottom left corner (x, y), width, height
    float nodesTotalHeight = axisHeight * (1. - gap);
    vec4 r = vec4( x* (nodeWidth + axisSpacing),
                   heightBefore * nodesTotalHeight + gapBefore * (gap*axisHeight),
                   nodeWidth,
                   nodeHeight * nodesTotalHeight
                   );

    float dy = axisHeight - 2.*r.y - r.w ;
    r.y += invert*dy;
    return r;
}

float transition(float start, float end, float step) {
  return end + (1. - step) * (start - end);
}

float getWhiteAmount(bool isContextShell, float weight) {
    if (!isContextShell) return transition(0., min(MIN_NODE_WHITENESS, 1. - weight), weightToInterval);
    else return transition(0., 0.5, weightToInterval);
}

vec4 reductNodeWidth(vec4 nodeRect, float widthReduction) {
    float newWidth = nodeRect.z * widthReduction;
    float newX = nodeRect.x + nodeRect.z/2. - newWidth/2.;
    return vec4(  newX,
                nodeRect.y,
                newWidth,
                nodeRect.w);
}


void main(void) {
    int s = int(floor(vertex.z)) * SIZEOF_NODE;
    index = float(s);
    nodeView n = nodeView (
                data[s+ 0].x,
                data[s+ 0].y,
                data[s+ 0].z,
                data[s+ 0].w,
                data[s+ 1].x,
                data[s+ 1].y,
                data[s+ 1].z,
                data[s+ 1].w,
                data[s+ 2].x,
                data[s+ 2].y,
                data[s+ 2].z,
                data[s+ 2].w,
                data[s+ 3]
            );

    float pix  = pixelHeight(res, matProjMod);
    float nodeHeight = transition(n.weight, n.intervalSize, weightToInterval);
    float heightBefore = transition(n.weightBefore, n.intervalSizeBefore, weightToInterval);

    vec4 r = nodeRect(n.x, nodeHeight, heightBefore, n.gapBefore, n.invert);
    r = reductNodeWidth(r, n.widthReduction);
    float minHeight = pix * 3.;
    float localHeight = max(r.w, minHeight);

    // Positions
    float x = r.x + vertex.x * r.z;
    float y = r.y + vertex.y * localHeight;
    float z = nodeZ;
    // Context detail depth positionin
    z += (1. - n.widthReduction) * 2.;
    vec4 position = vec4(x, y, z, 1.);


    // Translation based on 'anchor' for thumbnail
    position += vec4(anchor, 0., 0.);

    pos = vec2(vertex.x, vertex.y);
    pixelSize      = vec2(r.z/pix, localHeight/pix);

    float alpha = n.color.a;

    if (behind) {
        gaugeHeight = -1.;
        nodeColor = vec4(vec3(1. - greyScale), 1.);
        position.z = BEHIND_Z;
    } else if (n.subWeight >= 0. && n.subWeight <= 1.) {
        // In interval mode, subWeight is color-encoded
        // In ditribubtion/weight mode, subWeight is gauge-encoded
        gaugeHeight = transition(n.subWeight, 1., weightToInterval);
        float whiteAmount = min(.5, 1. - n.subWeight);
        vec3 selectionColor = mix(n.color.rgb, vec3(1.), whiteAmount);
        nodeColor = vec4(mix(n.color.rgb, selectionColor, weightToInterval), 1.);
    } else {
        gaugeHeight = -1.;
        float whiteAmount = getWhiteAmount(n.widthReduction > MAX_CONTEXT_WIDTH, n.weight);
        nodeColor = vec4(mix(n.color.rgb, vec3(1.), whiteAmount),  1.);
    }

    // Override alpha for animation
    nodeColor.a = min(nodeColor.a, alpha); // Override for animation fadding
    // Override color for colorpicking
    if (colorPicking) nodeColor = n.color;

    inverted = n.invert;
    enhanced = n.enhancement;
    if (n.enhancement > 0.) {
        position.z = ENHANCED_Z;
    }
    gl_Position = matProjMod * position;
}


/*_fragment_*/
#version 100
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
#endif

const int NB_DISTR = 10;
const int RESERVED_UNIFORM = 21;
const int SIZEOF_NODE = 4;
const int MAX_FRAGMENT_UNIFORM = (gl_MaxFragmentUniformVectors/2 - RESERVED_UNIFORM);
const float EPS = 1E-2;
const vec4 SELECTION_COLOR = vec4(0.42,0.59, 0.68,1.);
const vec4 ENHANCED_COLOR = vec4(1.,0.59, 0., 1.);
const float HISTOGRAM_MERGING_FACT = .6;
uniform vec4 distributionData[MAX_FRAGMENT_UNIFORM];
uniform bool colorPicking;
uniform bool behind;
uniform float distributionAlpha;
uniform float weightToInterval;

#ifndef GL_ES
in vec4  nodeColor;
in vec2  pos;
in vec2  pixelSize;
in float gaugeHeight;
in float index;
in float inverted;
in float enhanced;
#else
/* Varyings */
varying vec4  nodeColor;
varying vec2  pos;
varying vec2  pixelSize;
varying float gaugeHeight;
varying float index;
varying float inverted;
varying float enhanced;
#endif

struct distribView
{
    float distrib[NB_DISTR];
    float nbElement;
    float padding1;
    vec4 padding2;
};

float easeInOut(float x, float line)
{
    return (atan((x-0.5) * line)
             / (-2. * atan(-0.5*line))
             + 0.5 );
}

/*
float getArrayValue(float array[NB_DISTR], int i) {
    for (int j = 0; j < 10; j++) {
        if (j == i) {
            return distribution[j];
        }
    }
    return 0.;
}
*/

float getDistributionValue(float y, float inversion, float distribution[NB_DISTR], float nbElements) {
   float invertedY = nbElements*(inversion > .9 ? 1.-y : y);
   int binId = int(min(float(int(floor(invertedY))), nbElements-1.));

   float binValue = 0.;
   for (int i = 0; i < NB_DISTR; i++) {
       if (i == binId) {
           binValue = distribution[i];
           break;
       }
   }

   float dy = (invertedY - floor(invertedY))*2. - 1.;
   int neighboorBinId = int(min(nbElements-1., max(float(binId + int(sign(dy))), 0.)));

   // Smoothing
   float otherBinValue = binValue;
   if(neighboorBinId != binId){
       for (int i = 0;  i < NB_DISTR; i++) {
           if (i == neighboorBinId) {
               otherBinValue = distribution[i];
               break;
           }
       }
    }
   return mix(binValue, otherBinValue, easeInOut( abs(dy)/2., 6. ));
}

void main(void) {
    if (colorPicking) {
        gl_FragColor = vec4(nodeColor.rgb,1.);
        return;
    }

    if (behind && enhanced < 1.) {
        gl_FragColor = vec4(nodeColor.rgb,1.);
        return;
    }

   int dataIndex = int(floor(index+0.5));
   distribView distr;

   for (int i=0;i<=MAX_FRAGMENT_UNIFORM;++i) {
       if (i==dataIndex) {
           distr.distrib[0] = distributionData[i].x;
           distr.distrib[1] = distributionData[i].y;
           distr.distrib[2] = distributionData[i].z;
           distr.distrib[3] = distributionData[i].w;

           distr.distrib[4] = distributionData[i+1].x;
           distr.distrib[5] = distributionData[i+1].y;
           distr.distrib[6] = distributionData[i+1].z;
           distr.distrib[7] = distributionData[i+1].w;

           distr.distrib[8] =  distributionData[i+2].x;
           distr.distrib[9] =  distributionData[i+2].y;
           distr.nbElement = distributionData[i+2].z;
           break;
       }
   }

   float distrib = getDistributionValue(pos.y, inverted, distr.distrib, distr.nbElement);

   vec4 boxColor = nodeColor;
   vec4 histogramColor = vec4(1.);
   if (behind) histogramColor =  mix(vec4(1.), vec4(vec3(.9), 1.), weightToInterval);

   // Gradient effect on the distribution
   vec4 distribColor = mix(histogramColor, boxColor, distrib*HISTOGRAM_MERGING_FACT);

   if (inverted > 0.1 && inverted < 0.9) {

   } else if (inverted > 0. && inverted <  0.1) {
       boxColor = (abs((pos.x*2.)-1.) > distrib) ? boxColor : mix(boxColor, distribColor, (0.1-inverted)/(0.1-EPS) );
   } else if (inverted > 0.9 && inverted < 1.)
       boxColor = (abs((pos.x*2.)-1.) > distrib) ? boxColor : mix(boxColor, distribColor, 1.-(1.-inverted) / (1.- 0.9)) ;
   else {
       boxColor = (abs((pos.x*2.)-1.) > distrib) ? boxColor :  vec4(distribColor.rgb, 1.);
   }

   // Adding a thin delimiting border
   vec2 dPix = (vec2(1.,1.)-abs(pos*2. - vec2(1.,1.)))*pixelSize;
   boxColor = mix(boxColor, histogramColor, smoothstep(2.,  0., dPix.x));
   boxColor = mix(boxColor, histogramColor, smoothstep(2.,  0., dPix.y));

   if (gaugeHeight >= 0. && gaugeHeight < 1.) {
      boxColor = (pos.y > gaugeHeight) ? mix(histogramColor, boxColor, 0.5) : boxColor;
   }

    // Override alpha for animations
   boxColor.a = min(nodeColor.a, boxColor.a);

   if (enhanced > 0.) {
        boxColor = mix(boxColor, ENHANCED_COLOR, 0.5);
   }

   if (boxColor.a == 0.) discard;
   gl_FragColor = boxColor;
}
/*_end_*/
