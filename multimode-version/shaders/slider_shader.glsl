/*_vertex_*/
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
#endif

//Attributes
attribute float axisId;
attribute float relativeY;        // in [0., 1.]
attribute vec2 vertex;

//Uniforms
uniform vec2 res;  //screen res. used to compute the size of 1 px 4xy
uniform mat4 matProjMod; // modelview * projection matrix
uniform float height;
uniform float width;
uniform float nodeWidth;

const float SIZE = 10.;

float pixelHeight(vec2 resolution, mat4 projection)
{
    vec4  a = projection * vec4(0.,0., 0.,1.);
    vec4  b = projection * vec4(0.,1., 0.,1.);
    float d = abs(a.y - b.y);
    return (1./resolution.y) / d;
}

void main(void) {
   float pix = pixelHeight(res,matProjMod);
   float triangleSize  = clamp(SIZE*pix, .5*nodeWidth, (width+nodeWidth)/2.);

   float axisX = axisId * (width+nodeWidth) + nodeWidth/2.;
   float baseY = relativeY * height;

   gl_Position = matProjMod * vec4(axisX + vertex.x * triangleSize /2.,
                                   baseY + vertex.y * triangleSize,
                                   3.,
                                   1.);
}


/*_fragment_*/
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
#endif
const vec4 BLACK = vec4(0.,0.,0.,1.);

void main(void) {
     gl_FragColor = BLACK;
}
/*_end_*/
