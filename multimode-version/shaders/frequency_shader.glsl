/*_vertex_*/
#version 410
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
#endif

#define pi 3.141592653589793238462643383279



struct nodeView
{
    float weight;
    float gapBefore;
    float weightBefore;
    float x;

    float distortion;
    float subWeight;
    float invert;
    float intervalSize;

    float intervalSizeBefore;
    float biggestBucket;
    float widthReduction;
    float highlight;

    vec4  color;
};

const int SIZEOF_NODE = 4;
const int RESERVED_UNIFORM = 21;
const int MAX_NODES = (gl_MaxVertexUniformVectors/2 - RESERVED_UNIFORM) / SIZEOF_NODE;


//Attributes
#ifndef GL_ES
in vec2 vertex;
#endif
#ifdef GL_ES
attribute vec2 vertex;
#endif


// Uniform
uniform vec4 data[SIZEOF_NODE * MAX_NODES];
uniform mat4 matProjMod;
uniform float height;
uniform float axisSpacing;
uniform float nodeWidth;
uniform float gap;
uniform float z;
uniform vec2 anchor;
uniform float weightToInterval;


float transition(float start, float end, float step) {
  return end + (1. - step) * (start - end);
}

vec4 nodeRect(nodeView n, float step) // bottom left corner (x, y, width, height)
{
    float nodeHeight = transition(n.weight, n.intervalSize, weightToInterval);
    float heightBefore = transition(n.weightBefore, n.intervalSizeBefore, weightToInterval);
    vec4 r = vec4( n.x * (nodeWidth + axisSpacing),
                   heightBefore * (height - gap*height) + n.gapBefore * (gap*height),
                   nodeWidth,
                   nodeHeight * (height - gap*height)
                   );

    float dy   = height - 2. * r.y - r.w;
    r.y = r.y + n.invert*dy;
    return r;
}

vec4 reductNodeWidth(vec4 r, float widthReduction) {
    float newWidth = r.z * widthReduction;
    float newX = r.x + r.z/2. - newWidth/2.;
    return vec4(newX,
                r.y,
                newWidth,
                r.w);
}

float inv(float x, float line)  // line: ]0 ; +inf[
                                //       ]angle droit ; ligne droite[
{
   float l = 1. + (1. / ( -(1./line) -1. ));
   return line * (-(1./(l*x - 1.)) -1.);
}

float endFreq   (float x) { return 1. - inv(x, 0.1); }
float startFreq (float x) { return endFreq(1.-x);     }
float freqSmooth(float x) { return endFreq(x) * startFreq(x); }

const float minAmp = 0.3;

float freq(float x, float f) {
   return ( sin(2.*x*2.*pi)*f + 1. ) / 2.;
}

void main(void)
{
   int id = int(floor(vertex.y+0.5));
   int s = id * SIZEOF_NODE;

   nodeView n = nodeView (
      data[s+ 0].x,
      data[s+ 0].y,
      data[s+ 0].z,
      data[s+ 0].w,

      data[s+ 1].x,
      data[s+ 1].y,
      data[s+ 1].z,
      data[s+ 1].w,

      data[s+ 2].x,
      data[s+ 2].y,
      data[s+ 2].z,
      data[s+ 2].w,

      data[s+ 3]
      );

   vec4 r = nodeRect(n, weightToInterval);
   r = reductNodeWidth(r, n.widthReduction);
   float distortion = transition(n.distortion, 0., weightToInterval);
   vec4 position = vec4( r.x + freq(vertex.x, minAmp) * r.z + anchor.x,
                         r.y + vertex.x * r.w + anchor.y,
                         z,
                         1.);

   gl_Position = matProjMod * position;
}

/*_fragment_*/
#ifdef GL_ES
precision highp float;
precision highp int;
#endif
const vec4 BLACK = vec4(0.,0.,0.,.8);

void main(void) {
     gl_FragColor = BLACK;
}
/*_end_*/
