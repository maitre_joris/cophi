/*_vertex_*/
#version 100
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
#endif

struct edgeView
{
    float sourcePos;
    float destPos;
    float z;
    float subWeight;

    float sourceWeight;
    float destWeight;
    float weight;
    float isHighlighted;

    float padding2;
    vec3  idColor;
};

struct nodeView
{
    float weight;
    float gapBefore;
    float weightBefore;
    float x;

    float distortion;
    float subWeight;
    float invert;
    float intervalSize;

    float intervalSizeBefore;
    float biggestBucket;
    float widthReduction;
    float enhancement;

    vec4  color;
};

const int SIZEOF_EDGE = 2;
const int SIZEOF_NODE = 4;
const int RESERVED_UNIFORM = 21;
const int MAX_EDGES = (gl_MaxVertexUniformVectors/2 - RESERVED_UNIFORM) / (SIZEOF_EDGE + 2*SIZEOF_NODE);
const vec4 SELECTION_COLOR = vec4(0.42,0.59, 0.68,1.);
const vec4 ENHANCED_COLOR = vec4(1.,0.59, 0., 1.);
const vec4 BEHIND_COLOR = vec4(.8, .8, .8, 1.);
const float BRIGHTEN = -0.25;
const float REDUCTION_DURATION = 0.1;
// Visibility constraint in interval modes
const float MIN_EDGE_ALPHA = .1;
const float MAX_EDGE_ALPHA = 1.;
const float CONNECTING_POS_PRECISION = .8;
const float ENHANCED_Z = 10.;
const float BEHIND_Z = -50.;


// Uniform
uniform vec4 data[MAX_EDGES * (SIZEOF_EDGE + 2*SIZEOF_NODE)];

uniform mat4 matProjMod;

uniform vec2 res;
uniform float height;
uniform float axisSpacing;

uniform float nodeWidth;
uniform float gap;
uniform float edgeCurvature;
uniform float edgeReduction;

uniform bool colorPicking;
uniform bool behind;
uniform vec2 anchor;

uniform float weightToInterval;
uniform float greyScale;

#ifndef GL_ES
in vec3 vertex;
out vec4  color;
#else
attribute vec3 vertex;
varying vec4  color;
#endif




float easeInOut(float t, float line) {
    return ( atan((t-.5) * line) / (-2. * atan(-.5*line)) + .5 );
}

float inv(float x, float line)  // line: ]0 ; +inf[
//       ]angle droit ; ligne droite[
{
    /*float l = 1. - (1. / (1./line + 1. ));
    return line * (-(1./(l*x - 1.)) -1.);*/
    return x*line/(line-x+1.);
}

float animateEdgePos(float t) {
// NOTE: pas très propre
// nodeView.invert est déjà animé dans Render::Tick ...
    if (t >= 0.6)
        return (t-0.6) / 0.4;
    else
        return 0.;
}

float pixelHeight(vec2 resolution, mat4 projection)
{
    vec4  a = projection * vec4(0.,0., 0.,1.);
    vec4  b = projection * vec4(0.,1., 0.,1.);
    float d = abs(a.y - b.y);
    return (2./resolution.y) / d;
}

float transition(float start, float end, float step) {
  return end + (1. - step) * (start - end);
}

vec4 nodeRect(nodeView n, float step) // bottom left corner (x, y, width, height)
{
    float h = transition(n.weight, n.intervalSize, weightToInterval);
    float hb = transition(n.weightBefore, n.intervalSizeBefore, weightToInterval);
    vec4 r = vec4( n.x * (nodeWidth + axisSpacing),
                   hb * (height - gap*height) + n.gapBefore * (gap*height),
                   nodeWidth,
                   h * (height - gap*height)
                   );

    float dy = height - 2.*r.y - r.w ;
    r.y += n.invert*dy;
    return r;
}

vec4 reductNodeWidth(vec4 nodeRect, float widthReduction) {
    float newWidth = nodeRect.z * widthReduction;
    float newX = nodeRect.x + nodeRect.z/2. - newWidth/2.;
    return vec4(  newX,
                nodeRect.y,
                newWidth,
                nodeRect.w);
}

float widthAt(float t, float width) {
    /* Reduction at ends */
    float wf = 0.;
    if (t <= REDUCTION_DURATION)
        wf = 1. - (t/REDUCTION_DURATION);
    else if (t >= 1.-REDUCTION_DURATION)
        wf = (t-(1.-REDUCTION_DURATION)) / REDUCTION_DURATION;

    float reducedWidth = min(edgeReduction, 0.99)*width;
    float widthFactor = 1. + inv(wf, 0.5) * (width/reducedWidth-1.);
    float minWidth = pixelHeight(res, matProjMod) * 3.; // Visibility condition
    return max(widthFactor * reducedWidth, minWidth);
}

float posAtEnd(nodeView n, float weightModeGivenPos) {
    float intervalModePos = clamp(n.biggestBucket, CONNECTING_POS_PRECISION/2., 1.-CONNECTING_POS_PRECISION/2.);
    float pos = transition(weightModeGivenPos, intervalModePos, weightToInterval);
    return pos + (1. - 2.* pos) * animateEdgePos(n.invert);
}

float getSourceX(float reduction, vec4 r) {
    return r.x + nodeWidth * reduction;
}

float getDestinationX(float reduction, vec4 r) {
    return r.x;
}


vec2 bezierPos(vec2 origin, vec2 destination, vec2 control1, vec2 control2, float t) {
        /* Computing Bézier Curve position */
        t = (t-REDUCTION_DURATION) / (1. - 2.*REDUCTION_DURATION);

        vec2 c = 3. * (control1 - origin);
        vec2 b = 3. * (control2 - control1) - c;
        vec2 a = destination - origin - c - b;

        return a*t*t*t + b*t*t + c*t + origin;
}

void main(void)
{
    int id = int(floor(vertex.z));
    int s  = id * (SIZEOF_EDGE + 2*SIZEOF_NODE);

    edgeView e = edgeView (
                data[s+ 0].x,
                data[s+ 0].y,
                data[s+ 0].z,
                data[s+ 0].w,
                data[s+ 1].x,
                data[s+ 1].y,
                data[s+ 1].z,
                data[s+ 1].w,
                data[s+ 2].x,
                data[s+ 2].yzw
            );
    nodeView n1 = nodeView (
                data[s+ 3].x,
                data[s+ 3].y,
                data[s+ 3].z,
                data[s+ 3].w,

                data[s+ 4].x,
                data[s+ 4].y,
                data[s+ 4].z,
                data[s+ 4].w,

                data[s+ 5].x,
                data[s+ 5].y,
                data[s+ 5].z,
                data[s+ 5].w,

                data[s+ 6]
            );
    nodeView n2 = nodeView (
                data[s+ 7].x,
                data[s+ 7].y,
                data[s+ 7].z,
                data[s+ 7].w,

                data[s+ 8].x,
                data[s+ 8].y,
                data[s+ 8].z,
                data[s+ 8].w,

                data[s+ 9].x,
                data[s+ 9].y,
                data[s+ 9].z,
                data[s+ 9].w,

                data[s+ 10]
            );
    float t = vertex.x;

    float intervalSize = transition(n1.intervalSize, n2.intervalSize, t);
    float weight = transition(e.sourceWeight, e.destWeight, t);

    /* Variable width */
    float mainWidth =  transition(weight, intervalSize, weightToInterval) * (height - gap*height);
    float localWidth = widthAt(t, mainWidth);

    /* Positions */
    float sourcePos = posAtEnd(n1, e.sourcePos);
    float destPos = posAtEnd(n2, e.destPos);

    vec4 r1 = reductNodeWidth(nodeRect(n1, weightToInterval), n1.widthReduction);
    vec4 r2 = reductNodeWidth(nodeRect(n2, weightToInterval), n2.widthReduction);
    float sourceX = getSourceX(n1.widthReduction, r1);
    float destX = getDestinationX(n2.widthReduction, r2);

    vec2 position;
    float h;
    if (t <= REDUCTION_DURATION) {
        position = vec2(sourceX + t*axisSpacing, r1.y + sourcePos * r1.w);
    } else if (t >= 1.-REDUCTION_DURATION)
        position = vec2(destX - (1.-t)*axisSpacing, r2.y + destPos * r2.w);
    else {
        /* Bézier Curve parameters */
        vec2 origin      = vec2(sourceX + t*axisSpacing, r1.y + sourcePos * r1.w );
        vec2 destination = vec2(destX - (1.-t)*axisSpacing, r2.y + destPos  * r2.w );

        h = origin.y - destination.y;

        vec2 control1    = vec2(origin.x + (t+edgeCurvature)*axisSpacing,           origin.y);
        vec2 control2    = vec2(destination.x - (1.-t + edgeCurvature)*axisSpacing, destination.y );

        position = bezierPos(origin, destination, control1, control2, t);
    }

    if (e.subWeight > 0. && vertex.y > 0.) {
        float regularLevel = localWidth/2.;
        float portion = max(e.subWeight*localWidth, pixelHeight(res,matProjMod) * 2.) / localWidth;
        float gaugeLevel = regularLevel - (1. - portion)*localWidth;
        position.y += transition(gaugeLevel, regularLevel, weightToInterval);
    } else {
        if (t > REDUCTION_DURATION && t < 1.-REDUCTION_DURATION) {
            position.y += vertex.y * (localWidth/2.);
        } else {
            position.y += vertex.y * (localWidth/2.);

            /* Ensures correct junctions to nodes */
            if (t <= REDUCTION_DURATION) {
                position.y = min(max(position.y, r1.y), r1.y + r1.w);
             } else  {
                position.y = min(max(position.y, r2.y), r2.y + r2.w);
             }

        }
    }

    /* Translation for thumbnail */
    position.x += anchor.x;
    position.y += anchor.y;

    bool enhancement = n1.enhancement > 0. && n2.enhancement > 0.;
    bool behindOfHighlight = e.isHighlighted > 0.;
    bool highlight = e.subWeight > 0.;
    vec4 hPosition = vec4(position, e.z, 1.);

    // Greytone in weightMode and intervalMode
    // Grey colour intensity is weight dependant
    // Former alpha for opacity:
    float weightedGreyTone = 0.6 + ((1.-e.weight) * (1.-e.weight) * 0.3);
    float fadingAlpha = min(n1.color.a, n2.color.a);

    if (behind && !enhancement) {
        color = behindOfHighlight ? mix(SELECTION_COLOR, BEHIND_COLOR, 0.5) : BEHIND_COLOR;
        hPosition.z += BEHIND_Z;
    } else if (colorPicking) {
        color = vec4(e.idColor, 1.);
    } else if (enhancement) {
        vec3 overlayColor = mix(vec3(weightedGreyTone), ENHANCED_COLOR.rgb, 0.5);
        color = vec4(overlayColor.rgb, 1.);
        hPosition.z = ENHANCED_Z;
    } else if (highlight) {
        float whiteAmount = min(.5, 1. - e.subWeight);
        vec3 highlightColor = mix(SELECTION_COLOR.rgb, vec3(1.), whiteAmount);
        color = vec4(mix(SELECTION_COLOR.rgb, highlightColor, weightToInterval), 1.);
    } else {
        color = vec4(vec3(weightedGreyTone), 1.);
    }

    /* Alpha adjustments: fade for animation and bound in interval mode */
    if (!colorPicking && !enhancement && !behind) {
        float boundedAlpha = min(MAX_EDGE_ALPHA, max(MIN_EDGE_ALPHA, color.a));
        color.a = min(fadingAlpha, transition(color.a, boundedAlpha, weightToInterval));
    }

    gl_Position = matProjMod * hPosition;
}


/*_fragment_*/
#version 100
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
varying  vec4  color;
#else
in vec4 color;
#endif

void main(void)
{
    if (color.a == 0.) discard;
    gl_FragColor = color;
    return;
}
/*_end_*/
