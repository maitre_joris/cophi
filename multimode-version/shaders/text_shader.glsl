/*_vertex_*/
#version 410
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
#endif

uniform   mat4 matProjMod;
uniform   vec2 pos;
uniform   float size;
uniform   vec4 res;
uniform   vec2 anchorOffset;
uniform   float rotAngle;
#ifndef GL_ES
in vec4 posXYtexST;
out vec2 texcoord;
#else
attribute vec4 posXYtexST;
varying   vec2 texcoord;
#endif

float pixelSize(vec2 resolution, mat4 projection)
{
    vec4  a = projection * vec4(0.,0., 0.,1.);
    vec4  b = projection * vec4(0.,1., 0.,1.);
    float d = abs(a.y - b.y);
    return (1./resolution.y) / d;
}

void main()
{
    float labelScreenSize = length(matProjMod*vec4(0.0,size,0.0,0.0));
    //size constraints in points
    float minTxtSize = res.z;
    float maxTextSize = res.w;
    //find size in pixels of the constrained label
    float textPtSize = clamp(labelScreenSize*res.y, minTxtSize*1.75, maxTextSize*1.75);
    float scale = (textPtSize/res.y)/labelScreenSize;
    vec2 p = vec2((posXYtexST.xy + anchorOffset)* size);
    p *= scale;
    vec2 tmp = p.xy;
    p.x = cos(rotAngle)*tmp.x - sin(rotAngle)*tmp.y;
    p.y = sin(rotAngle)*tmp.x + cos(rotAngle)*tmp.y;
    p += pos;
    p.y += anchorOffset.y;
    gl_Position = matProjMod * vec4(p, -50., 1.); /// Z should be a parameter
    texcoord    = posXYtexST.zw;
}
/*_fragment_*/
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
#endif

#ifndef GL_ES
in vec2 texcoord;
#else
varying vec2 texcoord;
#endif

uniform sampler2D texture;
uniform vec3 glyph_color;
uniform bool activate_outline;
uniform vec3 outline_color;
uniform vec3 glow_color;

const float glyph_center   = 0.50;
const float outline_center = 0.55;
const float glow_center    = 1.25;

void main()
{
  vec4 c = texture2D(texture, texcoord);
  vec3 rgb;

  float dist  = c.a;
  float width = fwidth(dist);
  float alpha = smoothstep(glyph_center-width, glyph_center+width, dist);

  if(activate_outline)
  {
     /* Glow+outline */
     vec3 rgb = mix(glow_color, glyph_color, alpha);
     float mu = smoothstep(glyph_center, glow_center, sqrt(dist));
     c = vec4(rgb, max(alpha,mu));
     float beta = smoothstep(outline_center-width, outline_center+width, dist);
     rgb = mix(outline_color, c.rgb, beta);
     gl_FragColor = vec4(rgb, max(c.a,beta));
  }
  else
     gl_FragColor = vec4(glyph_color, alpha);
}


