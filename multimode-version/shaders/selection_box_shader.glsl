/*_vertex_*/
#version 330
#ifdef GL_ES
precision highp float;
precision highp int;
attribute vec2 vertex;
varying vec2 fragmentPos;
varying vec2 pixelSize;
#else
in vec2 vertex;
out vec2 fragmentPos;
out vec2 pixelSize;
#endif
uniform mat4 matProjMod; // modelview * projection matrix
uniform vec2 res;
uniform float z;
uniform vec2 lowerLeftAnchor;
uniform float height;
uniform float width;


float pixelHeight(vec2 resolution, mat4 projection) {
    vec4  a = projection * vec4(0.,0., 0.,1.);
    vec4  b = projection * vec4(0.,1., 0.,1.);
    float d = abs(a.y - b.y);
    return (2./resolution.y) / d;
}

float pixelWidth(vec2 resolution, mat4 projection) {
    vec4  a = projection * vec4(0.,0., 0.,1.);
    vec4  b = projection * vec4(1.,0., 0.,1.);
    float d = abs(a.x - b.x);
    float  normWidth = (2./resolution.x);

    return normWidth / d;
}

vec2 pixelInModel(vec2 resolution, mat4 projection) {
    vec4  a = projection * vec4(0.,0., 0.,1.);
    vec4  b = projection * vec4(1.,0., 0.,1.);
    vec4  c = projection * vec4(0.,1., 0.,1.);
    float dx = abs(a.x - b.x);
    float dy = abs(a.y - c.y);
    return vec2((2./ resolution.x) / dx, (2./resolution.y) / dy);
}


void main(void) {
    vec2 scaled = vec2(width*vertex.x,height*vertex.y);
    vec2 translated = scaled.xy + lowerLeftAnchor.xy;
    pixelSize = vec2(pixelWidth(res, matProjMod), pixelHeight(res, matProjMod));//pixelInModel(res, matProjMod);
    fragmentPos = scaled.xy - vec2(width/2., height/2.);
    gl_Position = matProjMod * vec4(translated.xy, z, 1.);
}

/*_fragment_*/
#version 330
#ifdef GL_ES
precision highp float;
precision highp int;
varying vec2 fragmentPos;
varying vec2 pixelSize;
#else
in vec2 fragmentPos;
in vec2 pixelSize;
#endif
uniform float height;
uniform float width;

const vec4 WHITE = vec4(1., 1., 1., 0.4);
const float BORDER = 3.;
const float DASH_LENGTH = 6.;


void main() {
    vec2 maxDist = vec2(width/2., height/2.);
    vec2 dashLength = DASH_LENGTH*pixelSize;

    bool onBorder = any(greaterThan(abs(fragmentPos), maxDist - BORDER*pixelSize));
    bool onXDashSpace = abs(fragmentPos.x) < maxDist.x - BORDER*pixelSize.x;
    bool onYDashSpace = abs(fragmentPos.y) < maxDist.y - BORDER*pixelSize.y;

    bool onXDashVoid = mod(abs(fragmentPos.x)/dashLength.x, 2.) >= 1.;
    bool onYDashVoid = mod(abs(fragmentPos.y)/dashLength.y, 2.) >= 1.;


    if (onBorder) {
        if ((onXDashSpace && onXDashVoid) || (onYDashSpace && onYDashVoid))
            discard;
        else
            gl_FragColor = vec4(0.,0.,0.,1.);
    } else {
        gl_FragColor = WHITE;
    }
}
/*_end_*/
