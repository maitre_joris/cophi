/*_vertex_*/
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
#endif

// Uniform
uniform mat4 matProjMod; // modelview * projection matrix
uniform float height;
uniform float width;
uniform float nodeWidth;

//const float AXIS_MARGIN = .6;

//Attributes
#ifndef GL_ES
in vec2 vertex;
in float z;
#else
attribute vec2 vertex;
attribute float z;
#endif


void main(void)
{
   float y = (vertex.y + 1.) * height/2.;
   gl_Position = matProjMod * vec4(vertex.x * (width+nodeWidth) + nodeWidth/2.,
                                   y,
                                   z,
                                   1.);
}

/*_fragment_*/
#ifdef GL_ES
#extension GL_OES_standard_derivatives : enable
precision highp float;
precision highp int;
#else

#endif

void main(void) {
     gl_FragColor = vec4(0.,0.,0.,.5);
}
/*_end_*/
