
#include <iostream>
#include "Notifier.h"

#ifdef EMSCRIPTEN
#include "clustering/IClustering.h"
#include "Visualization.h"
#include <emscripten.h>
#include "emscripten/bind.h"
#include "emscripten/val.h"
#endif

unsigned int Notifier::MAX_MODE_TO_BE_NOTIFIED = 110;
unsigned int Notifier::MIN_MODE_TO_BE_NOTIFIED = 3;

void Notifier::onHighlight(bool isOn) {
#ifdef EMSCRIPTEN
    unsigned int event = (isOn ? Events::HIGHLIGHT_ON : Events::HIGHLIGHT_OFF);
    EM_ASM_ARGS({eventCallback($0);}, event);
#else
#endif
}

void Notifier::onEvent(unsigned int event) {
#ifdef EMSCRIPTEN
    if (event <= MAX_MODE_TO_BE_NOTIFIED || event >= MIN_MODE_TO_BE_NOTIFIED)
      EM_ASM_ARGS({eventCallback($0);}, event);
#else
    //std::cout << "[Notifier] Event " << event << std::endl;
#endif
}

void Notifier::onVariationFilterChange(float center, float range) {
#ifdef EMSCRIPTEN
    EM_ASM_ARGS({onWheelParameterChange($0, $1);}, center, range);
#endif
}

void Notifier::onAxisChange() {
#ifdef EMSCRIPTEN
    EM_ASM_ARGS({eventCallback($0);}, Events::AXIS_CHANGE);
#else
    //std::cout << "[Notifier] Axis operation" << std::endl;
#endif
}

void Notifier::onError(ImportError::Type code) {
#ifdef EMSCRIPTEN
    EM_ASM_ARGS({errorCallback($0);}, code);
#endif
}

void Notifier::info(std::string s) {
#ifdef EMSCRIPTEN

#else
    std::cout << s << std::endl;
#endif
}

void Notifier::error(std::string s) {
#ifdef EMSCRIPTEN

#else
    std::cout << s << std::endl;
#endif

}

#ifdef EMSCRIPTEN
using namespace emscripten;
EMSCRIPTEN_BINDINGS(Visualization) {
  enum_<Visualization::State>("State")
      .value("DEFAULT", Visualization::DEFAULT)
      .value("INVERT_AXIS", Visualization::INVERT_AXIS)
      .value("MOVE_AXIS", Visualization::MOVE_AXIS)
      .value("DELETE_AXIS", Visualization::DELETE_AXIS)
      .value("INSERT_DIMENSION", Visualization::INSERT_DIMENSION)
      .value("SCALE", Visualization::SCALE)
      .value("FILTER", Visualization::FILTER)
      .value("PICKER", Visualization::PICKER);
  enum_<ImportError::Type>("Error")
      .value("MEMORY", ImportError::Type::MEMORY)
      .value("CLUSTERING", ImportError::Type::CLUSTERING)
      .value("DELIMITER", ImportError::Type::DELIMITER)
      .value("NO_FILE", ImportError::Type::NO_FILE)
      .value("NONSQUARE_TABLE", ImportError::Type::NONSQUARE_TABLE)
      .value("INVALID_VALUE", ImportError::Type::INVALID_VALUE)
      .value("UNEXPECTED", ImportError::Type::UNEXPECTED)
      .value("SERVER", ImportError::Type::SERVER);
  enum_<IClustering::Type>("Clustering")
      .value("KMEANS", IClustering::KMEANS)
      .value("NONE", IClustering::NONE)
      .value("BINNING", IClustering::BINNING)
      .value("CANOPY", IClustering::CANOPY)
      .value("ADAPTIVE_BINNING", IClustering::ADAPTIVE_BINNING);
  enum_<ColorScheme::Type>("ColorScheme")
       .value("GREEN_BLACK_RED", ColorScheme::GREEN_BLACK_RED)
       .value("BABY_ROOM", ColorScheme::BABY_ROOM)
       .value("PALAMPA", ColorScheme::PALAMPA)
       .value("VIRIDIS", ColorScheme::VIRIDIS)
       .value("LEGACY", ColorScheme::LEGACY);
};
#endif // EMSCRIPTEN