#include <iostream>
#include "data/Properties.h"

const cluster ClusterProp::INVALID_ID = -2;
const cluster ClusterProp::LOWER_META_NODE_ID = -1;
const cluster ClusterProp::HIGHER_META_NODE_ID = std::numeric_limits<int>::max();
const unsigned int ClusterProp::DISTRIBUTION_BUCKET_NB = 10;


bool ClusterProp::operator<(const ClusterProp &o) const {
    /*if (std::abs(min - o.min) < std::numeric_limits<float>::epsilon())
        return max < o.max;
    else {
        return min < o.min;
    }*/
    return clusterId < o.clusterId;
}

ClusterProp::ClusterProp(cluster id) : clusterId(id),
                                       min(std::numeric_limits<float>::infinity()),
                                       max(-std::numeric_limits<float>::infinity()) {}

ClusterProp::ClusterProp(cluster id, float value, unsigned long weight) : clusterId(id),
                                                                          weight(weight),
                                                                          min(value), max(value) {}

ClusterProp::ClusterProp(const ClusterProp &o) : clusterId(o.clusterId),
                                                 weight(o.weight),
                                                 min(o.min),
                                                 max(o.max),
                                                 distribution(o.distribution) {
}

ClusterProp::ClusterProp(const std::set<ClusterProp> toBeMerged, cluster id) : clusterId(id) {
    min = std::numeric_limits<float>::infinity();
    max = -min;
    for (const ClusterProp &cp : toBeMerged) {
        weight += cp.weight;
        min = std::min(cp.min, min);
        max = std::max(cp.max, max);
    }
#ifndef NO_DISTRIBUTIONS
    if (!isMeta()) initDistribution(toBeMerged);
#endif
    //std::cout << "Created aggregate: " << toString() << std::endl;
}


float ClusterProp::getMin() const {
    return min;
}

float ClusterProp::getMax() const {
    return max;
}

bool ClusterProp::inBound(float value) const {
    assert(std::isfinite(min) && std::isfinite(max));
    return value >= min && value <= max;
}

unsigned long ClusterProp::getWeight() const {
    return weight;
}


bool ClusterProp::isHigherContext() const {
    return clusterId == HIGHER_META_NODE_ID;
}

bool ClusterProp::isLowerContext() const {
    return clusterId == LOWER_META_NODE_ID;
}

void ClusterProp::merge(const ClusterProp &o) {
    min = std::min(min, o.min);
    max = std::max(max, o.max);
    weight += o.weight;
}

void ClusterProp::initDistribution(const std::set<ClusterProp> &toBeMerged) {
    distribution.clear();
    float intervalSize = (max - min);
    if (intervalSize >= std::numeric_limits<float>::epsilon()) {
        distribution = Distribution(DISTRIBUTION_BUCKET_NB, 0);
    } else {
        if (toBeMerged.size() == 1) distribution = toBeMerged.begin()->distribution;
        else std::cout << "Invalid cluster?" << std::endl;
    }
    // For each child bucket, use bucket interval's central point
    for (const ClusterProp &child : toBeMerged) {
        float childInterval = child.max - child.min;
        if (childInterval <= std::numeric_limits<float>::epsilon()) {
            float distanceToParentMin = child.min - min;
            int parentBucket = -1;
            if (distanceToParentMin <= std::numeric_limits<float>::epsilon()) parentBucket = 0;
            else parentBucket = (int) rint(distanceToParentMin / intervalSize * (DISTRIBUTION_BUCKET_NB - 1));
            assert(parentBucket >= 0);
            distribution.at(parentBucket) += child.distribution.at(0);
        } else {
            for (unsigned int i = 0; i < child.distribution.size(); i++) {
                float midBucketPoint = child.min + i * childInterval / DISTRIBUTION_BUCKET_NB;
                int parentBucket = (int) rint((midBucketPoint - min) / intervalSize * (DISTRIBUTION_BUCKET_NB - 1));
                if (parentBucket >= 0 && parentBucket < (int) DISTRIBUTION_BUCKET_NB) {
                    distribution.at(parentBucket) += child.distribution.at(i);
                } else {
                    std::cerr << "Error" << std::endl;
                }
            }
        }
    }
}

std::string ClusterProp::toString() const {
    std::stringstream ss;
    ss << "#" << clusterId << "[" << min << ";" << max << "], Weight: " << weight;
    return ss.str();
}

cluster ClusterProp::getId() const {
    return clusterId;
}

unsigned long ClusterProp::getBiggestBucket() const {
    if (distribution.empty()) return 0;
    else return *std::max(distribution.begin(), distribution.end());
}

bool ClusterProp::isMeta() const {
    return clusterId == LOWER_META_NODE_ID || clusterId == HIGHER_META_NODE_ID;
}

bool ClusterProp::focusable() const {
    return (max - min) >= std::numeric_limits<float>::epsilon(); // Depends on clustering algorithm?
}

NodeProp::NodeProp() : ClusterProp(INVALID_ID) {}

NodeProp::NodeProp(const ClusterProp &o, unsigned int axis, unsigned int dbDim) : ClusterProp(o),
                                                                                  dimension(axis),
                                                                                  dbDimension(dbDim) {
}

bool CompEdge::operator()(const std::pair<edge, float> &a,
                          const std::pair<edge, float> &b) const {
    return a.second < b.second;
}


bool CompNode::operator()(const node &a, const node &b) const {
    return prop[a] < prop[b];
}

CompNode::CompNode(const NodeProperty<NodeProp> &np) : prop(np) {}

