#include <iostream>
#include "data/ClusterIdentifier.h"

ClusterIdentifier::ClusterIdentifier() {

}

ClusterIdentifier::ClusterIdentifier(cluster id) {
    assert(id != ClusterProp::INVALID_ID);
    topDownChain.push_back(id);
}

ClusterIdentifier::ClusterIdentifier(const ClusterIdentifier &o, cluster id) : ClusterIdentifier(o) {
    assert(id != ClusterProp::INVALID_ID);
    topDownChain.push_back(id);
}

cluster ClusterIdentifier::getLeafId() const {
    assert(!topDownChain.empty());
    return topDownChain.back();
}

void ClusterIdentifier::chain(cluster focused) {
    topDownChain.push_back(focused);
}

bool ClusterIdentifier::isRoot() const {
    return topDownChain.empty();
}

std::string ClusterIdentifier::toString() const {
    std::string result = "";
    for (cluster id: topDownChain) {
        result += std::to_string(id) + ",";
    }
    result += "";
    return result;
}


std::vector<cluster>::const_iterator ClusterIdentifier::begin() const {
    return (topDownChain.begin());
};

std::vector<cluster>::const_iterator ClusterIdentifier::end() const {
    return (topDownChain.end());
};

size_t ClusterIdentifier::size() const { return topDownChain.size(); }

ClusterIdentifier ClusterIdentifier::getParent(const ClusterIdentifier &c) {
    assert(!c.isRoot());
    ClusterIdentifier result = ClusterIdentifier(c);
    result.topDownChain.pop_back();
    return result;
}

ClusterIdentifier ClusterIdentifier::parent() {
    ClusterIdentifier result = ClusterIdentifier(*this);
    result.topDownChain.pop_back();
    return result;
}

ClusterIdentifier ClusterIdentifier::drilled(cluster id) {
    ClusterIdentifier result = ClusterIdentifier(*this);
    result.topDownChain.push_back(id);
    return result;
}

unsigned int ClusterIdentifier::getNbLevels() const {
    return topDownChain.size();
}