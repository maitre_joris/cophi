#include "server/HighlightBase.h"

HighlightBase::HighlightBase() : isEmpty(true) {}

bool HighlightBase::isValid() const {
    return !isEmpty;
}

void HighlightBase::clear() {
    isEmpty = true;
}

HighlightBase::HighlightBase(unsigned int dimension, cluster id) :
        isEmpty(false), type(CLUSTER) {
    firstOrigin = ClusterPosition(dimension, id);
}

HighlightBase::HighlightBase(unsigned int firstDimension, cluster firstId,
                             unsigned int secondDimension, cluster secondId) :
        isEmpty(false), type(EDGE) {
    firstOrigin = ClusterPosition(firstDimension, firstId);
    secondOrigin = ClusterPosition(secondDimension, secondId);
}

HighlightBase::HighlightBase(const std::vector<ClusterInterval> &intervals) :
        isEmpty(false), type(INTERVAL), intervals(intervals) {
}


bool HighlightBase::isClusterBase() const {
    assert(!isEmpty);
    return type == CLUSTER;
}

bool HighlightBase::isIntervalBase() const {
    assert(!isEmpty);
    return type == INTERVAL;
}

bool HighlightBase::isEdgeBase() const {
    assert(!isEmpty);
    return type == EDGE;
}

ClusterPosition HighlightBase::getClusterPosition() const {
    assert(type == CLUSTER);
    return firstOrigin;
}


std::vector<ClusterInterval> HighlightBase::getIntervals() const {
    assert(type == INTERVAL);
    return intervals;
}

std::pair<ClusterPosition, ClusterPosition> HighlightBase::getEdgePosition() const {
    assert(type == EDGE);
    return std::make_pair(firstOrigin, secondOrigin);
};