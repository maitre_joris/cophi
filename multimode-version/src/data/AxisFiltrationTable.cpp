#include <iostream>
#include <Error.h>
#include "data/AxisFiltrationTable.h"

using namespace std;

void AxisFiltrationTable::insertCluster(cluster id,
                                        unsigned int axisId,
                                        LowHighPair<float> verticalScope) {
    /*assert(verticalScope.first <= 1.1f && verticalScope.first > -0.1f);
    assert(verticalScope.second >= 0.f && verticalScope.second < 1.1f);*/

    assert(verticalScope.getHigh() >= verticalScope.getLow());
    // Cap values
    verticalScope.getHigh() = min(verticalScope.getHigh(), 1.1f);
    verticalScope.getLow() = max(verticalScope.getLow(), -.1f);
    table.at(axisId).insert(std::make_pair(verticalScope, id));
}


void AxisFiltrationTable::init(unsigned int nbAxes) {
    table.resize(nbAxes);
}

AxisFiltrationTable::Hook AxisFiltrationTable::getUpperMost(unsigned int axisId,
                                                            float upLimit) const {
    auto it = table.at(axisId).rbegin();
    auto end = table.at(axisId).rend();
    while (it->first.getHigh() > upLimit && it != end) {
        it++;
    }
    if (it == end) throw ValueNotFound();
    else return std::make_pair((*it).second, (*it).first.getHigh());
}

AxisFiltrationTable::Hook AxisFiltrationTable::getLowest(unsigned int axisId,
                                                         float downLimit) const {
    auto it = table.at(axisId).begin();
    auto end = table.at(axisId).end();
    while (it->first.getLow() < downLimit && it != end) {
        it++;
    }
    if (it == end) throw ValueNotFound();
    else return std::make_pair((*it).second, (*it).first.getLow());
}

std::set<cluster> AxisFiltrationTable::getInBetweenClusters(unsigned int axisId,
                                                            cluster up,
                                                            cluster down) const {
    std::set<cluster> result;
    // Find up and down iterator
    auto it = table.at(axisId).cbegin();
    const auto end = table.at(axisId).cend();
    auto upIt = end;
    auto downIt = end;
    while (it != end) {
        if ((*it).second == up) upIt = it;
        if ((*it).second == down) downIt = it;
        it++;
    }
    assert(upIt != end && downIt != end);
    // Case 1: Single selection
    if (up == down) {
        result.insert(up);
        // Case 2: Empty selection
    } else if (*upIt < *downIt) {
        throw ValueNotFound();
    } else {
        // Case 3: Regular selection
        result.insert((*downIt).second);
        while (downIt != upIt) {
            downIt++;
            result.insert((*downIt).second);
        }
    }
    return result;
}

void AxisFiltrationTable::toString() const {
    cout << "AxisFiltrationTable[";
    for (unsigned int axisId = 0; axisId < table.size(); axisId++) {
        cout << table.at(axisId).size() << ";";
    }
    cout << "]" << endl;
}