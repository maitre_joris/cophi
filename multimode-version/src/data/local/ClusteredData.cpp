#include <data/ClusteredData.h>
#include <Error.h>
#include <clustering/NoClustering.h>

using namespace std;
#ifdef _OPENMP

#include <omp.h>

#endif

ClusteredData::ClusteredData(bool enabledMultiThreading,
                             IClustering *clustering,
                             const set<unsigned int> &noClustering,
                             const vector<vector<float>> &dimValues) {
    const unsigned int nbDimensions = dimValues.size();
    resizeVectors(nbDimensions);
    unsigned int nbIndividuals = dimValues.at(0).size();
    IClustering *none = new NoClustering();
    if (enabledMultiThreading) {
#ifdef _OPENMP
        vector<struct ThreadArguments> threadArgs(omp_get_max_threads());
#pragma omp parallel for
        for (unsigned int i = 0; i < nbDimensions; ++i) {
            IClustering *thisDimClustering = noClustering.find(i) == noClustering.end() ? clustering : none;
            int id = omp_get_thread_num();
            threadArgs.at(id) = ThreadArguments(thisDimClustering,
                                                &dimValues.at(i),
                                                nbIndividuals);
            createDimension(&threadArgs.at(id));
            fetchThreadResult(i, threadArgs.at(id));
        }
#endif
    } else {
        for (unsigned int i = 0; i < nbDimensions; ++i) {
            IClustering *thisDimClustering = noClustering.find(i) == noClustering.end() ? clustering : none;
            struct ThreadArguments args(thisDimClustering,
                                        &dimValues.at(i),
                                        nbIndividuals);
            createDimension(&args);
            fetchThreadResult(i, args);
        }
    }
    if (clusterProperties.size() != nbDimensions)
        throw InvalidStateError();
}

void ClusteredData::resizeVectors(unsigned int nbDimensions) {
    clusterProperties.resize(nbDimensions);
    individualToCluster.resize(nbDimensions);
    clusterToIndividuals.resize(nbDimensions);
    dimensionExtrema.resize(nbDimensions);
    dimensionMaxBucketWeight.resize(nbDimensions);
}

void ClusteredData::fetchThreadResult(unsigned int i,
                                      const ThreadArguments &args) {
    individualToCluster.at(i) = args.individualToClusters;
    clusterToIndividuals.at(i) = args.clusterToIndividuals;
    clusterProperties.at(i) = args.clusterProperties;
    dimensionExtrema.at(i) = args.extrema;
    dimensionMaxBucketWeight.at(i) = args.maxBucketWeight;
}

void computeDistribution(const Individuals &valueIds,
                         const vector<float> &rawValues,
                         ClusterProp &cp) {
    if (rawValues.empty()) return;
    if (abs(cp.min - cp.max) < numeric_limits<float>::epsilon()) {
        cp.distribution = Distribution(1, cp.weight);
    } else {
        cp.distribution = Distribution(10, 0);
        for (float vId : valueIds) {
            assert(rawValues.size() > vId);
            float value = rawValues.at(vId);
            if (value < cp.min || value > cp.max) throw InvalidStateError();
            fillDistribution(value, cp.distribution, cp.min, cp.max);
        }
    }
}

void matchIndividualToCluster(IClustering *clustering,
                              const vector<float> &values,
                              unordered_map<unsigned int, cluster> &toClusters,
                              unsigned int nbIndividuals,
                              unordered_map<cluster, Individuals> &toIndividuals) {
    vector<cluster> clusters;
    clustering->run(values, clusters);
    assert(clusters.size() == values.size());
    //cout << "Created " << nbClusters << " clusters w/" << clustering->toString() << endl;
    for (unsigned int i = 0; i < nbIndividuals; i++) {
        cluster valueCluster = clusters.at(i);
        toClusters.insert(std::make_pair(i, valueCluster));
        if (toIndividuals.find(valueCluster) == toIndividuals.end())
            toIndividuals.insert(std::make_pair(valueCluster, Individuals()));
        toIndividuals.at(valueCluster).push_back(i);
    }
}


void fillClusterProperties(
        unordered_map<unsigned int, cluster> &toClusters,
        const vector<float> &values,
        unordered_map<cluster, ClusterProp> &clusterProps,
        unordered_map<cluster, Individuals> &toIndividuals,
        float &globalMin,
        float &globalMax,
        unsigned int &globalBiggestBucketSize) {
    for (const auto &individualCluster : toClusters) {
        cluster valueClusterId = individualCluster.second;
        assert(values.size() > individualCluster.first);
        float value = values.at(individualCluster.first);
        // Updating dimension extrema
        globalMin = min(globalMin, value);
        globalMax = max(globalMax, value);
        auto clusterNodeIt = clusterProps.find(valueClusterId);
        // Insert new cluster in dimensionClusterProp with initial values
        if (clusterNodeIt == clusterProps.end()) {
            ClusterProp newClusterProp(valueClusterId);
            newClusterProp.min = value;
            newClusterProp.max = value;
            newClusterProp.weight = 0;
            clusterProps.insert({valueClusterId, newClusterProp});
        }
        // Update ClusterProp values
        ClusterProp &prop = clusterProps.at(valueClusterId);
        prop.min = min(value, prop.min);
        prop.max = max(value, prop.max);
        prop.weight++;
    }
#ifndef NO_DISTRIBUTIONS
    // Fill distribution property and finish average computation
    for (const auto &p : toIndividuals) {
        ClusterProp &cp = clusterProps.at(p.first);
        computeDistribution(p.second, values, cp);
        for (auto d : cp.distribution)
            globalBiggestBucketSize = max(globalBiggestBucketSize, d);
    }
#endif
}

void *ClusteredData::createDimension(void *args) {
    assert(args != nullptr);
    struct ThreadArguments &arguments = *(struct ThreadArguments *) args;
    const vector<float> &dimValues = *arguments.dimValues;
    unordered_map<unsigned int, cluster> &toClusters = arguments.individualToClusters;
    unordered_map<cluster, Individuals> &toIndividuals = arguments.clusterToIndividuals;
    unordered_map<cluster, ClusterProp> &clusterProps = arguments.clusterProperties;
    assert(dimValues.size() == arguments.nbIndividuals);
    toClusters.clear(); // Should be empty anyways
    matchIndividualToCluster(arguments.clustering,
                             dimValues, toClusters,
                             arguments.nbIndividuals,
                             toIndividuals);
    arguments.extrema.min = numeric_limits<float>::infinity();
    arguments.extrema.max = -numeric_limits<float>::infinity();
    arguments.maxBucketWeight = 0;
    fillClusterProperties(toClusters, dimValues, clusterProps, toIndividuals,
                          arguments.extrema.min,
                          arguments.extrema.max,
                          arguments.maxBucketWeight);
    return nullptr;
}


Individuals ClusteredData::getIndividuals(unsigned int dbDim,
                                          const ClusterIdentifier &clusterId) const {
    assert(clusterToIndividuals.size() > dbDim);
    cluster flatId = clusterId.getLeafId();
    const auto &dimIndividuals = clusterToIndividuals.at(dbDim);
    assert(dimIndividuals.find(flatId) != dimIndividuals.end());
    assert(!dimIndividuals.at(flatId).empty());
    return dimIndividuals.at(flatId);
}


void ClusteredData::forEachCluster(unsigned int dbDim,
                                   const ClusterIdentifier &,
                                   function<void(const ClusterProp &)> f) const {
    for (const auto &p : clusterProperties.at(dbDim))
        f(p.second);
}

ClusterProp ClusteredData::getCluster(unsigned int dbDim,
                                      const ClusterIdentifier &clusterId) const {
    return clusterProperties.at(dbDim).at(clusterId.getLeafId());
}


cluster ClusteredData::getCluster(unsigned int dbDim,
                                  unsigned int i,
                                  const ClusterIdentifier &) const {
    return individualToCluster.at(dbDim).at(i);
}

cluster ClusteredData::getValueCluster(unsigned int, float, const ClusterIdentifier &) const {
    assert(false);
    return ClusterProp::INVALID_ID;
}

unsigned int ClusteredData::getBiggestBucket(unsigned int dbDim, const ClusterIdentifier &) const {
    return dimensionMaxBucketWeight.at(dbDim);
}

const vector<Extrema> &ClusteredData::getDimensionExtrema() const {
    return dimensionExtrema;
}


std::vector<const HNode *> ClusteredData::initDimensionNodes() const {
    std::vector<const HNode *> result;
/*    for (const auto &dimensionClusters : clusterProperties) {
        result.push_back(&n);
    }*/
    return result;
}