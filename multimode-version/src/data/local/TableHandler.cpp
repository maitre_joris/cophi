#include "data/TableHandler.h"
#include <Error.h>
#include <fstream>

#define IGNORE_BAD_VALUES

const float TableHandler::INVALID_VALUE_PLACEHOLDER = -1.f;
const int TableHandler::READ_ALL_LINES = -1;

TableHandler::TableHandler(const TableHandler &rhs) {
    config = rhs.config;
    delimiter = rhs.delimiter;
    nbIndividuals = rhs.nbIndividuals;
    nbDimensions = rhs.nbDimensions;
    columnLabels = rhs.columnLabels;
    rowLabels = rhs.rowLabels;
    table = new Table(*rhs.table);
    rowLabelColumnName = rhs.rowLabelColumnName;
    nominalLabels = rhs.nominalLabels;
    foundInvalidValues = rhs.foundInvalidValues;
}

TableHandler::TableHandler(const Configuration &config) : config(config) {
    nominalLabels.resize(config.nominal.size());
    readData(config.linesToRead);
    table->computeReverseIndex();
}

TableHandler::TableHandler(const std::string filePath, int lineToRead) {
    config.data = filePath;
    config.isFilePath = true;
    readData(lineToRead);
    table->computeReverseIndex();
}

TableHandler::TableHandler(const std::vector<std::vector<float>> &values) :
        nbIndividuals(values.at(0).size()),
        nbDimensions(values.size()) {
    table = new Table(values);
    table->computeReverseIndex();
}

TableHandler::~TableHandler() {
    delete table;
}

char TableHandler::detectDelimiter(std::string &line) const {
    bool foundSpace = false;
    char delimiter = ' ';
    for (const auto &c : line) {
        if (c == '\t') {
            delimiter = '\t';
            break;
        }
        if (c == ' ') {
            foundSpace = true;
        } else if (c == ';') {
            delimiter = ';';
            break;
        } else if (c == ',') {
            delimiter = ',';
            break;
        }
    }
    if (!foundSpace && delimiter == ' ') {
        std::cerr << "Detecting delimiter on line:\n" << line << "\n failed" << std::endl;
        throw ImportError(ImportError::DELIMITER);
    } else {
        std::cout << "Identified delimiter ascii code: " << (int) delimiter << std::endl;
        return delimiter;
    }
}

int TableHandler::initRead(std::istream *in, int linesToRead) {
    std::string line;
    std::vector<std::string> tokens;
    std::getline(*in, line);
    delimiter = detectDelimiter(line);
    tokenize(line, tokens, delimiter);
    if (tokens.size() <= 2) throw ImportError(ImportError::DELIMITER);
    auto nbColumns = (unsigned int) tokens.size();
    nbDimensions = config.withRowLabels ? nbColumns - 1 : nbColumns;
    table = new Table(nbDimensions, linesToRead);
    std::cout << "Read nbDimensions =" << nbDimensions << std::endl;
    unsigned int firstValueIndex = config.withRowLabels ? 1 : 0;
    if (config.withColumnLabels) {
        if (config.withRowLabels)
            rowLabelColumnName = tokens[0];
        for (unsigned int i = firstValueIndex; i < tokens.size(); i++)
            columnLabels.push_back(tokens[i]);
    } else {
        // Return file reader to beginning
        in->clear();
        in->seekg(0, std::ios::beg);
    }
    return nbColumns;
}


std::vector<std::map<std::string, int>> TableHandler::parseFile(std::istream *in, int linesToRead, int nbColumns) {
    std::vector<std::string> tokens;
    int linesNo = 0;
    std::vector<std::map<std::string, int>> labelToValue(config.nominal.size());
    for (std::string line; std::getline(*in, line) && (linesToRead == -1 || linesNo <= linesToRead);) {
        if (!line.empty()) {
            tokens.clear();
            tokenize(line, tokens, delimiter);
            if (tokens.size() != nbColumns)
                throw ImportError(ImportError::NONSQUARE_TABLE, linesNo);
            unsigned int firstValueIndex;
            if (config.withRowLabels) {
                rowLabels.push_back(tokens[0]);
                firstValueIndex = 1;
            } else {
                firstValueIndex = 0;
            }
            try {
                for (unsigned i = firstValueIndex; i < nbColumns; i++) {
                    unsigned int dim = i - firstValueIndex;
                    assert(tokens.size() > i);
                    table->insertValue(dim, nbIndividuals, processToken(tokens[i], dim, labelToValue));
                }
            } catch (std::invalid_argument &e) {
                // Infinity not handle here
                throw ImportError(ImportError::INVALID_VALUE, linesNo);
            }
            nbIndividuals++; // New line
            linesNo++;
        } else {
            return labelToValue;
        }
    }
    return labelToValue;

}

void TableHandler::readData(std::istream *in, int linesToRead) {
    if (in->eof()) throw ImportError(ImportError::NO_FILE);
    int nbColumns = initRead(in, linesToRead); // Find delimiter, number of columns and labels, etc using first line
    std::vector<std::map<std::string, int>> labelToValue = parseFile(in, linesToRead, nbColumns);
    // Now reset categorical values so that the placeholders follow alphabetical order
    std::vector<std::unordered_map<int, int>> replacement(labelToValue.size());
    for (unsigned int d = 0; d < labelToValue.size(); d++) {
        assert(nominalLabels.find(d) != nominalLabels.end());
        auto &vtl = nominalLabels[d];
        const auto &ltv = labelToValue[d];
        auto &r = replacement[d];
        unsigned int orderedValue = 0;
        for (auto it = ltv.begin(); it != ltv.end(); it++) {
            vtl.insert({orderedValue, it->first});
            assert(it->second < replacement[d].size());
            assert(r.size() > it->second);
            r.insert({it->second, orderedValue});
            orderedValue++;
        }
    }

    // Replace categorical values
    for (unsigned int d = 0; d < labelToValue.size(); d++) {
        auto dimId = (unsigned int) std::distance(config.nominal.begin(), config.nominal.find(d));
        assert(labelToValue.size() > d);
        assert(nominalLabels.find(d) != nominalLabels.end());
        assert(table->table.size() > dimId);

        const auto &ltv = labelToValue[d];
        auto &r = replacement[d];
        auto &vtl = nominalLabels[d];
        auto &v = table->table[dimId];
        for (unsigned int i = 0; i < nbIndividuals; i++) {
            if (r.find(v[i]) != r.end()) {
                v[i] = r[v[i]];
            } else {
                v[i] = INVALID_VALUE_PLACEHOLDER;
            }
        }
    }


    if (foundInvalidValues)
        std::cout << "[TableHandler] Some invalid values were replaced by default (" << INVALID_VALUE_PLACEHOLDER << ")"
                  << std::endl;
}

void TableHandler::readData(int lineToRead) {
    assert(table == nullptr);
    std::istream *in = nullptr;

    if (config.isFilePath) {
        std::cout << "Reading a file" << std::endl;
        std::ifstream *ifn = new std::ifstream(config.data);
        if (!ifn->is_open()) throw ImportError(ImportError::NO_FILE);
        in = ifn;
        readData(in, lineToRead);
        ifn->close();
    } else {
        in = new std::istringstream(config.data);
        readData(in, lineToRead);
    }

    std::cout << "Finished loading CSV (" << nbIndividuals << " individuals)" << std::endl;
}

float TableHandler::processToken(std::string &token,
                                 unsigned int dim,
                                 std::vector<std::map<std::string, int>> &labelToValue) {
    // Handle value assignation for nominal values
    const auto &dimIt = config.nominal.find(dim);
    if (dimIt != config.nominal.end()) {
        int d = (int) std::distance(config.nominal.begin(), dimIt);
        assert(labelToValue.size() > d);
        auto &lvt = labelToValue[d];
        const auto &categoryId = lvt.find(token);
        if (categoryId == lvt.end()) lvt.insert({token, lvt.size()});
        return float(lvt[token]);
    }

    float result = 0.f;
    try {
        result = std::stof(token);
        if (!std::isnormal(result) && result != 0.f) {
            throw std::logic_error(std::string("Invalid: ") + token);
        }
    } catch (std::exception &e) {
#ifdef IGNORE_BAD_VALUES
        foundInvalidValues = true;
        result = INVALID_VALUE_PLACEHOLDER; // Hack for invalid/empty values
#else
        throw ImportError(ImportError::INVALID_VALUE);
#endif
    }
    return result;

}

LowHighPair<std::pair<unsigned int, float>> TableHandler::getOuter(float minFocus,
                                                                   float maxFocus,
                                                                   unsigned int dbDim) const {
    float minUpper = table->getMinUpper(dbDim, maxFocus);
    unsigned int countUpper = table->countOver(dbDim, maxFocus);
    unsigned int countLower = table->countUnder(dbDim, minFocus);
    float maxLower = table->getMaxLower(dbDim, maxFocus);
    return {{countLower, maxLower},
            {countUpper, minUpper}};
}

Extrema TableHandler::getCommonExtrema() const {
    bool provided = std::numeric_limits<float>::infinity() != config.minValue &&
                    std::numeric_limits<float>::infinity() != config.maxValue;
    if (provided) {
        return {config.minValue, config.maxValue};
    }

    return {};

}

bool TableHandler::isQualitative(unsigned int dim) const {
    return config.nominal.find(dim) == config.nominal.end() &&
           config.ordinal.find(dim) == config.ordinal.end();
}


unsigned int TableHandler::countInner(unsigned int dbDim, float min, float max) const {
    return table->countBetween(dbDim, min, max);
}

Individuals TableHandler::getInner(unsigned int dbDim,
                                   float min,
                                   float max) const {
    return table->getInBetween(dbDim, min, max);
}

const std::string TableHandler::getClusterLabel(unsigned int dim, int value) const {
    const auto &dimIt = config.nominal.find(dim);
    if (dimIt == config.nominal.end()) return "";
    const auto &dimLabels = nominalLabels[(unsigned long) std::distance(config.nominal.begin(), dimIt)];
    if (dimLabels.find(value) != dimLabels.end()) return dimLabels.at(value);
    return "";
}

unsigned int TableHandler::getNbIndividuals() const {
    assert(table != nullptr);
    return nbIndividuals;
}

unsigned int TableHandler::getNbDimensions() const {
    assert(table != nullptr);
    return nbDimensions;
}

const std::vector<std::vector<float>> &TableHandler::getTable() const {
    assert(table != nullptr);
    return table->table;
}

float TableHandler::getValue(unsigned int i, unsigned int dbDim) const {
    assert(i < nbIndividuals);
    return table->table[dbDim][i];
}

const std::vector<std::string> TableHandler::getLabels() const {
    assert(table != nullptr);
    return columnLabels;
}

void TableHandler::writeRow(unsigned int row,
                            const std::vector<unsigned int> &dimensionOrdering,
                            std::stringstream &ss) const {
    ss.str(std::string()); // Clear stream
    if (!this->rowLabels.empty())
        ss << this->rowLabels[row] << this->delimiter;
    for (unsigned int d = 0; d < dimensionOrdering.size(); d++) {
        ss << this->table->table[dimensionOrdering[d]][row];
        if (d < dimensionOrdering.size() - 1)
            ss << this->delimiter;
    }
}

void TableHandler::exportCSV(std::ostream *os,
                             const Individuals &individuals,
                             const std::vector<unsigned int> &dimensionOrdering) const {
    assert(table != nullptr);
    std::stringstream ss;
    if (!columnLabels.empty()) {
        if (!rowLabels.empty())
            ss << rowLabelColumnName << delimiter;
        for (unsigned int i = 0; i < dimensionOrdering.size(); i++) {
            ss << columnLabels[dimensionOrdering[i]];
            if (i + 1 < dimensionOrdering.size())
                ss << delimiter;
        }
    }
    (*os) << ss.str() << '\n';
    if (individuals.empty())
        for (unsigned int i = 0; i < nbIndividuals; i++) {
            writeRow(i, dimensionOrdering, ss);
            (*os) << ss.str() << '\n';
        }
    else
        for (unsigned int i : individuals) {
            writeRow(i, dimensionOrdering, ss);
            (*os) << ss.str() << '\n';
        }
}

void TableHandler::clear() {
    if (table != nullptr)
        delete table;
    columnLabels.clear();
    rowLabels.clear();
}

std::string TableHandler::getFilename() const {
    if (config.isFilePath)
        return config.data;
    return "";
}
