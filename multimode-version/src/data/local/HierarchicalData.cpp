#include <Error.h>
#include "data/HierarchicalData.h"

using namespace std;


HierarchicalData::HierarchicalData() {
    // No constructor?
}

void HierarchicalData::resizeVectors(unsigned int nbDimensions) {
    dimensionClustering.resize(nbDimensions);
    dimensionExtrema.resize(nbDimensions);
}

Individuals HierarchicalData::getIndividuals(unsigned int,
                                             const ClusterIdentifier &) const {
    //return findNode(dbDim, clusterId).getIndividuals();
    assert(false);
    return Individuals();
}

void HierarchicalData::forEachCluster(unsigned int dbDim,
                                      const ClusterIdentifier &parentId,
                                      std::function<void(const ClusterProp &)> f) const {
    const HNode &parent = parentId.isRoot() ? dimensionClustering.at(dbDim) : findNode(dbDim, parentId);
    parent.applyChildren(static_cast<std::function<void(const HNode &)>>(f));
}

cluster HierarchicalData::getValueCluster(unsigned int dbDim,
                                          float value,
                                          const ClusterIdentifier &parent) const {
    const HNode &parentNode = findNode(dbDim, parent);
    if (parentNode.inBound(value)) {
        cluster id = parentNode.getMatchingChildrenId([value](const HNode &child) {
            return child.inBound(value);
        });
        assert(id != HNode::INVALID_ID);
        return id;
    } else {
        assert(!parent.isRoot());
        if (value > parentNode.getMax()) return HNode::HIGHER_META_NODE_ID;
        else if (value < parentNode.getMin()) return HNode::LOWER_META_NODE_ID;
        return HNode::INVALID_ID;
    }
}

std::string HierarchicalData::dump() const {
    stringstream ss;
    unsigned int d = 0;
    const char separator = ',';
    for (const HNode &n : dimensionClustering) {
        auto dumpChildren = [d, separator, &ss](const HNode &c, ClusterIdentifier p) {
            ClusterIdentifier cp = ClusterIdentifier(p, c.clusterId);
            ss << d << separator << c.min << separator << c.max << separator << cp.toString() << endl;
        };
        ClusterIdentifier ci;
        n.applyChildren([ci, d, separator, &ss, &dumpChildren](const HNode &cn) {
            ClusterIdentifier cci = ClusterIdentifier(ci, cn.clusterId);
            ss << d << separator << cn.min << separator << cn.max << separator << cci.toString() << endl;
            cn.applyChildren([d, separator, cci, &ss, &dumpChildren](const HNode &ccn) {
                dumpChildren(ccn, cci);
            });
        });
        d++;
    }
    return ss.str();
}

HierarchicalData::Siblings HierarchicalData::getSiblings(unsigned int dbDim,
                                                         const ClusterIdentifier &id) const {
    ClusterIdentifier parent = ClusterIdentifier::getParent(id);
    const HNode &parentNode = findNode(dbDim, parent);
    cluster us = id.getLeafId();
    Siblings result;
    parentNode.applyChildren([us, &result](const ClusterProp &c) {
        if (c.getId() > us) {
            result.second.insert(c);
        } else if (c.getId() < us) {
            result.first.insert(c);
        }
    });
    return result;
}

ClusterProp HierarchicalData::getCluster(unsigned int dbDim,
                                         const ClusterIdentifier &id) const {
    assert(!id.isRoot());
    if (id.getLeafId() == ClusterProp::HIGHER_META_NODE_ID) {
        return ClusterProp(ClusterProp::INVALID_ID);
    } else if (id.getLeafId() == ClusterProp::LOWER_META_NODE_ID) {
        return ClusterProp(ClusterProp::INVALID_ID);
    } else {
        return findNode(dbDim, id);
    }
}

const HNode &HierarchicalData::findNode(unsigned int dbDim,
                                        const ClusterIdentifier &clusterId) const {
    if (clusterId.isRoot()) return dimensionClustering.at(dbDim);
    // For now: tree traversal
    const HNode *currentParent = &dimensionClustering.at(dbDim);
    for (cluster parent : clusterId) {
        currentParent = currentParent->getChild(parent);
        if (currentParent == nullptr) throw InvalidStateError();
    }
    assert(currentParent != nullptr);
    return *currentParent;
}


cluster HierarchicalData::getCluster(unsigned int,
                                     unsigned int,
                                     const ClusterIdentifier &) const {

    /*
     cluster findIndividualInChildren(const HNode &parent, unsigned int i) {
        cluster childId = parent.getMatchingChildrenId([i](const HNode &c) {
            return c.contains(i);
        });
        return childId;
    }

    const HNode &parentNode = findNode(dbDim, nearestParent);
    cluster childId = findIndividualInChildren(parentNode, i);
    if (childId == HNode::INVALID_ID) {
        // Find great-parent with one child holding i
        ClusterIdentifier greatParent;
        ClusterIdentifier parent = nearestParent;
        bool finishedTraversal = false;
        while (!finishedTraversal) {//throw InvalidStateError()
            greatParent = greatParent.getParent(parent);
            const HNode &greatParentNode = findNode(dbDim, greatParent);
            cluster uncleId = findIndividualInChildren(greatParentNode, i);
            if (uncleId == HNode::INVALID_ID) {
                if (greatParent.isRoot()) finishedTraversal = true;
            } else {
                if (uncleId > parent.getLeafId()) return HNode::HIGHER_META_NODE_ID;
                if (uncleId < parent.getLeafId()) return HNode::LOWER_META_NODE_ID;
                break;
            }
            parent = greatParent;
        }
        throw InvalidStateError();
    } else {
        return childId;
    }*/
    return ClusterProp::INVALID_ID;
}

unsigned int HierarchicalData::getBiggestBucket(unsigned int dbDim,
                                                const ClusterIdentifier &parent) const {
    return findNode(dbDim, parent).getChildrenBiggestBucket();
}

const std::vector<Extrema> &HierarchicalData::getDimensionExtrema() const {
    return dimensionExtrema;
}

std::vector<const HNode *> HierarchicalData::initDimensionNodes() const {
    std::vector<const HNode *> result;
    for (const auto &n : dimensionClustering)
        result.push_back(&n);
    return result;
}