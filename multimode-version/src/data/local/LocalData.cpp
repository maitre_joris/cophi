/*
  Copyright 2015 Romain Bourqui  <romain.bourqui@labri.fr>
  Copyright 2015 Timothée Jourde <itim.lcf@gmail.com>
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <map>
#include <clustering/Canopy.h>
#include <thread>
#include "Error.h"

#ifdef _OPENMP

#include <omp.h>

#endif
#ifdef HIERARCHICAL

#include <data/ClusteredHierarchicalData.h>

#else

#include <data/ClusteredData.h>

#endif

using namespace std;


Individuals individualsIntersection(Individuals &i1,
                                    Individuals &i2) {
    Individuals result;
    std::sort(i1.begin(), i1.end());
    std::sort(i2.begin(), i2.end());
    std::set_intersection(i1.begin(), i1.end(),
                          i2.begin(), i2.end(),
                          std::back_inserter(result));
    return result;
}

Individuals individualsUnion(Individuals &i1,
                             Individuals &i2) {
    Individuals result;
    std::sort(i1.begin(), i1.end());
    std::sort(i2.begin(), i2.end());
    std::set_union(i1.begin(), i1.end(),
                   i2.begin(), i2.end(),
                   std::back_inserter(result));
    return result;
}


Individuals individualsDifference(Individuals &i1,
                                  Individuals &i2) {
    Individuals result;
    std::sort(i1.begin(), i1.end());
    std::sort(i2.begin(), i2.end());
    std::set_difference(i1.begin(), i1.end(),
                        i2.begin(), i2.end(),
                        std::back_inserter(result));
    return result;
}

void LocalData::invertSelection() {
    if (current.highlight != nullptr) {
        Individuals selected = current.highlight->getIndividuals();
        std::sort(selected.begin(), selected.end());
        Individuals inversion;
        inversion.reserve(getNbIndividuals() - selected.size());
        unsigned int j = 0;
        for (unsigned int i : selected) {
            while (i > j) {
                inversion.push_back(j);
                j++;
            }
            j++;
        }
        clearHighlight();
        current.highlight = new Highlight(this, SelectedElement(), inversion);
        buildHighlight(current.highlight);
        if (observer_ != nullptr)
            observer_->onHighlight(current.highlight);
    }
}

void LocalData::clear() {
    provider = nullptr;
    clustering = nullptr;
}

LocalData::~LocalData() {
    observer_ = nullptr;
    clearHighlight();
    clearHighlightPreview();
    if (provider != nullptr)
        delete provider;
    provider = nullptr;
    if (clustering != nullptr)
        delete clustering;
    clustering = nullptr;
}

LocalData::LocalData(DataObserver *obs,
                     const TableHandler *csv,
                     const IClusteredData *dbData,
                     bool multiThreading) : DataHandler(obs, true),
                                            provider(csv),
                                            clustering(dbData),
                                            enabledMultiThreading(multiThreading) {
    init(csv->getNbDimensions(), csv->getNbIndividuals());
    dimensionLabels = csv->getLabels();
    buildData();
}


LocalData::LocalData(const LocalData &rhs) : DataHandler(rhs) {
    provider = rhs.provider;
    clustering = rhs.clustering;
    preview = nullptr;
    currentOp = rhs.currentOp;
    enabledMultiThreading = rhs.enabledMultiThreading;
    dimensionParentNode = rhs.dimensionParentNode;
}


LocalData::LocalData(DataObserver *obs,
                     const TableHandler *csv,
                     IClustering *c,
                     const set<unsigned int> &noClustering,
                     bool mt) : DataHandler(obs, true),
                                provider(csv),
                                enabledMultiThreading(mt) {
    init(csv->getNbDimensions(), csv->getNbIndividuals());
    dimensionLabels = csv->getLabels();
    if (c->getType() != IClustering::NONE &&
        (c->getNbClusters() > nbSourceIndividuals || c->getNbClusters() == 0))
        throw ImportError(ImportError::CLUSTERING);

#ifdef HIERARCHICAL
    clustering = new ClusteredHierarchicalData(enabledMultiThreading,
                                               (HClustering *) c,
                                               noClustering,
                                               csv->getTable());

#else
    clustering = new ClusteredData(enabledMultiThreading,
                                   c,
                                   noClustering,
                                   csv->getTable());
#endif
    buildData();
}

void LocalData::createFocusElements(unsigned int axisId, const ClusterIdentifier &) {
    unsigned int nbChildren = 0;
    unsigned int dbDim = toDbDimensions.at(axisId);
    dimensionParentNode.at(dbDim)->applyChildren([this, &nbChildren, axisId, dbDim](
            const ClusterProp &cp) {
        nbChildren++;
        node clusterNode = DataHandler::addNode();
        NodeProp np = NodeProp(cp, axisId, dbDim);
        nodeProperties[clusterNode] = np;
        if (axesNodes.at(axisId).find(cp.clusterId) ==
            axesNodes.at(axisId).end()) {
            axesNodes.at(axisId).insert({cp.clusterId, clusterNode});
        } else throw InvalidStateError();
    });
    assert(!axesNodes.at(axisId).empty());
    if (axisId > 0)
        createEdges(axisId - 1, axisId);
    if (axisId < getLastAxisId())
        createEdges(axisId, axisId + 1);
}

void LocalData::doCreateOpeningElements(unsigned int axisId, cluster, const ClusterIdentifier &newParent) {
    if (preview != nullptr) {
        clearHighlightPreview();
    }
    // Update dimensionParentNodePointer
    createFocusElements(axisId, newParent);
}


/*
void LocalData::applyOpening(unsigned int axisId, cluster, const ClusterIdentifier &newParent) {
    if (current.highlight != nullptr) {
        buildHighlight(current.highlight);
        if (observer_ != nullptr)
            observer_->onHighlight(current.highlight);
    }
}
*/


void LocalData::doCreateClosingElements(unsigned int axisId, unsigned int) {
    assert(axesNodes.at(axisId).empty());
    if (preview != nullptr) {
        clearHighlightPreview();
    }
    unsigned int dbDim = toDbDimensions.at(axisId);
    const ClusterIdentifier &parent = current.dimensionParents.at(dbDim);
    createContextElements(dbDim, axisId, parent);
    createFocusElements(axisId, parent);
    if (current.highlight != nullptr) {
        buildHighlight(current.highlight);
        if (observer_ != nullptr)
            observer_->onHighlight(current.highlight);
    }
}

/*
void LocalData::doCreateOpeningElements(unsigned int axisId, unsigned int ) {
    if (current.highlight != nullptr) {
        buildHighlight(current.highlight);
        if (observer_ != nullptr)
            observer_->onHighlight(current.highlight);
    }
}*/

LowHighPair<ClusterProp> createContextMT(Extrema extremaTot,
                                         float minFocus,
                                         float maxFocus,
                                         const vector<float> &values) {

    ClusterProp highContextNode(ClusterProp::HIGHER_META_NODE_ID);
    ClusterProp lowContextNode(ClusterProp::LOWER_META_NODE_ID);
    highContextNode.max = extremaTot.max;
    highContextNode.min = numeric_limits<float>::infinity();
    lowContextNode.min = extremaTot.min;
    lowContextNode.max = -numeric_limits<float>::infinity();

    auto init = LowHighPair<ClusterProp>(lowContextNode, highContextNode);
#ifdef _OPENMP
    float lMax = init.getLow().max;
    float hMin = init.getHigh().min;
    unsigned int lWeight = 0;
    unsigned int hWeight = 0;
#pragma omp parallel for schedule(static), reduction(min:hMin), reduction(max:lMax), reduction(+:lWeight, hWeight)
    for (unsigned int i = 0; i < values.size(); i++) {
        float v = values.at(i);
        if (v > maxFocus) {
            hMin = std::min(hMin, v);
            hWeight++;
        } else if (v < minFocus) {
            lMax = std::max(lMax, v);
            lWeight++;
        }
    }
    init.getLow().max = lMax;
    init.getLow().weight = lWeight;
    init.getHigh().min = hMin;
    init.getHigh().weight = hWeight;
    return init;
#else
    multiThreadingWrapper<float, LowHighPair<ClusterProp>>
            (values.size(),
             [&values](int i) {
                 return values.at(i);
             },
             [minFocus, maxFocus](const float &v, int, LowHighPair<ClusterProp> &r) {
                 if (v > maxFocus) {
                     ClusterProp &h = r.getHigh();
                     h.min = std::min(h.min, v);
                     h.weight++;
                 } else if (v < minFocus) {
                     ClusterProp &l = r.getLow();
                     l.max = std::max(l.max, v);
                     l.weight++;
                 }
             },
             [](const LowHighPair<ClusterProp> &r, LowHighPair<ClusterProp> &r0) {
                 r0.getLow().merge(r.getLow());
                 r0.getHigh().merge(r.getHigh());
             },
             init);
    return init;
#endif
}

void LocalData::createContextElements(unsigned int dbDim, unsigned int axisId, const ClusterIdentifier &parent) {
    if (parent.isRoot()) return;
    assert(dimensionParentNode.at(dbDim) != nullptr);
    const HNode *focusNodes = dimensionParentNode.at(dbDim);
    Extrema extrema = clustering->getDimensionExtrema().at(dbDim);
    LowHighPair<ClusterProp> contextNodes;
    if (enabledMultiThreading)
        contextNodes = createContextMT(extrema,
                                       focusNodes->getMin(),
                                       focusNodes->getMax(),
                                       provider->getTable().at(dbDim));
    else {
        auto contextLimits = provider->getOuter(focusNodes->getMin(), focusNodes->getMax(), dbDim);
        ClusterProp highContextNode(ClusterProp::HIGHER_META_NODE_ID);
        ClusterProp lowContextNode(ClusterProp::LOWER_META_NODE_ID);
        highContextNode.max = extrema.max;
        highContextNode.min = contextLimits.getHigh().second;
        highContextNode.weight = contextLimits.getHigh().first;
        lowContextNode.min = extrema.min;
        lowContextNode.max = contextLimits.getLow().second;
        lowContextNode.weight = contextLimits.getLow().first;
        contextNodes = LowHighPair<ClusterProp>(lowContextNode, highContextNode);
    }

    assert(contextNodes.getHigh().getWeight() + contextNodes.getLow().getWeight() + focusNodes->getWeight() ==
           nbSourceIndividuals);

    // Do insert
    if (contextNodes.getHigh().getWeight() > 0) {
        node nHigh = DataHandler::addNode();
        nodeProperties[nHigh] = NodeProp(contextNodes.getHigh(), axisId, dbDim);
        assert(axesNodes.at(axisId).find(contextNodes.getHigh().getId()) == axesNodes.at(axisId).end());
        axesNodes.at(axisId).insert({contextNodes.getHigh().getId(), nHigh});
    }
    if (contextNodes.getLow().getWeight() > 0) {
        node nLow = DataHandler::addNode();
        nodeProperties[nLow] = NodeProp(contextNodes.getLow(), axisId, dbDim);
        assert(axesNodes.at(axisId).find(contextNodes.getLow().getId()) == axesNodes.at(axisId).end());
        axesNodes.at(axisId).insert({contextNodes.getLow().getId(), nLow});
    }

}

unsigned int LocalData::getBiggestBucket(unsigned int dbDim) const {
    return clustering->getBiggestBucket(dbDim, current.dimensionParents.at(dbDim));
}


void LocalData::reorderAxes(DimensionInterval toDbDims) {
    assert(toDbDims.size() == toDbDimensions.size());
    toDbDimensions = toDbDims;
    graph.clear();
    graph.alloc(nodeProperties);
    graph.alloc(edgeProperties);
    axesNodes.clear();
    axesNodes.resize(nbSourceDimensions);
    for (unsigned int i = 0; i < nbSourceDimensions; ++i)
        createNodes(toDbDimensions.at(i), i);
    for (unsigned int i = 0; i < nbSourceDimensions - 1; ++i)
        createEdges(i, i + 1);
    checkCoherence();
}


void LocalData::buildData() {
    toDbDimensions = DimensionInterval(0, nbSourceDimensions - 1);
    axesNodes.clear();
    axesNodes.resize(nbSourceDimensions);
    dimensionParentNode = clustering->initDimensionNodes();
    for (unsigned int i = 0; i < nbSourceDimensions; ++i)
        createNodes(toDbDimensions.at(i), i);
    for (unsigned int i = 0; i < nbSourceDimensions - 1; ++i)
        createEdges(i, i + 1);
    finish();
}

void LocalData::buildHighlightPreview(HighlightingOp op,
                                      Individuals &newIndividuals) {
    assert(!newIndividuals.empty());
    if (preview != nullptr) {
        delete preview;
        preview = nullptr;
    }
    if (op == HighlightingOp::add) {
        Individuals curI(current.highlight->getIndividuals());
        Individuals newI(newIndividuals);

        Individuals i = individualsUnion(curI, newI);
        preview = new Highlight(this, SelectedElement(), i);
    } else if (op == HighlightingOp::substract) {
        Individuals previous = current.highlight->getIndividuals();
        Individuals i = individualsDifference(previous, newIndividuals);
        preview = new Highlight(this, SelectedElement(), i);
    } else if (op == HighlightingOp::intersect) {
        Individuals previous = current.highlight->getIndividuals();
        Individuals i = individualsIntersection(previous, newIndividuals);
        preview = new Highlight(this, SelectedElement(), i);
    } else return;
    buildHighlight(preview);
}

const Extrema LocalData::getAxisExtrema(unsigned int axisId) const {
    unsigned int dim = toDbDimensions.at(axisId);
    Extrema providedEx = provider->getCommonExtrema();
    Extrema effectiveEx = clustering->getDimensionExtrema().at(dim);
    if (provider->isQualitative(dim) && providedEx.isValid())
        return {std::min(effectiveEx.min, providedEx.min),
                std::max(effectiveEx.max, providedEx.max)}; //TODO Alarm when provided is smaller than effective

    return effectiveEx;
}

void LocalData::buildHighlight(Highlight *h) {
    const Individuals &individuals = h->getIndividuals();
    if (enabledMultiThreading) {
        assert(getNbAxes() > 2);
        vector<unordered_map<EdgeKey, unsigned int, edgekeyhash>> edgeWeights(getNbAxes() - 1);
        for (unsigned int d = 0; d < getNbAxes() - 1; d++) {
            edgeWeights.at(d).reserve(axesNodes.at(d).size() * axesNodes.at(d + 1).size());
        }
        vector<unordered_map<cluster, unsigned int>> nodeWeights(getNbAxes());
        vector<unordered_map<cluster, Distribution>> nodeDistributions(getNbAxes());
        buildSubGraphMT(individuals, 0, getLastAxisId(), edgeWeights, nodeWeights, nodeDistributions);

        for (unsigned int axisId = 0; axisId < getNbAxes(); axisId++) {
            for (auto &np: nodeWeights.at(axisId)) {
                const node &n = axesNodes.at(axisId).at(np.first);
                if (!h->hasNode(n)) {
                    assert(graph.isElement(n));
                    if (nodeDistributions[axisId].empty()) {
                        h->addNode(n, np.second, Distribution());
                    } else {
                        h->addNode(n, np.second, nodeDistributions[axisId][np.first]);
                    }
                }
            }
            if (axisId > 0) {
                for (auto &ek : edgeWeights.at(axisId - 1)) {
                    assert(ek.first.leftAxisId == axisId - 1);
                    try {
                        const node &leftNode = axesNodes.at(ek.first.leftAxisId).at(ek.first.leftId);
                        const node &rightNode = axesNodes.at(ek.first.leftAxisId + 1).at(ek.first.rightId);
                        assert(h->hasNode(leftNode));
                        assert(h->hasNode(rightNode));
                        edge e = graph.existEdge(leftNode, rightNode, false);
                        if (e.isValid()) {
                            if (!h->hasEdge(e)) {
                                h->addEdge(e, ek.second);
                            }
                        }
                    } catch (const QueryError &e) {

                    }
                }
            }
        }

    } else {
        unordered_map<node, unsigned int> nodeWeights;
        unordered_map<node, Distribution> nodeDistributions;
        unordered_map<edge, unsigned int> edgeWeights;
        buildSubGraph(individuals, 0, getLastAxisId(), nodeWeights, nodeDistributions, edgeWeights);
        for (auto &p : nodeWeights) {
            if (!h->hasNode(p.first)) {
                assert(graph.isElement(p.first));
                if (nodeDistributions.empty()) h->addNode(p.first, p.second, Distribution());
                else h->addNode(p.first, p.second, nodeDistributions[p.first]);
            }
        }
        for (auto &p : edgeWeights) {
            if (!h->hasEdge(p.first)) {
                assert(graph.isElement(p.first));
                // Ensuring end nodes are included in insertion bundle if not existing
                const auto &ends = graph.ends(p.first);
                assert(h->hasNode(ends.first) || nodeWeights.find(ends.first) != nodeWeights.end());
                assert(h->hasNode(ends.second) || nodeWeights.find(ends.second) != nodeWeights.end());
                h->addEdge(p.first, p.second);
            }
        }
    }
}

void LocalData::triggerHighlight(const edge &e) {
    if (!graph.isElement(e)) throw InvalidStateError();

    pair<node, node> ends = graph.ends(e);
    const NodeProp np1 = nodeProperties[ends.first];
    const NodeProp np2 = nodeProperties[ends.second];

    Individuals firstI = getClusterIndividuals(np1);
    Individuals secondI = getClusterIndividuals(np2);
    Individuals eIndividuals = individualsIntersection(firstI, secondI);
    if (eIndividuals.empty()) return;
    if (currentOp == HighlightingOp::replace) {
        clearHighlight();
        current.highlight = new Highlight(this, SelectedElement(e), eIndividuals);
        buildHighlight(current.highlight);
        if (observer_ != nullptr)
            observer_->onHighlight(current.highlight);
    } else {
        if (current.highlight == nullptr) throw InvalidStateError();
        buildHighlightPreview(currentOp, eIndividuals);
        if (observer_ != nullptr)
            observer_->onHighlight(preview);
    }
}

void LocalData::triggerHighlight(const node &n) {

    if (!graph.isElement(n)) throw InvalidStateError();
    const NodeProp &np = nodeProperties[n];
    Individuals nIndividuals = getClusterIndividuals(np);
    if (nIndividuals.empty()) return;
    if (currentOp == HighlightingOp::replace || current.highlight == nullptr) {
        clearHighlight();
        current.highlight = new Highlight(this, SelectedElement(n), nIndividuals);
        buildHighlight(current.highlight);
        if (observer_ != nullptr)
            observer_->onHighlight(current.highlight);
    } else {
        buildHighlightPreview(currentOp, nIndividuals);
        if (observer_ != nullptr)
            observer_->onHighlight(preview);
    }
}

void LocalData::benchTriggerHighlight(const edge &e) {
    if (!graph.isElement(e)) throw InvalidStateError();
    pair<node, node> ends = graph.ends(e);
    const NodeProp &np1 = nodeProperties[ends.first];
    const NodeProp &np2 = nodeProperties[ends.second];
    Individuals firstI = getClusterIndividuals(np1);
    Individuals secondI = getClusterIndividuals(np2);
    Individuals eIndividuals = individualsIntersection(firstI, secondI);
    vector<unordered_map<EdgeKey, unsigned int, edgekeyhash>> edgeWeights(getNbAxes() - 1);
    vector<unordered_map<cluster, unsigned int>> nodeWeights(getNbAxes());
    vector<unordered_map<cluster, Distribution>> nodeDistributions;
    buildSubGraphMT(eIndividuals, 0, getLastAxisId(), edgeWeights, nodeWeights, nodeDistributions);
}

void LocalData::benchTriggerHighlight(const node &n) {
    if (!graph.isElement(n)) throw InvalidStateError();
    const NodeProp &np = nodeProperties[n];
    Individuals nIndividuals = getClusterIndividuals(np);
    vector<unordered_map<EdgeKey, unsigned int, edgekeyhash>> edgeWeights(getNbAxes() - 1);
    vector<unordered_map<cluster, unsigned int>> nodeWeights(getNbAxes());
    vector<unordered_map<cluster, Distribution>> nodeDistributions;
    buildSubGraphMT(nIndividuals, 0, getLastAxisId(), edgeWeights, nodeWeights, nodeDistributions);
}

void LocalData::restitchHighlight(unsigned int start, unsigned int end) {
    assert(!current.highlight->isEmpty());
    unordered_map<node, unsigned int> nodeWeights;
    unordered_map<node, Distribution> nodeDistributions;
    unordered_map<edge, unsigned int> edgeWeights;
    buildSubGraph(current.highlight->getIndividuals(), start, end,
                  nodeWeights, nodeDistributions, edgeWeights);
    for (auto &p : nodeWeights) {
        if (!current.highlight->hasNode(p.first)) {
            assert(graph.isElement(p.first));
            if (nodeDistributions.empty())
                current.highlight->addNode(p.first, p.second, Distribution());
            else
                current.highlight->addNode(p.first, p.second, nodeDistributions[p.first]);
        }
    }
    for (auto &p : edgeWeights) {
        if (!current.highlight->hasEdge(p.first)) {
            assert(graph.isElement(p.first));
            // Ensuring end nodes are included in insertion bundle if not existing
            const auto &ends = graph.ends(p.first);
            assert(current.highlight->hasNode(ends.first) || nodeWeights.find(ends.first) != nodeWeights.end());
            assert(current.highlight->hasNode(ends.second) || nodeWeights.find(ends.second) != nodeWeights.end());
            current.highlight->addEdge(p.first, p.second);
        }
    }
}

void LocalData::triggerHighlight(const std::vector<SelectedElement> &elts) {
    Individuals selectionIndividuals;
    for (const SelectedElement &elt : elts) {
        if (elt.n.isValid()) {
            const Individuals clusterIndividuals = getClusterIndividuals(nodeProperties[elt.n]);
            std::copy(clusterIndividuals.begin(), clusterIndividuals.end(), std::back_inserter(selectionIndividuals));
        } else if (elt.e.isValid()) {
            const Ends ends = getEnds(elt.e);
            Individuals srcIndividuals = getClusterIndividuals(nodeProperties[ends.source]);
            Individuals dstIndividuals = getClusterIndividuals(nodeProperties[ends.dest]);
            Individuals edgeIndividuals = individualsIntersection(srcIndividuals, dstIndividuals);
            std::copy(edgeIndividuals.begin(), edgeIndividuals.end(), std::back_inserter(selectionIndividuals));
            // For now list of selected elements are only edges belonging to the same axis interval, and as such bear a null intersection
        }
    }

    Individuals individuals;
    if (current.highlight == nullptr || currentOp == HighlightingOp::replace) {
        individuals = selectionIndividuals;
    } else if (currentOp == HighlightingOp::substract) {
        Individuals previous = current.highlight->getIndividuals();
        individuals = individualsDifference(previous, selectionIndividuals);
    } else if (currentOp == HighlightingOp::add) {
        Individuals previous = current.highlight->getIndividuals();
        individuals = individualsUnion(previous, selectionIndividuals);
    } else if (currentOp == HighlightingOp::intersect) {
        Individuals previous = current.highlight->getIndividuals();
        individuals = individualsIntersection(previous, selectionIndividuals);
    }
    clearHighlight();
    current.highlight = new Highlight(this, SelectedElement(), individuals);
    buildHighlight(current.highlight);
    if (observer_ != nullptr)
        observer_->onHighlight(current.highlight);
}


void LocalData::triggerHighlight(const vector<ClusterInterval> &clusterIntervals) {
    Individuals individuals;
    for (unsigned int i = 0; i < nbSourceIndividuals; i++)
        individuals.push_back(i);
    for (const ClusterInterval &interval : clusterIntervals) {
        unsigned int dbDim = get<0>(interval);
        Individuals selected;
        for (cluster id = get<1>(interval); id <= get<2>(interval); id++) {
            const Individuals &clusterI = getClusterIndividuals(nodeProperties[getNodeFromDbDim(dbDim, id)]);
            selected.insert(selected.end(), clusterI.begin(), clusterI.end());
        }
        if (selected.size() != getNbIndividuals()) {
            individuals = individualsIntersection(selected, individuals);
        }
    }
    clearHighlight();
    current.highlight = new Highlight(this, SelectedElement(), individuals);
    buildHighlight(current.highlight);
    if (observer_ != nullptr)
        observer_->onHighlight(current.highlight);
}

void LocalData::clearHighlightPreview() {
    if (preview != nullptr) {
        delete preview;
        preview = nullptr;
    }
    if (observer_ != nullptr)
        observer_->onHighlight(current.highlight);
}


void LocalData::saveHighlightPreview() {
    assert(observer_ != nullptr);
    clearHighlight();
    if (preview != nullptr) {
        current.highlight = preview;
        preview = nullptr;
        if (observer_ != nullptr)
            observer_->onHighlight(current.highlight);
    }
}

const Individuals &LocalData::getHighlightedIndividuals() const {
    return current.highlight->getIndividuals();
}

void LocalData::setCurrentHighlightOp(HighlightingOp op) {
    currentOp = op;
}

HighlightingOp LocalData::getCurrentHighlightOp() {
    return currentOp;
}

bool LocalData::isEnabledMultiThreading() const {
    return enabledMultiThreading;
}

void LocalData::setEnabledMultiThreading(bool b) {
    enabledMultiThreading = b;
}

void LocalData::exportData(ostream *os,
                           bool highlightedOnly,
                           bool includeDeletedDimensions) const {
    Individuals in;
    vector<unsigned int> dimensions = toDbDimensions.asVector();
    if (highlightedOnly) in = getHighlightedIndividuals();
    else in = Individuals();
    if (includeDeletedDimensions) {
        vector<unsigned int> sortedDims(dimensions);
        sort(sortedDims.begin(), sortedDims.end());
        if (nbSourceDimensions > dimensions.size()) {
            unsigned int previousDim = 0;
            // Dimensions in-between displayed
            for (unsigned int current : sortedDims) {
                for (unsigned inter = previousDim + 1; inter <= current; ++inter)
                    dimensions.push_back(inter);
                previousDim = current;
            }
            previousDim++;
            // Dimensions greater than bigger displayed
            while (previousDim < nbSourceDimensions) {
                dimensions.push_back(previousDim);
                previousDim++;
            }
        }
    }
    provider->exportCSV(os, in, dimensions);
}

void LocalData::createDeletionElements(const AxisInterval &axes) {
    DataHandler::restitchAfterDeletion(axes, [this](unsigned int left, unsigned int right) {
        this->createEdges(left, right);
    });
}

void LocalData::createInsertionElements(const DimensionInterval &dbDims,
                                        int justAfterAxisId) {
    assert(justAfterAxisId >= -1);
    auto axisId = (unsigned int) (justAfterAxisId + 1);
    for (unsigned int dbDim : dbDims) {
        // Add new nodes to graph (+ update nodeProperties)
        createNodes(dbDim, axisId);
        // Adding edges on left side of newly created axis
        if (axisId > 0) createEdges(axisId - 1, axisId);
        if (axesNodes.at(axisId).empty()) throw InvalidStateError();
        axisId++;
    }
    // Add final edges on right side if needed
    if (axisId < getNbAxes()) createEdges(axisId - 1, axisId);
}

void LocalData::createEdges(unsigned int leftAxisId,
                            unsigned int rightAxisId) {
    assert(rightAxisId <= getLastAxisId());
    unsigned int lD = toDbDimensions.at(leftAxisId);
    unsigned int rD = toDbDimensions.at(rightAxisId);
    Dimension &lN = axesNodes.at(leftAxisId);
    Dimension &rN = axesNodes.at(rightAxisId);
    if (rN.empty() || lN.empty()) throw InvalidStateError();
    unordered_set<edge> preComputed = getEdges(leftAxisId);

    if (enabledMultiThreading) {
        typedef std::unordered_map<std::pair<cluster, cluster>, unsigned int, pairhash> Result;
#ifdef _OPENMP
#pragma omp parallel
        {
            Result results;
#pragma omp for schedule(static)
            for (unsigned int i = 0; i < nbSourceIndividuals; i++) {
                cluster iLeftCluster = getIndividualCluster(i, lD);
                cluster iRightCluster = getIndividualCluster(i, rD);
                assert(iLeftCluster != NodeProp::INVALID_ID);
                assert(iRightCluster != NodeProp::INVALID_ID);
                auto pair = make_pair(iLeftCluster, iRightCluster);
                if (results.find(pair) == results.end())
                    results.insert({pair, 1});
                else results.at(pair)++;
            }
#pragma omp critical
            {
                for (auto &k : results) {
                    if (lN.find(k.first.first) != lN.end() &&
                        rN.find(k.first.second) != rN.end()) {
                        node &leftNode = lN[k.first.first];
                        node &rightNode = rN[k.first.second];
                        edge e = graph.existEdge(leftNode, rightNode, false);
                        if (preComputed.find(e) == preComputed.end()) {
                            if (!e.isValid()) {
                                edge newEdge = DataHandler::addEdge(leftNode, rightNode);
                                edgeProperties[newEdge].weight = k.second;
                            } else {
                                edgeProperties[e].weight += k.second;
                            }
                        }
                    }
                }
            }
        }
#else
        Result results;
        multiThreadingWrapper<unsigned int, Result>
                (nbSourceIndividuals,
                 [](int i) {
                     return i;
                 },
                 [this, lD, rD]
                         (const unsigned int &, int i, Result &threadResults) {
                     cluster iLeftCluster = getIndividualCluster(i, lD);
                     cluster iRightCluster = getIndividualCluster(i, rD);
                     assert(iLeftCluster != NodeProp::INVALID_ID);
                     assert(iRightCluster != NodeProp::INVALID_ID);
                     auto pair = make_pair(iLeftCluster, iRightCluster);
                     if (threadResults.find(pair) == threadResults.end())
                         threadResults.insert({pair, 1});
                     else threadResults.at(pair)++;
                 },
                 [this, &lN, &rN, &preComputed](const Result &threadResults, Result &) {
                     for (auto &k : threadResults) {
                         if (lN.find(k.first.first) != lN.end() &&
                             rN.find(k.first.second) != rN.end()) {
                             node &leftNode = lN[k.first.first];
                             node &rightNode = rN[k.first.second];
                             edge e = graph.existEdge(leftNode, rightNode, false);
                             if (preComputed.find(e) == preComputed.end()) {
                                 if (!e.isValid()) {
                                     edge newEdge = DataHandler::addEdge(leftNode, rightNode);
                                     edgeProperties[newEdge].weight = k.second;
                                 } else {
                                     edgeProperties[e].weight += k.second;
                                 }
                             }
                         }
                     }
                 },
                 results);
#endif
    } else {
        for (unsigned int i = 0; i < nbSourceIndividuals; i++) {
            cluster iLeftCluster = getIndividualCluster(i, lD);
            cluster iRightCluster = getIndividualCluster(i, rD);
            assert(iLeftCluster != NodeProp::INVALID_ID);
            assert(iRightCluster != NodeProp::INVALID_ID);
            if (lN.find(iLeftCluster) == lN.end()) throw InvalidStateError();
            if (rN.find(iRightCluster) == rN.end()) throw InvalidStateError();

            node &leftNode = lN[iLeftCluster];
            node &rightNode = rN[iRightCluster];
            edge e = graph.existEdge(leftNode, rightNode, false);
            if (preComputed.find(e) == preComputed.end()) {
                if (!e.isValid()) {
                    edge newEdge = DataHandler::addEdge(leftNode, rightNode);
                    edgeProperties[newEdge].weight = 1;
                } else { edgeProperties[e].weight++; }
            }
        }
    }
}


void LocalData::createNodes(unsigned int dbDim,
                            unsigned int axisId) {

    // Focus nodes
#ifdef HIERARCHICAL
    dimensionParentNode.at(dbDim)->applyChildren([this, &dbDim, &axisId](const ClusterProp &cp) {
        node clusterNode = DataHandler::addNode();
        NodeProp np = NodeProp(cp, axisId, dbDim);
        nodeProperties[clusterNode] = np;
        assert(axesNodes.at(axisId).find(cp.clusterId) == axesNodes.at(axisId).end());
        axesNodes.at(axisId).insert({cp.clusterId, clusterNode});
        // Other values will be computed in positionNodes()
    });
    // Context nodes
    const ClusterIdentifier &parent = current.dimensionParents.at(dbDim);
    createContextElements(dbDim, axisId, parent);
    assert(!axesNodes.at(axisId).empty());
#else
    clustering->forEachCluster(dbDim, ClusterIdentifier(), [this, &dbDim, &axisId](const ClusterProp &cp) {
        node clusterNode = DataHandler::addNode();
        NodeProp np = NodeProp(cp, axisId, dbDim);
        nodeProperties[clusterNode] = np;
        assert(axesNodes.at(axisId).find(cp.clusterId) == axesNodes.at(axisId).end());
        axesNodes.at(axisId).insert({cp.clusterId, clusterNode});
        // Other values will be computed in positionNodes()
    });
#endif
}

void LocalData::fillBucket(unsigned int i,
                           unsigned int dbDim,
                           Distribution &d,
                           const ClusterProp *cp) {
    assert(d.size() == 10 || d.size() == 1);
    float value = provider->getValue(i, dbDim);
    fillDistribution(value, d, cp->min, cp->max);
}

cluster LocalData::getIndividualCluster(unsigned int i,
                                        unsigned int dbDim) const {
#ifndef HIERARCHICAL
    return clustering->getCluster(dbDim, i, ClusterIdentifier());
#else
    float value = provider->getValue(i, dbDim);
    const HNode *currentParentNode = dimensionParentNode.at(dbDim);
    cluster id = ClusterProp::INVALID_ID;
    if (value > currentParentNode->getMax()) id = ClusterProp::HIGHER_META_NODE_ID;
    else if (value < currentParentNode->getMin()) id = ClusterProp::LOWER_META_NODE_ID;
    else id = currentParentNode->findInChildren(value);
    assert(id != ClusterProp::INVALID_ID);
    assert(id == ClusterProp::HIGHER_META_NODE_ID ||
           id == ClusterProp::LOWER_META_NODE_ID ||
           axesNodes.at(toAxis(dbDim)).find(id) != axesNodes.at(toAxis(dbDim)).end());
    return id;
#endif
}

Individuals LocalData::getClusterIndividuals(const NodeProp &np) const {
#ifndef HIERARCHICAL
    return clustering->getIndividuals(np.dbDimension, np.clusterId);
#else
    assert(np.dbDimension < nbSourceDimensions);
    return provider->getInner(np.dbDimension, np.min, np.max);
#endif
}

void LocalData::buildSubGraph(const Individuals &individuals,
                              unsigned int from,
                              unsigned int to,
                              unordered_map<node, unsigned int> &nodeWeights,
                              unordered_map<node, Distribution> &nodeDistributions,
                              unordered_map<edge, unsigned int> &edgeWeights) {
    assert(from < getNbAxes() && to < getNbAxes());
    if (individuals.empty()) return;
    // For each axis, retrieve cluster node filtered for selected individuals
    for (unsigned int axisId = from; axisId <= to; ++axisId) {
        unsigned int dbDim = toDbDimensions.at(axisId);
        const Dimension &dimNodes = axesNodes.at(axisId);
        const Dimension *prevDimNodes = nullptr;
        unsigned int pDbDim = 0;
        if (axisId > from) {
            prevDimNodes = &axesNodes.at(axisId - 1);
            pDbDim = toDbDimensions.at(axisId - 1);
        }
        // For each selected element, fetch cluster nodes and shared edges
        for (unsigned int i : individuals) {
            cluster clusterId = getIndividualCluster(i, dbDim);
            const node &clusterNode = dimNodes.at(clusterId);
            // Add node to subGraph nodes if not present, else increment weight
            if (nodeWeights.find(clusterNode) == nodeWeights.end())
                nodeWeights.insert(std::make_pair(clusterNode, 1));
            else nodeWeights.at(clusterNode)++;
#ifndef NO_DISTRIBUTIONS
            if (nodeDistributions.find(clusterNode) == nodeDistributions.end())
                nodeDistributions.insert({clusterNode, Distribution(10, 0)});
            if (clusterId != ClusterProp::HIGHER_META_NODE_ID &&
                clusterId != ClusterProp::LOWER_META_NODE_ID) {
                const ClusterProp *cp = &nodeProperties[clusterNode];
                fillBucket(i,
                           dbDim,
                           nodeDistributions.at(clusterNode),
                           cp);
            }
#endif
            // Find edge shared by this dimension cluster of i and the previous one
            if (axisId > from) {
                cluster iPrevClusterId = getIndividualCluster(i, pDbDim);
                const node &prevClusterNode = prevDimNodes->at(iPrevClusterId);
                const edge &edge = graph.existEdge(clusterNode, prevClusterNode, false);
                if (edge.isValid()) {
                    if (edgeWeights.find(edge) == edgeWeights.end())
                        edgeWeights.insert(std::make_pair(edge, 1));
                    else edgeWeights[edge]++;
                } else {
                    throw InvalidStateError();
                }
            }
        }
    }
}


void threadRoutine(const LocalData *data,
                   unsigned int from, unsigned int to,
                   std::unordered_map<edge, unsigned int> &edgeWeights,
                   const Individuals &individuals,
                   int i, int j) {
    for (unsigned int axisId = from; axisId <= to; ++axisId) {
        unsigned int dbDim = data->toDimension(axisId);
        const Dimension &dimNodes = data->getDimension(axisId);
        const Dimension *pNodes = nullptr;
        unsigned int pDbDim = 0;
        if (axisId > from) {
            pNodes = &data->getDimension(axisId - 1);
            pDbDim = data->toDimension(axisId - 1);
        }
        // For each selected element, fetch cluster nodes and shared edges
        for (int k = i; k < j; k++) {
            unsigned int id = individuals.at(k);
            cluster thisCluster = data->getIndividualCluster(id, dbDim);
            const node &thisNode = dimNodes.at(thisCluster);
            // Find edge shared by this dimension cluster of i and the previous one
            if (axisId > from && pNodes != nullptr) {
                cluster pCluster = data->getIndividualCluster(id, pDbDim);
                const node &pNode = pNodes->at(pCluster);
                try {
                    edge e = data->graph.existEdge(thisNode, pNode, false);
                    if (e.isValid()) {
                        if (edgeWeights.find(e) == edgeWeights.end()) {
                            edgeWeights.insert({e, 1});
                        } else {
                            edgeWeights.at(e)++;
                        }
                    }
                } catch (const QueryError &e) {

                }
            }
        }
    }
}


void LocalData::buildSubGraphMT(const Individuals &individuals,
                                unsigned int from,
                                unsigned int to,
                                vector<unordered_map<EdgeKey, unsigned int, edgekeyhash>> &edgeWeights,
                                vector<unordered_map<cluster, unsigned int>> &nodeWeights,
                                vector<unordered_map<cluster, Distribution>> &nodeDistributions) {
    assert(from < getNbAxes() && to < getNbAxes());
    if (individuals.empty()) return;
#ifdef _OPENMP
    int nbThreads = omp_get_max_threads();
    typedef std::unordered_map<EdgeKey, unsigned int, edgekeyhash> EdgeMap;
    std::vector<std::vector<EdgeMap>> threadResults(nbThreads, std::vector<EdgeMap>(to - from + 1));
#pragma omp parallel for schedule(static)
    for (unsigned int t = 0; t < individuals.size(); t++) {
        unsigned int id = individuals[t];
        int threadId = omp_get_thread_num();
        auto &thisThreadMap = threadResults[threadId];
        cluster pCluster = ClusterProp::INVALID_ID;
        for (unsigned int axisId = from; axisId <= to; ++axisId) {
            unsigned int dbDim = toDimension(axisId);
            cluster thisCluster = getIndividualCluster(id, dbDim);
            // Find edge shared by this dimension cluster of i and the previous one
            if (axisId > from) {
                unsigned int leftAxisId = axisId - from - 1;
                assert(pCluster != ClusterProp::INVALID_ID);
                EdgeKey e = {leftAxisId, pCluster, thisCluster};
                auto &axisEdges = thisThreadMap[leftAxisId];
                auto atKey = axisEdges.find(e);
                if (atKey == axisEdges.end()) axisEdges.insert({e, 1});
                else atKey->second++;
            }
            pCluster = thisCluster;
        }
    }
#pragma omp parallel for schedule(static)
    for (unsigned int d = 0; d < threadResults[0].size(); d++) {
        auto &dimEdges = edgeWeights[d];
        auto &dimNodes = nodeWeights[d];
        for (int t = 0; t < nbThreads; t++) {
            for (auto &keyValue : threadResults[t][d]) {
                auto atEdge = dimEdges.find(keyValue.first);
                if (atEdge == dimEdges.end()) dimEdges.insert(keyValue);
                else atEdge->second += keyValue.second;
                const cluster &cluster1 = keyValue.first.leftId;
                assert(d == keyValue.first.leftAxisId);
                auto atCluster = dimNodes.find(cluster1);
                if (atCluster != dimNodes.end()) atCluster->second += keyValue.second;
                else dimNodes.insert(std::make_pair(cluster1, keyValue.second));


                if (d == edgeWeights.size() - 1) { // Also insert destination
                    const cluster &cluster2 = keyValue.first.rightId;
                    auto &otherDimNodes = nodeWeights[d + 1];
                    if (otherDimNodes.find(cluster2) != otherDimNodes.end())
                        otherDimNodes[cluster2] += keyValue.second;
                    else
                        otherDimNodes.insert(std::make_pair(cluster2, keyValue.second));
                }
            }
        }
    }
#endif
    //TODO distribution should be included in parallel process
    if (nodeDistributions.empty()) return;
    for (unsigned int axisId = from; axisId <= to; ++axisId) {
        unsigned int dbDim = toDbDimensions.at(axisId);
        const Dimension &dimNodes = axesNodes.at(axisId);
        // For each selected element, fetch cluster nodes and shared edges
        for (unsigned int i : individuals) {
            cluster clusterId = getIndividualCluster(i, dbDim);
            const node &clusterNode = dimNodes.at(clusterId);
            if (nodeDistributions[axisId].find(clusterId) == nodeDistributions[axisId].end())
                nodeDistributions[axisId].insert({clusterId, Distribution(10, 0)});
            /* if (clusterId != ClusterProp::HIGHER_META_NODE_ID &&
                 clusterId != ClusterProp::LOWER_META_NODE_ID)*/
            assert(nodeDistributions[axisId][clusterId].size() > 0);
            fillBucket(i,
                       dbDim,
                       nodeDistributions[axisId][clusterId],
                       &nodeProperties[clusterNode]);
        }
    }
}


string LocalData::getLabel(unsigned int dbDim, int id) const {
    return provider->getClusterLabel(dbDim, id);
}

void LocalData::benchFocus(const node &n) {
    const NodeProp np = nodeProperties[n];
    current.dimensionParents.at(np.dbDimension).chain(np.clusterId);
    current.dimensionParents.at(np.dbDimension).chain(np.clusterId);
#ifdef HIERARCHICAL
    const HNode *currentNode = dimensionParentNode.at(np.dbDimension);
    dimensionParentNode.at(np.dbDimension) = currentNode->getChild(np.clusterId);
#endif
    deleteAxisElements(np.dimension);
    doCreateOpeningElements(np.dimension, np.clusterId, current.dimensionParents.at(np.dbDimension));
}

void LocalData::benchFocusOut(unsigned int axisId) {
    unsigned int dbDim = toDbDimensions.at(axisId);
    ClusterIdentifier newParent = current.dimensionParents.at(dbDim);
#ifdef HIERARCHICAL
    const HNode *currentNode = dimensionParentNode.at(dbDim);
    dimensionParentNode.at(dbDim) = currentNode->getParent();
    assert(dimensionParentNode.at(dbDim) != nullptr);
#endif
    newParent = ClusterIdentifier::getParent(newParent);
    current.dimensionParents.at(dbDim) = newParent;
    deleteAxisElements(axisId);
    doCreateClosingElements(axisId, 1);
}


void LocalData::prepareHighlight(HighlightingOp op) {
    if ((current.highlight != nullptr || preview != nullptr) && op != HighlightingOp::replace) {
        saveHighlightPreview();
    }
    setCurrentHighlightOp(op);
};

void LocalData::saveHighlight() {
    if (current.highlight != nullptr)
        toBeRestored = current.highlight->getIndividuals();
}


void LocalData::restoreHighlight() {
    assert(current.highlight == nullptr);
    if (toBeRestored.empty()) return;
    current.highlight = new Highlight(this, SelectedElement(), toBeRestored);
    buildHighlight(current.highlight);
    toBeRestored.clear();
    if (observer_ != nullptr)
        observer_->onHighlight(current.highlight);
}