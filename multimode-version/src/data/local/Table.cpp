#include "data/Table.h"
#include <iostream>
#include <chrono>

#include "util/Utils.h"
#ifdef _OPENMP
#include <omp.h>
#endif

struct Comp {
    bool operator()(const float l, const std::pair<float, Individuals> &r) {
        return l < r.first;
    }

    bool operator()(const std::pair<float, Individuals> &l, const float r) {
        return l.first < r;
    }

};


Table::Table(const std::vector<std::vector<float>> &table) : knownNbIndividuals(true) {
    this->table = table;
}

Table::Table(unsigned int d, int n) : knownNbIndividuals(true) {
    table.resize(d);
    knownNbIndividuals = n > 0;
    if (knownNbIndividuals) {
        for (auto &i : table)
            i.resize(n);
    }
}

void Table::insertValue(unsigned int d, unsigned int i, float value) {
    assert(table.size() > d);
    if (knownNbIndividuals) {
        assert(table[d].size() > i);
        table[d][i] = value;
    } else table[d].push_back(value);

}

void Table::computeReverseIndex() {
    for (auto &d : table)
        d.shrink_to_fit();
    valueToRow.resize(table.size());
#ifdef _OPENMP
#pragma omp parallel for schedule(static)
#endif
    for (unsigned int d = 0; d < table.size(); d++) {
        std::vector<std::pair<float, unsigned int>> vTi;
        const auto &iTv = table.at(d);
        vTi.reserve(iTv.size());
        for (unsigned int i = 0; i < table.at(0).size(); i++) {
            float v = iTv.at(i);
            vTi.emplace_back(v, i);
        }
        std::sort(vTi.begin(), vTi.end());

        auto &asSet = valueToRow.at(d);
        asSet.reserve(vTi.size());
        asSet.emplace_back(vTi.begin()->first, Individuals(1, vTi.begin()->second));
        for (unsigned int i = 1; i < vTi.size(); i++) {
            if (std::abs(vTi.at(i).first - asSet.back().first) < std::numeric_limits<float>::epsilon()) {
                asSet.back().second.push_back(vTi.at(i).second);
            } else {
                asSet.emplace_back(vTi.at(i).first, Individuals(1, vTi.at(i).second));
            }
        }
        asSet.shrink_to_fit();
    }
}

Individuals Table::getInBetween(unsigned int d,
                                float min,
                                float max) const {
    assert(!valueToRow.empty());
    assert(d < valueToRow.size());
    Individuals result;
    const auto &vTi = valueToRow.at(d);
    const auto itBegin = std::lower_bound(vTi.begin(), vTi.end(), min, Comp());
    const auto itEnd = std::upper_bound(vTi.begin(), vTi.end(), max, Comp());
#ifdef _OPENMP
    unsigned int startI = itBegin - vTi.begin();
    unsigned int endI = itEnd - vTi.begin();
#pragma omp parallel
    {
        Individuals privateResult;
        privateResult.reserve(endI - startI);
#pragma omp for schedule(guided, 10000)
        for (unsigned int i = startI; i < endI; i++) {
            privateResult.insert(privateResult.end(), vTi.at(i).second.begin(), vTi.at(i).second.end());
        }
#pragma omp critical
        result.insert(result.end(), privateResult.begin(), privateResult.end());
    }
#else
    for (auto it = itBegin; it != itEnd; it++) {
        result.insert(result.end(), (*it).second.begin(), (*it).second.end());
    }
#endif
    return result;
}

unsigned int Table::countOver(unsigned int d, float v) const {
    const auto &vTi = valueToRow.at(d);
    const auto itBegin = std::upper_bound(vTi.begin(), vTi.end(), v, Comp());
    const auto itEnd = vTi.end();
    unsigned int startI = itBegin - vTi.begin();
    unsigned int endI = itEnd - vTi.begin();
    unsigned int count = 0;
#ifdef _OPENMP
#pragma omp parallel for schedule(guided,1000), reduction(+:count)
    for (unsigned int i = startI; i < endI; i++) {
        count += vTi.at(i).second.size();
    }
#else
    for (auto it = itBegin; it != itEnd; it++) {
        count += (*it).second.size();
    }
#endif
    return count;
}

unsigned int Table::countUnder(unsigned int d, float v) const {
    const auto &vTi = valueToRow.at(d);
    const auto itBegin = vTi.begin();
    const auto itEnd = std::lower_bound(vTi.begin(), vTi.end(), v, Comp());
    unsigned int startI = itBegin - vTi.begin();
    unsigned int endI = itEnd - vTi.begin();
    unsigned int count = 0;
#ifdef _OPENMP
#pragma omp parallel for schedule(guided,1000), reduction(+:count)
    for (unsigned int i = startI; i < endI; i++) {
        count += vTi.at(i).second.size();
    }
#else
    for (auto it = itBegin; it != itEnd; it++) {
        count += (*it).second.size();
    }
#endif
    return count;
}

float Table::getMinUpper(unsigned int d, float v) const {
    return std::upper_bound(valueToRow.at(d).begin(), valueToRow.at(d).end(), v, Comp())->first;
}

float Table::getMaxLower(unsigned int d, float v) const {
    return std::lower_bound(valueToRow.at(d).begin(), valueToRow.at(d).end(), v, Comp())->first;
}


unsigned int Table::countBetween(unsigned int d, float min, float max) const {

    const auto &vTi = valueToRow.at(d);
    const auto itBegin = std::lower_bound(vTi.begin(), vTi.end(), min, Comp());
    const auto itEnd = std::upper_bound(vTi.begin(), vTi.end(), max, Comp());
    unsigned int startI = itBegin - vTi.begin();
    unsigned int endI = itEnd - vTi.begin();
    unsigned int count = 0;
#ifdef _OPENMP
#pragma omp parallel for schedule(guided,10000), reduction(+:count)
    for (unsigned int i = startI; i < endI; i++) {
        count += vTi.at(i).second.size();
    }
#else
    for (auto it = itBegin; it != itEnd; it++) {
        count += (*it).second.size();
    }
#endif
    return count;
}