#include "data/FileHierarchicalData.h"

ClusterIdentifier parse(const std::vector<std::string> &tokens, int from) {
    ClusterIdentifier ci;
    try {
        for (unsigned int i = from; i < tokens.size(); i++) {
            ci.chain(std::stoi(tokens.at(i)));
        }
    } catch (std::invalid_argument &e) {
        std::cerr << "Could not convert token to number" << std::endl;
    }
    return ci;
}

void FileHierarchicalData::readLine(const std::string &line, TableHandler *valueProvider) {
    std::vector<std::string> tokens;
    tokenize(line, tokens, SEPARATOR);
    if (tokens.empty()) return; // Dropped a line
    try {
        int dim = std::stoi(tokens.at(0));
        assert(dim >= 0);
        if (dim >= dimensionClustering.size())
            resizeVectors(dim + 1);
        float min = std::stof(tokens.at(1));
        float max = std::stof(tokens.at(2));
        assert(isfinite(min) && isfinite(max));
        ClusterIdentifier ci = parse(tokens, 3);
        HNode *node = insertNode(dim, ci);
        assert(node != nullptr);
        // Add min and max values to actual node inserted
        node->min = min;
        node->max = max;
        node->weight = valueProvider->countInner(dim, min, max);
        // Update dimension extrema
        dimensionExtrema.at(dim).update(min);
        dimensionExtrema.at(dim).update(max);
    } catch (std::invalid_argument &e) {
        std::cerr << "Could not convert token to number" << std::endl;
    }
}

HNode *FileHierarchicalData::insertNode(unsigned int dbDim,
                                        const ClusterIdentifier &ci) {
    assert(dimensionClustering.size() > dbDim);
    HNode *lastParent = &dimensionClustering.at(dbDim);
    assert(lastParent != nullptr);
    for (cluster parentId  : ci) {
        assert(lastParent != nullptr);
        unsigned int ipos = lastParent->getChildPos(parentId);
        auto &lpChildren = lastParent->getChildren();
        if (lpChildren.size() > ipos && lpChildren.at(ipos).getId() == parentId) {
            lastParent = &lpChildren.at(ipos);
        } else {
            lastParent = lastParent->insertChild(parentId, ipos);
            assert(lastParent != nullptr);
        }
    }
    return lastParent;
}

FileHierarchicalData::FileHierarchicalData(std::istream &in,
                                           TableHandler *valueProvider,
                                           unsigned int k) {
    std::string line;
    unsigned int nbLines = 0;
    while (std::getline(in, line)) {
        if (!line.empty()) {
            nbLines++;
            readLine(line, valueProvider);
        }
    }
    std::cout << "Finished reading hierarchy file" << std::endl;

    // Update abstract nodes
    for (HNode &n : dimensionClustering) {
        n.min = std::numeric_limits<float>::infinity();
        n.max = -std::numeric_limits<float>::infinity();
        n.weight = 0;
        n.applyChildren([&n](const HNode &cn) {
            n.min = std::min(cn.min, n.min);
            n.max = std::max(cn.max, n.max);
            n.weight += cn.weight;
        });
        n.reLink();
    }

    // Check validity of all HNode + abstract nodes
#ifndef NDEBUG
    unsigned int nbNodes = 0;
    unsigned int nbDim = 0;
    for (const HNode &n : dimensionClustering) {
        assert(n.isValid(k, -1));
        nbNodes += n.getNbDescendants() - 1;
        nbDim++;
    }
    assert(nbLines == nbNodes);
#endif
}