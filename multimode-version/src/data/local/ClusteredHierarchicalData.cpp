#include "data/ClusteredHierarchicalData.h"
#include <clustering/NoClustering.h>

using namespace std;

ClusteredHierarchicalData::ClusteredHierarchicalData(bool enabledMultiThreading,
                                                     HClustering *clustering,
                                                     const std::set<unsigned int> &noClustering,
                                                     const std::vector<std::vector<float>> &dimValues) {
    const unsigned long nbIndividuals = dimValues.at(0).size();
    const unsigned int nbDimensions = dimValues.size();
    HClustering *none = new NoClustering();
    resizeVectors(nbDimensions);
    if (enabledMultiThreading) {
        vector<struct ThreadArguments> threadArgs(nbDimensions);
        vector<std::thread *> threads(nbDimensions);
        for (unsigned int i = 0; i < nbDimensions; ++i) {
            HClustering *thisDimClustering = noClustering.find(i) == noClustering.end() ? clustering : none;
            threadArgs.at(i) = {thisDimClustering,
                                &dimValues.at(i),
                                nbIndividuals};
            threads[i] = new thread(&createDimension, &threadArgs[i]);
        }
        // Wait for each thread to complete
        for (unsigned int i = 0; i < nbDimensions; ++i) {
            threads[i]->join();
            fetchThreadResult(i, threadArgs.at(i));
        }
        for (unsigned int i = 0; i < nbDimensions; ++i) {
            delete threads[i];
        }
    } else {
        for (unsigned int i = 0; i < nbDimensions; ++i) {
            HClustering *thisDimClustering = noClustering.find(i) == noClustering.end() ? clustering : none;
            struct ThreadArguments args = {
                    thisDimClustering,
                    &dimValues.at(i),
                    nbIndividuals};
            createDimension(&args);
            fetchThreadResult(i, args);
        }
    }

    assert(dimensionClustering.size() == nbDimensions);
}

void *ClusteredHierarchicalData::createDimension(void *args) {
    struct ThreadArguments &arguments = *(struct ThreadArguments *) args;
    const vector<float> &dimValues = *arguments.dimValues;
    assert(dimValues.size() == arguments.nbIndividuals);
    arguments.clusters = arguments.clustering->bottomUp(dimValues);
    // At initial level?
    arguments.extrema.min = numeric_limits<float>::infinity(); // Extrema would be root node min,max
    arguments.extrema.max = -numeric_limits<float>::infinity();
    for (const auto &initialCluster : arguments.clusters) {
        arguments.extrema.min = min(arguments.extrema.min, initialCluster.getMin());
        arguments.extrema.max = max(arguments.extrema.max, initialCluster.getMax());
    }
    return NULL;
}

void ClusteredHierarchicalData::fetchThreadResult(unsigned int i,
                                                  const ThreadArguments &args) {
    dimensionClustering.at(i) = HNode(args.clusters, HNode::ROOT_NODE_ID);
    dimensionClustering.at(i).reLink();
    dimensionExtrema.at(i) = args.extrema;
}
