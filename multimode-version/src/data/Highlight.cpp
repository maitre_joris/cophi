#include <util/Utils.h>
#include <Error.h>
#include "data/Highlight.h"
#include "data/DataHandler.h"

using namespace std;


Highlight::Highlight(DataHandler *base, SelectedElement e, Individuals &i) :
        origin(e), individuals(i), base(base) {}

Highlight::Highlight(DataHandler *base, SelectedElement e) : origin(e), base(base) {}


Highlight::Highlight() {
    assert(nodes.size() == 0);
}

Highlight::~Highlight() {
    clear();
}

bool Highlight::isAtRoot(unsigned int axisId) const {
    return base->isAtRoot(axisId);
}

void Highlight::forEachNode(std::function<void(const node &)> f) const {
    assert(base != nullptr);
    for (const auto &nodePair : nodes)
        f(get<0>(nodePair));
}

void Highlight::forEachNode(unsigned int axisId,
                            std::function<void(const node &)> f) const {
    const Dimension &clusters = base->getDimension(axisId);
    for (const auto &cluster : clusters)
        if (nodes.find(cluster.second) != nodes.end())
            f(cluster.second);
}


void Highlight::forEachEdge(std::function<void(const edge &)> f) const {
    assert(base != nullptr);
    for (const auto &edgePair : edges) {
        // Check consistency
        const auto &ends = base->getEnds(edgePair.first);
        assert(nodes.find(ends.source) != nodes.end());
        assert(nodes.find(ends.dest) != nodes.end());
        f(edgePair.first);
    }
}

unsigned int Highlight::getNbEdges() const {
    return (unsigned int) edges.size();
}

unsigned int Highlight::getNbNodes() const {
    return (unsigned int) nodes.size();
}

float Highlight::getNodeSubWeight(const node &n) const {
    assert(base != nullptr);
    assert(nodes.find(n) != nodes.end());
    const NodeProp &np = base->getNodeProperty(n);
    return (float) nodes.at(n).first / np.weight;
}

float Highlight::getEdgeSubWeight(const edge &e) const {
    assert(base != nullptr);
    assert(edges.find(e) != edges.end());
    const EdgeProp &ep = base->getEdgeProperty(e);
    return static_cast<float>(edges.at(e)) / ep.weight;
}

const Extrema Highlight::getAxisExtrema(unsigned int i) const {
    assert(base != nullptr);
    return base->getAxisExtrema(i);
}

void Highlight::clear() {
    origin.clear();
    nodes.clear();
    edges.clear();
    individuals.clear();
}

bool Highlight::isEmpty() const { return (nodes.empty() || edges.empty()); }


void Highlight::addNode(const node n, unsigned int w, Distribution d) {
    if (nodes.find(n) == nodes.end()) nodes.insert({n, {w, d}});
    else throw BuildError(BuildError::DOUBLE_NODE);
}


void Highlight::addEdge(const edge e, unsigned int w) {
    if (!hasEdgeEnds(e)) throw BuildError(BuildError::UNKNOWN_EDGE);
    if (edges.find(e) == edges.end())
        edges[e] = w;
    else throw BuildError(BuildError::DOUBLE_EDGE);
}

const Ends Highlight::getEnds(const edge &e) const {
    assert(base != nullptr);
    return base->getEnds(e);
}

const vector<edge> Highlight::getStar(const node &n) const {
    assert(base != nullptr);
    vector<edge> result;
    if (nodes.find(n) == nodes.end()) return result;
    vector<edge> baseStar = base->getStar(n);
    // Two possibility here: either (1) transform all edges into a set (nlog(n))
    // (n = edges.size()) and use find on sorted set at cost log(n)
    // or (2) use find on existing vector at cost n.
    // With s being the size of the star, n the size of edges:
    //  (1) (n+s)log(n)
    //  (2) ns
    for (const edge &e : baseStar)
        if (edges.find(e) != edges.end())
            result.push_back(e);
    return result;
}

unsigned int Highlight::getNbIndividuals() const {
    assert(base != nullptr);
    if (origin.isValid()) {
        if (origin.n.isValid()) {
            return base->getNodeProperty(origin.n).weight;
        } else {
            return base->getEdgeProperty(origin.e).weight;
        }
    } else {
        return individuals.size();
    }
}

bool Highlight::addNode(unsigned int dbDim,
                        cluster clusterId,
                        float,
                        float,
                        Distribution distribution) {
    assert(!distribution.empty());
    try {
        const node n = base->getNodeFromDbDim(dbDim, clusterId);
        if (nodes.find(n) == nodes.end()) {
            unsigned int weight = 0;
            for (const auto v : distribution) weight += v;
            nodes.insert({n, make_pair(weight, distribution)});
            return true;
        } else {
            // Bordering nodes on interval response (stitching deletion or adding inserted dimension)
            return true;
        }
    } catch (const DataException &e) {
        return false;
    }
}

bool Highlight::hasEdgeEnds(const edge &e) {
    Ends ends = base->getEnds(e);
    return (nodes.find(ends.source) != nodes.end() && nodes.find(ends.dest) != nodes.end());
}

bool Highlight::addEdge(unsigned int srcDbDimension,
                        unsigned int tgtDbDimension,
                        cluster srcClusterId,
                        cluster tgtClusterId,
                        unsigned int weight) {
    try {
        const edge &e = base->getEdge(srcDbDimension, tgtDbDimension,
                                      srcClusterId, tgtClusterId);
        if (hasEdgeEnds(e)) {
            if (edges.find(e) == edges.end()) {
                edges.insert({e, weight});
                return true;
            } else throw BuildError(BuildError::DOUBLE_EDGE);

        } else throw BuildError(BuildError::UNKNOWN_EDGE);

    }
    catch (const DataException &e) {
        return false;
    }
}

void Highlight::updateDimension(unsigned int,
                                const string &) {
}

void Highlight::forEachEdge(unsigned int leftAxis,
                            std::function<void(const edge &)> f) const {
    base->forEachEdge(leftAxis, [this, &f](const edge &e) {
        if (this->edges.find(e) != this->edges.end())
            f(e);
    });
}

void Highlight::deleteElements(AxisInterval axes) {
    const auto &toBeDeleted = base->getElements(axes, true);
    for (const node &n : toBeDeleted.second)
        nodes.erase(n);
    for (const edge &e : toBeDeleted.first)
        edges.erase(e);
}

void Highlight::deleteEdges(unsigned int justAfterAxisId) {
    for (const edge &e : base->getEdges(justAfterAxisId))
        edges.erase(e);
}

void Highlight::deleteNodes(unsigned int atAxis) {
    base->forEachNode(atAxis, [this](const node &n) {
        if (nodes.find(n) != nodes.end())
            nodes.erase(n);
    });
}

const vector<float> Highlight::getNormalizedDistribution(const node &n) const {
    const NodeProp &np = base->getNodeProperty(n);
    unsigned int normalizer = base->getBiggestBucket(np.dbDimension);
    return DataHandler::createNormalizedDistribution(nodes.at(n).second, normalizer);
}

const Individuals &Highlight::getIndividuals() const {
    return individuals;
}


string Highlight::toString() const {
    stringstream ss;
    ss << "Base: |E| = " << base->getNbEdges() << ", |V| = " << base->getNbNodes() << endl;
    ss << "Highlight: |E| = " << edges.size() << ", |V| = " << nodes.size() << endl;
    return ss.str();
}

bool Highlight::hasNode(const node &n) const {
    return nodes.find(n) != nodes.end();
}

bool Highlight::hasEdge(const edge &e) const {
    return edges.find(e) != edges.end();
}

SelectedElement Highlight::getOrigin() const {
    return origin;
}