#include <data/DataHandler.h>
#include <Error.h>
#include <tlp/ForEach.h>
#include <Notifier.h>

using namespace std;


DataHandler::DataHandler(const DataHandler &rhs) :
        contextCanBeHighlighted(rhs.contextCanBeHighlighted) {
    observer_ = rhs.observer_;
    graph.alloc(nodeProperties);
    graph.alloc(edgeProperties);
    axesNodes.resize(rhs.axesNodes.size());
    nbSourceDimensions = rhs.nbSourceDimensions;
    nbSourceIndividuals = rhs.nbSourceIndividuals;
    toDbDimensions = rhs.toDbDimensions;

    for (const node &n : rhs.graph.nodes()) {
        node newNode = graph.addNode();
        NodeProp np = rhs.nodeProperties[n];
        nodeProperties[newNode] = np;
        axesNodes.at(np.dimension).insert(std::make_pair(np.clusterId, newNode));
    }

    for (const edge &e : rhs.graph.edges()) {
        const auto &ends = rhs.getEnds(e);
        NodeProp tgtNp = rhs.nodeProperties[ends.dest];
        NodeProp srcNp = rhs.nodeProperties[ends.source];
        node tgtNode = axesNodes.at(tgtNp.dimension).at(tgtNp.clusterId);
        node srcNode = axesNodes.at(srcNp.dimension).at(srcNp.clusterId);
        edge newEdge = graph.addEdge(srcNode, tgtNode);
        edgeProperties[newEdge] = rhs.edgeProperties[e];
    }

    current = rhs.current;
    dimensionLabels = rhs.dimensionLabels;
    maxEdgeId = rhs.maxEdgeId;
    maxNodeId = rhs.maxNodeId;
    checkCoherence();
}

DataHandler::~DataHandler() {
    graph.free(nodeProperties);
    graph.free(edgeProperties);
}

DataHandler::DataHandler(DataObserver *obs, bool contextHighlight) :
        observer_(obs), contextCanBeHighlighted(contextHighlight) {
    graph.alloc(nodeProperties);
    graph.alloc(edgeProperties);
}

void DataHandler::createNodeAggregate(cluster id,
                                      unsigned int axisId,
                                      const set<ClusterProp> &nodes,
                                      const pair<Links, Links> &edges) {
    ClusterProp aggregate = ClusterProp(nodes, id);
    node n = addNode();
    unsigned int dbDim = toDbDimensions.at(axisId);
    nodeProperties[n] = NodeProp(aggregate, axisId, dbDim);
    assert(axesNodes.at(axisId).find(id) == axesNodes.at(axisId).end());
    axesNodes.at(axisId).insert(std::make_pair(id, n));
    assert(!axesNodes.at(axisId).empty());

    if (!edges.first.empty()) { // Incoming edges
        const Dimension &dimension = axesNodes.at(axisId - 1);
        assert(!dimension.empty());
        for (const auto &link : edges.first) {
            node otherNode = dimension.at(link.first);
            edge e = addEdge(otherNode, n);
            edgeProperties[e].weight = (unsigned int) link.second;
        }
    }
    if (!edges.second.empty()) { // Outgoing edges
        const Dimension &dimension = axesNodes.at(axisId + 1);
        assert(!dimension.empty());
        for (const auto &link : edges.second) {
            node otherNode = dimension.at(link.first);
            edge e = addEdge(n, otherNode);
            edgeProperties[e].weight = (unsigned int) link.second;
        }
    }
}

unsigned int DataHandler::getLastAxisId() const {
    return getNbAxes() - 1;
}

float DataHandler::getNodeSubWeight(const node &) const {
    return -1.f;
}

float DataHandler::getEdgeSubWeight(const edge &) const {
    return -1.f;
}

const DimensionInterval &DataHandler::getAxesAsDimensions() const {
    return toDbDimensions;
}

const ContextStack &DataHandler::getContextNodeHierarchy(unsigned int axisId, cluster id) const {
    unsigned int dbDim = toDbDimensions.at(axisId);
    if (id == ClusterProp::HIGHER_META_NODE_ID)
        return current.upperContextStacks.at(dbDim);
    if (id == ClusterProp::LOWER_META_NODE_ID)
        return current.lowerContextStacks.at(dbDim);
    throw InvalidStateError();
}

float DataHandler::getContextWeightRatio(unsigned int axisId) const {
    const Dimension &nodes = axesNodes.at(axisId);
    float contextResult = 0;
    if (nodes.find(ClusterProp::HIGHER_META_NODE_ID) != nodes.end())
        contextResult += nodeProperties[nodes.at(ClusterProp::HIGHER_META_NODE_ID)].weight;
    if (nodes.find(ClusterProp::LOWER_META_NODE_ID) != nodes.end())
        contextResult += nodeProperties[nodes.at(ClusterProp::LOWER_META_NODE_ID)].weight;
    return contextResult / nbSourceIndividuals;
}

Extrema DataHandler::getFocusExtrema(unsigned int axisId) const {
    Extrema result = getAxisExtrema(axisId);
    const Dimension &nodes = axesNodes.at(axisId);

    if (nodes.find(ClusterProp::HIGHER_META_NODE_ID) != nodes.end()) {
        result.max = nodeProperties[nodes.at(ClusterProp::HIGHER_META_NODE_ID)].min;
    }
    if (nodes.find(ClusterProp::LOWER_META_NODE_ID) != nodes.end()) {
        result.min = nodeProperties[nodes.at(ClusterProp::LOWER_META_NODE_ID)].max;
    }
    return result;
}

void DataHandler::invertSelection() {

}

bool DataHandler::hasEdge(const edge &e) const {
    return graph.isElement(e);
}

bool DataHandler::hasNode(const node &n) const {
    return graph.isElement(n);
}

Extrema DataHandler::getFocusInnerExtrema(unsigned int axisId) const {
    Extrema result;
    const Dimension &nodes = axesNodes.at(axisId);
    for (const auto &nodePair : nodes) {
        const NodeProp &np = nodeProperties[nodePair.second];
        if (nodePair.first != ClusterProp::HIGHER_META_NODE_ID &&
            nodePair.first != ClusterProp::LOWER_META_NODE_ID) {
            result.max = max(np.max, result.max);
            result.min = min(np.min, result.min);
        }

    }
    return result;
}

LowHighPair<float> DataHandler::getContextIntervalRatio(unsigned int axisId) const {
    const Dimension &nodes = axesNodes.at(axisId);
    float axisIntervalSize = getAxisExtrema(axisId).size();
    float lowContext = 0.f, highContext = 0.f;

    if (nodes.find(ClusterProp::HIGHER_META_NODE_ID) != nodes.end()) {
        const NodeProp &np = nodeProperties[nodes.at(ClusterProp::HIGHER_META_NODE_ID)];
        highContext += abs(np.max - np.min);
    }
    if (nodes.find(ClusterProp::LOWER_META_NODE_ID) != nodes.end()) {
        const NodeProp &np = nodeProperties[nodes.at(ClusterProp::LOWER_META_NODE_ID)];
        lowContext += abs(np.max - np.min);
    }
    return {lowContext / axisIntervalSize, highContext / axisIntervalSize};
}

void DataHandler::deleteDimension(AxisInterval axes) {
    saveState();
    checkCoherence();
    if (axes.getRight() > getLastAxisId())
        throw CanceledOperation(CanceledOperation::INVALID_AXIS_ARGUMENTS);
    if (axes.getLeft() == 0 && axes.getRight() >= getLastAxisId())
        throw CanceledOperation(CanceledOperation::ONE_AXIS_SHOULD_REMAIN);

    // Remove nodes and edges from base and highlight
    bool stitching = false;
    if (current.highlight != nullptr) {
        current.highlight->deleteElements(axes);
        stitching = axes.getLeft() > 0 && axes.getRight() < getLastAxisId();
    }

    for (const auto &axisId : axes)
        deleteNodes(axisId); // Deleting nodes deletes linked edges

    // Update toDbDimensions, axesNodes and dimensionProp sizes
    toDbDimensions.remove(axes.getLeft(), axes.size());
    erase<Dimension>(axesNodes, axes.getLeft(), axes.size());

    createDeletionElements(axes);

    // Shift
    for (unsigned int axisId = axes.getLeft();
         axisId <= getLastAxisId(); axisId++)
        for (const pair<const cluster, node> &clusterPair : axesNodes[axisId])
            nodeProperties[clusterPair.second].dimension = axisId;
    checkCoherence();
    updateDisplayValues(std::max(axes.getLeft(), 0u));

    if (current.highlight != nullptr) {
        if (stitching)
            restitchHighlight(axes.getLeft() - 1, axes.getLeft());
        observer_->onHighlight(current.highlight);
    }
}

void DataHandler::saveState() {

}

void DataHandler::insertAxes(DimensionInterval dbDims,
                             int justAfterAxisId) {
    saveState();
    checkCoherence();
    int previousLastAxisId = getLastAxisId();
    int newNbAxes = getNbAxes() + dbDims.size();
    int firstNewAxisId = justAfterAxisId + 1;

    // Consistency checks
    for (unsigned int dbDim : dbDims)
        for (unsigned int axisId = 0; axisId < getNbAxes(); ++axisId)
            if (toDbDimensions.at(axisId) == dbDim) {
                throw CanceledOperation(CanceledOperation::INVALID_AXIS_ARGUMENTS);
            }
    if (justAfterAxisId < -1 || justAfterAxisId > previousLastAxisId)
        throw CanceledOperation(CanceledOperation::INVALID_AXIS_ARGUMENTS);

    // Deleted edges between justAfterAxisId and its right-side neighbour
    if (justAfterAxisId >= 0 && justAfterAxisId < previousLastAxisId) {
        if (current.highlight != nullptr) current.highlight->deleteEdges(justAfterAxisId); // Merge into deleteEdges()?
        deleteEdges((unsigned int) justAfterAxisId);
    }

    // Make room for the new dimension
    emplace<Dimension>(axesNodes, justAfterAxisId, dbDims.size());
    toDbDimensions.insert(justAfterAxisId, dbDims);
    for (int i = firstNewAxisId; i < firstNewAxisId + dbDims.size(); ++i)
        axesNodes.at((unsigned long) i).clear();

    // Insert in base
    createInsertionElements(dbDims, justAfterAxisId);
    updateDisplayValues((unsigned int) max(justAfterAxisId, 0));

    // Insert in highlight
    if (current.highlight != nullptr) {
        if (justAfterAxisId < 0)
            restitchHighlight(0, dbDims.size() + 1);
        else if (justAfterAxisId == previousLastAxisId)
            restitchHighlight((unsigned int) justAfterAxisId, justAfterAxisId + dbDims.size());
        else
            restitchHighlight((unsigned int) justAfterAxisId, justAfterAxisId + dbDims.size() + 1);
    }

    // Move right axes nodes
    for (int axisId = justAfterAxisId + 1; axisId <= (int) getLastAxisId(); axisId++) {
        for (const pair<const cluster, node> &clusterPair : axesNodes[axisId])
            nodeProperties[clusterPair.second].dimension = (unsigned int) axisId;
    }
    //checkCoherence();

    if (current.highlight != nullptr)
        observer_->onHighlight(current.highlight);


}

void DataHandler::prepareHighlight(HighlightingOp) {

}


void DataHandler::applyHighlight(SelectedElement elt) {
    if (current.highlight != nullptr && current.highlight->getOrigin() == elt) return;
    if (elt.n.isValid()) triggerHighlight(elt.n);
    else if (elt.e.isValid()) triggerHighlight(elt.e);
    else throw InvalidStateError();
}

void DataHandler::applyHighlight(std::vector<SelectedElement> &elts) {
    for (const SelectedElement &e : elts)
        if (!e.isValid()) throw InvalidStateError();
    triggerHighlight(elts);
}

string DataHandler::getAxisLabel(unsigned int axisId) const {
    if (dimensionLabels.empty()) return "";
    return dimensionLabels.at(toDbDimensions.at(axisId));
}

string DataHandler::getDimensionLabel(unsigned int dbDim) const {
    if (dimensionLabels.empty()) return "";
    return dimensionLabels.at(dbDim);
}

void DataHandler::forEachEdge(unsigned int leftAxisId,
                              std::function<void(const edge &)> f) const {
    if (leftAxisId == getLastAxisId()) return;
    for (const edge &e : getEdges(leftAxisId))
        f(e);
}

Extrema DataHandler::getGlobalExtrema() const {
    Extrema global = getAxisExtrema(0);
    for (unsigned int axisId = 0; axisId < getNbAxes(); ++axisId) {
        Extrema e = getAxisExtrema(axisId);
        global.min = min(e.min, global.min);
        global.max = max(e.max, global.max);
    }
    return global;
}

void DataHandler::restitchAfterDeletion(const AxisInterval &axes,
                                        function<void(unsigned int, unsigned int)> createEdges) {
    if (axes.getLeft() > 0 && axes.getLeft() < getNbAxes())
        createEdges(axes.getLeft() - 1, axes.getLeft());
    if (axes.getLeft() > 0)
        positionEdges(axes.getLeft() - 1);
    if (axes.getLeft() < getNbAxes() - 1)
        positionEdges(axes.getLeft());
    //sortEdges(); // Useless?
}

void DataHandler::updateDisplayValues(unsigned int fromAxis) {
    unsigned long nbAxes = getNbAxes();
    for (unsigned int i = fromAxis; i < nbAxes; i++)
        positionNodes(i);
    // In a separate loop so that positionEdges accesses initialized values...
    for (unsigned int i = fromAxis; i < nbAxes; i++)
        positionEdges(i);
    //sortEdges();
}

unordered_set<edge> DataHandler::getEdges(unsigned int leftAxisId) const {
    unordered_set<edge> sharedEdges;
    const Dimension &leftClusters = axesNodes[leftAxisId];
    unsigned int rightAxisDbDim = toDbDimensions.at(leftAxisId + 1);
    for (const auto &pair : leftClusters) {
        for (const edge &e : graph.star(pair.second)) {
            const auto ends = graph.ends(e);
            const node otherEnd = (ends.first == pair.second ? ends.second
                                                             : ends.first);
            unsigned int otherEndDbDim = nodeProperties[otherEnd].dbDimension;
            // If end is in right axis
            if (otherEndDbDim == rightAxisDbDim) {
                if (e.isValid() && graph.isElement(e))
                    sharedEdges.insert(e);
                else throw InvalidStateError();
            }
        }
    }
    return sharedEdges;
}

const node DataHandler::getNodeFromDbDim(unsigned int dbDim,
                                         cluster clusterId) const {
    // Note: nbSourceDimensions cannot be used as we call this
    // method while parsing response
    int axisId = -1;
    for (unsigned int i = 0; i < toDbDimensions.size(); ++i) {
        if (toDbDimensions.at(i) == dbDim)
            axisId = i;
    }
    if (axisId == -1 || axisId >= (int) axesNodes.size())
        throw QueryError(QueryError::NODE_UNKNOWN);
    const auto &axisNodes = axesNodes.at((unsigned long) axisId);
    if (axisNodes.find(clusterId) == axisNodes.end())
        throw QueryError(QueryError::NODE_UNKNOWN);
    node n = axisNodes.at(clusterId);
    return n;
}

const node DataHandler::getNodeFromAxis(unsigned int axisId,
                                        cluster clusterId) const {
    if (axisId >= axesNodes.size()) throw QueryError(QueryError::NODE_UNKNOWN);
    const auto &dimNodes = axesNodes.at(axisId);
    if (dimNodes.find(clusterId) == dimNodes.end()) throw QueryError(QueryError::NODE_UNKNOWN);
    node n = dimNodes.at(clusterId);
    return n;
}

const edge DataHandler::getEdge(unsigned int srcDbDim,
                                unsigned int tgtDbDim,
                                cluster srcClusterId,
                                cluster tgtClusterId) const {
    const node &src = getNodeFromDbDim(srcDbDim, srcClusterId);
    const node &tgt = getNodeFromDbDim(tgtDbDim, tgtClusterId);
    edge e = graph.existEdge(src, tgt, false);
    if (!e.isValid()) throw QueryError(QueryError::EDGE_UNKNOWN);
    return e;
    /*} catch (const NodeUnknown &e) {
        cerr << "Could not find node" << endl;
        throw EdgeUnknown();
    }*/
}

const edge DataHandler::getEdge(std::pair<ClusterPosition, ClusterPosition> edgePosition) const {
    return getEdge(edgePosition.first.first, edgePosition.second.first,
                   edgePosition.first.second, edgePosition.second.second);
}

unsigned int DataHandler::getNodeDbDim(const node &n) const {
    return nodeProperties[n].dbDimension;
}

const Dimension &DataHandler::getDimension(unsigned int axisId) const {
    return axesNodes.at(axisId);
}

const EdgeProp &DataHandler::getEdgeProperty(const edge &e) const {
    return edgeProperties[e];
}

const Ends DataHandler::getEnds(const edge &e) const {
    const pair<node, node> ends = graph.ends(e);
    const NodeProp &np1 = nodeProperties[ends.first];
    const NodeProp &np2 = nodeProperties[ends.second];
    if (np1.dimension < np2.dimension) return {ends.first, ends.second};
    return {ends.second, ends.first};
}

const vector<edge> DataHandler::getStar(const node &n) const {
    return graph.star(n);
}

void DataHandler::forEachNode(std::function<void(const node &)> f) const {
    for (const auto &node : graph.nodes())
        f(node);
}

void DataHandler::forEachNode(unsigned int axisId,
                              std::function<void(const node &)> f) const {
    for (const auto &cluster : axesNodes[axisId])
        f(cluster.second);
}

void DataHandler::forEachEdge(std::function<void(const edge &)> f) const {
    for (const auto &edge : graph.edges())
        f(edge);
}

unsigned int DataHandler::getNbNodes() const {
    return graph.numberOfNodes();
}

unsigned int DataHandler::getNbEdges() const {
    return graph.numberOfEdges();
}

void DataHandler::checkCoherence() {
#ifndef NDEBUG
//    unsigned int nbDim = axesNodes.size();
//    assert(nbDim == toDbDimensions.size());
//    assert(dimensionLabels.empty() || dimensionLabels.size() == nbSourceDimensions);
//    assert(maxNodeId != 0 && maxEdgeId != 0);
//    assert(getNbIndividuals() > 0);
//    assert(graph.numberOfNodes() > 0);
//    assert(graph.numberOfEdges() > 0);
//    assert(nodeProperties.isValid());
//    assert(edgeProperties.isValid());
//    for (auto d : axesNodes)
//        assert(!d.empty());
//    for (unsigned int i = 0; i < axesNodes.size(); ++i) {
//        unsigned int incomingWeight = 0;
//        unsigned int nodeWeight = 0;
//        assert(axesNodes.at(i).size() > 0);
//        for (const auto &p : axesNodes.at(i)) {
//            const NodeProp &np = nodeProperties[p.second];
//            assert(np.weight > 0);
//            assert(std::isfinite(np.min));
//            assert(std::isfinite(np.max));
//            nodeWeight += np.weight;
//            assert(np.dimension - i <=
//                   numeric_limits<float>::epsilon());
//            assert(np.dbDimension == toDbDimensions.at(i));
//            for (const edge &e :graph.getInEdges(p.second)) {
//                const auto &ends = graph.ends(e);
//                incomingWeight += edgeProperties[e].weight;
//                if (ends.first == p.second)
//                    assert(nodeProperties[ends.second].dimension == i - 1);
//                else if (ends.second == p.second)
//                    assert(nodeProperties[ends.first].dimension == i - 1);
//            }
//            assert(!graph.star(p.second).empty());
//        }
//        assert(nbSourceIndividuals == 0 ||
//               incomingWeight == 0 ||
//               (incomingWeight == nbSourceIndividuals && nodeWeight == nbSourceIndividuals));
//    }
//    isConnected();
//    if (current.highlight != nullptr) {
//        current.highlight->forEachNode([this](const node &n) {
//            this->graph.isElement(n);
//        });
//        current.highlight->forEachEdge([this](const edge &e) {
//            this->graph.isElement(e);
//        });
//    }
#endif
}

const node DataHandler::addNode() { //TODO
    const node &clusterNode = graph.addNode();
    if (clusterNode.id > maxNodeId)
        maxNodeId = clusterNode.id;
    return clusterNode;
}

const edge DataHandler::addEdge(node &leftNode, node &rightNode) {
    assert(leftNode.isValid() && rightNode.isValid());
    assert(graph.isElement(leftNode) && graph.isElement(rightNode));
    const edge &existingEdge = graph.existEdge(leftNode, rightNode, false);
    assert (!existingEdge.isValid());
    edge newEdge = graph.addEdge(leftNode, rightNode);
    if (newEdge.id > maxEdgeId)
        maxEdgeId = newEdge.id;
    return newEdge;
}

void exploreNeighbours(VectorGraph &g, node s, NodeProperty<bool> &visited) {
    visited[s] = true;
    node n;
    forEach(n, g.getInOutNodes(s)) {
        if (!visited[n]) exploreNeighbours(g, n, visited);
    }
}

bool DataHandler::isConnected() {
    NodeProperty<bool> visited;
    graph.alloc(visited);
    exploreNeighbours(graph, graph.getOneNode(), visited);
    node n;
    forEach(n, graph.getNodes()) {
        if (!visited[n]) return false;
    }
    graph.free(visited);
    return true;
}

unsigned int DataHandler::getMaxNodeId() const {
    return maxNodeId;
}

void DataHandler::deleteEdges(unsigned int leftAxisId) {
    const unordered_set<edge> toBeDeleted = getEdges(leftAxisId);
    for (const edge &e : toBeDeleted) {
        assert(graph.isElement(e));
        graph.delEdge(e);
    }
}

void DataHandler::deleteNodes(unsigned int leftAxisId) {
    assert(axesNodes.size() > leftAxisId);
    for (const auto &nodePair : axesNodes.at(leftAxisId)) {
        assert(graph.isElement(nodePair.second));
        graph.delNode(nodePair.second);
    }
}

pair<set<edge>, set<node>> DataHandler::getElements(const AxisInterval i,
                                                    bool withSideEdges) const {
    if (i.contains(getNbAxes())) throw InvalidStateError();
    assert(i.getLeft() < axesNodes.size());
    set<edge> intervalEdges;
    set<node> intervalNodes;
    if (withSideEdges && (i.getLeft() > 0)) {
        unordered_set<edge> leftEdges = getEdges(i.getLeft() - 1);
        intervalEdges.insert(leftEdges.begin(), leftEdges.end());
    }
    if (withSideEdges && (i.getRight() < getLastAxisId())) {
        unordered_set<edge> rightEdges = getEdges(i.getRight());
        intervalEdges.insert(rightEdges.begin(), rightEdges.end());
    }
    for (const auto &pair : axesNodes[i.getLeft()]) {
        assert(graph.isElement(pair.second));
        intervalNodes.insert(pair.second);
    }
    if (i.size() > 1)
        for (unsigned int axisId = i.getLeft(); axisId < i.getRight(); axisId++) {
            const Dimension &leftClusters = axesNodes[axisId];
            unsigned int rightAxisDbDim = toDbDimensions.at(axisId + 1);
            for (const auto &pair : leftClusters) {
                for (const edge e : graph.star(pair.second)) {
                    const auto ends = graph.ends(e);
                    const node otherEnd = (ends.first == pair.second ? ends.second
                                                                     : ends.first);
                    const unsigned int otherEndDim = nodeProperties[otherEnd].dbDimension;
                    if (otherEndDim == rightAxisDbDim) {
                        intervalNodes.insert(otherEnd);
                        intervalEdges.insert(e);
                    }
                }
            }
        }
    return make_pair(intervalEdges, intervalNodes);
}

node DataHandler::getNodeById(unsigned int viewId) const {
    if (viewId <= maxNodeId) {
        node n = node(viewId);
        return graph.isElement(n) ? n : node();
    }
    return {};
}

edge DataHandler::getEdgeById(unsigned int clientId) const {
    if (clientId > maxNodeId && clientId <= maxNodeId + maxEdgeId) {
        assert(clientId - maxNodeId >= 1);
        edge e = edge(clientId - maxNodeId - 1);
        return graph.isElement(e) ? e : edge();
    }
    return {};
}

bool DataHandler::isElementById(int clientId) const {
    return getEdgeById((unsigned int) clientId).isValid() || getNodeById((unsigned int) clientId).isValid();
}

unsigned int DataHandler::getEdgeClientId(const edge &e) const {
    assert(e.isValid() && graph.isElement(e));
    return maxNodeId + e.id + 1;
}

unsigned int DataHandler::getNbAxes() const {
    return axesNodes.size();
}

const NodeProp &DataHandler::getNodeProperty(const node &n) const {
    assert(graph.isElement(n));
    return nodeProperties[n];
}

void DataHandler::sortEdges() {
    backZ = -1.f;
    vector<edge> edges = graph.edges();
    sort(edges.begin(), edges.end(), [this](edge a, edge b) {
        return edgeProperties[a].weight >
               edgeProperties[b].weight;
    });
    for (const edge e : edges) {
        pullEdgeBack(e);
    }
}


void DataHandler::pullEdgeBack(const edge &e) {
    edgeProperties[e].z = backZ;;
    backZ -= Z_UNIT;
}

bool DataHandler::isAtRoot(unsigned int axisId) const {
    return current.dimensionParents.at(toDbDimensions.at(axisId)).isRoot();
}


cluster DataHandler::getAncestor(unsigned int axisId, unsigned int generations) const {
    ClusterIdentifier individual = current.dimensionParents.at(toDbDimensions.at(axisId));
    while (generations > 0) {
        individual = ClusterIdentifier::getParent(individual);
        generations--;
    }
    return individual.getLeafId();
}

void DataHandler::positionNodes(unsigned int axisId) {
    float floatNbIndividuals = static_cast<float>(getNbIndividuals());
    vector<node> nodes;
    nodes.reserve(axesNodes.at(axisId).size());
    for (const auto &it : axesNodes.at(axisId))
        nodes.push_back(it.second);
    sort(nodes.begin(), nodes.end(), CompNode(nodeProperties));

    /* sum of gaps, sum of spaces, initialisation distortion */
    assert(graph.isElement(nodes.at(0)));
    NodeProp &np0 = nodeProperties[nodes.at(0)];
    // assert(np0.clusterId == 0 || np0.clusterId == NodeProp::LOWER_META_NODE_ID);  // Adapted to server talk
    totalInter = 0.f, totalIntra = 0.f;
    float prev = np0.max;
    float maxDimValue = np0.max;
    float minDimValue = np0.min;
    cluster previousCluster = np0.clusterId;
    for (unsigned int i = 1; i < nodes.size(); i++) {
        NodeProp &np = nodeProperties[nodes.at(i)];
        if (np.max > maxDimValue) maxDimValue = np.max;
        if (np.min < minDimValue) minDimValue = np.min;
        totalIntra += np.max - np.min;
        np.distortion = 0;
        totalInter += abs(np.min - prev);
        prev = np.max;
        assert(np.clusterId > previousCluster ||
               np.isHigherContext()); // Adapted to server talk
        previousCluster = np.clusterId;
    }

    /* minDistortion */
    float minDistortion = FLT_MAX;
    if (totalIntra == 0.f)
        minDistortion = 0.f;
    else
        for (auto &node : nodes) {
            NodeProp &np = nodeProperties[node];
            np.distortion = ((np.max - np.min) / totalIntra) -
                            (np.weight / floatNbIndividuals);
            if (np.distortion < minDistortion)
                minDistortion = np.distortion;
        }
    /* gapBefore, weightBefore, et distortion */
    np0.gapBefore = 0.f;
    np0.weightBefore = 0.f;
    float weightBefore = np0.weight;
    float gapBefore = 0;
    prev = np0.max;
    for (unsigned int i = 1; i < nodes.size(); i++) {
        NodeProp &np = nodeProperties[nodes[i]];
        np.distortion = (np.distortion - minDistortion) / 2.f;
        gapBefore += (totalInter == 0) ? 0 : abs(np.min - prev) / totalInter;
        np.gapBefore = gapBefore;
        np.weightBefore = weightBefore / floatNbIndividuals;
        weightBefore += np.weight;
        prev = np.max;
    }
    //if (totalInter == 0.f) np0.gapBefore = 0.5f; // Cosmetic choice left to the rendering side
}

void DataHandler::positionEdges(unsigned int axisId) {
    for (const auto &itN : axesNodes.at(axisId)) {
        const node &n = itN.second;
        const NodeProp &np = nodeProperties[n];
        const auto denominator = (float) (np.weight * 2);
        vector<pair<edge, float> > inEdges, outEdges;
        for (const edge &e : graph.star(n)) {
            const pair<node, node> ends = graph.ends(e);
            if (n == ends.first) {
                outEdges.emplace_back(e, nodeProperties[ends.second].weightBefore);
            } else if (n == ends.second) {
                inEdges.emplace_back(e, nodeProperties[ends.first].weightBefore);
            }
        }
        sort(inEdges.begin(), inEdges.end(), CompEdge());
        sort(outEdges.begin(), outEdges.end(), CompEdge());

        float pos = 0.f;
        for (const auto &it : inEdges) {
            EdgeProp &ep = edgeProperties[it.first];
            const float hh = (float) ep.weight / denominator;
            pos += hh;
            //assert(pos >= 0.f && pos <= 1.f);
            ep.destPos = pos;
            pos += hh;
        }

        pos = 0.f;
        for (const auto &it : outEdges) {
            EdgeProp &ep = edgeProperties[it.first];
            const float hh = (float) ep.weight / denominator;
            pos += hh;
            assert(pos >= 0.f && pos <= 1.01f);
            ep.sourcePos = pos;
            pos += hh;
        }
    }
}

float DataHandler::getFrontZ() const {
    return backZ;
}

unsigned int DataHandler::getNbSourceDimensions() const {
    return nbSourceDimensions;
}

unsigned int DataHandler::getNbIndividuals() const {
    return nbSourceIndividuals;
}

const std::vector<float> DataHandler::createNormalizedDistribution(const Distribution &d, unsigned int normalizer) {
    vector<float> result;
    if (d.empty()) return std::vector<float>();
    for (unsigned int count : d) {
        float normCount = static_cast<float>(count) / normalizer;
        //assert(normCount >= 0.f && normCount <= 1.f); // TODO
        result.push_back(normCount);
    }
    assert(result.size() == 1 || result.size() == NodeProp::DISTRIBUTION_BUCKET_NB);
    return result;
}

const std::vector<float> DataHandler::getNormalizedDistribution(const node &n) const {
    const NodeProp &np = nodeProperties[n];
    unsigned int normalizer = getBiggestBucket(np.dbDimension);
    assert(normalizer <= getNbIndividuals());
#ifndef NO_DISTRIBUTIONS
    return (np.isMeta() ? std::vector<float>() : createNormalizedDistribution(np.distribution, normalizer));
#else
    return std::vector<float>();
#endif
}

void DataHandler::clearHighlight() {
    if (current.highlight != nullptr) {
        delete current.highlight;
        current.highlight = nullptr;
    }
}

void DataHandler::init(unsigned int nbDimensions, unsigned int nbIndividuals) {
    nbSourceIndividuals = nbIndividuals;
    nbSourceDimensions = nbDimensions;
    current.dimensionParents.resize(nbSourceDimensions);
    current.lowerContextStacks.resize(nbSourceDimensions);
    current.upperContextStacks.resize(nbSourceDimensions);
}

void DataHandler::finish() {
    updateDisplayValues(0);
    checkCoherence();
#ifndef TEST
    cout << "[DataHandler] |V|= " << graph.numberOfNodes() << " |E|= " << graph.numberOfEdges() << endl;
    cout << "[DataHandler] Nb elements " << nbSourceIndividuals << endl;
#endif
    if (observer_ != nullptr)
        observer_->onDataLoaded(this);
}


unsigned int DataHandler::toAxis(unsigned int dbDim) const {
    for (unsigned int i = 0; i < toDbDimensions.size(); i++)
        if (toDbDimensions.at(i) == dbDim) {
            return i;
        }
    throw InvalidStateError();
}

DimensionInterval DataHandler::toDimensions(AxisInterval axes) const {
    DimensionInterval dc;
    for (unsigned int axis : axes)
        dc.chain(toDbDimensions.at(axis));
    return dc;
}

unsigned int DataHandler::toDimension(unsigned int axis) const {
    assert(toDbDimensions.size() > axis);
    return toDbDimensions.at(axis);
}

const Highlight *DataHandler::getCurrentHighlight() const {
    return current.highlight;
}

void DataHandler::mergeCluster(const pair<cluster, node> &cluster,
                               unsigned int axis,
                               set<ClusterProp> &clusterProps,
                               pair<Links, Links> &clusterLinks) const {
    clusterProps.insert(nodeProperties[cluster.second]);
    node m;
    forEach(m, graph.getInOutNodes(cluster.second)) {
        const NodeProp &mProp = nodeProperties[m];
        unsigned int outAxis = mProp.dimension;
        Links *linkHolder = nullptr;
        if (outAxis < axis) linkHolder = &clusterLinks.first;
        else if (outAxis > axis) linkHolder = &clusterLinks.second;
        assert(linkHolder != nullptr);
        auto link = linkHolder->find(mProp.clusterId);
        if (link == linkHolder->end()) linkHolder->insert({mProp.clusterId, 0});
        const edge &linkEdge = graph.existEdge(cluster.second, m, false);
        assert(linkEdge.isValid());
        linkHolder->at(mProp.clusterId) += edgeProperties[linkEdge].weight;
    }
}

void DataHandler::applyOpening(const node n) {
    const NodeProp np = nodeProperties[n];
    assert(np.focusable());
    saveHighlight();
    clearHighlight();


    // Create new parent cluster identifier
    current.dimensionParents.at(np.dbDimension).chain(np.clusterId);
#ifdef HIERARCHICAL
#ifdef LOCAL
    const HNode *currentNode = dimensionParentNode.at(np.dbDimension);
    dimensionParentNode.at(np.dbDimension) = currentNode->getChild(np.clusterId);
#endif
#endif
    set<ClusterProp> higherEndNodes;
    set<ClusterProp> lowerEndNodes;
    pair<Links, Links> higherEndStar;
    pair<Links, Links> lowerEndStar;
    ClusterProp highUncle(ClusterProp::HIGHER_META_NODE_ID), lowUncle(ClusterProp::LOWER_META_NODE_ID);

    // Merge context nodes data
    for (const auto &nodePair : axesNodes.at(np.dimension)) {
        NodeProp &snp = nodeProperties[nodePair.second];
        if (nodePair.first > np.clusterId) {// Cluster ids relate to their value ordering
            mergeCluster(nodePair, np.dimension, higherEndNodes, higherEndStar);
            if (nodePair.first < NodeProp::HIGHER_META_NODE_ID) {
                highUncle.weight += snp.weight;
                highUncle.min = min(snp.min, highUncle.min);
                highUncle.max = max(snp.max, highUncle.max);
            }
        } else if (nodePair.first < np.clusterId) {
            mergeCluster(nodePair, np.dimension, lowerEndNodes, lowerEndStar);
            if (nodePair.first > NodeProp::LOWER_META_NODE_ID) {
                lowUncle.weight += snp.weight;
                lowUncle.min = min(snp.min, lowUncle.min);
                lowUncle.max = max(snp.max, lowUncle.max);
            }
        }
    }

    // Update context stacks
    current.upperContextStacks.at(np.dbDimension).push_back(highUncle);
    current.lowerContextStacks.at(np.dbDimension).push_back(lowUncle);

    deleteAxisElements(np.dimension);

    // Create new context elements
    if (!higherEndNodes.empty()) {
        createNodeAggregate(ClusterProp::HIGHER_META_NODE_ID,
                            np.dimension,
                            higherEndNodes,
                            higherEndStar);
    }

    if (!lowerEndNodes.empty()) {
        createNodeAggregate(ClusterProp::LOWER_META_NODE_ID,
                            np.dimension,
                            lowerEndNodes,
                            lowerEndStar);
    }

    assert(!axesNodes.at(np.dimension).empty());
    doCreateOpeningElements(np.dimension, np.clusterId, current.dimensionParents.at(np.dbDimension));
    checkCoherence();
    updateDisplayValues((unsigned int) max(0, int(np.dimension) - 1));
    restoreHighlight();
}

void DataHandler::prepareOpening(unsigned int, cluster) {

};

void DataHandler::prepareClosing(const unsigned int, unsigned int) {

}

void DataHandler::applyClosing(const unsigned int axisId, unsigned int nbLevels) {
    unsigned int dbDim = toDbDimensions.at(axisId);
    // Create new parent cluster identifier and update context stacks
    ClusterIdentifier newParent = current.dimensionParents.at(dbDim);
    for (unsigned int i = 0; i < nbLevels; i++) {
        current.lowerContextStacks.at(dbDim).pop_back();
        current.upperContextStacks.at(dbDim).pop_back();
        newParent = ClusterIdentifier::getParent(newParent);
#ifdef HIERARCHICAL
#ifdef LOCAL
        const HNode *currentNode = dimensionParentNode.at(dbDim);
        dimensionParentNode.at(dbDim) = currentNode->getParent();
        assert(dimensionParentNode.at(dbDim) != nullptr);
#endif
#endif
    }
    Notifier::info("New chain: " + newParent.toString());
    current.dimensionParents.at(dbDim) = newParent;
    deleteAxisElements(axisId);
    doCreateClosingElements(axisId, nbLevels);
    checkCoherence();
    updateDisplayValues((unsigned int) max(0, int(axisId) - 1));
}

void DataHandler::deleteAxisElements(unsigned int axisId) {
    deleteNodes(axisId);
    axesNodes.at(axisId).clear();
}

unsigned int DataHandler::getLevel(unsigned int dbDim) const {
    return current.dimensionParents.at(dbDim).getNbLevels();
}

bool DataHandler::isContext(const SelectedElement &e) const {
    if (e.n.isValid()) return getNodeProperty(e.n).isMeta();
    if (e.e.isValid()) {
        const auto ends = graph.ends(e.e);
        return getNodeProperty(ends.first).isMeta() &&
               getNodeProperty(ends.second).isMeta();
    }
    return false;

}
