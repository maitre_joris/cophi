/*
  Copyright 2015 Timothée Jourde <itim.lcf@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <data/RemoteData.h>
#include <Error.h>
#include <tlp/ForEach.h>

using namespace std;

RemoteData::RemoteData(DataObserver *obs, RequestHandler *builder)
        : DataHandler(obs, false), provider(builder) {
    // Fill out axesNodes, graph
    graph.alloc(nSubGraphs);
    graph.alloc(eSubGraphs);
    if (!provider->parseDataSetResponse(this)) throw ImportError(ImportError::SERVER);
    init(getNbAxes(), countIndividuals());
    computeExtrema(rootDimensionExtrema);
    computeDimensionMaxBucketWeight();
    finish();
}

RemoteData::~RemoteData() {
    graph.free(nSubGraphs);
    graph.free(eSubGraphs);
}

void RemoteData::saveState() {
    DataHandler::saveState();
    /*previous = RollBackState(current);
    previous.graph = graph;
    previous.nodeProperties = nodeProperties;
    previous.edgeProperties = edgeProperties;
    previous.toDbDimensions = toDbDimensions;
    previous.toAxisId = toAxisId;*/
}

void RemoteData::rollBackState() {
    /*previous.rollBack(current);
    graph = previous.graph;
    nodeProperties = previous.nodeProperties;
    edgeProperties = previous.edgeProperties;
    toDbDimensions = previous.toDbDimensions;
    toAxisId = previous.toAxisId;*/
}

unsigned int RemoteData::countIndividuals() {
    assert(!axesNodes.empty());
    unsigned int count = 0;
    for (const auto &pair : axesNodes[0]) {
        const NodeProp &np = nodeProperties[pair.second];
        count += np.weight;
    }
    return count;
}

void RemoteData::triggerHighlight(const node &n) {
    if (!graph.isElement(n)) throw InvalidStateError();
    saveState();
    assert (observer_ != nullptr);
    const NodeProp &np = getNodeProperty(n);
    // Prepare new state
    current.highlight = nullptr;
    current.highlightOrigin = HighlightBase(np.dbDimension, np.clusterId);
    if (!isReadySubGraph(n)) {
        provider->requestHighlight(getAxesAsDimensions(),
                                   current.highlightOrigin);
    } else {
        clearSavedHighlight();
        current.highlight = &nSubGraphs[n];
        observer_->onHighlight(current.highlight);
    }
}

unsigned int RemoteData::getBiggestBucket(unsigned int dbDim) const {
    unsigned int v = dimensionBiggestBucket.at(dbDim);
    if (v == 0) throw InvalidStateError(); // Or ignore when using it?
    return v;
}

void RemoteData::doCreateClosingElements(unsigned int, unsigned int) {
    clearSubGraphs();
    displayFocus();
}


void RemoteData::doCreateOpeningElements(unsigned int,
                                         cluster,
                                         const ClusterIdentifier &) {
    clearSubGraphs();
    displayFocus();
}

void RemoteData::prepareOpening(unsigned int axisId,
                                cluster id) {
    //previous = RollBackState(current);
    provider->requestFocusIn(toDbDimensions.at(axisId), id);
    focusAxis = axisId;
}


void RemoteData::prepareClosing(unsigned int axisId,
                                unsigned int nbClosings) {
    //previous = current;
    provider->requestFocusOut(toDbDimensions.at(axisId), nbClosings);
    focusAxis = axisId;
}

void RemoteData::displayFocus() {
    if (provider->parseIntervalResponse(this)) {
    } else {
        //current = previous;
    }
}


void RemoteData::triggerHighlight(const edge &e) {
    if (!graph.isElement(e)) throw InvalidStateError();
    const auto &ends = getEnds(e);
    const NodeProp &npSrc = getNodeProperty(ends.source);
    const NodeProp &npDst = getNodeProperty(ends.dest);
    // Save previous state
    //previous = RollBackState(current);
    // Prepare new state
    current.highlight = nullptr;
    current.highlightOrigin = HighlightBase(npSrc.dbDimension, npSrc.clusterId,
                                            npDst.dbDimension, npDst.clusterId);
    if (!isReadySubGraph(e)) {
        provider->requestHighlight(getAxesAsDimensions(), current.highlightOrigin);
    } else {
        clearSavedHighlight();
        current.highlight = &eSubGraphs[e];
        if (observer_ != nullptr)
            observer_->onHighlight(current.highlight);
    }

}

bool RemoteData::prepareDeletion(const AxisInterval axes) {
    if (axes.isSideOn(getLastAxisId())) return true;
    AxisInterval expanded(axes, true);
#ifdef HIERARCHICAL
    DimensionInterval dims = toDimensions(axes);
    provider->requestDeletion(dims);
#else
    DimensionInterval sides(toDbDimensions.at(expanded.getLeft()));
    sides.chain(toDbDimensions.at(expanded.getRight()));
    provider->requestInterval(sides);
    if (current.highlight != nullptr)
        requestHighlightFromOrigin(sides);
#endif
    return false;
}

void RemoteData::requestHighlightFromOrigin(const DimensionInterval dims) {
    assert(current.highlightOrigin.isValid() && current.highlight != nullptr);
    provider->requestHighlight(dims, current.highlightOrigin);
}

void RemoteData::displayHighlighting() {
    if (!current.highlightOrigin.isValid() || current.highlight != nullptr) return;
    if (current.highlightOrigin.isIntervalBase()) {
        current.highlight = new Highlight(this, SelectedElement());
    } else if (current.highlightOrigin.isEdgeBase()) {
        const edge e = getEdge(current.highlightOrigin.getEdgePosition());
        eSubGraphs[e] = Highlight(this, SelectedElement(e));
        current.highlight = &eSubGraphs[e];
    } else if (current.highlightOrigin.isClusterBase()) {
        const auto cp = current.highlightOrigin.getClusterPosition();
        const node n = axesNodes.at(cp.first).at(cp.second);
        nSubGraphs[n] = Highlight(this, SelectedElement(n));
        current.highlight = &nSubGraphs[n];
    }
    if (provider->parseHighlightResponse(current.highlight)) {
        observer_->onHighlight(current.highlight);
        clearSavedHighlight();
    } else {
        // Something happened, rollback
        //TODO
        //current = previous;
        clearSavedHighlight();
    }
}


void RemoteData::updateDimension(unsigned int dbDim,
                                 const string &label) {
    unsigned int axisId;
    if (toAxisId.find(dbDim) != toAxisId.end()) {
        axisId = toAxisId.at(dbDim);
        assert(toDbDimensions.at(axisId) == dbDim);
    } else {
        // If dbDim has no corresponding axis, build
        axisId = (unsigned int) toAxisId.size();
        axesNodes.resize(axisId + 1);
        dimensionLabels.resize(axisId + 1);
        toAxisId.insert({dbDim, axisId});
        toDbDimensions.chain(dbDim);
        assert(dimensionLabels.size() > dbDim);
        dimensionLabels[dbDim] = label;
    }
}

bool RemoteData::addNode(unsigned int dbDim,
                         cluster clusterId,
                         float min,
                         float max,
                         Distribution distribution) {
    assert(toAxisId.find(dbDim) != toAxisId.end());
    unsigned int axisId = toAxisId.at(dbDim);
    assert(toDbDimensions.at(axisId) == dbDim);
    auto &thisDimNodes = axesNodes.at(axisId);
    node n;
    if (thisDimNodes.find(clusterId) == thisDimNodes.end()) {
        n = DataHandler::addNode();
        thisDimNodes[clusterId] = n;
        NodeProp &np = nodeProperties[n];
        np.clusterId = (cluster) clusterId;
        np.dimension = axisId;
        np.dbDimension = dbDim;
        np.min = min;
        np.max = max;
        np.weight = 0;
        assert(distribution.size() == 10 || distribution.size() == 1);
        np.distribution = distribution;
        float approximateSum = 0;
        float intervalSize = (max - min) / distribution.size();
        for (unsigned int i = 0; i < distribution.size(); i++) {
            np.weight += distribution[i];
            approximateSum += ((min + intervalSize * i) * distribution[i]);
        }
        //np.avg = approximateSum / np.weight;
        return (n.isValid() && graph.isElement(n));
    } else {
        // Bordering nodes on interval response (stitching deletion or adding inserted dimension)
        return true;
    }
}

bool RemoteData::addEdge(unsigned int srcDbDimension,
                         unsigned int tgtDbDimension,
                         cluster srcClusterId,
                         cluster tgtClusterId,
                         unsigned int weight) {
    string header = "Error while adding edge: ";
    stringstream ss;
    ss << "(d=" << to_string(srcDbDimension) <<
       ",n=" << to_string(srcClusterId) << "), (d="
       << to_string(tgtDbDimension) <<
       ",n=" << to_string(tgtClusterId) + ")";
    string edgeInfo = ss.str();

    try {
        node src = getNodeFromDbDim(srcDbDimension, srcClusterId);
        node tgt = getNodeFromDbDim(tgtDbDimension, tgtClusterId);
        if (graph.existEdge(src, tgt, false).isValid()) {
            cerr << header << edgeInfo << endl << "Edge already exists" << endl;
            return false;
        } else {
            edge e;
            if (toAxisId.at(srcDbDimension) > toAxisId.at(tgtDbDimension)) {
                e = DataHandler::addEdge(tgt, src);
            } else if (toAxisId.at(srcDbDimension) < toAxisId.at(tgtDbDimension)) {
                e = DataHandler::addEdge(src, tgt);
            }
            // Check hint of validity
            if (nodeProperties[src].weight < weight ||
                nodeProperties[tgt].weight < weight) {
                cerr << header << edgeInfo << endl << "Edge weight " << weight << " is superior to its ends' weight "
                     << nodeProperties[src].weight << " and " << nodeProperties[tgt].weight << endl;
                return false;
            } else {
                edgeProperties[e].weight = weight;
            }

            return true;
        }
    } catch (const BuildError &e) {
        cerr << header << edgeInfo << endl << e.what() << endl;
        return false;
    } catch (const QueryError &e) {
        cerr << header << edgeInfo << endl << e.what() << endl;
        return false;
    }

}

void RemoteData::createDeletionElements(const AxisInterval &axes) {
    clearSubGraphs();
    toAxisId.clear();
    for (unsigned int i = 0; i < toDbDimensions.size(); ++i) {
        toAxisId.insert({toDbDimensions.at(i), i});
    }
    assert(toAxisId.size() == toDbDimensions.size());
    DataHandler::restitchAfterDeletion(axes, [this](unsigned int, unsigned int) {
        // Fetch in between edges and add them to graph
        if (provider->parseIntervalResponse(this)) {

        } else {
            //rollBackState();
        }
    });


}

void RemoteData::createInsertionElements(const DimensionInterval &,
                                         int justAfterAxisId) {
    clearSubGraphs();
    assert(justAfterAxisId >= -1);
    toAxisId.clear();
    for (unsigned int i = 0; i < toDbDimensions.size(); ++i) {
        toAxisId.insert({toDbDimensions.at(i), i});
    }
    // Fill axesNodes and properties from base response
    if (provider->parseIntervalResponse(this)) {

    } else {
        //rollBackState();
    }
}

void RemoteData::computeExtrema(std::vector<Extrema> &holder) {
    // Here we assume every axis holds its canonical dimension and every
    // dimension is materialized
    holder.resize(axesNodes.size());
    for (unsigned int d = 0; d < axesNodes.size(); ++d) {
        Extrema &e = holder[d];
        e.min = numeric_limits<float>::infinity();
        e.max = -numeric_limits<float>::infinity();
        for (const auto pair : axesNodes[d]) {
            const NodeProp &np = nodeProperties[pair.second];
            e.max = max(e.max, np.max);
            e.min = min(e.min, np.min);
        }
    }
}

void RemoteData::computeDimensionMaxBucketWeight() {
    dimensionBiggestBucket = vector<unsigned int>(nbSourceDimensions, 0);
    for (unsigned int d = 0; d < nbSourceDimensions; ++d) {
        for (const auto nodePair : axesNodes[d]) {
            const NodeProp &np = nodeProperties[nodePair.second];
            assert(np.dimension == np.dbDimension);
            const Distribution &distribution = np.distribution;
            for (const auto w : distribution)
                dimensionBiggestBucket[d] = max(dimensionBiggestBucket[d],
                                                (unsigned int) w);
        }
    }
}

void RemoteData::restitchHighlight(unsigned int, unsigned int) {
    assert(current.highlightOrigin.isValid() && current.highlight != nullptr);
    provider->parseHighlightResponse(current.highlight);
}

void RemoteData::prepareInsertion(const AxisInterval selectedAxes,
                                  const DimensionInterval selectedDims,
                                  int insertAfter) {

    if (selectedAxes.isBefore(insertAfter))
        insertAfter -= selectedAxes.size();
#ifndef HIERARCHICAL
    DimensionInterval dims;
    if (insertAfter > -1)
        dims.chain(toDbDimensions.at(insertAfter));
    assert(selectedDims.size() == selectedAxes.size());
    dims.chain(selectedDims);
    if (insertAfter != getLastAxisId())
        dims.chain(toDbDimensions.at(insertAfter + 1));
    provider->requestInterval(dims);
    if (current.highlight != nullptr)
        requestHighlightFromOrigin(dims);
#else
    provider->requestInsertion(selectedDims, insertAfter + 1);
#endif
}

bool RemoteData::isReadySubGraph(const edge &e) const {
    return !(eSubGraphs[e].isEmpty());
}

bool RemoteData::isReadySubGraph(const node &n) const {
    return !(nSubGraphs[n].isEmpty());
}

void RemoteData::clearSubGraphs() {
    if (current.highlightOrigin.isValid()) {
        assert(current.highlight != nullptr);
        Highlight *tmp = new Highlight(*current.highlight);
        current.highlight = tmp;
    }
    node n;
    forEach(n, graph.getNodes()) {
        Highlight *sub = &(nSubGraphs[n]);
        if (!sub->isEmpty()) {
            sub->clear();
        }
    }
    edge e;
    forEach(e, graph.getEdges()) {
        Highlight *sub = &(eSubGraphs[e]);
        if (!sub->isEmpty()) {
            sub->clear();
        }
    }
}

void RemoteData::clearHighlight() {
    if (current.highlight == nullptr) return;
    assert(current.highlightOrigin.isValid());
    if (isDisplayed(current.highlightOrigin)) current.highlight = nullptr; // Highlight is in cache
    else DataHandler::clearHighlight();
    current.highlightOrigin.clear();
    clearSavedHighlight();
}

void RemoteData::clearSavedHighlight() {
    if (previous.highlightOrigin.isValid()) {
        if (isDisplayed(current.highlightOrigin))
            previous.highlight = nullptr; // Highlight is in cache
        else {
            if (previous.highlight != nullptr)
                delete previous.highlight;
            previous.highlight = nullptr;
        }
        previous.highlightOrigin.clear();
    }
}

bool RemoteData::isDisplayed(const HighlightBase &o) {
    assert(o.isValid());
    if (o.isClusterBase())
        return toAxisId.find(o.getClusterPosition().first) != toAxisId.end();
    else if (o.isEdgeBase()) {
        const auto nodePairs = o.getEdgePosition();
        return (toAxisId.find(nodePairs.first.first) != toAxisId.end() &&
                toAxisId.find(nodePairs.second.first) != toAxisId.end());
    } else return false;
}


void RemoteData::triggerHighlight(const std::vector<ClusterInterval> &clusterIntervals) {
    // Save previous state
    //previous = RollBackState(current);
    // Prepare new state
    current.highlight = nullptr;
    current.highlightOrigin = HighlightBase(clusterIntervals);
    provider->requestHighlight(getAxesAsDimensions(),
                               current.highlightOrigin);

}

void RemoteData::triggerHighlight(const std::vector<SelectedElement> &elts) {

}


const Extrema RemoteData::getAxisExtrema(unsigned int axisId) const {
    return rootDimensionExtrema.at(toDbDimensions.at(axisId));
}

void RemoteData::saveHighlight() {
    if (current.highlight != nullptr) {
        toBeRestored = current.highlightOrigin;
    }
}


void RemoteData::restoreHighlight() {

}