#include <util/AxisInterval.h>
#include <vector>
#include <algorithm>
#include <sstream>
#include <iterator>
#include <cassert>

AxisInterval::AxisInterval() : empty(true) { }

AxisInterval::AxisInterval(const AxisInterval &axes, bool expandAxes) :
        left(axes.getLeft()),
        right(axes.getRight()),
        empty(axes.isEmpty()) {
  if (expandAxes) {
    expand();
      empty = false;
  }
}

AxisInterval::AxisInterval(unsigned l, unsigned r) {
  if (l <= r) {
    left = l;
    right = r;
    empty = false;
  } else throw InvalidAxisInterval();
}

AxisInterval::AxisInterval(int l, int r) : AxisInterval((unsigned int) l,
                                                        (unsigned int) r) {
  if (l < 0 || r < 0) throw InvalidAxisInterval();
}


bool AxisInterval::isEmpty() const {
  return empty;
}

bool AxisInterval::contains(unsigned int i) const {
  return !empty && i >= left && i <= right;
}

bool AxisInterval::isDirectLeftNeighbour(unsigned int i) const {
  return !empty && left > 0 && i == left - 1;
}

size_t AxisInterval::size() const { return right - left + 1; }

void AxisInterval::clear() {
  empty = true;
}

bool AxisInterval::isBefore(int i) const {
  return !empty && ((int) left < i) && ((int) right < i);
}

unsigned int AxisInterval::getLeft() const {
  assert(!empty);
  return left;
}

unsigned int AxisInterval::getRight() const {
  assert(!empty);
  return right;
}

unsigned int AxisInterval::operator[](int index) const {
  assert(index < right - left + 1);
  return (left + index);
}

AxisInterval::const_iterator AxisInterval::begin() const {
  return AxisInterval::const_iterator(left);
}

AxisInterval::const_iterator AxisInterval::end() const {
  return AxisInterval::const_iterator(right + 1);
}

std::string AxisInterval::toString() {
  std::stringstream ss;
  ss << "[";
  if (!empty) ss << left << ";" << right;
  ss << "]";
  return ss.str();
}

unsigned int AxisInterval::getSingleAxis() const {
  assert(!empty);
  if (right != left)
    throw InvalidAxisInterval();
  else return right;
}

bool AxisInterval::isSideOn(unsigned int lastAxisId) const {
  assert(!empty);
  return contains(0) || contains(lastAxisId);
}

void AxisInterval::expand() {
  assert(!empty);
  left = (unsigned int) std::max(0, (int) left - 1);
  right += 1; // Should not be called on side axis. Risky tho.
  //right = (unsigned int) std::min(lastAxisId, right + 1);

}