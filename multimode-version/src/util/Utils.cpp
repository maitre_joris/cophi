/*
  Copyright 2015 Timothée Jourde <itim.lcf@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include "util/Utils.h"

using namespace std;

std::string fileExtension(const std::string &path) {
    const size_t unixDir = path.rfind("/");
    const size_t winDir = path.rfind("\\");

    size_t dir;
    if (unixDir != std::string::npos and winDir != std::string::npos)
        dir = std::max(unixDir, winDir);
    else if (unixDir != std::string::npos)
        dir = unixDir;
    else if (winDir != std::string::npos)
        dir = winDir;
    else
        return "";

    const std::string basename = path.substr(dir, std::string::npos);
    const size_t dot = basename.rfind('.');

    if (dot == 0 or dot == std::string::npos or dot == std::string::npos - 1)
        return "";
    else
        return basename.substr(dot + 1, std::string::npos);
}


bool rectContains(const Vec4f r, const Vec2f p) {
    return (p.x() >= r.x() and p.y() >= r.y()
            and p.x() <= r.z() and p.y() <= r.w()
    );
}

bool rectIntersects_(const Vec4f a, const Vec4f b) {
    const Vec2f bSize = Vec2f(b.z() - b.x(), b.w() - b.y());

    return (rectContains(a, Vec2f(b.x(), b.y()))
            or rectContains(a, Vec2f(b.z(), b.w()))
            or rectContains(a, Vec2f(b.x() + bSize.x(), b.y()))
            or rectContains(a, Vec2f(b.x(), b.y() + bSize.y()))

            or (a.x() >= b.x() and a.x() <= b.z()
                and a.z() >= b.x() and a.z() <= b.z()
                and a.y() <= b.y() and a.w() >= b.w()
            )
    );
}

bool rectIntersects(const Vec4f a, const Vec4f b) {
    return (rectIntersects_(a, b) || rectIntersects_(b, a));
}

Vec4f screenRect(const GlMat4f &invProj) {
    const Vec4f botLeft = invProj * Vec4f(-1, -1, 0, 1);
    const Vec4f topRight = invProj * Vec4f(1, 1, 0, 1);
    return Vec4f(botLeft.x(), botLeft.y(), topRight.x(), topRight.y());
}

Vec4f to_gl(Color c) {
    return Vec4f(c.getRGL(), c.getGGL(), c.getBGL(), c.getAGL());
}

bool isblank(const std::string &str) {
    for (auto c : str)
        if (not std::isblank(c)) return false;

    return true;
}

Vec5f vec5(float a, float b, float c, float d, float e) {
    Vec5f v;
    v[0] = a;
    v[1] = b;
    v[2] = c;
    v[3] = d;
    v[4] = e;
    return v;
}

bool isBlank(const std::string &str) {
    for (auto c : str)
        if (not std::isblank(c)) return false;
    return true;
}

void tokenize(const std::string &str,
              std::vector<std::string> &tokens,
              const char delimiter) {
    size_t prev = 0;
    size_t next = 0;
    while ((next = str.find_first_of(delimiter, prev)) != string::npos) {
        if (next - prev != 0)
            tokens.push_back(str.substr(prev, next - prev));
        else
            tokens.push_back("");
        prev = next + 1;
    }
    if (prev < str.size()) {
        tokens.push_back(str.substr(prev));
    }
}

void printSpentTime(Clock::time_point t0, std::string task) {
    Clock::time_point t1 = Clock::now();
    unsigned long inMicroSec = std::chrono::duration_cast<microseconds>(t1 - t0).count();
    unsigned long inMilliSec = std::chrono::duration_cast<milliseconds>(t1 - t0).count();
    unsigned long inSec = inMilliSec / 1000;
    std::cout << task << ": ";
    if (inMicroSec > 1000000) std::cout << inSec << " s";
    else if (inMicroSec > 1000) std::cout << inMilliSec << " ms";
    else std::cout << inMicroSec << " µs";
    cout << std::endl;
}

void fillDistribution(float value,
                      Distribution &distribution,
                      float min,
                      float max) {
    assert(distribution.size() == 10 || distribution.size() == 1);
    double intervalSize = max - min;
    if (intervalSize < numeric_limits<double>::epsilon()) {
        distribution[0] += 1.;
        if (distribution.size() > 1)
            distribution.resize(1);
    } else {
        int i = (int) rint((value - min) / intervalSize * 9.);
        assert (i >= 0 && i < 10);
        distribution[i] += 1.;
    }
}

std::string nu::integerToString(int i) {
    if (i >= BILLION)
        return (std::to_string((int) (float(i) / BILLION)) + "B");
    else if (i >= MILLION)
        return (std::to_string((int) (float(i) / MILLION)) + "M");
    else if (i >= KILO)
        return (std::to_string((int) (float(i) / KILO)) + "K");
    else
        return std::to_string(i);
}

int nu::readIntegerString(const std::string &s) {
    std::regex re("(\\d+)([KMB]?)",
                  std::regex_constants::ECMAScript | std::regex_constants::icase);
    auto re_number = std::sregex_token_iterator(s.begin(), s.end(), re, 1);
    auto re_letter = std::sregex_token_iterator(s.begin(), s.end(), re, 2);

    int result = 0;
    result += std::stoi((*re_number).str());
    std::string power = (*(re_letter)).str();
    assert(power.size() == 1);
    if (power[0] == 'M') result *= MILLION;
    else if (power[0] == 'B') result *= BILLION;
    else if (power[0] == 'K') result *= KILO;
    return result;
}