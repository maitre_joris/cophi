#include "util/DimensionSet.h"


void DimensionSet::add(DimensionInterval &c) {
  for (const unsigned int d : c)
    dimensions.insert(d);
}

void DimensionSet::remove(DimensionInterval &c) {
  for (const unsigned int d : c)
    dimensions.erase(d);
}

void DimensionSet::clear() {
  dimensions.clear();
}

bool DimensionSet::isEmpty() const {
  return dimensions.empty();
}

DimensionList DimensionSet::getAsVector() const {
  return std::vector<unsigned int>(dimensions.begin(), dimensions.end());
}