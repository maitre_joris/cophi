#include "util/DimensionInterval.h"
#include <sstream>
#include <algorithm>
#include <util/Utils.h>

DimensionInterval::DimensionInterval() {}

DimensionInterval::DimensionInterval(unsigned int dim) :
        orderedDimensions(1, dim) {
    assert(orderedDimensions.size() == 1);
}

DimensionInterval::DimensionInterval(unsigned int left, unsigned int right) {
    orderedDimensions.resize(right - left + 1);
    for (unsigned i = left; i <= right; ++i)
        orderedDimensions.at(i) = i;
}

unsigned int DimensionInterval::find(unsigned int dim) const {
    for (unsigned int axisId = 0; axisId < orderedDimensions.size(); axisId++) {
        if (orderedDimensions.at(axisId) == dim)
            return axisId;
    }
    throw NotFound();
}

DimensionInterval &DimensionInterval::chain(unsigned int d) {
    orderedDimensions.push_back(d);
    return *this;
}

DimensionInterval &DimensionInterval::chain(const DimensionInterval &dims) {
    for (unsigned int d : dims)
        orderedDimensions.push_back(d);
    return *this;
}

void DimensionInterval::clear() { orderedDimensions.clear(); }

bool DimensionInterval::empty() const { return orderedDimensions.empty(); }

typedef std::vector<unsigned int>::const_iterator const_iterator;

const_iterator DimensionInterval::begin() const {
    return (orderedDimensions.begin());
}

const_iterator DimensionInterval::end() const {
    return (orderedDimensions.end());
}

size_t DimensionInterval::size() const { return orderedDimensions.size(); }


std::string DimensionInterval::toCSV() const {
    std::stringstream ss;
    for (unsigned int i = 0; i < orderedDimensions.size(); i++) {
        ss << orderedDimensions.at(i);
        if (i < orderedDimensions.size() - 1)
            ss << ",";
    }
    return ss.str();
}

void DimensionInterval::insert(int posBefore, const DimensionInterval &dims) {
    emplace<unsigned int>(orderedDimensions, posBefore, dims.size());
    unsigned int axisId = posBefore + 1;
    for (unsigned int dbDim : dims) {
        orderedDimensions.at(axisId) = dbDim;
        axisId++;
    }
}

unsigned int DimensionInterval::at(unsigned int i) const {
    return orderedDimensions.at(i);
}

void DimensionInterval::remove(unsigned int pos, unsigned int size) {
    erase<unsigned int>(orderedDimensions, pos, size);
}

const std::vector<unsigned int> DimensionInterval::asVector() const {
    return orderedDimensions;

}

