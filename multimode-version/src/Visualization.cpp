#include <Visualization.h>

#ifndef TEST

#include <renderer/GlDisplay.h>

#else
#include <renderer/MockDisplay.h>

#endif


#ifdef LOCAL

#include <clustering/KMeans.h>
#include <clustering/AdaptiveBinning.h>
#include <clustering/Binning.h>
#include <clustering/NoClustering.h>
#include <clustering/Canopy.h>
#include <data/FileHierarchicalData.h>

#else

#include <data/RemoteData.h>

#endif

//
//Visualization::Visualization(std::vector<std::vector<float>> &values, unsigned int k) {
//    if (dataSource != nullptr) delete dataSource;
//    if (renderer != nullptr) delete renderer;
//    display = new MockDisplay(&displayParameters);
//    renderer = new Renderer(*display, displayParameters);
//    display->changeViewport(Vec4i(0, 0, width, height));
//#ifdef LOCAL
//    auto *csvHandler = new TableHandler(values);
//    Canopy clustering = Canopy(k);
//    std::set<unsigned int> noClustering;
//    dataSource = new LocalData(renderer,
//                               csvHandler,
//                               &clustering,
//                               noClustering,
//                               configureMultithreading());
//#else
//    RequestHandler requester;
//    dataSource = new RemoteData(nullptr, &requester);
//#endif
//
//}

Visualization::Visualization(unsigned int width, unsigned int height) : width(width), height(height) {
    init();
}


void Visualization::init() {
#ifndef LOCAL
    delete requester;
    requester = nullptr;
#endif
    delete renderer;
    delete display;
#ifndef TEST
    display = new GlDisplay(&displayParameters);
#else
    display = new MockDisplay(&displayParameters);
#endif
    renderer = new Renderer(*display, displayParameters);
    display->changeViewport(Vec4i(0, 0, width, height));
}

#ifdef LOCAL

void Visualization::loadHierarchy(const std::string &dataFile,
                                  const std::string &hierarchyFile) {
    init();
    clear();
    const int n = nu::readIntegerString(hierarchyFile);
    const int k = 31;
    TableHandler *csvHandler = new TableHandler(dataFile, n);
    std::ifstream in = std::ifstream(hierarchyFile);
    if (!in.is_open()) throw "Could not find hierarchy file";
    IClusteredData *dbData = new FileHierarchicalData(in, csvHandler, k);
    dataSource = new LocalData(renderer,
                               csvHandler,
                               dbData,
                               configureMultithreading());
    in.close();
}

void Visualization::loadLocalData(const TableHandler::Configuration &config) {
    init();
    clear();
    TableHandler *csvHandler = nullptr;
    IClustering *clustering = nullptr;
    try {
        csvHandler = new TableHandler(config);

        switch (config.clustering) {
            case IClustering::KMEANS:
                clustering = new KMeans(config.nbClusters, 1E-3, 33, 20);
                if (config.nbClusters == 0) throw ImportError(ImportError::CLUSTERING);
                break;
            case IClustering::ADAPTIVE_BINNING:
                clustering = new AdaptiveBinning(config.nbClusters);
                if (config.nbClusters == 0) throw ImportError(ImportError::CLUSTERING);
                break;
            case IClustering::BINNING:
                clustering = new Binning(config.nbClusters);
                if (config.nbClusters == 0) throw ImportError(ImportError::CLUSTERING);
                break;
            case IClustering::NONE:
                clustering = new NoClustering();
                break;
            case IClustering::CANOPY:
                clustering = new Canopy(config.nbClusters);
                if (config.nbClusters == 0) throw ImportError(ImportError::CLUSTERING);
                break;
        }
        if (clustering == nullptr) throw ImportError(ImportError::CLUSTERING);

        if (config.clustering == IClustering::NONE && config.nbClusters > 0) {
            std::cerr << "No clustering specified even though a number of clusters was. Using default binning."
                      << std::endl; //TODO Feedback
            clustering = new Binning(config.nbClusters);
        }

        std::set<unsigned int> noClustering;
        set_union(config.nominal.begin(),
                  config.nominal.end(),
                  config.ordinal.begin(),
                  config.ordinal.end(),
                  inserter(noClustering, noClustering.begin()));
        dataSource = new LocalData(renderer,
                                   csvHandler,
                                   clustering,
                                   noClustering,
                                   configureMultithreading());
        delete clustering;
    } catch (const ImportError &e) {
        delete clustering;
        delete csvHandler;
        delete dataSource;
        dataSource = nullptr;
        Notifier::onError(e.getType());
        std::rethrow_exception(std::current_exception());
    } catch (const std::exception &e) {
        delete clustering;
        delete csvHandler;
        delete dataSource;
        dataSource = nullptr;
        Notifier::onError(ImportError::UNEXPECTED);
        std::rethrow_exception(std::current_exception());
    }
}

#else

void Visualization::loadDataSet(const std::string &dataSet) {
    if (requester == nullptr) return;
    std::cout << "Loading data set..." << std::endl;
    requester->requestDataSet(dataSet, [this]() {
        this->dataSource = new RemoteData(this->renderer, this->requester);
    });
}

void Visualization::loadRemoteData(const std::string &dataSet,
                                   const std::string &serverAddress) {
    init();
    clear();
    requester = new RequestHandler(serverAddress);
    loadDataSet(dataSet);
}

void Visualization::loadRemoteData(const Server::Configuration &config,
                                   const std::string &dataSet) {
    init();
    clear();
    if (renderer == nullptr) return;
    requester = new RequestHandler(config);
    loadDataSet(dataSet);
}

#endif


bool Visualization::configureMultithreading() const {
#ifdef EMSCRIPTEN
#ifdef __EMSCRIPTEN_PTHREADS__
    return true; //TODO Implement phread support (output file handling, etc)
#else
    return false;
#endif
#else
    return true;
#endif
}

void Visualization::clear() {
    state = DEFAULT;
    requester = nullptr;
    selectedDims.clear();
    deletedDims.clear();
    selectedAxes.clear();
    selectedElements.clear();
    insertAfter = -1;
    animating = Animating::NONE;
    animatingMove = MoveStage::IDLE;
    selectionBox = false;
    movingThumbnail = false;
    movingSlider = false;
    currentModifier = Modifier::NONE;
    doPan = false;
    if (dataSource != nullptr) {
        delete dataSource;
        dataSource = nullptr;
    }
}

void Visualization::capture() {
    fromX = last.x;
    fromY = last.y;
    const DisplayParameters &p = renderer->getParameters();
    fromHeight = p.getHeight();
    fromWidth = renderer->getTotalSpacing();
    fromGap = p.getGapRatio();
    fromEdge = p.getEdgeReduction();
    fromNodeWidth = renderer->getTotalNodeWidth();
    fromCurvature = p.getEdgeCurvature();
    fromContextCompression = p.getContextCompression();
}

void Visualization::changeMode(State s) {
    // TODO: on mode change, unprocessed response should get flushed
    cancelSelection();
    if (s != state) {
        if (s == State::FILTER) renderer->showSliders();
        else if (state == State::FILTER) renderer->hideSliders();
        if (s == State::FOCUS) renderer->hideHighlight();
        state = s;
        capture();
        Notifier::onEvent(s);
    }
}

bool Visualization::isAxisSelectionMode(State s) const {
    return (s == State::MOVE_AXIS ||
            s == State::DELETE_AXIS ||
            s == State::INVERT_AXIS);
}

bool Visualization::isPanMode(State s) const {
    return (s != EDGE_LOOK && s != SCALE);
}

unsigned long Visualization::getNbAxes() const {
    return dataSource->getNbAxes();
}

std::string Visualization::getDimensionLabel(unsigned int dId) const {
    return dataSource->getDimensionLabel(dId);
}

void Visualization::setGap(float x) {
    renderer->setGap(x);
}

unsigned long Visualization::getNbIndividuals() const {
    return (dataSource == nullptr) ? 0 : dataSource->getNbIndividuals();
}

void Visualization::animateMoveInsertion(float time) {
    if (!renderer->isAnimating(Animation::INSERTION))
        renderer->triggerInsertion(time);
    else {
        bool finishedAnimation = renderer->tick(time);
        if (finishedAnimation) {
            animatingMove = MoveStage::IDLE;
            renderer->onAxisChange();
            Notifier::onAxisChange();
            displayParameters.displayLabels();
            displayParameters.displayAxes();
            selectedAxes.clear();
            selectedDims.clear();
        }
    }
}

void Visualization::animateMoveDeletion(float time) {
    if (!renderer->isAnimating(Animation::DELETION) && !renderer->isAnimating(Animation::INSERTION)) {
        renderer->triggerDeletion(time, true);
    } else if (renderer->isAnimating(Animation::DELETION)) {
        bool finishedAnimation = renderer->tick(time);
        if (finishedAnimation) {
            if (selectedAxes.isEmpty() || selectedDims.empty()) return;
            animatingMove = MoveStage::INSERTING;
#ifdef LOCAL
            triggerMoveInsertion();
#else
            ((RemoteData *) dataSource)->prepareInsertion(selectedAxes,
                                                          selectedDims,
                                                          insertAfter);
#endif
        }
    }
}

void Visualization::animateInsertion(float time) {
    if (!renderer->isAnimating(Animation::INSERTION)) {
        renderer->triggerInsertion(time);
    } else {
        animating = renderer->tick(time) ? Animating::NONE : Animating::INSERTION;
        if (animating == Animating::NONE) {
            displayParameters.setDisplayAxes(true);
            displayParameters.setDisplayLabels(true);
            renderer->onAxisChange();
            Notifier::onAxisChange();
        }
    }
}

void Visualization::animateFocus(float time) {
    if (!renderer->isAnimating(Animation::FOCUS)) {
        renderer->triggerFocus(time);
    } else {
        animating = renderer->tick(time) ? Animating::NONE : Animating::FOCUS;
        if (animating == Animating::NONE) {
            displayParameters.setDisplayAxes(true);
            displayParameters.setDisplayLabels(true);
            activateEnhancement = true;
        }
    }
}

void Visualization::animateDeletion(float time) {
    if (!renderer->isAnimating(Animation::DELETION)) {
        renderer->triggerDeletion(time, false);
    } else {
        animating = renderer->tick(time) ? Animating::NONE : Animating::DELETION;
        if (animating == Animating::NONE) {
            Notifier::onAxisChange();
        }
    }
}

void Visualization::readAxisOperationResponse() {
#ifndef LOCAL
    if (requester == nullptr) return;
    switch (state) {
        case DEFAULT:
            break;
        case DELETE_AXIS:
            if (!renderer->isHighlighting() ||
                requester->receivedResponse(RequestHandler::HIGHLIGHTING))
                triggerDeletion();
            break;
        case INSERT_DIMENSION:
            if (!renderer->isHighlighting() ||
                requester->receivedResponse(RequestHandler::HIGHLIGHTING))
                triggerInsertion();
            break;
        case MOVE_AXIS:
            if (animatingMove == MoveStage::DELETING) {
                if (!renderer->isHighlighting() ||
                    requester->receivedResponse(RequestHandler::HIGHLIGHTING))
                    triggerMoveDeletion();
            } else if (animatingMove == MoveStage::INSERTING) {
                if (!renderer->isHighlighting() ||
                    requester->receivedResponse(RequestHandler::HIGHLIGHTING))
                    triggerMoveInsertion();
            }
            break;
        case FOCUS:
            animating = Animating::FOCUS;
            activateEnhancement = true;
            renderer->onFocus();
        default:
            break;
    }
#endif
}

void Visualization::readHighlightResponse() {
#ifndef LOCAL
    if (state == State::DEFAULT && !selectedElements.empty() && selectedElements.at(0).isValid()) {
        ((RemoteData *) dataSource)->displayHighlighting();
        selectedElements.clear();
    } else if (state == FILTER) {
        ((RemoteData *) dataSource)->displayHighlighting();
    }
#endif
}


void Visualization::triggerMoveInsertion() {
    assert(!selectedAxes.isEmpty());
    assert(!selectedDims.empty());
    if (selectedAxes.isBefore(insertAfter))
        renderer->setDimensionsToInsert(selectedDims,
                                        insertAfter - (int) selectedAxes.size());
    else
        renderer->setDimensionsToInsert(selectedDims, insertAfter);
}


void Visualization::triggerMoveDeletion() {
    assert(!selectedDims.empty());
    assert(!selectedAxes.isEmpty());
    renderer->setAxesToDelete(selectedAxes);
}


void Visualization::onIdle(float time) {
#ifndef LOCAL
    if (requester != nullptr) {
        try {
            requester->performRequests();
            if (requester->receivedResponse(RequestHandler::INTERVAL))
                readAxisOperationResponse();
            if (requester->receivedResponse(RequestHandler::HIGHLIGHTING))
                readHighlightResponse();
        } catch (const std::exception &e) {
            delete requester;
            Notifier::onError(ImportError::SERVER);
            std::rethrow_exception(std::current_exception());
        }
    }
#endif
    // For the sake of initial data set fetching, request should be perform
    // even if no DataHandler have been instantiated
    if (dataSource == nullptr) return;
    if (animating == Animating::DELETION) animateDeletion(time);
    else if (animating == Animating::INSERTION) animateInsertion(time);
    else if (animating == Animating::FOCUS) animateFocus(time);
    else if (animatingMove != MoveStage::IDLE) {
        if (animatingMove == MoveStage::DELETING) animateMoveDeletion(time);
        else if (animatingMove == MoveStage::INSERTING) animateMoveInsertion(time);
    } else renderer->tick(time);
}

Visualization::~Visualization() {
    delete renderer;
    renderer = nullptr;
    delete dataSource;
    dataSource = nullptr;
    delete display;
    display = nullptr;
}

void Visualization::center() {
    if (dataSource != nullptr)
        display->center(dataSource->getNbAxes());
}

void Visualization::fit() {
    if (dataSource != nullptr) {
        display->fit(dataSource->getNbAxes());
        renderer->onAxisChange();
    }
}

void Visualization::switchParameter(SwitchableParameter parameter, float time) {
    // Here are any non-animated changes
    // Some good use some transition animation
    DisplayParameters &dp = renderer->getParameters();

    std::function<int(int)> switchBinaryOption = [](int current) -> int {
        return current == 0 ? 1 : 0;
    };

    switch (parameter) {
        case SwitchableParameter::INNER_NODE_VIEW:
            dp.setDistortion((bool) switchBinaryOption(static_cast<int>(dp.displayDistortion())));
            break;
        case SwitchableParameter::INTERVAL_HIGHLIGHT_MODE:
            dp.setIntervalHighlightMode((IntervalHighlight) switchBinaryOption(dp.getIntervalHighlightMode()));
            break;
        case SwitchableParameter::GRADIENT_SCOPE:
            dp.setGlobalGradient(!dp.globalGradientMode());
            break;
        case SwitchableParameter::GRADIENT:
            dp.nextGradient();
            break;
        case SwitchableParameter::CONTEXT_COMPRESSION_MODE:
            dp.setContextMode((ContextCompression) switchBinaryOption(dp.getContextMode()));
            break;
        case SwitchableParameter::HEIGHT_MAPPING:
            renderer->hideSliders();
            dp.setHeightMappingMode((HeightMapping) switchBinaryOption(dp.getHeightMappingMode()));
            renderer->animateMappingChange(time);
            break;
        case SwitchableParameter::DISPLAY_AXES:
            dp.setDisplayAxes(!dp.displayAxes());
            break;
        case SwitchableParameter::DISPLAY_LABELS:
            dp.setDisplayLabels(!dp.displayLabels());
            break;
    }
}


bool Visualization::isOutOfScreen(float x, float y) {
    return (x < 0 || x > width || y < 0 || y > height);
}

void Visualization::onMouseMove(float x, float y) {
    last.update(x, y);
}


void Visualization::onActiveMouseMove(float x, float y) {
    const float dx = x - fromX;
    const float dy = fromY - y;
    if (doPan) {
        display->translate(dx, dy);
        fromX = x;
        fromY = y;
        return;
    }
    // No pan modes
    if (state == SCALE) {
        scale(dx, dy);
        return;
    }
    if (state == EDGE_LOOK) {
        renderer->setEdgeReduction(
                fromEdge + (dx / (float) height) * ALTER_SENSITIVITY);
        renderer->setEdgeCurvature(
                fromCurvature + (dy / (float) height) * ALTER_SENSITIVITY);
        return;
    }
        // Special mode: pan only if not moving a slider
    else if (state == FILTER) {
#ifndef HIERARCHICAL
        if (movingSlider) renderer->onSliderMove(x, y);
        else doPan = true;
        return;
#endif
    }
    // Axis selection modes: pan on outer drag and inner drag (except for move mode)
    if (isAxisSelectionMode(state)) {
        if (state == MOVE_AXIS && !selectedAxes.isEmpty()) {
            // Case 1: Update or create thumbnail
            if (movingThumbnail) {
                renderer->updateAxisThumbnailPosition(x, y);
                return;
            }
            if (renderer->isInnerArea(x, y) &&
                renderer->selectionBoxContains(x, y)) {
                initiateAxisThumbnail(x, y);
                return;
            }
        } else if (selectedAxes.isEmpty() && selectionBox) {
            // Case 2: Update selection box drawing
            renderer->updateSelectionBox(x, y);
            return;
        }
    }
    // Finally activate pan
    if (abs(dx) > MIN_MOVE || abs(dy) > MIN_MOVE)
        doPan = true;
}

/*
void Visualization::preHighlighting(Modifier mod) {
#ifdef LOCAL
    if (renderer->isHighlighting() && mod != Modifier::NONE) {
        ((LocalData *) dataSource)->saveHighlightPreview();
        Notifier::onHighlight(true);
        return;
    }
    ((LocalData *) dataSource)->setCurrentHighlightOp(modifierToOp(mod));
#endif
}*/

bool Visualization::focus(float x, float y) {
    const node n = renderer->getNode(x, y);
    if (n.isValid()) {
        const NodeProp np = dataSource->getNodeProperty(n);
        if (np.isMeta()) {
            unsigned int k = renderer->getNbClosing(x, y, n);
            std::cout << "Focusing out " << np.dimension << ", closing " << k << " levels" << std::endl;
            activateEnhancement = false;
            renderer->setFocusClosingAxis(np.dimension, k);
        } else if (np.focusable()) {
            std::cout << "Requested focus activity on (" << np.dimension << ", " << np.clusterId << " [" << np.min
                      << ";"
                      << np.max << "])"
                      << std::endl;
            activateEnhancement = false;
            renderer->setFocusOpeningNode(np.dimension, np.clusterId);
        } else {
            std::cout << "Node has no children" << std::endl;
            return false;
        }

#ifdef LOCAL
        animating = Animating::FOCUS;
        activateEnhancement = true;
        renderer->onFocus();
#endif
    } else {
        std::cout << "Invalid node" << std::endl;
    }
    return n.isValid();
}


bool Visualization::highlight(std::vector<SelectedElement> &elts) {
    dataSource->prepareHighlight(modifierToOp(currentModifier));
    renderer->hideEnhancedElements();
    try {
#ifdef LOCAL
        bool selectContext = true;
#else
        bool selectContext = false;
#endif
        bool isValid = true;
        for (const SelectedElement &e : elts) {
            if (!e.isValid() || (!selectContext && dataSource->isContext(e))) {
                isValid = false;
                break;
            }
        }

        if (isValid) {
            dataSource->applyHighlight(elts);
            Notifier::onHighlight(true);
        } else {
            if (currentModifier == Modifier::NONE) {
                renderer->hideHighlight();
                Notifier::onHighlight(false);
            }
        }
        return isValid;
    } catch (InvalidStateError &e) {
        std::cerr << e.what() << std::endl;
        return false;
    }
    catch (const DataException &e) {
        std::cerr << e.what() << std::endl;
        printError();
        return false;
    }
}

void Visualization::printError() const {
    std::cerr << "Invalid operation: cancelling" << std::endl;
}

void Visualization::initiateBoxSelection(float x, float y) {
    assert(!movingThumbnail);
    selectionBox = true;
    selectedAxes.clear(); // Any selection start overrides past selection
    renderer->startSelectionBox(x, y);
}

void Visualization::terminateBoxSelection() {
    try {
        assert(selectionBox);
        selectedAxes = renderer->endSelectionBox();
        if (selectedAxes.isEmpty()) cancelSelection();
    } catch (const CanceledOperation &e) {
        cancelSelection();
    }
}

void Visualization::terminateAxisThumbnail(float x, float y) {
    assert(movingThumbnail);
    assert(!selectedAxes.isEmpty());
    insertAfter = std::get<0>(renderer->getSurroundingAxes(x, y));
    assert(insertAfter >= -1);
    if (selectedAxes.contains((unsigned int) insertAfter) ||
        selectedAxes.contains((unsigned int) insertAfter + 1)) {
        printError();
        cancelMove();
    } else {
        movingThumbnail = false;
        renderer->hideThumbnail();
        selectedDims = dataSource->toDimensions(selectedAxes);
        //renderer->setAnimatingSwap(true); // Needed for glitch in distorted axis drawing
        animatingMove = MoveStage::DELETING;
#ifdef LOCAL
        renderer->setAxesToDelete(selectedAxes);
#else
        bool noResourceNeeded = ((RemoteData *) dataSource)->prepareDeletion(selectedAxes);
        if (noResourceNeeded) renderer->setAxesToDelete(selectedAxes);
#endif
    }
    selectionBox = false;
    renderer->hideSelectionBox();
}

void Visualization::cancelSelection() {
    assert(!movingThumbnail);
    selectedAxes.clear();
    selectedDims.clear();
    //if (movingThumbnail) cancelMove();
    selectionBox = false;
    renderer->hideSelectionBox();
}

void Visualization::cancelMove() {
    movingThumbnail = false;
    renderer->hideThumbnail();
    selectedAxes.clear();
    selectedDims.clear();
    insertAfter = NONE_ID;
}

void Visualization::passiveAxisSelection(float x, float y) {
    // Passive selection ensures that, if existing, the underlying axis is
    // automatically selected when starting any operation
    // Passive selection happens only if the mode is compatible, and no
    // other operation active (selection drawing or axis movingThumbnail)
    assert(!movingThumbnail);
    //if (movingThumbnail) return; // Necessary?
    try { selectedAxes = renderer->passiveSelectionBox(x, y); }
    catch (const CanceledOperation &e) {
        printError();
    } //Do nothing?
}

HighlightingOp Visualization::modifierToOp(Modifier mod) {
    switch (mod) {
        case Modifier::SHIFT:
            return HighlightingOp::add;
        case Modifier::CONTROL:
            return HighlightingOp::intersect;
        case Modifier::ALT:
            return HighlightingOp::substract;
        case Modifier::NONE:
            return HighlightingOp::replace;
    }
}


void Visualization::previewHighlight(float x, float y) {
#ifdef LOCAL
//    auto *localDataSource = (LocalData *) dataSource;
//    if (currentModifier == Modifier::NONE) return;
//    assert(renderer->isHighlighting() && state == State::DEFAULT);
//    SelectedElement newElement = renderer->getElement(x, y);
//    HighlightingOp newOp = modifierToOp(currentModifier);
//    HighlightingOp lastOp = localDataSource->getCurrentHighlightOp();
//    if (!newElement.isValid()) {
//        hideHighlightPreview();
//    } else {
//        bool alreadyActive = (selectedElements.size() == 1 && selectedElements.at(0) == newElement);
//        if (!alreadyActive || newOp != lastOp) {
//            localDataSource->setCurrentHighlightOp(newOp);
//            dataSource->applyHighlight(newElement);
//        }
//    }
//    tmpSelectedElements = selectedElements;
    // Apply op?
#endif
}

void Visualization::hideHighlightPreview() {
#ifdef LOCAL
//    if (renderer->isHighlighting()) {
//        ((LocalData *) dataSource)->clearHighlightPreview();
//    }
//    tmpSelectedElements.clear();
#endif
}

void Visualization::initiateAxisThumbnail(float x, float y) {
    if (movingThumbnail) return;
    assert(!selectedAxes.isEmpty());
    movingThumbnail = true;
    selectionBox = false;
    selectedDims = dataSource->toDimensions(selectedAxes);
    // Keeping selection box here to preserve knowledge of the original
    // place of what is moved
    renderer->triggerAxisThumbnail(selectedAxes);
    renderer->updateAxisThumbnailPosition(x, y);
}

void Visualization::insertDimension(unsigned int d) {
    //cout << "Setting dimension " << d << " for insertion" << endl;
    if (deletedDims.isEmpty()) return;
    renderer->hideHighlight();
    selectedDims = DimensionInterval(d);
    insertAfter = std::get<0>(renderer->getSurroundingAxes(futureInsertionPos.getX(),
                                                           futureInsertionPos.getY()));
#ifndef LOCAL
    ((RemoteData *) dataSource)->prepareInsertion(selectedAxes,
                                                  selectedDims,
                                                  insertAfter);
#else
    triggerInsertion();
#endif
}

void Visualization::deleteAxis(unsigned int d) {
    selectedAxes = AxisInterval(d, d);
#ifdef LOCAL
    triggerDeletion();
#else
    bool noResourceNeeded = ((RemoteData *) dataSource)->prepareDeletion(selectedAxes);
    if (noResourceNeeded)
        triggerDeletion();
#endif
}


Pick Visualization::pickElement(float x, float y) {
    const SelectedElement e = renderer->getElement(x, y);
    if (!e.isValid()) {
        renderer->hideEnhancedElements();
        return Pick();
    }
    if (renderer->isHighlighting()) {
        const Highlight *h = dataSource->getCurrentHighlight();
        assert(h != nullptr);
        if (e.n.isValid()) {
            const auto &np = dataSource->getNodeProperty(e.n);
            if (h->hasNode(e.n)) {
                renderer->enhanceElement(x, y);
                return createNodePick(np, h->getNodeSubWeight(e.n) * np.weight / h->getNbIndividuals());
            }

            return createNodePick(np, 0.f);

        } else if (e.e.isValid()) {
            const auto &ep = dataSource->getEdgeProperty(e.e);
            if (h->hasEdge(e.e)) {
                return createEdgePick(ep.weight, h->getEdgeSubWeight(e.e) * ep.weight / h->getNbIndividuals());
            }

            return createEdgePick(ep.weight, 0.f);

        } else return Pick();
        //}
    } else return createRegularPick(e);
}

Pick Visualization::createNodePick(const NodeProp &np, float weightRatio) {
    Pick i;
    bool isCategory = false;
    std::cout << "Picked node ";
#ifdef LOCAL
    std::string label = ((LocalData *) dataSource)->getLabel(np.dimension, (int) np.min);
    if (!label.empty()) {
        isCategory = true;
        std::cout << label;
        i.categorical = label;
    }
#endif
    i.numerical.push_back(float(np.weight));
    if (!isCategory) {
        i.numerical.push_back(np.min);
        i.numerical.push_back(np.max);
        std::cout << "[" << np.min << ";" << np.max << "];";
    }
    if (weightRatio > 0.f) {
        i.numerical.push_back(weightRatio * 100.f);
        std::cout << ", " << weightRatio * 100.f << "% of highlighted subset";
    }
    std::cout << std::endl;
    return i;
}

Pick Visualization::createEdgePick(float weight, float weightRatio) {
    Pick i;
    i.numerical.push_back(weight);
    std::cout << "Picked edge [" << weight << "]";
    if (weightRatio > 0.f) {
        i.numerical.push_back(weightRatio * 100.f);
        std::cout << ", highlighted: " << weightRatio * 100.f << "%";
    }
    std::cout << std::endl;
    return i;
}

Pick Visualization::createRegularPick(const SelectedElement &e) {
    if (e.n.isValid()) return createNodePick(dataSource->getNodeProperty(e.n), 0.f);
    if (e.e.isValid()) return createEdgePick(dataSource->getEdgeProperty(e.e).weight, 0.f);
    else return Pick();
}

void Visualization::triggerDeletion() {
    assert(!selectedAxes.isEmpty());
    renderer->setAxesToDelete(selectedAxes);
    animating = Animating::DELETION;
    deletedDims.add(selectedDims);
}

void Visualization::triggerInsertion() {
    assert(!selectedDims.empty());
    renderer->setDimensionsToInsert(selectedDims, insertAfter);
    animating = Animating::INSERTION;
    deletedDims.remove(selectedDims);
    selectedDims.clear();
}

void Visualization::onSelectionBoxClick(float time) {
    assert(!selectedAxes.isEmpty());
    // Hide selection box and trigger operations
    if (state == State::DELETE_AXIS) {
        renderer->hideSelectionBox();
        selectionBox = false;
        selectedDims = dataSource->toDimensions(selectedAxes);
        assert(selectedDims.size() == selectedAxes.size());
#ifdef LOCAL
        triggerDeletion();
#else
        bool noResourceNeeded = ((RemoteData *) dataSource)->prepareDeletion(selectedAxes);
        if (noResourceNeeded)
            triggerDeletion();
#endif
    } else if (state == State::INVERT_AXIS) {
        selectionBox = false;
        renderer->hideSelectionBox();
        renderer->triggerInversion(selectedAxes, time);
        selectedAxes.clear();
    }
}


void Visualization::onMouseWheel(float x, float y, float dy) {
    // Wild fix for emscripten issue #3171
#ifdef EMSCRIPTEN
    dy = -dy;
#endif
    float sign = std::signbit(dy) ? -1.f : 1.f;
    if (state == State::VARIATION) {
        if (currentModifier == Modifier::SHIFT) {
            selectedEdges.setRange(selectedEdges.getRange() + sign * 0.01f);
        } else if (currentModifier == Modifier::NONE) {
            selectedEdges.setCenter(selectedEdges.getCenter() + sign * 0.01f);
        }
        enhanceFromEdgeVariation(x, y);
    } else {
        if (dy > 0) display->zoom(x, y, 1.2);
        else display->zoom(x, y, 0.8);
    }
}

void Visualization::onReshape(unsigned int width, unsigned int height) {
    this->width = width;
    this->height = height;
    display->changeViewport(Vec4i(0, 0, (int) width, (int) height));
}

void Visualization::onDraw() {
    renderer->draw();
}

bool Visualization::isHighlighting() {
    return renderer->isHighlighting();
}

bool Visualization::isReady() const {
    return (dataSource != nullptr &&
            dataSource->getNbSourceDimensions() != 0 &&
            !isAnimating()
            && renderer != nullptr);
}

void Visualization::onLeftButtonPress(float x, float y) {
    capture();
    if (state == State::FILTER)
        movingSlider = renderer->sliderClick(x, y);
    // Axis selection initialization
    if (isAxisSelectionMode(state)) {
        bool inner = renderer->isInnerArea(x, y);
        if (inner) {
            if (selectedAxes.isEmpty()) {
                initiateBoxSelection(x, y);
            } else if (!renderer->selectionBoxContains(x, y)) {
                cancelSelection();
                initiateBoxSelection(x, y);
            }
        }
    } else if (state != State::FOCUS) {
        renderer->hideEnhancedElements();
    }
}

void Visualization::highlightFromEdgeVariation(float x, float y) {
#ifdef LOCAL
    selectedElements.clear();

    int axis = renderer->getSurroundingAxes(x, y).first;
    if (axis != NONE_ID) {
        std::vector<edge> edges = renderer->getEdges(axis, selectedEdges.getMin(), selectedEdges.getMax());
        if (edges.empty()) {
            renderer->hideHighlight();
            return;
        }
        for (const edge &e:edges)
            selectedElements.emplace_back(e);
        highlight(selectedElements);
    } else {
        renderer->hideHighlight();
    }
#endif
}

void Visualization::onLeftButtonRelease(float x, float y, float time) {
    renderer->hideEnhancedElements();
    if (doPan) {
        doPan = false;
        return;
    }
    if (state == DEFAULT) {
        selectedElements.clear();
        SelectedElement e = renderer->getElement(x, y);
        selectedElements.push_back(e);
        highlight(selectedElements);
    } else if (state == State::VARIATION) {
        highlightFromEdgeVariation(x, y);
    } else if (state == PICKER) {
        pick = pickElement(x, y);
        if (!pick.empty())
            renderer->enhanceElement(x, y);
    } else if (state == FILTER) {
        renderer->onSliderRelease();
        Notifier::onHighlight(true);
    } else if (state == FOCUS) {
#ifdef HIERARCHICAL
        focus(x, y);
#endif
    } else if (state == INSERT_DIMENSION)
        setFutureInsertionPosition(x, y);
    else if (isAxisSelectionMode(state)) {
        // Either ending a box selection or triggering an operation
        if (selectedAxes.isEmpty()) {
            if (selectionBox) terminateBoxSelection();
        } else if (!selectedAxes.isEmpty()) {
            if (state == MOVE_AXIS && movingThumbnail)
                terminateAxisThumbnail(x, y);
            else if (renderer->isInnerArea(x, y) &&
                     renderer->selectionBoxContains(x, y))
                onSelectionBoxClick(time);
            else cancelSelection();
        }
    }
}

void Visualization::enhanceFromEdgeVariation(float x, float y) {
    int axis = renderer->getSurroundingAxes(x, y).first;
    if (axis != NONE_ID) {
        renderer->enhanceEdges(axis, selectedEdges.getMin(), selectedEdges.getMax());
    } else {
        renderer->hideEnhancedElements();
    }
}

void Visualization::onPassiveMouse(float x, float y) {
    if (state == State::DEFAULT) {
        if (activateEnhancement) {
            renderer->enhanceElement(x, y, false);
        }
        if (isHighlighting() && currentModifier != Modifier::NONE) {
            previewHighlight(x, y); // Not doing anything if no mod
        }
    } else if (state == State::FOCUS) {
        if (activateEnhancement)
            renderer->enhanceElement(x, y, true);
    } else if (state == State::PICKER) {
        if (activateEnhancement)
            renderer->enhanceElement(x, y, false);
    } else if (state == State::VARIATION) {
        enhanceFromEdgeVariation(x, y);
    } else {
        if (activateEnhancement)
            renderer->hideEnhancedElements();
        if (isAxisSelectionMode(state)) {
            // Axis operation auto-mono-selection
            if (!selectionBox && !movingThumbnail)
                passiveAxisSelection(x, y);
        }
    }
}

bool Visualization::isAnimating() const {
    return (animatingMove != MoveStage::IDLE ||
            animating != Animating::NONE);
}

void Visualization::exportData(std::string &result,
                               bool inFile,
                               bool highlightedOnly,
                               bool includeDeletedDimensions) {
    std::ostream *os = nullptr;
    if (inFile) {
        os = new std::ofstream(result, std::ios_base::trunc);
        if (!((std::ofstream *) os)->is_open())
            throw std::logic_error(std::string("Unable to open file ").append(result));
    } else os = new std::ostringstream(result);
#ifdef LOCAL
    ((LocalData *) dataSource)->exportData(os,
                                           highlightedOnly,
                                           includeDeletedDimensions);
#endif
    if (inFile)
        ((std::ofstream *) os)->close();
    else result = ((std::ostringstream *) os)->str();
}

void Visualization::scale(float dx, float dy) {
    const float d = display->windowToModelFactor();
    const float mdx = d * dx;
    const float mdy = d * dy;
    selectionBox = false;
    renderer->hideSelectionBox();
    if (currentModifier == Modifier::NONE) {
        // No modifier, enlarge inter-axis space and resize axis height
        renderer->setTotalSpacing(fromWidth + mdx);
        renderer->setHeight(fromHeight + mdy);
    } else if (currentModifier == Modifier::SHIFT) {
        // With some modifier, enlarge node + gap
        renderer->setTotalNodeWidth(fromNodeWidth + mdx);
        renderer->setGap(fromGap + (dy / (float) height) * ALTER_SENSITIVITY);
    } else if (currentModifier == Modifier::CONTROL) {
#ifdef HIERARCHICAL
        renderer->setContextCompression(fromContextCompression + (dy / (float) height) * ALTER_SENSITIVITY);
#endif
    }
}

Pick Visualization::getPick() {
    return pick;
}

DimensionSet Visualization::getDeletedDims() {
    return deletedDims;
}

void Visualization::setFutureInsertionPosition(float x, float y) {
    futureInsertionPos = Vec2f(x, y);
}

void Visualization::setColorScheme(ColorScheme::Type scheme) {
    if ((unsigned int) scheme >= ColorScheme::SCHEMES.size()) return;
    renderer->setColorGradient((unsigned int) scheme);
}

void Visualization::setCurrentModifier(Modifier mod) {
    currentModifier = mod;
}


Visualization::Modifier Visualization::getCurrentModifier() const {
    return currentModifier;
}

unsigned int Visualization::getNbSourceDimensions() const {
    return dataSource->getNbSourceDimensions();
}
