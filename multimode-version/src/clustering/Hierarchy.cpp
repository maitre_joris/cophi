#include <iostream>
#include "clustering/Hierarchy.h"

const cluster HNode::ROOT_NODE_ID = -3;


HNode::HNode() : ClusterProp(HNode::INVALID_ID) {}


HNode::HNode(float value,
             const Individuals &occurrences, cluster id) :
        ClusterProp(id, value, occurrences.size()) {
    assert(std::isfinite(value));
    assert(std::isfinite(min));
    assert(std::isfinite(max));

#ifndef NO_DISTRIBUTIONS
    distribution = Distribution(1, occurrences.size());
    childrenBiggestBucket = 0;
#endif
}

void HNode::renumberChildren() {
    std::sort(children.begin(), children.end());
    cluster index = 0;
    for (HNode &child : children) {
        child.clusterId = index;
        index++;
    }
}

HNode::HNode(cluster id) : ClusterProp(id) {
    min = std::numeric_limits<float>::infinity();
    max = -min;
}

HNode::HNode(const std::vector<HNode> &childrenNodes, cluster id) : ClusterProp(id),
                                                                    children(std::move(childrenNodes)) {
    renumberChildren();
    min = std::numeric_limits<float>::infinity();
    max = -min;
    for (HNode &child : children) {
        min = std::min(min, child.getMin());
        max = std::max(max, child.getMax());
        weight += child.getWeight();
    }

#ifndef NO_DISTRIBUTIONS
    float intervalSize = (max - min);
    if (intervalSize >= std::numeric_limits<float>::epsilon()) {
        distribution = Distribution(DISTRIBUTION_BUCKET_NB, 0);
    } else {
        if (children.size() == 1) distribution = children.begin()->distribution;
    }
    childrenBiggestBucket = 0;
    // For each child bucket, use bucket interval's central point
    for (const HNode &child : children) {
        float childInterval = child.max - child.min;
        if (childInterval <= std::numeric_limits<float>::epsilon()) {
            float distanceToParentMin = child.min - min;
            int parentBucket = -1;
            if (distanceToParentMin <= std::numeric_limits<float>::epsilon()) parentBucket = 0;
            else parentBucket = (int) rint(distanceToParentMin / intervalSize * (DISTRIBUTION_BUCKET_NB - 1));
            assert(parentBucket >= 0);
            childrenBiggestBucket = std::max(childrenBiggestBucket, child.distribution.at(0));
            distribution.at(parentBucket) += child.distribution.at(0);
        } else {
            for (unsigned int i = 0; i < DISTRIBUTION_BUCKET_NB; i++) {
                childrenBiggestBucket = std::max(childrenBiggestBucket, child.distribution.at(i));
                float midBucketPoint = child.min + i * childInterval / DISTRIBUTION_BUCKET_NB;
                int parentBucket = (int) rint((midBucketPoint - min) / intervalSize * (DISTRIBUTION_BUCKET_NB - 1));
                if (parentBucket >= 0 && (unsigned int) parentBucket < DISTRIBUTION_BUCKET_NB) {
                    distribution.at(parentBucket) += child.distribution.at(i);
                } else {
                    std::cerr << "Error" << std::endl;
                }
            }
        }
    }
#endif
    //assert(!empty());
}

unsigned int HNode::getChildrenBiggestBucket() const {
    return childrenBiggestBucket;
}

HNode::HNode(const HNode &o, unsigned int id) : ClusterProp(o),
                                                children(o.children),
                                                childrenBiggestBucket(o.childrenBiggestBucket) {
    clusterId = id;
    assert(std::isfinite(min));
    assert(std::isfinite(max));
    reLink();
}

unsigned int HNode::getMaxDepth() const {
    if (children.empty()) return 0;
    else {
        unsigned int max = 0;
        for (const auto &child : children)
            max = std::max(child.getMaxDepth(), max);
        return max + 1;
    }
}

cluster HNode::findInChildren(float value) const {
    if (children.empty()) return ClusterProp::INVALID_ID;
    cluster result = INVALID_ID;
    childrenBinarySearch([value](const HNode *n) {
                             if (n->min < value &&
                                 n->max < value)
                                 return -1;
                             else if (n->min > value &&
                                      n->max > value)
                                 return +1;
                             else return 0;
                         },
                         [this, &result, value](unsigned int p, bool isValid) {
                             if (isValid) result = children.at(p).getId();
                             else result = INVALID_ID;
                         });
    return result;
}

unsigned int HNode::getNbDescendants() const {
    if (children.empty()) return 1;
    else {
        unsigned int n = 0;
        for (const auto &child : children)
            n += child.getNbDescendants();
        return n + 1;
    }
}

HNode *HNode::getParent() const {
    return parent;
}

void HNode::reLink() {
    for (auto &child : children) {
        child.parent = this;
        child.reLink();
    }
}

cluster HNode::getMatchingChildrenId(std::function<bool(const HNode &)> f) const {
    for (const HNode &child: children)
        if (f(child)) return child.getId();
    return INVALID_ID;
}

void HNode::applyChildren(std::function<void(const HNode &)> f) const {
    for (const HNode &child: children)
        f(child);
}

float HNode::distance(const HNode &o) const {
    return std::min(std::abs(max - o.min), std::abs(min - o.max));
}

/*bool HNode::operator<(const HNode &other) const {
    return min < other.min;
}*/

bool HNode::isValid(unsigned int k, int l) const {
    //if (empty()) return false;
    bool itself = (children.size() <= k);
    if (!itself) std::cerr << "At level: " << l << " found " << children.size() << " nodes" << std::endl;
#ifndef NO_DISTRIBUTIONS
    //if (distribution.empty()) return false;
    //itself = itself && included.size() <= k;
#endif
    if (itself) {
        for (const HNode &child : children)
            if (!child.isValid(k, l + 1)) return false;
        return true;
    } else return false;
}

unsigned int HNode::getChildPos(cluster id) {
    unsigned int pos = 0;
    if (children.empty()) return 0;
    childrenBinarySearch([id](const HNode *n) {
                             if (n->getId() > id) return +1;
                             else if (n->getId() < id) return -1;
                             else return 0;
                         },
                         [this, &pos, id](unsigned int p, bool) {
                             if (children.at(p).getId() < id) pos = p + 1;
                             else pos = p;
                         });
    return pos;
}

const HNode *HNode::getChild(cluster id) const {
    const HNode *result = nullptr;
    if (children.empty()) return result;
    childrenBinarySearch([id](const HNode *n) {
                             if (id < n->getId()) return +1;
                             else if (id > n->getId()) return -1;
                             else return 0;
                         },
                         [this, &result, id](unsigned int p, bool isValid) {
                             if (isValid) result = &children.at(p);
                             else result = nullptr;
                         });
    return result;
}

void HNode::childrenBinarySearch(std::function<int(const HNode *)> compFunc,
                                 std::function<void(unsigned int, bool)> resultFunc) const {
    assert(!children.empty());
    int a = 0;
    int b = children.size() - 1;
    int m = (a + b) / 2;
    while (a < b) {
        int comp = compFunc(&children.at(m));
        if (comp < 0) {
            a = m + 1;
        } else if (comp > 0) {
            b = m - 1;
        } else if (comp == 0) {
            return resultFunc(m, true);
        }
        m = (a + b) / 2;
    }
    return resultFunc(m, compFunc(&children.at(m)) == 0);
}

HNode *HNode::insertChild(cluster id, unsigned int at) {
    HNode newNode = HNode(id);
    children.insert(children.begin() + at, newNode);
    return &children.at(at);
}

std::vector<HNode> &HNode::getChildren() {
    return children;
}