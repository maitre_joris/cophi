#include <clustering/IClustering.h>
#include <unordered_map>

using namespace std;

void IClustering::renumberClusters(const set<cluster> &assigned,
                                   vector<cluster> &result) {
    unordered_map<cluster, cluster> reIndexed;
    int index = 0;
    for (cluster id : assigned) {
        if (id != index) reIndexed.insert({id, index});
        index++;
    }
    if (reIndexed.empty()) return;
    for (cluster &valueCluster : result) {
        if (reIndexed.find(valueCluster) != reIndexed.end())
            valueCluster = reIndexed.at(valueCluster);
    }
}

void IClustering::reorderClusters(const map<float, cluster> &assigned,
                                  vector<cluster> &result) {
    unordered_map<cluster, cluster> reIndexed;
    int index = 0;
    for (const auto &p : assigned) {
        if (p.second != index) reIndexed.insert({p.second, index});
        index++;
    }
    if (reIndexed.empty()) return;
    for (cluster &valueCluster : result) {
        if (reIndexed.find(valueCluster) != reIndexed.end())
            valueCluster = reIndexed.at(valueCluster);
    }
}