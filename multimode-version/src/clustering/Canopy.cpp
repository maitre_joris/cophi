#include <cmath>
#include <unordered_set>
#include <iostream>
#include <map>
#include "clustering/Canopy.h"

Canopy::Canopy(unsigned int k) : HClustering(k) {

}

float minDistance(const std::vector<float> &input, int binSize) {
    if (binSize == 1) {
        float min = std::numeric_limits<float>::infinity();
        for (float f : input) {
            for (float g : input) {
                if (f != g)
                    min = std::min(min, std::abs(f - g));
            }
        }
        return min;
    } else {
        float min = std::abs(input.at((unsigned long) (binSize - 1)) - (input.at(0)));
        for (unsigned int i = 0; i < input.size() - binSize - 1; i++) {
            float intervalLength = std::abs(input.at(i + binSize - 1) - (input.at(i)));
            assert(intervalLength >= 0);
            if (intervalLength > 0) {
                min = std::min(min, intervalLength);
            }
        }
        return min;
    }
}

float intervalLength(const std::vector<float> &input) {
    float min = std::numeric_limits<float>::infinity();
    float max = -std::numeric_limits<float>::infinity();
    for (unsigned int i = 0; i < input.size(); i++) {
        min = std::min(min, input.at(i));
        max = std::max(max, input.at(i));
    }
    return std::abs(max - min);
}

unsigned int Canopy::run(const std::vector<float> &input,
                         std::vector<cluster> &result) {
    result.resize(input.size());
    unsigned int k = getNbClusters();
    float distance = intervalLength(input) / (k / 2.f) / 2.f;
    std::unordered_set<unsigned int> assigned;
    std::map<float, cluster> clusters;

    // Creating canopies
    for (unsigned int i = 0; i < input.size(); i++) {
        float value = input.at(i);
        if (assigned.find(i) == assigned.end()) {
            assigned.insert(i);
            result.at(i) = i;
            clusters.insert({value, i});
            for (unsigned int j = 0; j < input.size(); j++) {
                float oValue = input.at(j);
                if (j != i && assigned.find(j) == assigned.end()) {
                    if (std::abs(value - oValue) <= distance) {
                        result.at(j) = i;
                        assigned.insert(j);
                    }
                }
            }

        }
    }
    reorderClusters(clusters, result);
    return clusters.size();
}

IClustering::Type Canopy::getType() const {
    return IClustering::CANOPY;
}

std::string Canopy::toString() const {
    return "canopy";
}

unsigned long createBottomLevel(const std::vector<float> &input,
                                std::vector<HNode> &lowerLevel) {
    std::map<float, Individuals> groupedByValue;
    for (unsigned int i = 0; i < input.size(); i++) {
        if (groupedByValue.find(input.at(i)) == groupedByValue.end())
            groupedByValue.insert({input.at(i), Individuals()});
        groupedByValue.at(input.at(i)).push_back(i);
    }
    lowerLevel.reserve(groupedByValue.size());
    cluster index = 0;
    for (const auto &p: groupedByValue) {
        lowerLevel.push_back(HNode(p.first, p.second, index));
        index++;
    }
    return (unsigned long) index;
}

float minKIntervalLength(const std::vector<HNode> &input, unsigned int k) {
    // With input, ORDERED
    assert(k > 1);
    assert(k % 2 == 1);
    float min = input.at(k / 2 - 1).distance(input.at(0));
    for (unsigned int i = 0; i < input.size() - k - 1; i++) {
        float leftHalfInterval = input.at(i + k / 2 - 1).distance(input.at(i));
        float rightHalfInterval = input.at(i + k - 1).distance(input.at(i + k / 2 - 1));
        assert(rightHalfInterval >= 0);
        assert(leftHalfInterval >= 0);
        min = std::min(min, std::max(leftHalfInterval, rightHalfInterval));
    }
    return min;
}


std::vector<HNode> subRun(const std::vector<HNode> &currentLevel, float d) {
    std::unordered_set<cluster> assigned;
    std::vector<HNode> result;

    // Creating canopies
    unsigned long clusterIndex = 0;
    for (unsigned int i = 0; i < currentLevel.size(); i++) {
        if (assigned.find(i) == assigned.end()) {
            const HNode &canopyChief = currentLevel.at(i);
            cluster itemId = canopyChief.getId();
            assigned.insert(itemId);
            std::vector<HNode> children;
            for (unsigned int j = 0; j < currentLevel.size(); j++) {
                if (i != j && assigned.find(j) == assigned.end()) {
                    const HNode &canopyMember = currentLevel.at(j);
                    if (canopyChief.distance(canopyMember) <= d) {
                        children.push_back(canopyMember);
                        assigned.insert(canopyMember.getId());
                    }
                }
            }
            if (children.empty()) {
                // Remove this cluster from previous level as it remained ungrouped
                result.push_back(HNode(canopyChief, clusterIndex));
            } else {
                children.push_back(canopyChief);
                result.push_back(HNode(children, clusterIndex));
            }
            clusterIndex++;
        }
    }
    return result;
}

// Use dynamic allocation next step
std::vector<HNode> Canopy::bottomUp(const std::vector<float> &input) {
    assert(!input.empty());
    std::vector<HNode> result;
    unsigned int k = getNbClusters();
    if (k % 2 == 0) k--; // Ensure k is uneven
    unsigned long nbElements = createBottomLevel(input, result);
    while (nbElements > k) {
        // Create next level with distance so that no more than k element in a canopy
        float d = minKIntervalLength(result, k);
        std::vector<HNode> nextLevel = subRun(result, d);
        result = nextLevel;
        nbElements = result.size();
    }
    for (unsigned int i = 0; i < result.size() - 1; i++) {
        assert(result[i] < result[i + 1]);
        assert(result[i].max <= result[i + 1].min);
    }
    return result;
}