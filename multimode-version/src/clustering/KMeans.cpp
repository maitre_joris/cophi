#include <limits>
#include <iostream>
#include <vector>
#include <assert.h>
#include <map>
#include <math.h>
#include "clustering/KMeans.h"

using namespace std;

KMeans::KMeans(unsigned int k, float e, unsigned s,
               unsigned maxIter) : IClustering(k),
                                                       epsilon(e),
                                                       maxIteration(maxIter) {
    seed = s;
    srand(seed);
}

KMeans::KMeans() :
        KMeans(2, 1E-4, 0, 20) {
}

KMeans::KMeans(unsigned int k, unsigned maxIter) :
        KMeans(k, 1E-4, 0, maxIter) {
}


KMeans::~KMeans() {
}


void KMeans::init(const std::vector<Value> &data,
                  std::vector<Centroid *> &centroids) const {
    centroids = std::vector<Centroid *>();
    std::vector<bool> elected = std::vector<bool>(data.size(), false);
    unsigned int practicalK = min(getNbClusters(), (unsigned int) data.size());
    while (centroids.size() < practicalK) {
        unsigned long i = rand() % data.size();
        while (elected[i]) i = rand() % data.size();
        Centroid *newCentroid = new Centroid(data[i],
                                             (unsigned int) centroids.size());
        centroids.push_back(newCentroid);
        elected[i] = true;
    }
}

void KMeans::assignmentStep(std::vector<Value> &data,
                            std::vector<Centroid *> &centroids) const {
    for (Value &d : data) {
        d.setCentroid(nearest(d, centroids));
    }
}

float KMeans::updateStep(std::vector<Value> &data,
                         std::vector<Centroid *> &centroids) const {
    vector<float> sum(centroids.size());
    vector<unsigned int> count(centroids.size());
    for (unsigned i = 0; i < centroids.size(); ++i) {
        sum[i] = 0.f;
        count[i] = 0;
    }
    for (Value &d : data) {
        sum.at(d.getGroup()) += d.getValue();
        count.at(d.getGroup())++;
    }
    float centroidDeviation = 0.f;
    for (Centroid *c : centroids) {
        if (count.at(c->getId()) > 0) {
            float newCentroidValue =
                    sum.at(c->getId()) / (float) count.at(c->getId());
            assert(!isnan(newCentroidValue));
            centroidDeviation += fabs(c->getValue() - newCentroidValue);
            c->setValue(newCentroidValue);
        } else c->setValue(c->getValue());
    }
    return centroidDeviation;
}

float KMeans::doStep(std::vector<Value> &data,
                     std::vector<Centroid *> &centroids) const {
    assignmentStep(data, centroids);
    return updateStep(data, centroids);
}

bool KMeans::hasConverged(float centroidDeviation) const {
    return centroidDeviation < epsilon;
}

KMeans::Centroid *KMeans::nearest(const Value &pt,
                                  std::vector<Centroid *> &centroids) const {
    Centroid *i = nullptr;
    Centroid *min_i = nullptr;

    float min_d = numeric_limits<float>::infinity();
    for (Centroid *cPt : centroids) {
        i = cPt;
        float d;
        if (min_d > (d = cPt->dist(pt))) {
            min_d = d;
            min_i = i;
        }
    }
    return min_i;
}

unsigned int KMeans::run(const vector<float> &input,
                         vector<cluster> &results) {
    std::vector<Centroid *> centroids;
    std::vector<Value> data;
    float centroidDeviation;
    assert(results.empty());
    for (float v : input)
        data.push_back(Value(v));
    assert(input.size() == data.size());
    init(data, centroids);
    centroidDeviation = doStep(data, centroids);
    unsigned iterNo = maxIteration;
    while (iterNo > 0 && !hasConverged(centroidDeviation)) {
        centroidDeviation = doStep(data, centroids);
        iterNo--;
    }
    assignmentStep(data, centroids);

    // Renumbering clusters
    std::sort(centroids.begin(), centroids.end(), compCentroid);
    unsigned int nonEmpty = 0;
    for (unsigned int cId = 0; cId < centroids.size(); cId++) {
        Centroid *c = centroids.at(cId);
        if (c->getCount() > 0)
            c->setFinalId(nonEmpty++);
    }

    for (const Value &pt : data) {
        cluster id = (cluster) pt.getGroup();
        results.push_back(id);
    }
    return nonEmpty;
}

string KMeans::toString() const {
    return "k-means";
}

IClustering::Type KMeans::getType() const {
    return IClustering::KMEANS;
}

void KMeans::Centroid::initMinMax(float v) {
    assert(!isnanf(v));
    minValue = v;
    maxValue = v;
}

void KMeans::Centroid::setValue(float v) {
    assert(!isnanf(v));
    KMeans::Point::setValue(v);
    initMinMax(v);
    count = 0;
}

void KMeans::Centroid::assign(const KMeans::Point *p) {
    float v = p->getValue();
    assert(!isnanf(v));
    maxValue = std::max(v, maxValue);
    minValue = std::min(v, minValue);
    count++;
}


KMeans::Centroid::Centroid(KMeans::Point v,
                           unsigned int group) :
        KMeans::Point(v),
        id(group) {
    initMinMax(v.getValue());
}

unsigned int KMeans::Centroid::getId() const {
    assert(id > -1);
    return (unsigned int) id;
}

void KMeans::Centroid::setFinalId(unsigned int id) {
    this->id = id;
}
