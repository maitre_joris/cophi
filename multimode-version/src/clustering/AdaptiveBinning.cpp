#include "clustering/AdaptiveBinning.h"
#include <iostream>
#include <cmath>

AdaptiveBinning::AdaptiveBinning(unsigned int nbBins) : IClustering(nbBins) {
}

unsigned int AdaptiveBinning::run(const std::vector<float> &input,
                                  std::vector<cluster> &result) {
    assert(result.empty());
    std::vector<std::pair<float, unsigned int>> inputPairs;
    for (unsigned int i = 0; i < input.size(); ++i) {
        inputPairs.push_back(std::make_pair(input[i], i));
    }

    std::sort(inputPairs.begin(), inputPairs.end());

    unsigned int binSize = (unsigned int) rint(input.size() / getNbClusters());
    result.resize(input.size());
    unsigned int currentBinSize = 0;
    unsigned int group = 0;
    std::set<cluster> assigned;
    for (std::pair<float, unsigned int> &valId : inputPairs) {
        result[valId.second] = group;
        assigned.insert(group);
        if (currentBinSize >= binSize) {
            currentBinSize = 0;
            if (group < getNbClusters())
                group++;
        } else currentBinSize++;
    }
    renumberClusters(assigned, result);
    return (unsigned int) assigned.size();
}

IClustering::Type AdaptiveBinning::getType() const {
    return IClustering::ADAPTIVE_BINNING;
}


std::string AdaptiveBinning::toString() const {
    return "Adaptive binning";
}