#include <cmath>
#include "clustering/Binning.h"

Binning::Binning(unsigned int k) : IClustering(k) {

}

unsigned int Binning::run(const std::vector<float> &input,
                          std::vector<cluster> &result) {
    assert(!input.empty());
    float min = *std::min_element(input.begin(), input.end());
    float max = *std::max_element(input.begin(), input.end());
    assert(result.empty());
    std::set<cluster> assigned;
    if (min == max) {
        result = std::vector<cluster>(input.size(), 0);
    } else {
        result.resize(input.size());
        for (unsigned int i = 0; i < input.size(); ++i) {
            assert(input[i] >= min && input[i] <= max);
            int c = (int) rint((input[i] - min) / (max - min) * (getNbClusters() - 1));
            assert(c >= 0 && c < (int) getNbClusters());
            result[i] = c;
            assigned.insert(c);
        }
    }
    renumberClusters(assigned, result);
    return assigned.size();
}

IClustering::Type Binning::getType() const {
    return IClustering::BINNING;
}


std::string Binning::toString() const {
    return "Binning";
}