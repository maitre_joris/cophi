#include "clustering/NoClustering.h"
#include <iostream>
#include <unordered_map>

NoClustering::NoClustering() : HClustering(0) {
}

NoClustering::~NoClustering() {}

std::map<float, cluster> countDistinct(const std::vector<float> &input) {
    std::map<float, cluster> assigned;
    for (unsigned int i = 0; i < input.size(); ++i) {
        auto search = assigned.find(input[i]);
        if (search != assigned.end()) {
        } else {
            assigned.insert({input[i], 0});
        }
    }
    return assigned;
}

void renumberSequentially(std::map<float, cluster> &distinctCount) {
    cluster current = 0;
    for (auto &valuePair : distinctCount) {
        valuePair.second = current;
        current++;
    }
}

void fillResult(const std::map<float, cluster> &distinctCount,
                const std::vector<float> &input,
                std::vector<cluster> &result) {
    result.clear();
    for (unsigned int i = 0; i < input.size(); ++i) {
        auto search = distinctCount.find(input.at(i));
        if (search != distinctCount.end()) {
            result.push_back(search->second);
        } else {
            std::cerr << "Should not happen" << std::endl;
        }
    }
}

std::vector<HNode> createHNodes(const std::map<float, cluster> &distinctCount,
                                const std::vector<float> &input) {
    std::vector<HNode> result;
    std::unordered_map<cluster, Individuals> individuals;

    for (unsigned long i = 0; i < input.size(); ++i) {
        cluster id = distinctCount.at(input.at(i));
        if (individuals.find(id) == individuals.end())
            individuals.insert(std::make_pair(id, Individuals()));
        individuals.at(id).push_back(i);
    }
    for (const auto &cPair : individuals) {
        float value = input.at(*cPair.second.begin());
        result.push_back(HNode(value, cPair.second, cPair.first));
    }
    return result;
}

unsigned int NoClustering::run(const std::vector<float> &input,
                               std::vector<cluster> &result) {
    std::map<float, cluster> assigned = countDistinct(input);
    renumberSequentially(assigned);
    fillResult(assigned, input, result);
    setNbClusters(assigned.size());
    return getNbClusters();
}

std::vector<HNode> NoClustering::bottomUp(const std::vector<float> &input) {
    std::map<float, cluster> assigned = countDistinct(input);
    renumberSequentially(assigned);
    return createHNodes(assigned, input);
}


std::string NoClustering::toString() const {
    return "None";
}

HClustering::Type NoClustering::getType() const {
    return NONE;
}
