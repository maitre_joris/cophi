#include "server/HttpRequest.h"
#include <iostream>
#include <sstream>

HttpRequest::HttpRequest(std::string url,
                         std::function<void(std::string)> callback)
        : url(url), onComplete(callback) {

}

#ifndef EMSCRIPTEN

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>
#include <curlpp/Infos.hpp>
#include <curlpp/Multi.hpp>

static size_t treatResponse(std::stringstream &stream, char *ptr, size_t size,
                            size_t nmemb) {
    size_t bufSize = size * nmemb;
    stream.write(ptr, bufSize);
    return bufSize;
}

class CurlppHttpRequest : public HttpRequest {
private:
    curlpp::Multi *request;
    int nbRequests;
    bool finished;
    std::stringstream responseStream;
    curlpp::Easy *tmp;
    curlpp::options::Url *urlOp;
    curlpp::options::WriteFunction *fOp;

public:
    CurlppHttpRequest(std::string url,
                      std::function<void(std::string)> callback)
            : HttpRequest(url, callback) {
        using namespace std::placeholders;
        request = new cURLpp::Multi();
        tmp = new curlpp::Easy();
        urlOp = new curlpp::options::Url(url);
        tmp->setOpt(urlOp);
        tmp->setOpt(new curlpp::options::CookieFile("cookie.txt"));
        tmp->setOpt(new curlpp::options::CookieJar("cookie.txt")); //TODO: Find a way to not use a file

        curlpp::types::WriteFunctionFunctor f =
                curlpp::types::WriteFunctionFunctor(
                        utilspp::BindFirst(utilspp::make_functor(treatResponse),
                                           responseStream));
        fOp = new curlpp::options::WriteFunction(f);
        tmp->setOpt(fOp);
        request->add(tmp);
        nbRequests = 1; //TODO Why use a multi request if only performing one at a time?
        finished = false;
    }

    ~CurlppHttpRequest() {
        delete request;
        delete tmp;
    }

    bool perform() {
        if (!finished) {
            while (!request->perform(&nbRequests)) {};
            finished = nbRequests == 0;
            if (finished)
                onComplete(responseStream.str());
        }
        return !finished;
    }

    bool isFinished() { return finished; }
};

#else
#include <emscripten/emscripten.h>

class JSHttpRequest : public HttpRequest {
private :
    bool hasEnded;
    bool isError;
    bool finished;
    bool launched;
    std::string response;

    static void loadCallback(void* arg, void* ptr, int size) {
        JSHttpRequest* req = static_cast<JSHttpRequest*>(arg);
        req->response = std::string((const char *)ptr, (size_t)size);
        req->hasEnded = true;
    }

    static void errorCallback(void* arg) {
        std::cerr << "errorCallback" << std::endl;
        JSHttpRequest* req = static_cast<JSHttpRequest*>(arg);
        req->isError = true;
    }

public :
    JSHttpRequest(std::string url, std::function<void(std::string)> callback)
        : HttpRequest(url, callback), hasEnded(false), isError(false), response(""), finished(false), launched(false) {
    }

    bool perform() {
        if (!launched) {
            emscripten_async_wget_data(url.c_str(), (void*)this, loadCallback, errorCallback);
            launched = true;
        }
        if(hasEnded) {
            this->onComplete(response);
            finished = true;
            return false;
        }
        else if (isError) {
            finished = true;
            return false;
        }
        else return true;
    }

    bool isFinished() {
        return finished;
    }
};
#endif

HttpRequest *HttpRequest::makeRequest(std::string url, std::function<void(std::string)> callback) {
#ifndef EMSCRIPTEN
    return new CurlppHttpRequest(url, callback);
#else
    return new JSHttpRequest(url, callback);
#endif
}