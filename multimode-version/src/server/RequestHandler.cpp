#include "server/RequestHandler.h"
#include <server/RestServer.h>
#include <server/MockServer.h>
#include <server/QueryParameters.h>
#include <Error.h>
#include <Notifier.h>

using namespace std;

RequestHandler::RequestHandler(const Server::Configuration &c) {
    server = new RestServer(c);
}

RequestHandler::RequestHandler(const std::string &serverAddress) {
    server = new RestServer(serverAddress);
}

RequestHandler::RequestHandler() {
    server = new MockServer();
}

void RequestHandler::requestInterval(const DimensionInterval dims) {
    assert(server != nullptr);
    assert(intervalResponse.empty());
    std::string param = QueryParameters::makeInterval(dims);
    const auto respIt = cache.find(param);
#ifndef HIERARCHICAL
    if (respIt == cache.end()) {
        Notifier::info("Request: " + dataSet + param);
        if (cache.size() > CACHE_LIMIT) {
            cache.erase(cache.begin()); //TODO check
        }
        server->sendRequest(dataSet + param, [this, param](const string &response) {
            intervalResponse = response;
            cache.insert({param, intervalResponse});
        });
    } else
#endif
    {
        intervalResponse = (*respIt).second;
        Notifier::info("Cached:" + dataSet + param);
    }
}

void RequestHandler::requestDeletion(const DimensionInterval dims) {
    assert(server != nullptr);
    assert(intervalResponse.empty());
    std::string param = QueryParameters::makeRemove(dims);
    Notifier::info("Request: " + dataSet + param);
    server->sendRequest(
            dataSet + param,
            [this](const string response) {
                intervalResponse = response;
            });
}


void RequestHandler::requestInsertion(const DimensionInterval dims, unsigned int pos) {
    assert(server != nullptr);
    assert(intervalResponse.empty());
    std::string param = QueryParameters::makeInsert(dims, pos);
    Notifier::info("Request: " + dataSet + param);
    server->sendRequest(
            dataSet + param,
            [this](const string response) {
                intervalResponse = response;
            });
}


void RequestHandler::requestHighlight(const DimensionInterval dims,
                                      const HighlightBase base) {
    assert(base.isValid());
    assert(server != nullptr);
    std::string param;
    if (base.isClusterBase()) {
        const auto nodePair = base.getClusterPosition();
        param = QueryParameters::makeHighlightNode(dims,
                                                   nodePair.first,
                                                   nodePair.second);
    } else if (base.isIntervalBase()) {
        param = QueryParameters::make(dims, base.getIntervals());
    } else {
        const auto nodePairs = base.getEdgePosition();
        param = QueryParameters::makeHighlightEdge(dims,
                                                   nodePairs.first.first,
                                                   nodePairs.first.second,
                                                   nodePairs.second.first,
                                                   nodePairs.second.second);
    }
    Notifier::info("Request: " + dataSet + param);
    server->sendRequest(
            dataSet + param,
            [this](const string response) {
                this->highlightResponse = response;
            });
}

void RequestHandler::requestFocusIn(unsigned int dbDim, cluster id) {
    assert(server != nullptr);
    string arg = QueryParameters::makeFocusIn(dbDim, id);
    Notifier::info("Request: " + dataSet + arg);
    server->sendRequest(
            dataSet + arg,
            [this](const string response) {
                this->intervalResponse = response;
            });

}

void RequestHandler::requestFocusOut(unsigned int dbDim, unsigned int k) {
    assert(server != nullptr);
    string param = QueryParameters::makeFocusOut(dbDim, k);
    Notifier::info("Request: " + dataSet + param);
    server->sendRequest(
            dataSet + param,
            [this](const string response) {
                this->intervalResponse = response;
            });

}

void RequestHandler::requestDataSet(const string &dataSet,
                                    function<void()> onResponse) {
    assert(server != nullptr);
    this->dataSet = dataSet;
    server->sendRequest(dataSet,
                        [this, onResponse](const string response) {
                            this->fullDataSetResponse = response;
                            onResponse();
                        });
}

void RequestHandler::performRequests() {
    assert(server != nullptr);
    server->performRequests();
}

bool RequestHandler::parseDataSetResponse(Buildable *b) {
    return parseResponse(fullDataSetResponse, b, true, true);
}

bool RequestHandler::parseIntervalResponse(Buildable *b) {
    return parseResponse(intervalResponse, b, true, true);
}

bool RequestHandler::parseHighlightResponse(Buildable *b) {
    return parseResponse(highlightResponse, b, false, false);
}

const string ParseError::INVALID_LAST_TOKEN = "Invalid last token";
const string ParseError::INVALID_TOKEN = "Invalid token";
const string ParseError::INVALID_VALUE = "Invalid number value";
const string ParseError::INVALID_NODE = "Invalid node";
const string ParseError::INVALID_EDGE = "Invalid edge";
const string ParseError::INVALID_LINE_START = "Invalid line start";
const string ParseError::INCOHERENT_CONTENT = "Invalid content (double node or edges)";

void printLine(std::string &response, unsigned int line) {
    std::stringstream stream(response);
    std::string result;
    for (unsigned int i = 1; i <= line; i++) {
        std::getline(stream, result);
    }
    std::cerr << result << std::endl;
}

bool RequestHandler::parseResponse(string &response,
                                   Buildable *b,
                                   bool withMinMax,
                                   bool withHeader) {
    if (response.empty()) {
        cerr << "Empty response" << endl;
        return false;
    }
    stringstream s(response);
    try {
        unsigned line = 1;
        try {
            while (parseNodeLine(s, b, withMinMax, withHeader)) line++;
            line++;
            while (parseEdgeLine(s, b)) line++;
        }
        catch (const ParseError &e) {
            throw ParseError(e.message, line);
        }
        catch (const invalid_argument &e) {
            throw ParseError(ParseError::INVALID_VALUE, line);
        }
        catch (BuildError &eb) {
            throw ParseError(ParseError::INCOHERENT_CONTENT, line);

        }
    } catch (const ParseError &ep) {
        cerr << "Cancelling due to parsing error at l." << ep.line << endl;
        cerr << ">> Error type: " << ep.message << endl;
        printLine(response, ep.line);
        response.clear();
        return false;
    } catch (const DataException &e) {
        cerr << "Cancelling due to unknown error" << endl;
        response.clear();
        return false;
    }
    response.clear();
    return true;
}

bool RequestHandler::parseNodeLine(istream &s,
                                   Buildable *b,
                                   bool withMinMax,
                                   bool withHeader) {
    string token;
    switch (readToken(s, token)) {
        case REGULAR_VALUE:
            break;
        case LINE_END:
            return false;
        default:
            throw ParseError(ParseError::INVALID_LINE_START);
    }

    try {
        const unsigned int dbDim = (unsigned int) stoi(token);
        if (withHeader) {
            string lbl = readTokenOfType(s, REGULAR_VALUE);
            b->updateDimension(dbDim, lbl);
        }
        bool endLine = false;
        while (!endLine) {
            const int clusterId = stoi(readTokenOfType(s, REGULAR_VALUE));
            float min = 0;
            float max = 0;
            // Min an max values are not provided in highlighting server response
            if (withMinMax) {
                min = (float) stod(readTokenOfType(s, REGULAR_VALUE));
                max = (float) stod(readTokenOfType(s, REGULAR_VALUE));
                if (min > max) throw ParseError(ParseError::INVALID_NODE);
            }
            const unsigned int nbBars = (unsigned int) stoi(
                    readTokenOfType(s, REGULAR_VALUE));
            if (nbBars != 10 && nbBars != 1) {
                throw ParseError(ParseError::INVALID_NODE);
            }
            Distribution distribution(nbBars, 0);
            unsigned int i = 0;
            for (; i < nbBars - 1; i++) {
                distribution[i] = (unsigned int) stoi(readTokenOfType(s, REGULAR_VALUE));
            }
            switch (readToken(s, token)) {
                case REGULAR_VALUE:
                    break;
                case LINE_LAST_VALUE:
                    endLine = true;
                    break;
                default:
                    throw ParseError(ParseError::INVALID_LAST_TOKEN);
            }
            distribution[i] = (unsigned int) stoi(token);
            if (!b->addNode(dbDim, clusterId, min, max, distribution)) {
                throw ParseError(ParseError::INVALID_NODE);
            }
        }
    } catch (invalid_argument &e) {
        cerr << "Error parsing: " << e.what() << endl;
        throw ParseError(ParseError::INVALID_VALUE);
    } catch (out_of_range &e) {
        cerr << "Out of range: " << e.what() << endl;
        throw ParseError(ParseError::INVALID_VALUE);
    }
    return true;
}

bool RequestHandler::parseEdgeLine(istream &s, Buildable *b) {
    string token;
    switch (readToken(s, token)) {
        case REGULAR_VALUE:
            break;
        case LINE_END:
            return false;
        case FILE_END:
            return false;
        default:
            throw ParseError(ParseError::INVALID_LINE_START);
    }
    const int srcDim = stoi(token);
    switch (readToken(s, token)) {
        case REGULAR_VALUE:
            break;
        case LINE_END:
            return false;
        case FILE_END:
            return false;
        default:
            throw ParseError(ParseError::INVALID_LINE_START);
    }
    const int tgtDim = stoi(token);

    bool endLine = false;
    while (!endLine) {
        const int srcCluster = stoi(readTokenOfType(s, REGULAR_VALUE));
        const int tgtCluster = stoi(readTokenOfType(s, REGULAR_VALUE));

        switch (readToken(s, token)) {
            case REGULAR_VALUE:
                break;
            case LINE_LAST_VALUE:
                endLine = true;
                break;
            case FILE_LAST_VALUE:
                endLine = true;
                break;
            default:
                throw ParseError(ParseError::INVALID_LAST_TOKEN);
        }
        const unsigned long long weight = stoull(token);

        if (!(b->addEdge(srcDim, tgtDim, srcCluster, tgtCluster, weight))) {
            throw ParseError(ParseError::INVALID_EDGE);
        }
    }
    return true;
}

tokenType RequestHandler::readToken(istream &s, string &res) {
    res.clear();

    int c;

    c = s.get();
    if (c == ',') return EMPTY_VALUE;
    else if (c == '\n') return LINE_END;
    else if (c == EOF) return FILE_END;

    while (true) {
        res.push_back(c);
        c = s.get();
        if (c == ',') {
            return REGULAR_VALUE;
        } else if (c == '\n') {
            return LINE_LAST_VALUE;
        } else if (c == EOF) {
            return FILE_LAST_VALUE;
        }
    }
}

string RequestHandler::readTokenOfType(istream &s,
                                       tokenType type) {
    string res;
    tokenType t = readToken(s, res);
    if (t == type) {
        return res;
    } else {
        throw ParseError(ParseError::INVALID_TOKEN);
    }
}

bool RequestHandler::receivedResponse(Type type) const {
    switch (type) {
        case DATASET:
            return !fullDataSetResponse.empty();
        case HIGHLIGHTING:
            return !highlightResponse.empty();
        case INTERVAL:
            return !intervalResponse.empty();
    }
    return false;
}
