#include <server/QueryParameters.h>

const std::string QueryParameters::DIMENSIONS_KEY = "dimensions";
const std::string QueryParameters::FILTER_KEY = "f";
const std::string QueryParameters::NODE_DIMENSION_KEY = "axe";
const std::string QueryParameters::NODE_ID_KEY = "pos";
const std::string QueryParameters::EDGE_DIMENSION_SRC_KEY = "axeSrc";
const std::string QueryParameters::EDGE_DIMENSION_DST_KEY = "axeDst";
const std::string QueryParameters::EDGE_ID_SRC_KEY = "posSrc";
const std::string QueryParameters::EDGE_ID_DST_KEY = "posDst";
const std::string QueryParameters::START = "?";
const std::string QueryParameters::DELIMITER = "&";
const std::string QueryParameters::FOCUS_ON_KEY = "o";
const std::string QueryParameters::FOCUS_OFF_KEY = "f";
const std::string QueryParameters::INSERT_KEY = "a";
const std::string QueryParameters::REMOVE_KEY = "r";

std::string QueryParameters::makeParam(const std::string &key,
                                       const std::string &value) {
    return key + "=" + value;
}

std::string QueryParameters::makeRemove(const DimensionInterval dims) {
    return START + makeParam(REMOVE_KEY, dims.toCSV());
}

std::string QueryParameters::makeInsert(const DimensionInterval dims, unsigned int pos) {
    return START + makeParam(INSERT_KEY, dims.toCSV() + ";" + std::to_string(pos));
}


std::string QueryParameters::makeInterval(const DimensionInterval dims) {
    return START + makeParam(DIMENSIONS_KEY, dims.toCSV());
}

std::string QueryParameters::makeHighlightNode(const DimensionInterval dims,
                                               unsigned int dbDim,
                                               cluster clusterId) {
    std::stringstream ss;
    ss << START;
    ss << makeParam(DIMENSIONS_KEY, dims.toCSV()) << DELIMITER;
    ss << makeParam(NODE_DIMENSION_KEY, std::to_string(dbDim)) << DELIMITER;
    ss << makeParam(NODE_ID_KEY, std::to_string(clusterId));
    return ss.str();
}

std::string QueryParameters::makeFocusIn(unsigned int dbDim,
                                         cluster clusterId) {
    std::stringstream ss;
    ss << START;
    ss << makeParam(FOCUS_ON_KEY, std::to_string(dbDim) + ";" + std::to_string(clusterId));
    return ss.str();
}

std::string QueryParameters::makeFocusOut(unsigned int dbDim,
                                          unsigned int k) {
    std::stringstream ss;
    ss << START;
    ss << makeParam(FOCUS_OFF_KEY, std::to_string(dbDim) + ";" + std::to_string(k));
    return ss.str();
}


std::string QueryParameters::make(const DimensionInterval dims,
                                  const std::vector<ClusterInterval> &intervals) {
    std::stringstream filterArguments;
    for (const ClusterInterval &interval : intervals) {
        filterArguments << std::get<0>(interval) << ","
                        << std::get<1>(interval) << ","
                        << std::get<2>(interval) << ";";
    }
    std::stringstream ss;
    ss << START;
    ss << makeParam(DIMENSIONS_KEY, dims.toCSV()) << DELIMITER;
    ss << makeParam(FILTER_KEY, filterArguments.str());
    return ss.str();

}

std::string QueryParameters::makeHighlightEdge(const DimensionInterval dims,
                                               unsigned int srcDbDim, cluster srcClusterId,
                                               unsigned int dstDbDim, cluster dstClusterId) {
    std::stringstream ss;
    ss << START;
    ss << makeParam(DIMENSIONS_KEY, dims.toCSV()) << DELIMITER;
    ss << makeParam(EDGE_DIMENSION_SRC_KEY, std::to_string(srcDbDim)) <<
       DELIMITER;
    ss << makeParam(EDGE_DIMENSION_DST_KEY, std::to_string(dstDbDim)) <<
       DELIMITER;
    ss << makeParam(EDGE_ID_SRC_KEY, std::to_string(srcClusterId)) << DELIMITER;
    ss << makeParam(EDGE_ID_DST_KEY, std::to_string(dstClusterId));
    return ss.str();
}