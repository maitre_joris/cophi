#include "server/RestServer.h"
#include <iostream>
#include <sstream>
#include <util/Utils.h>
#include <fstream>

RestServer::RestServer(const Configuration config)
        : hostname(config.hostname), port(config.port), baseURL(config.base) {

}

RestServer::RestServer(const std::string &serverAddress) {
    if (serverAddress.size() < 1) return;
    try {
        std::string::size_type lastPos = 0;
        if (serverAddress.substr(0, 4) == std::string("http")) {
            lastPos = serverAddress.find_first_not_of(":/", 4);
            if (lastPos == std::string::npos)
                throw std::logic_error("Invalid http prefix");
        }
        std::string::size_type pos = serverAddress.find_first_of(":", lastPos);
        if (pos == std::string::npos)
            throw std::logic_error("Could not find ':'");
        hostname = serverAddress.substr(lastPos, pos - lastPos);
        lastPos = pos + 1;
        pos = serverAddress.find_first_of("/", lastPos);
        if (pos == std::string::npos)
            throw std::logic_error("Could not find '/' after hostname");
        std::string portString = serverAddress.substr(lastPos, pos - lastPos);
        if (portString.empty())
            throw std::logic_error("Invalid port");
        port = std::stoi(portString);
        baseURL = serverAddress.substr(pos);
    } catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
        throw std::logic_error("Invalid server address format");
    }
}

void RestServer::sendRequest(const std::string &requestPath,
                             std::function<void(std::string)> callback) {
    std::stringstream sstream;
    sstream << "http://" << hostname << ":" << port << baseURL << "/" << requestPath;
    std::cout << sstream.str() << std::endl;
    HttpRequest *req = HttpRequest::makeRequest(sstream.str(), callback);
    requests.push_back(req);
}

bool RestServer::performRequests() {
    bool remains = false;
    for (unsigned i = 0; i < requests.size(); ++i)
        remains |= requests[i]->perform();
    std::vector<HttpRequest *>::iterator i = requests.begin();
    while (i != requests.end()) {
        if ((*i)->isFinished()) {
            delete *i;
            i = requests.erase(i);
        } else ++i;
    }
    return remains;
}


RestServer::Configuration RestServer::parseConfigFile(const std::string &filePath) {
    try {
        std::ifstream in(filePath);
        if (!in.is_open()) throw std::logic_error("Could not find server connection file:" + filePath);
        std::string line;
        Configuration out;
        try {
            while (getline(in, line)) {
                if (line[0] != '#') {
                    std::vector<std::string> keyValue;
                    tokenize(line, keyValue, '=');
                    if (keyValue.size() == 2) {
                        if (keyValue[0] == "hostname") {
                            out.hostname = keyValue[1];
                        } else if (keyValue[0] == "port") {
                            out.port = (short int) std::stoi(keyValue[1], 0);
                        } else if (keyValue[0] == "baseurl") {
                            out.base = keyValue[1];
                        }
                    }
                }
            }
        } catch (std::exception &e) {
            throw std::logic_error("Invalid configuration file format");
        }
        if (out.hostname.empty() || out.base.empty() || out.port == 0)
            throw std::logic_error("Invalid configuration file format");
        return out;
    } catch (const std::exception &e) {
        throw std::logic_error("Not a file");
    }
}

void RestServer::clearWaitingRequests() {

}

RestServer::~RestServer() {
    for (const HttpRequest *request : requests)
        delete request;
    requests.clear();
}
