#include <fstream>
#include <sstream>
#include <iostream>
#include <assert.h>
#include <server/QueryParameters.h>
#include <Error.h>
#include "server/MockServer.h"

MockServer::MockServer() {
    //dimensionList = std::regex(DIMENSIONS, std::regex::icase);
}

MockServer::~MockServer() {

}

void MockServer::sendRequest(const std::string &queryParams,
                             std::function<void(std::string)> callback) {
    if (queryParams.empty()) throw QueryError(QueryError::INVALID_QUERY);
    std::smatch m;
    std::regex_search(queryParams, m, dimensionList);
    if (m.size() > 0)
        std::cerr << "Found " << m.size() << " dimensions in parameters" <<
                  std::endl;
    for (auto &d : m) {
        std::cerr << d << std::endl;
        dimensions.push_back((unsigned int) std::stoi(d));
    }
    this->callback = callback;
}

bool MockServer::performRequests() {
    if (this->callback != nullptr) {
        std::ifstream in;//TODO
        std::stringstream content;
        content << in.rdbuf();
        assert(content.str().size() > 0);
        auto usedCallback = this->callback;
        this->callback = nullptr;
        usedCallback(content.str());
    }
    return false;
}

void MockServer::clearWaitingRequests() {

}
