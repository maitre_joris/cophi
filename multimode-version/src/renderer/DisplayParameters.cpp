#include <cassert>
#include "renderer/DisplayParameters.h"

DisplayParameters::DisplayParameters() : observer(nullptr) {
    setColorScale(colorScaleIndex);
}


void DisplayParameters::setGreyScale(float v) {
    greyScale = v;
}

float DisplayParameters::getGreyScale() const {
    return greyScale;
}

void DisplayParameters::attachObserver(ParameterObserver *observer) {
    this->observer = observer;
}

void DisplayParameters::setHeight(float height) {
    dimensions.height = height;
    if (observer != nullptr)
        observer->onAxisChange();
}

float DisplayParameters::getContextCompression() const {
    return dimensions.contextCompression;
}

void DisplayParameters::setContextCompression(float c) {
    dimensions.contextCompression = c;
    if (observer != nullptr)
        observer->onViewsNeedUpdate();
}

void DisplayParameters::setGapRatio(float gap) {
    dimensions.weightVerticalSpace = 1.f - gap;
    if (observer != nullptr)
        observer->onScaleChange();
}

int DisplayParameters::getGridLines() {
    return nbHorizontalLines;
}

void DisplayParameters::setSpacing(float width) {
    dimensions.axisSpacing = width;
    if (observer != nullptr)
        observer->onScaleChange();
}

void DisplayParameters::setNodeWidth(float nodeWidth) {
    dimensions.maxNodeWidth = nodeWidth;
    if (observer != nullptr)
        observer->onScaleChange();
}

void DisplayParameters::setFrequencyDefinition(unsigned def) {
    frequencyDefinition = def;
}

unsigned DisplayParameters::getFrequencyDefinition() const {
    return frequencyDefinition;
}


float DisplayParameters::getHeight() const {
    return dimensions.height;
}

const Sizes &DisplayParameters::getDimensions() const {
    return dimensions;
}

float DisplayParameters::getGapRatio() const {
    return (1.f - dimensions.weightVerticalSpace);
}

float DisplayParameters::getAxisSpacing() const {
    return dimensions.axisSpacing;
}

float DisplayParameters::getMaxNodeWidth() const {
    return dimensions.maxNodeWidth;
}

float DisplayParameters::getMinNodeWidth() const {
    return dimensions.minNodeWidth;
}

float DisplayParameters::getAxisWidth() const {
    return dimensions.axisSpacing + dimensions.maxNodeWidth;
}

bool DisplayParameters::displayAxes() const {
    return showAxes;
}

bool DisplayParameters::displayLabels() const {
    return showLabels;
}

bool DisplayParameters::displayEdges() const {
    return showEdges;
}

void DisplayParameters::setColorScale(unsigned int index) {
    assert(index < ColorScheme::SCHEMES.size());
    colorScaleIndex = index;
    colorScale = ColorScale(ColorScheme::SCHEMES[colorScaleIndex], true);
    if (observer != nullptr)
        observer->onColorChange();
}

const ColorScale &DisplayParameters::getColorScale() const {
    return colorScale;
}

bool DisplayParameters::displayDistortion() const {
    return showDistortion;
}

ContextCompression DisplayParameters::getContextMode() const {
    return contextMode;
}

void DisplayParameters::setContextMode(ContextCompression m) {
    contextMode = m;
    if (observer != nullptr)
        observer->onViewsNeedUpdate();
}

void DisplayParameters::setHeightMappingMode(HeightMapping m) {
    mappingMode = m;
}

HeightMapping DisplayParameters::getHeightMappingMode() const {
    return mappingMode;
}

void DisplayParameters::setIntervalHighlightMode(IntervalHighlight m) {
    intervalHighlight = m;
}

IntervalHighlight DisplayParameters::getIntervalHighlightMode() const {
    return intervalHighlight;
}

float DisplayParameters::getEdgeReduction() const {
    return edgeReduction;
}

float DisplayParameters::getEdgeCurvature() const {
    return edgeCurvature;
}

void DisplayParameters::setEdgeReduction(float r) {
    edgeReduction = r;
}

void DisplayParameters::setEdgeCurvature(float c) {
    edgeCurvature = c;
}

unsigned DisplayParameters::getEdgeDefinition() const {
    return edgeDefinition;
}

void DisplayParameters::setEdgeDefinition(unsigned def) {
    edgeDefinition = def;
}

float DisplayParameters::getPadding() const {
    float maxValue = getAxisWidth() / 2.f;
    float value = dimensions.axisSpacing / 3.f - dimensions.maxNodeWidth / 6.f;
    float minValue = 0.f;
    return std::max(std::min(value, maxValue), minValue);
}

void DisplayParameters::setDistortion(bool display) {
    showDistortion = display;
    if (observer != nullptr)
        observer->onViewsNeedUpdate();
}

void DisplayParameters::setDisplayLabels(bool display) {
    showLabels = display;
    if (observer != nullptr)
        observer->onAxisChange();
}

void DisplayParameters::setDisplayAxes(bool display) {
    showAxes = display;
    if (observer != nullptr)
        observer->onAxisChange();
}

bool DisplayParameters::globalGradientMode() const {
    return globalGradient;
}

void DisplayParameters::setGlobalGradient(bool b) {
    globalGradient = b;
    if (observer != nullptr)
        observer->onColorChange();
}

void DisplayParameters::nextGradient() {
    unsigned int a = colorScaleIndex + 1;
    if (a >= ColorScheme::SCHEMES.size())
        a -= ColorScheme::SCHEMES.size();
    setColorScale(a);
}
