#include <renderer/animation/Animation.h>
#include "renderer/item/NodeView.h"
#include "util/Utils.h"

const float NodeView::CONTEXT_REDUCTION_WIDTH = 0.7f;
const float NodeView::NO_SUBWEIGHT = -1.f;
const Vec4f NodeView::CONTEXT_COLOR = Vec4f(.85f);
const float NodeView::MIN_SIZE = 0.001f;


void NodeView::setAlpha(float alpha) {
    color[3] = alpha;
}

void NodeView::setInversionRate(float invertRate) {
    invert = invertRate;
}

float NodeView::getInversionRate() const {
    return invert;
}

void NodeView::setEnhancement(bool v) {
    enhancement = v;
}

bool NodeView::isValid() const {
    bool validSubWeight = (subWeight == NO_SUBWEIGHT || isBetween(0.f, subWeight, 1.01f));
    bool noNan = std::isfinite(weight) &&
            std::isfinite(weightBefore) &&
            std::isfinite(gapBefore) &&
            std::isfinite(interval) &&
            std::isfinite(intervalBefore);
    bool validRange = isBetween(0.f, weight, 1.01f) &&
            isBetween(0.f, gapBefore, 1.01f) &&
            isBetween(0.f, weightBefore, 1.01f) &&
            isBetween(0.f, interval, 1.01f) &&
            isBetween(0.f, intervalBefore, 1.01f) &&
            isBetween(0.f, widthReduction, 1.01f);
    bool validDetailView = weight > 0.f;
    return noNan && validRange && validSubWeight && validDetailView;
}

void NodeView::setAsContext() {
    color = CONTEXT_COLOR;
    widthReduction = CONTEXT_REDUCTION_WIDTH;
}


void NodeView::setBiggestBucket(const std::vector<float> &d) {
    if (d.size() <= 1) biggestBucket = .5f;
    else biggestBucket = static_cast<float>(max_element(d.begin(), d.end()) - d.begin()) / d.size();
}

Vec4f NodeView::getDimensions(const DisplayParameters *params, float weightToInterval) const {
    assert(std::isfinite(interval));
    assert(std::isfinite(weight));
    assert(std::isfinite(intervalBefore));
    float height = Animation::transition(weight, interval, weightToInterval);
    float heightBefore = Animation::transition(weightBefore, intervalBefore, weightToInterval);
    float gap = Animation::transition(params->getGapRatio(), 0.f, weightToInterval);
    float gapBefore = Animation::transition(this->gapBefore, 0.f, weightToInterval);
    Vec4f r = Vec4f(x * params->getAxisWidth(),
                    heightBefore * (1.f - gap) * params->getHeight() +
                    gapBefore * gap * params->getHeight(),
                    params->getMaxNodeWidth(),
                    height * (1.f - gap) * params->getHeight());
    const float dy = params->getHeight() - 2.f * r.y() - r.w();
    r.setY(r.y() + getInversionRate() * dy);

    /* Some checks */
    assert(std::isfinite(r.x()));
    assert(std::isfinite(r.y()));
    assert(std::isfinite(r.z()));
    assert(std::isfinite(r.w()));
    return r;
}

Vec4f NodeView::bounds(const DisplayParameters *dp, float w2i) const {
    Vec4f r = getDimensions(dp, w2i);
    // Warning: Vector::getW() returns Vector::x(), not what you would expect
    return Vec4f(r.x(), r.y(), r.x() + r.z(), r.y() + r.w());
}