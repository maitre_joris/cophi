#include <tlp/Vector.h>
#include "renderer/item/TriangleView.h"

const float TriangleView::HEIGHT = 1.0f;

// To be filled on request
const Vec2f TriangleView::VERTICES[3] = {Vec2f(-1.f, 0.f),
                                         Vec2f(1.f, 0.f),
                                         Vec2f(0.f, 0.f)
};

TriangleView::TriangleView() : position(0.f) {}

TriangleView::TriangleView(float p) : position(p) {}

Vec4f TriangleView::getPoint(unsigned int axisId, bool upper,
                             unsigned int id) {
    assert(id < 3);
    Vec4f result = Vec4f(static_cast<float>(axisId),
                         position,
                         VERTICES[id].getX(),
                         VERTICES[id].getY());
    if (id < 2) // Editing the two first points with orientation dependent value
        result[3] = upper ? +1.0f : -1.0f;
    return result;
}

float TriangleView::getPosition() const { return position; }

void TriangleView::setPosition(float p) {
    //assert(p > = 0.f && p <= 1.f);
    position = p;
}