#include <renderer/item/TriangleView.h>
#include <renderer/selection/SelectionBoxRenderer.h>
#include "renderer/GlDisplay.h"

GlDisplay::GlDisplay(DisplayParameters *params) : Display(params),
                                                  lineShader("shaders/line_shader.glsl"),
                                                  arrowShader("shaders/arrow_shader.glsl"),
                                                  labelRenderer(&matrixManager) {
    matrixManager.initViewport(Vec4i(0, 0, 10, 10));
    glGetIntegerv(GL_MAX_VERTEX_UNIFORM_VECTORS, &maxVertexUniform);
    glGetIntegerv(GL_MAX_FRAGMENT_UNIFORM_VECTORS, &maxFragmentUniform);

    maxVertexUniform /= 2;
    maxFragmentUniform /= 2;
    maxVertexUniform -= RESERVED_UNIFORM; // on laisse de la marge pour les paramètres
    maxFragmentUniform -= RESERVED_UNIFORM;
    maxNodes = maxVertexUniform / NODE_VIEW_SIZE;
    maxEdges = maxVertexUniform / EDGE_VIEW_SIZE;

    glGenBuffers(1, &nodesBuffer);
    glGenBuffers(1, &frequenciesBuffer);
    glGenBuffers(1, &edgesBuffer);
    glGenBuffers(1, &lineBuffer);
    glGenBuffers(1, &arrowBuffer);
    glGenBuffers(1, &boxBuffer);

    vertexUniformBuffer = new Vec4f[maxVertexUniform];
    fragmentUniformBuffer = new Vec4f[maxFragmentUniform];

    setNodesBuffer();
    setEdgesBuffer();
    setFrequenciesBuffer();
}


void GlDisplay::setEdgesBuffer(unsigned curveDefinition,
                               unsigned endsDefinition) {
    if ((curveDefinition + 2 * endsDefinition) % 2 == 1)
        curveDefinition += 1; // le nombre de sommets sera pair (plus simple)

    auto getT = [](unsigned i, unsigned curveDefinition,
                   unsigned endsDefinition) -> float {
        const float d = 0.1f;

        if (i <= 1) return 0;
        if (i >= (curveDefinition + 2 * endsDefinition) - 2) return 1;
        else if (i < endsDefinition) return d * (i / (float) endsDefinition);
        else if (i > endsDefinition + curveDefinition)
            return 1 - d + d * ((i - endsDefinition - curveDefinition) /
                                (float) endsDefinition);
        else if (i % 2 == 1) i -= 1;
        return d + ((i - endsDefinition) / (float) curveDefinition) * (1 - 2 * d);
    };

    params->setEdgeDefinition(curveDefinition + 2 * endsDefinition);
    unsigned edgeDefinition = params->getEdgeDefinition(); //TODO ....
    // VBO
    auto *vertexBuffer = new Vec3f[edgeDefinition];

    for (unsigned i = 0; i < edgeDefinition; i++)
        vertexBuffer[i] = Vec3f(
                getT(i % edgeDefinition, curveDefinition, endsDefinition),
                ((float) ((i % 2) + 1) * 2) - 3,
                i / edgeDefinition);

    glBindBuffer(GL_ARRAY_BUFFER, edgesBuffer);
    glBufferData(GL_ARRAY_BUFFER, edgeDefinition * sizeof(Vec3f), vertexBuffer,
                 GL_STATIC_DRAW);
    delete[] vertexBuffer;
}


void GlDisplay::setNodesBuffer() {
    // VBO
    auto *buffer = new Vec3f[maxNodes * 6];

    for (unsigned int i = 0; i < maxNodes; i++) {
        buffer[6 * i + 0] = Vec3f(0.f, 0.f, i);
        buffer[6 * i + 1] = Vec3f(1.f, 0.f, i);
        buffer[6 * i + 2] = Vec3f(0.f, 1.f, i);
        buffer[6 * i + 3] = Vec3f(1.f, 1.f, i);
        buffer[6 * i + 4] = Vec3f(0.f, 1.f, i);
        buffer[6 * i + 5] = Vec3f(1.f, 0.f, i);
    }

    glBindBuffer(GL_ARRAY_BUFFER, nodesBuffer);
    glBufferData(GL_ARRAY_BUFFER, maxNodes * 6 * sizeof(Vec3f), buffer,
                 GL_STATIC_DRAW);
    delete[] buffer;
}

GlDisplay::~GlDisplay() {
    delete[] vertexUniformBuffer;
    delete[] fragmentUniformBuffer;
    glDeleteProgram(triangleBuffer);
    glDeleteBuffers(1, &triangleBuffer);
    glDeleteProgram(nodesBuffer);
    glDeleteBuffers(1, &nodesBuffer);
    glDeleteProgram(edgesBuffer);
    glDeleteBuffers(1, &edgesBuffer);
    glDeleteProgram(boxBuffer);
    glDeleteBuffers(1, &boxBuffer);
    glDeleteProgram(frequenciesBuffer);
    glDeleteBuffers(1, &frequenciesBuffer);
}

unsigned int GlDisplay::sendNode(const NodeView &nv,
                                 const DistribView &dv,
                                 NodeView *buffer,
                                 DistribView *distribBuffer,
                                 unsigned int bufferIndex) {
    if (bufferIndex + 1 > maxNodes) {
        glSendNodes(bufferIndex);
        bufferIndex = 0;
    }
    buffer[bufferIndex] = nv;
    distribBuffer[bufferIndex] = dv;
    bufferIndex++;
    return bufferIndex;
}

void GlDisplay::drawInit(const Vec4f &backgroundColor, bool colorPicking) {
    glClearColor(backgroundColor.r(),
                 backgroundColor.g(),
                 backgroundColor.b(),
                 backgroundColor.a());
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    if (colorPicking) {
#ifndef EMSCRIPTEN
        glDisable(GL_MULTISAMPLE);
#endif
        ;
    } else {
#ifndef EMSCRIPTEN
        glEnable(GL_MULTISAMPLE);
#endif
    }
}

void GlDisplay::drawCleanUp() {
#ifndef EMSCRIPTEN
    glDisable(GL_MULTISAMPLE);
#endif
    glDisable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    glFlush();
}


void GlDisplay::drawSelectionBox(Vec2f pointA,
                                 Vec2f pointB,
                                 float z) {
    GlMat4f mat = matrixManager.getMatProjMod();
    Vec2f res = Vec2f(matrixManager.getWindowWidth(),
                      matrixManager.getWindowHeight());
    Vec2f upperLeft = Vec2f(std::min(pointA.x(), pointB.x()), std::max(pointA.y(), pointB.y()));
    Vec2f lowerRight = Vec2f(std::max(pointA.x(), pointB.x()), std::min(pointA.y(), pointB.y()));
    rectangleShader.draw(mat, res, {upperLeft, lowerRight, z});
}

void GlDisplay::drawNodes(NodeViewStorage &views,
                          float w2i,
                          float distributionAlpha,
                          float nodeZ,
                          bool behind,
                          bool colorPicking,
                          bool asThumbnail,
                          Vec2f thumbnailAnchor) {
    GlMat4f mat = matrixManager.getMatProjMod();
    nodeShader.use();
    glBindBuffer(GL_ARRAY_BUFFER, this->nodesBuffer);

    /* Attributes mapping */
    GLuint loc = nodeShader.getAttributeLocation("vertex");
    glEnableVertexAttribArray(loc);
    glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    float scaleFactor = asThumbnail ? params->THUMBNAIL_SCALE_FACTOR : 1.0f;
    /* Uniforms */
    glUniformMatrix4fv(nodeShader.matProjMod, 1, GL_FALSE, &mat[0][0]);
    glUniform2f(nodeShader.res, matrixManager.getWindowWidth(),
                matrixManager.getWindowHeight());
    glUniform1f(nodeShader.axisHeight, scaleFactor * params->getHeight());
    glUniform1f(nodeShader.axisSpacing, scaleFactor * params->getAxisSpacing());
    glUniform1f(nodeShader.nodeWidth, scaleFactor * params->getMaxNodeWidth());
    float gap = Animation::transition(params->getGapRatio(), 0.f, w2i);
    glUniform1f(nodeShader.gap, scaleFactor * gap);
    glUniform1i(nodeShader.colorPicking, colorPicking);
    glUniform1i(nodeShader.behind, behind);
    glUniform1f(nodeShader.weightToInterval, w2i);
    glUniform1f(nodeShader.nodeZ, nodeZ);
    glUniform1f(nodeShader.greyScale, params->getGreyScale());
    if (asThumbnail) {
        glUniform2f(nodeShader.anchor,
                    thumbnailAnchor.x() + scaleFactor * params->getAxisSpacing() / 2.f,
                    thumbnailAnchor.y());
    } else {
        glUniform2f(nodeShader.anchor, 0.0f, 0.0f);
    }
    // Distribution should fade out on openings
    glUniform1f(nodeShader.distributionAlpha, between(0.f, distributionAlpha, 1.f));

    /* End Uniforms */

    const Vec4f screen = screenRect(mat.inverse());
    auto *buffer = (NodeView *) vertexUniformBuffer;
    auto *distribBuffer = (DistribView *) fragmentUniformBuffer;
    unsigned int bufferIndex = 0;
    for (const auto &viewPair: views.getNodeViews()) {
        NodeView nv = viewPair.second.first;
        const Vec4f rect = nv.bounds(params, w2i); // TODO: CPU computing that is surely unnecessary
        if (asThumbnail || rectIntersects(screen, rect)) {
            bufferIndex = sendNode(nv, viewPair.second.second, buffer, distribBuffer, bufferIndex);
        }
    }
    for (const auto &viewPair : views.getContextViews()) {
        const NodeView &nv = viewPair.second.first;
        const Vec4f rect = nv.bounds(params, w2i);
        if (asThumbnail || rectIntersects(screen, rect)) {
            if (!colorPicking) {
                for (const NodeView &nvv : viewPair.second.second) {
                    if (nvv.weight > 0.f)
                        bufferIndex = sendNode(nvv, DistribView(), buffer, distribBuffer, bufferIndex);
                }
            }
            bufferIndex = sendNode(nv, DistribView(), buffer, distribBuffer, bufferIndex);
        }
    }
    if (bufferIndex > 0)
        glSendNodes(bufferIndex);
    glDisableVertexAttribArray(loc);
}

void GlDisplay::drawSliders() {
    GlMat4f mat = matrixManager.getMatProjMod();
    triangleShader.use();
    glBindBuffer(GL_ARRAY_BUFFER, triangleBuffer);

    /* Attributes mapping */
    GLsizei stride = sizeof(GL_FLOAT) * 4;
    GLuint loc1 = triangleShader.getAttributeLocation("axisId");
    glEnableVertexAttribArray(loc1);
    glVertexAttribPointer(loc1, 1, GL_FLOAT, GL_FALSE, stride, nullptr);

    GLuint loc3 = triangleShader.getAttributeLocation("relativeY");
    glEnableVertexAttribArray(loc3);
    glVertexAttribPointer(loc3, 1, GL_FLOAT, GL_FALSE, stride,
                          (void *) (sizeof(float) * 1));

    GLuint loc4 = triangleShader.getAttributeLocation("vertex");
    glEnableVertexAttribArray(loc4);
    glVertexAttribPointer(loc4, 2, GL_FLOAT, GL_FALSE, stride,
                          (void *) (sizeof(float) * 2));

    /* Uniforms */
    glUniform2f(triangleShader.res, matrixManager.getWindowWidth(),
                matrixManager.getWindowHeight());
    triangleShader.setUniforms(&mat[0][0], params->getDimensions());
    glDrawArrays(GL_TRIANGLES, 0, (GLsizei) nbTriangles * 6);
    glDisableVertexAttribArray(loc1);
    glDisableVertexAttribArray(loc3);
    glDisableVertexAttribArray(loc4);

}

void GlDisplay::drawText() {
#ifndef EMSCRIPTEN
    glDisable(GL_MULTISAMPLE);
#endif
    labelRenderer.draw(params->getMaxNodeWidth());
#ifndef EMSCRIPTEN
    glEnable(GL_MULTISAMPLE);
#endif
}


void GlDisplay::updateTriangles(std::vector<LowHighPair<TriangleView>> &triangles) {
    // TODO: as changes occur only at on one axis at a time, update could be done
    // only on that particular axis except for initialisation
    nbTriangles = triangles.size();
    size_t bufferSize = triangles.size() * 6;
    auto *buffer = new Vec4f[bufferSize];
    for (unsigned axisId = 0; axisId < nbTriangles; ++axisId) {
        unsigned i = axisId * 6;
        TriangleView upper = triangles.at(axisId).getHigh();
        buffer[i] = upper.getPoint(axisId, true, 0);
        buffer[i + 1] = upper.getPoint(axisId, true, 1);
        buffer[i + 2] = upper.getPoint(axisId, true, 2);
        TriangleView lower = triangles.at(axisId).getLow();
        buffer[i + 3] = lower.getPoint(axisId, false, 0);
        buffer[i + 4] = lower.getPoint(axisId, false, 1);
        buffer[i + 5] = lower.getPoint(axisId, false, 2);
    }
    glBindBuffer(GL_ARRAY_BUFFER, triangleBuffer);
    glBufferData(GL_ARRAY_BUFFER, bufferSize * sizeof(Vec4f), buffer,
                 GL_STATIC_DRAW);
    delete[] buffer;
}


void GlDisplay::glSendNodes(unsigned int bufferSize) const {
    glUniform4fv(nodeShader.data, bufferSize * NODE_VIEW_SIZE,
                 (float *) vertexUniformBuffer);
    glUniform4fv(nodeShader.distributionData, bufferSize * DISTRIBUTION_VIEW_SIZE,
                 (float *) fragmentUniformBuffer);
    glDrawArrays(GL_TRIANGLES, 0, 6 * bufferSize);
}

void GlDisplay::drawEdges(const ViewStorage &views,
                          float w2i,
                          float zOffset,
                          bool behind,
                          bool colorPicking,
                          bool asThumbnail,
                          Vec2f thumbnailAnchor) {
    if (views.getNbEdges() == 0) return;
    GlMat4f mat = matrixManager.getMatProjMod();
    edgeShader.use();
    glBindBuffer(GL_ARRAY_BUFFER, edgesBuffer);
    /* Attributes mapping */
    GLuint loc = edgeShader.getAttributeLocation("vertex");
    glEnableVertexAttribArray(loc);
    glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    float scaleFactor = asThumbnail ? params->THUMBNAIL_SCALE_FACTOR : 1.0f;
    /* Uniforms */
    glUniformMatrix4fv(edgeShader.matProjMod, 1, GL_FALSE, &mat[0][0]);
    glUniform2f(edgeShader.res, matrixManager.getWindowWidth(),
                matrixManager.getWindowHeight());
    glUniform1f(edgeShader.height, scaleFactor * params->getHeight());
    glUniform1f(edgeShader.axisSpacing, scaleFactor * params->getAxisSpacing());
    glUniform1f(edgeShader.nodeWidth, scaleFactor * params->getMaxNodeWidth());
    float gap = Animation::transition(params->getGapRatio(), 0.f, w2i);
    glUniform1f(edgeShader.gap, scaleFactor * gap);
    glUniform1f(edgeShader.edgeCurvature, params->getEdgeCurvature());
    glUniform1f(edgeShader.edgeReduction, params->getEdgeReduction());
    glUniform1i(edgeShader.colorPicking, colorPicking);
    glUniform1i(edgeShader.behind, behind);
    glUniform1f(edgeShader.weightToInterval, w2i);
    glUniform1f(edgeShader.greyScale, params->getGreyScale());

    if (asThumbnail)
        glUniform2f(edgeShader.anchor,
                    thumbnailAnchor.x() + scaleFactor * params->getAxisSpacing() / 2.0f,
                    thumbnailAnchor.y());
    else glUniform2f(edgeShader.anchor, 0.0f, 0.0f);
    /* End Uniforms */
    const Vec4f screen = screenRect(mat.inverse());

    // If one edge is hovered, she will be more opaque than the rest
    std::vector<EdgeView> visibleEdges;
    visibleEdges.reserve(views.getNbEdges());
    float currentZ = zOffset;
    float stepZ = 10.f / views.getNbEdges();
    for (const edge &e: views.getSortedEdges()) {
        currentZ -= stepZ;
        EdgeView ev = views.getEdgeView(e);
        ev.z = currentZ;
        ev.weight /= (w2i * views.getMaxEdgeWeight() + (1 - w2i));
        const Vec4f rect = ev.bounds(params, w2i);
        if (colorPicking || asThumbnail || rectIntersects(screen, rect))
            visibleEdges.push_back(ev);
    }
    visibleEdges.shrink_to_fit();
    // When using depth buffering in an application, you need to be careful about the
    // order in which you render primitives. Fully opaque primitives need to be rendered
    // first, followed by partially opaque primitives in back-to-front order.
    // (https://www.opengl.org/archives/resources/faq/technical/transparency.htm)

    for (EdgeView &v:visibleEdges) {
        glUniform4fv(edgeShader.data, EDGE_VIEW_SIZE, (float *) &v);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, params->getEdgeDefinition());
    }
    glDisableVertexAttribArray(loc);
}


Vec2f GlDisplay::windowToModel(Vec2f coordinates) {
    return matrixManager.windowToModel(coordinates);
}

float GlDisplay::windowToModelFactor() {
    return matrixManager.windowToModelFactor();

}

void GlDisplay::init() {
    matrixManager.initViewport(Vec4i(0, 0, 10, 10));
    glGetIntegerv(GL_MAX_VERTEX_UNIFORM_VECTORS, &maxVertexUniform);
    glGetIntegerv(GL_MAX_FRAGMENT_UNIFORM_VECTORS, &maxFragmentUniform);

    maxVertexUniform /= 2;
    maxFragmentUniform /= 2;
    maxVertexUniform -= RESERVED_UNIFORM; // on laisse de la marge pour les paramètres
    maxFragmentUniform -= RESERVED_UNIFORM;
    maxNodes = maxVertexUniform / NODE_VIEW_SIZE;
    maxEdges = maxVertexUniform / EDGE_VIEW_SIZE;

    glGenBuffers(1, &nodesBuffer);
    glGenBuffers(1, &frequenciesBuffer);
    glGenBuffers(1, &edgesBuffer);
    glGenBuffers(1, &triangleBuffer);

    vertexUniformBuffer = new Vec4f[maxVertexUniform];
    fragmentUniformBuffer = new Vec4f[maxFragmentUniform];

    setNodesBuffer();
    setEdgesBuffer();
    setFrequenciesBuffer();
}

void GlDisplay::drawFrequencies(NodeViewStorage &views,
                                float w2i,
                                float zOffset,
                                bool asThumbnail,
                                Vec2f thumbnailAnchor) {
    GlMat4f mat = matrixManager.getMatProjMod();
    frequencyShader.use();
    glBindBuffer(GL_ARRAY_BUFFER, frequenciesBuffer);

    /* Attributes mapping */
    GLuint loc = frequencyShader.getAttributeLocation("vertex");
    glEnableVertexAttribArray(loc);
    glVertexAttribPointer(loc, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
    float scaleFactor = asThumbnail ? params->THUMBNAIL_SCALE_FACTOR : 1.0f;
    /* Uniforms */
    glUniformMatrix4fv(frequencyShader.matProjMod, 1, GL_FALSE, &mat[0][0]);
    glUniform1f(frequencyShader.height, scaleFactor * params->getHeight());
    glUniform1f(frequencyShader.axisSpacing, scaleFactor * params->getAxisSpacing());
    glUniform1f(frequencyShader.nodeWidth, scaleFactor * params->getMaxNodeWidth());
    float gap = Animation::transition(params->getGapRatio(), 0.f, w2i);
    glUniform1f(frequencyShader.gap, scaleFactor * gap);
    if (asThumbnail) {
        glUniform2f(frequencyShader.anchor,
                    thumbnailAnchor.x() + scaleFactor * params->getAxisSpacing() / 2.f,
                    thumbnailAnchor.y());
    } else {
        glUniform2f(frequencyShader.anchor, 0.0f, 0.0f);
    }
    glUniform1f(frequencyShader.z, zOffset);
    glUniform1f(frequencyShader.weightToInterval, w2i);
    /* End Uniforms */

    unsigned int renderedNodes = 0;
    auto *buffer = (NodeView *) vertexUniformBuffer;
    unsigned int bufferIndex = 0;
    const Vec4f screen = screenRect(mat.inverse());
    for (const auto &viewPair : views.getNodeViews()) {
        const NodeView &nv = viewPair.second.first;
        const Vec4f rect = nv.getDimensions(params, w2i);
        if (asThumbnail || rectIntersects(screen, rect)) {
            if (bufferIndex + 1 > maxNodes) {
                glUniform4fv(frequencyShader.data, bufferIndex * NODE_VIEW_SIZE, (float *) buffer);
                glDrawArrays(GL_LINES, 0, bufferIndex * params->getFrequencyDefinition());
                bufferIndex = 0;
            }

            buffer[bufferIndex] = nv;
            bufferIndex++;
            renderedNodes++;
        }
    }

    if (bufferIndex > 0) {
        glUniform4fv(frequencyShader.data, bufferIndex * NODE_VIEW_SIZE, (float *) buffer);
        glDrawArrays(GL_LINES, 0, bufferIndex * params->getFrequencyDefinition());
    }
    glDisableVertexAttribArray(loc);
}

void GlDisplay::setFrequenciesBuffer(unsigned definition) {
    definition = (definition - 2) * 2 + 2;
    params->setFrequencyDefinition(definition);

    /* VBO */
    auto *vertexBuffer = new Vec2f[maxNodes * definition];
    for (unsigned int n = 0; n < maxNodes; n++) {
        vertexBuffer[n * definition] = Vec2f(0, n);

        for (unsigned int i = 1; i < definition - 1; i += 2) {
            const Vec2f vertex = Vec2f(i / (float) (definition - 1), n);
            vertexBuffer[n * definition + i] = vertex;
            vertexBuffer[n * definition + i + 1] = vertex;
        }

        vertexBuffer[n * definition + (definition - 1)] = Vec2f(1, n);
    }
    glBindBuffer(GL_ARRAY_BUFFER, frequenciesBuffer);
    glBufferData(GL_ARRAY_BUFFER, maxNodes * definition * sizeof(Vec2f),
                 vertexBuffer, GL_STATIC_DRAW);
    delete[] vertexBuffer;
}

void GlDisplay::clearText() {
    labelRenderer.clear();
}

void GlDisplay::addText(const std::string &text, Vec2f pos) {
    labelRenderer.addLabel((isBlank(text)) ? "?" : text, pos.x(), pos.y());
}

void GlDisplay::updateLineBuffer(unsigned int nbAxes) {
    /* VBO */
    // Setup vertices for nbAxes vertical lines (for axes) and 3 horizontal lines (for a sort of grid layout)
    auto *buffer = new Vec3f[(nbAxes + params->getGridLines()) * 2];
    const float axisZ = -50.f;
    const float axisMargin = 0.02f;

    for (unsigned int i = 0; i < nbAxes; i++) {
        // One vertical line per dimension
        buffer[2 * i] = Vec3f(i, -(1.f + axisMargin), axisZ);
        buffer[(2 * i) + 1] = Vec3f(i, (1.f + axisMargin), axisZ);
    }

    const float gridZ = 2.f;
    for (unsigned int i = nbAxes; i < (nbAxes + params->getGridLines()); i++) {
        // One horizontal line
        float y = -1.f + 2.f / (params->getGridLines() + 1.f) * float(i - nbAxes + 1);
        buffer[2 * i] = Vec3f(0, y, gridZ);
        buffer[(2 * i) + 1] = Vec3f(nbAxes - 1, y, gridZ);
    }

    glBindBuffer(GL_ARRAY_BUFFER, lineBuffer);
    glBufferData(GL_ARRAY_BUFFER, (nbAxes + params->getGridLines()) * 2 * sizeof(Vec3f), buffer,
                 GL_STATIC_DRAW);
    delete[] buffer;
}

void GlDisplay::updateArrowBuffer(unsigned long nbAxes,
                                  const Inversion &inversions) {
    /* VBO */
    auto *buffer = new Vec4f[nbAxes * 3];

    for (unsigned i = 0; i < nbAxes; i++) {
        float inv = (1 - inversions.getState(i)) * 2 - 1; // [-1 ; 1]
        inv *= 1.02f; // margin
        buffer[(3 * i)] = Vec4f(i, inv, -1, 0);
        buffer[(3 * i) + 1] = Vec4f(i, inv, 0, inv);
        buffer[(3 * i) + 2] = Vec4f(i, inv, 1, 0);
    }

    glBindBuffer(GL_ARRAY_BUFFER, arrowBuffer);
    glBufferData(GL_ARRAY_BUFFER, nbAxes * 3 * sizeof(Vec4f), buffer,
                 GL_STATIC_DRAW);
    delete[] buffer;
}

void GlDisplay::drawLines(const Sizes &dimensions,
                          GlMat4f mat,
                          GLsizei nbLines) {
    lineShader.use();
    glBindBuffer(GL_ARRAY_BUFFER, lineBuffer);
    const int stride = 3;
    GLuint loc1 = lineShader.getAttributeLocation("vertex");
    glEnableVertexAttribArray(loc1);
    glVertexAttribPointer(loc1, 2, GL_FLOAT, GL_FALSE, sizeof(GL_FLOAT) * stride,
                          (void *) nullptr);

    GLuint loc2 = lineShader.getAttributeLocation("z");
    glEnableVertexAttribArray(loc2);
    glVertexAttribPointer(loc2, 1, GL_FLOAT, GL_FALSE, sizeof(GL_FLOAT) * stride,
                          (void *) (sizeof(float) * 2));

    lineShader.setUniforms(&mat[0][0], dimensions);

    glDrawArrays(GL_LINES, 0, nbLines * 2);
    glDisableVertexAttribArray(loc1);
    glDisableVertexAttribArray(loc2);
}

void GlDisplay::drawAxes(bool fade) {
    GlMat4f mat = matrixManager.getMatProjMod();
    drawLines(params->getDimensions(), mat, nbAxes + params->getGridLines());
    drawArrows(nbAxes, fade);
}

void GlDisplay::updateAxes(const Inversion &inversions) {
    Display::updateAxes(inversions);
    updateArrowBuffer(nbAxes, inversions);
    updateLineBuffer(nbAxes);
}

unsigned readPixelId(int x, int y) {
    unsigned char pixel[4];
    glReadPixels(x, y, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, pixel);
    return (unsigned) pixel[0] * 256 * 256 + pixel[1] * 256 + pixel[2];
}

int GlDisplay::pickingRoutine(int x, int y, std::function<bool(int)> isValid) {
    y = matrixManager.getWindowHeight() - y;
    glFlush();
    glFinish();
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    int dist = 0;
    while (dist < MAX_PICKING_DISTANCE) {
        int maxX = std::min(x + dist + 1, matrixManager.getWindowWidth());
        for (int px = std::max(1, x - dist); px < maxX; px++) {
            if (y - dist < 1 || y - dist > matrixManager.getWindowHeight()) continue;
            unsigned id = readPixelId(px, y - dist);
            if (isValid(id)) return id;
            if (y + dist < 1 || y + dist > matrixManager.getWindowHeight()) continue;
            id = readPixelId(px, y + dist);
            if (isValid(id)) return id;
        }

        int maxY = std::min(y + (dist - 1) + 1, matrixManager.getWindowHeight());
        for (int py = std::max(0, y - (dist - 1)); py < maxY; py++) {
            if (x - dist < 1 || x - dist > matrixManager.getWindowWidth()) continue;
            unsigned id = readPixelId(x - dist, py);
            if (isValid(id)) return id;
            if (x + dist < 1 || x + dist > matrixManager.getWindowWidth()) continue;
            id = readPixelId(x + dist, py);
            if (isValid(id)) return id;
        }
        dist++;
    }
    return NONE_ID;
}

void GlDisplay::drawArrows(GLsizei nbArrows, bool fade) {
    arrowShader.use();
    glBindBuffer(GL_ARRAY_BUFFER, arrowBuffer);

    GLuint loc1 = arrowShader.getAttributeLocation("axisId");
    glEnableVertexAttribArray(loc1);
    glVertexAttribPointer(loc1, 1, GL_FLOAT, GL_FALSE, sizeof(GL_FLOAT) * 4,
                          (void *) 0);

    GLuint loc2 = arrowShader.getAttributeLocation("y");
    glEnableVertexAttribArray(loc2);
    glVertexAttribPointer(loc2, 1, GL_FLOAT, GL_FALSE, sizeof(GL_FLOAT) * 4,
                          (void *) (sizeof(float) * 1));

    GLuint loc3 = arrowShader.getAttributeLocation("position");
    glEnableVertexAttribArray(loc3);
    glVertexAttribPointer(loc3, 2, GL_FLOAT, GL_FALSE, sizeof(GL_FLOAT) * 4,
                          (void *) (sizeof(float) * 2));

    arrowShader.setUniforms(params->getDimensions(), matrixManager, fade);
    glDrawArrays(GL_TRIANGLES, 0, nbArrows * 3);
    glDisableVertexAttribArray(loc1);
    glDisableVertexAttribArray(loc2);
    glDisableVertexAttribArray(loc3);
}


GlDisplay::NodeProgram::NodeProgram() : ShaderProgram(
        "shaders/node_shader.glsl") {
    matProjMod = glGetUniformLocation(program, "matProjMod");
    res = glGetUniformLocation(program, "res");
    data = glGetUniformLocation(program, "data");
    distributionData = glGetUniformLocation(program, "distributionData");
    axisHeight = glGetUniformLocation(program, "axisHeight");
    axisSpacing = glGetUniformLocation(program, "axisSpacing");
    nodeWidth = glGetUniformLocation(program, "nodeWidth");
    gap = glGetUniformLocation(program, "gap");
    colorPicking = glGetUniformLocation(program, "colorPicking");
    behind = glGetUniformLocation(program, "behind");
    nodeZ = glGetUniformLocation(program, "nodeZ");
    anchor = glGetUniformLocation(program, "anchor");
    weightToInterval = glGetUniformLocation(program, "weightToInterval");
    greyScale = glGetUniformLocation(program, "greyScale");
    distributionAlpha = glGetUniformLocation(program, "distributionAlpha");

}

GlDisplay::EdgeProgram::EdgeProgram() : ShaderProgram("shaders/edge_shader.glsl") {
    matProjMod = glGetUniformLocation(program, "matProjMod");
    res = glGetUniformLocation(program, "res");
    data = glGetUniformLocation(program, "data");
    height = glGetUniformLocation(program, "height");
    axisSpacing = glGetUniformLocation(program, "axisSpacing");
    nodeWidth = glGetUniformLocation(program, "nodeWidth");
    gap = glGetUniformLocation(program, "gap");
    edgeCurvature = glGetUniformLocation(program, "curvature");
    edgeReduction = glGetUniformLocation(program, "edgeReduction");
    colorPicking = glGetUniformLocation(program, "colorPicking");
    behind = glGetUniformLocation(program, "behind");
    anchor = glGetUniformLocation(program, "anchor");
    weightToInterval = glGetUniformLocation(program, "weightToInterval");
    greyScale = glGetUniformLocation(program, "greyScale");
}

GlDisplay::FrequencyProgram::FrequencyProgram() : ShaderProgram("shaders/frequency_shader.glsl") {
    matProjMod = glGetUniformLocation(program, "matProjMod");
    data = glGetUniformLocation(program, "data");
    height = glGetUniformLocation(program, "height");
    axisSpacing = glGetUniformLocation(program, "axisSpacing");
    nodeWidth = glGetUniformLocation(program, "nodeWidth");
    gap = glGetUniformLocation(program, "gap");
    z = glGetUniformLocation(program, "z");
    anchor = glGetUniformLocation(program, "anchor");
    weightToInterval = glGetUniformLocation(program, "weightToInterval");
}


GlDisplay::TriangleProgram::TriangleProgram() : BasicShaderProgram("shaders/slider_shader.glsl") {
    res = glGetUniformLocation(program, "res");
}

GlDisplay::ArrowShader::ArrowShader(const std::string &shaderPath) : BasicShaderProgram(shaderPath) {
    res = glGetUniformLocation(this->program, "res");
    alpha = glGetUniformLocation(this->program, "alpha");
}

void GlDisplay::ArrowShader::setUniforms(const Sizes &dimensions,
                                         MatrixManager &matrixManager,
                                         bool fade) {
    GlMat4f mat = matrixManager.getMatProjMod();
    BasicShaderProgram::setUniforms(&mat[0][0], dimensions);
    glUniform1f(alpha, fade ? 0.2f : 1.0f);
    glUniform2f(res, matrixManager.getWindowWidth(),
                matrixManager.getWindowHeight());
}

void GlDisplay::zoom(float x, float y, const float scale) {
    matrixManager.zoom(scale, Vec2f(x, y));
}

void GlDisplay::translate(float dx, float dy) {
    const float d = matrixManager.windowToModelFactor();
    matrixManager.moveModel(Vec3f(d * dx, d * dy, 0));
}

void GlDisplay::center(unsigned int nbAxes) {
    float totalSpacing = params->getAxisSpacing() * (nbAxes - 1);
    float totalNodeWidth = params->getMaxNodeWidth() * nbAxes;
    matrixManager.centerBox(Vec4f(0, 0, totalSpacing + totalNodeWidth, params->getHeight()));
}

void GlDisplay::fit(unsigned int nbAxes) {
    center(nbAxes);
    float totalNodeWidth = params->getMaxNodeWidth() * nbAxes;
    const float d = matrixManager.windowToModelFactor();
    const float height = 0.8f * d * matrixManager.getWindowHeight();
    params->setHeight(std::max(0.f, height));
    float spacing = d * matrixManager.getWindowWidth() - totalNodeWidth;
    float spacingNorm = std::max(0.f, spacing / float(nbAxes - 1));
    params->setSpacing(spacingNorm);
    center(nbAxes);
}

void GlDisplay::changeViewport(const Vec4i x) {
    matrixManager.changeViewport(x);
}
