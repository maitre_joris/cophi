#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>
#include <assert.h>
#include <Error.h>
#include "renderer/ShaderProgram.h"

#define MAX_MSG_SIZE 5120

using namespace std;
static const string versionHeader = "#version";

ShaderProgram::ShaderProgram(string filename) : _name(filename) {
    program = glCreateProgram();
    std::ifstream in(filename, std::ios::binary);
    if (!in.is_open()) throw InvalidStateError();
    stringstream vertex;
    stringstream fragment;
    string line;

    while (!in.eof() && getline(in, line)) {
#ifdef EMSCRIPTEN
        if (line.compare(0, versionHeader.length(), versionHeader) == 0) continue;
#endif
        if (line == "/*_fragment_*/") break;
        vertex << line << endl;
    }

    while (!in.eof() && getline(in, line)) {
        if (line == "/*_end_*/") break;
#ifdef EMSCRIPTEN
        if (line.compare(0, versionHeader.length(), versionHeader) == 0) continue;
#endif
        fragment << line << endl;
    }
    glAttachShader(program, createShader(vertex.str().c_str(), GL_VERTEX_SHADER));
    glAttachShader(program, createShader(fragment.str().c_str(), GL_FRAGMENT_SHADER));
    glLinkProgram(program);
    if (glGetError() != GL_NO_ERROR || glIsProgram(program) != GL_TRUE) {
        char msg[MAX_MSG_SIZE];
        glGetProgramInfoLog(program, sizeof msg, NULL, msg);
        if (strlen(msg) > 0) {
            cerr << "********************ProgInfoLog************************" << endl;
            cerr << "Shader : " << _name << endl;
            cerr << "Program info: " << endl << msg << endl;
            cerr << "*******************************************************" << endl;
        }
    }
}

ShaderProgram::~ShaderProgram() {
    glDeleteProgram(program);
}


GLuint ShaderProgram::createShader(const char source[], GLenum type) {
    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &source, nullptr);
    glCompileShader(shader);
    if (glGetError() != GL_NO_ERROR || glIsShader(shader) != GL_TRUE) {
        char msg[MAX_MSG_SIZE];
        glGetShaderInfoLog(shader, sizeof msg, nullptr, msg);
        if (strlen(msg) > 0) {
            cerr << "*******************************************************" << endl;
            cerr << ((type == GL_VERTEX_SHADER) ? "Vertex" : "Fragment");
            cerr << " shader : " << _name << endl;
            cerr << "Program info: " << endl << msg << endl;
            cerr << "*******************************************************" << endl;
        }
    }
    return shader;
}

void ShaderProgram::use() {
    glUseProgram(program);
}

GLuint ShaderProgram::getAttributeLocation(const char *attribute) {
    GLint loc = glGetAttribLocation(program, attribute);
    assert(loc != -1);
    return (GLuint) loc;

}
