#include <renderer/BasicShaderProgram.h>

BasicShaderProgram::BasicShaderProgram(std::string shaderPath) :
        ShaderProgram(shaderPath) {
    matProjMod = glGetUniformLocation(this->program, "matProjMod");
    height = glGetUniformLocation(this->program, "height");
    width = glGetUniformLocation(this->program, "width");
    nodeWidth = glGetUniformLocation(this->program, "nodeWidth");
}

void BasicShaderProgram::setUniforms(GLfloat *mat, const Sizes &dimensions) {
    glUniformMatrix4fv(this->matProjMod, 1, GL_FALSE, mat);
    glUniform1f(this->height, dimensions.height);
    glUniform1f(this->width, dimensions.axisSpacing);
    glUniform1f(this->nodeWidth, dimensions.maxNodeWidth);
}