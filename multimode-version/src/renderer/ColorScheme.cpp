#include<renderer/ColorScheme.h>
std::vector<std::vector<Color>> ColorScheme::SCHEMES = {
        {Color(0, 255, 0),     Color(0, 0, 0),       Color(255, 0, 0)},
        {Color(178, 196, 213), Color(200, 200, 160), Color(211, 179, 176)},
        {Color(87, 116, 230),  Color(182, 189, 56),  Color(156, 29, 29)},
        {Color(69, 13, 84), Color(45, 111, 142), Color(60, 188, 117), Color(253, 231, 37)},
        {Color(0, 57, 115),    Color(200, 200, 160), Color(145, 40, 55)}
        };