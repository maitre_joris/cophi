#include "renderer/ViewStorage.h"

using namespace std;

NodeViewStorage::NodeViewStorage() : data(nullptr) {
}

NodeViewStorage::NodeViewStorage(const ViewData *base) : data(base) {
}

ViewStorage::ViewStorage() {
    assert(empty());
}

ViewStorage::ViewStorage(const ViewData *base) : NodeViewStorage(base) {
    assert(empty());
}

void NodeViewStorage::initNodeViews(unsigned int size) {
    nodeViews.clear();
    nodeViews.reserve(size);
    contextNodeViews.clear();
}

void ViewStorage::initEdgeViews(unsigned int size) {
    edgeViews.clear();
    edgeViews.reserve(size);
}

void ViewStorage::clear() {
    maxEdgeWeight = 0.f;
    NodeViewStorage::clear();
    sortedEdgeKeys.clear();
    edgeViews.clear();
}

void NodeViewStorage::clear() {
    nodeViews.clear();
    contextNodeViews.clear();
    assert(NodeViewStorage::empty());
}

EdgeView &ViewStorage::getEdgeView(const edge &e) {
    if (edgeViews.find(e) != edgeViews.end())
        return edgeViews.at(e);
    else throw ViewNotFound();
}

const EdgeView &ViewStorage::getEdgeView(const edge &e) const {
    if (edgeViews.find(e) != edgeViews.end())
        return edgeViews.at(e);
    else throw ViewNotFound();
}

unsigned long ViewStorage::getNbEdges() const {
    assert(edgeViews.size() == sortedEdgeKeys.size());
    return edgeViews.size();
}

unsigned long NodeViewStorage::getNbNodes() const {
    return nodeViews.size();

}

float ViewStorage::getMaxEdgeWeight() const {
    return maxEdgeWeight;
}

void ViewStorage::sortEdges() {
    if (edgeViews.empty()) return;
    sortedEdgeKeys.clear();
    sortedEdgeKeys.reserve(edgeViews.size());
    for (const auto &pair : edgeViews) {
        sortedEdgeKeys.push_back(pair.first);
    }
    std::sort(sortedEdgeKeys.begin(), sortedEdgeKeys.end(), [this](const edge &a, const edge &b) -> bool {
        if (edgeViews[a].subWeight < 0 && edgeViews[b].subWeight < 0)
            return edgeViews[a].weight > edgeViews[b].weight;
        else return edgeViews[a].subWeight > edgeViews[b].subWeight;
    });
}

NodeView &NodeViewStorage::getNodeView(const node &n) {
    if (nodeViews.find(n) != nodeViews.end())
        return get<NodeView>(nodeViews.at(n));
    else if (contextNodeViews.find(n) != contextNodeViews.end())
        return get<NodeView>(contextNodeViews.at(n));
    else throw ViewNotFound();
}

const NodeView &NodeViewStorage::getNodeView(const node &n) const {
    if (nodeViews.find(n) != nodeViews.end())
        return get<NodeView>(nodeViews.at(n));
    else if (contextNodeViews.find(n) != contextNodeViews.end())
        return get<NodeView>(contextNodeViews.at(n));
    else throw ViewNotFound();
}

DistribView &NodeViewStorage::getDistribView(const node &n) {
    assert(nodeViews.find(n) != nodeViews.end());
    return get<DistribView>(nodeViews.at(n));
}


const DistribView &NodeViewStorage::getDistribView(const node &n) const {
    assert(nodeViews.find(n) != nodeViews.end());
    return get<DistribView>(nodeViews.at(n));
}

const ViewData *NodeViewStorage::getViewData() {
    return data;
}

bool NodeViewStorage::empty() const {
    return (nodeViews.empty() && contextNodeViews.empty());
}

bool ViewStorage::empty() const {
    return NodeViewStorage::empty() && edgeViews.empty();
}


bool NodeViewStorage::hasNode(const node &n) const {
    return nodeViews.find(n) != nodeViews.end() ||
           contextNodeViews.find(n) != contextNodeViews.end();
}

bool ViewStorage::hasEdge(const edge &e) const {
    return edgeViews.find(e) != edgeViews.end();
}

void ViewStorage::insertEdgeView(const edge e, EdgeView &ev) {
    assert(edgeViews.find(e) == edgeViews.end());
    maxEdgeWeight = std::max(maxEdgeWeight, ev.weight);
    edgeViews.insert(std::make_pair(e, ev));
}

const unordered_map<node, pair<NodeView, DistribView>> &NodeViewStorage::getNodeViews() const {
    return nodeViews;
}

const unordered_map<node, pair<NodeView, deque<NodeView>>> &NodeViewStorage::getContextViews() const {
    return contextNodeViews;
}

std::deque<NodeView> &NodeViewStorage::getDetailViews(const node &n) {
    if (contextNodeViews.find(n) == contextNodeViews.end()) throw ViewNotFound();
    return contextNodeViews.at(n).second;
}

const std::deque<NodeView> &NodeViewStorage::getDetailViews(const node &n) const {
    if (contextNodeViews.find(n) == contextNodeViews.end()) throw ViewNotFound();
    return contextNodeViews.at(n).second;
}

NodeView &NodeViewStorage::getDetailView(const node &n, unsigned int subNo) {
    std::deque<NodeView> &subViews = getDetailViews(n);
    unsigned int index = 0;
    for (NodeView &nv : subViews) {
        if (index == subNo) return nv;
        index++;
    }
    throw ViewNotFound();
}

void NodeViewStorage::repositionNodes(const Dimension &orderedNodes, bool weightOnly) {
    float weightBefore = 0.f;
    for (const auto &nodePair : orderedNodes) {
        const node &n = nodePair.second;
        if (nodeViews.find(n) != nodeViews.end()) {
            NodeView &nv = nodeViews.at(n).first;
            nv.weightBefore = weightBefore;
            weightBefore += nv.weight;
        } else if (contextNodeViews.find(n) != contextNodeViews.end()) {
            NodeView &nv = contextNodeViews.at(n).first;
            nv.weightBefore = weightBefore;
            assert(nv.weightBefore + nv.weight > 0.95f || nv.weightBefore == 0.f);

            weightBefore += nv.weight;
            deque<NodeView> &subViews = contextNodeViews.at(n).second;
            if (nv.weightBefore == 0.f) { // Lower node
                float wb = 0.f, pw = 0.f, ib = 0.f, pi = 0.f;
                for (NodeView &sv : subViews) {
                    wb += pw;
                    ib += pi;
                    sv.weightBefore = wb;
                    if (!weightOnly) {
                        sv.intervalBefore = ib;
                        pi = sv.interval;
                    }
                    pw = sv.weight;
                }
            } else { // Higher node
                float wb = 1.f, ib = 1.f;
                for (NodeView &sv : subViews) {
                    wb -= sv.weight;
                    ib -= sv.interval;
                    sv.weightBefore = wb;
                    if (!weightOnly) {
                        sv.intervalBefore = ib;
                    }
                }
            }
        }
    }
    assert(weightBefore > .98f);
}

void NodeViewStorage::rescaleWeightInterval(const Dimension &orderedNodes, bool weightOnly) {
    float weightScale = 0.f;
    float intervalScale = 0.f;
    for (const auto &nodePair : orderedNodes) {
        const node &n = nodePair.second;
        assert(hasNode(n));
        const NodeView *nv = nullptr;
        if (nodeViews.find(n) != nodeViews.end()) {
            nv = &nodeViews.at(n).first;
        } else if (contextNodeViews.find(n) != contextNodeViews.end()) {
            nv = &contextNodeViews.at(n).first;
        } else
            assert(false);
        weightScale += nv->weight;
        intervalScale = max(nv->interval + nv->intervalBefore, intervalScale);
    }

    for (const auto &nodePair : orderedNodes) {
        const node &n = nodePair.second;
        if (nodeViews.find(n) != nodeViews.end()) {
            NodeView &nv = nodeViews.at(n).first;
            nv.weight /= weightScale;
            if (!weightOnly) {
                nv.interval /= intervalScale;
                nv.intervalBefore /= intervalScale;
            }
        } else if (contextNodeViews.find(n) != contextNodeViews.end()) {
            NodeView &nv = contextNodeViews.at(n).first;
            nv.weight /= weightScale;
            if (!weightOnly) {
                nv.interval /= intervalScale;
                nv.intervalBefore /= intervalScale;
            }
            float subWeights = 0.f;
            float subIntervals = 0.f;
            // Ensures subviews are contained into their shell
            for (NodeView &sb : contextNodeViews.at(n).second) {
                subWeights += sb.weight;
                subIntervals += sb.interval;
            }
            for (NodeView &sb : contextNodeViews.at(n).second) {
                sb.weight = sb.weight / subWeights * nv.weight;
                if (!weightOnly) {
                    sb.interval = sb.interval / subIntervals * nv.interval;
                }
            }
        }
    }
}


const std::vector<edge> &ViewStorage::getSortedEdges() const {
    return sortedEdgeKeys;
}


const unordered_map<edge, EdgeView> &ViewStorage::getEdgeViews() const {
    return edgeViews;
}

void NodeViewStorage::insertNodeView(const node n,
                                     const NodeView &nv,
                                     const DistribView &dv) {
    assert(nodeViews.find(n) == nodeViews.end());
    nodeViews.insert(std::make_pair(n, make_pair(nv, dv)));
}

void ViewStorage::setEdgeAlphaIf(float alpha, std::function<bool(const EdgeView &)> f) {
    for (auto &evPair : edgeViews)
        if (f(get<EdgeView>(evPair)))
            get<EdgeView>(evPair).setAlpha(alpha);
}

void NodeViewStorage::setNodeAlpha(const node &n, float alpha) {
    getNodeView(n).setAlpha(alpha);
}

void NodeViewStorage::insertContextNodeView(const node n, const NodeView &shell, std::deque<NodeView> &nvs) {
    assert(contextNodeViews.find(n) == contextNodeViews.end());
    contextNodeViews.insert(std::make_pair(n, make_pair(shell, nvs)));
}

void NodeViewStorage::setNodesAlpha(float alpha) {
    for (auto &nodePair : nodeViews)
        nodePair.second.first.setAlpha(alpha);
}

void NodeViewStorage::setNodesAlphaIf(float alpha, std::function<bool(const NodeView &, Type type)> fun) {
    for (auto &nodePair : nodeViews)
        if (fun(nodePair.second.first, Type::FOCUS))
            nodePair.second.first.setAlpha(alpha);
    for (auto &nodePair : contextNodeViews) {
        if (fun(nodePair.second.first, Type::CONTEXT_SHELL))
            nodePair.second.first.setAlpha(alpha);
        for (auto &dv : nodePair.second.second)
            if (fun(dv, Type::CONTEXT_DETAIL))
                dv.setAlpha(alpha);
    }
}
