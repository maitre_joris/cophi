#include "renderer/animation/FocusAnimator.h"

using namespace std;


void transition(NodeView &nv, const NodeView &i, const NodeView &f, float step) {
    nv.widthReduction = Animation::transition(i.widthReduction, f.widthReduction, step);
    nv.weightBefore = Animation::transition(i.weightBefore, f.weightBefore, step);
    nv.weight = Animation::transition(i.weight, f.weight, step);
    nv.intervalBefore = Animation::transition(i.intervalBefore, f.intervalBefore, step);
    nv.interval = Animation::transition(i.interval, f.interval, step);
    nv.gapBefore = Animation::transition(i.gapBefore, f.gapBefore, step);
    nv.color[0] = Animation::transition(i.color[0], f.color[0], step);
    nv.color[1] = Animation::transition(i.color[1], f.color[1], step);
    nv.color[2] = Animation::transition(i.color[2], f.color[2], step);
    nv.color[3] = Animation::transition(i.color[3], f.color[3], step);
}

FocusAnimator::FocusAnimator(const DataHandler *dataHandler,
                             const DisplayParameters *parameters) :
        parameters(*parameters), dataHandler(*dataHandler) {

}


void FocusAnimator::initFocusExpand(cluster focusId, unsigned int axisId, const NodeViewStorage &views) {
    contractedFocusId = focusId;
    focusAxisId = axisId;
    type = Type::EXPAND;
    hierarchyChange = State(dataHandler.getContextNodeHierarchy(axisId, ClusterProp::HIGHER_META_NODE_ID),
                            dataHandler.getContextNodeHierarchy(axisId, ClusterProp::LOWER_META_NODE_ID),
                            0);
    setInitialState(views);
}

void FocusAnimator::initFocusCompress(cluster lastOpenedId,
                                      unsigned int axisId,
                                      unsigned int nbClosings,
                                      const NodeViewStorage &views) {
    assert(nbClosings > 0);
    focusAxisId = axisId;
    hierarchyChange = State(dataHandler.getContextNodeHierarchy(axisId, ClusterProp::HIGHER_META_NODE_ID),
                            dataHandler.getContextNodeHierarchy(axisId, ClusterProp::LOWER_META_NODE_ID),
                            nbClosings);
    contractedFocusId = lastOpenedId;
    type = Type::COMPRESS;
    setInitialState(views);
}

void FocusAnimator::clear() {
    focusAxisId = -1;
    contractedFocusId = ClusterProp::INVALID_ID;
}

void FocusAnimator::fadeVanishingNodes(float alpha, NodeViewStorage &views) {
    node upperContextNode;
    node lowerContextNode;

    // Fade context shells
    if (initialMapping.count(ClusterProp::HIGHER_META_NODE_ID) == 1) {
        upperContextNode = initialMapping.at(ClusterProp::HIGHER_META_NODE_ID);
        views.getNodeView(upperContextNode).setAlpha(alpha);
    }
    if (initialMapping.count(ClusterProp::LOWER_META_NODE_ID) == 1) {
        lowerContextNode = initialMapping.at(ClusterProp::LOWER_META_NODE_ID);
        views.getNodeView(lowerContextNode).setAlpha(alpha);
    }

    if (type == Type::EXPAND) {
        // Fade focus node
        const node &focusNode = initialMapping.at(contractedFocusId);
        views.getNodeView(focusNode).setAlpha(alpha);
    } else if (type == Type::COMPRESS) {
        // Fade detail views corresponding to contracted uncles
        unsigned int level = hierarchyChange.getLevelAfterClosings() + 1;
        LowHighPair<int> pos = hierarchyChange.getIdAtLevel(level);
        if (pos.getHigh() > -1)
            views.getDetailView(upperContextNode, pos.getHigh()).setAlpha(alpha);
        if (pos.getLow() > -1)
            views.getDetailView(lowerContextNode, pos.getLow()).setAlpha(alpha);
    }
}

void FocusAnimator::setAppearingAlpha(float alpha, NodeViewStorage &views) {
    /*for (const pair<cluster, node> &p : getLowerLevelNodes(focusAxisId))
        views.getNodeView(p.second).setAlpha(alpha);*/
    views.setNodesAlpha(alpha);
}


void removeMeta(Dimension &nodes) {
    if (nodes.find(ClusterProp::HIGHER_META_NODE_ID) != nodes.end())
        nodes.erase(ClusterProp::HIGHER_META_NODE_ID);
    if (nodes.find(ClusterProp::LOWER_META_NODE_ID) != nodes.end())
        nodes.erase(ClusterProp::LOWER_META_NODE_ID);
}

const Dimension FocusAnimator::getExpandedFocusNodes() const {
    Dimension result = getExpandedStateNodes();
    switch (type) {
        case Type::EXPAND:
            // When expanding, only focus node are contracted to focus shell
            removeMeta(result);
            break;
        case Type::COMPRESS:
            removeMeta(result);
            break;
    }
    return result;
}

const Dimension FocusAnimator::getExpandedContextNodes(ContextPosition position) const {
    // i.e retrieve uncles of focus node
    Dimension result = getCompressedStateNodes();
    removeMeta(result);
    if (result.find(contractedFocusId) != result.end()) {
        if (position == ContextPosition::HIGHER)
            result.erase(result.begin(), result.find(contractedFocusId));
        else if (position == ContextPosition::LOWER)
            result.erase(result.find(contractedFocusId), result.end());
        result.erase(contractedFocusId);
    }
    return result;
}

const Dimension &FocusAnimator::getCompressedStateNodes() const {
    assert(focusAxisId > -1);
    switch (type) {
        case Type::EXPAND:
            return initialMapping;
        case Type::COMPRESS:
            return dataHandler.getDimension(focusAxisId);
    }
    assert(false);
}

const Dimension &FocusAnimator::getExpandedStateNodes() const {
    assert(focusAxisId > -1);
    switch (type) {
        case Type::EXPAND:
            return dataHandler.getDimension((unsigned int) focusAxisId);;
        case Type::COMPRESS:
            return initialMapping;
    }
    assert(false);
}

const deque<NodeView> FocusAnimator::getMorphingDetailView(AtTime time, ContextPosition position) const {
    switch (type) {
        case Type::EXPAND:
            switch (position) {
                case ContextPosition::HIGHER:
                    switch (time) {
                        case AtTime::INITIAL:
                            return initialViews.upperContext.second;
                        case AtTime::FINAL:
                            return finalViews.upperContext.second;
                    }
                case ContextPosition::LOWER:
                    switch (time) {
                        case AtTime::INITIAL:
                            return initialViews.lowerContext.second;
                        case AtTime::FINAL:
                            return finalViews.lowerContext.second;
                    }
            }
        case Type::COMPRESS:
            switch (position) {
                case ContextPosition::HIGHER:
                    switch (time) {
                        case AtTime::INITIAL: //TODO
                            return initialViews.upperContext.second;
                        case AtTime::FINAL:
                            return initialViews.upperContext.second;
                    }
                case ContextPosition::LOWER:
                    switch (time) {
                        case AtTime::INITIAL:
                            return initialViews.upperContext.second;
                        case AtTime::FINAL:
                            return initialViews.upperContext.second;
                    }
            }
    }
    return deque<NodeView>();
}

void FocusAnimator::contractToContext(NodeViewStorage &views,
                                      ContextPosition position,
                                      float step) {
    // Contract uncle node
    for (const auto &nodePair : getExpandedContextNodes(position)) {
        if (views.hasNode(nodePair.second)) {
            NodeView &current = views.getNodeView(nodePair.second);
            const auto &i = getContractionView(nodePair.first, ContractedTo::CONTEXT, AtTime::INITIAL, position);
            const auto &f = getContractionView(nodePair.first, ContractedTo::CONTEXT, AtTime::FINAL, position);
            transition(current, i, f, step);
        }
    }
}

void FocusAnimator::reshapeContext(NodeViewStorage &views,
                                   pair<cluster, node> n,
                                   ContextPosition position,
                                   float step) {
    auto &ctxViews = views.getContextViews();
    // Morph detail views corresponding to provided node
    if (ctxViews.find(n.second) != ctxViews.end()) {
        const auto &i = (position == ContextPosition::HIGHER ? initialViews.upperContext
                                                             : initialViews.lowerContext).second;
        const auto &f = (position == ContextPosition::HIGHER ? finalViews.upperContext
                                                             : finalViews.lowerContext).second;
        auto &current = views.getDetailViews(n.second);
        unsigned int l = min(f.size(), min(i.size(), current.size()));
        for (unsigned int k = 0; k < l; k++) {
            transition(current.at(k), i.at(k), f.at(k), step);
        }
    }
}

const NodeView &FocusAnimator::getContractionView(cluster id,
                                                  ContractedTo to,
                                                  AtTime time,
                                                  ContextPosition position) const {
    switch (type) {
        case Type::EXPAND:
            switch (to) {
                case ContractedTo::FOCUS:
                    switch (time) {
                        case AtTime::INITIAL :
                            return contractedFocus.focus.at(id);
                        case AtTime::FINAL:
                            return finalViews.focus.at(id).first;
                    }
                case ContractedTo::CONTEXT:
                    switch (time) {
                        case AtTime::INITIAL :
                            return initialViews.focus.at(id).first;
                        case AtTime::FINAL:
                            return (position == ContextPosition::HIGHER ? upperContractedContext
                                                                        : lowerContractedContext).at(id);
                    }

            }
        case Type::COMPRESS:
            switch (to) {
                case ContractedTo::FOCUS:
                    switch (time) {
                        case AtTime::INITIAL :
                            return initialViews.focus.at(id).first;
                        case AtTime::FINAL:
                            return contractedFocus.focus.at(id);
                    }
                case ContractedTo::CONTEXT:
                    switch (time) {
                        case AtTime::INITIAL :
                            return (position == ContextPosition::HIGHER ? upperContractedContext
                                                                        : lowerContractedContext).at(id);
                        case AtTime::FINAL:
                            return finalViews.focus.at(id).first;
                    }

            }
    }
    assert(false);
}

void FocusAnimator::doExpandStep(float step, NodeViewStorage &mainViews, NodeViewStorage &overlayViews) {
    NodeViewStorage &withExpandedFocus = type == Type::EXPAND ? overlayViews : mainViews;
    NodeViewStorage &withExpandedContext = type == Type::EXPAND ? mainViews : overlayViews;

    assert(!contractedFocus.focus.empty());
    for (const auto &n : getExpandedStateNodes()) {
        // Focus nodes are always contracted to/from contractedFocus
        if (contractedFocus.focus.find(n.first) != contractedFocus.focus.end()) {
            const auto &i = getContractionView(n.first, ContractedTo::FOCUS, AtTime::INITIAL);
            const auto &f = getContractionView(n.first, ContractedTo::FOCUS, AtTime::FINAL);
            assert(i.isValid());
            assert(f.isValid());
            NodeView &current = withExpandedFocus.getNodeView(n.second);
            transition(current, i, f, step);
            assert(current.isValid());
        } else if (n.first == ClusterProp::HIGHER_META_NODE_ID) {
            reshapeContext(mainViews, n, ContextPosition::HIGHER, step);
            contractToContext(withExpandedContext, ContextPosition::HIGHER, step);
        } else if (n.first == ClusterProp::LOWER_META_NODE_ID) {
            reshapeContext(mainViews, n, ContextPosition::LOWER, step);
            contractToContext(withExpandedContext, ContextPosition::LOWER, step);
        }
    }
}


void FocusAnimator::doDivisionStep(float step,
                                   NodeViewStorage &mainViews,
                                   NodeViewStorage &overlayViews) {
    assert(focusAxisId > -1);
    switch (type) {
        case Type::EXPAND:
            if (overlayViews.empty()) {
                assert(!contractedFocus.focus.empty());
                // Add contractedToFocus views
                const Dimension &nodes = dataHandler.getDimension((unsigned int) focusAxisId);
                for (const auto &nodePair : nodes) {
                    if (contractedFocus.focus.find(nodePair.first) != contractedFocus.focus.end()) {
                        assert(finalViews.focus.count(nodePair.first) == 1);
                        NodeView &nv = contractedFocus.focus.at(nodePair.first);
                        DistribView db = finalViews.focus.at(nodePair.first).second;
                        overlayViews.insertNodeView(nodePair.second, nv, db);
                    }
                }
            }
            // Fade out focus node and context shell views
            fadeVanishingNodes(1.f - step, mainViews);
            break;
        case Type::COMPRESS:
            if (overlayViews.empty()) {
                assert(!contractedFocus.focus.empty());
                // Add contractedToContext views
                const Dimension &nodes = dataHandler.getDimension((unsigned int) focusAxisId);
                for (const auto &nodePair : nodes) {
                    NodeView nv;
                    if (upperContractedContext.find(nodePair.first) != upperContractedContext.end())
                        nv = upperContractedContext.at(nodePair.first);
                    if (lowerContractedContext.find(nodePair.first) != lowerContractedContext.end())
                        nv = lowerContractedContext.at(nodePair.first);
                    if (nv.isValid()) {
                        assert(finalViews.focus.count(nodePair.first) == 1);
                        DistribView db = finalViews.focus.at(nodePair.first).second;
                        overlayViews.insertNodeView(nodePair.second, nv, db);
                    }
                }
            }
            // Fade out expanded detail views and context shell views
            fadeVanishingNodes(1.f - step, mainViews);
            break;
    }
    overlayViews.setNodesAlpha(step);

}

const NodeView &FocusAnimator::getContextShell(ContextPosition position) const {
    switch (type) {
        case Type::COMPRESS: {
            // In compress mode, uncles are compressed in the nbClosings position from end
            unsigned int level = hierarchyChange.getLevelAfterClosings() + 1;
            assert(level > 0);
            LowHighPair<int> pos = hierarchyChange.getIdAtLevel(level);
            switch (position) {
                case ContextPosition::HIGHER:
                    return initialViews.upperContext.second.at((unsigned long) pos.getHigh());
                case ContextPosition::LOWER:
                    return initialViews.lowerContext.second.at((unsigned long) pos.getLow());
            }
        }
        case Type::EXPAND: {
            // In expand mode, uncles are compressed in the last detail view
            switch (position) {
                case ContextPosition::HIGHER:
                    return finalViews.upperContext.second.back();
                case ContextPosition::LOWER:
                    return finalViews.lowerContext.second.back();
            }
        }
    }
    return NodeView();
}

const NodeView &FocusAnimator::getFocusShell() const {
    switch (type) {
        case Type::EXPAND: {
            assert(!initialViews.focus.empty());
            assert(initialViews.focus.find(contractedFocusId) != initialViews.focus.end());
            return initialViews.focus.at(contractedFocusId).first;
        }
        case Type::COMPRESS: {
            assert(!finalViews.focus.empty());
            assert(finalViews.focus.find(contractedFocusId) != finalViews.focus.end());
            return finalViews.focus.at(contractedFocusId).first;
        }
    }
    return NodeView();
}


void FocusAnimator::setInitialState(const NodeViewStorage &views) {
    assert(focusAxisId > -1);
    saveViews(views, initialViews);
    initialMapping = dataHandler.getDimension((unsigned int) focusAxisId);
    if (type == Type::EXPAND)
        assert(initialViews.focus.find(contractedFocusId) != initialViews.focus.end());

}


void FocusAnimator::setFinalState(NodeViewStorage &views) {
    assert(focusAxisId > -1);
    saveViews(views, finalViews);
    switch (type) {
        case Type::EXPAND:
            computeContractedViews(initialViews, finalViews);
            break;
        case Type::COMPRESS:
            computeContractedViews(finalViews, initialViews);
            break;
    }
    if (type == Type::COMPRESS)
        assert(finalViews.focus.find(contractedFocusId) != finalViews.focus.end());

}

void FocusAnimator::saveViews(const NodeViewStorage &views,
                              FullNodeViewBundle &target) {
    assert(focusAxisId > -1);
    dataHandler.forEachNode(focusAxisId, [&](const node &n) {
        const NodeProp &np = dataHandler.getNodeProperty(n);
        cluster id = np.clusterId;
        const NodeView &nv = views.getNodeView(n);
        if (np.isLowerContext()) {
            target.lowerContext.first = nv;
            target.lowerContext.second = views.getDetailViews(n);
        } else if (np.isHigherContext()) {
            target.upperContext.first = nv;
            target.upperContext.second = views.getDetailViews(n);
        } else {
            target.focus.insert({id, std::make_pair(nv, views.getDistribView(n))});
        }
    });
}


void FocusAnimator::normalize(LightNodeViewBundle &views, bool withSpacing) {
    float totalWeight = 0.f;
    float totalGap = 0.f;
    float totalInterval = 0.f;
    float minGapBefore = std::numeric_limits<float>::infinity();
    float minIntervalBefore = std::numeric_limits<float>::infinity();
    float minWeightBefore = std::numeric_limits<float>::infinity();

    views.forEachInOrder([&](NodeView &nv) {
        totalWeight += nv.weight;
        totalGap = std::max(totalGap, nv.gapBefore);
        totalInterval = std::max(totalInterval, nv.intervalBefore + nv.interval);
        minGapBefore = std::min(minGapBefore, nv.gapBefore);
        minIntervalBefore = std::min(minIntervalBefore, nv.intervalBefore);
        minWeightBefore = std::min(minWeightBefore, nv.weightBefore);
    });
    if (totalGap > 0) totalGap -= minGapBefore;
    if (totalInterval > 0) totalInterval -= minIntervalBefore;
    float intervalBefore = 0.f;
    views.forEachInOrder([&](NodeView &nv) {
        nv.weight /= totalWeight;
        if (!withSpacing) nv.weight /= (1.f - parameters.getGapRatio());
        nv.weightBefore = (nv.weightBefore - minWeightBefore) / totalWeight;
        if (!withSpacing) nv.weightBefore /= (1.f - parameters.getGapRatio());
        if (withSpacing && totalGap > 0) nv.gapBefore = (nv.gapBefore - minGapBefore) / totalGap;
        else nv.gapBefore = 0.f;
        nv.interval /= totalInterval;
        if (withSpacing) nv.intervalBefore = (nv.intervalBefore - minIntervalBefore) / totalInterval;
        else nv.intervalBefore = intervalBefore;
        intervalBefore += nv.interval;
        //assert(nv.isValid());
    });
}

void FocusAnimator::fit(LightNodeViewBundle &views, const NodeView &shell, bool withSpacing) {
    // No special case when only one view need to be fitted to shell view
    normalize(views, withSpacing);

    float heightWeight = shell.weight * (1.f - parameters.getGapRatio());
    float heightInterval = shell.interval;
    float intervalBefore = shell.intervalBefore;
    views.forEachInOrder([&](NodeView &nv) {
        nv.color = shell.color;
        nv.widthReduction = shell.widthReduction;
        nv.weight *= heightWeight;
        if (withSpacing) {
            nv.gapBefore = std::min(1.f, shell.gapBefore + nv.gapBefore * heightWeight);
            nv.intervalBefore = shell.intervalBefore + nv.intervalBefore * heightInterval;
        } else {
            nv.gapBefore = shell.gapBefore;
            nv.intervalBefore = intervalBefore; // Somehow, quite wrong
        }
        nv.weightBefore = shell.weightBefore + nv.weightBefore * heightWeight;
        if (nv.subWeight != NodeView::NO_SUBWEIGHT)
            nv.subWeight *= heightWeight;
        nv.interval *= heightInterval;
        intervalBefore += nv.interval;
        assert(nv.weightBefore >= shell.weightBefore);
        assert(nv.gapBefore >= shell.gapBefore);
        assert(nv.isValid());
    });
}

void fillFocus(const Dimension &sourceNodes,
               const ViewIndex &sourceViews,
               map<cluster, NodeView> &result) {
    // Fill selected nodes' views into result
    for (const auto &n: sourceNodes) {
        const NodeView &target = sourceViews.at(n.first).first;
        assert(target.isValid());
        result.insert({n.first, target});
    }
}

void FocusAnimator::fill(const Dimension &sourceNodes,
                         const FullNodeViewBundle &sourceViews,
                         LightNodeViewBundle &result) {
    // Fill selected nodes' views into result
    fillFocus(sourceNodes, sourceViews.focus, result.focus);
    // Fill selected detail views into result (if expansion, nothing done)
    if (hierarchyChange.getNbClosings() > 0 && hierarchyChange.getNbLevels() > 0) {
        unsigned int level = hierarchyChange.getLevelAfterClosings();
        LowHighPair<int> pos = hierarchyChange.getIdAtLevel(level);
        const auto &upperDetailViews = sourceViews.upperContext.second;
        for (int i = pos.getHigh(); i < upperDetailViews.size(); i++) {
            result.upperDetails.insert({i, upperDetailViews.at((unsigned long) i)});
        }
        const auto &lowerDetailViews = sourceViews.upperContext.second;
        for (int i = pos.getLow(); i < lowerDetailViews.size(); i++) {
            result.lowerDetails.insert({i, lowerDetailViews.at((unsigned long) i)});
        }
    }
}

void FocusAnimator::computeContractedViews(const FullNodeViewBundle &withContractedFocus,
                                           const FullNodeViewBundle &withExpandedFocus) {
    // Contracted focus and detail views to focus shell
    const Dimension &expandedFocus = getExpandedFocusNodes();
    assert(!expandedFocus.empty());
    const NodeView &focusShell = getFocusShell();
    fill(expandedFocus, withExpandedFocus, contractedFocus);
    fit(contractedFocus, focusShell, true);

    //contract(expandedFocus, withExpandedFocus.focus, contractedFocus.focus, focusShell, true);
    assert(contractedFocus.focus.size() == expandedFocus.size());

    // Contracted focus nodes to context detail shell
    const Dimension &upperUncles = getExpandedContextNodes(ContextPosition::HIGHER);
    if (!upperUncles.empty()) {
        const NodeView &shell = getContextShell(ContextPosition::HIGHER);
        assert(shell.isValid());
        LightNodeViewBundle upper;
        fillFocus(upperUncles, withContractedFocus.focus, upper.focus);
        fit(upper, shell, false);
        upperContractedContext = upper.focus;
    }
    const Dimension &lowerUncles = getExpandedContextNodes(ContextPosition::LOWER);
    if (!lowerUncles.empty()) {
        const NodeView &shell = getContextShell(ContextPosition::LOWER);
        assert(shell.isValid());
        LightNodeViewBundle lower;
        fillFocus(lowerUncles, withContractedFocus.focus, lower.focus);
        fit(lower, shell, false);
        lowerContractedContext = lower.focus;
    }
}

