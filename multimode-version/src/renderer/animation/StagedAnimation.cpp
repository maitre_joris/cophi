#include <renderer/animation/StagedAnimation.h>
#include <assert.h>
#include <tuple>

using namespace std;

StagedAnimation::StagedAnimation(vector<pair<Stage, bool>> sequence) :
        stageSequence(sequence) {
  assert(get<Stage>(stageSequence.at(stageSequence.size() - 1)) == none);
}

StagedAnimation::Stage StagedAnimation::getStage() const {
  if (isAnimating()) return get<Stage>(stageSequence.at(currentStage));
  else return Stage::none;
}

bool StagedAnimation::is(Stage stage) const {
  return (isAnimating() && getStage() == stage);
}

void StagedAnimation::moveOnToNextStage(float time) {
  currentStage = (currentStage + 1) % stageSequence.size();
  initNextStep(getStepInit(currentStage));
  resetTime(time);
}


void StagedAnimation::skipStage(float time) {
  assert(isAnimating());
  moveOnToNextStage(time);
}

float StagedAnimation::getStepInit(unsigned int stageIndex) {
  return get<bool>(stageSequence.at(stageIndex)) ? 0.f : 1.f;
}

void StagedAnimation::animate(float time) {
  currentStage = 0;
  Animation::startAnimation(time, getStepInit(currentStage));
}

bool StagedAnimation::isEndingStage() const {
  return getStage() == none;
}

bool StagedAnimation::doStep(float time) {
  bool isEaseIn = get<bool>(stageSequence.at(currentStage));
  bool finished = isEaseIn ? doStepEaseIn(time) : doStepEaseOut(time);
  if (finished) {
    moveOnToNextStage(time);
    if (isEndingStage())
      endAnimation();
  }
  return finished;
}

void StagedAnimation::setStageSequence(
        std::vector<std::pair<Stage, bool>> sequence) {
  stageSequence = sequence;
}

void StagedAnimation::finish() {
  endAnimation();
}