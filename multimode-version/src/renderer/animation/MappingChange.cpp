#include <renderer/animation/MappingChange.h>

MappingChange::MappingChange() {}

void MappingChange::animate(float time, HeightMapping target) {
    if (target == HeightMapping::INTERVAL_SIZE) {
        easeIn = true;
        Animation::startAnimation(time, 0.f);
    } else {
        easeIn = false;
        Animation::startAnimation(time, 1.f);
    }
}

bool MappingChange::doStep(float time) {
    bool finished = (easeIn) ?
                    doStepEaseIn(time) :
                    doStepEaseOut(time);
    if (finished) {
        Animation::endAnimation();
    }
    return finished;
}

float MappingChange::getCurrentStep() const {
    if (isAnimating()) return Animation::getCurrentStep();
    else return 0.f;
}
