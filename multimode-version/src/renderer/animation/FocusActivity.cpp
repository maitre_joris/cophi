#include "renderer/animation/FocusActivity.h"
#include <assert.h>

using namespace std;

vector<pair<StagedAnimation::Stage, bool>> FocusActivity::FOCUS_SEQUENCE = {
        make_pair(StagedAnimation::fadeOut, false),
        make_pair(StagedAnimation::division, true),
        make_pair(StagedAnimation::merging, true),
        make_pair(StagedAnimation::fadeIn, true),
        make_pair(StagedAnimation::none, true)
};

FocusActivity::FocusActivity() : StagedAnimation(FOCUS_SEQUENCE) {}

void FocusActivity::animate(float time) {
    StagedAnimation::animate(time);
}

bool FocusActivity::doStep(float time) {
    assert(isAnimating());
    return StagedAnimation::doStep(time);
}

/*
bool FocusActivity::isForwards() const {
    return direction == forwards;
}*/
