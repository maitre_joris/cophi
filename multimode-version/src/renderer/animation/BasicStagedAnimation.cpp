#include "renderer/animation/BasicStagedAnimation.h"
#include <assert.h>

using namespace std;

vector<pair<StagedAnimation::Stage, bool>> BasicStagedAnimation::STAGE_SEQUENCE{
        make_pair(StagedAnimation::fadeOut, false),
        make_pair(StagedAnimation::shifting, true),
        make_pair(StagedAnimation::fadeIn, true),
        make_pair(StagedAnimation::none, true),
};

BasicStagedAnimation::BasicStagedAnimation() : StagedAnimation(
        BasicStagedAnimation::STAGE_SEQUENCE) {}

float BasicStagedAnimation::getStepDelta() {
    assert(isAnimating());
    return stepDelta.second - stepDelta.first;
}

float BasicStagedAnimation::getShiftAmount() const {
    assert(isAnimating());
    return shiftAmount;
}

void BasicStagedAnimation::animate(float time, float shiftAmount) {
    StagedAnimation::animate(time);
    assert(getStage() == fadeOut);
    stepDelta = std::make_pair(0.f, 0.f);
    this->shiftAmount = shiftAmount;
}

bool BasicStagedAnimation::doFadeOutStep(float time) {
    assert(isAnimating());
    bool finished = doStepEaseOut(time);
    if (finished) moveOnToNextStage(time);
    return finished;
}

bool BasicStagedAnimation::doShiftingStep(float time) {
    assert(isAnimating());
    bool finished = doStepEaseIn(time);
    if (shiftAmount > 0.f && !finished)
        stepDelta = std::make_pair(stepDelta.second, getCurrentStep());
    else moveOnToNextStage(time);
    return finished;
}


bool BasicStagedAnimation::doFadeInStep(float time) {
    assert(isAnimating());
    bool finished = doStepEaseIn(time);
    if (finished) {
        moveOnToNextStage(time);
        assert(getStage() == none);
        endAnimation();
    }
    return finished;
}

bool BasicStagedAnimation::doStep(float time) {
    bool finished = StagedAnimation::doStep(time);
    if (shiftAmount != 0.f && !finished)
        stepDelta = std::make_pair(stepDelta.second, getCurrentStep());
    return finished;
}