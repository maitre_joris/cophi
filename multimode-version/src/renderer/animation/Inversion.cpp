#include "renderer/animation/Inversion.h"
#include <util/Utils.h>

using namespace std;

Inversion::Inversion() : Animation() {}

bool Inversion::doStep(AxisInterval axes, float time) {
    assert(isAnimating());
    bool finished = doStepEaseIn(time);
    float step = Animation::getCurrentStep();
    for (unsigned int axisId : axes)
        isInverted[axisId] = (toBeInverted[axisId]) ? 1 - step : step;
    return finished;
}

void Inversion::clear() {
    endAnimation();
}

void Inversion::eraseAxes(AxisInterval axes, bool saveDeletedStates) {
    if (saveDeletedStates) {
        savedStates.clear();
        for (unsigned int axisId : axes)
            savedStates.push_back(isInverted[axisId]);
    }
    erase<float>(isInverted, axes.getLeft(), axes.size());
}

void Inversion::insertAxes(unsigned int fromAxisId, unsigned int size,
                           bool restoreSave) {
    if (restoreSave)
        emplace<float>(isInverted, fromAxisId, size);
    if (restoreSave) assert(savedStates.size() == size);
    for (unsigned int i = fromAxisId + 1; i < fromAxisId + size + 1; ++i) {
        isInverted[i] = restoreSave ? savedStates[i - (fromAxisId + 1)] : 0.f;
    }
}

float Inversion::getState(unsigned int axisId) const {
    return isInverted[axisId];
}

void Inversion::animate(AxisInterval axes, float time) {
    Animation::startAnimation(time, 0.f);
    toBeInverted.resize(axes.size());
    for (unsigned int axisId : axes)
        toBeInverted[axisId] = isInverted[axisId] != 0.f;
}

std::vector<float> Inversion::getStates() const {
    return isInverted;
}

void Inversion::init(unsigned int nbAxes) {
    isInverted = vector<float>(nbAxes, 0.f);
}

bool Inversion::isAnimating() const {
    return Animation::isAnimating() && !toBeInverted.empty();
}

void Inversion::endAnimation() {
    Animation::endAnimation();
    toBeInverted.clear();
}

unsigned int Inversion::getNbAxes() const {
    return isInverted.size();
}