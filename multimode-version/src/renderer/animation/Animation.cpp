#include <math.h>
#include <renderer/animation/Animation.h>

float Animation::easeInOut(float t) {// t must be in [0;1] as output is
  const float LINE = 9;   // in ]0;+inf[, when LINE -> 0, easeInOut(x) -> x
    if (t <= 0) return 0.f;
  else if (t >= 1) return 1;
  else
        return (float) (atan((t - .5f) * LINE)
                        / (-2 * atan(-.5f * LINE))
                        + .5);
}

Animation::Animation() { }

Animation::~Animation() { }


void Animation::setDuration(float dur) {
  duration = dur;
}

float Animation::transition(float start, float end, float step) {
  return end + (1.f - step) * (start - end);
}


float Animation::getCurrentStep() const {
  return currentStep;
}

bool Animation::isAnimating() const {
  return animating;
}

void Animation::endAnimation() {
  animating = false;
}

void Animation::startAnimation(float time, float initStep) {
  animating = true;
  startingTime = time;
  currentStep = initStep;
}


bool Animation::doStepEaseIn(float time) {
  currentStep = easeInOut((time - startingTime) / (duration));
  bool finished = currentStep >= 1.f;
    if (finished) currentStep = 1.f;
  return finished;
}

bool Animation::doStepEaseOut(float time) {
  currentStep = 1.f - easeInOut((time - startingTime) / (duration));
  bool finished = currentStep <= 0.f;
    if (finished) currentStep = 0.f;
  return finished;
}

void Animation::initNextStep(float step) {
  currentStep = step;
}

void Animation::resetTime(float time) {
  startingTime = time;
}