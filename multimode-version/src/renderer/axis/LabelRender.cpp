#include <fstream>
#include <queue>
#include <renderer/axis/LabelRender.h>
#include <renderer/axis/FontTextureManager.h>

using namespace std;

TextProgram::TextProgram() : ShaderProgram("shaders/text_shader.glsl") {
    posCoord = glGetAttribLocation(program, "posXYtexST");
    matProjMod = glGetUniformLocation(program, "matProjMod");
    texture = glGetUniformLocation(program, "texture");
    pos = glGetUniformLocation(program, "pos");
    res = glGetUniformLocation(program, "res");
    size = glGetUniformLocation(program, "size");
    anchorOffset = glGetUniformLocation(program, "anchorOffset");
    activateOutline = glGetUniformLocation(program, "activate_outline");
    outlineColor = glGetUniformLocation(program, "outline_color");
    glowColor = glGetUniformLocation(program, "glow_color");
    textColor = glGetUniformLocation(program, "glyph_color");
    rotation = glGetUniformLocation(program, "rotAngle");
}

const std::string LabelRender::FONT = "../fonts/Vera.ttf";
const float LabelRender::MIN_FONT_SIZE = 15;
const float LabelRender::MAX_FONT_SIZE = 30;

LabelRender::LabelRender(MatrixManager *manager) :
        matrixManager(manager),
        minTextSize(MIN_FONT_SIZE),
        maxTextSize(MAX_FONT_SIZE) {
    textShaders = new TextProgram();
    textColor = Color::Black;
    outlineColor = Color::Black;
    glowColor = Color::White;
}

LabelRender::~LabelRender() {
    clear();
    delete textShaders;
}

void LabelRender::clear() {
    for (Label *l : labels)
        delete l;
    labels.clear();
    labelsToDraw.clear();
}

void LabelRender::onScaleChange(const DisplayParameters &params) {
    for (unsigned i = 0; i < labels.size(); i++) {
        Label *label = labels[i];
        float x = float(i) * params.getAxisWidth() + params.getMaxNodeWidth() / 2;
        label->setHorizontalPosition(x);
    }
}

void LabelRender::addLabel(const string &str, float x, float y) {
    string labelText = str;
    // Ensuring max length
    if (str.length() > AXIS_LABEL_MAX_LENGTH)
        labelText = str.substr(0, AXIS_LABEL_MAX_LENGTH - LABEL_ELLIPSIS.size()) +
                    LABEL_ELLIPSIS;
    Label *lbl = new Label(labelText, FONT, 0, Vec2f(x, y));
    lbl->setRotation(AXIS_LABEL_ROT_ANGLE);
    labels.push_back(lbl);
}

void LabelRender::draw(float size) {
    if (labels.empty()) return;
    glEnable(GL_BLEND);
    glDisable(GL_CULL_FACE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDepthFunc(GL_LESS);

    std::vector<Label *>::const_iterator itOL = labels.begin();

    textShaders->use();
    glEnableVertexAttribArray(textShaders->posCoord);
    GlMat4f matProjMod = matrixManager->getMatProjMod();
    glUniformMatrix4fv(textShaders->matProjMod, 1, GL_FALSE, &matProjMod[0][0]);
    glUniform1i(textShaders->texture, 0);
    glActiveTexture(GL_TEXTURE0);
    int curFont = (*itOL)->getFont();
    texture_atlas_t *atlas = FontTextureManager::getInstance()->getAtlas(curFont);
    texture_font_t *font = nullptr;

    glBindTexture(GL_TEXTURE_2D, atlas->id);
    glUniform1i(textShaders->activateOutline, false);
    glUniform3f(textShaders->outlineColor, outlineColor.getR() / 255.f,
                outlineColor.getG() / 255.f, outlineColor.getB() / 255.f);
    glUniform3f(textShaders->glowColor, glowColor[0], glowColor[1], glowColor[2]);
    glUniform3f(textShaders->textColor, textColor.getR() / 255.f,
                textColor.getG() / 255.f, textColor.getB() / 255.f);
    glUniform4f(textShaders->res, matrixManager->getWindowWidth(),
                matrixManager->getWindowHeight(), minTextSize, maxTextSize);

    float i = 0;
    for (; itOL != labels.end(); ++itOL) {
        Label *l = (*itOL);
        {
            glBindBuffer(GL_ARRAY_BUFFER, l->getVBO());
            glVertexAttribPointer(textShaders->posCoord, 4, GL_FLOAT, GL_FALSE, 0,
                                  nullptr);
        }
        // This block could be get rid off as we are using a common font for all labels
        if (curFont != (*itOL)->getFont()) {
            curFont = (*itOL)->getFont();
            atlas = FontTextureManager::getInstance()->getAtlas((*itOL)->getFont());
            font = FontTextureManager::getInstance()->getTexture(curFont);
            glBindTexture(GL_TEXTURE_2D, atlas->id);
        }

        glUniform1f(textShaders->rotation, l->getRotation());
        glUniform2fv(textShaders->anchorOffset, 1, &(l->getAnchorOffset()[0]));
        glUniform1f(textShaders->size, size);
        glUniform2fv(textShaders->pos, 1, &(l->getPosition()[0]));
        glDrawArrays(GL_TRIANGLES, 0, l->getVBOSize());
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        i++;
    }
    glBindTexture(GL_TEXTURE_2D, 0);

    glDisableVertexAttribArray(textShaders->posCoord);
    glDisable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
}
