#include "renderer/axis/Slider.h"

using namespace std;

Slider::Slider() : TriangleView(), anchor(0), unchanged(true) {}

Slider::Slider(cluster a, float p) : TriangleView(p), anchor(a),
                                     unchanged(true) {
}


cluster Slider::getAnchor() const { return anchor; }

void Slider::setAnchor(cluster a) {
    anchor = a;
}

bool Slider::wasMoved() const { return !unchanged; }

MovingSlider::MovingSlider() : active(false) {}

MovingSlider::MovingSlider(bool upper, unsigned int axisId,
                           LowHighPair<Slider> *pair)
        : active(true), upper(upper), axisId(axisId), sliderPair(pair) {}

bool MovingSlider::isActive() const {
    return active;
}

void MovingSlider::deactivate() {
    //assert(active);
    active = false;
}

bool MovingSlider::isUpper() const {
    assert(isActive());
    return upper;
}

unsigned int MovingSlider::getAxisId() const {
    assert(isActive());
    return axisId;
}

void Slider::setPosition(float p) {
    TriangleView::setPosition(p);
    unchanged = false;
}

float MovingSlider::getPosition() const {
    if (upper) return sliderPair->getHigh().getPosition();
    else return sliderPair->getLow().getPosition();
}

cluster MovingSlider::getAnchor() const {
    if (upper) return sliderPair->getHigh().getAnchor();
    else return sliderPair->getLow().getAnchor();
}