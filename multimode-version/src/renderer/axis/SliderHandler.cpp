#include <renderer/axis/SliderHandler.h>

using namespace std;

const std::pair<float, float> SliderHandler::MOVE_BOUNDARIES =
        std::make_pair(1.02f, -0.02f);
const std::pair<float, float> SliderHandler::INIT_POS =
        std::make_pair(1.f, -0.f);

SliderHandler::SliderHandler(Display *display) : display(display) {
}


SliderHandler::~SliderHandler() = default;

void SliderHandler::updateBuffer() {
    // TODO: as changes occur only at on one axis at a time, update could be done
    // only on that particular axis except for initialisation
    std::vector<LowHighPair<TriangleView>> views;
    for (unsigned axisId = 0; axisId < sliders.size(); ++axisId) {
        Slider upper = sliders.at(axisId).getHigh();
        // Overriding position if moving upper slider
        if (slider.isActive() && slider.getAxisId() == axisId && slider.isUpper())
            upper.setPosition(slider.getPosition());
        Slider lower = sliders.at(axisId).getLow();
        // Overriding position if moving lower slider
        if (slider.isActive() && slider.getAxisId() == axisId && !slider.isUpper())
            lower.setPosition(slider.getPosition());
        views.emplace_back((TriangleView) lower, (TriangleView) upper);
    }
    display->updateTriangles(views);
}

void SliderHandler::init(unsigned int nbAxes) {
    sliders.resize(nbAxes);
    active = true;
}

void SliderHandler::setInitialPosition(unsigned int axisId,
                                       cluster upper,
                                       cluster lower) {
    sliders.at(axisId).getHigh() = Slider(upper, INIT_POS.first);
    sliders.at(axisId).getLow() = Slider(lower, INIT_POS.second);
}

bool SliderHandler::pickSlider(unsigned int axisId,
                               float modelY,
                               float modelHeight) {
    assert(axisId < sliders.size());
    LowHighPair<Slider> *sliderPair = &sliders[axisId];
    float y = modelY / modelHeight;
    float sliderSize = Slider::HEIGHT / modelHeight;

    if (abs(y - sliderPair->getHigh().getPosition()) <= sliderSize) {
        slider = MovingSlider(true, axisId, sliderPair);
    } else if ((abs(sliderPair->getLow().getPosition() - y) <= sliderSize)) {
        slider = MovingSlider(false, axisId, sliderPair);
    }
    return isSliderPicked();
}

bool SliderHandler::isSliderPicked() const {
    return slider.isActive();
}

void SliderHandler::release() {
    //TODO issue here on YY
    slider.deactivate();
}

const MovingSlider SliderHandler::getMovingSlider() const {
    assert(isSliderPicked());
    return slider;
}

pair<cluster, cluster> SliderHandler::getSliderAnchors(unsigned int axisId) const {
    const LowHighPair<Slider> &pair = sliders.at(axisId);
    return make_pair(pair.getHigh().getAnchor(), pair.getLow().getAnchor());
}

void SliderHandler::hookMovingSlider(cluster id, float position) {
    LowHighPair<Slider> &pair = sliders.at(slider.getAxisId());
    Slider &staticSlider = slider.isUpper() ? pair.getHigh() : pair.getLow();
    staticSlider.setPosition(position);
    staticSlider.setAnchor(id);
    assert(pair.getHigh().getPosition() > pair.getLow().getPosition());
}

bool SliderHandler::isEditedSliderPair(unsigned int axisId) const {
    const auto &p = sliders.at(axisId);
    return p.getHigh().wasMoved() || p.getLow().wasMoved();
}

bool SliderHandler::isActive() const {
    return active;
}

void SliderHandler::clear() {
    active = false;
    sliders.clear();
}