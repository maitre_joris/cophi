#include "renderer/axis/FontTextureManager.h"
#include <stdlib.h>
#include <cstring>
#include <edtaa3func.h>

unsigned char *
make_distance_map(unsigned char *img,
                  unsigned int width, unsigned int height) {
    short *xdist = (short *) malloc(width * height * sizeof(short));
    short *ydist = (short *) malloc(width * height * sizeof(short));
    double *gx = (double *) calloc(width * height, sizeof(double));
    double *gy = (double *) calloc(width * height, sizeof(double));
    double *data = (double *) calloc(width * height, sizeof(double));
    double *outside = (double *) calloc(width * height, sizeof(double));
    double *inside = (double *) calloc(width * height, sizeof(double));
    size_t i;

    // Convert img into double (data)
    double img_min = 255, img_max = -255;
    for (i = 0; i < width * height; ++i) {
        double v = img[i];
        data[i] = v;
        if (v > img_max) img_max = v;
        if (v < img_min) img_min = v;
    }
    // Rescale image levels between 0 and 1
    for (i = 0; i < width * height; ++i) {
        data[i] = (img[i] - img_min) / img_max;
    }

    // Compute outside = edtaa3(bitmap); % Transform background (0's)
    computegradient(data, width, height, gx, gy);
    edtaa3(data, gx, gy, height, width, xdist, ydist, outside);
    for (i = 0; i < width * height; ++i)
        if (outside[i] < 0)
            outside[i] = 0.0;

    // Compute inside = edtaa3(1-bitmap); % Transform foreground (1's)
    memset(gx, 0, sizeof(double) * width * height);
    memset(gy, 0, sizeof(double) * width * height);
    for (i = 0; i < width * height; ++i)
        data[i] = 1 - data[i];
    computegradient(data, width, height, gx, gy);
    edtaa3(data, gx, gy, height, width, xdist, ydist, inside);
    for (i = 0; i < width * height; ++i)
        if (inside[i] < 0)
            inside[i] = 0.0;

    // distmap = outside - inside; % Bipolar distance field
    unsigned char *out = (unsigned char *) malloc(
            width * height * sizeof(unsigned char));
    for (i = 0; i < width * height; ++i) {
        outside[i] -= inside[i];
        outside[i] = 128 + outside[i] * 16;
        if (outside[i] < 0) outside[i] = 0;
        if (outside[i] > 255) outside[i] = 255;
        out[i] = 255 - (unsigned char) outside[i];
        //out[i] = (unsigned char) outside[i];
    }

    free(xdist);
    free(ydist);
    free(gx);
    free(gy);
    free(data);
    free(outside);
    free(inside);
    return out;
}

FontTextureManager *FontTextureManager::singleton = 0;


FontTextureManager::FontTextureManager() :
        currentIndex(0) {

}

FontTextureManager::~FontTextureManager() {
    for (std::vector<texture_atlas_t *>::iterator it = atlasVector.begin();
         it != atlasVector.end();
         ++it) {
        texture_atlas_delete(*it);
    }

    for (std::vector<texture_font_t *>::iterator it = texturesVector.begin();
         it != texturesVector.end();
         ++it) {
        texture_font_delete(*it);
    }
}

int FontTextureManager::loadFont(std::string fontName) {
    texture_atlas_t *atlas = texture_atlas_new(1024, 1024, 1);
    texture_font_t *fontTexture = texture_font_new(atlas, fontName.c_str(), 64);
    texture_atlas_upload(atlas);
    atlasVector.push_back(atlas);
    texturesVector.push_back(fontTexture);
    int fontIndex = currentIndex++;
    fontMap[fontName] = fontIndex;
    return fontIndex;
}

int FontTextureManager::getId(std::string fontName) {

    std::map<std::string, int>::iterator font = fontMap.find(fontName);
    if (font == fontMap.end()) {
        return loadFont(fontName);
    }

    return font->second;
}

std::string FontTextureManager::getName(int fontId) {
    std::map<std::string, int>::iterator it = fontMap.begin();
    for (; it != fontMap.end(); ++it) {
        if (it->second == fontId)
            return it->first;
    }
    return "";
}

texture_atlas_t *FontTextureManager::getAtlas(int fontId) {
    return atlasVector[fontId];
}

texture_atlas_t *FontTextureManager::getAtlas(std::string fontName) {
    return getAtlas(getId(fontName));
}

texture_font_t *FontTextureManager::getTexture(int fontId) {
    return texturesVector[fontId];
}

texture_font_t *FontTextureManager::getTexture(std::string fontName) {
    return getTexture(getId(fontName));
}

void FontTextureManager::convertTextureToDistanceMap(int fontId) {
    if (!distanceMapComputed) {
        texture_atlas_t *atlas = getAtlas(fontId);
        unsigned char *map = make_distance_map(atlas->data, atlas->width,
                                               atlas->height);
        memcpy(atlas->data, map,
               atlas->width * atlas->height * sizeof(unsigned char));
        free(map);
        texture_atlas_upload(atlas);
    }
    distanceMapComputed = true;
}