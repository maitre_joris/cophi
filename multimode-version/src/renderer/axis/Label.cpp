#include <renderer/axis/FontTextureManager.h>
#include <tlp/utf8Iterator.h>
#include "renderer/axis/Label.h"

using namespace std;

Label::Label(string text, string font, float angle, Vec2f pos)
        : modelPos(pos), size(1.f), rotation(angle) {
    this->font = FontTextureManager::getInstance()->getId(font);
    glGenBuffers(1, &vboID);
    setText(text);
}

/*
Rectf Label::getBoundingBox() {
  texture_font_t *texFont = FontTextureManager::getInstance()->getTexture(font);
  float h = texFont->height;
  float ascender = texFont->ascender / h;
  float descender = texFont->descender / h;
  assert(length > 0);
  return Rectf(modelPos[0] - length / (2.f * h),
               modelPos[1] - ascender,
               modelPos[0] + length / (2.f * h),
               modelPos[1] - descender);
}*/

static std::vector<wchar_t> getCharcodes(std::string str) {
    Iterator<wchar_t> *utf8iter = new tlp::Utf8Iterator(str);
    std::vector<wchar_t> charcodes;
    while (utf8iter->hasNext()) {
        charcodes.push_back(utf8iter->next());
    }
    delete utf8iter;
    return charcodes;
}

void Label::setText(const string &text) {
    nbGlyphs = 0;
    data.clear();
    texture_font_t *textureFont = FontTextureManager::getInstance()->getTexture(
            font);

    std::vector<wchar_t> charcodes = getCharcodes(text);

    wchar_t previous = 0;
    vec2 pen = {{0, 0}};
    for (std::vector<wchar_t>::const_iterator it = charcodes.cbegin();
         it != charcodes.cend(); ++it) {
        texture_glyph_t *glyph = texture_font_get_glyph(textureFont, *it);
        if (glyph != NULL) {
            int kerning = 0;
            if (it != charcodes.cbegin()) {
                kerning = texture_glyph_get_kerning(glyph, previous);
            }
            pen.x += kerning;
            float x0 = pen.x + glyph->offset_x;
            float y0 = pen.y + glyph->offset_y;
            float x1 = x0 + glyph->width;
            float y1 = y0 - glyph->height;
            float s0 = glyph->s0;
            float t0 = glyph->t0;
            float s1 = glyph->s1;
            float t1 = glyph->t1;
            /*    2------1  5
             *    |     /  /|
             *    |   /  /  |
             *    | /  /    |
             *    0  3------4
             */
            data.push_back(Vec4f(x0, y0, s0, t0));
            data.push_back(Vec4f(x1, y1, s1, t1));
            data.push_back(Vec4f(x0, y1, s0, t1));
            data.push_back(Vec4f(x0, y0, s0, t0));
            data.push_back(Vec4f(x1, y0, s1, t0));
            data.push_back(Vec4f(x1, y1, s1, t1));
            pen.x += glyph->advance_x;
        }
        previous = *it;
        nbGlyphs++;
    }

    //length = pen.x;
    float min = pen.x, max = 0;
    for (unsigned int i = 0; i < data.size(); ++i) {
        if (data[i][0] < min) min = data[i][0];
        if (data[i][0] > max) max = data[i][0];
    }
    length = max - min;
    //float height = textureFont->ascender - textureFont->descender;
    float height = textureFont->height;

    for (unsigned int i = 0; i < data.size(); ++i) {
        data[i][0] = (data[i][0] - length / 2 - min) / height;
        data[i][1] /= height;
    }
    length /= height;

    glBindBuffer(GL_ARRAY_BUFFER, vboID);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vec4f) * data.size(), &data[0],
                 GL_STATIC_DRAW);

    //computeModelBox();
}

void Label::setRotation(const float angle) {
    rotation = angle;
}

/*RectangleNA Label::getBaseBox() {
    texture_font_t *texFont = FontTextureManager::getInstance()->getTexture(font);
    float h = texFont->height;
    float ascender = texFont->ascender / h;
    float descender = texFont->descender / h;
    return Rectf(-length / 2.f, descender,
                 length / 2.f, ascender);
}*/

int Label::getFont() const {
    return font;
}

void Label::setHorizontalPosition(float x) {
    modelPos.setX(x);
}

/*float Label::computeCorrectiveScale(const Vec2f &screenRes,
                                    const GlMat4f &matProjMod) {
  float projectedLabelSize = size * matProjMod[1][1] * screenRes.y() / 2.0;
  float minPixelSize = minSize * 1.75;
  float maxPixelSize = maxSize * 1.75;
  float constrainedSize = std::max(std::min(projectedLabelSize, maxPixelSize),
                                   minPixelSize);
  return constrainedSize / projectedLabelSize;
}*/

GLuint Label::getVBO() const {
    return vboID;
}

int Label::getVBOSize() const {
    return nbGlyphs * 6;
}

float Label::getRotation() const {
    return rotation;
}

Vec2f Label::getPosition() const {
    return modelPos;
}

Vec2f Label::getAnchorOffset() const {
    return Vec2f(-length / 2.f, -1.f);
}