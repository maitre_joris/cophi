#include "renderer/axis/AxisHandler.h"


AxisHandler::AxisHandler(Display *display) : display(display) {
}


void AxisHandler::init(Animation::AnimationObserver *o) {
    observer = o;
    inversions.init(o->getNbAxes());
    display->updateAxes(inversions);
}

bool AxisHandler::isAnimating(int mask) const {
    bool result = false;
    if ((Animation::INVERSION & mask) == Animation::INVERSION) {
        result = result || inversions.isAnimating();
    }
    if ((Animation::SWAP & mask) == Animation::SWAP) {
        result = result || animatingSwap;
    }
    if ((Animation::DELETION & mask) == Animation::DELETION) {
        result = result || deletions.isAnimating(); //!toDelete.isEmpty();
    }
    if ((Animation::INSERTION & mask) == Animation::INSERTION) {
        result = result || insertions.isAnimating(); //!toInsert.empty();
    }
    return result;
}

void AxisHandler::setCallBacks(SetOpacityFunc setOpacity,
                               RebuildFunc onAxisOperation,
                               ShiftFunc shiftViews,
                               InvertFunc updateInversionRate) {
    this->setOpacity = setOpacity;
    this->rebuildViews = onAxisOperation;
    this->shiftViews = shiftViews;
    this->updateInversionRate = updateInversionRate;
}

void AxisHandler::prepareDeletion(AxisInterval axes,
                                  unsigned int lastAxis) {
    toDelete = axes;
    noShifting = toDelete.getRight() == lastAxis;
    shiftAmount = !noShifting ? -(float) toDelete.size() : 0.f;
    noFade = toDelete.isSideOn(lastAxis);
}

void AxisHandler::prepareInsertion(DimensionInterval toInsert,
                                   int justAfter,
                                   unsigned int lastAxis) {
    this->toInsert = toInsert;
    prevAxis = justAfter;
    noShifting = prevAxis == (int) lastAxis;
    shiftAmount = !noShifting ? (float) toInsert.size() : 0.f;
    noFade = prevAxis == -1 || noShifting;
}

void AxisHandler::launchInsertion(float time) {
    insertions.animate(time, shiftAmount);
    bootstrapAnimation();
    assert(isAnimating(Animation::INSERTION));
}

void AxisHandler::launchInversion(AxisInterval toInvert, float time) {
    if (toInvert.isEmpty()) return;
    this->toInvert = toInvert;
    inversions.animate(toInvert, time);
    bootstrapAnimation();
    assert(inversions.isAnimating());
    assert(isAnimating(Animation::INVERSION));
}

void AxisHandler::launchDeletion(float time) {
    deletions.animate(time, shiftAmount);
    bootstrapAnimation();
    assert(isAnimating(Animation::DELETION));
}

bool AxisHandler::noActivityPending() const {
    return prevAxis == NONE_ID && toInsert.empty() && toDelete.isEmpty();
}

void AxisHandler::clearOperation() {
    prevAxis = NONE_ID;
    toInsert.clear();
    toDelete.clear();
    toInvert.clear();
}

void AxisHandler::setSwap(bool s) {
    animatingSwap = s;
}

bool AxisHandler::tickInsertion(float time) {
    BasicStagedAnimation::Stage stage = insertions.getStage();
    bool finished = insertions.doStep(time);
    float step = insertions.getCurrentStep();
    switch (stage) {
        case BasicStagedAnimation::fadeOut: {
            if (noFade && !finished) insertions.skipStage(time);
            if (noFade || finished) {
            } else {
                assert(prevAxis >= 0);
                setOpacity(prevAxis, prevAxis, step, true);
            }
            return false; // Not the last step
        }
        case BasicStagedAnimation::shifting: {
            if (noShifting && !finished) insertions.skipStage(time);
            if (noShifting || finished) {
                observer->insertAxes(toInsert,
                                     prevAxis);
                inversions.insertAxes((unsigned int) prevAxis,
                                      (unsigned int) toInsert.size(),
                                      animatingSwap);
                display->updateAxes(inversions);
                rebuildViews();
                unsigned int lastRealAxis =
                        std::min(prevAxis + (unsigned int) toInsert.size(),
                                 observer->getNbAxes() - 1);
                setOpacity((unsigned int) (prevAxis + 1), lastRealAxis, 0.f, false);
            } else {
                float sstep = std::max(insertions.getStepDelta(), 0.f); // Hard fix
                shiftViews((unsigned int) (prevAxis + 1),
                           sstep * insertions.getShiftAmount());
            }
            return false; // Not the last step
        }
        case BasicStagedAnimation::fadeIn: {
            if (finished) {
                wrapUpAnimation();
                display->getParameters()->setDisplayLabels(true);
                display->getParameters()->setDisplayAxes(true);
                animatingSwap = false;
            } else {
                unsigned int lastRealAxis = std::min((unsigned int) (prevAxis + toInsert.size()),
                                                     observer->getNbAxes() - 1);
                setOpacity((unsigned int) (prevAxis + 1), lastRealAxis, step, false);
            }
            return finished;
        }
        default:
            throw InvalidAnimationStage();
    }
}

bool AxisHandler::tick(float time) {
    if (isAnimating(Animation::INSERTION)) {
        return tickInsertion(time);
    } else if (isAnimating(Animation::DELETION)) {
        return tickDeletion(time);
    } else if (isAnimating(Animation::INVERSION)) {
        return tickInversion(time);
    } else return true; // Animation finished
}


bool AxisHandler::tickDeletion(float time) {
    assert(!toDelete.isEmpty());
    BasicStagedAnimation::Stage stage = deletions.getStage();
    bool finished = deletions.doStep(time);
    float step = deletions.getCurrentStep();
    switch (stage) {
        case BasicStagedAnimation::fadeOut: {
            if (finished) {
                setOpacity(toDelete.getLeft(), toDelete.getRight(), 0.f, false);
            } else {
                setOpacity(toDelete.getLeft(), toDelete.getRight(), step, false);
            }
            return false; // No the last step
        }
        case BasicStagedAnimation::fadeIn: {
            if (noFade) deletions.finish();
            if (noFade || finished) {
                unsigned int left = std::max((int) toDelete.getLeft() - 1, 0);
                setOpacity(left, left, 1.f, true);
                wrapUpAnimation();
                return true; // Last step
            } else {
                unsigned int left = std::max((int) toDelete.getLeft() - 1, 0);
                setOpacity(left, left, step, true);
            }
            break;
        }
        case BasicStagedAnimation::shifting: {
            if (noShifting && !finished) deletions.skipStage(time);
            if (noShifting || finished) {
                observer->deleteDimension(toDelete);
                inversions.eraseAxes(toDelete, animatingSwap);
                display->updateAxes(inversions);
                rebuildViews();
                // Prepare fading in for stitching edges where axes were removed
                if (toDelete.getLeft() > 0) {
                    setOpacity(toDelete.getLeft() - 1, toDelete.getLeft() - 1, 0.f, true);
                }
            } else {
                // Shifting dimensions to the right, from newly numbered axis
                shiftViews(toDelete.getLeft(),
                           deletions.getStepDelta() *
                           deletions.getShiftAmount());
            }
            return false; // Not the last step
        }
        default:
            throw InvalidAnimationStage();
    }
    return false;
}

void AxisHandler::bootstrapAnimation() {
    display->getParameters()->setDisplayLabels(false);
    display->getParameters()->setDisplayAxes(false);
}

void AxisHandler::wrapUpAnimation() {
    if (!animatingSwap) {
        inversions.clear();
        display->getParameters()->setDisplayLabels(true);
        display->getParameters()->setDisplayAxes(true);
    }
    clearOperation();
    rebuildViews();

}

bool AxisHandler::tickInversion(float time) {
    bool finishedStep = inversions.doStep(toInvert, time);
    display->updateAxes(inversions);
    assert(!toInvert.isEmpty());
    updateInversionRate(toInvert, inversions.getStates());
    if (finishedStep) {
        wrapUpAnimation();
    }
    return finishedStep;
}

float AxisHandler::getInversionRate(unsigned int axisId) const {
    return inversions.getState(axisId);
}