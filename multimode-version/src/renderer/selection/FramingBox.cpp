#include "renderer/selection/SelectionBox.h"

void FramingBox::setFirstPoint(float x) { firstX = x; }

void FramingBox::setSecondPoint(float x) { secondX = x; }

float FramingBox::getLeftX() const { return std::min(firstX, secondX); }

float FramingBox::getRightX() const { return std::max(firstX, secondX); }

void FramingBox::hide() { isActive = false; }

void FramingBox::show() { isActive = true; }

bool FramingBox::isShown() { return isActive; }

bool FramingBox::contains(float x) const {
    return x <= getRightX() && x >= getLeftX();
}

void FramingBox::adapt(const AxisInterval &axes,
                       float padding,
                       float width,
                       float nodeWidth) {
    float leftAxisPos = axes.getLeft() * (width + nodeWidth) + nodeWidth / 2.f;
    float rightAxisPos = axes.getRight() * (width + nodeWidth) + nodeWidth / 2.f;
    setFirstPoint(leftAxisPos - padding - nodeWidth / 2.f);
    setSecondPoint(rightAxisPos + padding + nodeWidth / 2.f);
}

void FramingBox::scale(float factor) {
    firstX *= factor;
    secondX *= factor;
}