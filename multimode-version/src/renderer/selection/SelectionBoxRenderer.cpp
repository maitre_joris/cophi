#include "renderer/selection/SelectionBoxRenderer.h"

using namespace std;

SelectionBoxRenderer::SelectionBoxRenderer() {
    startup();
    setBuffer();
}

SelectionBoxRenderer::SelectionBoxProgram::SelectionBoxProgram() : ShaderProgram("shaders/selection_box_shader.glsl") {
    matProjMod = glGetUniformLocation(program, "matProjMod");
    z = glGetUniformLocation(program, "z");
    height = glGetUniformLocation(program, "height");
    width = glGetUniformLocation(program, "width");
    lowerLeftAnchor = glGetUniformLocation(program, "lowerLeftAnchor");
    res = glGetUniformLocation(program, "res");

}

void SelectionBoxRenderer::startup() {
    /* This need to be done only once */
    glGenBuffers(1, &buffer);
}


void SelectionBoxRenderer::shutdown() {
    glDeleteBuffers(1, &buffer);

}

void SelectionBoxRenderer::setBuffer() {

    static const GLfloat vertices[4][2] = {
            {0.f, 1.f},
            {0.f, 0.f},
            {1.f, 1.f},
            {1.f, 0.f},
    };
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
}

void SelectionBoxRenderer::draw(GlMat4f mat, const Vec2f res, SolidRectangle r) {
    boxProgram.use();
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    GLuint loc = boxProgram.getAttributeLocation("vertex");
    glEnableVertexAttribArray(loc);
    glVertexAttribPointer(loc, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (void *) 0);
    glUniformMatrix4fv(boxProgram.matProjMod, 1, GL_FALSE, &mat[0][0]);
    glUniform1f(boxProgram.z, r.z);
    glUniform1f(boxProgram.width, r.getWidth());
    glUniform2f(boxProgram.res, res.x(), res.y());
    glUniform1f(boxProgram.height, r.getHeight());
    glUniform2fv(boxProgram.lowerLeftAnchor, 1, &(r.getLowerLeft())[0]);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glDisableVertexAttribArray(loc);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

SelectionBoxRenderer::~SelectionBoxRenderer() {
    shutdown();
}
