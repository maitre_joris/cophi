#include <GL/gl.h>

#include "renderer/MatrixManager.h"

MatrixManager::MatrixManager() :
        shouldRecompute(false), changed(false), winW(0), winH(0) {
  identity(mModelView);
  identity(mProjection);
  identity(mProjMod);
}

GlMat4f MatrixManager::getMatProjMod() {
  if (shouldRecompute) {
    computeMatProjMod();
    shouldRecompute = false;
  }
  return mProjMod;
}

void MatrixManager::setModelViewMatrix(GlMat4f m) {
  mModelView = m;
  shouldRecompute = true;
  changed = true;
}

void MatrixManager::setProjectionMatrix(GlMat4f m) {
  mProjection = m;
  shouldRecompute = true;
  changed = true;
}

void MatrixManager::moveModel(Vec3f translationVector) {
  glTranslate(mModelView, translationVector);
  shouldRecompute = true;
  changed = true;
}

void MatrixManager::moveScreen(Vec3f translationVector) {
  GlMat4f projInv = mProjection;
  projInv.inverse();
  GlMat4f trans;
  translate(trans, translationVector);

  mModelView = projInv * trans * mProjection * mModelView;
  shouldRecompute = true;
  changed = true;
}

void MatrixManager::zoom(float scale, Vec2f screenPos) {
  const float xScreen = (screenPos[0] / winW * 2.0f) - 1.0f;
  const float yScreen = -((screenPos[1] / winH * 2.0f) - 1.0f);

  const float scaleTransRatio = (1 - scale) / scale;
  moveScreen(Vec3f(xScreen * scaleTransRatio, yScreen * scaleTransRatio, 0));

  glScale(mProjection, Vec3f(scale, scale, 1.f));

  shouldRecompute = true;
  changed = true;
}

void MatrixManager::initViewport(Vec4i viewport) {
  winW = viewport[2];
  winH = viewport[3];

  glViewport(viewport[0], viewport[1], viewport[2], viewport[3]);
  identity(mProjection);
  ortho(mProjection, -winW / 2, winW / 2, -winH / 2, winH / 2, -100.f, 100.f);

  shouldRecompute = true;
  changed = true;
}

void MatrixManager::changeViewport(Vec4i viewport) {
  GlMat4f oldOrtho;
  ortho(oldOrtho, -winW / 2, winW / 2, -winH / 2, winH / 2, -100.f, 100.f);

  oldOrtho.inverse();

  GlMat4f mZoom = mProjection * oldOrtho;

  initViewport(viewport);

  mProjection = mZoom * mProjection;
}

void MatrixManager::centerBox(Vec4f modelBox) {
  reinitMatrices();
  Vec2f center = Vec2f((modelBox[2] + modelBox[0]) / 2.0f,
                       (modelBox[3] + modelBox[1]) / 2.0f);
  moveModel(Vec3f(-center, 0));

  ortho(mProjection, -winW / 2, winW / 2, -winH / 2, winH / 2, -100.f, 100.f);
  Vec2f graphBox = Vec2f(modelBox[2] - modelBox[0], modelBox[3] - modelBox[1]);
  const int margin = 30;
  float ratio = std::min((winH - margin) / graphBox[1],
                         (winW - margin) / graphBox[0]);
  glScale(mProjection, Vec3f(ratio, ratio, 1.0f));
}

void MatrixManager::reinitMatrices() {
  identity(mModelView);
  identity(mProjection);
  identity(mProjMod);
  shouldRecompute = true;
  changed = true;
}

void MatrixManager::computeMatProjMod() {
  mProjMod = mProjection * mModelView;
}


Vec2f MatrixManager::modelToScreen(Vec3f model) {
  Vec4f tmp = getMatProjMod() * Vec4f(model, 1.0);
  return Vec2f(tmp[0], tmp[1]);
}

Vec3f MatrixManager::screenToModel(Vec2f screen) {
  GlMat4f inv = getMatProjMod();
  inv.inverse();
  Vec4f tmp = inv * Vec4f(screen, 0.0, 1.0);
  return Vec3f(tmp);
}

Vec2f MatrixManager::windowToScreen(Vec2f window) const {
  return Vec2f((window[0] / winW * 2.0f) - 1.0f,
               -((window[1] / winH * 2.0f) - 1.0f));
}

Vec2f MatrixManager::windowToModel(const Vec2f window) {
  const Vec3f p = screenToModel(windowToScreen(window));
  return Vec2f(p.x(), p.y());
}


void MatrixManager::pushModelViewMatrix() {
  modelStack.push(mModelView);
}

void MatrixManager::pushProjectionMatrix() {
  projStack.push(mProjection);
}

void MatrixManager::popModelViewMatrix() {
  if (!modelStack.empty()) {
    mModelView = modelStack.top();
    modelStack.pop();
    shouldRecompute = true;
  }
}

void MatrixManager::popProjectionMatrix() {
  if (!projStack.empty()) {
    mProjection = projStack.top();
    projStack.pop();
    shouldRecompute = true;
  }
}

float MatrixManager::windowToModelFactor() const {
  const GlMat4f mProjection = getProjectionMatrix();
  const GlMat4f mModelView = getModelViewMatrix();
  const GlMat4f invMat = mProjection * mModelView;
  const Vec4f v1 = invMat * Vec4f(1.f, 0.f, 0.f, 1.f);
  const Vec4f v2 = invMat * Vec4f(0.f, 0.f, 0.f, 1.f);
  return (2.f / getWindowWidth()) / Vec3f(v1).dist(Vec3f(v2));
}

