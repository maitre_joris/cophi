/*
  Copyright 2015 Timothée Jourde <itim.lcf@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "renderer/Renderer.h"

using namespace std;

const Vec4f Renderer::BACKGROUND_COLOR = Vec4f(1.f, 1.f, 1.f, 1.f);

Renderer::Renderer(Display &display, DisplayParameters &dp)
        : display(display),
          params(&dp),
          axisHandler(&display),
          sliderHandler(&display) {

    params->attachObserver(this);
    display.init();
    bool highlighting = !coreHighlightingViews.empty();

    auto setOpacityFunc = [this](unsigned int left, unsigned int right, float alpha, bool edgesOnly) {
        for (unsigned int i = left; i <= right; ++i) {
            if (edgesOnly) {
                setEdgeViewsAlpha(right, coreViews, alpha);
                setEdgeViewsAlpha(right, coreHighlightingViews, alpha);
            } else {
                setAxisAlpha(i, coreViews, alpha);
                setAxisAlpha(i, coreHighlightingViews, alpha);
            }
        }
        //coreViews.sortEdges();
        //coreHighlightingViews.sortEdges();
    };

    auto shiftViewsFunc = [this, highlighting](unsigned int startingDim, float delta) {
        shiftViews(startingDim, delta, coreViews);
        shiftViews(startingDim, delta, coreHighlightingViews);
        //coreViews.sortEdges();
        //coreHighlightingViews.sortEdges();
    };

    auto invertFunc = [this, highlighting](AxisInterval axes, std::vector<float> rate) {
        for (unsigned int axisId = axes.getLeft(); axisId <= axes.getRight(); axisId++) {
            updateInversionRate(axisId, rate[axisId], coreViews);
            if (highlighting) updateInversionRate(axisId, rate[axisId], coreHighlightingViews);
        }
        coreViews.sortEdges();
        coreHighlightingViews.sortEdges();
    };


    auto rebuildViews = std::bind(&Renderer::onAxisChange, this);

    axisHandler.setCallBacks(setOpacityFunc, rebuildViews, shiftViewsFunc, invertFunc);
}

Renderer::~Renderer() = default;

bool Renderer::isOuterCoordinate(float modelX, float modelY,
                                 float margin) const {
    return (modelX > getTotalSpacing() + getTotalNodeWidth() + margin
            || modelX < -margin
            || modelY < -margin
            || modelY > params->getHeight() + margin);
}

int Renderer::getNearestAxisFromModelX(float modelX) const {
    auto sDim = (int) floor(
            (modelX + params->getAxisSpacing() / 2) / (params->getAxisWidth()));
    return min(sDim, (int) dataSource->getLastAxisId());
}

int Renderer::getLeftAxisFromModelX(float modelX) const {
    int sDim = (int) floor(
            (modelX - params->getMaxNodeWidth() / 2) / (params->getAxisWidth()));
    return min(sDim, (int) dataSource->getLastAxisId());
}

int Renderer::getRightAxisFromModelX(float modelX) const {
    int sDim = (int) ceil(
            (modelX - params->getMaxNodeWidth() / 2) / (params->getAxisWidth()));
    return max(sDim, 0);
}

AxisInterval Renderer::getSelectionBoxAxisInterval() {
    int leftAxis = getRightAxisFromModelX(currentSelection.getLeftX());
    int rightAxis = getLeftAxisFromModelX(currentSelection.getRightX());
    return AxisInterval(leftAxis, rightAxis); // Any invalidity is handled here
}

void Renderer::onDataLoaded(DataHandler *d, bool center) {
    dataSource = d;
    coreViews = ViewStorage(d);
    axisHandler.init(dataSource);
    onAxisChange();
    variationSelector.init(dataSource);
    if (center) display.fit(dataSource->getNbAxes());
}

Vec4f Renderer::propertyToColor(const NodeProp &np) const {
    float totalWeight = dataSource->getNbIndividuals();
    float totalInterval = dataSource->getAxisExtrema(np.dimension).size();
    float posValue;
    if (params->globalGradientMode()) {
        posValue = ((np.min) - globalExtrema.min) / (globalExtrema.max - globalExtrema.min);
    } else { //TODO color should be dependant on mode (interval vs weight)!!
        float weightMode = np.weightBefore + float(np.weight) / totalWeight / 2.f;
        float intervalMode = (np.min + (np.max - np.min) / 2.f) / totalInterval;
        weightMode = intervalMode;
        posValue = Animation::transition(weightMode, intervalMode, getWeightToInterval());
    }
    posValue = between(0.f, posValue, 1.f);
    return to_gl(params->getColorScale().getColorAtPos(posValue));
}

void Renderer::onAxisChange() {
    updateLabels();
    globalExtrema = dataSource->getGlobalExtrema();
    rebuildCurrentViews();
}

void Renderer::onHighlight(const Highlight *sub) {
    if (sub != nullptr) {
        coreHighlightingViews.clear();
        coreHighlightingViews = ViewStorage(sub);
        buildHighlightViews(sub, coreViews, coreHighlightingViews);
    } else {
        hideHighlight();
    }
}

void Renderer::buildHighlightViews(const ViewData *data, const ViewStorage &base, ViewStorage &target) {
    target.clear();
    for (const auto &view : base.getNodeViews()) {
        if (data->hasNode(view.first)) {
            NodeView nv = view.second.first;
            nv.subWeight = data->getNodeSubWeight(view.first);
            DistribView dv = DistribView(data->getNormalizedDistribution(view.first));
            target.insertNodeView(view.first, nv, dv);
        }
    }
    for (const auto &view : base.getContextViews()) {
        if (data->hasNode(view.first)) {
            const NodeProp &np = dataSource->getNodeProperty(view.first);
            NodeView nv = view.second.first;
            // Shell receive color instead of grey to enhance fullness
            nv.color = propertyToColor(np);
            nv.subWeight = data->getNodeSubWeight(view.first);
            target.insertNodeView(view.first, nv, DistribView());
        }
    }
    for (const auto &view : base.getEdgeViews()) {
        if (data->hasEdge(view.first)) {
            EdgeView ev = view.second;
            ev.subWeight = data->getEdgeSubWeight(view.first);
            target.insertEdgeView(view.first, ev);
        }
    }
    target.sortEdges();
}

void Renderer::draw(bool colorPicking) {
    display.drawInit(BACKGROUND_COLOR, colorPicking);
    if (dataSource == nullptr || dataSource->getNbAxes() == 0) {
        display.drawCleanUp();
        return;
    }
    if (!colorPicking) {
        if (sliderHandler.isActive()) display.drawSliders();
        if (params->displayAxes()) display.drawAxes(sliderHandler.isActive());
        if (!thumbnailViews.empty()) drawThumbnail();
    }
    const bool highlighting = !coreHighlightingViews.empty();
    const bool behind = highlighting && !colorPicking;
    const float weightToInterval = getWeightToInterval();
    const float distributionAlpha = focusActivity.isAnimating() ? focusActivity.getCurrentStep() : 1.f;

    display.drawNodes(coreViews, weightToInterval, distributionAlpha, 0.f, behind, colorPicking);
    if (params->displayEdges())
        display.drawEdges(coreViews, weightToInterval, -11.f, behind, colorPicking);

    if (!colorPicking) {
        if (highlighting) {
            display.drawNodes(coreHighlightingViews, weightToInterval, NODE_GAUGE_Z_OFFSET, 0.0f, false);
            if (params->displayEdges())
                display.drawEdges(coreHighlightingViews, weightToInterval, -1.f, false, false);
        }
        if (focusActivity.is(StagedAnimation::division) || focusActivity.is(StagedAnimation::merging)) {
            display.drawNodes(animationOverlay, weightToInterval, 1.f, DEFAULT_NODE_Z + FOCUS_ANIMATION_OFFSET);
        }

        // Fading could be better
        if (params->displayDistortion() &&
            !axisHandler.isAnimating(int(Animation::ALL)) &&
            !focusActivity.isAnimating()) {
            display.drawFrequencies(coreViews, weightToInterval, FREQUENCY_Z_OFFSET);
        }

        if (currentSelection.isShown()) drawSelectionBox();
        if (params->displayLabels()) display.drawText();
    }

    display.drawCleanUp();
}

void Renderer::drawThumbnail() {
    float baseZ = dataSource->getFrontZ() + THUMBNAIL_Z_OFFSET;
    float w2i = getWeightToInterval();
    bool highlighting = !coreHighlightingViews.empty();
    display.drawNodes(thumbnailViews,
                      w2i,
                      1.f,
                      baseZ,
                      highlighting, false,
                      true,
                      thumbnailAnchor);
    if (params->displayDistortion())
        display.drawFrequencies(thumbnailViews,
                                w2i, baseZ + FREQUENCY_Z_OFFSET,
                                true, thumbnailAnchor); //TODO behind?
    display.drawEdges(thumbnailViews,
                      w2i,
                      baseZ,
                      highlighting, false,
                      true, thumbnailAnchor);
    if (highlighting) {
        display.drawNodes(thumbnailHighlightingViews,
                          w2i,
                          1.f,
                          baseZ + .1f,
                          false, false,
                          true, thumbnailAnchor);
        display.drawEdges(thumbnailHighlightingViews,
                          w2i,
                          baseZ + .1f,
                          false, false,
                          true, thumbnailAnchor);
    }

    Vec2f pointA = Vec2f(
            thumbnailAnchor.x() +
            params->THUMBNAIL_SCALE_FACTOR * (params->getAxisWidth() - params->getMaxNodeWidth()) / 2.f,
            thumbnailAnchor.y());
    pointA -= Vec2f(params->getAxisSpacing() / 4.f, 1.f);
    float h = params->getHeight() * params->THUMBNAIL_SCALE_FACTOR + 2.f;
    float w = (thumbnailUnderlay.getRightX() - thumbnailUnderlay.getLeftX()) *
              params->THUMBNAIL_SCALE_FACTOR + params->getAxisSpacing() / 4.f;
    float z = dataSource->getFrontZ() + THUMBNAIL_UNDERLAY_Z_OFFSET;
    display.drawSelectionBox(pointA, pointA + Vec2f(w, h), z);
}

AxisInterval Renderer::passiveSelectionBox(float windowX, float windowY) {
    AxisInterval axis = getTightBoundaryAxisFrom(windowX, windowY,
                                                 params->getPadding());
    if (axis.isEmpty()) currentSelection.hide();
    else {
        currentSelection.adapt(axis,
                               params->getPadding(),
                               params->getAxisSpacing(),
                               params->getMaxNodeWidth());
        //display.setSelectionBox();
        currentSelection.show();
    }
    return axis;
}

AxisInterval Renderer::getTightBoundaryAxisFrom(float windowX,
                                                float windowY,
                                                float margin) {
    if (dataSource == nullptr)
        return AxisInterval();
    const Vec2f p = display.windowToModel(Vec2f(windowX, windowY));
    float modelX = p.x();
    if (isOuterCoordinate(modelX, p.y(), margin))
        return AxisInterval();
    float axisAreaWidth = params->getAxisWidth();
    auto axisId = (int) floor(
            (modelX + params->getPadding()) / axisAreaWidth);
    if (axisId >= 0 && axisId <= (int) dataSource->getLastAxisId()) { // Valid range
        float axisXPos = axisId * axisAreaWidth + params->getMaxNodeWidth() / 2.f;
        if (modelX <
            axisXPos + params->getMaxNodeWidth() / 2. + params->getPadding())
            return AxisInterval(axisId, axisId);
    }
    return AxisInterval();
}

void Renderer::hideSelectionBox() {
    //thumbnailUnderlay.hide(); // TODO Check no thumbnail
    currentSelection.hide();
}

float Renderer::getWeightToInterval() const {
    if (mappingChanges.isAnimating()) return mappingChanges.getCurrentStep();
    else {
        switch (params->getHeightMappingMode()) {
            case WEIGHT:
                return 0.f;
            case INTERVAL_SIZE:
                return 1.f;
        }
    }
    return 0.f;
}

void Renderer::updateSelectionBox(float windowX, float windowY) {
    const Vec2f p = display.windowToModel(Vec2f(windowX, windowY));
    currentSelection.setSecondPoint(p.x());
    currentSelection.show();
}

void Renderer::startSelectionBox(float windowX, float windowY) {
    currentSelection.hide();
    const Vec2f p = display.windowToModel(Vec2f(windowX, windowY));
    currentSelection.setFirstPoint(p.x());
    currentSelection.setSecondPoint(p.x());
}

AxisInterval Renderer::endSelectionBox() {
    if (!currentSelection.isShown()) return AxisInterval();
    try {
        // Tighten selection box boundary to axis
        AxisInterval axes = getSelectionBoxAxisInterval(); //TODO Exception handling
        currentSelection.adapt(axes,
                               params->getPadding(),
                               params->getAxisSpacing(),
                               params->getMaxNodeWidth());
        currentSelection.show();
        return axes;
    }
    catch (const AxisInterval::InvalidAxisInterval &e) {
        throw CanceledOperation(CanceledOperation::EMPTY_SELECTION);
    }
}


void Renderer::drawSelectionBox() {
    float arrowHeight = 1.f;
    GLfloat frontZ = dataSource->getFrontZ();
    // PointA: Upper-left, pointB: lower-right
    Vec2f pointA = Vec2f(currentSelection.getLeftX(),
                         params->getHeight() + arrowHeight +
                         params->getPadding());
    Vec2f pointB = Vec2f(currentSelection.getRightX(),
                         -params->getPadding());
    display.drawSelectionBox(pointA, pointB, frontZ + SELECTION_BOX_UNDERLAY_Z_OFFSET);
}

bool Renderer::tick(float time) {
    if (dataSource == nullptr) return true; //FALSE?
    if (mappingChanges.isAnimating()) {
        return tickMappingChange(time);
    } else if (axisHandler.isAnimating(Animation::INVERSION | Animation::INSERTION | Animation::DELETION)) {
        return axisHandler.tick(time);
    } else if (focusActivity.isAnimating()) {
        return tickFocus(time);
    }
    return true;
}

void Renderer::rebuildCurrentViews() {
    buildViews(coreViews);
    if (!coreHighlightingViews.empty())
        buildHighlightViews(coreHighlightingViews.getViewData(), coreViews, coreHighlightingViews);
}

bool Renderer::tickFocus(float time) {
    FocusActivity::Stage stage = focusActivity.getStage();
    bool finished = focusActivity.doStep(time);
    float step = focusActivity.getCurrentStep();
    auto aroundFocusAxis = [this](const EdgeView &ev) {
        return (ev.end1.x >= std::max(0, focusAxis - 1)) &&
               (ev.end2.x <= std::min(focusAxis + 1, (int) dataSource->getNbAxes()));
    };
    auto shellAroundFocusAxis = [this](const NodeView &nv, ViewStorage::Type type) {
        return nv.x == focusAxis && type == ViewStorage::Type::CONTEXT_SHELL;
    };
    switch (stage) {
        case FocusActivity::fadeOut : {
            if (!finished) {
                coreViews.setEdgeAlphaIf(step, aroundFocusAxis);
            } else {
                coreViews.setEdgeAlphaIf(0.f, aroundFocusAxis);
            }
            break;
        }
        case FocusActivity::division : {
            if (!finished) {
                focusAnimator->doDivisionStep(step, coreViews, animationOverlay);
            } else {

            }
            break;
        }
        case FocusActivity::merging : {
            if (finished) {
                buildViews(coreViews);
                // Prepare smooth fade in for edges and context shell nodes
                coreViews.setEdgeAlphaIf(0.f, aroundFocusAxis);
                coreViews.setNodesAlphaIf(0.f, shellAroundFocusAxis);
                animationOverlay.clear();
            } else {
                focusAnimator->doExpandStep(step, coreViews, animationOverlay);
            }
            break;
        }
        case FocusActivity::fadeIn : {
            if (finished) {
                coreViews.setEdgeAlphaIf(1.f, aroundFocusAxis);
                coreViews.setNodesAlphaIf(1.f, shellAroundFocusAxis);
                focusActivity.finish();
                focusNode = node();
                animationOverlay.clear();
                onAxisChange();
                delete focusAnimator;
                focusAnimator = nullptr;
                return true;
            } else {
                coreViews.setEdgeAlphaIf(step, aroundFocusAxis);
                coreViews.setNodesAlphaIf(step, shellAroundFocusAxis);
            }
            break;
        }
        default:
            throw InvalidAnimationStage();
    }
    return false;
}

bool Renderer::tickMappingChange(float time) {
    bool finishedStep = mappingChanges.doStep(time);
    buildViews(coreViews);
    if (highlighting)
        buildViews(coreHighlightingViews);
    if (finishedStep && sliderHandler.isActive()) {
        // TODOUpdate slider positions
        /*computeNodeYMap();
        for (unsigned int axisId = 0; axisId < dataSource->getNbAxes(); ++axisId) {
            // Get hook cluster for each axis
            const auto anchors = sliderHandler.getSliderAnchors(axisId);
            // Retrieve cluster anchors positions
            const node &nu = dataSource->getNodeFromAxis(axisId, anchors.getHigh());
            const node &nl = dataSource->getNodeFromAxis(axisId, anchors.getLow());
            float upper = getUpperLowerY(coreViews.getNodeView(nu)).getHigh();
            float lower = getUpperLowerY(coreViews.getNodeView(nl)).getLow();
            sliderHandler.setSliderPositions(axisId, lower, upper);
        }
        sliderHandler.updateBuffer();*/
    }

    return finishedStep;
}

Vec4f Renderer::getNodeDimensions(const NodeView &nv) const {
    float weightToInterval = getWeightToInterval();
    float height = Animation::transition(nv.weight, nv.interval, weightToInterval);
    float heightBefore = Animation::transition(nv.weightBefore, nv.intervalBefore, weightToInterval);
    float gap = Animation::transition(params->getGapRatio(), 0.f, weightToInterval);
    float gapBefore = Animation::transition(nv.gapBefore, 0.f, weightToInterval);
    Vec4f r = Vec4f(nv.x * params->getAxisWidth(),
                    heightBefore * (1.f - gap) * params->getHeight() +
                    gapBefore * gap * params->getHeight(),
                    params->getMaxNodeWidth(),
                    height * (1.f - gap) * params->getHeight());
    const float dy = params->getHeight() - 2.f * r.y() - r.w();
    r.setY(r.y() + nv.getInversionRate() * dy);
    return r;
}

EdgeView Renderer::createEdgeView(const ViewData *data,
                                  const edge &e,
                                  Vec3f color,
                                  ViewStorage &views) const {
    EdgeView ev;
    const auto &ep = dataSource->getEdgeProperty(e);
    unsigned long nbIndividuals = dataSource->getNbIndividuals();
    float weight = static_cast<float>(ep.weight) / nbIndividuals;
    ev.weight = weight;
    ev.z = ep.z;
    ev.idColor = color;
    ev.sourcePos = ep.sourcePos;
    ev.destPos = ep.destPos;
    ev.subWeight = data->getEdgeSubWeight(e); // Ratio with total weight

//    if (dataSource->getCurrentHighlight() != nullptr) {
//        ev.isHighlighted = dataSource->getCurrentHighlight()->hasEdge(e) ? 1.f : -1.f;
//    } else {
//        ev.isHighlighted = -1.f;
//    }
//
    const Ends ends = data->getEnds(e);
    ev.end1 = views.getNodeView(ends.source);
    ev.end2 = views.getNodeView(ends.dest);
    const NodeProp &np1 = dataSource->getNodeProperty(ends.source);
    const NodeProp &np2 = dataSource->getNodeProperty(ends.dest);
    float compression1 =
            ev.end1.weight / (static_cast<float>(np1.weight) / nbIndividuals);
    float compression2 = ev.end2.weight / (static_cast<float>(np2.weight) / nbIndividuals);
    ev.sourceWeight = weight * compression1;
    ev.destWeight = weight * compression2;
    return ev;
}

NodeView Renderer::createBaseNodeView(const NodeProp &np,
                                      const vector<float> &normalizedDistribution,
                                      float subWeight) const {
    NodeView nv;
    nv.x = static_cast<float>(np.dimension);
    nv.widthReduction = 1.f;
    nv.gapBefore = np.gapBefore;
    nv.distortion = np.distortion;
    nv.subWeight = subWeight;
    nv.setInversionRate(axisHandler.getInversionRate(np.dimension));
    nv.setBiggestBucket(normalizedDistribution);
    return nv;
}

NodeView Renderer::createNodeView(const node &n,
                                  const ViewData *data,
                                  NodeCompression compression) const {
    const auto &np = dataSource->getNodeProperty(n);
    // Common properties
    NodeView nv = createBaseNodeView(np,
                                     data->getNormalizedDistribution(n),
                                     data->getNodeSubWeight(n));

    // Type dependant properties
    const Extrema axisExtrema = data->getAxisExtrema(np.dimension);
    float axisInterval = axisExtrema.size();
    float weight = static_cast<float>(np.weight) / dataSource->getNbIndividuals();
    NodeCompression::WeightInterval nodeComp = np.isMeta() ? compression.context : compression.focus;
    nv.weight = max(NodeView::MIN_SIZE, weight * nodeComp.weight);
    nv.distortion /= nodeComp.weight;
    if (axisInterval == 0.f) {
        nv.intervalBefore = 0.5f;
        nv.weightBefore = 0.f;
        nv.gapBefore = 0.5f;
        nv.interval = NodeView::MIN_SIZE;
    } else {
        nv.interval = max(NodeView::MIN_SIZE, abs(np.max - np.min) / axisInterval * nodeComp.interval);
        // Optimisation: Some computing could be done once for each axis node views
        float contextComp = compression.context.interval / axisInterval;
        float focusComp = compression.focus.interval / axisInterval;
        const Extrema focusExtrema = dataSource->getFocusExtrema(np.dimension);
        float lowerContextSize = abs(focusExtrema.min - axisExtrema.min) * contextComp;
        if (np.isMeta()) {
            nv.setAsContext();
            if (np.isHigherContext()) {
                float focusInterval = abs(focusExtrema.max - focusExtrema.min);
                nv.intervalBefore = lowerContextSize + focusInterval * focusComp;
            } else {
                nv.intervalBefore = 0.f;
            }
        } else {
            float dist = abs(np.min - focusExtrema.min);
            nv.intervalBefore = lowerContextSize + dist * focusComp;
        }
    }
    return nv;
}


void Renderer::buildViews(ViewStorage &views,
                          bool colorPicking) {
    const ViewData *data = views.getViewData();
    buildNodeViews(views, data, colorPicking);
    buildEdgeViews(views, data);
}


void Renderer::triggerInversion(AxisInterval axes, float time) {
    if (dataSource == nullptr || axes.isEmpty()) return;
    axisHandler.launchInversion(axes, time);
}


void Renderer::updateAxisThumbnailPosition(float windowX, float windowY) {
    const Vec2f p = display.windowToModel(Vec2f(windowX, windowY));
    // Sets thumbnail on the bottom-left or cursor
    thumbnailAnchor = Vec2f(p.x(),
                            p.y() -
                            params->THUMBNAIL_SCALE_FACTOR * params->getHeight());
}

void Renderer::hideThumbnail() {
    thumbnailViews.clear();
    thumbnailHighlightingViews.clear();
    thumbnailUnderlay.hide();
    thumbnailAnchor = Vec2f(0.f, 0.f);
}

void Renderer::onFocus() {
    hideEnhancedElements();
    enhancedElements.clear();
    buildViews(coreViews, false);
}

void Renderer::hideEnhancedElements() {
    if (!enhancedElements.empty()) {
        for (auto &e : enhancedElements) {
            if (e.n.isValid()) {
                if (e.hasSubSelection()) {
                    coreViews.getDetailView(e.n, e.subNo).setEnhancement(false);
                } else {
                    coreViews.getNodeView(e.n).setEnhancement(false);
                }
            } else if (e.e.isValid()) {
                coreViews.getEdgeView(e.e).setEnhancement(false);
            }
        }
        enhancedElements.clear();
    }
}

// Should be in DataHandler
std::vector<edge> Renderer::getEdges(unsigned int axis, float min, float max) {
    return variationSelector.filter(dataSource->toDimension(axis), min, max);
}


void Renderer::enhanceEdges(unsigned int axis, float min, float max) {
    if (dataSource == nullptr) return;
    hideEnhancedElements();
    std::vector<edge> selected = getEdges(axis, min, max);
    for (const edge &e : selected) {
        SelectedElement ed(e);
        enhancedElements.push_back(ed);
        coreViews.getEdgeView(ed.e).setEnhancement(true);
    }
}

void Renderer::enhanceElement(float x, float y, bool focusableOnly) {
    if (dataSource == nullptr) return;
    const SelectedElement e = getElement(x, y);
    if (enhancedElements.size() == 1 && e == enhancedElements.at(0)) return;
    hideEnhancedElements();
    if (!e.isValid()) return;
    if (e.n.isValid()) {
        const NodeProp &np = dataSource->getNodeProperty(e.n);
        if (np.isMeta()) {
            if (!focusableOnly) {
#ifdef LOCAL
                // Enhance full context node
                enhancedElements.push_back(e);
                coreViews.getNodeView(e.n).setEnhancement(true);
#endif
            } else { // Enhance sub context nodes
                try {
                    pair<unsigned int, unsigned int> indexLevel = pickSubContextView(x, y, e.n);
                    enhancedElements.push_back(SelectedElement(e.n, indexLevel.first));
                    coreViews.getDetailView(e.n, indexLevel.first).setEnhancement(true);
                }
                catch (const DataException &) {
                    hideEnhancedElements();
                    return;
                }
            }
        } else {
            if (!focusableOnly || np.focusable()) {
                enhancedElements.push_back(e);
                coreViews.getNodeView(e.n).setEnhancement(true);
            }
        }
    } else if (e.e.isValid()) {
        auto ends = dataSource->getEnds(e.e);
#ifndef LOCAL
        bool focusable =
                !dataSource->getNodeProperty(ends.source).isMeta() && !dataSource->getNodeProperty(ends.dest).isMeta();
#else
        bool focusable = true;
#endif
        ;if (!focusableOnly || focusable) {
            enhancedElements.push_back(e);
            coreViews.getEdgeView(e.e).setEnhancement(true);
        }
    }
}

void Renderer::updateLabels() {
    if (dataSource == nullptr) return;
    if (!params->displayLabels()) return;
    display.clearText();

    for (unsigned axisId = 0; axisId < dataSource->getNbAxes(); axisId++) {
        const string &label = dataSource->getAxisLabel(axisId);
        float x = float(axisId) * params->getAxisWidth() +
                  params->getMaxNodeWidth() / 2.f;
        display.addText(label, Vec2f(x, params->LABEL_MARGIN));
        //TODO margin proportional to height?
        /*float s = (params->getAxisWidth() / 2) / cos(radian) / 2.f;*/
    }
}


pair<int, int> Renderer::getSurroundingAxes(float windowX, float windowY) {
    if (dataSource == nullptr) return make_pair(NONE_ID, NONE_ID);
    const Vec2f p = display.windowToModel(Vec2f(windowX, windowY));
    float x = p.x();
    float y = p.y();
    if (y < -params->getHeight() / 2 || y > params->getHeight() * 1.5f)
        return make_pair(NONE_ID, NONE_ID);
    int prev = NONE_ID;
    int sDim = (int) floor(x / params->getAxisWidth());
    if (sDim >= 0)
        prev = min(sDim, (int) dataSource->getLastAxisId());
    return make_pair(prev, prev + 1);
}

unsigned int Renderer::getNbClosing(float x, float y, const node &n) {
    try {
        return pickSubContextView(x, y, n).second;
    } catch (const VoidPick &e) {
        return 0;
    }
}

float Renderer::getVariation(edge e) const {
    return variationSelector.getVariation(e);
}

pair<unsigned int, unsigned int> Renderer::pickSubContextView(float x, float y, const node &n) {
    const float cursorY = display.windowToModel(Vec2f(x, y)).y() / params->getHeight();
    const NodeProp &np = dataSource->getNodeProperty(n);
    const ContextStack &stack = dataSource->getContextNodeHierarchy(np.dimension, np.clusterId);
    const auto shellUpperLower = getUpperLowerY(coreViews.getNodeView(n));
    if (cursorY >= shellUpperLower.getLow() && cursorY <= shellUpperLower.getHigh()) {
        unsigned int level = 0, index = 0;
        // SubView exists only for non empty sub context
        const deque<NodeView> &subContext = coreViews.getDetailViews(n);
        for (const ClusterProp &cur : stack) {
            if (cur.weight > 0) {
                const auto upperLower = getUpperLowerY(subContext.at(index));
                if (upperLower.getLow() <= cursorY && upperLower.getHigh() >= cursorY) {
                    return make_pair(index, (unsigned int) (stack.size() - level));
                }
                index++;
            }
            level++;
        }
    }
    throw VoidPick();
}

node Renderer::getNode(float windowX, float windowY) {
    if (dataSource == nullptr) return node();
    int id = pickId((int) windowX, (int) windowY);
    if (id < 0) return node();
    return dataSource->getNodeById((unsigned int) id);
}

edge Renderer::getEdge(float windowX, float windowY) {
    if (dataSource == nullptr) return edge();
    int id = pickId((int) windowX, (int) windowY);
    if (id < 0) return edge();
    return dataSource->getEdgeById((unsigned int) id);
}

int Renderer::pickId(int x, int y) {
    applyColorPickingColoring(coreViews);
    draw(true);
    int id = display.pickingRoutine(x, y, [this](int i) { return dataSource->isElementById(i); });
    applyGradientColoring(coreViews);
    draw(false);
    return id;
}

SelectedElement Renderer::getElement(float windowX,
                                     float windowY) {
    if (dataSource == nullptr) return SelectedElement();
    const Vec2f p = display.windowToModel(Vec2f(windowX, windowY));
    if (isOuterCoordinate(p.x(), p.y(), 0.5f))
        return SelectedElement();
    int maxNodeId = dataSource->getMaxNodeId();
    int id = pickId((int) windowX, (int) windowY);
    if (id == NONE_ID || id < 0) return SelectedElement();
    if (id <= maxNodeId) {
        node n = dataSource->getNodeById((unsigned int) id);
        if (n.isValid()) {
            return SelectedElement(n);
        }
    } else {
        edge e = dataSource->getEdgeById((unsigned int) id);
        if (e.isValid()) return SelectedElement(e);
    }
    return SelectedElement();
}

void Renderer::hideHighlight() {
    dataSource->clearHighlight();
    coreHighlightingViews.clear();
}


bool Renderer::isHighlighting() const {
    return !coreHighlightingViews.empty();
}


void Renderer::setHeight(const float x) {
    params->setHeight(max(0.f, x));
    updateLabels();
}

void Renderer::setTotalSpacing(const float x) {
    if (dataSource == nullptr) return;
    float spacing = max(0.f, x / float(dataSource->getNbAxes() - 1));
    params->setSpacing(spacing);
}

void Renderer::setTotalNodeWidth(const float x) {
    if (dataSource == nullptr) return;
    float nodeWidth = max(0.f, x / float(dataSource->getNbAxes()));
    params->setNodeWidth(nodeWidth);
}

void Renderer::setGap(const float x) {
    params->setGapRatio(between(0.f, x, 1.f));
}

void Renderer::setAxesToDelete(AxisInterval axes) {
    if (dataSource == nullptr) return;
    hideSelectionBox();
    axisHandler.prepareDeletion(axes, dataSource->getLastAxisId());
}

void Renderer::setDimensionsToInsert(DimensionInterval dbDims,
                                     int futurePrevAxis) {
    if (dataSource == nullptr || !axisHandler.noActivityPending()) return;
    axisHandler.prepareInsertion(dbDims, futurePrevAxis, dataSource->getLastAxisId());
}

void Renderer::triggerDeletion(float time, bool swap) {
    if (axisHandler.noActivityPending()) return;
    if (swap) axisHandler.setSwap(swap);
    axisHandler.launchDeletion(time);
}

void Renderer::triggerInsertion(float time) {
    if (axisHandler.noActivityPending()) return;
    params->setDisplayLabels(false);
    params->setDisplayAxes(false);
    axisHandler.launchInsertion(time);
}

void Renderer::setFocusOpeningNode(unsigned int axisId, cluster id) {
    if (dataSource == nullptr) return;
    focusNode = id;
    focusAxis = axisId;
    focusClosings = 0;
    dataSource->prepareOpening(axisId, id);
}

void Renderer::setFocusClosingAxis(unsigned int axisId, unsigned int nbClosings) {
    if (dataSource == nullptr) return;
    focusAxis = axisId;
    focusClosings = nbClosings;
    focusNode = ClusterProp::INVALID_ID;
    dataSource->prepareClosing(axisId, nbClosings);
}


void Renderer::triggerFocus(float time) {
    if (dataSource == nullptr || focusAnimator != nullptr) return;
    focusAnimator = new FocusAnimator(dataSource, params);
    coreHighlightingViews.clear();
    ViewStorage initialViews = coreViews;
    if (focusClosings == 0) {
        const node &n = dataSource->getNodeFromAxis(focusAxis, focusNode);
        const NodeProp np = dataSource->getNodeProperty(n);
        focusAnimator->initFocusExpand(focusNode, focusAxis, initialViews);
        dataSource->applyOpening(n);
    } else {
        cluster lastOpened = dataSource->getAncestor(focusAxis, focusClosings - 1);
        focusAnimator->initFocusCompress(lastOpened, focusAxis, focusClosings, initialViews);
        dataSource->applyClosing(focusAxis, focusClosings);

    }
    buildViews(coreViews);
    focusAnimator->setFinalState(coreViews);
    coreViews = initialViews;
    focusActivity.animate(time);
}

void Renderer::triggerAxisThumbnail(AxisInterval axes) {
    if (axes.isEmpty()) return;
    // Copying views instead of creating a shallow DataHandler object
    const auto elements = dataSource->getElements(axes);
    thumbnailViews.clear();
    thumbnailHighlightingViews.clear();
    bool highlighting = !coreHighlightingViews.empty();

    auto transferNode = [](const node &n,
                           const NodeProp &np,
                           float deltaX,
                           ViewStorage &sourceViews,
                           ViewStorage &destViews) -> void {
        NodeView nv = sourceViews.getNodeView(n);
        nv.x -= deltaX;
        if (np.isMeta()) {
            deque<NodeView> dv = sourceViews.getDetailViews(n);
            for (auto &v : dv) v.x -= deltaX;
            destViews.insertContextNodeView(n, nv, dv);
        } else {
            DistribView dv = sourceViews.getDistribView(n);
            destViews.insertNodeView(n, nv, dv);
        }
    };

    for (const node &n : elements.second) {
        const auto &np = dataSource->getNodeProperty(n);
        transferNode(n, np, axes.getLeft(), coreViews, thumbnailViews);
        if (highlighting && coreHighlightingViews.hasNode(n)) {
            transferNode(n, np, axes.getLeft(), coreHighlightingViews, thumbnailHighlightingViews);
        }
    }

    auto transferEdge = [](const edge &e,
                           float deltaX,
                           ViewStorage &sourceViews,
                           ViewStorage &destViews) -> void {
        EdgeView ev = sourceViews.getEdgeView(e);
        ev.end1.x -= deltaX;
        ev.end2.x -= deltaX;
        destViews.insertEdgeView(e, ev);
    };

    bool hasEdges = !elements.first.empty();

    if (hasEdges) {
        for (const edge &e : elements.first) {
            transferEdge(e, axes.getLeft(), coreViews, thumbnailViews);
            if (highlighting && coreHighlightingViews.hasEdge(e)) {
                transferEdge(e, axes.getLeft(), coreHighlightingViews, thumbnailHighlightingViews);
            }
        }
        thumbnailViews.sortEdges();
        if (highlighting) thumbnailHighlightingViews.sortEdges();
    }

    // Preparing thumbnail underlay
    thumbnailUnderlay.adapt(axes,
                            params->getPadding(),
                            params->getAxisSpacing(),
                            params->getMaxNodeWidth());
}


Vec3f Renderer::idToColor(const unsigned id) {
    const unsigned b = id % 256;
    const unsigned g = (id / 256) % 256;
    const unsigned r = (id / (256 * 256)) % 256;
    return Vec3f(r / 255.f, g / 255.f, b / 255.f);
}

Renderer::NodeCompression Renderer::getNodeCompression(unsigned int axisId) {
    NodeCompression result;

    float cWR = dataSource->getContextWeightRatio(axisId);
    float cIR = dataSource->getContextIntervalRatio(axisId).sum();
    float c = params->getContextCompression();
    if (cWR == 0.f) return result;
    switch (params->getContextMode()) {
        case ContextCompression::CONSTANT_SCREEN_SPACE: {
            result.context.weight = c / cWR;
            result.focus.weight = (1.f - c) / (1.f - cWR);
            result.context.interval = c / cIR;
            result.focus.interval = (1.f - c) / (1.f - cIR);
            return result;
        }
        case ContextCompression::CONSTANT_COMPRESSION: {
            result.context.weight = c;
            result.context.interval = c;
            result.focus.weight = (1.f - c * cWR) / (1.f - cWR);
            result.focus.interval = (1.f - c * cIR) / (1.f - cIR);
            return result;
        }
    }
    return result;
}

void Renderer::buildNodeViews(NodeViewStorage &views,
                              const ViewData *data,
                              bool colorPicking) {
    views.initNodeViews(data->getNbNodes());
    views.largestFocusWeights.resize(dataSource->getNbAxes());
    for (unsigned int i = 0; i < dataSource->getNbAxes(); i++) {
        NodeCompression compression = getNodeCompression(i);
        NodeCompression noCompression;
        Dimension nodes;
        float axisInterval = data->getAxisExtrema(i).size();
        float largestFocusWeight = 0.f;
        if (axisInterval == 0.f) { // One focus node
            data->forEachNode(i, [&](const node &n) {
                const NodeProp &np = dataSource->getNodeProperty(n);
                nodes.insert({np.clusterId, n});
                NodeView unit = createNodeView(n, data, noCompression);
                largestFocusWeight = 1.f;
                DistribView dv = DistribView(data->getNormalizedDistribution(n));
                views.insertNodeView(n, unit, dv);
            });
        } else {
            data->forEachNode(i, [&](const node &n) {
                const NodeProp &np = dataSource->getNodeProperty(n);
                nodes.insert({np.clusterId, n});
                if (np.isMeta()) { // One nodeView per level
                    NodeView shell = createNodeView(n, data, compression);
                    deque<NodeView> nvs = createSubViews(np, compression.context);
                    views.insertContextNodeView(n, shell, nvs);
                } else {
                    NodeView nv = createNodeView(n, data, compression);
                    largestFocusWeight = std::max(largestFocusWeight, nv.weight);
                    DistribView dv = DistribView(data->getNormalizedDistribution(n));
                    views.insertNodeView(n, nv, dv);
                }
            });
            views.largestFocusWeights[i] = largestFocusWeight;
#ifdef HIERARCHICAL
            views.rescaleWeightInterval(nodes, false);
            views.repositionNodes(nodes, false);
#else
            views.rescaleWeightInterval(nodes, true);
            views.repositionNodes(nodes, true);
#endif

            data->forEachNode(i, [&](const node &n) {
                const NodeProp &np = dataSource->getNodeProperty(n);
                nodes.insert({np.clusterId, n});
                if (!np.isMeta()) {
                    views.getNodeView(n).weight /= Animation::transition(1.f, views.largestFocusWeights[np.dimension],
                                                                         getWeightToInterval());
                }
            });
        }
    }
    if (colorPicking) applyColorPickingColoring(views);
    else applyGradientColoring(views);
}

deque<NodeView> Renderer::createSubViews(const NodeProp &np,
                                         NodeCompression::WeightInterval compression) const {
    const ContextStack &stack = dataSource->getContextNodeHierarchy(np.dimension, np.clusterId);
    unsigned long totWeight = dataSource->getNbIndividuals();
    deque<NodeView> result;
    const float minWidth = params->getMinNodeWidth();
    const float maxWidth = NodeView::CONTEXT_REDUCTION_WIDTH - 0.01f;
    float step = (maxWidth - minWidth) / (stack.size() - 1);
    float nodeWidth = minWidth;
    bool higherNode = np.clusterId == ClusterProp::HIGHER_META_NODE_ID;
    float intervalBefore = (higherNode ? 1.f : 0.f);
    float axisIntervalSize = dataSource->getAxisExtrema(np.dimension).size();
    for (const ClusterProp &cur : stack) { // From oldest to newest
        if (cur.weight > 0) {
            float weight = max(NodeView::MIN_SIZE, static_cast<float>(cur.weight) / totWeight * compression.weight);
            NodeView nv;
            nv.x = static_cast<float>(np.dimension);
            nv.gapBefore = np.gapBefore; //?
            nv.setInversionRate(axisHandler.getInversionRate(np.dimension));
            nv.color = propertyToColor(np);
            nv.widthReduction = nodeWidth;
            nv.weight = weight;
            float intervalSize = abs(cur.max - cur.min) / axisIntervalSize;
            nv.interval = max(NodeView::MIN_SIZE, intervalSize * compression.interval);
            if (higherNode) intervalBefore -= nv.interval;
            nv.intervalBefore = intervalBefore;
            if (!higherNode) intervalBefore += nv.interval;
            assert(nv.weight <= 1.f && nv.weight >= 0.f);
            assert(isfinite(nv.interval));
            result.push_back(nv);
        }
        nodeWidth += step;

    }
    return result;
}

void Renderer::buildEdgeViews(ViewStorage &views,
                              const ViewData *data) {
    views.initEdgeViews(data->getNbEdges());
    data->forEachEdge([this, data, &views](const edge &e) {
        EdgeView ev = createEdgeView(data, e, idToColor(dataSource->getEdgeClientId(e)), views);
        views.insertEdgeView(e, ev);
    });
    views.sortEdges();
}

void Renderer::animateMappingChange(float time) {
    mappingChanges.animate(time, params->getHeightMappingMode());
}

void Renderer::shiftViews(unsigned int startingDim,
                          float delta,
                          ViewStorage &views) {
    if (views.empty()) return;
    for (unsigned int i = startingDim; i < dataSource->getNbAxes(); ++i)
        views.getViewData()->forEachNode(i, [&views, delta](const node &n) {
            views.getNodeView(n).x += delta;
            try {
                for (NodeView &dv : views.getDetailViews(n)) {
                    dv.x += delta;
                }
            } catch (ViewStorage::ViewNotFound) {

            }
        });
    for (unsigned int i = startingDim; i < dataSource->getLastAxisId(); ++i)
        views.getViewData()->forEachEdge(i, [&views, delta](const edge &e) {
            EdgeView &ev = views.getEdgeView(e);
            ev.end1.x += delta;
            ev.end2.x += delta;
        });
}


bool Renderer::selectionBoxContains(float windowX, float windowY) {
    if (!currentSelection.isShown()) return false;
    const Vec2f p = display.windowToModel(Vec2f(windowX, windowY));
    return currentSelection.contains(p.x());
}

void Renderer::showSliders() {
    hideHighlight();
    // Slider map could be only computed when required
    unsigned int nbAxes = dataSource->getNbAxes();
    computeNodeYMap();
    sliderHandler.init(nbAxes);
    for (unsigned int axisId = 0; axisId < nbAxes; ++axisId) {
        AxisFiltrationTable::Hook upper = clusterPositions.getUpperMost(axisId, SliderHandler::INIT_POS.first);
        AxisFiltrationTable::Hook lower = clusterPositions.getLowest(axisId, SliderHandler::INIT_POS.second);
        sliderHandler.setInitialPosition(axisId, upper.first, lower.first);
    }
    sliderHandler.updateBuffer();
}

void Renderer::hideSliders() {
    sliderHandler.clear();
}

float Renderer::getTotalSpacing() const {
    return dataSource == nullptr ? 0 : params->getAxisSpacing() *
                                       (dataSource->getNbAxes() - 1);
}

float Renderer::getTotalNodeWidth() const {
    return (dataSource == nullptr) ? 0 : params->getMaxNodeWidth() *
                                         dataSource->getNbAxes();
}

//bool Renderer::isAnimatingInsertion() const { return insertions.isAnimating(); }

bool Renderer::isAnimating(Animation::Type t) const {
    if (t == Animation::FOCUS)
        return focusActivity.isAnimating();
    return axisHandler.isAnimating(t);
}

void Renderer::setColorGradient(unsigned int index) {
    params->setColorScale(index);
    applyGradientColoring(coreViews);
    if (isHighlighting()) applyGradientColoring(coreHighlightingViews);
}

bool Renderer::sliderClick(float windowX, float windowY) {
    // Figure out the underlying axis
    AxisInterval axis = getTightBoundaryAxisFrom(windowX, windowY, 10.f);
    Vec2f modelCoordinates = display.windowToModel(
            Vec2f(windowX, windowY));
    // Now figure out the height portion and compare with sliders
    return !axis.isEmpty() && sliderHandler.pickSlider(axis.getSingleAxis(),
                                                       modelCoordinates.y(),
                                                       params->getHeight());
}

void Renderer::onSliderMove(float windowX, float windowY) {
    if (!sliderHandler.isSliderPicked()) return;
    Vec2f modelCoordinates = display.windowToModel(Vec2f(windowX, windowY));
    float newPosition = modelCoordinates.y() / params->getHeight();
    const MovingSlider &s = sliderHandler.getMovingSlider();
    unsigned int axisId = s.getAxisId();
    pair<cluster, float> newAnchor;
    cluster pastAnchor = s.getAnchor();
    try {
        if (s.isUpper()) newAnchor = clusterPositions.getUpperMost(axisId, newPosition);
        else newAnchor = clusterPositions.getLowest(axisId, newPosition);
        // First check: Detectable move
        if (pastAnchor != newAnchor.first) {
            const auto anchors = sliderHandler.getSliderAnchors(axisId);
            cluster upper = s.isUpper() ? newAnchor.first : anchors.first;
            cluster lower = s.isUpper() ? anchors.second : newAnchor.first;
            // This should be redone faster
            const auto &selected = clusterPositions.getInBetweenClusters(axisId,
                                                                         upper,
                                                                         lower);
            // Second check: valid move
            if (!selected.empty()) {
                sliderHandler.hookMovingSlider(newAnchor.first, newAnchor.second);
#ifdef LOCAL
                //applySliderFiltering();
#endif
                sliderHandler.updateBuffer();
                display.drawSliders();
            }
        }
    } catch (ValueNotFound &e) {
        // Invalid slider move. Possible feedback: red slider, ERROR SOUND!!
    }
}

void Renderer::applySliderFiltering() {
    unsigned int nbAxes = dataSource->getNbAxes();
    // Assuming clusters are ordered and sequential, only framing clusters are needed
    vector<ClusterInterval> result;
    for (unsigned int axisId = 0; axisId < nbAxes; axisId++) {
        if (sliderHandler.isEditedSliderPair(axisId)) {
            unsigned int dbDim = dataSource->toDimension(axisId);
            const auto &anchors = sliderHandler.getSliderAnchors(axisId);
            // ClusterInterval format: (dbDim, minCluster, maxCluster)
            if (anchors.first > anchors.second) result.emplace_back(dbDim, anchors.second, anchors.first);
            else result.emplace_back(dbDim, anchors.first, anchors.second);
        }
    }
    if (result.empty()) return;
    dataSource->triggerHighlight(result);
}

void Renderer::onSliderRelease() {
    sliderHandler.release();
    applySliderFiltering();
}

void Renderer::updateInversionRate(unsigned int axisId,
                                   float inversionRate,
                                   ViewStorage &views) {
    const ViewData *data = views.getViewData();
    data->forEachNode(axisId,
                      [this, &views, data, inversionRate, axisId](const node &n) {
                          const NodeProp &np = dataSource->getNodeProperty(n);
                          if (np.isMeta())
                              // Edit sub context views as well
                              for (NodeView &nv : views.getDetailViews(n))
                                  nv.setInversionRate(inversionRate);
                          views.getNodeView(n).setInversionRate(inversionRate);
                          views.getNodeView(n).color = propertyToColor(np);
                          for (const edge &e : data->getStar(n)) {
                              EdgeView &ev = views.getEdgeView(e);
                              auto endAxis = (unsigned int) floor(ev.end1.x);
                              if (endAxis == axisId) ev.end1.setInversionRate(inversionRate);
                              else ev.end2.setInversionRate(inversionRate);
                          }
                      });
}

void Renderer::setAxisAlpha(unsigned int axisId,
                            ViewStorage &views,
                            float alpha) {
    const ViewData *data = views.getViewData();
    if (data == nullptr) return;
    if (views.empty()) return;
    data->forEachNode(axisId, [&views, data, &alpha](const node &n) {
        if (views.hasNode(n)) {
            views.getNodeView(n).setAlpha(alpha);
            try {
                for (NodeView &dv : views.getDetailViews(n))
                    dv.setAlpha(alpha);
            } catch (ViewStorage::ViewNotFound) {

            }
            for (const edge &e : data->getStar(n)) {
                views.getEdgeView(e).setAlpha(alpha);
            }
        }
    });
}

void Renderer::setEdgeViewsAlpha(unsigned int leftAxisId,
                                 ViewStorage &views,
                                 float alpha) {
    if (views.empty()) return;
    const ViewData *data = views.getViewData();
    data->forEachEdge(leftAxisId, [this, data, &views, alpha](const edge &e) {
        try {
            views.getEdgeView(e).setAlpha(alpha);
        }
        catch (ViewStorage::ViewNotFound) {
        }
    });
}

LowHighPair<float> Renderer::getUpperLowerY(const NodeView &nv) const {
    const Vec4f rect = nv.bounds(params, getWeightToInterval()); // Takes into acount inversion
    return {rect.y() / params->getHeight(), rect.w() / params->getHeight()};
}

DisplayParameters &Renderer::getParameters() {
    return *params;
}

void Renderer::applyColorPickingColoring(NodeViewStorage &views) {
    // EdgeView keep from birth color picking colors as their regular display
    // color is computed in shader from their weight
    views.getViewData()->forEachNode([this, &views](const node &n) {
        Vec3f color = idToColor(n.id);
        views.getNodeView(n).color = Vec4f(color.x(), color.y(), color.z(), 1.f);
    });
}

void Renderer::applyGradientColoring(NodeViewStorage &views) {
    if (views.getViewData() == nullptr) return;
    views.getViewData()->forEachNode([this, &views](const node &n) {
        const NodeProp &np = dataSource->getNodeProperty(n);
        if (np.isMeta()) {
            views.getNodeView(n).color = NodeView::CONTEXT_COLOR;
            for (NodeView &dv : views.getDetailViews(n))
                dv.color = propertyToColor(np);
        } else {
            views.getNodeView(n).color = propertyToColor(np);
        }
    });
}

void Renderer::computeNodeYMap() {
    unsigned long nbAxes = dataSource->getNbAxes();
    clusterPositions.init(nbAxes);
    for (unsigned int axisId = 0; axisId < nbAxes; axisId++) {
        const Dimension &dimNodes = dataSource->getDimension(axisId);
        for (const auto &pair : dimNodes) {
            const cluster clusterId = get<0>(pair);
            const NodeView &nv = coreViews.getNodeView(get<1>(pair));
            clusterPositions.insertCluster(clusterId, axisId, getUpperLowerY(nv)); // Takes into acount inversion
        }
    }
}

bool Renderer::isInnerArea(float windowX, float windowY) {
    const Vec2f p = display.windowToModel(Vec2f(windowX, windowY));
    return !isOuterCoordinate(p.x(), p.y(), 5. * params->getPadding());
}


void Renderer::setEdgeCurvature(float c) {
    params->setEdgeCurvature(between(0.01f, c, 1.f));
}

void Renderer::setEdgeReduction(float r) {
    params->setEdgeReduction(between(0.01f, r, 1.f));
}

void Renderer::setContextCompression(float c) {
    params->setContextCompression(between(0.01f, c, 1.f));
    buildViews(coreViews);
    //TODO highlight?
}

void Renderer::onScaleChange() {
    updateLabels();
}

void Renderer::onViewsNeedUpdate() {
    if (dataSource == nullptr) return;
    if (!coreViews.empty()) buildViews(coreViews);
    if (!coreHighlightingViews.empty()) buildViews(coreHighlightingViews);
    if (!thumbnailViews.empty()) buildViews(thumbnailViews);
}

void Renderer::onColorChange() {
    if (dataSource == nullptr) return;
    if (!coreViews.empty()) applyGradientColoring(coreViews);
    if (!coreHighlightingViews.empty()) applyGradientColoring(coreHighlightingViews);
    if (!thumbnailViews.empty()) applyGradientColoring(thumbnailViews);
}
