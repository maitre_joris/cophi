/*
  Copyright 2015 Timothée Jourde <itim.lcf@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//#define GLFW_INCLUDE_ES2 // Specifically use only OpenGL ES 2.0
#include <GLFW/glfw3.h>
#include <Visualization.h>

#ifdef EMSCRIPTEN
#include <emscripten.h>
#include <stdio.h>
#include <string.h>
#include <emscripten/html5.h>
#endif //EMSCRIPTEN
#ifndef LOCAL

#include <server/RestServer.h>

#endif


using namespace std;
Visualization *vis = nullptr;
#ifdef LOCAL
TableHandler::Configuration config;
#endif
GLFWwindow *window = nullptr;
static std::string NB_CLUSTERS_OPTION = "-k";

void printUsage() {
    std::cerr << "Usage:\t<width> <height> ";
#ifdef LOCAL
#ifndef HIERARCHICAL
    std::cerr << "<filePath> <-c|--clustering kmeans|canopy|binning|adaptive-binning> ";
#endif
    std::cerr << "<" << NB_CLUSTERS_OPTION
              << " nbClusters> [-h|--with-column-labels] [-i|--with-row-labels] [-n|--nominal|-o|--ordinal] list of column numbers";
#else
    std::cerr << "<dataSetName> <serverServiceAddress>";
#endif
    std::cerr << "\nWhere bracket arguments are mandatory." << std::endl;
}

bool findValidClusterArgument(int nbArgs, char *args[]) {
    for (int i = 1; i < nbArgs; ++i) {
        if (string(args[i]) == NB_CLUSTERS_OPTION) {
            try {
                stoi(string(args[i + 1]));
                return true;
            } catch (std::invalid_argument &) {
                return false;
            }
        }
    }
    return false;
}

#ifdef LOCAL

TableHandler::Configuration parseConfiguration(int nbArgs, char **args) {
    TableHandler::Configuration c;
    c.data = args[0];
    c.isFilePath = true;

    for (int i = 1; i < nbArgs; ++i) {
        if (string(args[i]) == NB_CLUSTERS_OPTION) {
            c.nbClusters = (unsigned) std::stoi(args[i + 1]);
            Notifier::info("K = " + std::to_string(c.nbClusters));
            i++;
        }
#ifndef HIERARCHICAL
        else if (string(args[i]) == "--clustering" || string(args[i]) == "-c") {
            if (i + 1 >= nbArgs)
                throw std::logic_error(string("Missing clustering name"));
            string clusteringName = string(args[i + 1]);
            if (clusteringName == "kmeans" || clusteringName == "k-means")
                c.clustering = IClustering::KMEANS;
            else if (clusteringName == "binning")
                c.clustering = IClustering::BINNING;
            else if (clusteringName == "adaptive-binning")
                c.clustering = IClustering::ADAPTIVE_BINNING;
            else if (clusteringName == "canopy")
                c.clustering = IClustering::CANOPY;
            else throw std::logic_error(string("Unsupported clustering ").append(clusteringName));
            Notifier::info("Using " + clusteringName);
            i++;
        }
#endif
        else if (string(args[i]) == "--with-column-labels" || string(args[i]) == "-h")
            c.withColumnLabels = true;
        else if (string(args[i]) == "--with-row-labels" || string(args[i]) == "-i")
            c.withRowLabels = true;
        else if (string(args[i]) == "-n" || string(args[i]) == "--nominal") {
            c.parseCommaList(string(args[i + 1]), c.nominal);
            i++;
        } else if (string(args[i]) == "-o" || string(args[i]) == "--ordinal") {
            c.parseCommaList(string(args[i + 1]), c.ordinal);
            i++;
        } else if (string(args[i]) == "-t" || string(args[i]) == "--threshold") {
            c.linesToRead = stoi(args[i + 1]);
            i++;
        } else
            throw logic_error(string("Unsupported option ").append(string(args[i])));
    }
#ifdef HIERARCHICAL // One clustering only in hierarchical mode
    c.clustering = IClustering::CANOPY;
#endif
    return c;
}

#endif

void loadFromArgs(unsigned int nbParams,
                  char **params,
                  Visualization *vis) {
    if (nbParams < 2) {
        throw "Invalid number of parameters";
    }
#ifndef LOCAL
    string dataSet = std::string(params[0]);
    try {
        Server::Configuration c = RestServer::parseConfigFile(params[1]);
        Notifier::info("Using: " + c.hostname + ":" + std::to_string(c.port) + "/" + c.base);
        vis->loadRemoteData(c, dataSet);
    } catch (const std::exception &e) {
        // If second argument is not a valid file path, interpret it as a server address
        vis->loadRemoteData(dataSet, std::string(params[1]));
        Notifier::info("Using: " + std::string(params[1]));
    }
#else
    config = parseConfiguration(nbParams, params);
    vis->loadLocalData(config);
    /*} else {
        string dataPath = string(params[0]);
        string hierarchyPath = string(params[1]);
        vis->loadHierarchy(dataPath, hierarchyPath);
    }*/
#endif
}

Visualization::Modifier getModifier(int key, int mod) {
    if (key == GLFW_KEY_LEFT_SHIFT ||
        key == GLFW_KEY_RIGHT_SHIFT ||
        mod == GLFW_MOD_SHIFT)
        return Visualization::Modifier::SHIFT;
    if (key == GLFW_KEY_LEFT_ALT ||
        key == GLFW_KEY_RIGHT_ALT ||
        mod == GLFW_MOD_ALT)
        return Visualization::Modifier::ALT;
    if (key == GLFW_KEY_LEFT_CONTROL ||
        key == GLFW_KEY_RIGHT_CONTROL ||
        mod == GLFW_MOD_CONTROL)
        return Visualization::Modifier::CONTROL;
    return Visualization::Modifier::NONE;
}


Visualization::Modifier getPressed() {
    if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS ||
        glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS)
        return Visualization::Modifier::SHIFT;
    if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS ||
        glfwGetKey(window, GLFW_KEY_RIGHT_CONTROL) == GLFW_PRESS)
        return Visualization::Modifier::CONTROL;
    if (glfwGetKey(window, GLFW_KEY_LEFT_ALT) == GLFW_PRESS ||
        glfwGetKey(window, GLFW_KEY_RIGHT_ALT) == GLFW_PRESS)
        return Visualization::Modifier::ALT;
    return Visualization::Modifier::NONE;
}


void onKeyDown(int key) {
    float time = glfwGetTime();
    switch (key) {
        case GLFW_KEY_SPACE:
            vis->changeMode(Visualization::DEFAULT);
            break;
        case GLFW_KEY_A:
            vis->activateEnhancement = !vis->activateEnhancement;
            break;
        case GLFW_KEY_B:
            vis->switchParameter(Visualization::SwitchableParameter::INTERVAL_HIGHLIGHT_MODE, time);
            break;
        case GLFW_KEY_C:
            vis->center();
            break;
        case GLFW_KEY_D:
            vis->changeMode(Visualization::MOVE_AXIS);
            break;
        case GLFW_KEY_E:
            vis->changeMode(Visualization::EDGE_LOOK);
            break;
        case GLFW_KEY_F:
            vis->fit();
            break;
        case GLFW_KEY_G:
            vis->switchParameter(Visualization::SwitchableParameter::GRADIENT, time);
            break;
        case GLFW_KEY_H:
            vis->switchParameter(Visualization::SwitchableParameter::INNER_NODE_VIEW, time);
            break;
        case GLFW_KEY_I:
            vis->changeMode(Visualization::INVERT_AXIS);
            break;
        case GLFW_KEY_J:
            vis->changeMode(Visualization::FOCUS);
            break;
        case GLFW_KEY_K:
            vis->switchParameter(Visualization::SwitchableParameter::CONTEXT_COMPRESSION_MODE, time);
            break;
        case GLFW_KEY_L:
            vis->switchParameter(Visualization::SwitchableParameter::DISPLAY_LABELS, time);
            break;
        case GLFW_KEY_M:
            vis->switchParameter(Visualization::SwitchableParameter::HEIGHT_MAPPING, time);
            break;
        case GLFW_KEY_N:
            break;
        case GLFW_KEY_O:
            vis->switchParameter(Visualization::SwitchableParameter::GRADIENT_SCOPE, time);
            break;
        case GLFW_KEY_P:
            vis->changeMode(Visualization::PICKER);
            break;
        case GLFW_KEY_Q:
            break;
        case GLFW_KEY_R:
#ifdef LOCAL
#ifndef EMSCRIPTEN
            vis->loadLocalData(config);
#endif
#endif
            break;
        case GLFW_KEY_S:
            vis->changeMode(Visualization::SCALE);
            break;
        case GLFW_KEY_T:
            vis->changeMode(Visualization::DELETE_AXIS);
            break;
        case GLFW_KEY_U:
            vis->changeMode(Visualization::TOP_VARIATION);
            break;
        case GLFW_KEY_V:
            vis->changeMode(Visualization::VARIATION);
            break;
        case GLFW_KEY_W:
            vis->changeMode(Visualization::INSERT_DIMENSION);
            break;
        case GLFW_KEY_X:
            vis->dataSource->invertSelection();
            break;
        case GLFW_KEY_Y:
#ifndef HIERARCHICAL
            vis->changeMode(Visualization::FILTER);
#endif
            break;
        case GLFW_KEY_Z:
            break;
        default:
            break;
    }
}

void reshapeCallback(GLFWwindow *, int width, int height) {
    if (vis == nullptr || !vis->isReady()) return;
    vis->onReshape((unsigned int) width, (unsigned int) height);
}

void onReshape(unsigned int width, unsigned int height) {
    if (vis == nullptr) return;
    //glfwSetWindowSize(window,width, height);
    vis->onReshape(width, height);
}

void errCallback(int error, const char *description) {
    cerr << "Error " << to_string(error) << ":" << string(description) << endl;
}

void keyCallback(GLFWwindow *window, int key, int, int action, int mods) {
    if (vis == nullptr || !vis->isReady()) return;
    switch (action) {
        case GLFW_PRESS:
            onKeyDown(key);
            break;
        case GLFW_RELEASE:
            if (vis->getCurrentModifier() == Visualization::Modifier::NONE) {
                vis->hideHighlightPreview();
            }
            break;
        default:
            break;
    }
#ifndef EMSCRIPTEN
    if (key == GLFW_KEY_A && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
#endif
}

void mouseMoveCallback(GLFWwindow *window, double x, double y) {
    if (vis == nullptr || !vis->isReady()) return;
    int mouseButton = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    vis->onMouseMove(x, y);
    if (mouseButton == GLFW_PRESS)
        vis->onActiveMouseMove(x, y);
    else {
        vis->onPassiveMouse(x, y);
    }
}

void mouseButtonCallback(GLFWwindow *window, int button, int action, int mods) {
    if (vis == nullptr || !vis->isReady()) return;
    double x, y;
    glfwGetCursorPos(window, &x, &y);
    //auto m = getModifier(-1, mods);
    auto m = getPressed();
    if (m != Visualization::Modifier::NONE)
        vis->setCurrentModifier(m);
    else {
        vis->setCurrentModifier(Visualization::Modifier::NONE);
    }
    if (vis->isOutOfScreen(x, y)) return;
    switch (action) {
        case GLFW_PRESS: {
            if (button == GLFW_MOUSE_BUTTON_LEFT)
                vis->onLeftButtonPress(x, y);
            break;
        }
        case GLFW_RELEASE: {

            if (button == GLFW_MOUSE_BUTTON_LEFT)
                vis->onLeftButtonRelease(x, y, glfwGetTime());
            break;
        }
        default:
            break;
    }
}

void mouseScrollCallback(GLFWwindow *window, double, double yOffSet) {
    if (vis == nullptr || !vis->isReady()) return;
    double x, y;
    glfwGetCursorPos(window, &x, &y);
    vis->onMouseWheel(x, y, yOffSet);
}

void mainLoop() {
    if (vis == nullptr) return;
    vis->onDraw();
    try {
        vis->onIdle(glfwGetTime());
    } catch (const std::exception &e) {
        Notifier::error(e.what());
#ifndef EMSCRIPTEN
        glfwSetWindowShouldClose(window, GL_TRUE);
#endif
    }
    glfwSwapInterval(1);
    glfwSwapBuffers(window);
    glfwPollEvents();
}

bool validateWindowSizeParameters(char *argv[]) {
    try {
        std::stoi(argv[1]);
        std::stoi(argv[2]);
    } catch (const std::invalid_argument &) {
        return false;
    } catch (const std::out_of_range &) {
        return false;
    }
    return true;
}


void init() {
#ifdef HIERARCHICAL
    std::cout << "Hierarchical version, ";
#else
    std::cout << "Flat version, ";
#endif
#ifdef LOCAL
    std::cout << "standalone mode" << std::endl;
#else
    std::cout << "server-supported mode" << std::endl;
#endif
#ifdef EMSCRIPTEN
    EmscriptenWebGLContextAttributes attr;
    emscripten_webgl_init_context_attributes(&attr);
    attr.alpha = attr.premultipliedAlpha = 0;
    attr.depth = attr.antialias = 1;
    attr.failIfMajorPerformanceCaveat = 0;
    attr.enableExtensionsByDefault = 1;
    attr.majorVersion = 1; // Updates needed for WebGL2
    attr.minorVersion = 0;
    EMSCRIPTEN_WEBGL_CONTEXT_HANDLE ctx = emscripten_webgl_create_context(0, &attr);
    emscripten_webgl_make_context_current(ctx);
#endif
}

bool initWindowArguments(int argc, char *argv[], int &width, int &height) {
#ifndef EMSCRIPTEN
    if (argc < 3) return false;
    if (!validateWindowSizeParameters(argv)) {
        return false;
    }
    width = stoi(argv[1]);
    height = stoi(argv[2]);
#endif
    return true;
}

GLFWwindow *initGLFW(int width, int height) {
    if (glfwInit() == 0) return nullptr;
    //glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
    glfwWindowHint(GLFW_SAMPLES, 4);     // Anti-aliasing
    window = glfwCreateWindow(width, height, "Visualisation", nullptr, nullptr);
    if (window == nullptr) return nullptr;
    /* Context */
    glfwMakeContextCurrent(window);
    glfwGetFramebufferSize(window, &width, &height);
    /* Callbacks */
    glfwSetErrorCallback(errCallback);
    glfwSetKeyCallback(window, keyCallback);
    glfwSetCursorPosCallback(window, mouseMoveCallback);
    glfwSetMouseButtonCallback(window, mouseButtonCallback);
    glfwSetScrollCallback(window, mouseScrollCallback);
    glfwSetWindowSizeCallback(window, reshapeCallback);
    return window;
}

void clean() {
    if (window != nullptr)
        glfwDestroyWindow(window);
    if (vis != nullptr)
        delete vis;
    glfwTerminate();
}

int main(int argc, char *argv[]) {
    init();
    int width = 1000, height = 400;
    if (!initWindowArguments(argc, argv, width, height)) {
        printUsage();
        return EXIT_FAILURE;
    }
    window = initGLFW(width, height);
    if (window == nullptr) {
        clean();
        return EXIT_FAILURE;
    }

    try {
        vis = new Visualization(width, height);
    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }
    /* Main loop */
#ifdef EMSCRIPTEN
    emscripten_set_main_loop(mainLoop, 0, true);
#else
    try {
        loadFromArgs(argc - 3, &(argv[3]), vis);
    } catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
        printUsage();
        clean();
        return EXIT_FAILURE;
    }
    while (glfwWindowShouldClose(window) != true) {
        mainLoop();
    }
#endif
    clean();
    return EXIT_SUCCESS;
}

#ifdef LOCAL
/*
void loadData(const std::string &data,
              bool file,
              const std::string &ordinalDims,
              const std::string &nominalDims,
              float minValue,
              float maxValue,
              bool withColumnLabels,
              bool withRowLabels,
              unsigned int nbClusters,
              IClustering::Type clustering) {
    TableHandler::Configuration c;
    c.data = data;
    c.isFilePath = file;
    c.withRowLabels = withRowLabels;
    c.withColumnLabels = withColumnLabels;
    c.clustering = clustering;
    c.nbClusters = nbClusters;
    c.parseCommaList(ordinalDims, c.ordinal);
    c.parseCommaList(nominalDims, c.nominal);
    c.minValue = minValue;
    c.maxValue = maxValue;
    if (vis != nullptr)
        vis->loadLocalData(c);
}

*/
void loadData(const std::string &data,
              bool file,
              const std::string &ordinalDims,
              const std::string &nominalDims,
              bool withColumnLabels,
              bool withRowLabels,
              unsigned int nbClusters,
              IClustering::Type clustering) {
    TableHandler::Configuration c;
    c.data = data;
    c.isFilePath = file;
    c.withRowLabels = withRowLabels;
    c.withColumnLabels = withColumnLabels;
    c.clustering = clustering;
    c.nbClusters = nbClusters;
    c.parseCommaList(ordinalDims, c.ordinal);
    c.parseCommaList(nominalDims, c.nominal);
    c.minValue = 0;
    c.maxValue = 0;
    if (vis != nullptr)
        vis->loadLocalData(c);
}

#else
void loadData(const string &file, const string &hostname, short int port, const string &baseName) {
    if (vis == nullptr || !vis->isReady()) return;
    RestServer::Configuration c;
    c.hostname = hostname;
    c.port = port;
    c.base = baseName;
    vis->loadRemoteData(c, file);
}
#endif

void center() {
    if (vis == nullptr || !vis->isReady()) return;
    vis->center();
}

void fit() {
    if (vis == nullptr || !vis->isReady()) return;
    vis->fit();
}

void changeMode(int s) {
    if (vis == nullptr || !vis->isReady()) return;
    vis->changeMode((Visualization::State) s);
}

void setColorScheme(ColorScheme::Type scheme) {
    if (vis == nullptr || !vis->isReady()) return;
    vis->setColorScheme(scheme);
}

unsigned long getNbAxes() {
    if (vis == nullptr || !vis->isReady()) return 0;
    return vis->getNbAxes();
}


unsigned long getNbSourceDims() {
    if (vis == nullptr || !vis->isReady()) return 0;
    return vis->getNbSourceDimensions();
}

string getDimensionLabel(unsigned int d) {
    if (vis == nullptr || !vis->isReady()) return "";
    return vis->getDimensionLabel(d);
}

void switchInnerNodeView() {
    if (vis == nullptr || !vis->isReady()) return;
    vis->switchParameter(Visualization::SwitchableParameter::INNER_NODE_VIEW, glfwGetTime());
}

void setGap(float x) {
    if (vis == nullptr || !vis->isReady()) return;
    vis->setGap(x);
}

void printHello() {
    puts("Hello");
}

void setDimensionToInsert(unsigned int d) {
    if (vis == nullptr || !vis->isReady()) return;
    vis->insertDimension(d);
}

void toggleLabels() {
    if (vis == nullptr || !vis->isReady()) return;
    vis->switchParameter(Visualization::SwitchableParameter::DISPLAY_LABELS, glfwGetTime());
}

void changeMapping() {
    if (vis == nullptr || !vis->isReady()) return;
    vis->switchParameter(Visualization::SwitchableParameter::HEIGHT_MAPPING, glfwGetTime());
}

void exportData(const string &result,
                bool highlightOnly,
                bool includeDeletedDimensions) {
    std::string r = result;
    if (vis == nullptr || !vis->isReady()) return;
    vis->exportData(r, true, highlightOnly, includeDeletedDimensions);
}

string exportDataAsString(bool highlightOnly,
                          bool includeDeletedDimensions) {
    string result;
    if (vis == nullptr || !vis->isReady()) return result;
    vis->exportData(result, false, highlightOnly, includeDeletedDimensions);
    return result;
}

std::vector<float> getNumericalInfo() {
    if (vis == nullptr || !vis->isReady()) return std::vector<float>();
    return vis->getPick().numerical;
}

string getCategory() {
    if (vis->isReady())
        return vis->getPick().categorical;
    return "";
}

std::vector<unsigned int> getDeletedDims() {
    if (vis == nullptr || !vis->isReady())return std::vector<unsigned int>();
    return vis->getDeletedDims().getAsVector();

}

void changeGradientScope() {
    if (vis == nullptr || !vis->isReady()) return;
    vis->switchParameter(Visualization::SwitchableParameter::GRADIENT_SCOPE, glfwGetTime());

}

void setEdgeReduction(float value) {
    if (vis == nullptr || !vis->isReady()) return;
    vis->displayParameters.setEdgeReduction(value);
}

void setEdgeCurvature(float value) {
    if (vis == nullptr || !vis->isReady()) return;
    vis->displayParameters.setEdgeCurvature(value);
}


bool isFocusAvailable() {
#ifdef HIERARCHICAL
    return true;
#else
    return false;
#endif
}

void setGreyScaleTone(float value) {
    if (vis == nullptr || !vis->isReady()) return;
    vis->displayParameters.setGreyScale(value);

}

void setVariationToolRange(float value) {
    if (vis == nullptr || !vis->isReady()) return;
    vis->selectedEdges.setRange(value);
    vis->renderer->hideEnhancedElements();

}

void setVariationToolCenter(float value) {
    if (vis == nullptr || !vis->isReady()) return;
    vis->selectedEdges.setCenter(value);
    vis->renderer->hideEnhancedElements();

}


std::vector<float> getIndividual(unsigned int i) {
    if (vis == nullptr || !vis->isReady()) return std::vector<float>();;
    return std::vector<float>();//vis->dataSource->
}

std::vector<unsigned int> getHighlight() {
    if (vis->isReady()) {
        const Highlight *h = vis->dataSource->getCurrentHighlight();
        if (h == nullptr) return Individuals();
        else return h->getIndividuals();
    }
    return std::vector<unsigned int>();
}

void invertSelection() {
    if (vis->isReady()) {
        vis->dataSource->invertSelection();
        Notifier::onHighlight(true);
    }
}

void setActivateEnhancement(bool b) {
    if (vis->isReady()) {
        vis->activateEnhancement = b;
    }
}

std::vector<std::string> getPickingLabels() {
    return {"weight", "min", "max"};
}

void clearDisplay() {
    if (vis->isReady()) {
        vis->clear();
    }
}


#ifdef EMSCRIPTEN
#include "emscripten/bind.h"
#include "emscripten/val.h"

EMSCRIPTEN_BINDINGS(ParallelCoordinates) {
    emscripten::register_vector<float>("VecFloat");
    emscripten::register_vector<std::string>("VecString");
    emscripten::register_vector<unsigned int>("VecUInt");
    emscripten::function("loadData", &loadData);
    emscripten::function("clearDisplay", &clearDisplay);
    emscripten::function("center", &center);
    emscripten::function("fit", &fit);
    emscripten::function("changeMode", &changeMode);
    emscripten::function("getNbDims", &getNbSourceDims);
    emscripten::function("getNbAxes", &getNbAxes);
    emscripten::function("getDimensionLabel", &getDimensionLabel);
    emscripten::function("switchInnerNodeView", &switchInnerNodeView);
    emscripten::function("setGap", &setGap);
    emscripten::function("printHello", &printHello);
    emscripten::function("setDimensionToInsert", &setDimensionToInsert);
    emscripten::function("toggleLabels", &toggleLabels);
    emscripten::function("changeMapping", &changeMapping);
    emscripten::function("exportData", &exportData);
    emscripten::function("exportDataAsString", &exportDataAsString);
    emscripten::function("getNumericalInfo", &getNumericalInfo);
    emscripten::function("getCategory", &getCategory);
    emscripten::function("changeGradientScope", &changeGradientScope);
    emscripten::function("onReshape", &onReshape);
    emscripten::function("getDeletedDims", &getDeletedDims);
    emscripten::function("setColorScheme", &setColorScheme);
    emscripten::function("isFocusAvailable", &isFocusAvailable);
    emscripten::function("setEdgeReduction", &setEdgeReduction);
    emscripten::function("setEdgeCurvature", &setEdgeCurvature);
    emscripten::function("setGreyScaleTone", &setGreyScaleTone);
    emscripten::function("getHighlight", &getHighlight);
    emscripten::function("getIndividual", &getIndividual);
    emscripten::function("getPickingLabels", &getPickingLabels);
    emscripten::function("setActivateEnhancement", &setActivateEnhancement);
    emscripten::function("setVariationToolRange", &setVariationToolRange);
    emscripten::function("setVariationToolCenter", &setVariationToolCenter);
    emscripten::function("invertSelection", &invertSelection);
};
#endif // EMSCRIPTEN

