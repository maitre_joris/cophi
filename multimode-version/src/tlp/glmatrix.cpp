#include "glmatrix.h"

/**
 * init identity 4x4 matrix.
 */
void identity(GlMat4f &m) {
    m.fill(0.);
    m[0][0] = 1;
    m[1][1] = 1;
    m[2][2] = 1;
    m[3][3] = 1;
}

/**
 * init rotation 4x4 matrix.
 */
void rotate(GlMat4f &m, const float angle,  const Vec3f &coords) {
    double s(sin(angle)),c(cos(angle));
//    sincos(angle, &s, &c);
    m.fill(0);
    Vec3f v(coords);
    v.normalize();
    m[0][0] = v[0] * v[0] * (1. - c) + c;
    m[0][1] = v[1] * v[0] * (1. - c) + v[2] * s;
    m[0][2] = v[0] * v[2] * (1. - c) - v[1] * s;
    m[1][0] = v[0] * v[1] * (1. - c) - v[2] * s;
    m[1][1] = v[1] * v[1] * (1. - c) + c;
    m[1][2] = v[1] * v[2] * (1. - c) + v[0] * s;
    m[2][0] = v[0] * v[2] * (1. - c) + v[1] * s;
    m[2][1] = v[1] * v[2] * (1. - c) - v[0] * s;
    m[2][2] = v[2] * v[2] * (1. - c) + c;
    m[3][3] = 1;
}
void glRotate(GlMat4f &m, const float angle,  const Vec3f &coords) {
    GlMat4f tmp;
    rotate(tmp, angle, coords);
    m = tmp * m;
}

/**
 * init translate 4x4 matrix.
 */
void scale(GlMat4f &m, const Vec3f &coords) {
    identity(m);
    m[0][0] = coords[0];
    m[1][1] = coords[1];
    m[2][2] = coords[2];
}
void glScale(GlMat4f &m, const Vec3f &coords) {
    GlMat4f tmp;
    scale(tmp, coords);
    m = tmp * m;
}

/**
 * init translate 4x4 matrix.
 */
void translate(GlMat4f &m, const Vec3f &coords) {
    identity(m);
    m[3][0] = coords[0];
    m[3][1] = coords[1];
    m[3][2] = coords[2];
}

void glTranslate(GlMat4f &m, const Vec3f &coords) {
    GlMat4f tmp;
    translate(tmp, coords);
    m = tmp * m;
}

void perspective(GlMat4f &m, float fovy, float aspect, float zNear, float zFar) {
   identity(m);
   double cotangent, deltaZ;
   float radians = fovy / 2. * M_PI / 180.;
   deltaZ = zFar - zNear;
   double sine(sin(radians)), cosine(cos(radians));
//   sincos(radians, &sine, &cosine);
   if ((deltaZ < 1E-4) || (sine < 1E-4) || (aspect < 1E-4))
      return;
   cotangent = cosine / sine;
   m[0][0] = cotangent / aspect;
   m[1][1] = cotangent;
   m[2][2] = -(zFar + zNear) / deltaZ;
   m[2][3] = -1;
   m[3][2] = -2 * zNear * zFar / deltaZ;
   m[3][3] = 0;
}

void ortho(GlMat4f &m, float left, float right, float bottom, float top, float zNear, float zFar) {
    identity(m);
    m[0][0] = 2. / (right - left);
    m[1][1] = 2. / (top - bottom);
    m[2][2] = -2. / (zFar - zNear);
    m[3][0] = -(right + left) / (right - left);
    m[3][1] = -(top + bottom) / (top - bottom);
    m[3][2] = -(zFar + zNear) / (zFar - zNear);
}

Vec3f project(const Vec3f &obj, const GlMat4f &model, const GlMat4f &proj, const Vec4i view) {
    Vec4f res(proj * model * Vec4f(obj, 1.));
    res[0] = view[0] + (view[2] * (res[0] + 1.))/2.;
    res[1] = view[1] + (view[3] * (res[1] + 1.))/2.;
    res[2] = (res[2] + 1.)/2.;
    return res;
}

Vec3f unProject(const Vec3f &obj, const GlMat4f &model, const GlMat4f &proj, const Vec4i view) {
    Vec4f res;
    res[0] = 2. * ( obj[0] - view[0]) / view[2]  - 1.;
    res[1] = 2. * ( obj[1] - view[1]) / view[3]  - 1.;
    res[2] = 2. * obj[2]  - 1.;
    res[3] = 1.;
    return (proj * model).inverse() * res;
}

