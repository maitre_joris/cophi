#!/usr/bin/env bash
CORES=8 # Set your own number of cores
SHARED_DIRECTORIES=("fonts" "thirdparty")
SRC_FOLDER="./multimode-version"

# Check executing environment
for dir in ${SHARED_DIRECTORIES[@]}
do
    if [[ ! -d "./${dir}" ]];
    then
        >&2 echo "Could not @{dir} directory in current working directory"
        exit 0
    fi
done

# Clean executable
if [[ -f "${SRC_FOLDER}/bin/paraCoord" ]];
then
    rm ${SRC_FOLDER}/bin/paraCoord;
fi


if [[ "$#" -eq 0 ]];
then
	echo "No argument provided, default parameters will be used. Use '--help' for usage.";
fi

# Default parameters
export mode="Standalone" # or 'Client'
export hierarchical="OFF"
export browser="sensible-browser"
export launch=false
export dataSet="./data-samples/cars.csv"
export parameters="--with-column-labels -k 10 -c canopy -n 6,7,8,9 -o 1,5"

parameters() {
	echo "Using parameters:"
	echo -e "\tMode: ${mode}"
	echo -e "\tHierarchical navigation: ${hierarchical}"
	if [ ! ${launch} ];
	then
        echo "\tBuild only"
        return
	fi

	if [ ${mode} == "Standalone" ];
    then
        echo -e "\tRunning with data set: ${dataSet} and parameters ${parameters}"
    fi
	if [ ${mode} == "Client" ];
	then
         echo -e "\tRunning with data set: ${dataSet} and parameters ${parameters}"
	fi
}

usage() {
    echo "Usage: $0 [-m <Standalone|Client>] [-h <ON|OFF>] [-c] [-r]" 1>&2;
    echo -e "Use option '-c' in between changing mode to clean build files and option '-r' to run the built executable."
    echo -e "When running with '-r' in \033[1mbold\033[0m Standalone mode, a default data set and parameters will be used. To specify another one, use '-d' followed by your data set file path. Other parameters depends on the version (hierarchical or not) and should be provided with '-p' and quoted."
    echo -e "When running with '-r' in \033[1mbold\033[0m Client mode, no default options are provided. You should specify the server's service address to use with the option '-p' and a corresponding data set with '-d'."
    exit 1;
}

clean() {
    if [[ -d "${SRC_FOLDER}/buildStandalone" ]];
    then
        rm -r ${SRC_FOLDER}/buildStandalone;
    fi
    if [[ -d "${SRC_FOLDER}/buildClient" ]];
    then
        rm -r ${SRC_FOLDER}/buildClient;
    fi
}


if [[ "$1" == "--help" || "$1" == "help" ]];
then
	echo "Utility to launch the web interface for parallel coordinates app";
	echo "Default parameters are:";
	parameters
	usage
	exit 0
fi
while getopts "m:h:d:p:cr" opt; do
	case "${opt}" in
		m)
			[[ ${OPTARG} == "Standalone" || ${OPTARG} == "Client" ]] || usage;
			mode=${OPTARG}
			;;
		h)
			[[  "${OPTARG}" == "ON"  || "${OPTARG}" == "OFF" ]] || usage;
			hierarchical=${OPTARG}
			;;
		d)
			dataSet=${OPTARG}
			;;
		p)
			parameters=${OPTARG}
			;;
		c)
			clean
			exit 0
			;;
		r)
			launch=true;
			;;
		*)
			usage
			;;
	esac
	#shift $((OPTIND-1))

	if [ -z "${mode}" ] || [ -z "${hierarchical}" ]; then
		usage
	fi

done

# Print used parameters
parameters

cd ${SRC_FOLDER};

# Create symbolic links for shared directory
for dir in ${SHARED_DIRECTORIES[@]}
do
	if [ ! \( -e ${dir} \) ]
	then
		ln -s ../${dir} ${dir}
	fi
done

mkdir -p build${mode}; cd build${mode};

echo "Compiling..."
cmake .. -DCMAKE_BUILD_TYPE=Release -DHIERARCHICAL=${hierarchical} -DBASE_MODE=${mode}
echo "Building...";
make -j$(($NB_CORES +1)) paraCoord;
make install;

cd ..;
chmod +x ./bin/paraCoord;

# Launch application with data set
if [ ${launch} == true ];
then
	if [[ ${mode} == "Standalone" ]];
	then
	    # Copy provided file (provided with path relative to top directory)
	    cp ../${dataSet} tmp_data_set
        ./bin/paraCoord 1000 600 tmp_data_set ${parameters}
        rm tmp_data_set
	else
        ./bin/paraCoord 1000 600 ${dataSet} ${parameters}
	fi
fi

