##!/bin/bash
RED='\e[0;31m'
GREEN='\e[0;32m'
NC='\033[0m' # No Color

cd multimode-version;
nb_proc=$(nproc --all);
failed=0;
passed=0;

launch_test() 
{
	h=$1
	m=$2
	mkdir -p tmp; cd tmp;
	cmake .. -DCMAKE_BUILD_TYPE=Release -DHIERARCHICAL=${h} -DBASE_MODE=${m} >& /dev/null;
	make -j"${nb_proc}" paraCoord >& /dev/null;
	if [ $? -ne 0 ]; 
	then
		echo -ne "\n${RED}Failed for mode "
		if [ "${h}" == 'ON' ];
		then
	        	echo -n "Hierarchical+"
	        else
	        	echo -n "Flat+"
		echo -e ${m}${NC}
		failed=$((failed+1));
		cd ..;
		rm -rf tmp;
		echo "Exiting after ${passed} passed and ${failed} failed"
		exit 1;
		fi	
	else
		echo -ne "${GREEN}.${NC}";
		passed=$((passed+1));
		cd ..;
		rm -rf tmp;
	fi
}

echo "Starting building tests";

launch_test 'OFF' 'Standalone';
launch_test 'OFF' 'Client';
launch_test 'ON' 'Standalone';
launch_test 'ON' 'Client';

echo -e "\n${GREEN}Successfuily compiled all ${passed} modes${NC}";
