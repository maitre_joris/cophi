var vis, sideBar, dataHandler;

/*
 This object's attributes belonging to State enum defined in Visualization.cpp
 have to correspond to the same values in both JS and C++ code as these
 states will be passed from compiled C++ via eventCallback() so
 that the UI reflects mode change.
 */

var vizEvent = {};
vizEvent.Enum = { //TODO: divide event into modes and others?
    HIGHLIGHT_ON: {name: "highlightOn", value: 1},
    HIGHLIGHT_OFF: {name: "highlightOff", value: 2},
    DEFAULT: {name: "default", value: 3},
    INVERT_AXIS: {name: "inversion", value: 4},
    MOVE_AXIS: {name: "moveAxis", value: 5},
    DELETE_AXIS: {name: "deleteAxis", value: 6},
    INSERT_DIMENSION: {name: "insertDimension", value: 7},
    SCALE: {name: "scale", value: 8},
    FILTER: {name: "filter", value: 9},
    PICKER: {name: "bubbleInfo", value: 10},
    HELP: {name: "help", value: 11},
    AXIS_CHANGE: {name: "axisChange", value: 12}
};

/* Starting mode */
vizEvent.initMode = function () {
    "use strict";
    if (vizEvent.currentState !== undefined) {
        vis.toolBar.buttonSwitch(vizEvent.currentState.name, false);
    }
    vizEvent.switchMode(vizEvent.Enum.DEFAULT);
    vis.toolBar.buttonSwitch(vizEvent.Enum.DEFAULT.name, true);
};

vizEvent.switchMode = function (newState) {
    "use strict";
    if (newState !== vizEvent.Enum.HIGHLIGHT_OFF &&
        newState !== vizEvent.Enum.HIGHLIGHT_ON &&
        newState !== vizEvent.Enum.HELP) {
        vizEvent.currentState = newState;
    }
    vis.setCursor(vizEvent.currentState);
};

vizEvent.getName = function (value) {
    "use strict";
    var key;
    for (key in vizEvent.Enum) {
        if (vizEvent.Enum.hasOwnProperty(key)) {
            if (vizEvent.Enum[key].value === value) {
                return vizEvent.Enum[key].name;
            }
        }
    }
    console.log("Could not find " + value);
};

vizEvent.getEnum = function (value) {
    "use strict";
    var key;
    for (key in vizEvent.Enum) {
        if (vizEvent.Enum.hasOwnProperty(key)) {
            if (vizEvent.Enum[key].value === value) {
                return vizEvent.Enum[key];
            }
        }
    }
    console.log("Could not find " + value);
};

function eventCallback(eventValue) {
    "use strict";
    var pastEvent, newEvent;
    switch (eventValue) {
        case vizEvent.Enum.HIGHLIGHT_ON.value:
            sideBar.onHighlight(true);
            dataHandler.fetchData();
            break;
        case vizEvent.Enum.HIGHLIGHT_OFF.value:
            sideBar.onHighlight(false);
            dataHandler.fetchData();
            break;
        case vizEvent.Enum.AXIS_CHANGE.value:
            dataHandler.fetchData();
            sideBar.onAxisChange();
            break;
        default:
            if (eventValue) {
                pastEvent = vizEvent.currentState;
                newEvent = vizEvent.getEnum(eventValue);
                vizEvent.switchMode(newEvent);
                if (vizEvent.currentState !== pastEvent) {
                    vis.toolBar.buttonSwitch(pastEvent.name, false);
                    vis.toolBar.buttonSwitch(newEvent.name, true);
                }
                vis.setCursor(newEvent);
            }
    }
}

function errorCallback(errorCode) {
    "use strict";
    vis.onError(errorCode);
}

var sideBar, Module, vis;

var makeDataHandler = function () {
    'use strict';
    var dataOptions = {},
        filename,
        exportLink,
        self = {},
        fileData = [],
        highlightData = [];

    function handleFileReadAbort() {
        vis.onError(Module.Error.NO_FILE.value);
    }

    function handleFileReadError() {
        vis.onError(Module.Error.NO_FILE.value);
    }

    function writeFile(evt) {
        var fileString = evt.target.result; // Obtain the file contents, which was read into memory.
        //evt.target is a FileReader object, not a File object; so window.URL.createObject(evt.target) won't work here!
        if (evt.target.readyState === FileReader.DONE) { // DONE == 2
            FS.writeFile(importedFilePath, fileString, {
                encoding: 'utf8',
                flags: 'w+'
            });
            console.log("Finished loading file, please proceed to import.");
        } else {
            console.log("File is not ready to be written");
        }
    }

    function displayFile(evt) {
        var fileString = evt.target.result; // Obtain the file contents, which was read into memory.
        vis.hideError();
        if (evt.target.readyState === FileReader.DONE) { // DONE == 2
            fileData = parseCSV(fileString);
            if (fileData !== undefined) {
                console.log("Parsed " + fileData.length + " rows");
                updateExportURI(exportToCSV(fileData));
                vis.onDataLoaded(fileData, false);
                vis.onResize();
            } else {
                vis.clear();
                vis.onError(Module.Error.DELIMITER.value);
            }
        } else {
            console.log("File is not ready to be displayed");
        }
    }

    self.startFileRead = function (fileObject) {
        var reader = new FileReader();
        reader.onloadend = displayFile;
        reader.abort = handleFileReadAbort;
        reader.onerror = handleFileReadError;
        if (fileObject) {
            console.log("Starting new file reading");
            filename = fileObject.name;
            sideBar.setFilename(fileObject.name);
            reader.readAsText(fileObject);
        } else {
            filename = undefined;
            vis.onError(Module.Error.NO_FILE.value);
        }
    };

    /*
     * Guess delimiter and return matrix of values
     */
    function parseCSV(inputString) {
        if (inputString.length === 0) return undefined;
        var lines = inputString
            .replace(/&/g, '&amp;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')
            .replace(/"/g, '')
            .split(/[\n\r]/)
            .filter(function (n) {
                return (n.length > 0);
            });
        if (lines.length === 0) return undefined;
        // Find delimiter
        var foundSpace = false;
        var delimiter = null;
        var found = lines[0].split('').some(function (char) {
            if (char == ' ' || char == '&nbsp;' || char == '&ensp;' || char == '&emsp;') {
                foundSpace = true;
            } else if (char == '\t') {
                delimiter = '\t';
                return true;
            } else if (char == ',') {
                delimiter = ',';
                return true;
            } else if (char == ';') {
                delimiter = ';';
                return true;
            }
            return false;
        });
        if (!found && foundSpace) {
            delimiter = ' ';
            found = true;
        }
        if (found)
            return lines.map(function (line) {
                return line.split(delimiter);
            });
        else {
            vis.onError(Module.Error.DELIMITER.value);
            throw "Could not determine delimiter";
        }
    }

    function updateExportURI(blob) {
        // If we are replacing a previously generated file we need to
        // manually revoke the object URL to avoid memory leaks.
        if (exportLink !== null) {
            window.URL.revokeObjectURL(dataOptions.exportLink);
        }
        exportLink = window.URL.createObjectURL(
            new Blob([blob], {type: 'text/csv'})
        );
    }

    function exportToCSV(data, header) {
        //index == 0 && header? "data:text/csv;charset=utf-8," +
        var lineArray = [];
        data.forEach(function (lineValues, index) {
            var line = lineValues.join(",");
            lineArray.push(line);
        });
        return lineArray.join("\n");
    }

    /*
     Initial file loading
     */
    self.loadFile = function (formOptions) {
        if (fileData !== undefined) {
            vis.onDataLoaded(fileData, formOptions.withColumnLabels);
            sideBar.onReload();
            Module.loadData(
                exportToCSV(fileData),
                false,
                formOptions.ordinalDims,
                formOptions.nominalDims,
                formOptions.withColumnLabels,
                formOptions.withRowLabels,
                formOptions.nbClusters,
                formOptions.clusteringType);
            vis.onResize();
        }
    };

    self.getExportURI = function () {
        return exportLink;
    };

    /*
     Data re-loading for axis edition and highlighting operation
     Base data is exported from the core component as a file read/write
     Options: { highlightOnly, includeDeleted }
     */
    self.fetchData = function () {
        if (filename === undefined) {
            return;
        }
        var dataBlob, options, target;
        options = sideBar.getExportOptions();
        dataBlob = Module.exportDataAsString(options.highlightOnly, options.includeDeleted);
//      dataBlob = FS.readFile(exportFilePath, {encoding: 'utf8'});
        target = parseCSV(dataBlob, dataOptions.delimiter, target);
        vis.onDataLoaded(target, dataOptions.withColumnLabels);
        updateExportURI(dataBlob);
    };

    return self;
};

var Parser = (function () {
    "use strict";
    var self = {},
            elements = [],
            currentElement,
            currentItemList = [],
            currentItemWidth;

        function tryTitle(line) {
            var isTitle = /(#{1,2})([^#{}]+)(.*)/.exec(line);
            if (isTitle) {
                if (isTitle[1].length === 1) {
                    if (currentElement) {
                        elements.push(currentElement);
                    }
                    currentElement = {};
                    currentElement.title = isTitle[2];
                    currentElement.content = [];
                } else {
                    currentElement.content.push("<h3 class='help-sub-title'>" + isTitle[2] + "</h3>");
                }
            }
            return isTitle !== undefined;
        }

        function tryItemListStart(line) {
            var isItemListStart = /-([0-9])-/.exec(line);
            if (isItemListStart) {
                currentItemWidth = parseInt(isItemListStart[1]);
                currentItemList.push("<div class='aligned-container'>");
            }
            return isItemListStart !== undefined;
        }

        function tryIsItemEnd(line) {
            var isItemListEnd = /---/.exec(line);
            if (isItemListEnd) {
                currentItemList.push("</div>");
                currentItemList.forEach(function (e) {
                    currentElement.content.push(e);
                });
                currentItemList = [];
            }
            return isItemListEnd !== undefined;
        }

        function tryItem(line) {
            var isItem = /^>([^{}]+)?\{([a-zA-Z0-9.\-]+)\}(.*)/.exec(line);
            if (isItem) {
                currentItemList.push("<div class='aligned'>");
                currentItemList.push("<img class='help-icon'" +
                    " src='" + Path.imgFolder +  "/" + isItem[2] + "'/>");
                if (isItem[1]) {
                    currentItemList.push("<h4 class='legend-title'>" + isItem[1] + "</h4>");
                }
                if (isItem[3]) {
                    currentItemList.push("<p>" + isItem[3] + "</p>");
                }
                currentItemList.push("</div>");
            }
            return isItem !== undefined;
        }

        function tryPar(line) {
            var isPar = /^([^-#>].+)/.exec(line);
            if (isPar) {
                currentElement.content.push("<p>" + isPar[1] + "</p>");
            }
            return isPar !== undefined;
        }

        function tryEmpty(line) {
            return (/^\s*$/.exec(line) !== undefined);
        }

        self.parse = function (fileAsLines) {
            var tryProcedures = [tryEmpty, tryTitle, tryItemListStart,
                tryIsItemEnd, tryItem, tryPar];
            fileAsLines.forEach(function (line) {
                tryProcedures.some(function (element) {
                    element(line);
                });
                /*if (!reachedEnd) {
                 throw ("Unrecognized line: " + line);
                 }*/
            });
            return elements;
        };

        return self;
    }());

/*global
    makeDataHandler, makeVis, makeSideBar
*/
var vis, sideBar, dataHandler;

window.onError = function () {
    "use strict";
    vis.onError("Something bad happened");
};

var Path = {
  helpContent: "./assets/help.hd",
  errorContent: "./assets/errorMessages.json",
  imgFolder : "./assets"
};

var Module = {
    "arguments": ["400", "400"], //TODO better arguments
     "postRun": [function () {
       "use strict";
        dataHandler = makeDataHandler();
        vis = makeVis("canvasContainer");
        sideBar = makeSideBar();
    }],
    "print": function () {
        "use strict";
        var element = document.getElementById("output");
        if (element) {
            element.value = "";
        } // clear browser cache
        return function (text) {
            if (arguments.length > 1) {
                text = Array.prototype.slice.call(arguments).join(" ");
            }
            console.log(text);
            if (element) {
                element.value += text + "\n";
                element.scrollTop = element.scrollHeight; // focus on bottom
            }
        };
    },
    "printErr": function (text) {
        "use strict";
        if (arguments.length > 1) {
            text = Array.prototype.slice.call(arguments).join(" ");
        }
        console.error(text);
    },
    "canvas": (function () {
        "use strict";
        var canvas = document.getElementById("glcanvas");
        // As a default initial behavior, pop up an alert when webgl context is lost. To make your
        // application robust, you may want to override this behavior before shipping!
        // See http://www.khronos.org/registry/webgl/specs/latest/1.0/#5.15.2
        canvas.addEventListener("webglcontextlost", function (e) {
            alert("WebGL context lost. You will need to reload the page.");
            e.preventDefault();
        }, false);

        return canvas;
    }())
};

window.onresize = function () {
    "use strict";
    vis.onResize();
};

var Module, dataHandler, vis, addSwitchOptionToList, vizEvent, deactivateOption, activateOption;

var makeImportMenu = function (containerId) {
    "use strict";
    var self = {}, form = {};

    function handleDragOver(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
    }

    function handleDropFileSelect(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        dataHandler.startFileRead(evt.dataTransfer.files[0]);
    }

    function handleFileSelect() {
        dataHandler.startFileRead(form.fileBrowser.files[0]);
    }

    function onClusteringChange() {
        var isNone = (onClusteringChange.selector.value === "NONE");
        console.log("On clustering change");
        onClusteringChange.block.hidden = isNone;
        onClusteringChange.inputList.forEach(function (e) {
            e.required = !isNone;
        });
    }

    self.getOptionValues = function () {
        return {
            nbClusters: Math.max(2, parseInt(form.nbClusters.value, 10)),
            clusteringType: Module.Clustering[form.clusteringSelector.value],
            withRowLabels: form.withRowLabels.checked,
            withColumnLabels: form.withColumnLabels.checked,
            ordinalDims: form.ordinalDimensions.value,
            nominalDims: form.nominalDimensions.value
        };
    };

    self.styleFailedImport = function () {
        form.dropZone.className = "invalid";
    };

    self.initStyle = function () {
        form.dropZone.className = "";
    };

    function localInit() {
      form.clusteringSelector = document.getElementById("clusteringType");
      form.nbClusters = document.getElementById("nbClusters");
      form.clusteringSelector.onchange = onClusteringChange;
      form.withRowLabels = document.getElementById("withRowLabels");
      form.withColumnLabels = document.getElementById("withColumnLabels");
      form.ordinalDimensions = document.getElementById("ordinalDimensions");
      form.nominalDimensions = document.getElementById("nominalDimensions");

      onClusteringChange.selector = form.clusteringSelector;
      onClusteringChange.block = document.getElementById("clusteringOption");
      onClusteringChange.inputList = [form.nbClusters];

      // Drop zone listeners setup if File API is available
      if (window.File && window.FileReader && window.FileList && window.Blob) {
          form.dropZone = document.getElementById("dropZone");
          form.fileBrowser = document.getElementById("fileBrowser");
          form.dropZone.addEventListener('dragover', handleDragOver, false);
          form.dropZone.addEventListener('drop', handleDropFileSelect, false);
          form.fileBrowser.addEventListener('change', handleFileSelect, false);
      } else {
          vis.onError(Module.Error.NO_FILE.value);
      }
    }

    function remoteInit() {
      request(serverAddress, "plain/text", function(response) {
        var select = document.getElementById("selectDataset");
        response.split("\n").forEach(function(dataSetName) {
          var option = document.createElement("option");
          option.value = dataSetName;
          option.text = dataSetName.substring(0, 30) + "...";
          select.add(option);
        });
      });
    }

    function init() {
      if (VERSION == "local") localInit();
      else if (VERSION == "remote") remoteInit();
      else throw "Major incoherence in JS (invalid version)";
    }

    init();
    return self;
};

var makeRenderingMenu = function (rootId) {
    "use strict";
    var self = {}, options, colorSchemes, container, schemeSelector;
    container = document.getElementById(rootId);

    colorSchemes = [
        {
            id: "green-black-red",
            name: "Green Black Red",
            value: Module.ColorScheme.GREEN_BLACK_RED
        },
        {id: "viridis", name: "Viridis", value: Module.ColorScheme.VIRIDIS},
        {id: "legacy", name: "Legacy", value: Module.ColorScheme.LEGACY},
        {
            id: "baby-room",
            name: "Baby room",
            value: Module.ColorScheme.BABY_ROOM
        },
        {id: "palampa", name: "Palampa", value: Module.ColorScheme.PALAMPA}];

    options = {
        inNode: {
            label: "Histogram/Distortion",
            title: "Change inner node diagram mode",
            defaultChecked: false,
            defaultDisabled: true,
            onclick: function () {
				var e = document.getElementById("inNode");
				if (!e.disabled) e.checked = !e.checked;
                Module.switchInnerNodeView();
            }
        },
        nodeWeight: {
            label: "Weight/Range size",
            title: "Change node layout",
            defaultChecked: false,
            defaultDisabled: true,
            onclick: function () {
				var e = document.getElementById("nodeWeight");
				if (!e.disabled) e.checked = !e.checked;
                Module.changeMapping();
            }
        },
        gradient: {
            label: "Per axis/Global",
            title: "Change gradient scope",
            defaultChecked: false,
            defaultDisabled: true,
            onclick: function () {
				var e = document.getElementById("gradient");
				if (!e.disabled) e.checked = !e.checked;
                Module.changeGradientScope();
            }
        }
    };

    self.activateOption = function () {
        activateOption(options, Array.prototype.slice.call(arguments));
    };

    self.deactivateOption = function () {
        deactivateOption(options, Array.prototype.slice.call(arguments));
    };

    self.clear = function() {
        var o;
        for (o in options)
          if (options.hasOwnProperty(o))
             document.getElementById(o).checked = false;
	schemeSelector.firstElementChild.selected = true;
    };

    function onColorSchemeChange() {
        colorSchemes.some(function (scheme) {
            if (scheme.id === onColorSchemeChange.selector.value) {
                Module.setColorScheme(scheme.value);
                return true;
            }
            return false;
        });
    }

    function init() {
        schemeSelector = document.getElementById("colorSchemeSelector");
        var optionList = container.getElementsByTagName("ul")[0];
        colorSchemes.forEach(function (scheme) {
            var op = new Option();
            op.value = scheme.id;
            op.text = scheme.name;
            schemeSelector.add(op);
        });
        schemeSelector.firstElementChild.selected = true;
        schemeSelector.onchange = onColorSchemeChange;
        onColorSchemeChange.selector = schemeSelector;
        addSwitchOptionToList(options, optionList);
    }

    init();
    return self;
};

var makeExportMenu = function (rootId) {
    "use strict";
    var self = {}, options, exportLink;
    // Should be disabled at loading
    options = {
        "highlightOnly": {
            label: "Highlighted only",
            title: "Filter in highlighted elements",
            defaultChecked: true,
            defaultDisabled: true,
            onclick: function() {
				var e = document.getElementById("highlightOnly");
				if (!e.disabled) e.checked = !e.checked;
				dataHandler.fetchData();
			}
        },
        "includeDeleted": {
            label: "Include deleted",
            title: "Display all dimensions, including deleted ones",
            defaultChecked: true,
            defaultDisabled: true,
            onclick: function() {
				var e = document.getElementById("includeDeleted");
				if (!e.disabled) e.checked = !e.checked;
				dataHandler.fetchData();
			}
        }
    };

    function onExport() {
        exportLink.href = dataHandler.getExportURI();
        exportLink.click();
    }

    function createMenu() {
        var list, button, link, input;
        list = document.createElement("ul");
        addSwitchOptionToList(options, list);
        button = document.createElement("li");
        link = document.createElement("a");
        link.download = "export.csv";
        link.id = "downloadLink";
        link.style = "display: none;";
        input = document.createElement("input");
        input.type = "submit";
        input.value = "Download";
        input.id = "exportButton";
        input.addEventListener("click", onExport);
        button.appendChild(link);
        button.appendChild(input);
        list.appendChild(button);
        return list;
    }

    self.activateOption = function () {
        activateOption(options, Array.prototype.slice.call(arguments));
    };

    self.deactivateOption = function () {
        deactivateOption(options, Array.prototype.slice.call(arguments));
    };

    self.clear = function() {
        var o;
        for (o in options)
          if (options.hasOwnProperty(o))
             document.getElementById(o).checked = false;
    };

    self.getOptions = function () {
        var result = {}, oName, oElement;
        for (oName in options) {
            if (options.hasOwnProperty(oName)) {
                oElement = document.getElementById(oName);
                result[oName] = !oElement.disabled && oElement.checked;
            }
        }
        return result;
    };

    function init() {
        var container = document.getElementById(rootId);
	clearChildren(container);
	container.appendChild(createMenu());
        exportLink = document.getElementById('downloadLink');

    }
    init();
    return self;
};

function makeSideBar() {
    "use strict";
    var self = {}, importMenu, exportMenu, renderingMenu;
    self.importForm = {};

    self.onReload = function() {
        if (VERSION == 'local')
          exportMenu.clear();
        renderingMenu.clear();
    };

    function onReady() {
        importMenu = makeImportMenu("importMenu");
        renderingMenu = makeRenderingMenu("displayMenu");
        if (VERSION == 'local')
          exportMenu = makeExportMenu("exportMenu");
        document.getElementsByClassName("sidebar")[0].style.visibility = "visible";
    }

    self.getExportOptions = function () {
        if (VERSION == 'local')
          return exportMenu.getOptions();
        else return {};
    };

    self.setFilename = function (name) {
        document.getElementById("inputFileMessage").textContent = name;
    };
    
    self.onLoadSuccess = function () {
      if (VERSION == 'local')
        exportMenu.deactivateOption("highlightOnly");
      renderingMenu.activateOption();
    };

    self.onHighlight = function (on) {
      if (VERSION == 'remote') return;
        if (on) {
            exportMenu.activateOption("highlightOnly");
        } else {
            exportMenu.deactivateOption("highlightOnly");
        }
    };

    self.onAxisChange = function () {
      if (VERSION == 'remote') return;
        if (Module.getNbAxes() < Module.getNbDims()) {
            exportMenu.activateOption("includeDeleted");
        }
    };

    function loadDataSet() {
      var select = document.getElementById("selectDataset");
      var dataSetName = select.options[select.selectedIndex].value;
      Module.loadData(dataSetName, Server.hostname, Server.port, Server.base);
    }

    self.onImport = function () {
        //try {
          if (VERSION == "local") dataHandler.loadFile(importMenu.getOptionValues());
          else if (VERSION == "remote") loadDataSet();
            vizEvent.initMode();
            vis.onResize();
            Module.fit();
            vis.hideError();
            self.onLoadSuccess();
        /*} catch (err) {
            console.error("ImportError");
        }*/
    };
    onReady();
    return self;
}

Array.range = function (a, b, step) {
    'use strict';
    var A = [];
    if (typeof a === 'number') {
        A[0] = a;
        step = step || 1;
        while (a + step <= b) {
            a += step;
            A[A.length] = a;
        }
    }
    return A;
};

var clearChildren = function (e) {
    "use strict";
    while (e.firstChild) {
        e.removeChild(e.firstChild);
    }
};

function request(path, mimeType, onLoaded) {
    "use strict";
    // "XMLHttpRequest can be used to retrieve any type of data, not just XML"
    var xhr = new XMLHttpRequest();
    xhr.onerror = function () {
        console.log("Error while loading asset file " + path);
    };
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            onLoaded(xhr.responseText);
        }
    };
    xhr.open("GET", path, true);
    xhr.overrideMimeType(mimeType);
    xhr.send(null);
}

/*
 Append a switch-styled checkbox option to an existing HTML list
 Options format:
 { "option1Id": {
 label: "option1Label",
 title: "option1BubbleContent",
 defaultChecked: bool,
 defaultDisabled: bool,
 onclick: func
 },
 "option2Id" ...
 }
 */
var addSwitchOptionToList = function (switchOptions, listHTMLElement) {
    "use strict";
    var option, li, input, label;
    for (option in switchOptions) {
        if (switchOptions.hasOwnProperty(option)) {
            li = document.createElement("li");
            li.title = switchOptions[option].title;
            input = document.createElement("input");
            input.id = option;
            input.type = "checkbox";
            input.disabled = switchOptions[option].defaultDisabled;
            li.addEventListener("click", switchOptions[option].onclick);
            label = document.createElement("label");
            label.for = option;
            label.textContent = switchOptions[option].label;
            li.appendChild(input);
            li.appendChild(label);
            listHTMLElement.appendChild(li);
        }
    }
};

function deactivateOption(existing, provided) {
    "use strict";
    var key;
    if (provided.length === 0) { // ALL
        for (key in existing) {
            if (existing.hasOwnProperty(key)) {
                provided.push(key);
            }
        }
    }
    provided.forEach(function (name) {
        if (existing.hasOwnProperty(name)) {
            var element = document.getElementById(name);
            element.disabled = true;
            element.checked = false;
        } else {
            throw "InvalidOption: " + name;
        }
    });
}

function activateOption(existing, provided) {
    "use strict";
    var key;
    if (provided.length === 0) {
        for (key in existing) {
            if (existing.hasOwnProperty(key)) {
                provided.push(key);
            }
        }
    }
    provided.forEach(function (name) {
        if (existing.hasOwnProperty(name)) {
            var element = document.getElementById(name);
            element.disabled = false;
            element.checked = existing[name].defaultChecked;
        } else {
            throw "InvalidOption: " + name;
        }
    });
}

var makeLayout = function (containerClass, titleClass, tabList) {
    "use strict";
    var container, title, sectionContent;
    container = document.createElement("div");
    container.className = containerClass;
    tabList.forEach(function (tabElement, i) {
        title = document.createElement("h2");
        title.className = titleClass;
        title.textContent = tabElement.title;
        sectionContent = document.createElement("div");
        sectionContent.innerHTML = "<div>" + tabElement.content.join(" ") + "</div>";
        container.appendChild(title);
        container.appendChild(sectionContent);
    });
    return container;
};

var Module, clearChildren, vizEvent, makeAccordion, request, Parser;

/*
 Controller for visualisation toolbar
 */
var makeToolBar = function (parent) {
    "use strict";
    var self = {}, container = document.getElementById("toolBar");

    function formatTool(targetDOMList, contentSource) {
      var itemToDelete = [];
      [].forEach.call(targetDOMList.getElementsByTagName('li'), function (item) {
        var innerIcon = item.firstElementChild;
        if (!innerIcon) return;
        if (innerIcon.hasAttribute("data-content")) {
          var toolName = innerIcon.dataset.content;
          if (!toolName) return;
          if (contentSource.hasOwnProperty(toolName)) {
            var contentItem = contentSource[toolName];
            if (contentItem.hasOwnProperty("state")) {
              item.addEventListener("click", function () {
                  parent.onButtonClick(contentItem.state);
                });
              item.id = contentItem.state.name;
            } else {
              item.addEventListener("click", function () {
                if (toolName in Module)
                    Module["" + toolName]();
              });
              item.id = toolName;
            }
            var altText = contentItem.title;
            if (contentItem.hasOwnProperty("key"))
              altText += " (" + contentItem.key + ")";
            item.dataset.tooltip = altText;
          } else {
            itemToDelete.push(item);
          }
        }
      });
      itemToDelete.forEach(function(item) {
        targetDOMList.removeChild(item);
      });
    }

    function init() {
      var leftList, rightList;
      container.style.visibility = 'visible';
      leftList = container.getElementsByClassName("ragged-left")[0];
      rightList = container.getElementsByClassName("ragged-right")[0];
      formatTool(leftList, toolBarTools.left);
      formatTool(rightList, toolBarTools.right);
    }

    /* Switch any tool button image. Image file name should follow the pattern
     * <state.Enum.name[_on].png> and be located in ./img. Each toolbar tool has
     * at least one icon/image. Part of them require an image change on activation */

    self.buttonSwitch = function (button, toggle) {
        document.getElementById(button).firstElementChild.className = (toggle ? "selected" : "");
    };

    init();
    return self;
};

var CursorPosition = (function () {
    "use strict";
    var self = {},
        cX = 0,
        cY = 0,
        rX = 0,
        rY = 0;
    // Copyright 2006,2007 Bontrager Connection, LLC
    // http://www.willmaster.com/
    // Version: July 28, 2007
    function updateCursorPosition(e) {
        cX = e.pageX;
        cY = e.pageY;
    }

    function updateCursorPositionDocAll() {
        cX = event.clientX;
        cY = event.clientY;
    }

    if (document.all) {
        document.onmousemove = updateCursorPositionDocAll;
    } else {
        document.onmousemove = updateCursorPosition;
    }

    self.getCurrent = function () {
        if (document.documentElement && document.documentElement.scrollTop) {
            rX = document.documentElement.scrollLeft;
            rY = document.documentElement.scrollTop;
        } else if (document.body) {
            rX = document.body.scrollLeft;
            rX = cX;
            rY = document.body.scrollTop;
            rY = cY;
        }
        if (document.all) {
            cX += rX;
            cY += rY;
        }
        return {x: cX, y: cY};
    };

    return self;
}());

/*
 Controller for info bubble and insertion menu, available on click in
 picking and insertion modes
 */
var makePopUp = function (makeContent, rootId) {
    "use strict";
    var self = {},
        container = document.getElementById(rootId);


    function assignPosition(d) {
        var curPos = CursorPosition.getCurrent();
        d.style.left = (curPos.x + 1) + "px";
        d.style.top = (curPos.y + 1) + "px";
    }

    self.hidePopUp = function () {
        container.hidden = true;
    };

    function showPopUp() {
        container.hidden = false;
    }

    self.display = function () {
        self.hidePopUp();
        if (makeContent(container)) {
            showPopUp();
            assignPosition(container);
        }
    };
    return self;
};

/*
 Controller for data table, updated on file import and highlighting
 */

var makeDataTable = function (rootId) {
    "use strict";
    var self = {},
        activated,
        currentLastRow,
        header,
        data,
        container = document.getElementById(rootId),
        table,
        blockSize = 15;

    function fillRow(data, row) {
        data.forEach(function (label, i) {
            row.insertCell(i).innerText = label;
        });
    }

    function addRowsTo(n) {
        if (currentLastRow < n) {
            var rows = data.slice(currentLastRow, Math.min(n, data.length));
            rows.forEach(function (rowData) {
                fillRow(rowData, table.tBodies[0].insertRow(currentLastRow));
                currentLastRow += 1;
            });
        }
    }

    function checkTableScroll() {
        var newLastRow;
        //table = container.firstElementChild;
        if (!data || currentLastRow === data.length || checkTableScroll.atBottom) {
            return;
        }
        checkTableScroll.atBottom = (this.scrollHeight - this.scrollTop <= this.clientHeight);
        if (checkTableScroll.atBottom) {
            // Load more rows
            if (table) {
                newLastRow = Math.min(currentLastRow + blockSize, data.length);
                addRowsTo(newLastRow);
                checkTableScroll.atBottom = false;
            }
        }
    }

    function makeEmptyHTMLTable() {
        table = document.createElement("table");
        table.className = "table table-responsive table-striped table-condensed";
        if (header !== undefined) {
            fillRow(header, table.createTHead().insertRow(0));
        }
        table.createTBody();
        return table;
    }
    
    self.clear = function(verticalSpace) {
        // Clear previous table
        currentLastRow = 0;
        clearChildren(container);
        table = makeEmptyHTMLTable();
        container.appendChild(table);
        self.onResize(verticalSpace);
        container.scrollTop = 0;
        checkTableScroll.atBottom = false;
    
    };

    self.update = function (loadedData, hasHeader, verticalSpace) {
        if (!activated) return;
        if (hasHeader && loadedData.length > 0) {
            header = loadedData[0];
            data = loadedData.slice(1, loadedData.length);
        } else {
            header = undefined;
            data = loadedData;
        }
        self.clear(verticalSpace);
    };

    self.onResize = function (freeVerticalSpace) {
        if (!activated) return;
        if (table !== undefined) {
            var cellHeight, nbRows;
            addRowsTo(1);// Table should hold at least one cell for height measurement
            cellHeight = table.rows[0].firstElementChild.offsetHeight;
            nbRows = Math.round(freeVerticalSpace / cellHeight * 2); // or anything ensure going above the exact filling number
            addRowsTo(nbRows);
        }
    };

    activated = (VERSION == 'local');
    // Data table scroll listener
    container.onscroll = checkTableScroll;
    checkTableScroll.call(container);
    return self;
};

function makeVis(rootId) {
    "use strict";
    var self = {},
        container = document.getElementById(rootId),
        pickingBubble,
        insertionMenu,
        availableDimensions,
        dataTable,
        errorContainer = document.getElementById("errorMessage").firstElementChild,
        errorMessages;
    self.tableData = undefined;

    function initTooltip() {
    }

    function initInsertionMenu() {

        function itemClick(i) {
			console.log(availableDimensions[i].id);
            Module.setDimensionToInsert(availableDimensions[i].id);
            /*availableDimensions.splice(i, 1);*/ // Rebuild at each click?
            insertionMenu.hidePopUp();
        }

        function makeMenuItem(label, onClick) {
            var item = document.createElement("div");
            if (onClick) {
                item.addEventListener("click", onClick, true);
                item.className = "clickable";
            }
            item.textContent = label;
            return item;
        }

        function updateMenu(menuContainer) {
            clearChildren(menuContainer);
            if (availableDimensions.length === 0) {
                menuContainer.appendChild(
                    makeMenuItem("No dimension to insert", undefined)
                );
            } else {
                menuContainer.appendChild(
                    makeMenuItem("Select a dimension", undefined)
                );
                console.log(availableDimensions);
                availableDimensions.forEach(function (dimElem, i) {
                    menuContainer.appendChild(
                        makeMenuItem(dimElem.name, function () {
                            itemClick(i);
                        })
                    );
                });
            }
        }

        function makeInsertionMenu(menuCOntainer) {
            availableDimensions = [];
            var dims = Module.getDeletedDims();
            if (dims.size() > 0) {
                Array.range(0, dims.size() - 1).forEach(function (i) {
                    availableDimensions.push({
                        name: Module.getDimensionLabel(dims.get(i)),
                        id: dims.get(i)
                    });
                });
            }
            updateMenu(menuCOntainer);
            return true;
        }


        insertionMenu = makePopUp(makeInsertionMenu, "insertionMenu");
        availableDimensions = [];
    }

    self.hideError = function () {
        errorContainer.style.visibility = "hidden";
        Module.canvas.hidden = false;
    };

    self.clear = function() {
       dataTable.clear(getTableVerticalSpace());
    };

    self.onError = function (errorCode) {
        var errorKey, content, title, subtitle, suggestions;
        for (errorKey in errorMessages) {
            if (errorMessages.hasOwnProperty(errorKey)) {
                if (Module.Error.hasOwnProperty(errorKey)) {
                    if (Module.Error[errorKey].value === errorCode) {
                        content = errorMessages[errorKey];
                    }
                }
            } else console.log("Error code not found");
        }
        if (content === undefined) {
            content = errorMessages.UNEXPECTED;
        }
        clearChildren(errorContainer);
        title = document.createElement("div");
        title.className = "error-title";
        subtitle = document.createElement("div");
        subtitle.className = "error-subtitle";
        title.textContent = "/!\\ " + content.title;
        subtitle.textContent = content.subtitle;
        errorContainer.appendChild(title);
        errorContainer.appendChild(subtitle);
        errorContainer.appendChild(document.createElement("hr"));
        if (content.suggestions.length !== 0) {
            suggestions = document.createElement("ul");
            content.suggestions.forEach(function (s) {
                var li = document.createElement("li");
                li.textContent = s;
                suggestions.appendChild(li);
            });
            errorContainer.appendChild(suggestions);
        }
        errorContainer.style.visibility = "visible";
        Module.canvas.hidden = true;

    };

    function getTableVerticalSpace() {
        return window.innerHeight - container.offsetHeight;
    }
    
    self.onDataLoaded = function (data, withHeader) {
      if (VERSION == 'local')
        dataTable.update(data, withHeader, getTableVerticalSpace());
    };

    function initModal() {
        var modalBody = document.getElementsByClassName("modal-body")[0];
        request(Path.helpContent, "text/plain", function (f) {
            var lines = f.split("\n");
            modalBody.appendChild(makeLayout("help-content", "stripped-title", Parser.parse(lines)));
        });
    }

    function initErrorMessages() {
        request(Path.errorContent, "text/json", function (f) {
            errorMessages = JSON.parse(f);
        });
    }

    self.onResize = function () {
        //Module.setCanvasSize(canvasContainer.clientWidth, canvasContainer.clientHeight);
        //Module.canvas.width = self.canvasContainer.clientWidth;
        //Module.canvas.height = self.canvasContainer.clientHeight;
        // https://www.khronos.org/webgl/wiki/HandlingHighDPI
        Module.canvas.style.width = self.canvasContainer.clientWidth + "px";
        Module.canvas.style.height = self.canvasContainer.clientHeight + "px";
        var devicePixelRatio = window.devicePixelRatio || 1;
        Module.canvas.width = self.canvasContainer.clientWidth * devicePixelRatio;
        Module.canvas.height = self.canvasContainer.clientHeight * devicePixelRatio;
        
        Module.onReshape(Module.canvas.width, Module.canvas.height);
        if (VERSION == 'local')
          dataTable.onResize(getTableVerticalSpace());
    };

    function makeBubbleInfo(container) {
    
        function formatPickEntry(label, value) {
           return "<b>" + label + "</b>: " + format(value);
        }
    
        function formatPickPercentage(value) {
           return formatPickEntry("highlighted", value) + "%";
        }

        function format(number) {
            return (Number.isInteger(number) ? number : number.toFixed(2));
        }

        var labels = ["weight", "min", "max", "avg"], // Non-categorical nodes
            info = Module.getNumericalInfo(),
            category = Module.getCategory(),
            span = document.createElement("span"),
            isValid;
        clearChildren(container);
        
        var isCategoricalNode = category.length > 0;
        var isEdge = !isCategoricalNode && info.size() <= 2;
        var isRegularNode = info.size() >= labels.length;
        var hasPercentage = (!isCategoricalNode && info.size() == 2) || info.size() == labels.length + 1;
        
        var htmlLines = [];
        
        // First case: categorical nodes
        if (isCategoricalNode) {
           htmlLines.push("<b>" + category + "</b>");
        }
        if (isEdge || isCategoricalNode) {
           htmlLines.push(formatPickEntry("weight", info.get(0)));
        }
        if (isRegularNode) {
           labels.forEach(function (label, index) {
             htmlLines.push(formatPickEntry(label, info.get(index)));
           });
        }
        if (hasPercentage)
           htmlLines.push(formatPickPercentage(info.get(isRegularNode ? labels.length : 1)));
           
        span.innerHTML = htmlLines.join("<br/>");
        container.appendChild(span);
       
        return isEdge || isRegularNode || isCategoricalNode;
    }


    function onReady() {
        self.canvasContainer = document.getElementById("canvasContainer");
        self.canvasContainer.onresize = self.onResize;
        // Rebinding emscripten GLFW window listeners to canvas
        window.removeEventListener("keypress", GLFW.onKeyPress, true);
        window.removeEventListener("keydown", GLFW.onKeydown, true);
        window.removeEventListener("keyup", GLFW.onKeyup, true);
        Module.canvas.addEventListener("keypress", GLFW.onKeyPress, true);
        Module.canvas.addEventListener("keydown", GLFW.onKeydown, true);
        Module.canvas.addEventListener("keyup", GLFW.onKeyup, true);
        // Adding missing listeners
        Module.canvas.addEventListener("mouseup", GLFW.onMouseButtonUp, true);
        // Prevent default event for meta keys Ctrl and Shift
        Module.canvas.onkeydown = function (e) {
            e = e || window.event;
            if (e.ctrlKey || e.shiftKey) {
                e.preventDefault();
                e.stopPropagation();
            }
        };
        self.toolBar = makeToolBar(self);
        initModal();
        initTooltip();
        initInsertionMenu();
        // Creating all sub-object, shallow if unused in current version
        // (typically, inactivated in remote/client version)
        pickingBubble = makePopUp(makeBubbleInfo, "infoBubble");
        if (VERSION === 'local')
          dataTable = makeDataTable("resultTable");
        initErrorMessages();
        self.onResize();
    }


    self.setCursor = function (mode) {
        switch (mode) {
            case vizEvent.Enum.INVERT_AXIS:
                Module.canvas.style.cursor = "row-resize";
                break;
            case vizEvent.Enum.MOVE_AXIS:
                Module.canvas.style.cursor = "ew-resize";
                break;
            case vizEvent.Enum.DELETE_AXIS:
                Module.canvas.style.cursor = "pointer";
                break;
            case vizEvent.Enum.INSERT_DIMENSION:
                Module.canvas.style.cursor = "crosshair";
                break;
            case vizEvent.Enum.PICKER:
                Module.canvas.style.cursor = "help";
                break;
            default:
                Module.canvas.style.cursor = "default";
                break;
        }
    };

    self.flushPreviousMode = function () {
        Module.canvas.removeEventListener("click", pickingBubble.display);
        Module.canvas.removeEventListener("click", insertionMenu.display);
        insertionMenu.hidePopUp();
        pickingBubble.hidePopUp();
    };

    self.onButtonClick = function (newMode) {
        // Only called for mode switch, newMode cannot be a highlighting event,
        // center, fit, or help click
        self.flushPreviousMode();
        if (newMode) {
            if (vizEvent.currentState.value === newMode.value) {
                Module.changeMode(vizEvent.Enum.DEFAULT.value);
            } else {
                Module.changeMode(newMode.value); // compatible w/ Module.State
                if (newMode === vizEvent.Enum.PICKER) {
                    Module.canvas.addEventListener("click", pickingBubble.display);
                } else if (newMode === vizEvent.Enum.INSERT_DIMENSION) {
                    Module.canvas.addEventListener("click", insertionMenu.display);
                }
            }
            self.setCursor(vizEvent.currentState);
        }
    };

    onReady();
    return self;
}

var VERSION = "local"; 

var toolBarTools = {
  left : {
    pan: {
      title: "Exploration",
      key: "Space",
      state: vizEvent.Enum.DEFAULT
    },
    scale: {
      title: "Scale",
      key: "S",
      state: vizEvent.Enum.SCALE
    },
    delete: {
      title: "Delete",
      key: "T",
      state: vizEvent.Enum.DELETE_AXIS
    },
    insert: {
      title: "Insert",
      key: "W",
      state: vizEvent.Enum.INSERT_DIMENSION
    },
    invert: {
      title: "Inversion",
      key: "I",
      state: vizEvent.Enum.INVERT_AXIS
    },
    move: {
      title: "Move",
      key: "D",
      state: vizEvent.Enum.MOVE_AXIS
    },
    filter: {
      title: "Filter",
      key: "Y",
      state: vizEvent.Enum.FILTER
    },
    picker: {
      title: "Info picker",
      key: "P",
      state: vizEvent.Enum.PICKER
    }
  },
    right : {
      center : {
        title: "Center",
        key: "C"
      },
      fit: {
        title: "Fit",
        key: "F"
      },
      help: {
        title: "Help"
      }
    }
  }

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIl9jcHBDYWxiYWNrcy5qcyIsImRhdGFIYW5kbGVyLmpzIiwiaGVscEZpbGVQYXJzZXIuanMiLCJsb2FkLmpzIiwic2lkZWJhci5qcyIsInV0aWxzLmpzIiwidmlzdWFsaXphdGlvbi5qcyIsImxvY2FsLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDekdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDcExBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUMzRkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ25FQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUMxV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDbklBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDL2hCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoidWkuanMiLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgdmlzLCBzaWRlQmFyLCBkYXRhSGFuZGxlcjtcblxuLypcbiBUaGlzIG9iamVjdCdzIGF0dHJpYnV0ZXMgYmVsb25naW5nIHRvIFN0YXRlIGVudW0gZGVmaW5lZCBpbiBWaXN1YWxpemF0aW9uLmNwcFxuIGhhdmUgdG8gY29ycmVzcG9uZCB0byB0aGUgc2FtZSB2YWx1ZXMgaW4gYm90aCBKUyBhbmQgQysrIGNvZGUgYXMgdGhlc2VcbiBzdGF0ZXMgd2lsbCBiZSBwYXNzZWQgZnJvbSBjb21waWxlZCBDKysgdmlhIGV2ZW50Q2FsbGJhY2soKSBzb1xuIHRoYXQgdGhlIFVJIHJlZmxlY3RzIG1vZGUgY2hhbmdlLlxuICovXG5cbnZhciB2aXpFdmVudCA9IHt9O1xudml6RXZlbnQuRW51bSA9IHsgLy9UT0RPOiBkaXZpZGUgZXZlbnQgaW50byBtb2RlcyBhbmQgb3RoZXJzP1xuICAgIEhJR0hMSUdIVF9PTjoge25hbWU6IFwiaGlnaGxpZ2h0T25cIiwgdmFsdWU6IDF9LFxuICAgIEhJR0hMSUdIVF9PRkY6IHtuYW1lOiBcImhpZ2hsaWdodE9mZlwiLCB2YWx1ZTogMn0sXG4gICAgREVGQVVMVDoge25hbWU6IFwiZGVmYXVsdFwiLCB2YWx1ZTogM30sXG4gICAgSU5WRVJUX0FYSVM6IHtuYW1lOiBcImludmVyc2lvblwiLCB2YWx1ZTogNH0sXG4gICAgTU9WRV9BWElTOiB7bmFtZTogXCJtb3ZlQXhpc1wiLCB2YWx1ZTogNX0sXG4gICAgREVMRVRFX0FYSVM6IHtuYW1lOiBcImRlbGV0ZUF4aXNcIiwgdmFsdWU6IDZ9LFxuICAgIElOU0VSVF9ESU1FTlNJT046IHtuYW1lOiBcImluc2VydERpbWVuc2lvblwiLCB2YWx1ZTogN30sXG4gICAgU0NBTEU6IHtuYW1lOiBcInNjYWxlXCIsIHZhbHVlOiA4fSxcbiAgICBGSUxURVI6IHtuYW1lOiBcImZpbHRlclwiLCB2YWx1ZTogOX0sXG4gICAgUElDS0VSOiB7bmFtZTogXCJidWJibGVJbmZvXCIsIHZhbHVlOiAxMH0sXG4gICAgSEVMUDoge25hbWU6IFwiaGVscFwiLCB2YWx1ZTogMTF9LFxuICAgIEFYSVNfQ0hBTkdFOiB7bmFtZTogXCJheGlzQ2hhbmdlXCIsIHZhbHVlOiAxMn1cbn07XG5cbi8qIFN0YXJ0aW5nIG1vZGUgKi9cbnZpekV2ZW50LmluaXRNb2RlID0gZnVuY3Rpb24gKCkge1xuICAgIFwidXNlIHN0cmljdFwiO1xuICAgIGlmICh2aXpFdmVudC5jdXJyZW50U3RhdGUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICB2aXMudG9vbEJhci5idXR0b25Td2l0Y2godml6RXZlbnQuY3VycmVudFN0YXRlLm5hbWUsIGZhbHNlKTtcbiAgICB9XG4gICAgdml6RXZlbnQuc3dpdGNoTW9kZSh2aXpFdmVudC5FbnVtLkRFRkFVTFQpO1xuICAgIHZpcy50b29sQmFyLmJ1dHRvblN3aXRjaCh2aXpFdmVudC5FbnVtLkRFRkFVTFQubmFtZSwgdHJ1ZSk7XG59O1xuXG52aXpFdmVudC5zd2l0Y2hNb2RlID0gZnVuY3Rpb24gKG5ld1N0YXRlKSB7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG4gICAgaWYgKG5ld1N0YXRlICE9PSB2aXpFdmVudC5FbnVtLkhJR0hMSUdIVF9PRkYgJiZcbiAgICAgICAgbmV3U3RhdGUgIT09IHZpekV2ZW50LkVudW0uSElHSExJR0hUX09OICYmXG4gICAgICAgIG5ld1N0YXRlICE9PSB2aXpFdmVudC5FbnVtLkhFTFApIHtcbiAgICAgICAgdml6RXZlbnQuY3VycmVudFN0YXRlID0gbmV3U3RhdGU7XG4gICAgfVxuICAgIHZpcy5zZXRDdXJzb3Iodml6RXZlbnQuY3VycmVudFN0YXRlKTtcbn07XG5cbnZpekV2ZW50LmdldE5hbWUgPSBmdW5jdGlvbiAodmFsdWUpIHtcbiAgICBcInVzZSBzdHJpY3RcIjtcbiAgICB2YXIga2V5O1xuICAgIGZvciAoa2V5IGluIHZpekV2ZW50LkVudW0pIHtcbiAgICAgICAgaWYgKHZpekV2ZW50LkVudW0uaGFzT3duUHJvcGVydHkoa2V5KSkge1xuICAgICAgICAgICAgaWYgKHZpekV2ZW50LkVudW1ba2V5XS52YWx1ZSA9PT0gdmFsdWUpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdml6RXZlbnQuRW51bVtrZXldLm5hbWU7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG4gICAgY29uc29sZS5sb2coXCJDb3VsZCBub3QgZmluZCBcIiArIHZhbHVlKTtcbn07XG5cbnZpekV2ZW50LmdldEVudW0gPSBmdW5jdGlvbiAodmFsdWUpIHtcbiAgICBcInVzZSBzdHJpY3RcIjtcbiAgICB2YXIga2V5O1xuICAgIGZvciAoa2V5IGluIHZpekV2ZW50LkVudW0pIHtcbiAgICAgICAgaWYgKHZpekV2ZW50LkVudW0uaGFzT3duUHJvcGVydHkoa2V5KSkge1xuICAgICAgICAgICAgaWYgKHZpekV2ZW50LkVudW1ba2V5XS52YWx1ZSA9PT0gdmFsdWUpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdml6RXZlbnQuRW51bVtrZXldO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuICAgIGNvbnNvbGUubG9nKFwiQ291bGQgbm90IGZpbmQgXCIgKyB2YWx1ZSk7XG59O1xuXG5mdW5jdGlvbiBldmVudENhbGxiYWNrKGV2ZW50VmFsdWUpIHtcbiAgICBcInVzZSBzdHJpY3RcIjtcbiAgICB2YXIgcGFzdEV2ZW50LCBuZXdFdmVudDtcbiAgICBzd2l0Y2ggKGV2ZW50VmFsdWUpIHtcbiAgICAgICAgY2FzZSB2aXpFdmVudC5FbnVtLkhJR0hMSUdIVF9PTi52YWx1ZTpcbiAgICAgICAgICAgIHNpZGVCYXIub25IaWdobGlnaHQodHJ1ZSk7XG4gICAgICAgICAgICBkYXRhSGFuZGxlci5mZXRjaERhdGEoKTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlIHZpekV2ZW50LkVudW0uSElHSExJR0hUX09GRi52YWx1ZTpcbiAgICAgICAgICAgIHNpZGVCYXIub25IaWdobGlnaHQoZmFsc2UpO1xuICAgICAgICAgICAgZGF0YUhhbmRsZXIuZmV0Y2hEYXRhKCk7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSB2aXpFdmVudC5FbnVtLkFYSVNfQ0hBTkdFLnZhbHVlOlxuICAgICAgICAgICAgZGF0YUhhbmRsZXIuZmV0Y2hEYXRhKCk7XG4gICAgICAgICAgICBzaWRlQmFyLm9uQXhpc0NoYW5nZSgpO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICBpZiAoZXZlbnRWYWx1ZSkge1xuICAgICAgICAgICAgICAgIHBhc3RFdmVudCA9IHZpekV2ZW50LmN1cnJlbnRTdGF0ZTtcbiAgICAgICAgICAgICAgICBuZXdFdmVudCA9IHZpekV2ZW50LmdldEVudW0oZXZlbnRWYWx1ZSk7XG4gICAgICAgICAgICAgICAgdml6RXZlbnQuc3dpdGNoTW9kZShuZXdFdmVudCk7XG4gICAgICAgICAgICAgICAgaWYgKHZpekV2ZW50LmN1cnJlbnRTdGF0ZSAhPT0gcGFzdEV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgIHZpcy50b29sQmFyLmJ1dHRvblN3aXRjaChwYXN0RXZlbnQubmFtZSwgZmFsc2UpO1xuICAgICAgICAgICAgICAgICAgICB2aXMudG9vbEJhci5idXR0b25Td2l0Y2gobmV3RXZlbnQubmFtZSwgdHJ1ZSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHZpcy5zZXRDdXJzb3IobmV3RXZlbnQpO1xuICAgICAgICAgICAgfVxuICAgIH1cbn1cblxuZnVuY3Rpb24gZXJyb3JDYWxsYmFjayhlcnJvckNvZGUpIHtcbiAgICBcInVzZSBzdHJpY3RcIjtcbiAgICB2aXMub25FcnJvcihlcnJvckNvZGUpO1xufVxuIiwidmFyIHNpZGVCYXIsIE1vZHVsZSwgdmlzO1xuXG52YXIgbWFrZURhdGFIYW5kbGVyID0gZnVuY3Rpb24gKCkge1xuICAgICd1c2Ugc3RyaWN0JztcbiAgICB2YXIgZGF0YU9wdGlvbnMgPSB7fSxcbiAgICAgICAgZmlsZW5hbWUsXG4gICAgICAgIGV4cG9ydExpbmssXG4gICAgICAgIHNlbGYgPSB7fSxcbiAgICAgICAgZmlsZURhdGEgPSBbXSxcbiAgICAgICAgaGlnaGxpZ2h0RGF0YSA9IFtdO1xuXG4gICAgZnVuY3Rpb24gaGFuZGxlRmlsZVJlYWRBYm9ydCgpIHtcbiAgICAgICAgdmlzLm9uRXJyb3IoTW9kdWxlLkVycm9yLk5PX0ZJTEUudmFsdWUpO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGhhbmRsZUZpbGVSZWFkRXJyb3IoKSB7XG4gICAgICAgIHZpcy5vbkVycm9yKE1vZHVsZS5FcnJvci5OT19GSUxFLnZhbHVlKTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiB3cml0ZUZpbGUoZXZ0KSB7XG4gICAgICAgIHZhciBmaWxlU3RyaW5nID0gZXZ0LnRhcmdldC5yZXN1bHQ7IC8vIE9idGFpbiB0aGUgZmlsZSBjb250ZW50cywgd2hpY2ggd2FzIHJlYWQgaW50byBtZW1vcnkuXG4gICAgICAgIC8vZXZ0LnRhcmdldCBpcyBhIEZpbGVSZWFkZXIgb2JqZWN0LCBub3QgYSBGaWxlIG9iamVjdDsgc28gd2luZG93LlVSTC5jcmVhdGVPYmplY3QoZXZ0LnRhcmdldCkgd29uJ3Qgd29yayBoZXJlIVxuICAgICAgICBpZiAoZXZ0LnRhcmdldC5yZWFkeVN0YXRlID09PSBGaWxlUmVhZGVyLkRPTkUpIHsgLy8gRE9ORSA9PSAyXG4gICAgICAgICAgICBGUy53cml0ZUZpbGUoaW1wb3J0ZWRGaWxlUGF0aCwgZmlsZVN0cmluZywge1xuICAgICAgICAgICAgICAgIGVuY29kaW5nOiAndXRmOCcsXG4gICAgICAgICAgICAgICAgZmxhZ3M6ICd3KydcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJGaW5pc2hlZCBsb2FkaW5nIGZpbGUsIHBsZWFzZSBwcm9jZWVkIHRvIGltcG9ydC5cIik7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIkZpbGUgaXMgbm90IHJlYWR5IHRvIGJlIHdyaXR0ZW5cIik7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBmdW5jdGlvbiBkaXNwbGF5RmlsZShldnQpIHtcbiAgICAgICAgdmFyIGZpbGVTdHJpbmcgPSBldnQudGFyZ2V0LnJlc3VsdDsgLy8gT2J0YWluIHRoZSBmaWxlIGNvbnRlbnRzLCB3aGljaCB3YXMgcmVhZCBpbnRvIG1lbW9yeS5cbiAgICAgICAgdmlzLmhpZGVFcnJvcigpO1xuICAgICAgICBpZiAoZXZ0LnRhcmdldC5yZWFkeVN0YXRlID09PSBGaWxlUmVhZGVyLkRPTkUpIHsgLy8gRE9ORSA9PSAyXG4gICAgICAgICAgICBmaWxlRGF0YSA9IHBhcnNlQ1NWKGZpbGVTdHJpbmcpO1xuICAgICAgICAgICAgaWYgKGZpbGVEYXRhICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIlBhcnNlZCBcIiArIGZpbGVEYXRhLmxlbmd0aCArIFwiIHJvd3NcIik7XG4gICAgICAgICAgICAgICAgdXBkYXRlRXhwb3J0VVJJKGV4cG9ydFRvQ1NWKGZpbGVEYXRhKSk7XG4gICAgICAgICAgICAgICAgdmlzLm9uRGF0YUxvYWRlZChmaWxlRGF0YSwgZmFsc2UpO1xuICAgICAgICAgICAgICAgIHZpcy5vblJlc2l6ZSgpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB2aXMuY2xlYXIoKTtcbiAgICAgICAgICAgICAgICB2aXMub25FcnJvcihNb2R1bGUuRXJyb3IuREVMSU1JVEVSLnZhbHVlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRmlsZSBpcyBub3QgcmVhZHkgdG8gYmUgZGlzcGxheWVkXCIpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgc2VsZi5zdGFydEZpbGVSZWFkID0gZnVuY3Rpb24gKGZpbGVPYmplY3QpIHtcbiAgICAgICAgdmFyIHJlYWRlciA9IG5ldyBGaWxlUmVhZGVyKCk7XG4gICAgICAgIHJlYWRlci5vbmxvYWRlbmQgPSBkaXNwbGF5RmlsZTtcbiAgICAgICAgcmVhZGVyLmFib3J0ID0gaGFuZGxlRmlsZVJlYWRBYm9ydDtcbiAgICAgICAgcmVhZGVyLm9uZXJyb3IgPSBoYW5kbGVGaWxlUmVhZEVycm9yO1xuICAgICAgICBpZiAoZmlsZU9iamVjdCkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJTdGFydGluZyBuZXcgZmlsZSByZWFkaW5nXCIpO1xuICAgICAgICAgICAgZmlsZW5hbWUgPSBmaWxlT2JqZWN0Lm5hbWU7XG4gICAgICAgICAgICBzaWRlQmFyLnNldEZpbGVuYW1lKGZpbGVPYmplY3QubmFtZSk7XG4gICAgICAgICAgICByZWFkZXIucmVhZEFzVGV4dChmaWxlT2JqZWN0KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGZpbGVuYW1lID0gdW5kZWZpbmVkO1xuICAgICAgICAgICAgdmlzLm9uRXJyb3IoTW9kdWxlLkVycm9yLk5PX0ZJTEUudmFsdWUpO1xuICAgICAgICB9XG4gICAgfTtcblxuICAgIC8qXG4gICAgICogR3Vlc3MgZGVsaW1pdGVyIGFuZCByZXR1cm4gbWF0cml4IG9mIHZhbHVlc1xuICAgICAqL1xuICAgIGZ1bmN0aW9uIHBhcnNlQ1NWKGlucHV0U3RyaW5nKSB7XG4gICAgICAgIGlmIChpbnB1dFN0cmluZy5sZW5ndGggPT09IDApIHJldHVybiB1bmRlZmluZWQ7XG4gICAgICAgIHZhciBsaW5lcyA9IGlucHV0U3RyaW5nXG4gICAgICAgICAgICAucmVwbGFjZSgvJi9nLCAnJmFtcDsnKVxuICAgICAgICAgICAgLnJlcGxhY2UoLzwvZywgJyZsdDsnKVxuICAgICAgICAgICAgLnJlcGxhY2UoLz4vZywgJyZndDsnKVxuICAgICAgICAgICAgLnJlcGxhY2UoL1wiL2csICcnKVxuICAgICAgICAgICAgLnNwbGl0KC9bXFxuXFxyXS8pXG4gICAgICAgICAgICAuZmlsdGVyKGZ1bmN0aW9uIChuKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIChuLmxlbmd0aCA+IDApO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIGlmIChsaW5lcy5sZW5ndGggPT09IDApIHJldHVybiB1bmRlZmluZWQ7XG4gICAgICAgIC8vIEZpbmQgZGVsaW1pdGVyXG4gICAgICAgIHZhciBmb3VuZFNwYWNlID0gZmFsc2U7XG4gICAgICAgIHZhciBkZWxpbWl0ZXIgPSBudWxsO1xuICAgICAgICB2YXIgZm91bmQgPSBsaW5lc1swXS5zcGxpdCgnJykuc29tZShmdW5jdGlvbiAoY2hhcikge1xuICAgICAgICAgICAgaWYgKGNoYXIgPT0gJyAnIHx8IGNoYXIgPT0gJyZuYnNwOycgfHwgY2hhciA9PSAnJmVuc3A7JyB8fCBjaGFyID09ICcmZW1zcDsnKSB7XG4gICAgICAgICAgICAgICAgZm91bmRTcGFjZSA9IHRydWU7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKGNoYXIgPT0gJ1xcdCcpIHtcbiAgICAgICAgICAgICAgICBkZWxpbWl0ZXIgPSAnXFx0JztcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoY2hhciA9PSAnLCcpIHtcbiAgICAgICAgICAgICAgICBkZWxpbWl0ZXIgPSAnLCc7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKGNoYXIgPT0gJzsnKSB7XG4gICAgICAgICAgICAgICAgZGVsaW1pdGVyID0gJzsnO1xuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9KTtcbiAgICAgICAgaWYgKCFmb3VuZCAmJiBmb3VuZFNwYWNlKSB7XG4gICAgICAgICAgICBkZWxpbWl0ZXIgPSAnICc7XG4gICAgICAgICAgICBmb3VuZCA9IHRydWU7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGZvdW5kKVxuICAgICAgICAgICAgcmV0dXJuIGxpbmVzLm1hcChmdW5jdGlvbiAobGluZSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBsaW5lLnNwbGl0KGRlbGltaXRlcik7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICB2aXMub25FcnJvcihNb2R1bGUuRXJyb3IuREVMSU1JVEVSLnZhbHVlKTtcbiAgICAgICAgICAgIHRocm93IFwiQ291bGQgbm90IGRldGVybWluZSBkZWxpbWl0ZXJcIjtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIHVwZGF0ZUV4cG9ydFVSSShibG9iKSB7XG4gICAgICAgIC8vIElmIHdlIGFyZSByZXBsYWNpbmcgYSBwcmV2aW91c2x5IGdlbmVyYXRlZCBmaWxlIHdlIG5lZWQgdG9cbiAgICAgICAgLy8gbWFudWFsbHkgcmV2b2tlIHRoZSBvYmplY3QgVVJMIHRvIGF2b2lkIG1lbW9yeSBsZWFrcy5cbiAgICAgICAgaWYgKGV4cG9ydExpbmsgIT09IG51bGwpIHtcbiAgICAgICAgICAgIHdpbmRvdy5VUkwucmV2b2tlT2JqZWN0VVJMKGRhdGFPcHRpb25zLmV4cG9ydExpbmspO1xuICAgICAgICB9XG4gICAgICAgIGV4cG9ydExpbmsgPSB3aW5kb3cuVVJMLmNyZWF0ZU9iamVjdFVSTChcbiAgICAgICAgICAgIG5ldyBCbG9iKFtibG9iXSwge3R5cGU6ICd0ZXh0L2Nzdid9KVxuICAgICAgICApO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGV4cG9ydFRvQ1NWKGRhdGEsIGhlYWRlcikge1xuICAgICAgICAvL2luZGV4ID09IDAgJiYgaGVhZGVyPyBcImRhdGE6dGV4dC9jc3Y7Y2hhcnNldD11dGYtOCxcIiArXG4gICAgICAgIHZhciBsaW5lQXJyYXkgPSBbXTtcbiAgICAgICAgZGF0YS5mb3JFYWNoKGZ1bmN0aW9uIChsaW5lVmFsdWVzLCBpbmRleCkge1xuICAgICAgICAgICAgdmFyIGxpbmUgPSBsaW5lVmFsdWVzLmpvaW4oXCIsXCIpO1xuICAgICAgICAgICAgbGluZUFycmF5LnB1c2gobGluZSk7XG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm4gbGluZUFycmF5LmpvaW4oXCJcXG5cIik7XG4gICAgfVxuXG4gICAgLypcbiAgICAgSW5pdGlhbCBmaWxlIGxvYWRpbmdcbiAgICAgKi9cbiAgICBzZWxmLmxvYWRGaWxlID0gZnVuY3Rpb24gKGZvcm1PcHRpb25zKSB7XG4gICAgICAgIGlmIChmaWxlRGF0YSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICB2aXMub25EYXRhTG9hZGVkKGZpbGVEYXRhLCBmb3JtT3B0aW9ucy53aXRoQ29sdW1uTGFiZWxzKTtcbiAgICAgICAgICAgIHNpZGVCYXIub25SZWxvYWQoKTtcbiAgICAgICAgICAgIE1vZHVsZS5sb2FkRGF0YShcbiAgICAgICAgICAgICAgICBleHBvcnRUb0NTVihmaWxlRGF0YSksXG4gICAgICAgICAgICAgICAgZmFsc2UsXG4gICAgICAgICAgICAgICAgZm9ybU9wdGlvbnMub3JkaW5hbERpbXMsXG4gICAgICAgICAgICAgICAgZm9ybU9wdGlvbnMubm9taW5hbERpbXMsXG4gICAgICAgICAgICAgICAgZm9ybU9wdGlvbnMud2l0aENvbHVtbkxhYmVscyxcbiAgICAgICAgICAgICAgICBmb3JtT3B0aW9ucy53aXRoUm93TGFiZWxzLFxuICAgICAgICAgICAgICAgIGZvcm1PcHRpb25zLm5iQ2x1c3RlcnMsXG4gICAgICAgICAgICAgICAgZm9ybU9wdGlvbnMuY2x1c3RlcmluZ1R5cGUpO1xuICAgICAgICAgICAgdmlzLm9uUmVzaXplKCk7XG4gICAgICAgIH1cbiAgICB9O1xuXG4gICAgc2VsZi5nZXRFeHBvcnRVUkkgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiBleHBvcnRMaW5rO1xuICAgIH07XG5cbiAgICAvKlxuICAgICBEYXRhIHJlLWxvYWRpbmcgZm9yIGF4aXMgZWRpdGlvbiBhbmQgaGlnaGxpZ2h0aW5nIG9wZXJhdGlvblxuICAgICBCYXNlIGRhdGEgaXMgZXhwb3J0ZWQgZnJvbSB0aGUgY29yZSBjb21wb25lbnQgYXMgYSBmaWxlIHJlYWQvd3JpdGVcbiAgICAgT3B0aW9uczogeyBoaWdobGlnaHRPbmx5LCBpbmNsdWRlRGVsZXRlZCB9XG4gICAgICovXG4gICAgc2VsZi5mZXRjaERhdGEgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmIChmaWxlbmFtZSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgdmFyIGRhdGFCbG9iLCBvcHRpb25zLCB0YXJnZXQ7XG4gICAgICAgIG9wdGlvbnMgPSBzaWRlQmFyLmdldEV4cG9ydE9wdGlvbnMoKTtcbiAgICAgICAgZGF0YUJsb2IgPSBNb2R1bGUuZXhwb3J0RGF0YUFzU3RyaW5nKG9wdGlvbnMuaGlnaGxpZ2h0T25seSwgb3B0aW9ucy5pbmNsdWRlRGVsZXRlZCk7XG4vLyAgICAgIGRhdGFCbG9iID0gRlMucmVhZEZpbGUoZXhwb3J0RmlsZVBhdGgsIHtlbmNvZGluZzogJ3V0ZjgnfSk7XG4gICAgICAgIHRhcmdldCA9IHBhcnNlQ1NWKGRhdGFCbG9iLCBkYXRhT3B0aW9ucy5kZWxpbWl0ZXIsIHRhcmdldCk7XG4gICAgICAgIHZpcy5vbkRhdGFMb2FkZWQodGFyZ2V0LCBkYXRhT3B0aW9ucy53aXRoQ29sdW1uTGFiZWxzKTtcbiAgICAgICAgdXBkYXRlRXhwb3J0VVJJKGRhdGFCbG9iKTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIHNlbGY7XG59O1xuIiwidmFyIFBhcnNlciA9IChmdW5jdGlvbiAoKSB7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG4gICAgdmFyIHNlbGYgPSB7fSxcbiAgICAgICAgICAgIGVsZW1lbnRzID0gW10sXG4gICAgICAgICAgICBjdXJyZW50RWxlbWVudCxcbiAgICAgICAgICAgIGN1cnJlbnRJdGVtTGlzdCA9IFtdLFxuICAgICAgICAgICAgY3VycmVudEl0ZW1XaWR0aDtcblxuICAgICAgICBmdW5jdGlvbiB0cnlUaXRsZShsaW5lKSB7XG4gICAgICAgICAgICB2YXIgaXNUaXRsZSA9IC8oI3sxLDJ9KShbXiN7fV0rKSguKikvLmV4ZWMobGluZSk7XG4gICAgICAgICAgICBpZiAoaXNUaXRsZSkge1xuICAgICAgICAgICAgICAgIGlmIChpc1RpdGxlWzFdLmxlbmd0aCA9PT0gMSkge1xuICAgICAgICAgICAgICAgICAgICBpZiAoY3VycmVudEVsZW1lbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsZW1lbnRzLnB1c2goY3VycmVudEVsZW1lbnQpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnRFbGVtZW50ID0ge307XG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnRFbGVtZW50LnRpdGxlID0gaXNUaXRsZVsyXTtcbiAgICAgICAgICAgICAgICAgICAgY3VycmVudEVsZW1lbnQuY29udGVudCA9IFtdO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnRFbGVtZW50LmNvbnRlbnQucHVzaChcIjxoMyBjbGFzcz0naGVscC1zdWItdGl0bGUnPlwiICsgaXNUaXRsZVsyXSArIFwiPC9oMz5cIik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGlzVGl0bGUgIT09IHVuZGVmaW5lZDtcbiAgICAgICAgfVxuXG4gICAgICAgIGZ1bmN0aW9uIHRyeUl0ZW1MaXN0U3RhcnQobGluZSkge1xuICAgICAgICAgICAgdmFyIGlzSXRlbUxpc3RTdGFydCA9IC8tKFswLTldKS0vLmV4ZWMobGluZSk7XG4gICAgICAgICAgICBpZiAoaXNJdGVtTGlzdFN0YXJ0KSB7XG4gICAgICAgICAgICAgICAgY3VycmVudEl0ZW1XaWR0aCA9IHBhcnNlSW50KGlzSXRlbUxpc3RTdGFydFsxXSk7XG4gICAgICAgICAgICAgICAgY3VycmVudEl0ZW1MaXN0LnB1c2goXCI8ZGl2IGNsYXNzPSdhbGlnbmVkLWNvbnRhaW5lcic+XCIpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGlzSXRlbUxpc3RTdGFydCAhPT0gdW5kZWZpbmVkO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gdHJ5SXNJdGVtRW5kKGxpbmUpIHtcbiAgICAgICAgICAgIHZhciBpc0l0ZW1MaXN0RW5kID0gLy0tLS8uZXhlYyhsaW5lKTtcbiAgICAgICAgICAgIGlmIChpc0l0ZW1MaXN0RW5kKSB7XG4gICAgICAgICAgICAgICAgY3VycmVudEl0ZW1MaXN0LnB1c2goXCI8L2Rpdj5cIik7XG4gICAgICAgICAgICAgICAgY3VycmVudEl0ZW1MaXN0LmZvckVhY2goZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgICAgICAgICAgY3VycmVudEVsZW1lbnQuY29udGVudC5wdXNoKGUpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIGN1cnJlbnRJdGVtTGlzdCA9IFtdO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGlzSXRlbUxpc3RFbmQgIT09IHVuZGVmaW5lZDtcbiAgICAgICAgfVxuXG4gICAgICAgIGZ1bmN0aW9uIHRyeUl0ZW0obGluZSkge1xuICAgICAgICAgICAgdmFyIGlzSXRlbSA9IC9ePihbXnt9XSspP1xceyhbYS16QS1aMC05LlxcLV0rKVxcfSguKikvLmV4ZWMobGluZSk7XG4gICAgICAgICAgICBpZiAoaXNJdGVtKSB7XG4gICAgICAgICAgICAgICAgY3VycmVudEl0ZW1MaXN0LnB1c2goXCI8ZGl2IGNsYXNzPSdhbGlnbmVkJz5cIik7XG4gICAgICAgICAgICAgICAgY3VycmVudEl0ZW1MaXN0LnB1c2goXCI8aW1nIGNsYXNzPSdoZWxwLWljb24nXCIgK1xuICAgICAgICAgICAgICAgICAgICBcIiBzcmM9J1wiICsgUGF0aC5pbWdGb2xkZXIgKyAgXCIvXCIgKyBpc0l0ZW1bMl0gKyBcIicvPlwiKTtcbiAgICAgICAgICAgICAgICBpZiAoaXNJdGVtWzFdKSB7XG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnRJdGVtTGlzdC5wdXNoKFwiPGg0IGNsYXNzPSdsZWdlbmQtdGl0bGUnPlwiICsgaXNJdGVtWzFdICsgXCI8L2g0PlwiKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYgKGlzSXRlbVszXSkge1xuICAgICAgICAgICAgICAgICAgICBjdXJyZW50SXRlbUxpc3QucHVzaChcIjxwPlwiICsgaXNJdGVtWzNdICsgXCI8L3A+XCIpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBjdXJyZW50SXRlbUxpc3QucHVzaChcIjwvZGl2PlwiKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBpc0l0ZW0gIT09IHVuZGVmaW5lZDtcbiAgICAgICAgfVxuXG4gICAgICAgIGZ1bmN0aW9uIHRyeVBhcihsaW5lKSB7XG4gICAgICAgICAgICB2YXIgaXNQYXIgPSAvXihbXi0jPl0uKykvLmV4ZWMobGluZSk7XG4gICAgICAgICAgICBpZiAoaXNQYXIpIHtcbiAgICAgICAgICAgICAgICBjdXJyZW50RWxlbWVudC5jb250ZW50LnB1c2goXCI8cD5cIiArIGlzUGFyWzFdICsgXCI8L3A+XCIpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGlzUGFyICE9PSB1bmRlZmluZWQ7XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiB0cnlFbXB0eShsaW5lKSB7XG4gICAgICAgICAgICByZXR1cm4gKC9eXFxzKiQvLmV4ZWMobGluZSkgIT09IHVuZGVmaW5lZCk7XG4gICAgICAgIH1cblxuICAgICAgICBzZWxmLnBhcnNlID0gZnVuY3Rpb24gKGZpbGVBc0xpbmVzKSB7XG4gICAgICAgICAgICB2YXIgdHJ5UHJvY2VkdXJlcyA9IFt0cnlFbXB0eSwgdHJ5VGl0bGUsIHRyeUl0ZW1MaXN0U3RhcnQsXG4gICAgICAgICAgICAgICAgdHJ5SXNJdGVtRW5kLCB0cnlJdGVtLCB0cnlQYXJdO1xuICAgICAgICAgICAgZmlsZUFzTGluZXMuZm9yRWFjaChmdW5jdGlvbiAobGluZSkge1xuICAgICAgICAgICAgICAgIHRyeVByb2NlZHVyZXMuc29tZShmdW5jdGlvbiAoZWxlbWVudCkge1xuICAgICAgICAgICAgICAgICAgICBlbGVtZW50KGxpbmUpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIC8qaWYgKCFyZWFjaGVkRW5kKSB7XG4gICAgICAgICAgICAgICAgIHRocm93IChcIlVucmVjb2duaXplZCBsaW5lOiBcIiArIGxpbmUpO1xuICAgICAgICAgICAgICAgICB9Ki9cbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgcmV0dXJuIGVsZW1lbnRzO1xuICAgICAgICB9O1xuXG4gICAgICAgIHJldHVybiBzZWxmO1xuICAgIH0oKSk7XG4iLCIvKmdsb2JhbFxuICAgIG1ha2VEYXRhSGFuZGxlciwgbWFrZVZpcywgbWFrZVNpZGVCYXJcbiovXG52YXIgdmlzLCBzaWRlQmFyLCBkYXRhSGFuZGxlcjtcblxud2luZG93Lm9uRXJyb3IgPSBmdW5jdGlvbiAoKSB7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG4gICAgdmlzLm9uRXJyb3IoXCJTb21ldGhpbmcgYmFkIGhhcHBlbmVkXCIpO1xufTtcblxudmFyIFBhdGggPSB7XG4gIGhlbHBDb250ZW50OiBcIi4vYXNzZXRzL2hlbHAuaGRcIixcbiAgZXJyb3JDb250ZW50OiBcIi4vYXNzZXRzL2Vycm9yTWVzc2FnZXMuanNvblwiLFxuICBpbWdGb2xkZXIgOiBcIi4vYXNzZXRzXCJcbn07XG5cbnZhciBNb2R1bGUgPSB7XG4gICAgXCJhcmd1bWVudHNcIjogW1wiNDAwXCIsIFwiNDAwXCJdLCAvL1RPRE8gYmV0dGVyIGFyZ3VtZW50c1xuICAgICBcInBvc3RSdW5cIjogW2Z1bmN0aW9uICgpIHtcbiAgICAgICBcInVzZSBzdHJpY3RcIjtcbiAgICAgICAgZGF0YUhhbmRsZXIgPSBtYWtlRGF0YUhhbmRsZXIoKTtcbiAgICAgICAgdmlzID0gbWFrZVZpcyhcImNhbnZhc0NvbnRhaW5lclwiKTtcbiAgICAgICAgc2lkZUJhciA9IG1ha2VTaWRlQmFyKCk7XG4gICAgfV0sXG4gICAgXCJwcmludFwiOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIFwidXNlIHN0cmljdFwiO1xuICAgICAgICB2YXIgZWxlbWVudCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwib3V0cHV0XCIpO1xuICAgICAgICBpZiAoZWxlbWVudCkge1xuICAgICAgICAgICAgZWxlbWVudC52YWx1ZSA9IFwiXCI7XG4gICAgICAgIH0gLy8gY2xlYXIgYnJvd3NlciBjYWNoZVxuICAgICAgICByZXR1cm4gZnVuY3Rpb24gKHRleHQpIHtcbiAgICAgICAgICAgIGlmIChhcmd1bWVudHMubGVuZ3RoID4gMSkge1xuICAgICAgICAgICAgICAgIHRleHQgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMpLmpvaW4oXCIgXCIpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY29uc29sZS5sb2codGV4dCk7XG4gICAgICAgICAgICBpZiAoZWxlbWVudCkge1xuICAgICAgICAgICAgICAgIGVsZW1lbnQudmFsdWUgKz0gdGV4dCArIFwiXFxuXCI7XG4gICAgICAgICAgICAgICAgZWxlbWVudC5zY3JvbGxUb3AgPSBlbGVtZW50LnNjcm9sbEhlaWdodDsgLy8gZm9jdXMgb24gYm90dG9tXG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgfSxcbiAgICBcInByaW50RXJyXCI6IGZ1bmN0aW9uICh0ZXh0KSB7XG4gICAgICAgIFwidXNlIHN0cmljdFwiO1xuICAgICAgICBpZiAoYXJndW1lbnRzLmxlbmd0aCA+IDEpIHtcbiAgICAgICAgICAgIHRleHQgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMpLmpvaW4oXCIgXCIpO1xuICAgICAgICB9XG4gICAgICAgIGNvbnNvbGUuZXJyb3IodGV4dCk7XG4gICAgfSxcbiAgICBcImNhbnZhc1wiOiAoZnVuY3Rpb24gKCkge1xuICAgICAgICBcInVzZSBzdHJpY3RcIjtcbiAgICAgICAgdmFyIGNhbnZhcyA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZ2xjYW52YXNcIik7XG4gICAgICAgIC8vIEFzIGEgZGVmYXVsdCBpbml0aWFsIGJlaGF2aW9yLCBwb3AgdXAgYW4gYWxlcnQgd2hlbiB3ZWJnbCBjb250ZXh0IGlzIGxvc3QuIFRvIG1ha2UgeW91clxuICAgICAgICAvLyBhcHBsaWNhdGlvbiByb2J1c3QsIHlvdSBtYXkgd2FudCB0byBvdmVycmlkZSB0aGlzIGJlaGF2aW9yIGJlZm9yZSBzaGlwcGluZyFcbiAgICAgICAgLy8gU2VlIGh0dHA6Ly93d3cua2hyb25vcy5vcmcvcmVnaXN0cnkvd2ViZ2wvc3BlY3MvbGF0ZXN0LzEuMC8jNS4xNS4yXG4gICAgICAgIGNhbnZhcy5hZGRFdmVudExpc3RlbmVyKFwid2ViZ2xjb250ZXh0bG9zdFwiLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgYWxlcnQoXCJXZWJHTCBjb250ZXh0IGxvc3QuIFlvdSB3aWxsIG5lZWQgdG8gcmVsb2FkIHRoZSBwYWdlLlwiKTtcbiAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgfSwgZmFsc2UpO1xuXG4gICAgICAgIHJldHVybiBjYW52YXM7XG4gICAgfSgpKVxufTtcblxud2luZG93Lm9ucmVzaXplID0gZnVuY3Rpb24gKCkge1xuICAgIFwidXNlIHN0cmljdFwiO1xuICAgIHZpcy5vblJlc2l6ZSgpO1xufTtcbiIsInZhciBNb2R1bGUsIGRhdGFIYW5kbGVyLCB2aXMsIGFkZFN3aXRjaE9wdGlvblRvTGlzdCwgdml6RXZlbnQsIGRlYWN0aXZhdGVPcHRpb24sIGFjdGl2YXRlT3B0aW9uO1xuXG52YXIgbWFrZUltcG9ydE1lbnUgPSBmdW5jdGlvbiAoY29udGFpbmVySWQpIHtcbiAgICBcInVzZSBzdHJpY3RcIjtcbiAgICB2YXIgc2VsZiA9IHt9LCBmb3JtID0ge307XG5cbiAgICBmdW5jdGlvbiBoYW5kbGVEcmFnT3ZlcihldnQpIHtcbiAgICAgICAgZXZ0LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICBldnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgZXZ0LmRhdGFUcmFuc2Zlci5kcm9wRWZmZWN0ID0gJ2NvcHknOyAvLyBFeHBsaWNpdGx5IHNob3cgdGhpcyBpcyBhIGNvcHkuXG4gICAgfVxuXG4gICAgZnVuY3Rpb24gaGFuZGxlRHJvcEZpbGVTZWxlY3QoZXZ0KSB7XG4gICAgICAgIGV2dC5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgZXZ0LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIGRhdGFIYW5kbGVyLnN0YXJ0RmlsZVJlYWQoZXZ0LmRhdGFUcmFuc2Zlci5maWxlc1swXSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gaGFuZGxlRmlsZVNlbGVjdCgpIHtcbiAgICAgICAgZGF0YUhhbmRsZXIuc3RhcnRGaWxlUmVhZChmb3JtLmZpbGVCcm93c2VyLmZpbGVzWzBdKTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBvbkNsdXN0ZXJpbmdDaGFuZ2UoKSB7XG4gICAgICAgIHZhciBpc05vbmUgPSAob25DbHVzdGVyaW5nQ2hhbmdlLnNlbGVjdG9yLnZhbHVlID09PSBcIk5PTkVcIik7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiT24gY2x1c3RlcmluZyBjaGFuZ2VcIik7XG4gICAgICAgIG9uQ2x1c3RlcmluZ0NoYW5nZS5ibG9jay5oaWRkZW4gPSBpc05vbmU7XG4gICAgICAgIG9uQ2x1c3RlcmluZ0NoYW5nZS5pbnB1dExpc3QuZm9yRWFjaChmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgZS5yZXF1aXJlZCA9ICFpc05vbmU7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHNlbGYuZ2V0T3B0aW9uVmFsdWVzID0gZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgbmJDbHVzdGVyczogTWF0aC5tYXgoMiwgcGFyc2VJbnQoZm9ybS5uYkNsdXN0ZXJzLnZhbHVlLCAxMCkpLFxuICAgICAgICAgICAgY2x1c3RlcmluZ1R5cGU6IE1vZHVsZS5DbHVzdGVyaW5nW2Zvcm0uY2x1c3RlcmluZ1NlbGVjdG9yLnZhbHVlXSxcbiAgICAgICAgICAgIHdpdGhSb3dMYWJlbHM6IGZvcm0ud2l0aFJvd0xhYmVscy5jaGVja2VkLFxuICAgICAgICAgICAgd2l0aENvbHVtbkxhYmVsczogZm9ybS53aXRoQ29sdW1uTGFiZWxzLmNoZWNrZWQsXG4gICAgICAgICAgICBvcmRpbmFsRGltczogZm9ybS5vcmRpbmFsRGltZW5zaW9ucy52YWx1ZSxcbiAgICAgICAgICAgIG5vbWluYWxEaW1zOiBmb3JtLm5vbWluYWxEaW1lbnNpb25zLnZhbHVlXG4gICAgICAgIH07XG4gICAgfTtcblxuICAgIHNlbGYuc3R5bGVGYWlsZWRJbXBvcnQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGZvcm0uZHJvcFpvbmUuY2xhc3NOYW1lID0gXCJpbnZhbGlkXCI7XG4gICAgfTtcblxuICAgIHNlbGYuaW5pdFN0eWxlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBmb3JtLmRyb3Bab25lLmNsYXNzTmFtZSA9IFwiXCI7XG4gICAgfTtcblxuICAgIGZ1bmN0aW9uIGxvY2FsSW5pdCgpIHtcbiAgICAgIGZvcm0uY2x1c3RlcmluZ1NlbGVjdG9yID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjbHVzdGVyaW5nVHlwZVwiKTtcbiAgICAgIGZvcm0ubmJDbHVzdGVycyA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibmJDbHVzdGVyc1wiKTtcbiAgICAgIGZvcm0uY2x1c3RlcmluZ1NlbGVjdG9yLm9uY2hhbmdlID0gb25DbHVzdGVyaW5nQ2hhbmdlO1xuICAgICAgZm9ybS53aXRoUm93TGFiZWxzID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJ3aXRoUm93TGFiZWxzXCIpO1xuICAgICAgZm9ybS53aXRoQ29sdW1uTGFiZWxzID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJ3aXRoQ29sdW1uTGFiZWxzXCIpO1xuICAgICAgZm9ybS5vcmRpbmFsRGltZW5zaW9ucyA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwib3JkaW5hbERpbWVuc2lvbnNcIik7XG4gICAgICBmb3JtLm5vbWluYWxEaW1lbnNpb25zID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJub21pbmFsRGltZW5zaW9uc1wiKTtcblxuICAgICAgb25DbHVzdGVyaW5nQ2hhbmdlLnNlbGVjdG9yID0gZm9ybS5jbHVzdGVyaW5nU2VsZWN0b3I7XG4gICAgICBvbkNsdXN0ZXJpbmdDaGFuZ2UuYmxvY2sgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNsdXN0ZXJpbmdPcHRpb25cIik7XG4gICAgICBvbkNsdXN0ZXJpbmdDaGFuZ2UuaW5wdXRMaXN0ID0gW2Zvcm0ubmJDbHVzdGVyc107XG5cbiAgICAgIC8vIERyb3Agem9uZSBsaXN0ZW5lcnMgc2V0dXAgaWYgRmlsZSBBUEkgaXMgYXZhaWxhYmxlXG4gICAgICBpZiAod2luZG93LkZpbGUgJiYgd2luZG93LkZpbGVSZWFkZXIgJiYgd2luZG93LkZpbGVMaXN0ICYmIHdpbmRvdy5CbG9iKSB7XG4gICAgICAgICAgZm9ybS5kcm9wWm9uZSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZHJvcFpvbmVcIik7XG4gICAgICAgICAgZm9ybS5maWxlQnJvd3NlciA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZmlsZUJyb3dzZXJcIik7XG4gICAgICAgICAgZm9ybS5kcm9wWm9uZS5hZGRFdmVudExpc3RlbmVyKCdkcmFnb3ZlcicsIGhhbmRsZURyYWdPdmVyLCBmYWxzZSk7XG4gICAgICAgICAgZm9ybS5kcm9wWm9uZS5hZGRFdmVudExpc3RlbmVyKCdkcm9wJywgaGFuZGxlRHJvcEZpbGVTZWxlY3QsIGZhbHNlKTtcbiAgICAgICAgICBmb3JtLmZpbGVCcm93c2VyLmFkZEV2ZW50TGlzdGVuZXIoJ2NoYW5nZScsIGhhbmRsZUZpbGVTZWxlY3QsIGZhbHNlKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdmlzLm9uRXJyb3IoTW9kdWxlLkVycm9yLk5PX0ZJTEUudmFsdWUpO1xuICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIHJlbW90ZUluaXQoKSB7XG4gICAgICByZXF1ZXN0KHNlcnZlckFkZHJlc3MsIFwicGxhaW4vdGV4dFwiLCBmdW5jdGlvbihyZXNwb25zZSkge1xuICAgICAgICB2YXIgc2VsZWN0ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJzZWxlY3REYXRhc2V0XCIpO1xuICAgICAgICByZXNwb25zZS5zcGxpdChcIlxcblwiKS5mb3JFYWNoKGZ1bmN0aW9uKGRhdGFTZXROYW1lKSB7XG4gICAgICAgICAgdmFyIG9wdGlvbiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJvcHRpb25cIik7XG4gICAgICAgICAgb3B0aW9uLnZhbHVlID0gZGF0YVNldE5hbWU7XG4gICAgICAgICAgb3B0aW9uLnRleHQgPSBkYXRhU2V0TmFtZS5zdWJzdHJpbmcoMCwgMzApICsgXCIuLi5cIjtcbiAgICAgICAgICBzZWxlY3QuYWRkKG9wdGlvbik7XG4gICAgICAgIH0pO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gaW5pdCgpIHtcbiAgICAgIGlmIChWRVJTSU9OID09IFwibG9jYWxcIikgbG9jYWxJbml0KCk7XG4gICAgICBlbHNlIGlmIChWRVJTSU9OID09IFwicmVtb3RlXCIpIHJlbW90ZUluaXQoKTtcbiAgICAgIGVsc2UgdGhyb3cgXCJNYWpvciBpbmNvaGVyZW5jZSBpbiBKUyAoaW52YWxpZCB2ZXJzaW9uKVwiO1xuICAgIH1cblxuICAgIGluaXQoKTtcbiAgICByZXR1cm4gc2VsZjtcbn07XG5cbnZhciBtYWtlUmVuZGVyaW5nTWVudSA9IGZ1bmN0aW9uIChyb290SWQpIHtcbiAgICBcInVzZSBzdHJpY3RcIjtcbiAgICB2YXIgc2VsZiA9IHt9LCBvcHRpb25zLCBjb2xvclNjaGVtZXMsIGNvbnRhaW5lciwgc2NoZW1lU2VsZWN0b3I7XG4gICAgY29udGFpbmVyID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQocm9vdElkKTtcblxuICAgIGNvbG9yU2NoZW1lcyA9IFtcbiAgICAgICAge1xuICAgICAgICAgICAgaWQ6IFwiZ3JlZW4tYmxhY2stcmVkXCIsXG4gICAgICAgICAgICBuYW1lOiBcIkdyZWVuIEJsYWNrIFJlZFwiLFxuICAgICAgICAgICAgdmFsdWU6IE1vZHVsZS5Db2xvclNjaGVtZS5HUkVFTl9CTEFDS19SRURcbiAgICAgICAgfSxcbiAgICAgICAge2lkOiBcInZpcmlkaXNcIiwgbmFtZTogXCJWaXJpZGlzXCIsIHZhbHVlOiBNb2R1bGUuQ29sb3JTY2hlbWUuVklSSURJU30sXG4gICAgICAgIHtpZDogXCJsZWdhY3lcIiwgbmFtZTogXCJMZWdhY3lcIiwgdmFsdWU6IE1vZHVsZS5Db2xvclNjaGVtZS5MRUdBQ1l9LFxuICAgICAgICB7XG4gICAgICAgICAgICBpZDogXCJiYWJ5LXJvb21cIixcbiAgICAgICAgICAgIG5hbWU6IFwiQmFieSByb29tXCIsXG4gICAgICAgICAgICB2YWx1ZTogTW9kdWxlLkNvbG9yU2NoZW1lLkJBQllfUk9PTVxuICAgICAgICB9LFxuICAgICAgICB7aWQ6IFwicGFsYW1wYVwiLCBuYW1lOiBcIlBhbGFtcGFcIiwgdmFsdWU6IE1vZHVsZS5Db2xvclNjaGVtZS5QQUxBTVBBfV07XG5cbiAgICBvcHRpb25zID0ge1xuICAgICAgICBpbk5vZGU6IHtcbiAgICAgICAgICAgIGxhYmVsOiBcIkhpc3RvZ3JhbS9EaXN0b3J0aW9uXCIsXG4gICAgICAgICAgICB0aXRsZTogXCJDaGFuZ2UgaW5uZXIgbm9kZSBkaWFncmFtIG1vZGVcIixcbiAgICAgICAgICAgIGRlZmF1bHRDaGVja2VkOiBmYWxzZSxcbiAgICAgICAgICAgIGRlZmF1bHREaXNhYmxlZDogdHJ1ZSxcbiAgICAgICAgICAgIG9uY2xpY2s6IGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0dmFyIGUgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImluTm9kZVwiKTtcblx0XHRcdFx0aWYgKCFlLmRpc2FibGVkKSBlLmNoZWNrZWQgPSAhZS5jaGVja2VkO1xuICAgICAgICAgICAgICAgIE1vZHVsZS5zd2l0Y2hJbm5lck5vZGVWaWV3KCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIG5vZGVXZWlnaHQ6IHtcbiAgICAgICAgICAgIGxhYmVsOiBcIldlaWdodC9SYW5nZSBzaXplXCIsXG4gICAgICAgICAgICB0aXRsZTogXCJDaGFuZ2Ugbm9kZSBsYXlvdXRcIixcbiAgICAgICAgICAgIGRlZmF1bHRDaGVja2VkOiBmYWxzZSxcbiAgICAgICAgICAgIGRlZmF1bHREaXNhYmxlZDogdHJ1ZSxcbiAgICAgICAgICAgIG9uY2xpY2s6IGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0dmFyIGUgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm5vZGVXZWlnaHRcIik7XG5cdFx0XHRcdGlmICghZS5kaXNhYmxlZCkgZS5jaGVja2VkID0gIWUuY2hlY2tlZDtcbiAgICAgICAgICAgICAgICBNb2R1bGUuY2hhbmdlTWFwcGluZygpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBncmFkaWVudDoge1xuICAgICAgICAgICAgbGFiZWw6IFwiUGVyIGF4aXMvR2xvYmFsXCIsXG4gICAgICAgICAgICB0aXRsZTogXCJDaGFuZ2UgZ3JhZGllbnQgc2NvcGVcIixcbiAgICAgICAgICAgIGRlZmF1bHRDaGVja2VkOiBmYWxzZSxcbiAgICAgICAgICAgIGRlZmF1bHREaXNhYmxlZDogdHJ1ZSxcbiAgICAgICAgICAgIG9uY2xpY2s6IGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0dmFyIGUgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImdyYWRpZW50XCIpO1xuXHRcdFx0XHRpZiAoIWUuZGlzYWJsZWQpIGUuY2hlY2tlZCA9ICFlLmNoZWNrZWQ7XG4gICAgICAgICAgICAgICAgTW9kdWxlLmNoYW5nZUdyYWRpZW50U2NvcGUoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH07XG5cbiAgICBzZWxmLmFjdGl2YXRlT3B0aW9uID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBhY3RpdmF0ZU9wdGlvbihvcHRpb25zLCBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMpKTtcbiAgICB9O1xuXG4gICAgc2VsZi5kZWFjdGl2YXRlT3B0aW9uID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBkZWFjdGl2YXRlT3B0aW9uKG9wdGlvbnMsIEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cykpO1xuICAgIH07XG5cbiAgICBzZWxmLmNsZWFyID0gZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciBvO1xuICAgICAgICBmb3IgKG8gaW4gb3B0aW9ucylcbiAgICAgICAgICBpZiAob3B0aW9ucy5oYXNPd25Qcm9wZXJ0eShvKSlcbiAgICAgICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChvKS5jaGVja2VkID0gZmFsc2U7XG5cdHNjaGVtZVNlbGVjdG9yLmZpcnN0RWxlbWVudENoaWxkLnNlbGVjdGVkID0gdHJ1ZTtcbiAgICB9O1xuXG4gICAgZnVuY3Rpb24gb25Db2xvclNjaGVtZUNoYW5nZSgpIHtcbiAgICAgICAgY29sb3JTY2hlbWVzLnNvbWUoZnVuY3Rpb24gKHNjaGVtZSkge1xuICAgICAgICAgICAgaWYgKHNjaGVtZS5pZCA9PT0gb25Db2xvclNjaGVtZUNoYW5nZS5zZWxlY3Rvci52YWx1ZSkge1xuICAgICAgICAgICAgICAgIE1vZHVsZS5zZXRDb2xvclNjaGVtZShzY2hlbWUudmFsdWUpO1xuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBpbml0KCkge1xuICAgICAgICBzY2hlbWVTZWxlY3RvciA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY29sb3JTY2hlbWVTZWxlY3RvclwiKTtcbiAgICAgICAgdmFyIG9wdGlvbkxpc3QgPSBjb250YWluZXIuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJ1bFwiKVswXTtcbiAgICAgICAgY29sb3JTY2hlbWVzLmZvckVhY2goZnVuY3Rpb24gKHNjaGVtZSkge1xuICAgICAgICAgICAgdmFyIG9wID0gbmV3IE9wdGlvbigpO1xuICAgICAgICAgICAgb3AudmFsdWUgPSBzY2hlbWUuaWQ7XG4gICAgICAgICAgICBvcC50ZXh0ID0gc2NoZW1lLm5hbWU7XG4gICAgICAgICAgICBzY2hlbWVTZWxlY3Rvci5hZGQob3ApO1xuICAgICAgICB9KTtcbiAgICAgICAgc2NoZW1lU2VsZWN0b3IuZmlyc3RFbGVtZW50Q2hpbGQuc2VsZWN0ZWQgPSB0cnVlO1xuICAgICAgICBzY2hlbWVTZWxlY3Rvci5vbmNoYW5nZSA9IG9uQ29sb3JTY2hlbWVDaGFuZ2U7XG4gICAgICAgIG9uQ29sb3JTY2hlbWVDaGFuZ2Uuc2VsZWN0b3IgPSBzY2hlbWVTZWxlY3RvcjtcbiAgICAgICAgYWRkU3dpdGNoT3B0aW9uVG9MaXN0KG9wdGlvbnMsIG9wdGlvbkxpc3QpO1xuICAgIH1cblxuICAgIGluaXQoKTtcbiAgICByZXR1cm4gc2VsZjtcbn07XG5cbnZhciBtYWtlRXhwb3J0TWVudSA9IGZ1bmN0aW9uIChyb290SWQpIHtcbiAgICBcInVzZSBzdHJpY3RcIjtcbiAgICB2YXIgc2VsZiA9IHt9LCBvcHRpb25zLCBleHBvcnRMaW5rO1xuICAgIC8vIFNob3VsZCBiZSBkaXNhYmxlZCBhdCBsb2FkaW5nXG4gICAgb3B0aW9ucyA9IHtcbiAgICAgICAgXCJoaWdobGlnaHRPbmx5XCI6IHtcbiAgICAgICAgICAgIGxhYmVsOiBcIkhpZ2hsaWdodGVkIG9ubHlcIixcbiAgICAgICAgICAgIHRpdGxlOiBcIkZpbHRlciBpbiBoaWdobGlnaHRlZCBlbGVtZW50c1wiLFxuICAgICAgICAgICAgZGVmYXVsdENoZWNrZWQ6IHRydWUsXG4gICAgICAgICAgICBkZWZhdWx0RGlzYWJsZWQ6IHRydWUsXG4gICAgICAgICAgICBvbmNsaWNrOiBmdW5jdGlvbigpIHtcblx0XHRcdFx0dmFyIGUgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImhpZ2hsaWdodE9ubHlcIik7XG5cdFx0XHRcdGlmICghZS5kaXNhYmxlZCkgZS5jaGVja2VkID0gIWUuY2hlY2tlZDtcblx0XHRcdFx0ZGF0YUhhbmRsZXIuZmV0Y2hEYXRhKCk7XG5cdFx0XHR9XG4gICAgICAgIH0sXG4gICAgICAgIFwiaW5jbHVkZURlbGV0ZWRcIjoge1xuICAgICAgICAgICAgbGFiZWw6IFwiSW5jbHVkZSBkZWxldGVkXCIsXG4gICAgICAgICAgICB0aXRsZTogXCJEaXNwbGF5IGFsbCBkaW1lbnNpb25zLCBpbmNsdWRpbmcgZGVsZXRlZCBvbmVzXCIsXG4gICAgICAgICAgICBkZWZhdWx0Q2hlY2tlZDogdHJ1ZSxcbiAgICAgICAgICAgIGRlZmF1bHREaXNhYmxlZDogdHJ1ZSxcbiAgICAgICAgICAgIG9uY2xpY2s6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHR2YXIgZSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiaW5jbHVkZURlbGV0ZWRcIik7XG5cdFx0XHRcdGlmICghZS5kaXNhYmxlZCkgZS5jaGVja2VkID0gIWUuY2hlY2tlZDtcblx0XHRcdFx0ZGF0YUhhbmRsZXIuZmV0Y2hEYXRhKCk7XG5cdFx0XHR9XG4gICAgICAgIH1cbiAgICB9O1xuXG4gICAgZnVuY3Rpb24gb25FeHBvcnQoKSB7XG4gICAgICAgIGV4cG9ydExpbmsuaHJlZiA9IGRhdGFIYW5kbGVyLmdldEV4cG9ydFVSSSgpO1xuICAgICAgICBleHBvcnRMaW5rLmNsaWNrKCk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gY3JlYXRlTWVudSgpIHtcbiAgICAgICAgdmFyIGxpc3QsIGJ1dHRvbiwgbGluaywgaW5wdXQ7XG4gICAgICAgIGxpc3QgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwidWxcIik7XG4gICAgICAgIGFkZFN3aXRjaE9wdGlvblRvTGlzdChvcHRpb25zLCBsaXN0KTtcbiAgICAgICAgYnV0dG9uID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImxpXCIpO1xuICAgICAgICBsaW5rID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImFcIik7XG4gICAgICAgIGxpbmsuZG93bmxvYWQgPSBcImV4cG9ydC5jc3ZcIjtcbiAgICAgICAgbGluay5pZCA9IFwiZG93bmxvYWRMaW5rXCI7XG4gICAgICAgIGxpbmsuc3R5bGUgPSBcImRpc3BsYXk6IG5vbmU7XCI7XG4gICAgICAgIGlucHV0ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImlucHV0XCIpO1xuICAgICAgICBpbnB1dC50eXBlID0gXCJzdWJtaXRcIjtcbiAgICAgICAgaW5wdXQudmFsdWUgPSBcIkRvd25sb2FkXCI7XG4gICAgICAgIGlucHV0LmlkID0gXCJleHBvcnRCdXR0b25cIjtcbiAgICAgICAgaW5wdXQuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIG9uRXhwb3J0KTtcbiAgICAgICAgYnV0dG9uLmFwcGVuZENoaWxkKGxpbmspO1xuICAgICAgICBidXR0b24uYXBwZW5kQ2hpbGQoaW5wdXQpO1xuICAgICAgICBsaXN0LmFwcGVuZENoaWxkKGJ1dHRvbik7XG4gICAgICAgIHJldHVybiBsaXN0O1xuICAgIH1cblxuICAgIHNlbGYuYWN0aXZhdGVPcHRpb24gPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGFjdGl2YXRlT3B0aW9uKG9wdGlvbnMsIEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cykpO1xuICAgIH07XG5cbiAgICBzZWxmLmRlYWN0aXZhdGVPcHRpb24gPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGRlYWN0aXZhdGVPcHRpb24ob3B0aW9ucywgQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzKSk7XG4gICAgfTtcblxuICAgIHNlbGYuY2xlYXIgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyIG87XG4gICAgICAgIGZvciAobyBpbiBvcHRpb25zKVxuICAgICAgICAgIGlmIChvcHRpb25zLmhhc093blByb3BlcnR5KG8pKVxuICAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKG8pLmNoZWNrZWQgPSBmYWxzZTtcbiAgICB9O1xuXG4gICAgc2VsZi5nZXRPcHRpb25zID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgcmVzdWx0ID0ge30sIG9OYW1lLCBvRWxlbWVudDtcbiAgICAgICAgZm9yIChvTmFtZSBpbiBvcHRpb25zKSB7XG4gICAgICAgICAgICBpZiAob3B0aW9ucy5oYXNPd25Qcm9wZXJ0eShvTmFtZSkpIHtcbiAgICAgICAgICAgICAgICBvRWxlbWVudCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKG9OYW1lKTtcbiAgICAgICAgICAgICAgICByZXN1bHRbb05hbWVdID0gIW9FbGVtZW50LmRpc2FibGVkICYmIG9FbGVtZW50LmNoZWNrZWQ7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICB9O1xuXG4gICAgZnVuY3Rpb24gaW5pdCgpIHtcbiAgICAgICAgdmFyIGNvbnRhaW5lciA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKHJvb3RJZCk7XG5cdGNsZWFyQ2hpbGRyZW4oY29udGFpbmVyKTtcblx0Y29udGFpbmVyLmFwcGVuZENoaWxkKGNyZWF0ZU1lbnUoKSk7XG4gICAgICAgIGV4cG9ydExpbmsgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnZG93bmxvYWRMaW5rJyk7XG5cbiAgICB9XG4gICAgaW5pdCgpO1xuICAgIHJldHVybiBzZWxmO1xufTtcblxuZnVuY3Rpb24gbWFrZVNpZGVCYXIoKSB7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG4gICAgdmFyIHNlbGYgPSB7fSwgaW1wb3J0TWVudSwgZXhwb3J0TWVudSwgcmVuZGVyaW5nTWVudTtcbiAgICBzZWxmLmltcG9ydEZvcm0gPSB7fTtcblxuICAgIHNlbGYub25SZWxvYWQgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgaWYgKFZFUlNJT04gPT0gJ2xvY2FsJylcbiAgICAgICAgICBleHBvcnRNZW51LmNsZWFyKCk7XG4gICAgICAgIHJlbmRlcmluZ01lbnUuY2xlYXIoKTtcbiAgICB9O1xuXG4gICAgZnVuY3Rpb24gb25SZWFkeSgpIHtcbiAgICAgICAgaW1wb3J0TWVudSA9IG1ha2VJbXBvcnRNZW51KFwiaW1wb3J0TWVudVwiKTtcbiAgICAgICAgcmVuZGVyaW5nTWVudSA9IG1ha2VSZW5kZXJpbmdNZW51KFwiZGlzcGxheU1lbnVcIik7XG4gICAgICAgIGlmIChWRVJTSU9OID09ICdsb2NhbCcpXG4gICAgICAgICAgZXhwb3J0TWVudSA9IG1ha2VFeHBvcnRNZW51KFwiZXhwb3J0TWVudVwiKTtcbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZShcInNpZGViYXJcIilbMF0uc3R5bGUudmlzaWJpbGl0eSA9IFwidmlzaWJsZVwiO1xuICAgIH1cblxuICAgIHNlbGYuZ2V0RXhwb3J0T3B0aW9ucyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKFZFUlNJT04gPT0gJ2xvY2FsJylcbiAgICAgICAgICByZXR1cm4gZXhwb3J0TWVudS5nZXRPcHRpb25zKCk7XG4gICAgICAgIGVsc2UgcmV0dXJuIHt9O1xuICAgIH07XG5cbiAgICBzZWxmLnNldEZpbGVuYW1lID0gZnVuY3Rpb24gKG5hbWUpIHtcbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJpbnB1dEZpbGVNZXNzYWdlXCIpLnRleHRDb250ZW50ID0gbmFtZTtcbiAgICB9O1xuICAgIFxuICAgIHNlbGYub25Mb2FkU3VjY2VzcyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIGlmIChWRVJTSU9OID09ICdsb2NhbCcpXG4gICAgICAgIGV4cG9ydE1lbnUuZGVhY3RpdmF0ZU9wdGlvbihcImhpZ2hsaWdodE9ubHlcIik7XG4gICAgICByZW5kZXJpbmdNZW51LmFjdGl2YXRlT3B0aW9uKCk7XG4gICAgfTtcblxuICAgIHNlbGYub25IaWdobGlnaHQgPSBmdW5jdGlvbiAob24pIHtcbiAgICAgIGlmIChWRVJTSU9OID09ICdyZW1vdGUnKSByZXR1cm47XG4gICAgICAgIGlmIChvbikge1xuICAgICAgICAgICAgZXhwb3J0TWVudS5hY3RpdmF0ZU9wdGlvbihcImhpZ2hsaWdodE9ubHlcIik7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBleHBvcnRNZW51LmRlYWN0aXZhdGVPcHRpb24oXCJoaWdobGlnaHRPbmx5XCIpO1xuICAgICAgICB9XG4gICAgfTtcblxuICAgIHNlbGYub25BeGlzQ2hhbmdlID0gZnVuY3Rpb24gKCkge1xuICAgICAgaWYgKFZFUlNJT04gPT0gJ3JlbW90ZScpIHJldHVybjtcbiAgICAgICAgaWYgKE1vZHVsZS5nZXROYkF4ZXMoKSA8IE1vZHVsZS5nZXROYlNvdXJjZURpbXMoKSkge1xuICAgICAgICAgICAgZXhwb3J0TWVudS5hY3RpdmF0ZU9wdGlvbihcImluY2x1ZGVEZWxldGVkXCIpO1xuICAgICAgICB9XG4gICAgfTtcblxuICAgIGZ1bmN0aW9uIGxvYWREYXRhU2V0KCkge1xuICAgICAgdmFyIHNlbGVjdCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwic2VsZWN0RGF0YXNldFwiKTtcbiAgICAgIHZhciBkYXRhU2V0TmFtZSA9IHNlbGVjdC5vcHRpb25zW3NlbGVjdC5zZWxlY3RlZEluZGV4XS52YWx1ZTtcbiAgICAgIE1vZHVsZS5sb2FkRGF0YShkYXRhU2V0TmFtZSwgU2VydmVyLmhvc3RuYW1lLCBTZXJ2ZXIucG9ydCwgU2VydmVyLmJhc2UpO1xuICAgIH1cblxuICAgIHNlbGYub25JbXBvcnQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIC8vdHJ5IHtcbiAgICAgICAgICBpZiAoVkVSU0lPTiA9PSBcImxvY2FsXCIpIGRhdGFIYW5kbGVyLmxvYWRGaWxlKGltcG9ydE1lbnUuZ2V0T3B0aW9uVmFsdWVzKCkpO1xuICAgICAgICAgIGVsc2UgaWYgKFZFUlNJT04gPT0gXCJyZW1vdGVcIikgbG9hZERhdGFTZXQoKTtcbiAgICAgICAgICAgIHZpekV2ZW50LmluaXRNb2RlKCk7XG4gICAgICAgICAgICB2aXMub25SZXNpemUoKTtcbiAgICAgICAgICAgIE1vZHVsZS5maXQoKTtcbiAgICAgICAgICAgIHZpcy5oaWRlRXJyb3IoKTtcbiAgICAgICAgICAgIHNlbGYub25Mb2FkU3VjY2VzcygpO1xuICAgICAgICAvKn0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkltcG9ydEVycm9yXCIpO1xuICAgICAgICB9Ki9cbiAgICB9O1xuICAgIG9uUmVhZHkoKTtcbiAgICByZXR1cm4gc2VsZjtcbn1cbiIsIkFycmF5LnJhbmdlID0gZnVuY3Rpb24gKGEsIGIsIHN0ZXApIHtcbiAgICAndXNlIHN0cmljdCc7XG4gICAgdmFyIEEgPSBbXTtcbiAgICBpZiAodHlwZW9mIGEgPT09ICdudW1iZXInKSB7XG4gICAgICAgIEFbMF0gPSBhO1xuICAgICAgICBzdGVwID0gc3RlcCB8fCAxO1xuICAgICAgICB3aGlsZSAoYSArIHN0ZXAgPD0gYikge1xuICAgICAgICAgICAgYSArPSBzdGVwO1xuICAgICAgICAgICAgQVtBLmxlbmd0aF0gPSBhO1xuICAgICAgICB9XG4gICAgfVxuICAgIHJldHVybiBBO1xufTtcblxudmFyIGNsZWFyQ2hpbGRyZW4gPSBmdW5jdGlvbiAoZSkge1xuICAgIFwidXNlIHN0cmljdFwiO1xuICAgIHdoaWxlIChlLmZpcnN0Q2hpbGQpIHtcbiAgICAgICAgZS5yZW1vdmVDaGlsZChlLmZpcnN0Q2hpbGQpO1xuICAgIH1cbn07XG5cbmZ1bmN0aW9uIHJlcXVlc3QocGF0aCwgbWltZVR5cGUsIG9uTG9hZGVkKSB7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG4gICAgLy8gXCJYTUxIdHRwUmVxdWVzdCBjYW4gYmUgdXNlZCB0byByZXRyaWV2ZSBhbnkgdHlwZSBvZiBkYXRhLCBub3QganVzdCBYTUxcIlxuICAgIHZhciB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcbiAgICB4aHIub25lcnJvciA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgY29uc29sZS5sb2coXCJFcnJvciB3aGlsZSBsb2FkaW5nIGFzc2V0IGZpbGUgXCIgKyBwYXRoKTtcbiAgICB9O1xuICAgIHhoci5vbnJlYWR5c3RhdGVjaGFuZ2UgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmICh4aHIucmVhZHlTdGF0ZSA9PT0gWE1MSHR0cFJlcXVlc3QuRE9ORSkge1xuICAgICAgICAgICAgb25Mb2FkZWQoeGhyLnJlc3BvbnNlVGV4dCk7XG4gICAgICAgIH1cbiAgICB9O1xuICAgIHhoci5vcGVuKFwiR0VUXCIsIHBhdGgsIHRydWUpO1xuICAgIHhoci5vdmVycmlkZU1pbWVUeXBlKG1pbWVUeXBlKTtcbiAgICB4aHIuc2VuZChudWxsKTtcbn1cblxuLypcbiBBcHBlbmQgYSBzd2l0Y2gtc3R5bGVkIGNoZWNrYm94IG9wdGlvbiB0byBhbiBleGlzdGluZyBIVE1MIGxpc3RcbiBPcHRpb25zIGZvcm1hdDpcbiB7IFwib3B0aW9uMUlkXCI6IHtcbiBsYWJlbDogXCJvcHRpb24xTGFiZWxcIixcbiB0aXRsZTogXCJvcHRpb24xQnViYmxlQ29udGVudFwiLFxuIGRlZmF1bHRDaGVja2VkOiBib29sLFxuIGRlZmF1bHREaXNhYmxlZDogYm9vbCxcbiBvbmNsaWNrOiBmdW5jXG4gfSxcbiBcIm9wdGlvbjJJZFwiIC4uLlxuIH1cbiAqL1xudmFyIGFkZFN3aXRjaE9wdGlvblRvTGlzdCA9IGZ1bmN0aW9uIChzd2l0Y2hPcHRpb25zLCBsaXN0SFRNTEVsZW1lbnQpIHtcbiAgICBcInVzZSBzdHJpY3RcIjtcbiAgICB2YXIgb3B0aW9uLCBsaSwgaW5wdXQsIGxhYmVsO1xuICAgIGZvciAob3B0aW9uIGluIHN3aXRjaE9wdGlvbnMpIHtcbiAgICAgICAgaWYgKHN3aXRjaE9wdGlvbnMuaGFzT3duUHJvcGVydHkob3B0aW9uKSkge1xuICAgICAgICAgICAgbGkgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwibGlcIik7XG4gICAgICAgICAgICBsaS50aXRsZSA9IHN3aXRjaE9wdGlvbnNbb3B0aW9uXS50aXRsZTtcbiAgICAgICAgICAgIGlucHV0ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImlucHV0XCIpO1xuICAgICAgICAgICAgaW5wdXQuaWQgPSBvcHRpb247XG4gICAgICAgICAgICBpbnB1dC50eXBlID0gXCJjaGVja2JveFwiO1xuICAgICAgICAgICAgaW5wdXQuZGlzYWJsZWQgPSBzd2l0Y2hPcHRpb25zW29wdGlvbl0uZGVmYXVsdERpc2FibGVkO1xuICAgICAgICAgICAgbGkuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIHN3aXRjaE9wdGlvbnNbb3B0aW9uXS5vbmNsaWNrKTtcbiAgICAgICAgICAgIGxhYmVsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImxhYmVsXCIpO1xuICAgICAgICAgICAgbGFiZWwuZm9yID0gb3B0aW9uO1xuICAgICAgICAgICAgbGFiZWwudGV4dENvbnRlbnQgPSBzd2l0Y2hPcHRpb25zW29wdGlvbl0ubGFiZWw7XG4gICAgICAgICAgICBsaS5hcHBlbmRDaGlsZChpbnB1dCk7XG4gICAgICAgICAgICBsaS5hcHBlbmRDaGlsZChsYWJlbCk7XG4gICAgICAgICAgICBsaXN0SFRNTEVsZW1lbnQuYXBwZW5kQ2hpbGQobGkpO1xuICAgICAgICB9XG4gICAgfVxufTtcblxuZnVuY3Rpb24gZGVhY3RpdmF0ZU9wdGlvbihleGlzdGluZywgcHJvdmlkZWQpIHtcbiAgICBcInVzZSBzdHJpY3RcIjtcbiAgICB2YXIga2V5O1xuICAgIGlmIChwcm92aWRlZC5sZW5ndGggPT09IDApIHsgLy8gQUxMXG4gICAgICAgIGZvciAoa2V5IGluIGV4aXN0aW5nKSB7XG4gICAgICAgICAgICBpZiAoZXhpc3RpbmcuaGFzT3duUHJvcGVydHkoa2V5KSkge1xuICAgICAgICAgICAgICAgIHByb3ZpZGVkLnB1c2goa2V5KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbiAgICBwcm92aWRlZC5mb3JFYWNoKGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgICAgIGlmIChleGlzdGluZy5oYXNPd25Qcm9wZXJ0eShuYW1lKSkge1xuICAgICAgICAgICAgdmFyIGVsZW1lbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChuYW1lKTtcbiAgICAgICAgICAgIGVsZW1lbnQuZGlzYWJsZWQgPSB0cnVlO1xuICAgICAgICAgICAgZWxlbWVudC5jaGVja2VkID0gZmFsc2U7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aHJvdyBcIkludmFsaWRPcHRpb246IFwiICsgbmFtZTtcbiAgICAgICAgfVxuICAgIH0pO1xufVxuXG5mdW5jdGlvbiBhY3RpdmF0ZU9wdGlvbihleGlzdGluZywgcHJvdmlkZWQpIHtcbiAgICBcInVzZSBzdHJpY3RcIjtcbiAgICB2YXIga2V5O1xuICAgIGlmIChwcm92aWRlZC5sZW5ndGggPT09IDApIHtcbiAgICAgICAgZm9yIChrZXkgaW4gZXhpc3RpbmcpIHtcbiAgICAgICAgICAgIGlmIChleGlzdGluZy5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XG4gICAgICAgICAgICAgICAgcHJvdmlkZWQucHVzaChrZXkpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuICAgIHByb3ZpZGVkLmZvckVhY2goZnVuY3Rpb24gKG5hbWUpIHtcbiAgICAgICAgaWYgKGV4aXN0aW5nLmhhc093blByb3BlcnR5KG5hbWUpKSB7XG4gICAgICAgICAgICB2YXIgZWxlbWVudCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKG5hbWUpO1xuICAgICAgICAgICAgZWxlbWVudC5kaXNhYmxlZCA9IGZhbHNlO1xuICAgICAgICAgICAgZWxlbWVudC5jaGVja2VkID0gZXhpc3RpbmdbbmFtZV0uZGVmYXVsdENoZWNrZWQ7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aHJvdyBcIkludmFsaWRPcHRpb246IFwiICsgbmFtZTtcbiAgICAgICAgfVxuICAgIH0pO1xufVxuXG52YXIgbWFrZUxheW91dCA9IGZ1bmN0aW9uIChjb250YWluZXJDbGFzcywgdGl0bGVDbGFzcywgdGFiTGlzdCkge1xuICAgIFwidXNlIHN0cmljdFwiO1xuICAgIHZhciBjb250YWluZXIsIHRpdGxlLCBzZWN0aW9uQ29udGVudDtcbiAgICBjb250YWluZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO1xuICAgIGNvbnRhaW5lci5jbGFzc05hbWUgPSBjb250YWluZXJDbGFzcztcbiAgICB0YWJMaXN0LmZvckVhY2goZnVuY3Rpb24gKHRhYkVsZW1lbnQsIGkpIHtcbiAgICAgICAgdGl0bGUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiaDJcIik7XG4gICAgICAgIHRpdGxlLmNsYXNzTmFtZSA9IHRpdGxlQ2xhc3M7XG4gICAgICAgIHRpdGxlLnRleHRDb250ZW50ID0gdGFiRWxlbWVudC50aXRsZTtcbiAgICAgICAgc2VjdGlvbkNvbnRlbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO1xuICAgICAgICBzZWN0aW9uQ29udGVudC5pbm5lckhUTUwgPSBcIjxkaXY+XCIgKyB0YWJFbGVtZW50LmNvbnRlbnQuam9pbihcIiBcIikgKyBcIjwvZGl2PlwiO1xuICAgICAgICBjb250YWluZXIuYXBwZW5kQ2hpbGQodGl0bGUpO1xuICAgICAgICBjb250YWluZXIuYXBwZW5kQ2hpbGQoc2VjdGlvbkNvbnRlbnQpO1xuICAgIH0pO1xuICAgIHJldHVybiBjb250YWluZXI7XG59O1xuIiwidmFyIE1vZHVsZSwgY2xlYXJDaGlsZHJlbiwgdml6RXZlbnQsIG1ha2VBY2NvcmRpb24sIHJlcXVlc3QsIFBhcnNlcjtcblxuLypcbiBDb250cm9sbGVyIGZvciB2aXN1YWxpc2F0aW9uIHRvb2xiYXJcbiAqL1xudmFyIG1ha2VUb29sQmFyID0gZnVuY3Rpb24gKHBhcmVudCkge1xuICAgIFwidXNlIHN0cmljdFwiO1xuICAgIHZhciBzZWxmID0ge30sIGNvbnRhaW5lciA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwidG9vbEJhclwiKTtcblxuICAgIGZ1bmN0aW9uIGZvcm1hdFRvb2wodGFyZ2V0RE9NTGlzdCwgY29udGVudFNvdXJjZSkge1xuICAgICAgdmFyIGl0ZW1Ub0RlbGV0ZSA9IFtdO1xuICAgICAgW10uZm9yRWFjaC5jYWxsKHRhcmdldERPTUxpc3QuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ2xpJyksIGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgICAgIHZhciBpbm5lckljb24gPSBpdGVtLmZpcnN0RWxlbWVudENoaWxkO1xuICAgICAgICBpZiAoIWlubmVySWNvbikgcmV0dXJuO1xuICAgICAgICBpZiAoaW5uZXJJY29uLmhhc0F0dHJpYnV0ZShcImRhdGEtY29udGVudFwiKSkge1xuICAgICAgICAgIHZhciB0b29sTmFtZSA9IGlubmVySWNvbi5kYXRhc2V0LmNvbnRlbnQ7XG4gICAgICAgICAgaWYgKCF0b29sTmFtZSkgcmV0dXJuO1xuICAgICAgICAgIGlmIChjb250ZW50U291cmNlLmhhc093blByb3BlcnR5KHRvb2xOYW1lKSkge1xuICAgICAgICAgICAgdmFyIGNvbnRlbnRJdGVtID0gY29udGVudFNvdXJjZVt0b29sTmFtZV07XG4gICAgICAgICAgICBpZiAoY29udGVudEl0ZW0uaGFzT3duUHJvcGVydHkoXCJzdGF0ZVwiKSkge1xuICAgICAgICAgICAgICBpdGVtLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICBwYXJlbnQub25CdXR0b25DbGljayhjb250ZW50SXRlbS5zdGF0ZSk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIGl0ZW0uaWQgPSBjb250ZW50SXRlbS5zdGF0ZS5uYW1lO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgaXRlbS5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGlmICh0b29sTmFtZSBpbiBNb2R1bGUpXG4gICAgICAgICAgICAgICAgICAgIE1vZHVsZVtcIlwiICsgdG9vbE5hbWVdKCk7XG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICBpdGVtLmlkID0gdG9vbE5hbWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB2YXIgYWx0VGV4dCA9IGNvbnRlbnRJdGVtLnRpdGxlO1xuICAgICAgICAgICAgaWYgKGNvbnRlbnRJdGVtLmhhc093blByb3BlcnR5KFwia2V5XCIpKVxuICAgICAgICAgICAgICBhbHRUZXh0ICs9IFwiIChcIiArIGNvbnRlbnRJdGVtLmtleSArIFwiKVwiO1xuICAgICAgICAgICAgaXRlbS5kYXRhc2V0LnRvb2x0aXAgPSBhbHRUZXh0O1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBpdGVtVG9EZWxldGUucHVzaChpdGVtKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgICAgaXRlbVRvRGVsZXRlLmZvckVhY2goZnVuY3Rpb24oaXRlbSkge1xuICAgICAgICB0YXJnZXRET01MaXN0LnJlbW92ZUNoaWxkKGl0ZW0pO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gaW5pdCgpIHtcbiAgICAgIHZhciBsZWZ0TGlzdCwgcmlnaHRMaXN0O1xuICAgICAgY29udGFpbmVyLnN0eWxlLnZpc2liaWxpdHkgPSAndmlzaWJsZSc7XG4gICAgICBsZWZ0TGlzdCA9IGNvbnRhaW5lci5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKFwicmFnZ2VkLWxlZnRcIilbMF07XG4gICAgICByaWdodExpc3QgPSBjb250YWluZXIuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZShcInJhZ2dlZC1yaWdodFwiKVswXTtcbiAgICAgIGZvcm1hdFRvb2wobGVmdExpc3QsIHRvb2xCYXJUb29scy5sZWZ0KTtcbiAgICAgIGZvcm1hdFRvb2wocmlnaHRMaXN0LCB0b29sQmFyVG9vbHMucmlnaHQpO1xuICAgIH1cblxuICAgIC8qIFN3aXRjaCBhbnkgdG9vbCBidXR0b24gaW1hZ2UuIEltYWdlIGZpbGUgbmFtZSBzaG91bGQgZm9sbG93IHRoZSBwYXR0ZXJuXG4gICAgICogPHN0YXRlLkVudW0ubmFtZVtfb25dLnBuZz4gYW5kIGJlIGxvY2F0ZWQgaW4gLi9pbWcuIEVhY2ggdG9vbGJhciB0b29sIGhhc1xuICAgICAqIGF0IGxlYXN0IG9uZSBpY29uL2ltYWdlLiBQYXJ0IG9mIHRoZW0gcmVxdWlyZSBhbiBpbWFnZSBjaGFuZ2Ugb24gYWN0aXZhdGlvbiAqL1xuXG4gICAgc2VsZi5idXR0b25Td2l0Y2ggPSBmdW5jdGlvbiAoYnV0dG9uLCB0b2dnbGUpIHtcbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoYnV0dG9uKS5maXJzdEVsZW1lbnRDaGlsZC5jbGFzc05hbWUgPSAodG9nZ2xlID8gXCJzZWxlY3RlZFwiIDogXCJcIik7XG4gICAgfTtcblxuICAgIGluaXQoKTtcbiAgICByZXR1cm4gc2VsZjtcbn07XG5cbnZhciBDdXJzb3JQb3NpdGlvbiA9IChmdW5jdGlvbiAoKSB7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG4gICAgdmFyIHNlbGYgPSB7fSxcbiAgICAgICAgY1ggPSAwLFxuICAgICAgICBjWSA9IDAsXG4gICAgICAgIHJYID0gMCxcbiAgICAgICAgclkgPSAwO1xuICAgIC8vIENvcHlyaWdodCAyMDA2LDIwMDcgQm9udHJhZ2VyIENvbm5lY3Rpb24sIExMQ1xuICAgIC8vIGh0dHA6Ly93d3cud2lsbG1hc3Rlci5jb20vXG4gICAgLy8gVmVyc2lvbjogSnVseSAyOCwgMjAwN1xuICAgIGZ1bmN0aW9uIHVwZGF0ZUN1cnNvclBvc2l0aW9uKGUpIHtcbiAgICAgICAgY1ggPSBlLnBhZ2VYO1xuICAgICAgICBjWSA9IGUucGFnZVk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gdXBkYXRlQ3Vyc29yUG9zaXRpb25Eb2NBbGwoKSB7XG4gICAgICAgIGNYID0gZXZlbnQuY2xpZW50WDtcbiAgICAgICAgY1kgPSBldmVudC5jbGllbnRZO1xuICAgIH1cblxuICAgIGlmIChkb2N1bWVudC5hbGwpIHtcbiAgICAgICAgZG9jdW1lbnQub25tb3VzZW1vdmUgPSB1cGRhdGVDdXJzb3JQb3NpdGlvbkRvY0FsbDtcbiAgICB9IGVsc2Uge1xuICAgICAgICBkb2N1bWVudC5vbm1vdXNlbW92ZSA9IHVwZGF0ZUN1cnNvclBvc2l0aW9uO1xuICAgIH1cblxuICAgIHNlbGYuZ2V0Q3VycmVudCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKGRvY3VtZW50LmRvY3VtZW50RWxlbWVudCAmJiBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wKSB7XG4gICAgICAgICAgICByWCA9IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxMZWZ0O1xuICAgICAgICAgICAgclkgPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wO1xuICAgICAgICB9IGVsc2UgaWYgKGRvY3VtZW50LmJvZHkpIHtcbiAgICAgICAgICAgIHJYID0gZG9jdW1lbnQuYm9keS5zY3JvbGxMZWZ0O1xuICAgICAgICAgICAgclggPSBjWDtcbiAgICAgICAgICAgIHJZID0gZG9jdW1lbnQuYm9keS5zY3JvbGxUb3A7XG4gICAgICAgICAgICByWSA9IGNZO1xuICAgICAgICB9XG4gICAgICAgIGlmIChkb2N1bWVudC5hbGwpIHtcbiAgICAgICAgICAgIGNYICs9IHJYO1xuICAgICAgICAgICAgY1kgKz0gclk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHt4OiBjWCwgeTogY1l9O1xuICAgIH07XG5cbiAgICByZXR1cm4gc2VsZjtcbn0oKSk7XG5cbi8qXG4gQ29udHJvbGxlciBmb3IgaW5mbyBidWJibGUgYW5kIGluc2VydGlvbiBtZW51LCBhdmFpbGFibGUgb24gY2xpY2sgaW5cbiBwaWNraW5nIGFuZCBpbnNlcnRpb24gbW9kZXNcbiAqL1xudmFyIG1ha2VQb3BVcCA9IGZ1bmN0aW9uIChtYWtlQ29udGVudCwgcm9vdElkKSB7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG4gICAgdmFyIHNlbGYgPSB7fSxcbiAgICAgICAgY29udGFpbmVyID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQocm9vdElkKTtcblxuXG4gICAgZnVuY3Rpb24gYXNzaWduUG9zaXRpb24oZCkge1xuICAgICAgICB2YXIgY3VyUG9zID0gQ3Vyc29yUG9zaXRpb24uZ2V0Q3VycmVudCgpO1xuICAgICAgICBkLnN0eWxlLmxlZnQgPSAoY3VyUG9zLnggKyAxKSArIFwicHhcIjtcbiAgICAgICAgZC5zdHlsZS50b3AgPSAoY3VyUG9zLnkgKyAxKSArIFwicHhcIjtcbiAgICB9XG5cbiAgICBzZWxmLmhpZGVQb3BVcCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgY29udGFpbmVyLmhpZGRlbiA9IHRydWU7XG4gICAgfTtcblxuICAgIGZ1bmN0aW9uIHNob3dQb3BVcCgpIHtcbiAgICAgICAgY29udGFpbmVyLmhpZGRlbiA9IGZhbHNlO1xuICAgIH1cblxuICAgIHNlbGYuZGlzcGxheSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgc2VsZi5oaWRlUG9wVXAoKTtcbiAgICAgICAgaWYgKG1ha2VDb250ZW50KGNvbnRhaW5lcikpIHtcbiAgICAgICAgICAgIHNob3dQb3BVcCgpO1xuICAgICAgICAgICAgYXNzaWduUG9zaXRpb24oY29udGFpbmVyKTtcbiAgICAgICAgfVxuICAgIH07XG4gICAgcmV0dXJuIHNlbGY7XG59O1xuXG4vKlxuIENvbnRyb2xsZXIgZm9yIGRhdGEgdGFibGUsIHVwZGF0ZWQgb24gZmlsZSBpbXBvcnQgYW5kIGhpZ2hsaWdodGluZ1xuICovXG5cbnZhciBtYWtlRGF0YVRhYmxlID0gZnVuY3Rpb24gKHJvb3RJZCkge1xuICAgIFwidXNlIHN0cmljdFwiO1xuICAgIHZhciBzZWxmID0ge30sXG4gICAgICAgIGFjdGl2YXRlZCxcbiAgICAgICAgY3VycmVudExhc3RSb3csXG4gICAgICAgIGhlYWRlcixcbiAgICAgICAgZGF0YSxcbiAgICAgICAgY29udGFpbmVyID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQocm9vdElkKSxcbiAgICAgICAgdGFibGUsXG4gICAgICAgIGJsb2NrU2l6ZSA9IDE1O1xuXG4gICAgZnVuY3Rpb24gZmlsbFJvdyhkYXRhLCByb3cpIHtcbiAgICAgICAgZGF0YS5mb3JFYWNoKGZ1bmN0aW9uIChsYWJlbCwgaSkge1xuICAgICAgICAgICAgcm93Lmluc2VydENlbGwoaSkuaW5uZXJUZXh0ID0gbGFiZWw7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGFkZFJvd3NUbyhuKSB7XG4gICAgICAgIGlmIChjdXJyZW50TGFzdFJvdyA8IG4pIHtcbiAgICAgICAgICAgIHZhciByb3dzID0gZGF0YS5zbGljZShjdXJyZW50TGFzdFJvdywgTWF0aC5taW4obiwgZGF0YS5sZW5ndGgpKTtcbiAgICAgICAgICAgIHJvd3MuZm9yRWFjaChmdW5jdGlvbiAocm93RGF0YSkge1xuICAgICAgICAgICAgICAgIGZpbGxSb3cocm93RGF0YSwgdGFibGUudEJvZGllc1swXS5pbnNlcnRSb3coY3VycmVudExhc3RSb3cpKTtcbiAgICAgICAgICAgICAgICBjdXJyZW50TGFzdFJvdyArPSAxO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBmdW5jdGlvbiBjaGVja1RhYmxlU2Nyb2xsKCkge1xuICAgICAgICB2YXIgbmV3TGFzdFJvdztcbiAgICAgICAgLy90YWJsZSA9IGNvbnRhaW5lci5maXJzdEVsZW1lbnRDaGlsZDtcbiAgICAgICAgaWYgKCFkYXRhIHx8IGN1cnJlbnRMYXN0Um93ID09PSBkYXRhLmxlbmd0aCB8fCBjaGVja1RhYmxlU2Nyb2xsLmF0Qm90dG9tKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgY2hlY2tUYWJsZVNjcm9sbC5hdEJvdHRvbSA9ICh0aGlzLnNjcm9sbEhlaWdodCAtIHRoaXMuc2Nyb2xsVG9wIDw9IHRoaXMuY2xpZW50SGVpZ2h0KTtcbiAgICAgICAgaWYgKGNoZWNrVGFibGVTY3JvbGwuYXRCb3R0b20pIHtcbiAgICAgICAgICAgIC8vIExvYWQgbW9yZSByb3dzXG4gICAgICAgICAgICBpZiAodGFibGUpIHtcbiAgICAgICAgICAgICAgICBuZXdMYXN0Um93ID0gTWF0aC5taW4oY3VycmVudExhc3RSb3cgKyBibG9ja1NpemUsIGRhdGEubGVuZ3RoKTtcbiAgICAgICAgICAgICAgICBhZGRSb3dzVG8obmV3TGFzdFJvdyk7XG4gICAgICAgICAgICAgICAgY2hlY2tUYWJsZVNjcm9sbC5hdEJvdHRvbSA9IGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gbWFrZUVtcHR5SFRNTFRhYmxlKCkge1xuICAgICAgICB0YWJsZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJ0YWJsZVwiKTtcbiAgICAgICAgdGFibGUuY2xhc3NOYW1lID0gXCJ0YWJsZSB0YWJsZS1yZXNwb25zaXZlIHRhYmxlLXN0cmlwZWQgdGFibGUtY29uZGVuc2VkXCI7XG4gICAgICAgIGlmIChoZWFkZXIgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgZmlsbFJvdyhoZWFkZXIsIHRhYmxlLmNyZWF0ZVRIZWFkKCkuaW5zZXJ0Um93KDApKTtcbiAgICAgICAgfVxuICAgICAgICB0YWJsZS5jcmVhdGVUQm9keSgpO1xuICAgICAgICByZXR1cm4gdGFibGU7XG4gICAgfVxuICAgIFxuICAgIHNlbGYuY2xlYXIgPSBmdW5jdGlvbih2ZXJ0aWNhbFNwYWNlKSB7XG4gICAgICAgIC8vIENsZWFyIHByZXZpb3VzIHRhYmxlXG4gICAgICAgIGN1cnJlbnRMYXN0Um93ID0gMDtcbiAgICAgICAgY2xlYXJDaGlsZHJlbihjb250YWluZXIpO1xuICAgICAgICB0YWJsZSA9IG1ha2VFbXB0eUhUTUxUYWJsZSgpO1xuICAgICAgICBjb250YWluZXIuYXBwZW5kQ2hpbGQodGFibGUpO1xuICAgICAgICBzZWxmLm9uUmVzaXplKHZlcnRpY2FsU3BhY2UpO1xuICAgICAgICBjb250YWluZXIuc2Nyb2xsVG9wID0gMDtcbiAgICAgICAgY2hlY2tUYWJsZVNjcm9sbC5hdEJvdHRvbSA9IGZhbHNlO1xuICAgIFxuICAgIH07XG5cbiAgICBzZWxmLnVwZGF0ZSA9IGZ1bmN0aW9uIChsb2FkZWREYXRhLCBoYXNIZWFkZXIsIHZlcnRpY2FsU3BhY2UpIHtcbiAgICAgICAgaWYgKCFhY3RpdmF0ZWQpIHJldHVybjtcbiAgICAgICAgaWYgKGhhc0hlYWRlciAmJiBsb2FkZWREYXRhLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgIGhlYWRlciA9IGxvYWRlZERhdGFbMF07XG4gICAgICAgICAgICBkYXRhID0gbG9hZGVkRGF0YS5zbGljZSgxLCBsb2FkZWREYXRhLmxlbmd0aCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBoZWFkZXIgPSB1bmRlZmluZWQ7XG4gICAgICAgICAgICBkYXRhID0gbG9hZGVkRGF0YTtcbiAgICAgICAgfVxuICAgICAgICBzZWxmLmNsZWFyKHZlcnRpY2FsU3BhY2UpO1xuICAgIH07XG5cbiAgICBzZWxmLm9uUmVzaXplID0gZnVuY3Rpb24gKGZyZWVWZXJ0aWNhbFNwYWNlKSB7XG4gICAgICAgIGlmICghYWN0aXZhdGVkKSByZXR1cm47XG4gICAgICAgIGlmICh0YWJsZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICB2YXIgY2VsbEhlaWdodCwgbmJSb3dzO1xuICAgICAgICAgICAgYWRkUm93c1RvKDEpOy8vIFRhYmxlIHNob3VsZCBob2xkIGF0IGxlYXN0IG9uZSBjZWxsIGZvciBoZWlnaHQgbWVhc3VyZW1lbnRcbiAgICAgICAgICAgIGNlbGxIZWlnaHQgPSB0YWJsZS5yb3dzWzBdLmZpcnN0RWxlbWVudENoaWxkLm9mZnNldEhlaWdodDtcbiAgICAgICAgICAgIG5iUm93cyA9IE1hdGgucm91bmQoZnJlZVZlcnRpY2FsU3BhY2UgLyBjZWxsSGVpZ2h0ICogMik7IC8vIG9yIGFueXRoaW5nIGVuc3VyZSBnb2luZyBhYm92ZSB0aGUgZXhhY3QgZmlsbGluZyBudW1iZXJcbiAgICAgICAgICAgIGFkZFJvd3NUbyhuYlJvd3MpO1xuICAgICAgICB9XG4gICAgfTtcblxuICAgIGFjdGl2YXRlZCA9IChWRVJTSU9OID09ICdsb2NhbCcpO1xuICAgIC8vIERhdGEgdGFibGUgc2Nyb2xsIGxpc3RlbmVyXG4gICAgY29udGFpbmVyLm9uc2Nyb2xsID0gY2hlY2tUYWJsZVNjcm9sbDtcbiAgICBjaGVja1RhYmxlU2Nyb2xsLmNhbGwoY29udGFpbmVyKTtcbiAgICByZXR1cm4gc2VsZjtcbn07XG5cbmZ1bmN0aW9uIG1ha2VWaXMocm9vdElkKSB7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG4gICAgdmFyIHNlbGYgPSB7fSxcbiAgICAgICAgY29udGFpbmVyID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQocm9vdElkKSxcbiAgICAgICAgcGlja2luZ0J1YmJsZSxcbiAgICAgICAgaW5zZXJ0aW9uTWVudSxcbiAgICAgICAgYXZhaWxhYmxlRGltZW5zaW9ucyxcbiAgICAgICAgZGF0YVRhYmxlLFxuICAgICAgICBlcnJvckNvbnRhaW5lciA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZXJyb3JNZXNzYWdlXCIpLmZpcnN0RWxlbWVudENoaWxkLFxuICAgICAgICBlcnJvck1lc3NhZ2VzO1xuICAgIHNlbGYudGFibGVEYXRhID0gdW5kZWZpbmVkO1xuXG4gICAgZnVuY3Rpb24gaW5pdFRvb2x0aXAoKSB7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gaW5pdEluc2VydGlvbk1lbnUoKSB7XG5cbiAgICAgICAgZnVuY3Rpb24gaXRlbUNsaWNrKGkpIHtcblx0XHRcdGNvbnNvbGUubG9nKGF2YWlsYWJsZURpbWVuc2lvbnNbaV0uaWQpO1xuICAgICAgICAgICAgTW9kdWxlLnNldERpbWVuc2lvblRvSW5zZXJ0KGF2YWlsYWJsZURpbWVuc2lvbnNbaV0uaWQpO1xuICAgICAgICAgICAgLyphdmFpbGFibGVEaW1lbnNpb25zLnNwbGljZShpLCAxKTsqLyAvLyBSZWJ1aWxkIGF0IGVhY2ggY2xpY2s/XG4gICAgICAgICAgICBpbnNlcnRpb25NZW51LmhpZGVQb3BVcCgpO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gbWFrZU1lbnVJdGVtKGxhYmVsLCBvbkNsaWNrKSB7XG4gICAgICAgICAgICB2YXIgaXRlbSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIik7XG4gICAgICAgICAgICBpZiAob25DbGljaykge1xuICAgICAgICAgICAgICAgIGl0ZW0uYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIG9uQ2xpY2ssIHRydWUpO1xuICAgICAgICAgICAgICAgIGl0ZW0uY2xhc3NOYW1lID0gXCJjbGlja2FibGVcIjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGl0ZW0udGV4dENvbnRlbnQgPSBsYWJlbDtcbiAgICAgICAgICAgIHJldHVybiBpdGVtO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gdXBkYXRlTWVudShtZW51Q29udGFpbmVyKSB7XG4gICAgICAgICAgICBjbGVhckNoaWxkcmVuKG1lbnVDb250YWluZXIpO1xuICAgICAgICAgICAgaWYgKGF2YWlsYWJsZURpbWVuc2lvbnMubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICAgICAgbWVudUNvbnRhaW5lci5hcHBlbmRDaGlsZChcbiAgICAgICAgICAgICAgICAgICAgbWFrZU1lbnVJdGVtKFwiTm8gZGltZW5zaW9uIHRvIGluc2VydFwiLCB1bmRlZmluZWQpXG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgbWVudUNvbnRhaW5lci5hcHBlbmRDaGlsZChcbiAgICAgICAgICAgICAgICAgICAgbWFrZU1lbnVJdGVtKFwiU2VsZWN0IGEgZGltZW5zaW9uXCIsIHVuZGVmaW5lZClcbiAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGF2YWlsYWJsZURpbWVuc2lvbnMpO1xuICAgICAgICAgICAgICAgIGF2YWlsYWJsZURpbWVuc2lvbnMuZm9yRWFjaChmdW5jdGlvbiAoZGltRWxlbSwgaSkge1xuICAgICAgICAgICAgICAgICAgICBtZW51Q29udGFpbmVyLmFwcGVuZENoaWxkKFxuICAgICAgICAgICAgICAgICAgICAgICAgbWFrZU1lbnVJdGVtKGRpbUVsZW0ubmFtZSwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGl0ZW1DbGljayhpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBtYWtlSW5zZXJ0aW9uTWVudShtZW51Q09udGFpbmVyKSB7XG4gICAgICAgICAgICBhdmFpbGFibGVEaW1lbnNpb25zID0gW107XG4gICAgICAgICAgICB2YXIgZGltcyA9IE1vZHVsZS5nZXREZWxldGVkRGltcygpO1xuICAgICAgICAgICAgaWYgKGRpbXMuc2l6ZSgpID4gMCkge1xuICAgICAgICAgICAgICAgIEFycmF5LnJhbmdlKDAsIGRpbXMuc2l6ZSgpIC0gMSkuZm9yRWFjaChmdW5jdGlvbiAoaSkge1xuICAgICAgICAgICAgICAgICAgICBhdmFpbGFibGVEaW1lbnNpb25zLnB1c2goe1xuICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogTW9kdWxlLmdldERpbWVuc2lvbkxhYmVsKGRpbXMuZ2V0KGkpKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkOiBkaW1zLmdldChpKVxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHVwZGF0ZU1lbnUobWVudUNPbnRhaW5lcik7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuXG5cbiAgICAgICAgaW5zZXJ0aW9uTWVudSA9IG1ha2VQb3BVcChtYWtlSW5zZXJ0aW9uTWVudSwgXCJpbnNlcnRpb25NZW51XCIpO1xuICAgICAgICBhdmFpbGFibGVEaW1lbnNpb25zID0gW107XG4gICAgfVxuXG4gICAgc2VsZi5oaWRlRXJyb3IgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGVycm9yQ29udGFpbmVyLnN0eWxlLnZpc2liaWxpdHkgPSBcImhpZGRlblwiO1xuICAgICAgICBNb2R1bGUuY2FudmFzLmhpZGRlbiA9IGZhbHNlO1xuICAgIH07XG5cbiAgICBzZWxmLmNsZWFyID0gZnVuY3Rpb24oKSB7XG4gICAgICAgZGF0YVRhYmxlLmNsZWFyKGdldFRhYmxlVmVydGljYWxTcGFjZSgpKTtcbiAgICB9O1xuXG4gICAgc2VsZi5vbkVycm9yID0gZnVuY3Rpb24gKGVycm9yQ29kZSkge1xuICAgICAgICB2YXIgZXJyb3JLZXksIGNvbnRlbnQsIHRpdGxlLCBzdWJ0aXRsZSwgc3VnZ2VzdGlvbnM7XG4gICAgICAgIGZvciAoZXJyb3JLZXkgaW4gZXJyb3JNZXNzYWdlcykge1xuICAgICAgICAgICAgaWYgKGVycm9yTWVzc2FnZXMuaGFzT3duUHJvcGVydHkoZXJyb3JLZXkpKSB7XG4gICAgICAgICAgICAgICAgaWYgKE1vZHVsZS5FcnJvci5oYXNPd25Qcm9wZXJ0eShlcnJvcktleSkpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKE1vZHVsZS5FcnJvcltlcnJvcktleV0udmFsdWUgPT09IGVycm9yQ29kZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29udGVudCA9IGVycm9yTWVzc2FnZXNbZXJyb3JLZXldO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSBlbHNlIGNvbnNvbGUubG9nKFwiRXJyb3IgY29kZSBub3QgZm91bmRcIik7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGNvbnRlbnQgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgY29udGVudCA9IGVycm9yTWVzc2FnZXMuVU5FWFBFQ1RFRDtcbiAgICAgICAgfVxuICAgICAgICBjbGVhckNoaWxkcmVuKGVycm9yQ29udGFpbmVyKTtcbiAgICAgICAgdGl0bGUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO1xuICAgICAgICB0aXRsZS5jbGFzc05hbWUgPSBcImVycm9yLXRpdGxlXCI7XG4gICAgICAgIHN1YnRpdGxlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKTtcbiAgICAgICAgc3VidGl0bGUuY2xhc3NOYW1lID0gXCJlcnJvci1zdWJ0aXRsZVwiO1xuICAgICAgICB0aXRsZS50ZXh0Q29udGVudCA9IFwiLyFcXFxcIFwiICsgY29udGVudC50aXRsZTtcbiAgICAgICAgc3VidGl0bGUudGV4dENvbnRlbnQgPSBjb250ZW50LnN1YnRpdGxlO1xuICAgICAgICBlcnJvckNvbnRhaW5lci5hcHBlbmRDaGlsZCh0aXRsZSk7XG4gICAgICAgIGVycm9yQ29udGFpbmVyLmFwcGVuZENoaWxkKHN1YnRpdGxlKTtcbiAgICAgICAgZXJyb3JDb250YWluZXIuYXBwZW5kQ2hpbGQoZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImhyXCIpKTtcbiAgICAgICAgaWYgKGNvbnRlbnQuc3VnZ2VzdGlvbnMubGVuZ3RoICE9PSAwKSB7XG4gICAgICAgICAgICBzdWdnZXN0aW9ucyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJ1bFwiKTtcbiAgICAgICAgICAgIGNvbnRlbnQuc3VnZ2VzdGlvbnMuZm9yRWFjaChmdW5jdGlvbiAocykge1xuICAgICAgICAgICAgICAgIHZhciBsaSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJsaVwiKTtcbiAgICAgICAgICAgICAgICBsaS50ZXh0Q29udGVudCA9IHM7XG4gICAgICAgICAgICAgICAgc3VnZ2VzdGlvbnMuYXBwZW5kQ2hpbGQobGkpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBlcnJvckNvbnRhaW5lci5hcHBlbmRDaGlsZChzdWdnZXN0aW9ucyk7XG4gICAgICAgIH1cbiAgICAgICAgZXJyb3JDb250YWluZXIuc3R5bGUudmlzaWJpbGl0eSA9IFwidmlzaWJsZVwiO1xuICAgICAgICBNb2R1bGUuY2FudmFzLmhpZGRlbiA9IHRydWU7XG5cbiAgICB9O1xuXG4gICAgZnVuY3Rpb24gZ2V0VGFibGVWZXJ0aWNhbFNwYWNlKCkge1xuICAgICAgICByZXR1cm4gd2luZG93LmlubmVySGVpZ2h0IC0gY29udGFpbmVyLm9mZnNldEhlaWdodDtcbiAgICB9XG4gICAgXG4gICAgc2VsZi5vbkRhdGFMb2FkZWQgPSBmdW5jdGlvbiAoZGF0YSwgd2l0aEhlYWRlcikge1xuICAgICAgaWYgKFZFUlNJT04gPT0gJ2xvY2FsJylcbiAgICAgICAgZGF0YVRhYmxlLnVwZGF0ZShkYXRhLCB3aXRoSGVhZGVyLCBnZXRUYWJsZVZlcnRpY2FsU3BhY2UoKSk7XG4gICAgfTtcblxuICAgIGZ1bmN0aW9uIGluaXRNb2RhbCgpIHtcbiAgICAgICAgdmFyIG1vZGFsQm9keSA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoXCJtb2RhbC1ib2R5XCIpWzBdO1xuICAgICAgICByZXF1ZXN0KFBhdGguaGVscENvbnRlbnQsIFwidGV4dC9wbGFpblwiLCBmdW5jdGlvbiAoZikge1xuICAgICAgICAgICAgdmFyIGxpbmVzID0gZi5zcGxpdChcIlxcblwiKTtcbiAgICAgICAgICAgIG1vZGFsQm9keS5hcHBlbmRDaGlsZChtYWtlTGF5b3V0KFwiaGVscC1jb250ZW50XCIsIFwic3RyaXBwZWQtdGl0bGVcIiwgUGFyc2VyLnBhcnNlKGxpbmVzKSkpO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBpbml0RXJyb3JNZXNzYWdlcygpIHtcbiAgICAgICAgcmVxdWVzdChQYXRoLmVycm9yQ29udGVudCwgXCJ0ZXh0L2pzb25cIiwgZnVuY3Rpb24gKGYpIHtcbiAgICAgICAgICAgIGVycm9yTWVzc2FnZXMgPSBKU09OLnBhcnNlKGYpO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBzZWxmLm9uUmVzaXplID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAvL01vZHVsZS5zZXRDYW52YXNTaXplKGNhbnZhc0NvbnRhaW5lci5jbGllbnRXaWR0aCwgY2FudmFzQ29udGFpbmVyLmNsaWVudEhlaWdodCk7XG4gICAgICAgIC8vTW9kdWxlLmNhbnZhcy53aWR0aCA9IHNlbGYuY2FudmFzQ29udGFpbmVyLmNsaWVudFdpZHRoO1xuICAgICAgICAvL01vZHVsZS5jYW52YXMuaGVpZ2h0ID0gc2VsZi5jYW52YXNDb250YWluZXIuY2xpZW50SGVpZ2h0O1xuICAgICAgICAvLyBodHRwczovL3d3dy5raHJvbm9zLm9yZy93ZWJnbC93aWtpL0hhbmRsaW5nSGlnaERQSVxuICAgICAgICBNb2R1bGUuY2FudmFzLnN0eWxlLndpZHRoID0gc2VsZi5jYW52YXNDb250YWluZXIuY2xpZW50V2lkdGggKyBcInB4XCI7XG4gICAgICAgIE1vZHVsZS5jYW52YXMuc3R5bGUuaGVpZ2h0ID0gc2VsZi5jYW52YXNDb250YWluZXIuY2xpZW50SGVpZ2h0ICsgXCJweFwiO1xuICAgICAgICB2YXIgZGV2aWNlUGl4ZWxSYXRpbyA9IHdpbmRvdy5kZXZpY2VQaXhlbFJhdGlvIHx8IDE7XG4gICAgICAgIE1vZHVsZS5jYW52YXMud2lkdGggPSBzZWxmLmNhbnZhc0NvbnRhaW5lci5jbGllbnRXaWR0aCAqIGRldmljZVBpeGVsUmF0aW87XG4gICAgICAgIE1vZHVsZS5jYW52YXMuaGVpZ2h0ID0gc2VsZi5jYW52YXNDb250YWluZXIuY2xpZW50SGVpZ2h0ICogZGV2aWNlUGl4ZWxSYXRpbztcbiAgICAgICAgXG4gICAgICAgIE1vZHVsZS5vblJlc2hhcGUoTW9kdWxlLmNhbnZhcy53aWR0aCwgTW9kdWxlLmNhbnZhcy5oZWlnaHQpO1xuICAgICAgICBpZiAoVkVSU0lPTiA9PSAnbG9jYWwnKVxuICAgICAgICAgIGRhdGFUYWJsZS5vblJlc2l6ZShnZXRUYWJsZVZlcnRpY2FsU3BhY2UoKSk7XG4gICAgfTtcblxuICAgIGZ1bmN0aW9uIG1ha2VCdWJibGVJbmZvKGNvbnRhaW5lcikge1xuICAgIFxuICAgICAgICBmdW5jdGlvbiBmb3JtYXRQaWNrRW50cnkobGFiZWwsIHZhbHVlKSB7XG4gICAgICAgICAgIHJldHVybiBcIjxiPlwiICsgbGFiZWwgKyBcIjwvYj46IFwiICsgZm9ybWF0KHZhbHVlKTtcbiAgICAgICAgfVxuICAgIFxuICAgICAgICBmdW5jdGlvbiBmb3JtYXRQaWNrUGVyY2VudGFnZSh2YWx1ZSkge1xuICAgICAgICAgICByZXR1cm4gZm9ybWF0UGlja0VudHJ5KFwiaGlnaGxpZ2h0ZWRcIiwgdmFsdWUpICsgXCIlXCI7XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBmb3JtYXQobnVtYmVyKSB7XG4gICAgICAgICAgICByZXR1cm4gKE51bWJlci5pc0ludGVnZXIobnVtYmVyKSA/IG51bWJlciA6IG51bWJlci50b0ZpeGVkKDIpKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHZhciBsYWJlbHMgPSBbXCJ3ZWlnaHRcIiwgXCJtaW5cIiwgXCJtYXhcIiwgXCJhdmdcIl0sIC8vIE5vbi1jYXRlZ29yaWNhbCBub2Rlc1xuICAgICAgICAgICAgaW5mbyA9IE1vZHVsZS5nZXROdW1lcmljYWxJbmZvKCksXG4gICAgICAgICAgICBjYXRlZ29yeSA9IE1vZHVsZS5nZXRDYXRlZ29yeSgpLFxuICAgICAgICAgICAgc3BhbiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJzcGFuXCIpLFxuICAgICAgICAgICAgaXNWYWxpZDtcbiAgICAgICAgY2xlYXJDaGlsZHJlbihjb250YWluZXIpO1xuICAgICAgICBcbiAgICAgICAgdmFyIGlzQ2F0ZWdvcmljYWxOb2RlID0gY2F0ZWdvcnkubGVuZ3RoID4gMDtcbiAgICAgICAgdmFyIGlzRWRnZSA9ICFpc0NhdGVnb3JpY2FsTm9kZSAmJiBpbmZvLnNpemUoKSA8PSAyO1xuICAgICAgICB2YXIgaXNSZWd1bGFyTm9kZSA9IGluZm8uc2l6ZSgpID49IGxhYmVscy5sZW5ndGg7XG4gICAgICAgIHZhciBoYXNQZXJjZW50YWdlID0gKCFpc0NhdGVnb3JpY2FsTm9kZSAmJiBpbmZvLnNpemUoKSA9PSAyKSB8fCBpbmZvLnNpemUoKSA9PSBsYWJlbHMubGVuZ3RoICsgMTtcbiAgICAgICAgXG4gICAgICAgIHZhciBodG1sTGluZXMgPSBbXTtcbiAgICAgICAgXG4gICAgICAgIC8vIEZpcnN0IGNhc2U6IGNhdGVnb3JpY2FsIG5vZGVzXG4gICAgICAgIGlmIChpc0NhdGVnb3JpY2FsTm9kZSkge1xuICAgICAgICAgICBodG1sTGluZXMucHVzaChcIjxiPlwiICsgY2F0ZWdvcnkgKyBcIjwvYj5cIik7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGlzRWRnZSB8fCBpc0NhdGVnb3JpY2FsTm9kZSkge1xuICAgICAgICAgICBodG1sTGluZXMucHVzaChmb3JtYXRQaWNrRW50cnkoXCJ3ZWlnaHRcIiwgaW5mby5nZXQoMCkpKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAoaXNSZWd1bGFyTm9kZSkge1xuICAgICAgICAgICBsYWJlbHMuZm9yRWFjaChmdW5jdGlvbiAobGFiZWwsIGluZGV4KSB7XG4gICAgICAgICAgICAgaHRtbExpbmVzLnB1c2goZm9ybWF0UGlja0VudHJ5KGxhYmVsLCBpbmZvLmdldChpbmRleCkpKTtcbiAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGhhc1BlcmNlbnRhZ2UpXG4gICAgICAgICAgIGh0bWxMaW5lcy5wdXNoKGZvcm1hdFBpY2tQZXJjZW50YWdlKGluZm8uZ2V0KGlzUmVndWxhck5vZGUgPyBsYWJlbHMubGVuZ3RoIDogMSkpKTtcbiAgICAgICAgICAgXG4gICAgICAgIHNwYW4uaW5uZXJIVE1MID0gaHRtbExpbmVzLmpvaW4oXCI8YnIvPlwiKTtcbiAgICAgICAgY29udGFpbmVyLmFwcGVuZENoaWxkKHNwYW4pO1xuICAgICAgIFxuICAgICAgICByZXR1cm4gaXNFZGdlIHx8IGlzUmVndWxhck5vZGUgfHwgaXNDYXRlZ29yaWNhbE5vZGU7XG4gICAgfVxuXG5cbiAgICBmdW5jdGlvbiBvblJlYWR5KCkge1xuICAgICAgICBzZWxmLmNhbnZhc0NvbnRhaW5lciA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY2FudmFzQ29udGFpbmVyXCIpO1xuICAgICAgICBzZWxmLmNhbnZhc0NvbnRhaW5lci5vbnJlc2l6ZSA9IHNlbGYub25SZXNpemU7XG4gICAgICAgIC8vIFJlYmluZGluZyBlbXNjcmlwdGVuIEdMRlcgd2luZG93IGxpc3RlbmVycyB0byBjYW52YXNcbiAgICAgICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJrZXlwcmVzc1wiLCBHTEZXLm9uS2V5UHJlc3MsIHRydWUpO1xuICAgICAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcihcImtleWRvd25cIiwgR0xGVy5vbktleWRvd24sIHRydWUpO1xuICAgICAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcihcImtleXVwXCIsIEdMRlcub25LZXl1cCwgdHJ1ZSk7XG4gICAgICAgIE1vZHVsZS5jYW52YXMuYWRkRXZlbnRMaXN0ZW5lcihcImtleXByZXNzXCIsIEdMRlcub25LZXlQcmVzcywgdHJ1ZSk7XG4gICAgICAgIE1vZHVsZS5jYW52YXMuYWRkRXZlbnRMaXN0ZW5lcihcImtleWRvd25cIiwgR0xGVy5vbktleWRvd24sIHRydWUpO1xuICAgICAgICBNb2R1bGUuY2FudmFzLmFkZEV2ZW50TGlzdGVuZXIoXCJrZXl1cFwiLCBHTEZXLm9uS2V5dXAsIHRydWUpO1xuICAgICAgICAvLyBBZGRpbmcgbWlzc2luZyBsaXN0ZW5lcnNcbiAgICAgICAgTW9kdWxlLmNhbnZhcy5hZGRFdmVudExpc3RlbmVyKFwibW91c2V1cFwiLCBHTEZXLm9uTW91c2VCdXR0b25VcCwgdHJ1ZSk7XG4gICAgICAgIC8vIFByZXZlbnQgZGVmYXVsdCBldmVudCBmb3IgbWV0YSBrZXlzIEN0cmwgYW5kIFNoaWZ0XG4gICAgICAgIE1vZHVsZS5jYW52YXMub25rZXlkb3duID0gZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIGUgPSBlIHx8IHdpbmRvdy5ldmVudDtcbiAgICAgICAgICAgIGlmIChlLmN0cmxLZXkgfHwgZS5zaGlmdEtleSkge1xuICAgICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgICBzZWxmLnRvb2xCYXIgPSBtYWtlVG9vbEJhcihzZWxmKTtcbiAgICAgICAgaW5pdE1vZGFsKCk7XG4gICAgICAgIGluaXRUb29sdGlwKCk7XG4gICAgICAgIGluaXRJbnNlcnRpb25NZW51KCk7XG4gICAgICAgIC8vIENyZWF0aW5nIGFsbCBzdWItb2JqZWN0LCBzaGFsbG93IGlmIHVudXNlZCBpbiBjdXJyZW50IHZlcnNpb25cbiAgICAgICAgLy8gKHR5cGljYWxseSwgaW5hY3RpdmF0ZWQgaW4gcmVtb3RlL2NsaWVudCB2ZXJzaW9uKVxuICAgICAgICBwaWNraW5nQnViYmxlID0gbWFrZVBvcFVwKG1ha2VCdWJibGVJbmZvLCBcImluZm9CdWJibGVcIik7XG4gICAgICAgIGlmIChWRVJTSU9OID09PSAnbG9jYWwnKVxuICAgICAgICAgIGRhdGFUYWJsZSA9IG1ha2VEYXRhVGFibGUoXCJyZXN1bHRUYWJsZVwiKTtcbiAgICAgICAgaW5pdEVycm9yTWVzc2FnZXMoKTtcbiAgICAgICAgc2VsZi5vblJlc2l6ZSgpO1xuICAgIH1cblxuXG4gICAgc2VsZi5zZXRDdXJzb3IgPSBmdW5jdGlvbiAobW9kZSkge1xuICAgICAgICBzd2l0Y2ggKG1vZGUpIHtcbiAgICAgICAgICAgIGNhc2Ugdml6RXZlbnQuRW51bS5JTlZFUlRfQVhJUzpcbiAgICAgICAgICAgICAgICBNb2R1bGUuY2FudmFzLnN0eWxlLmN1cnNvciA9IFwicm93LXJlc2l6ZVwiO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSB2aXpFdmVudC5FbnVtLk1PVkVfQVhJUzpcbiAgICAgICAgICAgICAgICBNb2R1bGUuY2FudmFzLnN0eWxlLmN1cnNvciA9IFwiZXctcmVzaXplXCI7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIHZpekV2ZW50LkVudW0uREVMRVRFX0FYSVM6XG4gICAgICAgICAgICAgICAgTW9kdWxlLmNhbnZhcy5zdHlsZS5jdXJzb3IgPSBcInBvaW50ZXJcIjtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2Ugdml6RXZlbnQuRW51bS5JTlNFUlRfRElNRU5TSU9OOlxuICAgICAgICAgICAgICAgIE1vZHVsZS5jYW52YXMuc3R5bGUuY3Vyc29yID0gXCJjcm9zc2hhaXJcIjtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2Ugdml6RXZlbnQuRW51bS5QSUNLRVI6XG4gICAgICAgICAgICAgICAgTW9kdWxlLmNhbnZhcy5zdHlsZS5jdXJzb3IgPSBcImhlbHBcIjtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgTW9kdWxlLmNhbnZhcy5zdHlsZS5jdXJzb3IgPSBcImRlZmF1bHRcIjtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgIH07XG5cbiAgICBzZWxmLmZsdXNoUHJldmlvdXNNb2RlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBNb2R1bGUuY2FudmFzLnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBwaWNraW5nQnViYmxlLmRpc3BsYXkpO1xuICAgICAgICBNb2R1bGUuY2FudmFzLnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBpbnNlcnRpb25NZW51LmRpc3BsYXkpO1xuICAgICAgICBpbnNlcnRpb25NZW51LmhpZGVQb3BVcCgpO1xuICAgICAgICBwaWNraW5nQnViYmxlLmhpZGVQb3BVcCgpO1xuICAgIH07XG5cbiAgICBzZWxmLm9uQnV0dG9uQ2xpY2sgPSBmdW5jdGlvbiAobmV3TW9kZSkge1xuICAgICAgICAvLyBPbmx5IGNhbGxlZCBmb3IgbW9kZSBzd2l0Y2gsIG5ld01vZGUgY2Fubm90IGJlIGEgaGlnaGxpZ2h0aW5nIGV2ZW50LFxuICAgICAgICAvLyBjZW50ZXIsIGZpdCwgb3IgaGVscCBjbGlja1xuICAgICAgICBzZWxmLmZsdXNoUHJldmlvdXNNb2RlKCk7XG4gICAgICAgIGlmIChuZXdNb2RlKSB7XG4gICAgICAgICAgICBpZiAodml6RXZlbnQuY3VycmVudFN0YXRlLnZhbHVlID09PSBuZXdNb2RlLnZhbHVlKSB7XG4gICAgICAgICAgICAgICAgTW9kdWxlLmNoYW5nZU1vZGUodml6RXZlbnQuRW51bS5ERUZBVUxULnZhbHVlKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgTW9kdWxlLmNoYW5nZU1vZGUobmV3TW9kZS52YWx1ZSk7IC8vIGNvbXBhdGlibGUgdy8gTW9kdWxlLlN0YXRlXG4gICAgICAgICAgICAgICAgaWYgKG5ld01vZGUgPT09IHZpekV2ZW50LkVudW0uUElDS0VSKSB7XG4gICAgICAgICAgICAgICAgICAgIE1vZHVsZS5jYW52YXMuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIHBpY2tpbmdCdWJibGUuZGlzcGxheSk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChuZXdNb2RlID09PSB2aXpFdmVudC5FbnVtLklOU0VSVF9ESU1FTlNJT04pIHtcbiAgICAgICAgICAgICAgICAgICAgTW9kdWxlLmNhbnZhcy5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgaW5zZXJ0aW9uTWVudS5kaXNwbGF5KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBzZWxmLnNldEN1cnNvcih2aXpFdmVudC5jdXJyZW50U3RhdGUpO1xuICAgICAgICB9XG4gICAgfTtcblxuICAgIG9uUmVhZHkoKTtcbiAgICByZXR1cm4gc2VsZjtcbn1cbiIsInZhciBWRVJTSU9OID0gXCJsb2NhbFwiOyBcblxudmFyIHRvb2xCYXJUb29scyA9IHtcbiAgbGVmdCA6IHtcbiAgICBwYW46IHtcbiAgICAgIHRpdGxlOiBcIkV4cGxvcmF0aW9uXCIsXG4gICAgICBrZXk6IFwiU3BhY2VcIixcbiAgICAgIHN0YXRlOiB2aXpFdmVudC5FbnVtLkRFRkFVTFRcbiAgICB9LFxuICAgIHNjYWxlOiB7XG4gICAgICB0aXRsZTogXCJTY2FsZVwiLFxuICAgICAga2V5OiBcIlNcIixcbiAgICAgIHN0YXRlOiB2aXpFdmVudC5FbnVtLlNDQUxFXG4gICAgfSxcbiAgICBkZWxldGU6IHtcbiAgICAgIHRpdGxlOiBcIkRlbGV0ZVwiLFxuICAgICAga2V5OiBcIlRcIixcbiAgICAgIHN0YXRlOiB2aXpFdmVudC5FbnVtLkRFTEVURV9BWElTXG4gICAgfSxcbiAgICBpbnNlcnQ6IHtcbiAgICAgIHRpdGxlOiBcIkluc2VydFwiLFxuICAgICAga2V5OiBcIldcIixcbiAgICAgIHN0YXRlOiB2aXpFdmVudC5FbnVtLklOU0VSVF9ESU1FTlNJT05cbiAgICB9LFxuICAgIGludmVydDoge1xuICAgICAgdGl0bGU6IFwiSW52ZXJzaW9uXCIsXG4gICAgICBrZXk6IFwiSVwiLFxuICAgICAgc3RhdGU6IHZpekV2ZW50LkVudW0uSU5WRVJUX0FYSVNcbiAgICB9LFxuICAgIG1vdmU6IHtcbiAgICAgIHRpdGxlOiBcIk1vdmVcIixcbiAgICAgIGtleTogXCJEXCIsXG4gICAgICBzdGF0ZTogdml6RXZlbnQuRW51bS5NT1ZFX0FYSVNcbiAgICB9LFxuICAgIGZpbHRlcjoge1xuICAgICAgdGl0bGU6IFwiRmlsdGVyXCIsXG4gICAgICBrZXk6IFwiWVwiLFxuICAgICAgc3RhdGU6IHZpekV2ZW50LkVudW0uRklMVEVSXG4gICAgfSxcbiAgICBwaWNrZXI6IHtcbiAgICAgIHRpdGxlOiBcIkluZm8gcGlja2VyXCIsXG4gICAgICBrZXk6IFwiUFwiLFxuICAgICAgc3RhdGU6IHZpekV2ZW50LkVudW0uUElDS0VSXG4gICAgfVxuICB9LFxuICAgIHJpZ2h0IDoge1xuICAgICAgY2VudGVyIDoge1xuICAgICAgICB0aXRsZTogXCJDZW50ZXJcIixcbiAgICAgICAga2V5OiBcIkNcIlxuICAgICAgfSxcbiAgICAgIGZpdDoge1xuICAgICAgICB0aXRsZTogXCJGaXRcIixcbiAgICAgICAga2V5OiBcIkZcIlxuICAgICAgfSxcbiAgICAgIGhlbHA6IHtcbiAgICAgICAgdGl0bGU6IFwiSGVscFwiXG4gICAgICB9XG4gICAgfVxuICB9XG4iXX0=
