#!/bin/bash
NB_CORES=8 # Set your own number of cores
SHARED_DIRECTORIES=("fonts" "thirdparty")

# Check executing environment
if [[ ! -f ./gulpfile.js ]];
then
    >&2 echo "Could not find gulp file. Please execute from the root dir of the web application."
    exit 0
fi


if [[ "$#" -eq 0 ]];
then
   echo "No argument provided, default parameters will be used";
fi


# Default parameters
export mode="Standalone" # or 'Client'
export hierarchical="OFF"
export emscripten_SDK_dir="/home/joris/Applications/emsdk-portable/"
export browser="sensible-browser"
export launch_server=false
export script_directory="$( cd "$( dirname "$0" )" && pwd )"
echo "${script_directory}"
export install_dir="${script_directory}/../web/src/bin";

parameters() {
    echo "Using parameters:"
    echo -e "\tMode: ${mode} (-m Standalone|Client)";
    echo -e "\tHierarchical navigation: ${hierarchical} (-h)";
    echo -e "\tEmscripten SDK: ${emscripten_SDK_dir} (-e)";
    echo -e "\tLaunch & serve to browser: ${launch_server} (-s)";
}

usage() { echo "Usage: $0 [-m <Standalone|Client>] [-h] [-e <path>] [-c] [-s]" 1>&2; exit 1; }

clean() { rm -rf jsbuild;}

# Detect help call
if [[ "$1" == "--help" ]];
then
   echo "Utility to launch the web interface for parallel coordinates app";
   usage
   echo "Default parameters are:";
   parameters
   exit 0
fi

# Parse other parameters
while getopts "m:e:csh" opt; do
    case "${opt}" in
        m)
            [[ ${OPTARG} == "Standalone" || ${OPTARG} == "Client" ]] || usage;
            mode=${OPTARG}
            ;;
        h)
            hierarchical="ON"
            ;;
        e)
            if [[ ! -f "${OPTARG}/emsdk_env.sh" ]];
            then
                echo "Invalid Emscripten SDK location"
                usage
            else
                emscripten_SDK_dir=${OPTARG}
            fi
            ;;
        c)
            clean
            ;;
        s)
            launch_server=true;
            ;;
        *)
            usage
            ;;
    esac
    #shift $((OPTIND-1))

    if [ -z "${emscripten_SDK_dir}" ] || [ -z "${mode}" ] || [ -z "${hierarchical}" ]; then
        usage
    fi
done

# Print parsed parameters
parameters

# Prepare environement
source ${emscripten_SDK_dir}/emsdk_env.sh
mkdir -p ${script_directory}/src/bin;
cd ${script_directory}/../multimode-version;

# Creating symbolic links for shared directory
for DIR in ${SHARED_DIRECTORIES[@]}
do
	if [ ! \( -e $DIR \) ]
		then
		ln -s ../$DIR $DIR
	fi
done

mkdir -p jsbuild; cd jsbuild;

echo "Compiling..."
emcmake cmake .. -DJS_INSTALL_DIR=${install_dir} -DCMAKE_BUILD_TYPE=Release -DHIERARCHICAL=${hierarchical} -DBASE_MODE=${mode}
echo "Building..."
emmake make -j$(($NB_CORES +1))
emmake make install

cd ${script_directory};

# Install dependencies
if [[ ! -d ./node_modules || ! -d ./bower_components ]];
then
    npm install
    bower install
fi

# Launch server
if [ ${launch_server} == true ];
then
    if [[ ${mode} == "Standalone" ]];
    then
        gulp live --mode local
    else
        gulp live --mode remote
    fi
fi
