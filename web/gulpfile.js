var gulp = require('gulp'),
    gutil = require('gulp-util'),
    less = require('gulp-less'),
    path = require('path'),
    jshint = require('gulp-jshint'),
    rename = require('gulp-rename'),
    inject = require('gulp-inject'),
    concat = require('gulp-concat'),
    browserSync = require('browser-sync'),
    sourcemaps = require('gulp-sourcemaps'),
    run = require('gulp-run'),
    extender = require('gulp-html-extend');
var reload = browserSync.reload;

String.prototype.capitalizeFirst = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
};

/*
* Workflow options:
*    - --mode {local,remote}
*    - --type {production} // Uglifying JS actually strip Module method names. Should not be used
*/

gulp.task('default', ['build']);

gulp.task('build', ['build-less', 'build-js', 'copy-cpp-js', 'inject-html', 'inject']);

gulp.task('live', ['watch', 'build', 'serve']);

gulp.task('emscripten', function () {

});

/*
* Compose app html from main file and separate html components
* and move resulting file to public folder
*/
gulp.task('inject-html', function () {
  var main = 'main' + gutil.env.mode.capitalizeFirst() + '.html';
  return gulp.src('./src/templates/' + main)
             .pipe(extender({annotations:true,verbose:true}))
             .pipe(rename('index.html'))
             .pipe(gulp.dest('./public/'));
});

/*
* Execute linter on vanilla js
*/
gulp.task('jshint', function() {
  return gulp.src('src/js/*.js')
             .pipe(jshint())
             .pipe(jshint.reporter('jshint-stylish'));
});

/*
* "I am the sword in the darkness
* I am the watcher on the walls
* I am the fire that burns against the cold
* The light that brings the dawn
* The horn that wakes the sleepers
* I am the shield that guards the realms of men.""
*/

gulp.task('watch', function() {
  gulp.watch('./src/**/*.js', ['jshint', 'build-js']);
  gulp.watch('./src/bin/*', ['copy-cpp-js']);
  gulp.watch('./src/templates/*.html', ['inject-html', 'inject']);
  gulp.watch('./src/less/*.less', ['build-less']);
});

/*
* Merge js files and move finished product into public folder
*/
gulp.task('build-js', function() {
    var mode = gutil.env.mode || 'local';
  var param = './params/' + mode + '.js';
  return gulp.src(['./js/*.js', param], {cwd : './src'})
  .pipe(sourcemaps.init())
  .pipe(concat('ui.js'))
  .pipe(sourcemaps.write())
  .pipe(gulp.dest('public/scripts'));
});

/*
* Move emscripten output files to public folder
*/
gulp.task('copy-cpp-js', function() {
  return gulp.src(['*' + "paraCoord" + '*'], {cwd : './src/bin'})
             .pipe(gulp.dest('./public/'));
});

/*
* LESS IS MORE!
* Move output stylish style sheets to public folder
*/
gulp.task('build-less', function () {
  return gulp.src(['src/less/*.less'])
  .pipe(sourcemaps.init())  // Process the original sources
  .pipe(less({
    paths: [ path.join(__dirname, 'less', 'includes') ]
  }))
  .pipe(sourcemaps.write()) // Add the map to modified source.
  .pipe(concat('styles.css'))
  .pipe(gulp.dest('./public/styles/'));
});

gulp.task('serve', function() {
  browserSync({
    server: {
      baseDir: 'public',
      openBrowser: false
    }
  });

  gulp.watch(['./**/*.html', './**/*.css', './**/*.js*'], {cwd: 'public'}, reload);
});

var injectJsCss = function(mode) {
  var target = gulp.src('./public/index.html');
  var sources = gulp.src(['./scripts/ui.js',
                          //'scripts/paraCoord' + mode.capitalizeFirst() + '.js',
                          './styles/*.css'],
                          {read: false, cwd: './public'});
  return target.pipe(inject(sources, {relative: true}))
               .pipe(gulp.dest('./public'));
};

/*
* Defines both injecting task with dependency on the html building proces and
* the css/js building and copying processes
*/

gulp.task('inject', ['inject-html', 'build-js', 'copy-cpp-js', 'build-less'], function() {
  return (injectJsCss(gutil.env.mode))
});
