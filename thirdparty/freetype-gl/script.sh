#!/bin/sh
BOLD="\033[1m"
GREEN_B="\033[32m"
NORM="\033[0m"

EMSC_DIR=/opt/emsdk/emscripten/master/

echo "$BOLD$GREEN_B" "Generating texture-*.o...$NORM"
$EMSC_DIR/em++ -O2 -c -I. -s USE_FREETYPE=1 texture-*.c
echo "$BOLD$GREEN_B" "Generating vector.o...$NORM"
$EMSC_DIR/em++ -O2 -c vector.c
#echo "$BOLD$GREEN_B" "Generating texture-font.o...$NORM"
#$EMSC_DIR/em++ -O2 -c texture-font.c
echo "$BOLD$GREEN_B" "Generating edtaa3func.o...$NORM"
$EMSC_DIR/em++ -O2 -c edtaa3func.c
echo "$BOLD$GREEN_B" "Binding freetypeGL.o...$NORM"
$EMSC_DIR/em++ -O2 texture-*.o vector.o edtaa3func.o -o freetypeGL.o

if [ ! -d obj ]; then
    mkdir obj
fi

mv freetypeGL.o obj/libfreetypegl.o
echo "$BOLD$GREEN_B" "freetypeGL.o ready"
echo " Cleaning directory...$NORM"
rm *.o
