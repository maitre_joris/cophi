SET(FREETYPE_GL_SRC
        texture-atlas.c
        texture-font.c
        edtaa3func.c
        vector.c)

include_directories(${FREETYPE_INCLUDE_DIRS} .)
set(FREETYPE_GL_LIBRARY freetype-gl)

#Setting up Freetype as well as Freetype GL
if (NOT EMSCRIPTEN)
  add_library(${FREETYPE_GL_LIBRARY} STATIC ${FREETYPE_GL_SRC})
  target_link_libraries(${FREETYPE_GL_LIBRARY} ${FREETYPE_LIBRARY})
else (NOT EMSCRIPTEN)
  add_definitions("-s USE_FREETYPE=1")
endif (NOT EMSCRIPTEN)



